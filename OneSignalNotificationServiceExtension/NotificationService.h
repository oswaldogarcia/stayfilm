//
//  NotificationService.h
//  OneSignalNotificationServiceExtension
//
//  Created by Henrique Ormonde on 21/08/17.
//  Copyright © 2017 Stayfilm. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
