//
//  VideoUploadOperation.h
//  Stayfilm for Messenger
//
//  Created by Henrique Ormonde on 26/10/15.
//  Copyright © 2015 Stayfilm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Media.h"

@protocol VideoUploaderDelegate;

@interface VideoUploadOperation : NSOperation <NSURLSessionDelegate, NSURLSessionTaskDelegate, NSURLSessionDataDelegate>

@property (nonatomic, assign) id <VideoUploaderDelegate> delegate;
@property (nonatomic, strong) NSURLSession *sessionURLBackground;
@property (nonatomic, strong) NSString *pathFile;
@property (nonatomic, strong) Media *uploadedMedia;


- (id)initWithMediaPath:(NSString *)p_pathFile withEcho:(NSString *)p_echo withAlbumID:(NSString *)p_albumID withBackgroundSessionIdentifier:(NSString *)identifier delegate:(id<VideoUploaderDelegate>) theDelegate;

@end

@protocol VideoUploaderDelegate <NSObject>
- (void)videoUploaderDidFinish:(VideoUploadOperation *)uploader;
- (void)videoUploaderDidFail:(VideoUploadOperation *)uploader;


@end
