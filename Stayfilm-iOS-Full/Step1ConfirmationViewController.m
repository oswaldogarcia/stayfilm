  //
//  Step1ConfirmationViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Henrique Ormonde on 06/11/17.
//  Copyright © 2017 Stayfilm. All rights reserved.
//

#import "Step1ConfirmationViewController.h"
#import "Step3StylesViewController.h"
#import "StayfilmApp.h"
#import <Google/Analytics.h>
#import "PhotoCollectionViewCell.h"
#import "MovieMakerStep1ViewController.h"
#import "PlayerViewController.h"
#import "PopupMenuAnimation.h"
#import "RemoverViewController.h"
#import "ReorderViewController.h"
#import "AlbumViewerViewController.h"
#import "UIView+Toast.h"

@interface Step1ConfirmationViewController ()

@property (nonatomic, strong) NSArray *initialSelectedMedias;
@property (nonatomic, weak) StayfilmApp *sfApp;
@property (nonatomic, strong) Uploader* uploaderStayfilm;

@property (nonatomic, strong) UIViewController *modalPreview;
@property (nonatomic, assign) BOOL isLongPressing;

@property (nonatomic, strong) PopupMenuAnimation *transitionAnimation;

@property (nonatomic, weak) RemoverViewController *removerVC;
@property (nonatomic, weak) ReorderViewController *reorderVC;
@property (nonatomic, weak) MovieMakerStep1ViewController *addVC;

//@property (nonatomic, strong) UIColor *barColor;

@end

@implementation Step1ConfirmationViewController

@synthesize photosCollectionView, thumbConnection, modalPreview, videoImageManager, transitionAnimation, uploaderStayfilm, sfApp;
@synthesize but_back, but_MoreOptions, txt_movieTitle, txt_stripInfo;

static NSString *cellIdentifierPhoto = @"ConfirmItemCell";



-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
//    self.navigationController.navigationBar.shadowImage = [[UIImage alloc] init];
//    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationItem.backBarButtonItem setTitle:@""];
    
    self.automaticallyAdjustsScrollViewInsets = NO;

    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    self.thumbConnection = [NSURLSession sessionWithConfiguration:config];

    self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    //self.uploaderStayfilm = [Uploader sharedUploadStayfilmSingletonWithDelegate:self];
    
    if(self.sfApp.isEditingMode) {
        self.initialSelectedMedias = [self.sfApp.editedMedias copy];
    } else {
        self.initialSelectedMedias = [self.sfApp.finalMedias copy];
    }
    
    [self.photosCollectionView registerClass:[PhotoCollectionViewCell class] forCellWithReuseIdentifier:cellIdentifierPhoto];
    
    self.isLongPressing = NO;
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = 1; //seconds
    lpgr.delegate = self;
    [self.photosCollectionView addGestureRecognizer:lpgr];

    if (self.sfApp.finalMedias.count > 0 && ![self.editingType isEqualToString:@"title"]) {
        [self updateFilmStrip];
    }

//    //Google Analytics
//    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName value:@"Iphone_Nativo_Tela_4_SFMobile_Passo_2_escolher-fotos_facebook-album"];
//    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    //[self.sfApp.filmStrip setHidden:NO];
    
    
    
    self.transitionAnimation = [[PopupMenuAnimation alloc] init];
    
    [self.txt_movieTitle addTarget:self action:@selector(txt_movieTitle_Ended) forControlEvents:UIControlEventEditingDidEndOnExit];
    [self.txt_movieTitle addTarget:self action:@selector(txt_movieTitle_Changed) forControlEvents:UIControlEventEditingChanged];
    //self.barColor = self.navigationController.navigationBar.barTintColor;
    
    // this code is just in case there is a navigation bar
//    UIApplication *app = [UIApplication sharedApplication];
//    CGFloat statusBarHeight = app.statusBarFrame.size.height;
//    UIView *statusBarView = [[UIView alloc] initWithFrame:CGRectMake(0, -statusBarHeight, [UIScreen mainScreen].bounds.size.width, statusBarHeight)];
//    statusBarView.backgroundColor = [UIColor colorWithRed:51.0/255 green:87.0/255 blue:113.0/255 alpha:1.0];
//    [self.navigationController.navigationBar addSubview:statusBarView];
    
 /*   UIApplication *app = [UIApplication sharedApplication];
    CGFloat statusBarHeight = app.statusBarFrame.size.height;
    UIView *statusBarView =  [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, statusBarHeight)];
    statusBarView.backgroundColor  =  [UIColor colorWithRed:51.0/255.0 green:87.0/255.0 blue:113.0/255.0 alpha:1.0];
    [self.view addSubview:statusBarView];
    
    [self setNeedsStatusBarAppearanceUpdate];*/
}

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBar.hidden = YES;
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];

    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = [UIColor colorWithRed:0.20 green:0.34 blue:0.44 alpha:1.0];
    }

    if(self.sfApp == nil)
    {
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    }
   
    [self.sfApp.filmStrip setHidden:NO];
    
    [self.sfApp.filmStrip setViewInsideFilmStrip];
    
    NSString *langcode = [[NSLocale currentLocale] objectForKey:NSLocaleLanguageCode];
    
    if([langcode isEqualToString:@"pt"])
    {
        self.tapToStartImage.image = [UIImage imageNamed:@"messageAddPortuguese"];
    }
    else if([langcode isEqualToString:@"es"])
    {
        self.tapToStartImage.image = [UIImage imageNamed:@"messageAddSpanish"];
    }
    
    if(self.sfApp.isEditingMode) {
        if([self.editingType isEqualToString:@"title"]) {
            [self.sfApp.filmStrip setHidden:YES];
        }
        [self hideViewsToShow:self.editingType];
    } else {
        [self hideViewsToShow:@""];
        [self.sfApp.filmStrip setUpdateButtonPositionHigher:YES];
    }
    
    if(self.sfApp.editedTitle != nil) {
        
        self.txt_movieTitle.text = self.sfApp.editedTitle;
    }else if(self.sfApp.selectedTitle != nil) {
        
        self.txt_movieTitle.text = self.sfApp.selectedTitle;
    }
    [self.view bringSubviewToFront:self.but_MoreOptions];
    
    if(![StayfilmApp isArray:self.sfApp.finalMedias equalToArray:self.sfApp.editedMedias]) {
        [self.delegate mediaChanged];
    }
    

    NSArray *mediaArray ;
    
    if(self.sfApp.isEditingMode) {
        mediaArray = [self.sfApp.editedMedias copy];
    }else{
        mediaArray =  [self.sfApp.finalMedias copy];
    }
    if (mediaArray.count == 0){
        [self.noMediaView setHidden:NO];
        [self.removeButton setEnabled:NO];
        [self.reorderButton setEnabled:NO];
        [self.reorderLabel setTextColor:[UIColor colorWithRed:0.87 green:0.86 blue:0.87 alpha:1.0]];
        [self.removeLabel setTextColor:[UIColor colorWithRed:0.87 green:0.86 blue:0.87 alpha:1.0]];
        
    }else{
        
        [self.noMediaView setHidden:YES];
        [self.removeButton setEnabled:YES];
        [self.reorderButton setEnabled:YES];
        [self.reorderLabel setTextColor:[UIColor colorWithRed:0.20 green:0.34 blue:0.44 alpha:1.0]];
        [self.removeLabel setTextColor:[UIColor colorWithRed:0.20 green:0.34 blue:0.44 alpha:1.0]];
        
    }
    
    if(self.sfApp.isEditingMode) {
        [self hideViewsToShow:self.editingType];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.uploaderStayfilm = [Uploader sharedUploadStayfilmSingletonWithDelegate:self];
    
    [self.sfApp.filmStrip.but_NextStep removeTarget:nil action:nil forControlEvents:UIControlEventAllEvents];
    [self.sfApp.filmStrip.but_NextStep addTarget:self action:@selector(but_NextStep_Clicked) forControlEvents:UIControlEventTouchUpInside];
   // [self.sfApp.filmStrip setHidden:NO];
    if(![self.editingType isEqualToString:@"title"])
        [self.sfApp.filmStrip updateFilmStrip];
    
    if(self.sfApp.isEditingMode) {
        [self hideViewsToShow:self.editingType];
    }
    
    [self.sfApp.filmStrip connectivityViewUpdate];
    

//    // Disable swipe gesture to go back to previous viewcontroller
//    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
//        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
//    }

    CGFloat height = self.photosCollectionView.collectionViewLayout.collectionViewContentSize.height;
    NSArray *constraints = [self.photosCollectionView constraints];
    NSLayoutConstraint *constraint = nil;
    for (NSLayoutConstraint *object in constraints) {
        if ( [object.identifier isEqualToString:@"photosAlbumHeightConstraint"] ) {
            constraint = object;
            break;
        }
    }
    if(constraint != nil) {
        [constraint setConstant:height + 40];
    }
    
    if(self.sfApp.isEditingMode) {
        [self.photosCollectionView reloadData];
        
        if(self.editingType != nil && [self.editingType isEqualToString:@"title"]) {
            //Google Analytics
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker set:kGAIScreenName value:@"film-editing_title"];
            [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
            
        } else {
            //Google Analytics
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker set:kGAIScreenName value:@"film-editing_content"];
            [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
        }
    } else {
        //Google Analytics
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:@"moviemaker_content_film_strip"];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    }
    
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = [UIColor clearColor];
    }
    if (!self.sfApp.isEditingMode){
        [self.sfApp.filmStrip setViewOutsideFilmStrip];
        [self.sfApp.filmStrip setUpdateButtonPositionHigher:NO];
    }
//    else{
//        [self.sfApp.filmStrip setHidden:YES];
//    }
    
    [self.sfApp.filmStrip connectivityViewUpdate];
    
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    //[self.navigationController.navigationBar setTintColor:self.barColor];
     self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
     [self hideViewsToShow:@""];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

BOOL sentTitleChangeAnalytics = NO;
-(void)txt_movieTitle_Ended {
    
    self.sfApp.selectedTitle = self.txt_movieTitle.text;
    [self.view endEditing:YES];
    
    if(!sentTitleChangeAnalytics) {
        sentTitleChangeAnalytics = YES;
        
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"film-production"
                                                              action:@"add-title"
                                                               label:@""
                                                               value:@1] build]];
    }
}
-(void)txt_movieTitle_Changed {
    if(!self.sfApp.isEditingMode){
        if (self.txt_movieTitle.text.length > 80) {
            self.txt_movieTitle.text = [self.txt_movieTitle.text substringToIndex:80];
        }
        self.sfApp.selectedTitle = self.txt_movieTitle.text;
        
        if(!sentTitleChangeAnalytics) {
            sentTitleChangeAnalytics = YES;
            
            //Google Analytics Event
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"film-production"
                                                                  action:@"add-title"
                                                                   label:@""
                                                                   value:@1] build]];
        }
    }
}

-(void)dismissModalMenu
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)clearAlbumViewController
{
    self.removerVC = nil;
    self.reorderVC = nil;
    self.addVC = nil;
}


-(void)updateFilmStrip {
    [self.sfApp.filmStrip updateFilmStrip];
}

- (void)hideViewsToShow:(NSString*) view{
    
    if([view isEqualToString:@"title"]){
        
        if(self.sfApp.isEditingMode) {
            [self.photosCollectionView reloadData];
        }
        
        [self.photosCollectionView setHidden:YES];
        [self.selectedContentLabel setHidden:YES];
        [self.editPhotoTabBarView setHidden:YES];
        [self.txt_stripInfo setHidden:YES];
        [self.noMediaView setHidden:YES];
        [self.sfApp.filmStrip setHidden:YES];
        if(self.sfApp.editedTitle != nil) {
            self.txt_movieTitle.text = self.sfApp.editedTitle;
        }else if(self.sfApp.selectedTitle != nil) {
            self.txt_movieTitle.text = self.sfApp.selectedTitle;
        }else{
            self.txt_movieTitle.text = self.temporalTitle;
        }
        [self.txt_movieTitle becomeFirstResponder];
        
    }else if([view isEqualToString:@"editPhoto"]){
        
        [self.filmTitleLabel setHidden:YES];
        [self.txt_movieTitle setHidden:YES];
        [self.titleView setHidden:YES];
        [self.txt_stripInfo setHidden:YES];
        
        [self.collectionTopConstraint setConstant:30.0];
        
        
    }else{
        
        [self.filmTitleLabel setHidden:NO];
        [self.photosCollectionView setHidden:NO];
        [self.selectedContentLabel setHidden:NO];
        [self.editPhotoTabBarView setHidden:NO];
        [self.txt_movieTitle setHidden:NO];
        [self.titleView setHidden:NO];
        [self.txt_stripInfo setHidden:NO];
        
        [self.collectionTopConstraint setConstant:111.0];
    }
    
    
}

#pragma mark - Button Action delegates

- (IBAction)checkEditAction:(id)sender {
    
    if (self.sfApp.isEditingMode){
        
        if(self.editingType != nil && [self.editingType isEqualToString:@"title"]) {
            
            if (![self.sfApp.selectedTitle isEqualToString:self.txt_movieTitle.text]){
                
                //Google Analytics Event
                id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"edit-film"
                                                                      action:@"title"
                                                                       label:@"ok"
                                                                       value:@1] build]];
                
            }
        }
        
        self.sfApp.editedTitle = self.txt_movieTitle.text;
        
        [self.delegate titleChanged:self.txt_movieTitle.text];
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

- (IBAction)scrollView_Tapped:(id)sender {
    [self.view endEditing:YES];
}

- (IBAction)but_back_Clicked:(id)sender {
    [self backButtonPressed];
}
- (void)backButtonPressed
{
    [self.view endEditing:YES];
   
        if (self.sfApp.isEditingMode){
            
            
            if(self.editingType != nil && [self.editingType isEqualToString:@"title"]) {
                
                if (![self.sfApp.editedTitle isEqualToString:self.txt_movieTitle.text]){
                
                //Google Analytics Event
                id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"edit-film"
                                                                      action:@"title"
                                                                       label:@"cancel"
                                                                       value:@1] build]];
                
                }
            } else {
                //Google Analytics Event
                id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"edit-film"
                                                                      action:@"back"
                                                                       label:@""
                                                                       value:@1] build]];
            }
            
            
            [self.navigationController popViewControllerAnimated:YES];
        }else{
             @try {
                [self.sfApp.filmStrip changeTopStatusBarColorToWhite:NO];
                for (UIViewController *view in self.navigationController.viewControllers) {
                    if([view isKindOfClass:[AlbumViewerViewController class]])
                    {
                        [view performSelectorOnMainThread:@selector(changeSelectedMedias) withObject:nil waitUntilDone:YES];
                        break;
                    }
                }
                 //Google Analytics Event
                 id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                 [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"back"
                                                                       action:@" "
                                                                        label:@""
                                                                        value:@1] build]];
                 
                [self.navigationController popViewControllerAnimated:YES];
            }
            @catch (NSException *exception) {
                NSLog(@"StayLog: EXCEPTION on backButtonPressed with error: %@", exception.description);
            }
        }
}

-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if(!self.isLongPressing)
    {
        self.isLongPressing = YES;
        CGPoint p = [gestureRecognizer locationInView:self.photosCollectionView];
        
        NSIndexPath *indexPath = [self.photosCollectionView indexPathForItemAtPoint:p];
        if (indexPath == nil) {
            //            NSLog(@"long press on table view but not on a row");
        } else if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
            //            NSLog(@"long press on table view at row %d", indexPath.item);
            
            self.modalPreview = nil;
            UIImageView *imageView = nil;
            NSInteger i = indexPath.item;
            
            if((self.sfApp.isEditingMode)?
               (self.sfApp.editedMedias != nil && self.sfApp.editedMedias.count > 0) :
               (self.sfApp.finalMedias != nil && self.sfApp.finalMedias.count > 0))
            {
                if((self.sfApp.isEditingMode)?
                   ([[self.sfApp.editedMedias objectAtIndex:i] isKindOfClass:[PHAsset class]]) :
                   ([[self.sfApp.finalMedias objectAtIndex:i] isKindOfClass:[PHAsset class]]) )
                {
                    PHAsset * asset = nil;
                    if(self.sfApp.isEditingMode) {
                        asset = [self.sfApp.editedMedias objectAtIndex:i];
                    } else {
                        asset = [self.sfApp.finalMedias objectAtIndex:i];;
                    }
                    if(asset.mediaType == PHAssetMediaTypeVideo) {
                        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"Player" owner:self options:nil];
                        PlayerViewController *moviePlayerViewController = [topLevelObjects objectAtIndex:0];
                        
                        moviePlayerViewController.assetMovie = asset;
                        moviePlayerViewController.currentMovie = nil;
                        moviePlayerViewController.videoUrl = nil;

                        PHVideoRequestOptions *options = [[PHVideoRequestOptions alloc] init];
                        options.deliveryMode = PHVideoRequestOptionsDeliveryModeFastFormat;
                        [options setNetworkAccessAllowed:YES];
                        
                        
                        if(asset.mediaSubtypes ==  PHAssetMediaSubtypeVideoHighFrameRate ){
                            
                            [self.view makeToastActivity:CSToastPositionCenter];
                            
                            
                            [[PHImageManager defaultManager] requestAVAssetForVideo:asset options:options resultHandler:^(AVAsset * avasset, AVAudioMix * audioMix, NSDictionary * info) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                                    NSString *documentsDirectory = paths.firstObject;
                                    NSString *myPathDocs =  [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"video-%d.mov",arc4random() % 1000]];
                                    NSURL *url = [NSURL fileURLWithPath:myPathDocs];
                                    
                                    AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:avasset presetName:AVAssetExportPreset640x480];
                                    exporter.outputURL = url;
                                    exporter.outputFileType = AVFileTypeQuickTimeMovie;
                                    exporter.shouldOptimizeForNetworkUse = YES;
                                    
                                    [exporter exportAsynchronouslyWithCompletionHandler:^{
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            if (exporter.status == AVAssetExportSessionStatusCompleted) {
                                                NSURL *URL = exporter.outputURL;
                                                moviePlayerViewController.player = [AVPlayer playerWithURL:URL];
                                                
                                                [self.view hideToastActivity];
                                                [self presentViewController:moviePlayerViewController animated:YES completion:nil];
                                                self.isLongPressing = NO;
                                            }
                                        });
                                    }];
                                });
                            }];
                        }else{
                        
                            if(self.videoImageManager == nil)
                                self.videoImageManager = [[PHCachingImageManager alloc] init];
                            [self.videoImageManager requestAVAssetForVideo:asset options:nil resultHandler:^(AVAsset * _Nullable asset, AVAudioMix * _Nullable audioMix, NSDictionary * _Nullable info) {
                                moviePlayerViewController.player = [AVPlayer playerWithURL:((AVURLAsset *)asset).URL];
                                
                                [self presentViewController:moviePlayerViewController animated:YES completion:nil];
                                self.isLongPressing = NO;
                            }];
                        }
                       
                        return;
                    }else{
                        self.modalPreview = [[UIViewController alloc] init];
                        
                        self.modalPreview.view.backgroundColor = [UIColor whiteColor];
                        self.modalPreview.view.userInteractionEnabled = YES;
                        
                        imageView = [[UIImageView alloc] initWithFrame:self.modalPreview.view.frame];
                        imageView.contentMode = UIViewContentModeScaleAspectFit;
                        
                        if(self.videoImageManager == nil)
                            self.videoImageManager = [[PHCachingImageManager alloc] init];
                        [self.videoImageManager requestImageForAsset:asset targetSize:imageView.frame.size contentMode:PHImageContentModeAspectFill options:nil resultHandler:^(UIImage *result, NSDictionary *info)
                         {
                             imageView.image = result;
                         }];
                    }
                }else if((self.sfApp.isEditingMode)?
                        ([[self.sfApp.editedMedias objectAtIndex:i] isKindOfClass:[Media class]]) :
                        ([[self.sfApp.finalMedias objectAtIndex:i] isKindOfClass:[Media class]]) )
                {
                    self.modalPreview = [[UIViewController alloc] init];
                    
                    self.modalPreview.view.backgroundColor = [UIColor whiteColor];
                    self.modalPreview.view.userInteractionEnabled = YES;
                    
                    imageView = [[UIImageView alloc] initWithFrame:self.modalPreview.view.frame];
                    imageView.contentMode = UIViewContentModeScaleAspectFit;
                    
                    Media * asset = nil;
                    if(self.sfApp.isEditingMode) {
                        asset = [self.sfApp.editedMedias objectAtIndex:i];
                    } else {
                        asset = [self.sfApp.finalMedias objectAtIndex:i];;
                    }
                    
                    if([asset.source containsString:@".mp4"] ||
                       [asset.source containsString:@".mpg"] ||
                       [asset.source containsString:@".avi"] ||
                       [asset.source containsString:@".mov"] )
                    {
                        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"Player" owner:self options:nil];
                        PlayerViewController *moviePlayerViewController = [topLevelObjects objectAtIndex:0];
                        
                        moviePlayerViewController.currentMovie = nil;
                        moviePlayerViewController.assetMovie = nil;
                        moviePlayerViewController.videoUrl = asset.source;
                        
                        [self presentViewController:moviePlayerViewController animated:YES completion:nil];
                        //[self performSegueWithIdentifier: @"PlaySelectedMovieSegue" sender:asset.source];
                        self.isLongPressing = NO;
                        return;
                    }
                    else {
                        if(self.sfApp.imagesCache != nil && [self.sfApp.imagesCache objectForKey:asset.source] != nil)
                        {
                            imageView.image = [self.sfApp.imagesCache objectForKey:asset.source];
                        }
                        else
                        {
                            NSURLSessionDataTask *task = [self.thumbConnection dataTaskWithURL:[NSURL URLWithString:asset.source] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                if(error)
                                {
                                    NSLog(@"StayLog: PhotoCollectionImage request error: %@", error);
                                }
                                else {
                                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                        imageView.image = [UIImage imageWithData:data];
                                        if(self.sfApp.imagesCache != nil && imageView.image != nil)
                                            [self.sfApp.imagesCache setObject:imageView.image forKey:asset.source];
                                    }];
                                }
                            }];
                            [task resume];
                        }
                    }
                }
                else if((self.sfApp.isEditingMode)?
                        ([[self.sfApp.editedMedias objectAtIndex:i] isKindOfClass:[PhotoFB class]]) :
                        ([[self.sfApp.finalMedias objectAtIndex:i] isKindOfClass:[PhotoFB class]]) )
                {
                    self.modalPreview = [[UIViewController alloc] init];
                    
                    self.modalPreview.view.backgroundColor = [UIColor whiteColor];
                    self.modalPreview.view.userInteractionEnabled = YES;
                    
                    imageView = [[UIImageView alloc] initWithFrame:self.modalPreview.view.frame];
                    imageView.contentMode = UIViewContentModeScaleAspectFit;
                    
                    PhotoFB * asset = nil;
                    if(self.sfApp.isEditingMode) {
                        asset = [self.sfApp.editedMedias objectAtIndex:i];
                    } else {
                        asset = [self.sfApp.finalMedias objectAtIndex:i];;
                    }
                    
                    if([asset.source containsString:@".mp4"] ||
                       [asset.source containsString:@".mpg"] ||
                       [asset.source containsString:@".avi"] ||
                       [asset.source containsString:@".mov"] )
                    {
                        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"Player" owner:self options:nil];
                        PlayerViewController *moviePlayerViewController = [topLevelObjects objectAtIndex:0];
                        
                        moviePlayerViewController.videoUrl = asset.source;
                        moviePlayerViewController.currentMovie = nil;
                        moviePlayerViewController.assetMovie = nil;
                        
                        [self presentViewController:moviePlayerViewController animated:YES completion:nil];
                        //[self performSegueWithIdentifier: @"PlaySelectedMovieSegue" sender:asset.source];
                        self.isLongPressing = NO;
                        return;
                    }
                    else {
                        if([self.sfApp.imagesCache objectForKey:asset.source] != nil)
                        {
                            imageView.image = [self.sfApp.imagesCache objectForKey:asset.source];
                        }
                        else
                        {
                            NSURLSessionDataTask *task = [self.thumbConnection dataTaskWithURL:[NSURL URLWithString:asset.source] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                if(error)
                                {
                                    NSLog(@"StayLog: PhotoCollectionImage request error: %@", error);
                                }
                                else {
                                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                        imageView.image = [UIImage imageWithData:data];
                                        if(imageView.image != nil)
                                            [self.sfApp.imagesCache setObject:imageView.image forKey:asset.source];
                                    }];
                                }
                            }];
                            [task resume];
                        }
                    }
                }
                else if((self.sfApp.isEditingMode)?
                        ([[self.sfApp.editedMedias objectAtIndex:i] isKindOfClass:[VideoFB class]]) :
                        ([[self.sfApp.finalMedias objectAtIndex:i] isKindOfClass:[VideoFB class]]) )
                {
                    self.modalPreview = [[UIViewController alloc] init];
                    
                    self.modalPreview.view.backgroundColor = [UIColor whiteColor];
                    self.modalPreview.view.userInteractionEnabled = YES;
                    
                    imageView = [[UIImageView alloc] initWithFrame:self.modalPreview.view.frame];
                    imageView.contentMode = UIViewContentModeScaleAspectFit;
                    
                    VideoFB * asset = nil;
                    if(self.sfApp.isEditingMode) {
                        asset = [self.sfApp.editedMedias objectAtIndex:i];
                    } else {
                        asset = [self.sfApp.finalMedias objectAtIndex:i];;
                    }
                    
                    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"Player" owner:self options:nil];
                    PlayerViewController *moviePlayerViewController = [topLevelObjects objectAtIndex:0];
                    
                    moviePlayerViewController.currentMovie = nil;
                    moviePlayerViewController.assetMovie = nil;
                    moviePlayerViewController.videoUrl = asset.source;
                    
                    [self presentViewController:moviePlayerViewController animated:YES completion:nil];
                    //[self performSegueWithIdentifier: @"PlaySelectedMovieSegue" sender:asset.source];
                    self.isLongPressing = NO;
                    return;
                }
                
                if(imageView != nil) {
                    [self.modalPreview.view addSubview:imageView];
                    
                    UITapGestureRecognizer *modalTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissModalView)];
                    [self.modalPreview.view addGestureRecognizer:modalTap];
                    
                    self.modalPreview.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                    self.modalPreview.modalPresentationStyle = UIModalPresentationFullScreen;
                    
                    [self presentViewController:self.modalPreview animated:YES completion:nil];
                }
            }
        }
        self.isLongPressing = NO;
    }
}

BOOL isMoving = NO;
- (void)but_NextStep_Clicked {
    if(!isMoving) {
        isMoving = YES;
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"film-production"
                                                              action:@"next"
                                                               label:@"film-strip"
                                                               value:@1] build]];
        if(self.sfApp.selectedGenre != nil && self.sfApp.selectedTemplate != nil && self.sfApp.canSkipStyles){
            [self performSegueWithIdentifier:@"confirmationToProgress" sender:self];
        }else{
            [self performSegueWithIdentifier:@"ChooseStyleSegue" sender:self];
        }
        
        isMoving = NO;
    }
}

BOOL isOpeningMore = NO;
- (IBAction)but_moreOptions_Clicked:(id)sender {
    if(!isOpeningMore) {
        isOpeningMore = YES;
        Popover_ConfirmationMoreOptions_ViewController * view = [[Popover_ConfirmationMoreOptions_ViewController alloc] initWithDelegate:self];
        view.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        view.transitioningDelegate = self;
        view.view.frame = self.view.frame;
        self.transitionAnimation.duration = 0.3;
        self.transitionAnimation.isPresenting = YES;
        self.transitionAnimation.originFrame = self.view.frame;
        
        UITapGestureRecognizer *modalTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissModalMenu)];
        modalTap.numberOfTapsRequired = 1;
        [view.view addGestureRecognizer:modalTap];
        
        [self presentViewController:view animated:YES completion:nil];
        isOpeningMore = NO;
    }
}

-(void)changeEditedMedias:(NSMutableArray *)p_edited
{
    // only called in removerVC or reorderVC
    if(self.sfApp == nil) {
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    }
    self.sfApp.editedMedias = nil;
    self.sfApp.editedMedias = [p_edited mutableCopy];
    
    [self updateFilmStrip];
    
    [self.photosCollectionView reloadData];
    
    if(![StayfilmApp isArray:self.sfApp.finalMedias equalToArray:self.sfApp.editedMedias]) {
        [self.delegate mediaChanged];
    }
}

-(void)changeSelectedMedias:(NSMutableArray *)p_selected
{
    // only called in removerVC or reorderVC
    if(self.sfApp == nil) {
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    }
    self.sfApp.finalMedias = nil;
    self.sfApp.finalMedias = [p_selected mutableCopy];
    
    [self updateFilmStrip];
    
    [self.photosCollectionView reloadData];
}


#pragma mark - Actions

- (IBAction)addAction:(id)sender {
    
    if(self.sfApp.isEditingMode) {
        self.addVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MovieMakerStep1"];
        
        self.addVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        self.addVC.modalPresentationStyle = UIModalPresentationFullScreen;
        self.addVC.initialEditedMedias = self.sfApp.editedMedias;
        //self.addVC.flowNavigationController = self.navigationController;
        //[self presentViewController:self.addVC animated:YES completion:nil];
        
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"edit-film"
                                                              action:@"add-content"
                                                               label:@""
                                                               value:@1] build]];
        
        [self.navigationController showViewController:self.addVC sender:self];
    } else {
        for (UIViewController *view in self.navigationController.viewControllers) {
            if([view isKindOfClass:[AlbumViewerViewController class]])
            {
                [view performSelectorOnMainThread:@selector(changeSelectedMedias) withObject:nil waitUntilDone:YES];
                break;
            }
        }
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"film-production"
                                                              action:@"add-media"
                                                               label:@"film-strip"
                                                               value:@1] build]];
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

- (IBAction)reorderAction:(id)sender {
    
    self.reorderVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ReorderViewer"];
    if(self.sfApp.isEditingMode) {
        self.reorderVC.initialSelectedMedias = [self.sfApp.editedMedias copy];
        
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"edit-film"
                                                              action:@"reorder"
                                                               label:@""
                                                               value:@1] build]];
    } else {
        self.reorderVC.initialSelectedMedias = [self.sfApp.finalMedias copy];
        
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"film-production"
                                                              action:@"reorder"
                                                               label:@""
                                                               value:@1] build]];
    }
    
    
    if(self.videoImageManager == nil) {
        self.reorderVC.videoImageManager = self.videoImageManager;
//        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
//            switch (status) {
//                case PHAuthorizationStatusAuthorized:
//                {
//                    self.videoImageManager = [[PHCachingImageManager alloc] init];
//                    self.reorderVC.videoImageManager = self.videoImageManager;
//                }
//                    break;
//                case PHAuthorizationStatusDenied:
//                case PHAuthorizationStatusRestricted:
//                case PHAuthorizationStatusNotDetermined:
//                {
//                    self.reorderVC.videoImageManager = nil;
//                }
//                    break;
//                default:
//                    break;
//            }
//        }];
    } else {
        self.reorderVC.videoImageManager = nil;
    }
    
    self.reorderVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    //self.reorderVC.modalPresentationStyle = UIModalPresentationCurrentContext;
    self.reorderVC.modalPresentationStyle = UIModalPresentationFullScreen;
    self.reorderVC.navigationController = self.navigationController;
    [self presentViewController:self.reorderVC animated:YES completion:nil];
    //[self.navigationController showViewController:self.reorderVC sender:self];
    
}

- (IBAction)removeAction:(id)sender {
    
    self.removerVC = [self.storyboard instantiateViewControllerWithIdentifier:@"RemoverViewer"];
    if(self.sfApp.isEditingMode) {
        self.removerVC.initialSelectedMedias = [self.sfApp.editedMedias copy];
        
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"edit-film"
                                                              action:@"removed"
                                                               label:@""
                                                               value:@1] build]];
    } else {
        self.removerVC.initialSelectedMedias = [self.sfApp.finalMedias copy];
        
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"film-production"
                                                              action:@"remove-media"
                                                               label:@""
                                                               value:@1] build]];
    }
    
    if(self.videoImageManager == nil) {
        self.removerVC.imageManager = self.videoImageManager;
//        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
//            switch (status) {
//                case PHAuthorizationStatusAuthorized:
//                {
//                    self.videoImageManager = [[PHCachingImageManager alloc] init];
//                    self.removerVC.imageManager = self.videoImageManager;
//                }
//                    break;
//                case PHAuthorizationStatusDenied:
//                case PHAuthorizationStatusRestricted:
//                case PHAuthorizationStatusNotDetermined:
//                {
//                    self.removerVC.imageManager = nil;
//                }
//                    break;
//                default:
//                    break;
//            }
//        }];
    }else {
        self.removerVC.imageManager = nil;
    }
    
    self.removerVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    //self.removerVC.modalPresentationStyle = UIModalPresentationCurrentContext;
    self.removerVC.modalPresentationStyle = UIModalPresentationFullScreen;
    self.removerVC.navigationController = self.navigationController;
    [self presentViewController:self.removerVC animated:YES completion:nil];
    //[self.navigationController pushViewController:self.removerVC animated:YES];
    // [self.navigationController showViewController:self.removerVC sender:self];
    
}




//#pragma mark - Popover Delegates
//
//-(void)decidedOption:(NSString *)p_option {
//    if([p_option isEqualToString:@"remove"]) {
//        self.removerVC = [self.storyboard instantiateViewControllerWithIdentifier:@"RemoverViewer"];
//        self.removerVC.initialSelectedMedias = [self.sfApp.finalMedias copy];
//
//        if(self.videoImageManager == nil) {
//            [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
//                switch (status) {
//                    case PHAuthorizationStatusAuthorized:
//                    {
//                        self.videoImageManager = [[PHCachingImageManager alloc] init];
//                        self.removerVC.imageManager = self.videoImageManager;
//                    }
//                        break;
//                    case PHAuthorizationStatusDenied:
//                    case PHAuthorizationStatusRestricted:
//                    case PHAuthorizationStatusNotDetermined:
//                    {
//                        self.removerVC.imageManager = nil;
//                    }
//                        break;
//                    default:
//                        break;
//                }
//            }];
//        }
//        
//        self.removerVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//        self.removerVC.modalPresentationStyle = UIModalPresentationFullScreen;
//        self.removerVC.navigationController = self.navigationController;
//        [self presentViewController:self.removerVC animated:YES completion:nil];
//        //[self.navigationController pushViewController:self.removerVC animated:YES];
//       // [self.navigationController showViewController:self.removerVC sender:self];
//    }
//    else if ([p_option isEqualToString:@"reorder"]) {
//        self.reorderVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ReorderViewer"];
//        self.reorderVC.initialSelectedMedias = [self.sfApp.finalMedias copy];
//
//        if(self.videoImageManager == nil) {
//            [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
//                switch (status) {
//                    case PHAuthorizationStatusAuthorized:
//                    {
//                        self.videoImageManager = [[PHCachingImageManager alloc] init];
//                        self.reorderVC.videoImageManager = self.videoImageManager;
//                    }
//                        break;
//                    case PHAuthorizationStatusDenied:
//                    case PHAuthorizationStatusRestricted:
//                    case PHAuthorizationStatusNotDetermined:
//                    {
//                        self.reorderVC.videoImageManager = nil;
//                    }
//                        break;
//                    default:
//                        break;
//                }
//            }];
//        }
//        
//        self.reorderVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//        self.reorderVC.modalPresentationStyle = UIModalPresentationFullScreen;
//        self.reorderVC.navigationController = self.navigationController;
//         [self presentViewController:self.reorderVC animated:YES completion:nil];
//        //[self.navigationController showViewController:self.reorderVC sender:self];
//    }
//    else if ([p_option isEqualToString:@"addMore"]) {
//        if(self.sfApp.isEditingMode) {
//            self.addVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MovieMakerStep1"];
//            
//            self.addVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//            self.addVC.modalPresentationStyle = UIModalPresentationFullScreen;
//            //self.addVC.flowNavigationController = self.navigationController;
//            [self presentViewController:self.addVC animated:YES completion:nil];
//        } else {
//            [self dismissViewControllerAnimated:YES completion:nil];
//            [self.navigationController popViewControllerAnimated:YES];
//        }
//    }
//}

-(void)dismissModalView
{
    [self dismissViewControllerAnimated:YES completion:nil];
    self.modalPreview = nil;
}


#pragma mark - Animation Delegates

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source {
    return self.transitionAnimation;
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    self.transitionAnimation.isPresenting = NO;
    return self.transitionAnimation;
}


#pragma mark - Collection Delegates

//-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
//    [self.view endEditing:YES];
//}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    [(UIActivityIndicatorView *)[self.view viewWithTag:10] stopAnimating];
    
    if(self.sfApp.isEditingMode) {
        if(self.sfApp.editedMedias != nil)
            return [self.sfApp.editedMedias count];
        return 0;
    } else {
        if(self.sfApp.finalMedias != nil)
            return [self.sfApp.finalMedias count];
        return 0;
    }
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        PhotoCollectionViewCell *cell = (PhotoCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifierPhoto forIndexPath:indexPath];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PhotoItem" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        if(cell.task)
        {
            [cell.task cancel];
        }
        
        cell.photoImageView.image = nil;
        [cell.spinner startAnimating];
        [cell.spinner setHidden:NO];
        [cell.selectedView setHidden:YES];
        cell.photoImageView.alpha = 1.0;
        cell.source = nil;
        
        if(self.sfApp == nil)
        {
            self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
        }
        if((self.sfApp.isEditingMode)?
           (self.sfApp.editedMedias != nil && self.sfApp.editedMedias.count > 0) :
           (self.sfApp.finalMedias != nil && self.sfApp.finalMedias.count > 0) )
        {
            NSInteger i = indexPath.item;
            
            if((self.sfApp.isEditingMode)?
               ([[self.sfApp.editedMedias objectAtIndex:i] isKindOfClass:[PHAsset class]]) :
               ([[self.sfApp.finalMedias objectAtIndex:i] isKindOfClass:[PHAsset class]]) )
            {
                cell.source = nil;
                PHAsset * asset = nil;
                if(self.sfApp.isEditingMode){
                    asset = [self.sfApp.editedMedias objectAtIndex:i];
                } else {
                    asset = [self.sfApp.finalMedias objectAtIndex:i];
                }
                if(self.uploaderStayfilm == nil)
                    self.uploaderStayfilm = [Uploader sharedUploadStayfilmSingletonWithDelegate:self];
                
                if([self.uploaderStayfilm isAssetOnUploadList:asset]) {
                    //runOnMainQueueWithoutDeadlocking(^{
                        [cell.spinner startAnimating];
                        [cell.spinner setHidden:NO];
                        cell.photoImageView.alpha = 0.75;
                    //});
                }
                else {
                    //runOnMainQueueWithoutDeadlocking(^{
                        [cell.spinner stopAnimating];
                        [cell.spinner setHidden:YES];
                        cell.photoImageView.alpha = 1.0;
                    //});
                }
                
                if(asset.mediaType == PHAssetMediaTypeVideo) {  // video
                    [cell.vid_Play setHidden:NO];
                    [cell.vid_Time setHidden:NO];
                    [cell.vid_Gradient setHidden:NO];
                    
                    long seconds = lroundf(asset.duration);
                    int hour = (int)seconds / 3600;
                    int mins = (seconds % 3600) / 60;
                    int secs = seconds % 60;
                    if(hour > 0) {
                        cell.vid_Time.text = [NSString stringWithFormat:@"%d:%02d:%02d",hour,mins,secs];
                    } else {
                        cell.vid_Time.text = [NSString stringWithFormat:@"%d:%02d",mins,secs];
                    }
                } else { // photo
                    [cell.vid_Play setHidden:YES];
                    [cell.vid_Gradient setHidden:YES];
                    [cell.vid_Time setHidden:YES];
                }
                if(self.videoImageManager == nil)
                    self.videoImageManager = [[PHCachingImageManager alloc] init];
                [self.videoImageManager requestImageForAsset:asset targetSize:cell.photoImageView.frame.size contentMode:PHImageContentModeAspectFill options:nil resultHandler:^(UIImage *result, NSDictionary *info)
                 {
                     if(result == nil) {
                         
                         cell.task = [self.thumbConnection dataTaskWithURL:[NSURL URLWithString:cell.source] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                             if(error)
                             {
                                 NSLog(@"StayLog: PhotoCollectionImage request error: %@", error);
                             }
                             else {
                                 [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                     cell.photoImageView.image = [UIImage imageWithData:data];
                                     if(cell.photoImageView.image != nil)
                                         [self.sfApp.imagesCache setObject:cell.photoImageView.image forKey:cell.source];
                                 }];
                             }
                         }];
                         [cell.task resume];
                     }
                     else {
                         runOnMainQueueWithoutDeadlocking(^{
                             cell.photoImageView.image = result;
                         });
                     }
                 }];
                if(cell.photoImageView.image == nil) {
                    [[PHImageManager defaultManager] requestAVAssetForVideo:asset options:nil resultHandler:^(AVAsset *avAsset, AVAudioMix *audioMix, NSDictionary *info) {
                        runOnMainQueueWithoutDeadlocking(^{
                            AVAssetImageGenerator *generator = [AVAssetImageGenerator assetImageGeneratorWithAsset:avAsset];
                            generator.appliesPreferredTrackTransform = YES;
                            CMTime time = CMTimeMakeWithSeconds(1, 2);
                            NSError *error = nil;
                            @try {
                                CGImageRef img = [generator copyCGImageAtTime:time actualTime:nil error:&error];
                                if(!error)
                                {
                                    UIImage *image = [UIImage imageWithCGImage:img];
                                    cell.photoImageView.image = image;
                                    if(cell.photoImageView.image != nil)
                                        [self.sfApp.imagesCache setObject:cell.photoImageView.image forKey:cell.source];
                                }
                                [cell.vid_Play setHidden:NO];
                                [cell.vid_Gradient setHidden:NO];
                            } @catch (NSException *ex) {
                                NSLog(@"StayLog: EXCEPTION getting video thumb with error: %@", ex.description);
                            }
                        });
                    }];
                }
                return cell;
            }
            else if((self.sfApp.isEditingMode)?
               ([[self.sfApp.editedMedias objectAtIndex:i] isKindOfClass:[Media class]]) :
               ([[self.sfApp.finalMedias objectAtIndex:i] isKindOfClass:[Media class]]) )
            {
                Media * media = nil;
                if(self.sfApp.isEditingMode) {
                    media = [self.sfApp.editedMedias objectAtIndex:i];
                } else {
                    media = [self.sfApp.finalMedias objectAtIndex:i];
                }
                cell.source = media.source;
                cell.duration = nil;
            }
            else if((self.sfApp.isEditingMode)?
                    ([[self.sfApp.editedMedias objectAtIndex:i] isKindOfClass:[PhotoFB class]]) :
                    ([[self.sfApp.finalMedias objectAtIndex:i] isKindOfClass:[PhotoFB class]]) )
            {
                PhotoFB * passet = nil;
                if(self.sfApp.isEditingMode) {
                    passet = [self.sfApp.editedMedias objectAtIndex:i];
                } else {
                    passet = [self.sfApp.finalMedias objectAtIndex:i];
                }
                cell.source = passet.source;
                cell.duration = nil;
            }
            else if((self.sfApp.isEditingMode)?
                    ([[self.sfApp.editedMedias objectAtIndex:i] isKindOfClass:[VideoFB class]]) :
                    ([[self.sfApp.finalMedias objectAtIndex:i] isKindOfClass:[VideoFB class]]) )
            {
                VideoFB * passet = nil;
                if(self.sfApp.isEditingMode) {
                    passet = [self.sfApp.editedMedias objectAtIndex:i];
                } else {
                    passet = [self.sfApp.finalMedias objectAtIndex:i];
                }
                cell.source = passet.source;
                cell.duration = passet.length;
            }
            if(cell.source != nil) {
                
                cell.photoImageView.alpha = 1.0;
                if([self.sfApp.imagesCache objectForKey:cell.source] != nil)
                {
                    runOnMainQueueWithoutDeadlocking(^{
                        cell.photoImageView.image = [self.sfApp.imagesCache objectForKey:cell.source];
                        [cell.spinner stopAnimating];
                        [cell.spinner setHidden:YES];
                        if([cell.source containsString:@".mp4"] ||
                           [cell.source containsString:@".avi"] ||
                           [cell.source containsString:@".mpg"] ||
                           [cell.source containsString:@".mov"] )
                        {
                            [cell.vid_Play setHidden:NO];
                            [cell.vid_Gradient setHidden:NO];
                            
                            if((self.sfApp.isEditingMode)?
                               ([[self.sfApp.editedMedias objectAtIndex:i] isKindOfClass:[VideoFB class]]) :
                               ([[self.sfApp.finalMedias objectAtIndex:i] isKindOfClass:[VideoFB class]]) )
                            {
                                [cell.vid_Time setHidden:NO];
                                if(self.sfApp.isEditingMode) {
                                    cell.duration = ((VideoFB*)[self.sfApp.editedMedias objectAtIndex:i]).length;
                                } else {
                                    cell.duration = ((VideoFB*)[self.sfApp.finalMedias objectAtIndex:i]).length;
                                }
                                long seconds = lroundf([cell.duration floatValue]);
                                int hour = (int)seconds / 3600;
                                int mins = (seconds % 3600) / 60;
                                int secs = seconds % 60;
                                if(hour > 0) {
                                    cell.vid_Time.text = [NSString stringWithFormat:@"%d:%02d:%02d",hour,mins,secs];
                                } else {
                                    cell.vid_Time.text = [NSString stringWithFormat:@"%d:%02d",mins,secs];
                                }
                            }
                        }
                        else {
                            [cell.vid_Play setHidden:YES];
                            [cell.vid_Gradient setHidden:YES];
                            [cell.vid_Time setHidden:YES];
                        }
                    });
                }
                else
                {
                    __weak typeof(self) selfDelegate = self;
                    if([cell.source containsString:@".mp4"] ||
                       [cell.source containsString:@".avi"] ||
                       [cell.source containsString:@".mpg"] ||
                       [cell.source containsString:@".mov"] )
                    {
                        AVAsset *asset = [AVAsset assetWithURL:[NSURL URLWithString:cell.source]];
                        AVAssetImageGenerator *generator = [AVAssetImageGenerator assetImageGeneratorWithAsset:asset];
                        generator.appliesPreferredTrackTransform = YES;
                        CMTime time = CMTimeMakeWithSeconds(1, 2);
                        NSError *error = nil;
                        @try {
                            CGImageRef img = [generator copyCGImageAtTime:time actualTime:nil error:&error];
                            if(!error)
                            {
                                UIImage *image = [UIImage imageWithCGImage:img];
                                cell.photoImageView.image = image;
                                if(cell.photoImageView.image != nil)
                                    [selfDelegate.sfApp.imagesCache setObject:cell.photoImageView.image forKey:cell.source];
                            }
                            runOnMainQueueWithoutDeadlocking(^{
                                [cell.vid_Play setHidden:NO];
                                [cell.vid_Gradient setHidden:NO];
                                if((self.sfApp.isEditingMode)?
                                   ([[self.sfApp.editedMedias objectAtIndex:i] isKindOfClass:[VideoFB class]]) :
                                   ([[self.sfApp.finalMedias objectAtIndex:i] isKindOfClass:[VideoFB class]]) )
                                {
                                    [cell.vid_Time setHidden:NO];
                                    if(self.sfApp.isEditingMode) {
                                        cell.duration = ((VideoFB*)[self.sfApp.editedMedias objectAtIndex:i]).length;
                                    } else {
                                        cell.duration = ((VideoFB*)[self.sfApp.finalMedias objectAtIndex:i]).length;
                                    }
                                    long seconds = lroundf([cell.duration floatValue]);
                                    int hour = (int)seconds / 3600;
                                    int mins = (seconds % 3600) / 60;
                                    int secs = seconds % 60;
                                    if(hour > 0) {
                                        cell.vid_Time.text = [NSString stringWithFormat:@"%d:%02d:%02d",hour,mins,secs];
                                    } else {
                                        cell.vid_Time.text = [NSString stringWithFormat:@"%d:%02d",mins,secs];
                                    }
                                }
                                [cell.spinner stopAnimating];
                                [cell.spinner setHidden:YES];
                            });
                        } @catch (NSException *ex) {
                            NSLog(@"StayLog: EXCEPTION getting video thumb with error: %@", ex.description);
                        }
                    }
                    else { // image
                        cell.task = [self.thumbConnection dataTaskWithURL:[NSURL URLWithString:cell.source] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                            if(error)
                            {
                                NSLog(@"StayLog: PhotoCollectionImage request error: %@", error);
                            }
                            else {
                                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                    cell.photoImageView.image = [UIImage imageWithData:data];
                                    if(cell.photoImageView.image != nil)
                                        [selfDelegate.sfApp.imagesCache setObject:cell.photoImageView.image forKey:cell.source];
                                }];
                            }
                            runOnMainQueueWithoutDeadlocking(^{
                                [cell.vid_Play setHidden:YES];
                                [cell.vid_Gradient setHidden:YES];
                                [cell.vid_Time setHidden:YES];
                                [cell.spinner stopAnimating];
                                [cell.spinner setHidden:YES];
                            });
                        }];
                        [cell.task resume];
                    }
                }
            }
        }
        return cell;
    }
    @catch (NSException *ex) {
        NSLog(@"StayLog: EXCEPTION in collectionview cellForItemAtIndexPath in Step1Confirm with error: %@", ex.description);
    }
    return nil;
}



- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeMake(0, 0);
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    //Set cell size to fit all screens
    
    CGFloat width  = self.view.frame.size.width;
    width -= 6.0;
    width /= 3.0;
    return CGSizeMake(width,width);
}


#pragma mark - Uploader Delegates

-(void)uploadProgressChanged:(NSNumber *)progress
{
    // Not important on this page.
}

- (void)converterDidFailMedia:(NSString *)type
{
    NSString *message = nil;
    if(self.sfApp == nil)
    {
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    }
    
    if ( (self.sfApp.isEditingMode)? (self.sfApp.editedMedias.count < [self.sfApp.sfConfig.min_photos intValue]) : (self.sfApp.finalMedias.count < [self.sfApp.sfConfig.min_photos intValue]) ) {
        message = [NSString stringWithFormat:NSLocalizedString(@"UPLOADER_CONVERT_FAILED_UPLOAD_AGAIN_MESSAGE", nil), ((self.sfApp.isEditingMode)? ([self.sfApp.sfConfig.min_photos intValue] - self.sfApp.editedMedias.count) : ([self.sfApp.sfConfig.min_photos intValue] - self.sfApp.finalMedias.count))];
    }
    else if ([type isEqualToString:@"image"]) {
        message = NSLocalizedString(@"UPLOADER_CONVERT_FAILED_IMAGE_MESSAGE", nil);
    }
    else
    {
        message = NSLocalizedString(@"UPLOADER_CONVERT_FAILED_VIDEO_MESSAGE", nil);
    }
    
    if(self.sfApp.isEditingMode) {
        @synchronized (self.sfApp.editedMedias) {
            for (id obj in self.sfApp.editedMedias) {
                if([obj isKindOfClass:[PHAsset class]]) {
                    BOOL find = NO;
                    for (PHAsset *asset in self.uploaderStayfilm.listUploadMedias) {
                        if(asset == obj) {
                            find = YES;
                            break;
                        }
                    }
                    if(!find) {
                        NSUInteger i = [self.sfApp.editedMedias indexOfObject:obj];
                        [self.sfApp.editedMedias removeObjectAtIndex:i];
                        [self.photosCollectionView reloadData];
                        break;
                    }
                }
            }
        }
    } else {
        @synchronized (self.sfApp.finalMedias) {
            for (id obj in self.sfApp.finalMedias) {
                if([obj isKindOfClass:[PHAsset class]]) {
                    BOOL find = NO;
                    for (PHAsset *asset in self.uploaderStayfilm.listUploadMedias) {
                        if(asset == obj) {
                            find = YES;
                            break;
                        }
                    }
                    if(!find) {
                        NSUInteger i = [self.sfApp.finalMedias indexOfObject:obj];
                        [self.sfApp.finalMedias removeObjectAtIndex:i];
                        [self.photosCollectionView reloadData];
                        break;
                    }
                }
            }
        }
    }
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_CONVERT_FAILED_TITLE", nil)
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)uploadDidFailMedia:(NSString *)type
{
    NSString *message = nil;
    if(self.sfApp == nil)
    {
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    }
    
    if ( (self.sfApp.isEditingMode)? (self.sfApp.editedMedias.count < [self.sfApp.sfConfig.min_photos intValue]) : (self.sfApp.finalMedias.count < [self.sfApp.sfConfig.min_photos intValue]) ) {
        message = [NSString stringWithFormat:NSLocalizedString(@"UPLOADER_UPLOAD_FAILED_UPLOAD_AGAIN_MESSAGE", nil), ((self.sfApp.isEditingMode)? ([self.sfApp.sfConfig.min_photos intValue] - self.sfApp.editedMedias.count) : ([self.sfApp.sfConfig.min_photos intValue] - self.sfApp.finalMedias.count))];
    }
    else if ([type isEqualToString:@"image"]) {
        message = NSLocalizedString(@"UPLOADER_UPLOAD_FAILED_IMAGE_MESSAGE", nil);
    }
    else
    {
        message = NSLocalizedString(@"UPLOADER_UPLOAD_FAILED_VIDEO_MESSAGE", nil);
    }
    
    if(self.sfApp.isEditingMode) {
        @synchronized (self.sfApp.editedMedias) {
            for (id obj in self.sfApp.editedMedias) {
                if([obj isKindOfClass:[PHAsset class]]) {
                    BOOL find = NO;
                    for (PHAsset *asset in self.uploaderStayfilm.listUploadMedias) {
                        if(asset == obj) {
                            find = YES;
                            break;
                        }
                    }
                    if(!find) {
                        NSUInteger i = [self.sfApp.editedMedias indexOfObject:obj];
                        [self.sfApp.editedMedias removeObjectAtIndex:i];
                        [self.photosCollectionView reloadData];
                        break;
                    }
                }
            }
        }
    } else {
        @synchronized (self.sfApp.finalMedias) {
            for (id obj in self.sfApp.finalMedias) {
                if([obj isKindOfClass:[PHAsset class]]) {
                    BOOL find = NO;
                    for (PHAsset *asset in self.uploaderStayfilm.listUploadMedias) {
                        if(asset == obj) {
                            find = YES;
                            break;
                        }
                    }
                    if(!find) {
                        NSUInteger i = [self.sfApp.finalMedias indexOfObject:obj];
                        [self.sfApp.finalMedias removeObjectAtIndex:i];
                        [self.photosCollectionView reloadData];
                        break;
                    }
                }
            }
        }
    }
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_UPLOAD_FAILED_TITLE", nil)
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)uploadDidFail:(Uploader *)uploader
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_ERROR_TITLE", nil)
                                                                   message:NSLocalizedString(@"UPLOADER_ERROR_MESSAGE", nil)
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)uploadDidStartSendingConfirmationMedias:(Uploader *)uploader
{
    // not important on this page.
}

//-(void)uploadDidStart
//{
//    // does not happen in this page
//}

-(void)uploadDidFinish:(Uploader *)uploader
{
    if(self.sfApp == nil) {
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    }
    if(self.uploaderStayfilm.listUploadedMedias != nil && self.uploaderStayfilm.listUploadedMedias.count >0) {
        for (PHAsset * asset in self.uploaderStayfilm.listUploadedMedias.allValues) {
            if(self.sfApp.isEditingMode) {
                if(![self.sfApp.editedMedias containsObject:asset]) {
                    [self.sfApp.editedMedias addObject:asset];
                }
            } else {
                if(![self.sfApp.finalMedias containsObject:asset]) {
                    [self.sfApp.finalMedias addObject:asset];
                }
            }
        }
    }
    if(self.sfApp.isDebugMode) {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_FINISHED", nil)
                                                                       message:[[NSString alloc] initWithFormat:NSLocalizedString(@"UPLOADED_X_MEDIAS", nil), uploader.listUploadedMedias.count]
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    [self.photosCollectionView reloadData];
}

-(void)uploadFinishedMedia:(PHAsset *)asset {
    [self.photosCollectionView reloadData];
}

- (void)uploadDidStart {
    // do nothing
    [self.photosCollectionView reloadData];
}

#pragma mark - Text Field Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    const char *cString = [self.txt_movieTitle.text UTF8String];
    int textLength = (int)strlen(cString);
    const char *cAddString = [string UTF8String];
    int textAddlen =(int)strlen(cAddString);
    
    if (textLength + textAddlen > 80) {
        if([string  isEqual: @""]){
            return YES;
        }else{
            return NO;
        }
        //self.txt_movieTitle.text = [self.txt_movieTitle.text substringToIndex:80];
    }
    
    if(self.sfApp.isEditingMode) {
        if (self.txt_movieTitle.text.length == 0) {
        
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"edit-film"
                                                              action:@"erase-title"
                                                               label:@""
                                                               value:@1] build]];
        }
    }
    
    
    if (self.sfApp.isEditingMode && (![self.sfApp.selectedTitle isEqualToString:string])){
        
        [self.sfApp.filmStrip setHidden:YES];
        [self.checkEditButton.layer setZPosition:1];
        [self.checkEditButton setHidden:NO];
        [self.but_back setImage:nil forState:UIControlStateNormal];
        [self.widthCancelConstraint setConstant:85.0];
        [self.but_back setTitle:NSLocalizedString(@"CANCEL", nil) forState:UIControlStateNormal];
        
    }else{
        
        [self.checkEditButton setHidden:YES];
        [self.but_back setImage:[UIImage imageNamed:@"ARROW_Back"] forState:UIControlStateNormal];
        [self.widthCancelConstraint setConstant:40.0];
        [self.but_back setTitle:@"" forState:UIControlStateNormal];
        
    }
    
    return YES;
}

#pragma mark - Navigation

BOOL isMovingToStep2 = NO;
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [self.view endEditing:YES];
    if([segue.identifier isEqualToString:@"ChooseStyleSegue"] && !isMovingToStep2)
    {
        isMovingToStep2 = YES;
        if(self.txt_movieTitle.text != nil) {
            self.sfApp.selectedTitle = self.txt_movieTitle.text;
        }
        //Step3StylesViewController *step3style = segue.destinationViewController;
        isMovingToStep2 = NO;

    }else if([segue.identifier isEqualToString:@"confirmationToProgress"] && !isMovingToStep2){
        
        isMovingToStep2 = YES;
        if(self.txt_movieTitle.text != nil) {
            self.sfApp.selectedTitle = self.txt_movieTitle.text;
        }
        isMovingToStep2 = NO;
        
    }
    
    if ([segue.identifier isEqualToString:@"PlaySelectedMovieSegue"] && !isMovingToStep2)
    {
        isMovingToStep2 = YES;
        //        //Google Analytics
        //        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        //        [tracker set:kGAIScreenName value:@"Iphone_Nativo_Tela_10_SFMobile_Meus-filmes_ver-filme"];
        //        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
        
        //Bypass Audio lock
        AVAudioSession *audioSession = [AVAudioSession sharedInstance];
        NSError *error = nil;
        BOOL result = NO;
        result = [audioSession setActive:YES withOptions:0 error:&error];
        
        if (!result && error) {
            // deal with the error
            NSLog(@"StayLog: AVAudioSession Error: %@", error);
        }
        
        error = nil;
        result = [audioSession setCategory:AVAudioSessionCategoryPlayback error:&error];
        
        if (!result && error) {
            // deal with the error
            NSLog(@"StayLog: AVAudioSession setCategory Error: %@", error);
        }
        
        PlayerViewController *moviePlayerViewController = segue.destinationViewController;
        if([sender isKindOfClass:[PHAsset class]]) {
            moviePlayerViewController.assetMovie = (PHAsset *)sender;
            moviePlayerViewController.currentMovie = nil;
            
            if(self.videoImageManager == nil)
                self.videoImageManager = [[PHCachingImageManager alloc] init];
            [self.videoImageManager requestAVAssetForVideo:sender options:nil resultHandler:^(AVAsset * _Nullable asset, AVAudioMix * _Nullable audioMix, NSDictionary * _Nullable info) {
                moviePlayerViewController.player = [AVPlayer playerWithURL:((AVURLAsset *)asset).URL];
            }];
        }
        else if([sender isKindOfClass:[Movie class]]) {
            moviePlayerViewController.currentMovie = (Movie *)sender;
            moviePlayerViewController.assetMovie = nil;
            moviePlayerViewController.videoUrl = [sender videoUrl];
        }
        else if([sender isKindOfClass:[NSString class]]) {
            moviePlayerViewController.assetMovie = nil;
            moviePlayerViewController.currentMovie = nil;
            moviePlayerViewController.videoUrl = sender;
        }
        
        isMovingToStep2 = NO;
    }
}

- (IBAction)unwindToStep1:(UIStoryboardSegue *)segue
{
    //TODO: Cleanup maybe?
}


-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if([identifier isEqualToString:@"ChooseStyleSegue"])
    {
        return YES;
    }
    return NO;
}

@end

