//
//  VideoFinishedViewController.h
//  fbmessenger
//
//  Created by Henrique Ormonde on 18/06/15.
//  Copyright (c) 2015 Stayfilm. All rights reserved.
//

@import FBSDKShareKit;

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "Movie.h"
//#import <FBSDKMessengerShareKit/FBSDKMessengerShareKit.h>
//#import <FBSDKMessengerShareKit/FBSDKMessengerURLHandler.h>
#import "StayfilmApp.h"
#import "Downloader.h"
#import "PopupMenuAnimation.h"
#import "Popover_ShareVideo_ViewController.h"
#import "Popover_SaveVideo_ViewController.h"
#import "Popover_MoreOptionsViewController.h"
#import "Popover_Delete_ViewController.h"
#import "ProgressBarViewController.h"
#import "ChangePrivacyViewController.h"
#import "LoginFacebookOperation.h"
#import "LoginToSocialNetworkOperation.h"
#import "PreviewNotificationViewController.h"
#import "NotificationNotAllowedViewController.h"
#import "GoToSettingsNotificationViewController.h"

@interface UIImage (ImageBlur)
- (UIImage *)imageWithGaussianBlur;
@end


@interface VideoFinishedViewController : UIViewController <NSURLConnectionDelegate, UIGestureRecognizerDelegate, UIViewControllerAnimatedTransitioning, UIViewControllerTransitioningDelegate, FBSDKSharingDelegate, LoginFacebookOperationDelegate, LoginToSocialNetworkOperationDelegate, Popover_ShareVideo_ViewControllerDelegate, Popover_SaveVideo_ViewControllerDelegate, Popover_MoreOptionsDelegate, Popover_DeleteDelegate, ProgressBarViewControllerDelegate, StayfilmSingletonDelegate, StayfilmDownloaderDelegate>

@property (weak, nonatomic) IBOutlet UIBarButtonItem *discardButton;
@property (weak, nonatomic) IBOutlet UIButton *playButton;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *but_menuPrivacy;
@property (weak, nonatomic) IBOutlet UILabel *txt_userName;
@property (weak, nonatomic) IBOutlet UILabel *txt_MovieTitle;
@property (weak, nonatomic) IBOutlet UIImageView *img_User;
@property (weak, nonatomic) IBOutlet UIButton *but_Publish;
@property (weak, nonatomic) IBOutlet UIButton *but_MoreOptions;
@property (weak, nonatomic) IBOutlet UIImageView *movieThumb;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_MovieThumb;

@property (nonatomic, strong) Movie *movie;
@property (nonatomic, strong) NSString *movieFile;
@property (nonatomic, strong) PopupMenuAnimation *transitionAnimation;
@property (nonatomic, assign) BOOL downloading;

@property (nonatomic, assign) BOOL notificationsAllowed;

//- (void)downloadMovieToDevice;

@end
