//
//  Popover_ConfirmPrivacyShareViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Henrique on 8/29/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import "Popover_ConfirmPrivacyShareViewController.h"

@interface Popover_ConfirmPrivacyShareViewController ()

@end

@implementation Popover_ConfirmPrivacyShareViewController

@synthesize but_continue, but_cancel;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *modalTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOutside)];
    modalTap.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:modalTap];
    
    self.but_cancel.layer.borderWidth = 1.0f;
    self.but_cancel.layer.borderColor = [[UIColor colorWithRed:80.0/255.0 green:128.0/255.0 blue:146.0/255.0 alpha:1.0] CGColor];
    self.but_cancel.layer.cornerRadius = 5;
    self.but_cancel.clipsToBounds = YES;
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tapOutside {
    [self.delegate performSelector:@selector(chooseOption:) withObject:@"cancelShare"];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)but_continue_Clicked:(id)sender {
    NSString * opt = [@"continueShare-" stringByAppendingString:self.option];
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.delegate performSelector:@selector(chooseOption:) withObject:opt];
}

- (IBAction)but_cancel_Clicked:(id)sender {
    [self tapOutside];
    [self.delegate performSelector:@selector(chooseOption:) withObject:@"cancelShare"];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
