//
//  PhotoFB.m
//  Stayfilm for Messenger
//
//  Created by Henrique Ormonde on 15/05/15.
//  Copyright (c) 2015 Stayfilm. All rights reserved.
//

#import "PhotoFB.h"

@implementation ImageFB

@synthesize width, height, source;

+ (id)customClassWithProperties:(NSDictionary *)properties {
    if(properties != nil)
    {
        NSMutableDictionary *filteredObjs = [[NSMutableDictionary alloc] init];
        for (NSString* key in properties) {
            id value = [properties objectForKey:key];
            if ([key isEqualToString:@"width"]) {
                [filteredObjs setValue:value forKey:@"width"];
            }
            else if ([key isEqualToString:@"height"]) {
                [filteredObjs setValue:value forKey:@"height"];
            }
            else if ([key isEqualToString:@"source"]) {
                [filteredObjs setValue:value forKey:@"source"];
            }
        }
        return [[self alloc] initWithProperties:filteredObjs];
    }
    else
    {
        return nil;
    }
}

- (id)initWithProperties:(NSDictionary *)properties {
    if (self = [self init]) {
        @try {
            [self setValuesForKeysWithDictionary:properties];
        }
        @catch (NSException *ex) {
            return nil;
        }
    }
    return self;
}

-(id)init {
    self = [super init];
    if (0 != self) {
        self.source = nil;
    }
    return self;
}

@end


@implementation PhotoFB

@synthesize idPhoto, idAlbum, width, height, source, images;

+ (id)customClassWithProperties:(NSDictionary *)properties {
    @try {
        if(properties != nil)
        {
            NSMutableDictionary *filteredObjs = [[NSMutableDictionary alloc] init];
            for (NSString* key in properties) {
                id value = [properties objectForKey:key];
                if ([key isEqualToString:@"id"]) {
                    [filteredObjs setValue:value forKey:@"idPhoto"];
                }
                else if ([key isEqualToString:@"width"]) {
                    [filteredObjs setValue:value forKey:@"width"];
                }
                else if ([key isEqualToString:@"height"]) {
                    [filteredObjs setValue:value forKey:@"height"];
                }
                else if ([key isEqualToString:@"source"]) {
                    [filteredObjs setValue:value forKey:@"source"];
                }
                else if ([key isEqualToString:@"images"]) {
                    NSMutableArray *aux = [[NSMutableArray alloc] init];
                    for(NSDictionary *var in value) {
                        [aux addObject:[ImageFB customClassWithProperties:var]];
                    }
                    NSArray * images = [aux copy];
                    [filteredObjs setValue:images forKey:@"images"];
                }
            }
            return [[self alloc] initWithProperties:filteredObjs];
        }
        else
        {
            return nil;
        }

    }
    @catch (NSException *exception) {
        return nil;
    }

}

- (id)initWithProperties:(NSDictionary *)properties {
    if (self = [self init]) {
        @try {
            [self setValuesForKeysWithDictionary:properties];
        }
        @catch (NSException *ex) {
            return nil;
        }
    }
    return self;
}

-(id)init {
    self = [super init];
    if (0 != self) {
        self.source = nil;
        self.idAlbum = nil;
    }
    return self;
}

@end
