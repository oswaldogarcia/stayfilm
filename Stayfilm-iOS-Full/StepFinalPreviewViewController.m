//
//  StepFinalPreviewViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Henrique on 1/8/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import "StepFinalPreviewViewController.h"
#import "ProgressViewController.h"

@interface StepFinalPreviewViewController ()

@property (nonatomic, strong) StayfilmApp *sfAppStep3;
@property (nonatomic, weak) Uploader *uploaderStayfilm;

@end

@implementation StepFinalPreviewViewController

@synthesize txt_Title, txt_Style, txt_TotalCountMedias, but_finish;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.sfAppStep3 = [StayfilmApp sharedStayfilmAppSingleton];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.txt_TotalCountMedias.text = [[NSString alloc] initWithFormat:@"%i", [self.selectedMedias count] ];
    self.txt_Title.text = self.selectedTitle;
    self.txt_Style.text = self.selectedGenre.name;
    
    self.uploaderStayfilm = [Uploader sharedUploadStayfilmSingletonWithDelegate:self];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Uploader Delegates


-(void)uploadProgressChanged:(NSNumber *)progress
{
    // Not important on this page.
}

- (void)converterDidFailMedia:(NSString *)type
{
    NSString *message = nil;
    if(self.sfAppStep3 == nil)
    {
        self.sfAppStep3 = [StayfilmApp sharedStayfilmAppSingleton];
    }
    if (self.uploaderStayfilm.listUploadMedias.count + self.sfAppStep3.selectedMedias.count < [self.sfAppStep3.sfConfig.min_photos intValue]) {
        message = [NSString stringWithFormat:NSLocalizedString(@"UPLOADER_CONVERT_FAILED_UPLOAD_AGAIN_MESSAGE", nil), [self.sfAppStep3.sfConfig.min_photos intValue] - (self.uploaderStayfilm.listUploadMedias.count + self.sfAppStep3.selectedMedias.count)];
    }
    else if ([type isEqualToString:@"image"]) {
        message = NSLocalizedString(@"UPLOADER_CONVERT_FAILED_IMAGE_MESSAGE", nil);
    }
    else
    {
        message = NSLocalizedString(@"UPLOADER_CONVERT_FAILED_VIDEO_MESSAGE", nil);
    }
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_CONVERT_FAILED_TITLE", nil)
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)uploadDidFailMedia:(NSString *)type
{
    NSString *message = nil;
    if(self.sfAppStep3 == nil)
    {
        self.sfAppStep3 = [StayfilmApp sharedStayfilmAppSingleton];
    }
    if (self.uploaderStayfilm.listUploadMedias.count + self.sfAppStep3.selectedMedias.count < [self.sfAppStep3.sfConfig.min_photos intValue]) {
        message = [NSString stringWithFormat:NSLocalizedString(@"UPLOADER_UPLOAD_FAILED_UPLOAD_AGAIN_MESSAGE", nil), [self.sfAppStep3.sfConfig.min_photos intValue] - (self.uploaderStayfilm.listUploadMedias.count + self.sfAppStep3.selectedMedias.count)];
    }
    else if ([type isEqualToString:@"image"]) {
        message = NSLocalizedString(@"UPLOADER_UPLOAD_FAILED_IMAGE_MESSAGE", nil);
    }
    else
    {
        message = NSLocalizedString(@"UPLOADER_UPLOAD_FAILED_VIDEO_MESSAGE", nil);
    }
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_UPLOAD_FAILED_TITLE", nil)
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)uploadDidFailed:(Uploader *)uploader
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_ERROR_TITLE", nil)
                                                                   message:NSLocalizedString(@"UPLOADER_ERROR_MESSAGE", nil)
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    
    //[self reloadAlbumsCounters];
}

-(void)uploadDidStartSendingConfirmationMedias:(Uploader *)uploader
{
    // not important on this page.
}

-(void)uploadDidStart
{
    // does not happen in this page
}

-(void)uploadDidFinish:(Uploader *)uploader
{
    if(self.uploaderStayfilm.listUploadMedias != nil && self.uploaderStayfilm.listUploadMedias.count > 0) {
        for (PHAsset * asset in self.uploaderStayfilm.listUploadMedias) {
            if(![self.selectedMedias containsObject:asset]) {
                [self.selectedMedias addObject:asset];
            }
        }
    }
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_FINISHED", nil)
                                                                   message:[[NSString alloc] initWithFormat:NSLocalizedString(@"UPLOADED_X_MEDIAS", nil), uploader.listUploadedMedias.count]
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}


#pragma mark - Navigation

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
//    if(self.selectedMedias.count >= [self.sfAppStep3.sfConfig.min_photos intValue])
//    {
//        return YES;
//    }
//    else
//    {
////        //Google Analytics
////        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
////        [tracker set:kGAIScreenName value:@"Iphone_Nativo_Tela_13_SFMobile_Erro-do-usuario_3_faltou-fotos"];
////        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
//
//        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CHOOSE_MINIMUM_PHOTOS", nil)
//                                                                       message:[[NSString alloc] initWithFormat:NSLocalizedString(@"CHOOSE_MINIMUM_PHOTOS_MSG", nil),   [self.sfAppStep3.sfConfig.min_photos intValue]]
//                                                                preferredStyle:UIAlertControllerStyleAlert];
//
//        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
//                                                              handler:^(UIAlertAction * action) {}];
//
//        [alert addAction:defaultAction];
//        [self presentViewController:alert animated:YES completion:nil];
//        return NO;
//    }
    return YES;
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    @try {
        if ([segue.identifier isEqualToString:@"StepFinaltoProgress"]) {
            ProgressViewController *progressViewController = segue.destinationViewController;
            progressViewController.selectedGenre = self.selectedGenre;
            progressViewController.selectedMedias = [self.selectedMedias mutableCopy];
            progressViewController.selectedTitle = [self.selectedTitle copy];
            
            if(self.sfAppStep3 == nil)
            {
                self.sfAppStep3 = [StayfilmApp sharedStayfilmAppSingleton];
            }
            self.sfAppStep3.selectedGenre = self.selectedGenre;
            self.sfAppStep3.selectedMedias = self.selectedMedias;
            self.sfAppStep3.selectedTitle = self.selectedTitle;
        }
    }
    @catch (NSException *exception) {
        NSLog(@"EXCEPTION segue Step3 to Progress with error: %@", exception.description);
    }
}


@end
