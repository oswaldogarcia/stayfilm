//
//  GoToSettingsNotificationViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 12/10/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import "GoToSettingsNotificationViewController.h"

@interface GoToSettingsNotificationViewController ()

@end

@implementation GoToSettingsNotificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setLanguage];
}

- (void)setLanguage{
    
    
    UIFont *font1 = [UIFont fontWithName:@"ProximaNova-Regular" size:20];
    UIFont *font2 = [UIFont fontWithName:@"ProximaNova-Semibold" size:20];
    
    NSDictionary *fontDict1 = [NSDictionary dictionaryWithObject: font1 forKey:NSFontAttributeName];
    NSMutableDictionary *fontDict2 = [[NSMutableDictionary alloc]init];
    [fontDict2 setObject:font2  forKey:NSFontAttributeName];
    
    NSMutableAttributedString *fontType1 = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"PLEASE_ALLOW", nil) attributes: fontDict1];
    
    NSMutableAttributedString *fontType2 = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"SETTINGS", nil) attributes: fontDict2];
    
    NSMutableAttributedString *fontType3 = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"YOUR_DEVICE", nil) attributes: fontDict1];
    
    [fontType1 appendAttributedString:fontType2];
    [fontType1 appendAttributedString:fontType3];
    
    self.pleaseAllowLabel.attributedText =  fontType1;
    
//    self.pleaseAllowLabel.text =  NSLocalizedString(@"PLEASE_ALLOW", nil);
    [self.goToSettingsButton setTitle:NSLocalizedString(@"GO_TO_SETTING", nil) forState:UIControlStateNormal];
    [self.cancelButton setTitle:NSLocalizedString(@"CANCEL", nil) forState:UIControlStateNormal];
    
}


- (IBAction)goToSettingsAction:(id)sender {
    
    [self.delegate goToSettings];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}
- (IBAction)cancelAction:(id)sender {
    
    [self.delegate closeModal];
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
