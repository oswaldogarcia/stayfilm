//
//  AlbumFB.m
//  Stayfilm for Messenger
//
//  Created by Henrique Ormonde on 14/05/15.
//  Copyright (c) 2015 Stayfilm. All rights reserved.
//

#import "AlbumFB.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@implementation AlbumFB

@synthesize idAlbum, name, link, cover_photo, count, idMedias;


+ (id)customClassWithProperties:(NSDictionary *)properties {
    if(properties != nil)
    {
        NSMutableDictionary *filteredObjs = [[NSMutableDictionary alloc] init];
        for (NSString* key in properties) {
            id value = [properties objectForKey:key];
            if ([key isEqualToString:@"id"]) {
                [filteredObjs setValue:value forKey:@"idAlbum"];
            }
            else if ([key isEqualToString:@"name"]) {
                [filteredObjs setValue:value forKey:@"name"];
            }
            else if ([key isEqualToString:@"link"]) {
                [filteredObjs setValue:value forKey:@"link"];
            }
            else if ([key isEqualToString:@"cover_photo"]) {
                [filteredObjs setValue:value forKey:@"cover_photo"];
            }
            else if ([key isEqualToString:@"picture"]) {
                id valueData = [value objectForKey:@"data"];
                [filteredObjs setValue:valueData[@"url"] forKey:@"cover_photo"];
            }
            else if ([key isEqualToString:@"count"]) {
                [filteredObjs setValue:value forKey:@"count"];
            }
        }
        return [[self alloc] initWithProperties:filteredObjs];
    }
    else
    {
        return nil;
    }
}

- (id)initWithProperties:(NSDictionary *)properties {
    if (self = [self init]) {
        @try {
            [self setValuesForKeysWithDictionary:properties];
        }
        @catch (NSException *ex) {
            return nil;
        }
    }
    return self;
}

-(id)init {
    self = [super init];
    if (0 != self) {
        self.idAlbum = nil;
        self.idMedias = [[NSMutableArray alloc] init];
    }
    return self;
}

@end
