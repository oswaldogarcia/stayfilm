/* 
  Localizable.strings
  Stayfilm-iOS-Full

  Created by Henrique Ormonde on 24/08/17.
  Copyright © 2017 Stayfilm. All rights reserved.
*/

"WHATSAPP_SHARE_MESSAGE" = "Hey! Check out this film created using Stayfilm \r\n %@";
"VERSION_X" = "version %@";
"OK" = "OK";
"CANCEL" = "Cancel";
"YES" = "Yes";
"NO" = "No";
"X_ITEMS" = "%@ items";
"MALE" = "Male";
"FEMALE" = "Female";
"UNDEFINED" = "Not specified";
"NEXT" = "NEXT";
"EDIT_FILM" = "UPDATE MY FILM";
"CAMERA_ROLL" = "Camera Roll";
"FACEBOOK" = "Facebook";
"INSTAGRAM" = "Instagram";
"WHATSAPP" = "WhatsApp";
"TWITTER" = "Twitter";
"MESSENGER" = "Messenger";
"GOOGLE_PHOTOS" = "Google Photos";
"STAYFILM" = "Stayfilm";
"VIDEOS" = "Videos";
"COPY_LINK" = "Copy link";
"MORE" = "More";
"DESELECT_ALL" = "Deselect All";
"SELECT_ALL" = "Select All";
"LIMIT_REACHED" = "  Limit reached  ";
"LIMIT_X_REACHED" = "Limit reached. You can't add more than %d.";
"LIMIT_REACHED_X" = "Add more photos and videos, at least %d.";

"X_AT_LEAST" = "%@ at least ";
"MORE_BETTER" = "  The more, the better  ";
"CHOOSE_TEMPLATE" = "Style not selected...";
"CHOOSE_TEMPLATE_MSG" = "Please choose a style to proceed.";
"FACEBOOK_FALIED" = "Failed to connect to Facebook";
"FACEBOOK_FALIED_MESSAGE" = "3 attempts to connect to Facebook have failed. Please try again later.";
"FACEBOOK_ID_FALIED" = "Something doesn't seem right...";
"FACEBOOK_ID_FALIED_MESSAGE" = "You may have changed the login on your Facebook App. This could generate some errors getting your medias from Facebook. We recommend that you logout and login again in the Stayfilm App";
"FACEBOOK_FAILED_TOKEN" = "There was a problem saving your session.";
"FACEBOOK_FAILED_TOKEN_MESSAGE" = "We will try again... Please be patient with us... 😅";
"FACEBOOK_LOGGED_DIFFERENT_USER" = "Something doesn't seem right...";
"FACEBOOK_LOGGED_DIFFERENT_USER_MESSAGE" = "Looks like this Facebook account is not the same user that you logged in via email and password. Please make sure that you are logged in Facebook with your correct account. If not, please logout and then login again via email and integrate Facebook again. If the facebook account is the correct one, please ignore this message.";
"FACEBOOK_ID_WAITING" = "Connecting to Facebook...";
"FACEBOOK_ID_WAITING_MESSAGE"= "Please try again in a couple of seconds. We are checking your facebook connection.";
"FACEBOOK_ERROR" = "Failed to connect to Facebook";
"FACEBOOK_ERROR_MESSAGE" = "Something is wrong with the connection to Facebook. Please try again later.";
"FACEBOOK_ERROR_AUTH" = "Facebook token needs to be updated";
"FACEBOOK_ERROR_AUTH_MESSAGE" = "Please log in with facebook to update permissions.";
"FACEBOOK_LOGGEDOUT" = "User cancelled or user logged out Facebook";
"FACEBOOK_LOGGEDOUT_MESSAGE" = "You have cancelled or you are not logged in Facebook App";
"FACEBOOK_SHARED" = "Your film was shared...";
"FACEBOOK_SHARED_MESSAGE" = "Your film was successfuly shared on Facebook.";
"FACEBOOK_SHARE_LOGIN_TITLE" = "Not logged in Facebook yet";
"FACEBOOK_SHARE_LOGIN_MESSAGE" = "You haven't logged on Facebook yet. Please log in now.";
"INSTAGRAM_NOT_FOUND" = "Instagram App not found.";
"INSTAGRAM_NOT_FOUND_MESSAGE" = "Please install Instagram and try again.";
"SOCIAL_FALIED" = "Something is wrong...";
"SOCAIL_FALIED_MESSAGE" = "Lost connection to Social Network. Please try again later.";
"REGISTRATION_ERROR" = "Something went wrong...";
"REGISTRATION_ERROR_MESSAGE" = "There was a problem with your registration. Please try again later.\nIf this problem persists please contact us so we can fix it ASAP.";
"CONNECTION_ERROR" = "Something went wrong...";
"CONNECTION_ERROR_MESSAGE"= "Connection failed. Please check your connection and try again.";
"CONNECTION_ERROR_MESSAGE_2"= "Connection failed. Trying again...";
"NO_INTERNET_CONNECTION" = "Connection lost...";
"NO_INTERNET_CONNECTION_MESSAGE" = "There is no internet connection at the moment. We'll keep trying to reconnect...";
"NO_INTERNET_RECONNECTING" = "No internet connection";
"USER_CANCELLED" = "User cancelled";
"USER_CANCELLED_MESSAGE" = "User cancelled or didn't give necessary permissions to connect with the Social Network.";
"LOGIN_FAILED" = "Could not login...";
"LOGIN_FAILED_MESSAGE" = "Login process has failed. Please try again later.";
"LOGIN_FAILED_WITH_MESSAGE" = "Login process has failed with the following message: %@ ";
"LOGIN_MISSING_CREDENTIALS" = "Credentials missing...";
"LOGIN_MISSING_CREDENTIALS_MESSAGE" = "Please fill in both email and password.";
"NO_MEDIAS" = "Missing Medias";
"NO_MEDIAS_MESSAGE" = "No Medias were found in this album.";
"DOWNLOADING_PROGRESS" = "Downloading... %d %%";
"DOWNLOADING_MOVIE_TITLE_STILL" = "Please wait...";
"DOWNLOADING_MOVIE_MESSAGE_STILL" = "Your film is being downloaded.";
"DOWNLOADING_MOVIE_TITLE_STARTED" = "Download started...";
"DOWNLOADING_MOVIE_MESSAGE_STARTED" = "We need to download the film so you can share on Instagram, please wait...";
"UPLOADER_FAILED" = "Sorry! There was a problem with the Upload. Your internet connection may be experiencing some difficulties and/or your device memory may be full. Please try again later.";
"UPLOADER_ERROR_TITLE" = "Uploader Error";
"UPLOADER_LOAD_ERROR_MESSAGE" = "Sorry! The Upload component failed to initialize. Please use only medias from Facebook network for now and please report this to contato@stayfilm.com since this error was not supposed to happen.";
"UPLOADER_ALREADY_ADDED_TITLE" = "Warning...";
"UPLOADER_ALREADY_ADDED_MESSAGE" = "%d media(s) you selected were already added and won't be uploaded again.";
"UPLOADER_CONVERT_FAILED_TITLE" = "Warning...";
"UPLOADER_CONVERT_FAILED_VIDEO_MESSAGE" = "A video failed to be compressed. This could be caused because your device may be almost full or the video was way too big to upload. We will make your film with the remaining medias.";
"UPLOADER_CONVERT_FAILED_IMAGE_MESSAGE" = "An image failed to be compressed. This could be caused because your device may be almost full or the image was way too big to upload. We will make your film with the remaining medias.";
"UPLOADER_CONVERT_FAILED_UPLOAD_AGAIN_MESSAGE" = "A media failed to be compressed. This could be caused because your device may be almost full or the media was way too big to upload. Please select or upload %d more medias to reach the minimum medias required to make a film.";
"UPLOADER_UPLOAD_FAILED_VIDEO_MESSAGE" = "A video failed to upload. This could be caused because of low internet connectivity or the video was way too big to upload. But don't worry, we will make your film with the remaining medias. 😉";
"UPLOADER_UPLOAD_FAILED_IMAGE_MESSAGE" = "An image failed to upload. This could be caused because of low internet connectivity or the image was way too big to upload. But don't worry, we will make your film with the remaining medias. 😉";
"UPLOADER_UPLOAD_FAILED_UPLOAD_AGAIN_MESSAGE" = "A media failed to upload. This could be caused because of low internet connectivity or the media was way too big to upload. Please select or upload %d more media(s) to reach the minimum medias required to make a film.";
"UPLOADER_ERROR_MESSAGE" = "Sorry! Something went wrong, we could not confirm the upload of your medias. This could happen because of low internet connectivity. Please try uploading your medias again.";
"UPLOADER_UPLOAD_FAILED_TITLE" = "Upload has failed.";
"UPLOADED_X_MEDIAS" = "%d medias were uploaded successfuly.";
"UPLOADER_FINISHED" = "Upload finished";
"STILL_UPLOADING" = "Uploading medias, please wait...";
"UPLOAD_FINISHED_START_CONFIRMATION" = "Confirming upload. Please wait...";
"UPLOAD_FINISHED_START_PROGRESS" = "The upload has finished. Starting film production...";
"UPLOAD_PROGRESS" = "Uploaded %i of %i medias... When it ends we will automatically make a film for you. Please wait...";
"PROGRESS_PRODUCE_START" = "Starting to produce your film. Please wait...";
"PROGRESS_FINISHED" = "Your film is almost done... Please wait, your film will open in a few seconds.";
"PROGRESS_WORKING" = "We are doing our magic... 🎬 Please wait...";
"ARE_YOU_SURE" = "Are you sure?";
"CONFIRM_UNWIND_TO_ALBUMS" = "If you go back you will cancel this album photo selection.";
"CONFIRM_UNWIND_FROM_REMOVE" = "If you go back you will cancel this removal selection.";
"CONFIRM_UNWIND_FROM_REORDER" = "If you go back you will cancel the reordering you just made.";
"CONFIRM_UNWIND_FROM_PROGRESS" = "If you go back you will cancel the production of this film. 😕";
"CONFIRM_UNWIND_FROM_STEP2" = "You've already selected some medias, if you go back you will cancel this selection.";
"CONFIRM_UNWIND_FROM_VIDEOFINISHED" = "If you cancel you will discard this film. 😕";
"CONFIRM_UNWIND_FROM_MOREOPTIONS" = "If you cancel you will lose all changes you made. 🤔";
"STAY" = "stay here";
"PASSWORD" = "PASSWORD";
"EMAIL" = "EMAIL";
"FILL_REQUIRED_FIELD" = "* These fields are required";
"EMAIL_ERROR" = "Please verify your email";
"EMAIL_ERROR_MESSAGE" = "This email is not valid.";
"FIRST_NAME" = "FIRST NAME";
"LAST_NAME" = "LAST NAME";
"NAME" = "NAME";
"SAVE" = "SAVE";
"SAVING" = "SAVING";
"SAVED" = "SAVED";
"SHARING" = "SHARING";
"SHARED" = "SHARED";
"DOWNLOAD_FAILED"= "Download failed...";
"DOWNLOADING"= "Downloading";
"DOWNLOADING_PROGRESS"= "%.f%%";
"DOWNLOADED" = "Downloaded";
"DOWNLOAD" = "Download";
"ALREADY_DOWNLOADED" = "This film was already downloaded...";
"IM_DONE" = "Done";
"LINK_COPIED" = "The link has been successfully copied";
"SHARED_LOWERED" = "Shared";
"BACK_TAP_CANCEL_DOWNLOAD" = "Your film is being downloaded";
"BACK_TAP_CANCEL_DOWNLOAD_MESSAGE" = "Are you sure you want to cancel the download?";

"RECOVER_CODE_TITLE" = "An email will be sent to %@ if it has been found in our database. Please check your inbox for the validation code.";
"INVALID_RECOVER_CODE_ERROR" = "* Invalid or expired code. Please try again.";
"INVALID_RECOVER_CODE_EMPTY" = "* Please fill in the recovery code.";
"PASSWORD_MISSMATCH" = "* The passwords do not match.";
"PASSWORD_MIN_ERROR" = "Password is too short";
"PASSWORD_MIN_ERROR_MESSAGE" = "* Password must be at least 5 characters long.";
"LOGGING_IN" = "Logging in...";
"RECOVER_EMAIL_RESENT" = "Your email has been resent";
"RECOVER_EMAIL_RESENT_MESSAGE" = "In case your email is registered in our system, another email has been sent to %@. Please check your inbox and junk/spam mail box too, as sometimes the email could end up there.";
"RECOVER_EMAIL_RESENT_TITLE" = "In case your email is registered in our system, another email has been sent to %@. Please check your inbox and junk/spam mail box too.";

"SHARE_FAILED" = "Something is wrong";
"SHARE_FAILED_MESSAGE" = "There was a problem trying to share your film. Please try again in a few moments.";

"UNABLE_LOAD_MOVIE_THUMB" = "Connection problem...";
"UNABLE_LOAD_MOVIE_THUMB_MESSAGE" = "We are sorry, but the internet connection failed and we were unable to load the film thumbnail. You still may be able to play the film, try tapping on the PLAY button!";

"COPIED_TO_CLIPBOARD_TITLE" = "Film link copied";
"COPIED_TO_CLIPBOARD_MESSAGE" = "Film link was successfully copied to clipboard.";

"SOMETHING_WRONG" = "Something is wrong...";
"PROBLEM_UNCERTAIN_CONTENT" = "Sorry... This is embarrassing... But the content to make a film is uncertain. Please try again.";
"PROBLEM_NOT_ENOUGH_CONTENT" = "Sorry... but for some reason there was not enough content to make your film.";
"PROBLEM_MELIES" = "We are sorry. Something strange happened and we weren't able to create your film. 👽 Please try again.";
"PROBLEM_EXPLORE" = "We are sorry. Something went wrong loading the galleries. 👽 Please try again later.";

"WHATSAPP_NOT_FOUND" = "WhatsApp not found";
"WHATSAPP_NOT_FOUND_MESSAGE" = "Please install WhatsApp and try again";
"TWITTER_SHARED" = "Your film was shared";
"TWITTER_SHARED_MESSAGE" = "Your film was successfuly shared on Twitter.";
"TWITTER_ERROR" = "Failed to connect to Twitter";
"TWITTER_ERROR_MESSAGE" = "Something is wrong with the connection to Twitter. Please try again later.";

"DO_LOGOUT" = "Do you want to logout?";

//Profile

"PROFILE" = "Profile";
"FILMS" = " My Films";
"SHARE" = "Share";
"NO_FILMS_YET" = "You didn’t make any films yet. 😞";

"EDIT_PROFILE" = "Edit Profile";
"LOGOUT" = "Logout";
"SAVE_CHANGE" = "SAVE CHANGES";

"CHANGE_SAVED" = "The changes were saved";
"CHANGE_NOT_SAVED" = "The changes could not be saved, try again";

"CHANGE_PRIVACY" = "Change film privacy";
"DELETE_FILM" = "Delete this film";
"FILM_DELETED" = "Film Deleted";
"DELETING" = "Deleting...";

"TAKE_PICTURE" = "Take a picture";
"CHOOSE_PICTURE" = "Choose a picture";

// Disney

"HOW_DISNEY_WORK" = "How Disney films work? Check it out!";

//Privacy

"WHO_CAN_SEE" = "WHO CAN SEE THIS FILM";
"PUBLIC" = "Public";
"PUBLIC_DESCRIPTION" = "Public films can be watched and discovered by anyone";
"FRIENDS" = "Link";
"FRIENDS_DESCRIPTION" = "Anyone with the link can watch this film";
"PRIVATE" = "Private";
"PRIVATE_DESCRIPTION" = "Can only be viewed by the film owner";
"LINK" = "Link";
"LINK_DESCRIPTION" = "Anyone with the link can watch this film.";

//MORE OPTIONS

"MEDIAS_COUNT" = "%lu photo(s) / %lu video(s)";

//Terms Style

"TO_CHOOSE_STYLE" = "To choose this style, some types of content are not allowed in the film. You have to agree with the terms to proceed.";
"NO_SMOKING" = "No smoking,\n alcohol or drugs";
"NO_ILEGAL" = "No illegal \n or offensive";
"NO_COMMERCIAL" = "No commercial \npurposes";
"I_AGREE" = "I agree with ";
"TERMS_DISNEY" = "Terms and Policy from Disney.";
"TERMS_STAYFILM" = "Terms and Policy from Stayfilm.";
"CONFIRM" = "CONFIRM";

//Preview Notification

"SAVED_FILM_LABEL" = "Yes! Your film is saved!";

"VERIFYING_FILM" = "Now our team is verifying if the content is in compliance with our policy.";

"TOLD_NOTIFICATION" = "When we finish you’ll be told by email and notification.";

//Preview Notification not Allowed

"ALLOW_NOTIFICATION_LABEL" = "When we finish you’ll be told by email and notification. Would you like to allow Stayfilm send you notifications to this device?";

"ALLOW_NOTIFICATION" = "Allow Notifications";
"NOT_NOW" = "Not Now";

// Go to Settings
"PLEASE_ALLOW" = "Please allow Stayfilm to receive notifications on the ";
"SETTINGS" = "Settings";
"YOUR_DEVICE" = " of your device.";
"GO_TO_SETTING" = "Go to settings";

//Notification HOW DISNEY WORK

"HOW_DISNEY_WORK_TITLE" = "How Disney films work?";
"TERM_DISNEY" = "Disney films go through a content review process to ensure they are in compliance with the ";
"TERMS_AND_POLICY" = "Terms and Policy.";
"IN_REVIEW_TITLE" = "IN REVIEW";
"IN_REVIEW_TEXT" = "If you see a red flag saying \"in review\" it means your film is still being analyzed.\nThis process takes some time according to the demand of our user’s films.";

"EDITED_TITLE" = "EDITED OR DELETED FILM";
"EDITED_TEXT" = "If you are not seeing a film that you previously created or any photo disappeared from your films, they were deleted because it wasn’t in compliance with our policy.\n\nYou’ll be informed as soon as your films are reviewed. We’ll send you a notification and email.";

"DISMISS" = "Dismiss";

//Orientation

"HORIZONTAL" = "Horizontal";
"VERTICAL" = "Vertical";

"VERTICAL_NO_AVAILABLE" = "Vertical orientation for this style will be available soon. You can choose horizontal instead or try another style.";

"FILM_ORIENTATION" = "Film orientation";

//Automatic

"AF_NOTIFICATION_TITLE" = "Your new film from %@ is waiting for you";
"AF_NOTIFICATION_BODY" = "Wanna see how great your photos would look into a film for free? Don’t miss it!!";
"AF_PROGRESS_TEXT" = "YOUR NEW FILM %@ IS ALMOST READY.";
"MAKE_FILM_BUTTON" = "SUGGESTED FOR YOU"; //"MAKE FILM IN ONE TAP";

//Instagram

"SHARE_INSTAGRAM" = "Share to Instagram";
"OPTIONS_INSTAGRAM" = "Please select where you want to share";
"STORIES" = "Stories";
"FEED" = "Feed";

//PURCHASE

"UNLOCK" = "Unlock";
"SUBSCRIBE_TO_UNLOCK" ="Subscribe to unlock this style and get unlimited access to all Stayfilm Premium styles.";
"SPECIAL_OFFER"= "SPECIAL OFFER";
"PRICE_MONTH" = "%@ / Month";
"PRODUCT_DESCRIPTION" = "Payment will be charged to iTunes Account at confirmation of purchase. Subscription automatically renews unless auto-renew is turned off at least 24-hours before the end of the current period. Account will be charged for renewal within 24-hours prior to the end of the current period. Subscriptions may be managed by the user and auto-renewal may be turned off by going to the user's Account Settings after purchase or using unsubscribe button on My Account App Section. Any unused portion of a free trial period, if offered, will be forfeited when the user purchases a subscription to that publication, where applicable.";

///"Unlimited films to create and share whenever you want.\n\nWith the Stayfilm Premium, you unlock several exclusive styles and have access to several different films.";

"RESTORE_PURCHASE" = "Restore Previous Purchase";
"PURCHASE_TERMS_TITLE" = "Terms and Policy";

"CONFIRM_PURCHASE" = "Your subscription has been confirmed.\n\nThanks for subscribing Stayfilm Premium.\nNow enjoy unlimited access to all styles.";

"NO_SUBSCRIPTION" = "You don't have an active subscription";

"BENEFITS" = "BENEFITS";
"BENEFITS_LIST" = "- Exclusive Styles\n- More films in the styles\n- No Ads";

"CONDITIONS_LABEL" = "By Selecting the plan below you agree to our:";
"CONDITIONS_BUTTON" = "Subscription Conditions";
"CONDITIONS_TITLE" = "By acquiring the Subscription plan you agree to our Subscription Conditions:";
"CONDITIONS_LIST" = "• You’ll have access to Premium features including adding exclusive Styles, more films in the current Styles, no ads.\n\n• Payment will be charged to iTunes Account at confirmation of purchase.\n\n• Subscription automatically renews unless auto-renew is turned off at least 24-hours before the end of the current period.\n\n• Account will be charged for renewal within 24-hours prior to the end of the current period.\n\n• Subscriptions may be managed by the user and auto-renewal may be turned off by going to the user’s Account Settings after purchase.";
