//
//  ReorderViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Rick on 29/11/17.
//  Copyright © 2017 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICollectionView+Draggable.h"
#import "Uploader.h"
#import "Media.h"
#import "AlbumFB.h"
#import "PhotoFB.h"
#import "AlbumInternal.h"
#import <SDWebImage/UIImageView+WebCache.h>


@interface ReorderViewController : UIViewController <StayfilmUploaderDelegate, UICollectionViewDataSource_Draggable, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *animationView;
//@property (weak, nonatomic) IBOutlet UIButton *but_ConfirmReorder;
@property (weak, nonatomic) IBOutlet UICollectionView *draggableCollection;

@property (weak, nonatomic) UINavigationController *navigationController;
@property (nonatomic, strong) NSArray *initialSelectedMedias;
@property (nonatomic, strong) NSMutableArray *selectedMedias;
@property (nonatomic, strong) PHCachingImageManager *videoImageManager;
@property (nonatomic, strong) NSURLSession *thumbConnection;

@end
