//
//  ExplorerPlayerViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 14/06/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import "ExplorerPlayerViewController.h"
#import "Movie.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <Google/Analytics.h>
#import "UIView+Toast.h"
#import "PlayerViewController.h"


@interface ExplorerPlayerViewController ()<PlayerExplorerDelegate>{
    
    int timeTick;
    
    BOOL isImage;
    
    BOOL isLongPress;
    
    BOOL isChangingContent;
    
    BOOL isExiting;
    
    BOOL needRemoveObsever;
    
    

}
@property(nonatomic, weak) StayfilmApp *sfApp;
@property(atomic, strong) NSTimer *timer;
@property (nonatomic, assign) BOOL isSetContent;
@end


@implementation ExplorerPlayerViewController


- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self prefersStatusBarHidden];
    [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    
    self.sfApp = [StayfilmApp sharedStayfilmAppSingletonWithDelegate:self];
    
    isChangingContent = NO;
    needRemoveObsever = NO;
    isExiting = NO;
    self.isStillAvailable = YES;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    self.sfApp = [StayfilmApp sharedStayfilmAppSingletonWithDelegate:self];
    
    [self.view makeToastActivity:CSToastPositionCenter];
    [self setPlayers];
    [self.view hideToastActivity];
    
    isChangingContent = YES;
    needRemoveObsever = NO;
    self.isStillAvailable = YES;
    
    if(self.explorerDelegate && [self.explorerDelegate respondsToSelector:@selector(setStoryLogo:)])
        [self.explorerDelegate setStoryLogo:self.story.logoUrl];
    
    // configuration of the segmented bar
    if (@available(iOS 11.0, *)) {
        UIWindow *mainWindow = [[[UIApplication sharedApplication] delegate] window];
        if (mainWindow.safeAreaInsets.top > 0.0) {  // is iPhone X
            self.segmentedProgressBar = [[SegmentedProgressBar alloc] initWithFrame:CGRectMake(10, 35, self.view.frame.size.width - 20, 7)];
        } else {
            self.segmentedProgressBar = [[SegmentedProgressBar alloc] initWithFrame:CGRectMake(10, 15, self.view.frame.size.width - 20, 7)];
        }
    }
    else {
        self.segmentedProgressBar = [[SegmentedProgressBar alloc] initWithFrame:CGRectMake(10, 15, self.view.frame.size.width - 20, 7)];
    }
    self.segmentedProgressBar.completedSegmentColor = [UIColor whiteColor];
    self.segmentedProgressBar.segmentColor = [UIColor lightGrayColor];
    self.segmentedProgressBar.numberOfSegments = (int)self.storiesContent.count;
    
    self.segmentedProgressBar.numberOfCompletedSegments = self.indexConten;
    
    [self.segmentedProgressBar setSegmentProgress:0.0];
    
    [self.view addSubview:self.segmentedProgressBar];
      [self.segmentedProgressBar.layer setZPosition:1];
    
    UIView *gestureView;
    
    if (@available(iOS 11.0, *)) {
        
        gestureView = self.contentOverlayView;
        
    }else{
        
        gestureView = self.view;
    }
    
    
    // set frame to back and next buttom
    CGRect nextRect = CGRectMake(100, self.view.frame.origin.y, self.view.frame.size.width - 100, self.view.frame.size.height);
    CGRect backRect = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, 100 , self.view.frame.size.height);
    
    self.nextView = [[UIButton alloc] initWithFrame:nextRect];
    [self.nextView.layer setZPosition:1];
    
    self.backView = [[UIButton alloc] initWithFrame:backRect];
    [self.backView.layer setZPosition:1];
    
    [self.nextView addTarget:self
                      action:@selector(changeStoryNextContentTap)
            forControlEvents:UIControlEventTouchUpInside];
    
    [self.backView addTarget:self
                      action:@selector(changeStoryBackContentTap)
            forControlEvents:UIControlEventTouchUpInside];
    
    // add back and next buttom
    [gestureView addSubview:self.nextView];
    
    [gestureView addSubview:self.backView];
    
    // Add Swipe Down Gesture
    UISwipeGestureRecognizer *swipeDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeDownGesture)];
    
    [swipeDown setDirection:UISwipeGestureRecognizerDirectionDown];
         
    [gestureView addGestureRecognizer:swipeDown];
    
    // Add Swipe Up Gesture
    UISwipeGestureRecognizer *swipeUp = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeUpGesture)];
    
    [swipeUp setDirection:UISwipeGestureRecognizerDirectionUp];
    
    [gestureView addGestureRecognizer:swipeUp];
    
    // Add Long Press Gesture
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc]
                                               initWithTarget:self
                                               action:@selector(handleLongPress:)];
    longPress.minimumPressDuration = 0.5;
    [gestureView addGestureRecognizer:longPress];
 
    
    // Add double tap Gesture to replace the native double tap zoom gesture on avplayer
    UITapGestureRecognizer *dobleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dobleTapGesture)];
    
    [dobleTap setNumberOfTapsRequired:2];
    
    [gestureView addGestureRecognizer:dobleTap];
    
    if(self.isViewLoaded)
        [self changeStoryNextContent];
    
    //Google Analytics
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:[@"explore_gallery-" stringByAppendingString:[self.story.title stringByReplacingOccurrencesOfString:@" " withString:@"_"]]];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
}

- (void)setPlayers{
    
        if(self.story.myPlayers.count < self.story.content.count){
            
            [self callAllPlayers];
            
        }else{
            self.myPlayers = self.story.myPlayers;
            [self setContent];
            self.isSetContent = YES;
        }
    
    
}
-(void)callAllPlayers{
    __weak typeof(self) weakSelf = self;
    
    //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
    
        [self.story setPlayersWithCompletion:^{
            
            weakSelf.myPlayers = weakSelf.story.myPlayers;
           
            if(!weakSelf.isSetContent){
                weakSelf.isSetContent = YES;
                [weakSelf setContent];
            }
            
//            if(weakSelf.story.myPlayers.count < weakSelf.story.content.count){
//                [weakSelf callAllPlayers];
//            }
        }];
        
        
    //});
    
}
- (void)setContent{
    
    
    
        [self resetPlayerObsevers];
        
        self.customPlayer = [[AVQueuePlayer alloc] initWithItems:self.myPlayers];
        //self.player = self.customPlayer;
        self.player.automaticallyWaitsToMinimizeStalling = NO;
        
        
        if (self.story.isStoryViewed){
            self.indexConten = -1;
        }
        [self.view hideToastActivity];
        
//    }@catch (NSException *ex) {
//        NSLog(@"StayLog: EXCEPTION in setContent with error: %@",ex.description);
//    }
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    isChangingContent = YES;
    needRemoveObsever = NO;
    timeTick = 0;
    isLongPress = NO;
    
    [self setShowsPlaybackControls:NO];
    [self.player seekToTime:CMTimeMake(0, 1)];
    [self.player pause];
    [self.customPlayer removeAllItems];
    
}


- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    isChangingContent = YES;
    
    self.isStillAvailable = NO;
    
    
    [self.player seekToTime:CMTimeMake(0, 1)];
    [self.timer invalidate];
    [self.player pause];
    
    
    
    [self.customPlayer removeAllItems];
    self.player = nil;
    

}

-(NSString *)getAnalyticString {
    @try {
        StoryContent *content = self.storiesContent[self.indexConten];
        
        NSString *analytic = nil;
        switch (content.type) {
            case ExploreStoryContent_IMAGE:
                analytic = @"image";
                break;
            case ExploreStoryContent_MoviePORTRAIT:
            case ExploreStoryContent_MovieLANDSCAPE:
                if(content.idMovie == nil || [content.idMovie isEqualToString:@""]) {
                    analytic = @"film";
                } else {
                    analytic = @"video";
                }
                break;
                
            default:
                break;
        }
        self.sfApp.analyticsTag = analytic;
        return analytic;
    }@catch(NSException *ex) {
        NSLog(@"StayLog: EXCEPTION in getAnalyticstring with error: %@",ex.description);
        return nil;
    }
}


// Content duration
#pragma mark - Timer

- (void)starTimer{
    
    [self.timer invalidate];
    self.timer = nil;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:0.001 target:self selector:@selector(myTicker) userInfo:nil repeats:YES];
}

-(void)myTicker{
    
//    CMTime currentTime = self.player.currentItem.currentTime;
//    float videoDurationSeconds = CMTimeGetSeconds(currentTime);
    //NSLog(@"TIME: %f",videoDurationSeconds);
    
    //increment the timer
    timeTick++;
    
    CGFloat progress = (float)((timeTick/10)/self.storyDuration)/100;
    
    [self updateProgressBar:progress];
    
    //if we want the timer to stop after a certain number of seconds we can do
    if(timeTick / 1000 == self.storyDuration){ //stop the timer after n seconds
        [self.timer invalidate];
        [self.player pause];
        @try{
            if(self.isViewLoaded && self.isStillAvailable)
                [self changeStoryNextContent];
            else if(self.isStillAvailable == NO)
                    [self.timer invalidate];
        }@catch (NSException *exception)
        {
            NSLog(@"StayLog: EXCEPTION myTicker :%@", exception.description);
            
        }
        
    }
}

#pragma mark - Activity

-(void)updateProgressBar:(CGFloat)progress{
    
     [self.segmentedProgressBar updateSegmentPrgress:progress];
}

#pragma mark - Story Navigation

-(void)resetPlayerObsevers{
    
    [self.player pause];
    [self.customPlayer removeAllItems];
    
    [self.view hideAllToasts];
    [self.segmentedProgressBar setSegmentProgress:0.0];
    [self.timer invalidate];
    timeTick = 0;
    [self.player seekToTime:CMTimeMake(0, 1)];
    
}

-(void)changeStoryNextContentTap{
    if(!isChangingContent) {
        isChangingContent = YES;
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:[self getAnalyticString]
                                                              action:@"tap-right"
                                                               label:@""
                                                               value:@1] build]];

        [self changeStoryNextContent];
    }
}
-(void)changeStoryNextContent{
    
    [self resetPlayerObsevers];
    
    self.indexConten = self.indexConten + 1;
    
    if (self.indexConten < (int)self.storiesContent.count){
        
        StoryContent *content = self.storiesContent[self.indexConten];
        
        [self showStoryContent:content];
        
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"explore-gallery"
                                                              action:[self.story.title stringByReplacingOccurrencesOfString:@" " withString:@"_"]
                                                               label:[self getAnalyticString]
                                                               value:@1] build]];
        
        needRemoveObsever = YES;
        
        StoryContent *contentToSetViewed;
        
        int contentPosition = self.indexConten - 1;
//            NSLog(@"contentPosition:%d", contentPosition);
        
        if (contentPosition < 0){
            contentToSetViewed = self.storiesContent[0];
        }else{
            contentToSetViewed = self.storiesContent[contentPosition];
        }
       
        @try {
            if(self.isViewLoaded && self.explorerDelegate != nil && [self.explorerDelegate respondsToSelector:@selector(setViewedId:onId:)])
                [self.explorerDelegate setViewedId:self.story.idStory onId:contentToSetViewed.idContent];
        } @catch (NSException *exception) {
            NSLog(@"StayLog: EXCEPTION 1 changeStoryNextContent: %@", exception.description);
        }
        
    }else{
        
        @try{
            if(self.explorerDelegate != nil && [self.explorerDelegate respondsToSelector:@selector(setViewedId:onId:)])
                [self.explorerDelegate setViewedId:self.story.idStory onId:((StoryContent*)[self.storiesContent lastObject]).idContent];
            self.indexConten = -1;
            
            if(self.explorerDelegate != nil && [self.explorerDelegate respondsToSelector:@selector(moveToNext)])
                [self.explorerDelegate moveToNext];
        }
        @catch (NSException *exception)
        {
            NSLog(@"StayLog: EXCEPTION 2 changeStoryNextContent: %@", exception.description);
        }
        
    }
    
    isChangingContent = NO;
}

-(void)changeStoryBackContentTap{
    if(!isChangingContent) {
        isChangingContent = YES;
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:[self getAnalyticString]
                                                              action:@"tap-left"
                                                               label:@""
                                                               value:@1] build]];

        [self changeStoryBackContent];
    }
}
-(void)changeStoryBackContent{

    [self resetPlayerObsevers];
    
    self.indexConten = _indexConten - 1;
//        NSLog(@"contentPositionBack:%d", self.indexConten);
    
    if ((self.indexConten < (int)self.storiesContent.count) &&  (self.indexConten >= 0)){
        
        StoryContent *content = self.storiesContent[_indexConten];
        
        [self showStoryContent:content];
        
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"explore-gallery"
                                                              action:[self.story.title stringByReplacingOccurrencesOfString:@" " withString:@"_"]
                                                               label:[self getAnalyticString]
                                                               value:@1] build]];
        
        @try {
            if(self.explorerDelegate != nil && [self.explorerDelegate respondsToSelector:@selector(setViewedId:onId:)])
                [self.explorerDelegate setViewedId:self.story.idStory onId:content.idContent];
        } @catch (NSException *exception) {
            NSLog(@"StayLog: EXCEPTION 1 changeStoryBackContent: %@", exception.description);
        }
        
    }else if (self.indexConten < 0){
        
        self.indexConten = 0;
        
        @try {
            if(self.explorerDelegate != nil && [self.explorerDelegate respondsToSelector:@selector(moveToBack)])
                [self.explorerDelegate moveToBack];
        }
        @catch (NSException *exception)
        {
            NSLog(@"StayLog: EXCEPTION 2 changeStoryBackContent: %@", exception.description);
        }
    }
    
    isChangingContent = NO;
}

- (void)showStoryContent:(StoryContent*) content{
    
    @try{
       // [self.view makeToastActivity:CSToastPositionCenter];
        NSString *analytic = nil;
        switch (content.type) {
            case ExploreStoryContent_IMAGE:
                analytic = @"image";
                break;
            case ExploreStoryContent_MoviePORTRAIT:
            case ExploreStoryContent_MovieLANDSCAPE:
                if(content.idMovie == nil || [content.idMovie isEqualToString:@""]) {
                    analytic = @"film";
                } else {
                    analytic = @"video";
                }
                break;
                
            default:
                break;
        }
        self.sfApp.analyticsTag = analytic;
        
        
        self.segmentedProgressBar.numberOfCompletedSegments = self.indexConten;
        
        [self.segmentedProgressBar setNumberOfCompletedSegments:self.indexConten];
        
        if (content.type != 3 ){
            
            isImage = NO;
            
            AVPlayerItem *playerItem = self.myPlayers[self.indexConten];
            CMTime videoDuration = playerItem.asset.duration;
            NSUInteger dTotalSeconds = CMTimeGetSeconds(videoDuration);
            NSNumber *numberOfSeconds =  [NSNumber numberWithUnsignedInteger:dTotalSeconds];

            if ([numberOfSeconds floatValue]  < [content.duration floatValue]){
                [self setStoryDuration:[numberOfSeconds floatValue]];
            }else{
                [self setStoryDuration:[content.duration floatValue]];

            }
            
            if(self.isViewLoaded && self.isStillAvailable)
            {
                if (content.contentDescription != nil || ![content.contentDescription isEqualToString: @""]){
                    if(self.explorerDelegate && [self.explorerDelegate respondsToSelector:@selector(setContentDescription:)])
                        [self.explorerDelegate setContentDescription:content.contentDescription];
                }else{
                    if(self.explorerDelegate && [self.explorerDelegate respondsToSelector:@selector(setContentDescription:)])
                        [self.explorerDelegate setContentDescription:@""];
                }
        
                dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^(void){
                    //Background Thread
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        //Run UI Updates
                        [self.backgroundImage sd_setImageWithURL:[NSURL URLWithString:content.imgUrl] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                            
                            [self.backgroundImage setImage:[SFHelper blurredImageWithImage:image]];
                            
                            self.backgroundImage.contentMode = UIViewContentModeScaleAspectFill;
                            self.backgroundImage.clipsToBounds = YES;
                            
                            CIContext *context = [CIContext contextWithOptions:nil];
                            CIImage *inputImage = [[CIImage alloc] initWithImage:self.backgroundImage.image]; //your input image
                            
                            CIFilter *filter= [CIFilter filterWithName:@"CIColorControls"];
                            [filter setValue:inputImage forKey:@"inputImage"];
                            [filter setValue:[NSNumber numberWithFloat:-0.05] forKey:@"inputBrightness"];
                            
                            // Your output image
                            [self.backgroundImage setImage:[UIImage imageWithCGImage:[context createCGImage:filter.outputImage fromRect:filter.outputImage.extent]]];
                        }];
                    });
                });
                
                if (self.myPlayers.count < self.indexConten){
                    [self.view makeToastActivity:CSToastPositionCenter];
                    __weak typeof(self) weakSelf = self;
                    [self.story setPlayersWithCompletion:^{
                        
                        [weakSelf.view hideToastActivity];
                        [weakSelf playAtIndex:weakSelf.indexConten];
                        weakSelf.player = weakSelf.customPlayer;
                        [weakSelf.player play];
                    }];
                    
                }else{
                    [self playAtIndex:self.indexConten];
                    self.player = self.customPlayer;
                    [self.player play];
                }
                
                if(self.explorerDelegate && [self.explorerDelegate respondsToSelector:@selector(showSwipeUpView:)])
                    [self.explorerDelegate showSwipeUpView:YES];
                [self starTimer];
                
                
                StayfilmApp *sfApp = [StayfilmApp sharedStayfilmAppSingleton];
        
                if(content.idGenre != nil) {
                    if(self.explorerDelegate && [self.explorerDelegate respondsToSelector:@selector(storyButtonEnable:)])
                        [self.explorerDelegate storyButtonEnable:YES];
                    [sfApp.storyState setCurrentViewedIDGenre:[content.idGenre intValue] andIDTemplate:((content.idTemplate != nil)? [content.idTemplate intValue] : -1)];
                } else {
                    if(self.explorerDelegate && [self.explorerDelegate respondsToSelector:@selector(storyButtonEnable:)])
                        [self.explorerDelegate storyButtonEnable:NO];
                    [sfApp.storyState setCurrentViewedIDGenre:-1 andIDTemplate:-1];
                }
                
                if(self.explorerDelegate && [self.explorerDelegate respondsToSelector:@selector(changeStoryButtonText:Color:andIcon:)])
                    [self.explorerDelegate changeStoryButtonText:content.buttonText Color:[SFHelper colorFromHexString:content.buttonColor] andIcon:content.buttonIcon];
            } else {
                [self.timer invalidate];
            }
            
        }else{
            
            self.player = nil;
            
            [self setStoryDuration:[content.duration floatValue]];
            
            [self.view makeToastActivity:CSToastPositionCenter];
            
            isImage = YES;
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^(void){
                //Background Thread
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    //Run UI Updates
                    
                    if (content.contentDescription != nil){
                        if(self.explorerDelegate && [self.explorerDelegate respondsToSelector:@selector(setContentDescription:)])
                            [self.explorerDelegate setContentDescription:content.contentDescription];
                    }else{
                        if(self.explorerDelegate && [self.explorerDelegate respondsToSelector:@selector(setContentDescription:)])
                            [self.explorerDelegate setContentDescription:@""];
                    }
                    
                    [self.backgroundImage sd_setImageWithURL:[NSURL URLWithString: content.imgUrl] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                        [self.backgroundImage setImage:image];
                        
                        self.backgroundImage.contentMode = UIViewContentModeScaleAspectFill;
                        self.backgroundImage.clipsToBounds = YES;
                        
                        CIContext *context = [CIContext contextWithOptions:nil];
                        CIImage *inputImage = [[CIImage alloc] initWithImage:self.backgroundImage.image]; //your input image
                        
                        CIFilter *filter= [CIFilter filterWithName:@"CIColorControls"];
                        [filter setValue:inputImage forKey:@"inputImage"];
                        [filter setValue:[NSNumber numberWithFloat:-0.05] forKey:@"inputBrightness"];
                        
                        // Your output image
                        [self.backgroundImage setImage:[UIImage imageWithCGImage:[context createCGImage:filter.outputImage fromRect:filter.outputImage.extent]]];
                        
                        [self.view hideToastActivity];
                        [self starTimer];
                    }];
                });
            });
            StayfilmApp *sfApp = [StayfilmApp sharedStayfilmAppSingleton];
            
            if(self.isViewLoaded && self.isStillAvailable)
            {
                if(content.idGenre != nil) {
                    if(self.explorerDelegate && [self.explorerDelegate respondsToSelector:@selector(storyButtonEnable:)])
                        [self.explorerDelegate storyButtonEnable:YES];
                    
                    if(self.explorerDelegate && [self.explorerDelegate respondsToSelector:@selector(changeStoryButtonText:Color:andIcon:)])
                        [self.explorerDelegate changeStoryButtonText:content.buttonText Color:[SFHelper colorFromHexString:content.buttonColor] andIcon:content.buttonIcon];
                    
                    [sfApp.storyState setCurrentViewedIDGenre:[content.idGenre intValue] andIDTemplate:((content.idTemplate != nil)? [content.idTemplate intValue] : -1)];
                } else {
                    if(self.explorerDelegate && [self.explorerDelegate respondsToSelector:@selector(storyButtonEnable:)])
                        [self.explorerDelegate storyButtonEnable:NO];
                    [sfApp.storyState setCurrentViewedIDGenre:-1 andIDTemplate:-1];
                }
                
                if(self.explorerDelegate && [self.explorerDelegate respondsToSelector:@selector(showSwipeUpView:)])
                    [self.explorerDelegate showSwipeUpView:NO];
            } else {
                [self.timer invalidate];
            }
        
        }
    
    }@catch (NSException *exception)
    {
        NSLog(@"StayLog: SHOW CONTENT EXCEPTION: %@", exception.description);
        [self.view hideToastActivity];
    }
    
    
}
- (void)playAtIndex:(NSInteger)index
{
    self.player = nil;
    [_customPlayer removeAllItems];
    for (int i = (int)index; i <_myPlayers.count; i ++) {
        AVPlayerItem* obj = [_myPlayers objectAtIndex:i];
        if ([_customPlayer canInsertItem:obj afterItem:nil]) {
            [obj seekToTime:kCMTimeZero];
            [_customPlayer insertItem:obj afterItem:nil];
        }
    }
}

#pragma mark - Gesture Action

-(void)swipeDownGesture{
    if(isExiting) {
        return;
    }
    isExiting = YES;
    self.isStillAvailable = NO;
    
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:[self getAnalyticString]
                                                          action:@"close"
                                                           label:@"swipe-down"
                                                           value:@1] build]];
    
    [self.timer invalidate];
    self.timer = nil;
    [self resetPlayerObsevers];
//    [self.sfApp clearGalleryPlayerObservers];
    if(self.explorerDelegate && [self.explorerDelegate respondsToSelector:@selector(swipeDown)]) {
        [self.explorerDelegate swipeDown];
    }
}

-(void)swipeUpGesture{
    
    [self.player pause];
    
    if (!isImage){
        
        @try{
//            [self.player removeTimeObserver:self.playerObserver];
//            [self.player.currentItem  removeObserver:self forKeyPath:@"playbackBufferEmpty"];
//            self.playerObserver = nil;
        }@catch(NSException *ex){
            
            NSLog(@"StayLog: EXCEPTION OBSERVER swipeupgesture with error:%@",ex.description);
        }
        
        StoryContent *content = self.storiesContent[_indexConten];
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue]  < 11.0 ){
            
            AVPlayerViewController *playerVC = [[AVPlayerViewController alloc]init];
            playerVC.player = [AVPlayer playerWithURL:[NSURL URLWithString:content.videoUrl]];
            [playerVC setShowsPlaybackControls:YES];
            playerVC.player.automaticallyWaitsToMinimizeStalling = NO;
            [playerVC.player play];
            [self presentViewController:playerVC animated:YES completion:nil];
            
        }else{
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"Player" owner:self options:nil];
            PlayerViewController *moviePlayerViewController = [topLevelObjects objectAtIndex:0];
            moviePlayerViewController.player.automaticallyWaitsToMinimizeStalling = NO;
            moviePlayerViewController.currentMovie = nil;
            moviePlayerViewController.comeFromStory = YES;
            moviePlayerViewController.playerExplorerDelegate = self;
            moviePlayerViewController.showsPlaybackControls = YES;
            
            moviePlayerViewController.player = [AVPlayer playerWithURL:[NSURL URLWithString:content.videoUrl]];
            
//            moviePlayerViewController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            
            moviePlayerViewController.view.frame = self.view.frame;
            
            [self presentViewController:moviePlayerViewController animated:YES completion:nil];
        }
    }
    
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:[self getAnalyticString]
                                                          action:@"swipe-up"
                                                           label:@""
                                                           value:@1] build]];
}

-  (void)handleLongPress:(UILongPressGestureRecognizer*)sender {
    if (sender.state == UIGestureRecognizerStateEnded) {
//        NSLog(@"UIGestureRecognizerStateEnded");
        if (self.player != nil){
            isLongPress = NO;
            [self.player play];
        }else{
            [self starTimer];
        }
    }
    else if (sender.state == UIGestureRecognizerStateBegan){
//        NSLog(@"UIGestureRecognizerStateBegan.");
        if ( self.player != nil){
            isLongPress = YES;
            [self.player pause];
        }else{
             [self.timer invalidate];
        }
    }
}
-(void)dobleTapGesture{
//    NSLog(@"DOUBLE TAP");
}

#pragma mark - Player Explore Delegate

- (void)playStoryAgain{
    
    if (self.player != nil){
    
        [self.player play];
        [self starTimer];
    }
    
}


#pragma mark - StayfilmApp Delegates

-(void)sfSingletonCreated {
    // nothing
}
-(void)connectivityChanged:(BOOL)isConnected {
    if(!isConnected) {
        // pause story
        if ( self.player != nil){
            isLongPress = YES;
            [self.player pause];
        }else{
            [self.timer invalidate];
        }
        
        [self.explorerDelegate changedConnectivity:NO];
//        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"NO_INTERNET_CONNECTION", nil)
//                                                                       message:NSLocalizedString(@"NO_INTERNET_CONNECTION_MESSAGE", nil)
//                                                                preferredStyle:UIAlertControllerStyleAlert];
//        
//        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
//                                                              handler:^(UIAlertAction * action) {}];
//        
//        [alert addAction:defaultAction];
//        [self presentViewController:alert animated:YES completion:nil];
    } else {
        // resume story
        if (self.player != nil){
            isLongPress = NO;
            [self.player play];
        }else{
            [self starTimer];
        }
        
        [self.explorerDelegate changedConnectivity:YES];
    }
}


@end
