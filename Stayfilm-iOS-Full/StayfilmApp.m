 
//
//  StayfilmApp.m
//  Stayfilm for Messenger
//
//  Created by Henrique Ormonde on 11/05/15.
//  Copyright (c) 2015 Stayfilm. All rights reserved.
//

#import "StayfilmApp.h"
#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "Reachability.h"
#import "ProgressViewController.h"
#import "AppDelegate.h"
#import "CommonCrypto/CommonDigest.h"
#import "Uploader.h"


@interface StayfilmApp ()<SKProductsRequestDelegate>

@property (nonatomic, strong) Uploader *uploader;

@end

@implementation StayfilmApp

@synthesize sfConfig, wsConfig, settings, currentUser, storyState, imagesCache, selectedMedias, finalMedias, selectedGenre, selectedTitle, selectedTemplate, uploadedAlbum, isEditingMode, operationsQueue;


static StayfilmApp *singletonObject = nil;

+ (StayfilmApp *) sharedStayfilmAppSingletonWithDelegate:(id<StayfilmSingletonDelegate>)theDelegate
{
    @try {
        @synchronized(self)
        {
            if(singletonObject == nil)
            {
                singletonObject = [[self alloc] init];
            }
            singletonObject.delegate = theDelegate;
         
            [(NSObject *)singletonObject.delegate performSelectorOnMainThread:@selector(sfSingletonCreated) withObject:nil waitUntilDone:NO];
        }
        return singletonObject;
    }
    @catch (NSException * ex) {
        NSLog(@"StayLog: EXCEPTION in sharedStayfilmAppSingletonWithDelegate with error: %@", ex.description);
        return nil;
    }
}


+ (StayfilmApp *) sharedStayfilmAppSingleton
{
    @try {
        @synchronized(self)
        {
            if(singletonObject == nil)
            {
                singletonObject = [[self alloc] init];
            }
        }
        return singletonObject;
    }
    @catch (NSException * ex) {
        NSLog(@"StayLog: EXCEPTION in sharedStayfilmAppSingleton with error: %@", ex.description);
        return nil;
    }
}

- (id)init
{
    if(self = [super init])
    {
        self.isDebugMode = NO;   // DEBUG MODE
        self.sfConfig = nil;
        self.wsConfig = [[WSConfig alloc] init];
        self.settings = nil;
        self.currentUser = nil;
        self.storyState = [StoryState sharedStoryStateSingleton];
        self.imagesCache = [[NSCache alloc] init];
        self.selectedMedias = [[NSMutableArray alloc] init];
        self.finalMedias = [[NSMutableArray alloc] init];
        self.selectedTitle = nil;
        self.selectedGenre = nil;
        self.selectedTemplate = nil;
        self.uploadedAlbum = nil;
        self.editedMedias = [[NSMutableArray alloc] init];
        self.editedGenre = nil;
        self.editedTemplate = nil;
        self.editedTitle = nil;
        self.deletedFilms = [[NSMutableArray alloc] init];
        self.operationsQueue = [NSOperationQueue new];
        self.operationsQueue.name = @"StayfilmApp Process Queue";
        self.operationsQueue.maxConcurrentOperationCount = 1;
        self.operationsQueue.qualityOfService = NSOperationQualityOfServiceUserInitiated;
        self.selectedMediasToAutoFilm = [[NSMutableArray alloc] init];
        self.selectedTitleToAutoFilm = nil;
        
//        self.selectedOrientation = HORIZONTAL;
//        self.editedOrientation = HORIZONTAL;
        
        self.playerArray = [[NSMutableArray alloc] init];
        self.playerObserverArray = [[NSMutableArray alloc] init];
        
        
        if(SKPaymentQueue.canMakePayments){
            
            NSSet * dataSet = [[NSSet alloc] initWithObjects:IAP_MONTHLY_SUBSCRIPTION, nil];
            
            SKProductsRequest *productRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:dataSet];
            
            productRequest.delegate = self;
            [productRequest start];
        }
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:) name:kReachabilityChangedNotification object:nil];
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus networkStatus = [reachability currentReachabilityStatus];
        if (networkStatus == NotReachable) {
            return nil;
        }
        
        [[NSNotificationCenter defaultCenter] addObserverForName:FBSDKAccessTokenDidChangeNotification
                                                          object:nil
                                                           queue:[NSOperationQueue mainQueue]
                                                      usingBlock:
         ^(NSNotification *notification) {
             if (notification.userInfo[FBSDKAccessTokenDidChangeUserID]) {
                 // Handle user change
                 if(self.settings != nil) {
                     if(self.settings[@"FB_id"] != nil && ![self.settings[@"FB_id"] isEqualToString:[[FBSDKAccessToken currentAccessToken] userID]]) {
                         [self logout];
                     }
                 }
             }
         }];
        
        @try {
            self.settings = [[[NSUserDefaults standardUserDefaults] dictionaryForKey:@"settings"] mutableCopy];
            if(self.settings == nil)
            {
                self.settings = [[NSMutableDictionary alloc] init];
            }
            if([[NSUserDefaults standardUserDefaults] objectForKey:@"hash_config"] != nil)
            {
                StayWS *ws = [[StayWS alloc] init];
                NSDictionary *response = nil;
                NSArray *pathArgs = nil;
                NSDictionary *args = nil;
                
                NSString * hash = [[NSUserDefaults standardUserDefaults] stringForKey:@"hash_config"];

//                NSString *langID = [[NSLocale preferredLanguages] objectAtIndex:0];
//                NSLocale *local = [NSLocale localeWithLocaleIdentifier:@"EN"];
//                NSString * language = [[local displayNameForKey:NSLocaleLanguageCode value:langID]lowercaseString];
//
//                args = @{ @"device" : @"ios-native",
//                          @"hash_config" : hash,
//                          @"language" : language };
                
                args = @{ @"device" : @"ios-native",
                          @"hash_config" : hash };
                
                response = [ws requestWebServiceWithURL:[self.wsConfig getConfigPathWithKey:@"getConfig"] withRequestType:@"GET" withPathArguments:pathArgs withArguments:args withIdSession:nil withCache:NO];
                
                NSHTTPURLResponse *statusResponse = (NSHTTPURLResponse *) ws.responseMessage;
                if(statusResponse.statusCode == 304)  // same config
                {
                    // this next line will load saved config into memory
                    BOOL readCfg = [self getCurrentConfig];
                    if(!readCfg) { // just in case SFConfig is not set
                        self.sfConfig = [SFConfig getAllConfigsFromWS:self.wsConfig];
                    }
                }
                else if(response != nil && (statusResponse.statusCode == 200 || statusResponse.statusCode == 201)) {
                    // different config on server
                    
                    NSMutableDictionary *conf = [[NSMutableDictionary alloc] init];
                    //get ios-native config
                    self.sfConfig = [[SFConfig alloc] init];
                    if(response[@"general"] != nil)
                    {
                        self.sfConfig.generalConfig = response[@"general"];
                        [conf setObject:response[@"general"] forKey:@"general"];
                        
                        if(response[@"general"][@"enable_push_geo"] != nil)
                        {
                            [conf setObject:response[@"general"][@"enable_push_geo"] forKey:@"enable_push_geo"];
                            self.sfConfig.push_enable_geo = response[@"general"][@"enable_push_geo"];
                        }
                        if(response[@"general"][@"min_upload_photo"] != nil)
                        {
                            [conf setObject:response[@"general"][@"min_upload_photo"] forKey:@"min_upload_photo"];
                            self.sfConfig.min_photos = response[@"general"][@"min_upload_photo"];
                        }
                        if(response[@"general"][@"max_photo"] != nil)
                        {
                            [conf setObject:response[@"general"][@"max_photo"] forKey:@"max_photo"];
                            self.sfConfig.max_photos = response[@"general"][@"max_photo"];
                        }else{
                            // Manual set the maximum photos
                             [conf setObject:[NSNumber numberWithInteger:100] forKey:@"max_photo"];
                            self.sfConfig.max_photos = [NSNumber numberWithInteger:100];
                        }
                    }
                    if(response[@"moviemaker"] != nil)
                    {
                        if(response[@"moviemaker"][@"genres"] != nil)
                        {
                            [conf setObject:response[@"moviemaker"][@"genres"] forKey:@"moviemaker_genres"];
                            
                            self.sfConfig.genres = [[NSMutableArray alloc] init];
                            for(NSDictionary *var in response[@"moviemaker"][@"genres"]) {
                                [self.sfConfig.genres addObject:[Genres customClassWithProperties:var]];
                            }
                            [self.sfConfig getTemplates];
                        }
                    }
                    if(response[@"moviemaker"] != nil && response[@"general"] != nil) {
                        NSData *dataSave = [NSKeyedArchiver archivedDataWithRootObject:conf];
                        [[NSUserDefaults standardUserDefaults] setObject:dataSave forKey:@"SFConfig"];

                        NSString * md5string = [[NSString alloc] initWithData:ws.responseBody   encoding:NSUTF8StringEncoding];
                        NSString *md5 = [StayfilmApp MD5String:md5string];
                        [[NSUserDefaults standardUserDefaults] setObject:md5 forKey:@"hash_config"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                    }
                }
                else // request gone wrong
                {
                    [self performSelectorInBackground:@selector(readSFConfig) withObject:nil];
                }
            }
            else { // does not have hash_config set yet (first time opening app)
                self.sfConfig = [SFConfig getAllConfigsFromWS:self.wsConfig];
            }
        }
        @catch (NSException *exception) {
            self.sfConfig = nil;
            self = nil;
        }
    }
    return self;
}

- (BOOL)getCurrentConfig {
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"SFConfig"] != nil)
    {
        self.sfConfig = [[SFConfig alloc] init];
        NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"SFConfig"];
        NSDictionary *conf = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        
        if(conf[@"general"] != nil) {
            self.sfConfig.generalConfig = conf[@"general"];
        }
        if(conf[@"moviemaker_genres"] != nil) {
            self.sfConfig.genres = [[NSMutableArray alloc] init];
            for(NSDictionary *var in conf[@"moviemaker_genres"]) {
                [self.sfConfig.genres addObject:[Genres customClassWithProperties:var]];
            }
            [self.sfConfig getTemplates];
        }
        if(conf[@"min_upload_photo"] != nil) {
            self.sfConfig.min_photos = conf[@"min_upload_photo"];
        }
        if(conf[@"max_photo"] != nil) {
            self.sfConfig.max_photos = conf[@"max_photo"];
        } else{
            self.sfConfig.max_photos = [NSNumber numberWithInteger:100];
        }
        if(conf[@"enable_push_geo"]) {
            self.sfConfig.push_enable_geo = conf[@"enable_push_geo"];
        }
        return YES;
    }
    return NO;
}

- (void)reachabilityDidChange:(NSNotification *)notification {
    Reachability *reachability = (Reachability *)[notification object];
    //Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    
    if (networkStatus != NotReachable) {
        NSLog(@"StayLog: Network Reachable");
        if(self.delegate != nil && [self.delegate respondsToSelector:@selector(connectivityChanged:)]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.delegate connectivityChanged:YES];
            });
        }
    } else {
        NSLog(@"StayLog: Network Unreachable");
        if(self.delegate != nil && [self.delegate respondsToSelector:@selector(connectivityChanged:)]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.delegate connectivityChanged:NO];
            });
        }
    }
}


#pragma mark - Operation Queue Settings

- (void)setOperationsQueueMaxCuncurrency:(int)p_max {
    self.operationsQueue.maxConcurrentOperationCount = p_max;
}
- (void)setOperationsQueueQoS:(NSQualityOfService)p_quality {
    self.operationsQueue.qualityOfService = p_quality;
}


#pragma mark - Stayfilm Methods


+ (NSString *)MD5String:(NSString*)input {
    const char* str = [input UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(str, (CC_LONG)strlen(str), result);
    
    NSMutableString *ret = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH*2];
    for(int i = 0; i<CC_MD5_DIGEST_LENGTH; i++) {
        [ret appendFormat:@"%02x",result[i]];
    }
    return ret;
}

- (void)readSFConfig
{
    self.sfConfig = [SFConfig getAllConfigsFromWS:self.wsConfig];
}

- (id)reloadSettings
{
    @try {
        self.settings = [[[NSUserDefaults standardUserDefaults] dictionaryForKey:@"settings"] mutableCopy];
    }
    @catch (NSException *exception) {
        NSLog(@"StayLog: Error reloading settings: %@",exception.description);
    }
}

- (BOOL)isLoggedIn
{
    if(self.currentUser != nil && [self.currentUser.idSession length] != 0)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

- (void)setEditingMode:(BOOL)isEditing {
    self.isEditingMode = isEditing;
    [self.filmStrip setEditingMode:isEditing];
}

+(BOOL)isArray:(NSArray*)array1 equalToArray:(NSArray *)array2
{
//    NSSet *set1 = [NSSet setWithArray:array1];
//    NSSet *set2 = [NSSet setWithArray:array2];
//
//    if ([set1 isEqualToSet:set2] && array1.count == array2.count) {
//        return YES;
//    }
//    return NO;
    if(array1 == nil && array2 == nil) {
        return YES;
    } else if(array1 == nil || array2 == nil) {
        return NO;
    }
    if(array1.count != array2.count) {
        return NO;
    }
    
    BOOL equal = YES;
    for(int x=0; x < array1.count; x++) {
        if([array1 objectAtIndex:x] != [array2 objectAtIndex:x]) {
            equal = NO;
            break;
        }
    }
    return equal;
}

- (void)saveSettings
{
    @synchronized(self) {
        @try {
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"settings"];
            [[NSUserDefaults standardUserDefaults] setObject:self.settings forKey:@"settings"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        @catch (NSException *exception) {
            NSLog(@"Exception while saving settings: %@", exception.description);
        }
    }
}

- (int)loginUserWithFacebook
{
    @try {
        if(self.sfConfig != nil)
        {
            User *appUser = [User loginWithFacebook:self.settings[@"facebookToken"] andSFAppSingleton:self]; // already saves to sfApp.settings
            
            UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
            
            while (topController.presentedViewController) {
                topController = topController.presentedViewController;
            }
            
            if(appUser == nil)
            {
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CONNECTION_ERROR", nil)
                                                                               message:NSLocalizedString(@"CONNECTION_ERROR_MESSAGE", nil)
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {}];
                
                [alert addAction:defaultAction];
                [topController presentViewController:alert animated:YES completion:nil];
                NSLog(@"StayLog: connection error 5");
                //exit(0);
                return 0;
            }
            else if(!appUser.userExists && !appUser.isResponseError) // register user
            {
                return -1;
            }
            else if(!appUser.isLogged)
            {
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"LOGIN_FAILED", nil)
                                                                               message:NSLocalizedString(@"LOGIN_FAILED_MESSAGE", nil)
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {}];
                
                [alert addAction:defaultAction];
                [topController presentViewController:alert animated:YES completion:nil];
                NSLog(@"StayLog: connection error 6");
                //exit(0);
                return 0;
            }
            else //user logged in successfully
            {
                NSData *dataSave = [NSKeyedArchiver archivedDataWithRootObject:appUser];
                [[NSUserDefaults standardUserDefaults] setObject:dataSave forKey:@"currentUser"];
                self.currentUser = appUser;
                [self.settings setObject:[[FBSDKAccessToken currentAccessToken] userID] forKey:@"FB_id"];
                [self.settings setObject:[[FBSDKAccessToken currentAccessToken] tokenString] forKey:@"facebookToken"];
                // check Login   0 = not started  1 = true   2 = false
                [self.settings setObject:@"1" forKey:@"CHECK_Login"];
                [self saveSettings];
                
                return 1;
            }
        }
        else
        {
            UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
            
            while (topController.presentedViewController) {
                topController = topController.presentedViewController;
            }
            
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CONNECTION_ERROR", nil)
                                                                           message:NSLocalizedString(@"CONNECTION_ERROR_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [topController presentViewController:alert animated:YES completion:nil];
            NSLog(@"StayLog: connection error 7");
            // exit(0);
            return 0;
        }

    }
    @catch (NSException *exception) {
        NSLog(@"StayLog: EXCEPTION login Facebook with error: %@", exception.description);
    }
}

- (BOOL)deleteMovieFromDevice:(NSString*)idMovie
{
    @try
    {
        NSString* filePath;
        
        if (idMovie != nil && [idMovie length] > 0)
        {
            NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString  *documentsDirectory = ([paths objectAtIndex:0] != nil)? [paths objectAtIndex:0] : nil;
            
            NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/StayFilms"];
            
            //Check StayfilmMessenger folder if it does exists
            NSError *error;
            if ([[NSFileManager defaultManager] fileExistsAtPath:dataPath])
            {
                filePath = [NSString stringWithFormat:@"%@/%@", dataPath, [idMovie stringByAppendingString:@".mp4"]];
                //Checks if file exists
                if ([[NSFileManager defaultManager] fileExistsAtPath:filePath])
                {
                    [[NSFileManager defaultManager] removeItemAtPath:filePath error:&error]; //Delete File
                }
            }
            return YES;
        }
        else
        {
            return NO;
        }
    }
    @catch (NSException *exception)
    {
        NSLog(@"StayLog: EXCEPTION in deleteMovieFromDevice with error: %@",exception.description);
        return NO;
    }
    
}

- (UIViewController*)topViewController {
    return [self topViewControllerWithRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}
- (UIViewController*)topViewControllerWithRootViewController: (UIViewController*)rootViewController
{
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController* tabBarController = (UITabBarController*)rootViewController;
        return [self topViewControllerWithRootViewController:tabBarController.selectedViewController];
    } else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController* navigationController = (UINavigationController*)rootViewController;
        return [self topViewControllerWithRootViewController:navigationController.visibleViewController];
    } else if (rootViewController.presentedViewController) {
        UIViewController* presentedViewController = rootViewController.presentedViewController;
        return [self topViewControllerWithRootViewController:presentedViewController];
    } else {
        return rootViewController;
    }
}

- (BOOL)logout {
    //@synchronized(self) {
        @try {
            
            [self cleanSettings];
            [self.settings removeAllObjects];
            [self saveSettings];
            [self cleanRemakeState];
            self.currentUser = nil;
            for (Story* story in self.sfConfig.stories) { // clear current viewState session
                story.isStoryViewed = NO;
                story.viewedIDContent = nil;
               // story.viewedIndex = -1;
            }
            if([[NSUserDefaults standardUserDefaults] objectForKey:@"StoryState"] != nil) {
                [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"StoryState"];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"StoryState"];
            }
            if([[NSUserDefaults standardUserDefaults] objectForKey:@"currentUser"] != nil)
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"currentUser"];
            [self.storyState resetStories];
            
            if([FBSDKAccessToken currentAccessToken]) {
                [FBSDKProfile setCurrentProfile:nil];
                [FBSDKAccessToken setCurrentAccessToken:nil];
                FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
                [loginManager logOut];
            }
            
            AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            [appdelegate goToLoginPage];
            
            return YES;
        } @catch(NSException *ex) {
            NSLog(@"StayLog: EXCEPTION in logout with error: %@", ex.description);
            return NO;
        }
    //}
}

BOOL isClearingObservers = NO;
- (void)clearGalleryPlayerObservers {
    if(isClearingObservers)
        return;
    isClearingObservers = YES;
    if(self.playerObserverArray.count > 0) {
        for (int i=0; i<self.playerObserverArray.count; ++i) {
            @try {
                [((AVQueuePlayer*)self.playerArray[i]) pause];
                [((AVQueuePlayer*)self.playerArray[i]) removeTimeObserver:self.playerObserverArray[i]];
                
            } @catch (NSException *exception) {
                NSLog(@"StayLog: EXCEPTION TIME OBSERVER clearGalleryPlayerObservers :%@", exception.description);
            }
        }
        [self.playerObserverArray removeAllObjects];
        [self.playerArray removeAllObjects];
    }
    isClearingObservers = NO;
}

- (void)cleanSettings
{
    [self.settings removeObjectForKey:@"LOGGEDIN_Email"];
    [self.settings setObject:@"" forKey:@"facebookToken"];
    [self.settings setObject:@"" forKey:@"FB_id"];
    [self.settings setObject:@"" forKey:@"CHECK_Login"];
    [self.settings setObject:@"" forKey:@"idSession"];
    [self.settings setObject:@"" forKey:@"idUser"];
    [self.settings setObject:@"" forKey:@"USER_Photo"];
    [self.settings setObject:@"" forKey:@"USER_Name"];
    [self saveSettings];
}

- (void)cleanRemakeState
{
    self.selectedMedias = nil;
    self.uploadedAlbum = nil;
    self.selectedGenre = nil;
    self.selectedTitle = nil;
    self.selectedTemplate = nil;
    self.finalMedias = nil;
    self.editedMedias = nil;
    self.editedGenre = nil;
    self.editedTemplate = nil;
    self.editedTitle = nil;
    
    self.selectedMedias = [[NSMutableArray alloc] init];
    self.finalMedias = [[NSMutableArray alloc] init];
    self.editedMedias = [[NSMutableArray alloc] init];
    
    self.selectedMediasToAutoFilm = [[NSMutableArray alloc] init];
    self.selectedTitleToAutoFilm = nil;
    self.isAutomaticMode = NO;
    self.isFromExplorer = NO;
    
    self.uploader = [Uploader sharedUploadStayfilmSingleton];
    if(self.uploader.listUploadMedias.count > 0) {
        [self.uploader resetUpload];
    }
    
    [self cleanFilmFolder];
}

- (BOOL)cleanFilmFolder
{
    // Clean uploaded Temp files
    @try {
        NSString* tempPath = [NSTemporaryDirectory()stringByStandardizingPath];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:tempPath])
        {
            for (NSString *file in [[NSFileManager defaultManager] contentsOfDirectoryAtPath:tempPath error:nil])
            {
                if([file containsString:@"sf_video_"] || [file containsString:@"sf_photo_"])
                {
                    [[NSFileManager defaultManager] removeItemAtPath:[tempPath stringByAppendingString:[[NSString alloc] initWithFormat:@"/%@",file]] error:nil];
                }
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"StayLog: EXCEPTION on clearTempFiles with error: %@", exception.description);
    }
    
    @try // Clean saved videos.
    {
        int maxMovieCache = 10;
//        if(self.settings[@"MAX_MOVIE_CACHE"] != nil)
//        {
//            movieCount = (int)self.settings[@"MAX_MOVIE_CACHE"];
//        }
        
        NSFileManager *fm = [NSFileManager defaultManager];
        NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = ([paths objectAtIndex:0] != nil)? [[paths objectAtIndex:0] stringByAppendingPathComponent:@"/StayFilms"] : nil;
        NSError *error = nil;
        
        if ([fm fileExistsAtPath:documentsDirectory])
        {
            for (NSString *file in [fm contentsOfDirectoryAtPath:documentsDirectory error:&error])
            {
                NSDictionary *attributes = [fm attributesOfItemAtPath:[NSString stringWithFormat:@"%@/%@", documentsDirectory, file] error:nil];
                NSDate *fileCreated = attributes.fileModificationDate;
                
                NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
                NSInteger startDay = [calendar ordinalityOfUnit:NSCalendarUnitDay
                                                         inUnit:NSCalendarUnitEra
                                                        forDate:fileCreated];
                NSInteger endDay = [calendar ordinalityOfUnit:NSCalendarUnitDay
                                                       inUnit:NSCalendarUnitEra
                                                      forDate:[NSDate date]];
                if(labs(endDay-startDay) >= 1)
                {
                    BOOL success = [fm removeItemAtPath:[NSString stringWithFormat:@"%@/%@", documentsDirectory, file] error:&error];
                    if (!success || error) {
                        return NO;
                    }
                }
            }
        }
        
        //Delete oldest file until number of films is equal movieCount
        while ([[fm contentsOfDirectoryAtPath:documentsDirectory error:nil] count] > maxMovieCache)
        {
            NSArray *files = [fm contentsOfDirectoryAtPath:documentsDirectory error:nil];
            NSString *oldestFile = nil;
            NSDate *oldestDate = [NSDate date];
            for (NSString *file in files)
            {
                NSDictionary *attributes = [fm attributesOfItemAtPath:[NSString stringWithFormat:@"%@/%@", documentsDirectory, file] error:nil];
                NSDate *fileCreated = attributes.fileCreationDate;
                if(fileCreated == nil)
                {
                    fileCreated = attributes.fileModificationDate;
                }
                
                if ([oldestDate earlierDate:fileCreated] == fileCreated)
                {
                    oldestFile = file;
                    oldestDate = fileCreated;
                }
            }
            if(oldestFile != nil)
            {
                BOOL success = [fm removeItemAtPath:[NSString stringWithFormat:@"%@/%@", documentsDirectory, oldestFile] error:&error];
                if (!success || error) {
                    return NO;
                }
            }
        }
        
        return YES;
    }
    @catch (NSException *exception)
    {
        NSLog(@"StayLog: EXCEPTION StayfilmAppSingleton cleanFilmFolder exception with error %@", exception.description);
        return NO;
    }
}

void runOnMainQueueWithoutDeadlocking(void (^block)(void))
{
    if ([NSThread isMainThread])
    {
        block();
    }
    else
    {
        dispatch_sync(dispatch_get_main_queue(), block);
    }
}

//Purchase

-(void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response{
    
    if (response.products.count != 0) {
        self.product = response.products[0];
    }else{
        NSLog(@"There are no products.");
    }
}

@end


