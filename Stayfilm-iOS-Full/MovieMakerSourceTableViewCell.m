//
//  MovieMakerSourceTableViewCell.m
//  Stayfilm-iOS-Full
//
//  Created by Henrique Ormonde on 21/09/17.
//  Copyright © 2017 Stayfilm. All rights reserved.
//

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "MovieMakerSourceTableViewCell.h"
#import "AlbumSocialCollectionViewCell.h"
#import "StayfilmApp.h"
#import "AlbumSN.h"

@implementation IndexedCollectionView

@end


@interface MovieMakerSourceTableViewCell ()

@property (nonatomic, weak) StayfilmApp *sfAppSourceTable;

@end

@implementation MovieMakerSourceTableViewCell

@synthesize albums, img_SocialSource, expandArrow, isExpanded, txt_Title, txt_showMoreLess, but_showMoreLess, albumCollection, sfAppSourceTable, thumbConnection, constraint_AlbumsHeight;

static NSString *cellIdentifier = @"AlbumSocialNetworkSourceItemCell";

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    if(isExpanded)
    {
//        //TODO ANIMATION hide expand arrow
//        [UIView animateWithDuration:0.35 animations:^{
//            self.expandArrow.alpha = 0;
//        } completion:^(BOOL finished) {
//            [self.expandArrow setHidden:YES];
//        }];
        
        //Animate table height
        [UIView animateWithDuration:0.35 animations:^{
            [self.superview sizeToFit];
        }];
        
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        self.thumbConnection = [NSURLSession sessionWithConfiguration:config];
    }
}

-(void)dealloc {
    [self.thumbConnection invalidateAndCancel];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
    if(!isExpanded)
    {
        [UIView animateWithDuration:0.35 animations:^{
            self.expandArrow.alpha = 1;
        }];
    }
}

- (nonnull __kindof UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath { 
    @try {
        AlbumSocialCollectionViewCell * cell = (AlbumSocialCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
        if(cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AlbumSocialCollectionViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        int index = (int)indexPath.item - 1;
        
        cell.txt_Title.text = [(AlbumSN *) [self.albums objectAtIndex:index] name];
        cell.txt_Total.text = [NSString stringWithFormat:@"%@", [(AlbumSN *)[self.albums objectAtIndex:index] count]];
        cell.imageBig.image = nil;
        cell.imageLeft = nil;
        cell.imageRight = nil;
        [cell.loaderSpinner startAnimating];
        [cell.loaderSpinner setHidden:NO];
        
        if(self.albums.count == 0) {
            [self.albumCollection setHidden:YES];
        }
        
        NSString *coverURL = [(AlbumSN *)[self.albums objectAtIndex:index] imageBIG];
        if(coverURL != nil)
        {
            if(self.sfAppSourceTable == nil)
                self.sfAppSourceTable = [StayfilmApp sharedStayfilmAppSingleton];
            
            if([self.sfAppSourceTable.imagesCache objectForKey:coverURL] != nil)
            {
                [cell.loaderSpinner stopAnimating];
                cell.imageBig.image = [self.sfAppSourceTable.imagesCache objectForKey:coverURL];
            }
            else
            {
                __weak typeof(self) selfDelegate = self;
                cell.task = [self.thumbConnection dataTaskWithURL:[NSURL URLWithString:coverURL] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    
                    if(error)
                    {
                        NSLog(@"StayLog: AlbumCollectionImage request error: %@", error);
                        
                        NSString *coverURLsecondRequest = [NSString stringWithFormat:@"%@", [(AlbumSN *)[selfDelegate.albums objectAtIndex:index] idAlbum]];
                        FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                                      initWithGraphPath:coverURLsecondRequest
                                                      parameters:@{@"fields": @"id, picture"}
                                                      HTTPMethod:@"GET"];
                        [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                                              id result,
                                                              NSError *error) {
                            if(error)
                            {
                                NSLog(@"StayLog: Error imageBIG request failed with error: %@", error.description);
                                [selfDelegate.albumCollection reloadData];
                            }
                            else if(result[@"picture"] != nil)
                            {
                                ((AlbumSN*)selfDelegate.albums[index]).imageBIG = result[@"picture"][@"data"][@"url"];
                                [cell.loaderSpinner stopAnimating];
                                
                                cell.imageBig.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:((AlbumSN*)selfDelegate.albums[index]).imageBIG]]];
                                [self.sfAppSourceTable.imagesCache setObject:cell.imageBig.image forKey:((AlbumSN*)selfDelegate.albums[index]).imageBIG];
                                [selfDelegate.albumCollection reloadData];
                            }
                        }];
                        
                    }
                    else {
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            [cell.loaderSpinner stopAnimating];
                            cell.imageBig.image = [UIImage imageWithData:data];
                            [self.sfAppSourceTable.imagesCache setObject:cell.imageBig.image forKey:((AlbumSN*)selfDelegate.albums[index]).imageBIG];
                        }];
                    }
                }];
                [cell.task resume];
            }
        }
        
        NSString *imgURL = [(AlbumSN *)[self.albums objectAtIndex:index] imageLEFT];
        if(imgURL != nil)
        {
            if(self.sfAppSourceTable == nil)
                self.sfAppSourceTable = [StayfilmApp sharedStayfilmAppSingleton];
            
            if([self.sfAppSourceTable.imagesCache objectForKey:imgURL] != nil)
            {
                [cell.loaderSpinner stopAnimating];
                cell.imageLeft.image = [self.sfAppSourceTable.imagesCache objectForKey:imgURL];
            }
            else
            {
                __weak typeof(self) selfDelegate = self;
                cell.task = [self.thumbConnection dataTaskWithURL:[NSURL URLWithString:imgURL] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    
                    if(error)
                    {
                        NSLog(@"StayLog: AlbumCollectionImage request error: %@", error);
                        
                        NSString *coverURLsecondRequest = [NSString stringWithFormat:@"/%@/photos", [(AlbumSN *)[selfDelegate.albums objectAtIndex:index] idAlbum]];
                        FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                                      initWithGraphPath:coverURLsecondRequest
                                                      parameters:@{@"fields": @"id, source, width, height, images"}
                                                      HTTPMethod:@"GET"];
                        [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                                              id result,
                                                              NSError *error) {
                            if(error)
                            {
                                NSLog(@"StayLog: Error imageLeft request failed with error: %@", error.description);
                                [selfDelegate.albumCollection reloadData];
                            }
                            else
                            {
                                @try {
                                    if(result[@"data"] != nil)
                                    {
                                        for(NSDictionary *var in result[@"data"]) {
                                            [[[selfDelegate.albums objectAtIndex:index] idMedias] addObject:[PhotoFB customClassWithProperties:var]];
                                            ((PhotoFB*)([[[selfDelegate.albums objectAtIndex:index] idMedias] lastObject])).idAlbum = [[selfDelegate.albums objectAtIndex:index] idAlbum];
                                        }
                                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                            if([[[selfDelegate.albums objectAtIndex:index] idMedias] count] > 0) // get lowest quality thumb for LEFT thumb image
                                            {
                                                ((AlbumSN*)selfDelegate.albums[index]).imageLEFT = ((ImageFB *)[((PhotoFB *)[[[selfDelegate.albums objectAtIndex:index] idMedias] objectAtIndex:0]).images lastObject]).source;
                                                cell.imageLeft.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:((AlbumSN*)selfDelegate.albums[index]).imageLEFT]]];
                                                [self.sfAppSourceTable.imagesCache setObject:cell.imageLeft.image forKey:((AlbumSN*)selfDelegate.albums[index]).imageLEFT];
                                                
                                                if([[[selfDelegate.albums objectAtIndex:index] idMedias] count] > 1) // get thumb for RIGHT image
                                                {
                                                    ((AlbumSN*)selfDelegate.albums[index]).imageRIGHT = ((ImageFB *)[((PhotoFB *)[[[selfDelegate.albums objectAtIndex:index] idMedias] objectAtIndex:1]).images lastObject]).source;
                                                    cell.imageRight.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:((AlbumSN*)selfDelegate.albums[index]).imageRIGHT]]];
                                                    [self.sfAppSourceTable.imagesCache setObject:cell.imageRight.image forKey:((AlbumSN*)selfDelegate.albums[index]).imageRIGHT];
                                                }
                                            }
                                            [selfDelegate.albumCollection reloadData];
                                        }];
                                    }
                                } @catch (NSException *ex) {
                                    NSLog(@"StayLog: EXCEPTION in MovieMakerSourceTable collectionViewcellForItemAtIndexPath with error: %@", ex.description);
                                }
                            }
                        }];
                        
                    }
                    else {
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            [cell.loaderSpinner stopAnimating];
                            cell.imageLeft.image = [UIImage imageWithData:data];
                            [self.sfAppSourceTable.imagesCache setObject:cell.imageLeft.image forKey:((AlbumSN*)selfDelegate.albums[index]).imageLEFT];
                        }];
                    }
                }];
                [cell.task resume];
            }
        }
        
        imgURL = [(AlbumSN *)[self.albums objectAtIndex:index] imageRIGHT];
        if(imgURL != nil)
        {
            if(self.sfAppSourceTable == nil)
                self.sfAppSourceTable = [StayfilmApp sharedStayfilmAppSingleton];
            
            if([self.sfAppSourceTable.imagesCache objectForKey:imgURL] != nil)
            {
                [cell.loaderSpinner stopAnimating];
                cell.imageRight.image = [self.sfAppSourceTable.imagesCache objectForKey:imgURL];
            }
            else
            {
                __weak typeof(self) selfDelegate = self;
                cell.task = [self.thumbConnection dataTaskWithURL:[NSURL URLWithString:imgURL] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    
                    if(error)
                    {
                        NSLog(@"StayLog: AlbumCollectionImage request error: %@", error);
                        
                        NSString *coverURLsecondRequest = [NSString stringWithFormat:@"/%@/photos", [(AlbumSN *)[selfDelegate.albums objectAtIndex:index] idAlbum]];
                        FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                                      initWithGraphPath:coverURLsecondRequest
                                                      parameters:@{@"fields": @"id, source, width, height, images"}
                                                      HTTPMethod:@"GET"];
                        [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                                              id result,
                                                              NSError *error) {
                            if(error)
                            {
                                NSLog(@"StayLog: Error imageLeft request failed with error: %@", error.description);
                                [selfDelegate.albumCollection reloadData];
                            }
                            else
                            {
                                @try {
                                    if(result[@"data"] != nil)
                                    {
                                        for(NSDictionary *var in result[@"data"]) {
                                            [[[selfDelegate.albums objectAtIndex:index] idMedias] addObject:[PhotoFB customClassWithProperties:var]];
                                            ((PhotoFB*)([[[selfDelegate.albums objectAtIndex:index] idMedias] lastObject])).idAlbum = [[selfDelegate.albums objectAtIndex:index] idAlbum];
                                        }
                                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                            if([[[selfDelegate.albums objectAtIndex:index] idMedias] count] > 1) // get thumb for RIGHT image
                                            {
                                                ((AlbumSN*)selfDelegate.albums[index]).imageRIGHT = ((ImageFB *)[((PhotoFB *)[[[selfDelegate.albums objectAtIndex:index] idMedias] objectAtIndex:1]).images lastObject]).source;
                                                cell.imageRight.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:((AlbumSN*)selfDelegate.albums[index]).imageRIGHT]]];
                                                [self.sfAppSourceTable.imagesCache setObject:cell.imageRight.image forKey:((AlbumSN*)selfDelegate.albums[index]).imageRIGHT];
                                            }
                                            [selfDelegate.albumCollection reloadData];
                                        }];
                                    }
                                } @catch (NSException *ex) {
                                    NSLog(@"StayLog: EXCEPTION in MovieMakerSourceTable collectionViewcellForItemAtIndexPath with error: %@", ex.description);
                                }
                            }
                        }];
                        
                    }
                    else {
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            [cell.loaderSpinner stopAnimating];
                            cell.imageRight.image = [UIImage imageWithData:data];
                            [self.sfAppSourceTable.imagesCache setObject:cell.imageRight.image forKey:((AlbumSN*)selfDelegate.albums[index]).imageRIGHT];
                        }];
                    }
                }];
                [cell.task resume];
            }
        }
        return cell;
    } @catch (NSException *ex) {
        NSLog(@"StayLog: EXCEPTION in moviemakerSourceTableCell collectionView cellforitematindexpath with error: %@", ex.description);
    }
    return nil;
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section { 
    return (self.albums != nil)? [self.albums count] : 0;
}


- (IBAction)expandTapped:(UITapGestureRecognizer *)sender {
    [self.txt_showMoreLess setHidden:YES];
    [self.but_showMoreLess setHidden:YES];
}


#pragma mark - IndexedCollectionView Delegates

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (!(self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) return nil;

    [self.albumCollection registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"IndexedCollectionCellIdentifier"];
    self.albumCollection.backgroundColor = [UIColor whiteColor];
    
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    self.albumCollection.frame = self.contentView.bounds;
}

- (void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate indexPath:(NSIndexPath *)indexPath
{
    self.albumCollection.dataSource = dataSourceDelegate;
    self.albumCollection.delegate = dataSourceDelegate;
    self.albumCollection.indexPath = indexPath;
    [self.albumCollection setContentOffset:self.albumCollection.contentOffset animated:NO];
    
    [self.albumCollection reloadData];
}

@end
