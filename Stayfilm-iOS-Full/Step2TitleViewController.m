//
//  Step2TitleViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Henrique Ormonde on 21/09/17.
//  Copyright © 2017 Stayfilm. All rights reserved.
//

#import "Step2TitleViewController.h"
#import "Step3StylesViewController.h"


@interface Step2TitleViewController ()

@property (nonatomic, weak) StayfilmApp *sfApp;
@property (nonatomic, weak) Uploader *uploaderStayfilm;

@end

@implementation Step2TitleViewController

@synthesize uploaderStayfilm, sfApp;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.txt_Title.delegate = self;
    
    self.but_NextStep.layer.cornerRadius = 5;
    self.but_NextStep.layer.shadowColor = [UIColor blackColor].CGColor;
    self.but_NextStep.layer.shadowOpacity = 0.2f;
    self.but_NextStep.layer.shadowRadius = 5.0f;
    self.but_NextStep.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    self.but_NextStep.layer.masksToBounds = NO;
    
    self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    self.uploaderStayfilm = [Uploader sharedUploadStayfilmSingletonWithDelegate:self];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if(self.sfApp == nil) {
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    }
    self.uploaderStayfilm = [Uploader sharedUploadStayfilmSingletonWithDelegate:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((Step2TitleViewController *) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    [self.pageController setCurrentPage:index];
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((Step2TitleViewController *) viewController).pageIndex;
    
    if (index == NSNotFound) {
        return nil;
    }
    [self.pageController setCurrentPage:index];
    
    index++;
    if (index == 3) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}


- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    [self.pageController setNumberOfPages:3];
    return 3;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 1;
}


- (UIViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (index >= 3) {
        return nil;
    }
    
    switch (index) {
        case 2:
        {
            Step2TitleViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Step2TitleViewController"];
            return  pageContentViewController;
        }
            break;
            
        default:
            return self;
            break;
    }
}


#pragma mark - Delegates

- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    if (theTextField == self.txt_Title) {
        [theTextField resignFirstResponder];
    }
    return YES;
}


#pragma mark - Uploader Delegates

-(void)uploadProgressChanged:(NSNumber *)progress
{
    // Not important on this page.
}

- (void)converterDidFailMedia:(NSString *)type
{
    NSString *message = nil;
    if(self.sfApp == nil)
    {
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    }
    if (/*self.uploaderStayfilm.listUploadMedias.count + self.sfApp.selectedMedias.count*/ self.sfApp.finalMedias.count < [self.sfApp.sfConfig.min_photos intValue]) {
        message = [NSString stringWithFormat:NSLocalizedString(@"UPLOADER_CONVERT_FAILED_UPLOAD_AGAIN_MESSAGE", nil), [self.sfApp.sfConfig.min_photos intValue] - (/*self.uploaderStayfilm.listUploadMedias.count +*/ self.sfApp.finalMedias.count)];
    }
    else if ([type isEqualToString:@"image"]) {
        message = NSLocalizedString(@"UPLOADER_CONVERT_FAILED_IMAGE_MESSAGE", nil);
    }
    else
    {
        message = NSLocalizedString(@"UPLOADER_CONVERT_FAILED_VIDEO_MESSAGE", nil);
    }
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_CONVERT_FAILED_TITLE", nil)
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)uploadDidFailMedia:(NSString *)type
{
    NSString *message = nil;
    if(self.sfApp == nil)
    {
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    }
    if (/*self.uploaderStayfilm.listUploadMedias.count + self.sfApp.selectedMedias.count*/ self.sfApp.finalMedias.count < [self.sfApp.sfConfig.min_photos intValue]) {
        message = [NSString stringWithFormat:NSLocalizedString(@"UPLOADER_CONVERT_FAILED_UPLOAD_AGAIN_MESSAGE", nil), [self.sfApp.sfConfig.min_photos intValue] - (/*self.uploaderStayfilm.listUploadMedias.count +*/ self.sfApp.finalMedias.count)];
    }
    else if ([type isEqualToString:@"image"]) {
        message = NSLocalizedString(@"UPLOADER_UPLOAD_FAILED_IMAGE_MESSAGE", nil);
    }
    else
    {
        message = NSLocalizedString(@"UPLOADER_UPLOAD_FAILED_VIDEO_MESSAGE", nil);
    }
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_UPLOAD_FAILED_TITLE", nil)
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)uploadDidFail:(Uploader *)uploader
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_ERROR_TITLE", nil)
                                                                   message:NSLocalizedString(@"UPLOADER_ERROR_MESSAGE", nil)
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    
    //[self reloadAlbumsCounters];
}

-(void)uploadDidStartSendingConfirmationMedias:(Uploader *)uploader
{
    // not important on this page.
}

-(void)uploadDidStart
{
    // does not happen in this page
}

-(void)uploadDidFinish:(Uploader *)uploader
{
    if(self.sfApp == nil) {
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    }
    if(self.uploaderStayfilm.listUploadedMedias != nil && self.uploaderStayfilm.listUploadedMedias.count > 0) {
        for (PHAsset * asset in self.uploaderStayfilm.listUploadedMedias.allValues) {
            if(![self.sfApp.finalMedias containsObject:asset]) {
                [self.sfApp.finalMedias addObject:asset];
            }
        }
    }
    if(self.sfApp.isDebugMode) {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_FINISHED", nil)
                                                                       message:[[NSString alloc] initWithFormat:NSLocalizedString(@"UPLOADED_X_MEDIAS", nil), uploader.listUploadedMedias.count]
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

-(void)uploadFinishedMedia:(PHAsset *)asset {
    // not important on this view
}


#pragma mark - Navigation

BOOL isMovingToStyles = NO;
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if ([segue.identifier isEqualToString:@"Step2toStylesSegue"] && !isMovingToStyles) {
        isMovingToStyles = YES;
//        Step3StylesViewController *step3styles = segue.destinationViewController;
        //step3styles.selectedMedias = [self.selectedMedias mutableCopy];
        if(self.sfApp == nil) {
            self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
        }
        self.sfApp.selectedTitle = self.txt_Title.text;
        //step3styles.selectedTitle = self.txt_Title.text;
        //step3styles.videoImageManager = self.imageManager;
        isMovingToStyles = NO;
    }
    
}


@end
