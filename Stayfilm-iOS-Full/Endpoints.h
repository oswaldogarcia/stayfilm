//
//  Endpoints.h
//  Stayfilm
//
//  Created by Victor Manuel Roldan on 5/16/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#ifndef Endpoints_h
#define Endpoints_h

typedef NS_ENUM(NSInteger, Endpoints){
    endpointUploadProfilePicture,
    endpointGetTemplates,
    endpointGetUserMovies,
    endpointDeleteMovie,
    endpointSubscriptionPurchase,
    endpointValidationPurchase,
    
};

#endif /* Endpoints_h */
