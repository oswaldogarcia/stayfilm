//
//  AlbumSF.m
//  Stayfilm for Messenger
//
//  Created by Henrique Ormonde on 08/05/15.
//  Copyright (c) 2015 Stayfilm. All rights reserved.
//

#import "AlbumSF.h"
#import "SFConfig.h"
//#import "AppDelegate.h"
#import "StayfilmApp.h"
#import "Media.h"


@interface AlbumSF()
@end


@implementation AlbumSF

@synthesize idAlbum, idMedias, name, description, cover, network, mediaCount;

StayfilmApp *sfAppAlbum;


+ (id)customClassWithProperties:(NSDictionary *)properties {
    if(properties != nil)
    {
        NSMutableDictionary *filteredObjs = [[NSMutableDictionary alloc] init];
        for (NSString* key in properties) {
            id value = [properties objectForKey:key];
            if ([key isEqualToString:@"id"] || [key isEqualToString:@"idAlbum"]) {
                [filteredObjs setValue:value forKey:@"idAlbum"];
            }
            else if ([key isEqualToString:@"idMedias"]) {
                [filteredObjs setValue:value forKey:@"idMedias"];
            }
            else if ([key isEqualToString:@"name"]) {
                [filteredObjs setValue:value forKey:@"name"];
            }
            else if ([key isEqualToString:@"description"]) {
                [filteredObjs setValue:value forKey:@"description"];
            }
            else if ([key isEqualToString:@"cover"]) {
                [filteredObjs setValue:value forKey:@"cover"];
            }
            else if ([key isEqualToString:@"network"]) {
                [filteredObjs setValue:value forKey:@"network"];
            }
            else if ([key isEqualToString:@"mediaCount"]) {
                [filteredObjs setValue:value forKey:@"mediaCount"];
            }
        }
        return [[self alloc] initWithProperties:filteredObjs];
    }
    else
    {
        return nil;
    }
}

- (id)initWithProperties:(NSDictionary *)properties {
    if (self = [self init]) {
        @try {
            [self setValuesForKeysWithDictionary:properties];
        }
        @catch (NSException *ex) {
            return nil;
        }
    }
    return self;
}

-(id)init {
    self = [super init];
    if (0 != self) {
        self.idAlbum = nil;
        self.idMedias = [[NSMutableArray alloc] init];
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone
{
    AlbumSF *copy = [[AlbumSF alloc] init];
    
    if (copy)
    {
        
        copy.idAlbum = self.idAlbum;
        copy.idMedias = self.idMedias;
        copy.name = self.name;
        copy.description = self.description;
        copy.cover = self.cover;
        copy.network = self.network;
        copy.mediaCount = self.mediaCount;
    }
    
    return copy;
}

+(NSMutableArray *)getAlbumsFromUser:(User *)p_user fromNetwork:(NSString *)p_network withOffset:(int)p_offset withLimit:(int)p_limit {
    @try
    {
        NSMutableArray *albums = nil;
        StayWS *ws = [[StayWS alloc] init];
        
        sfAppAlbum = [StayfilmApp sharedStayfilmAppSingleton];
        NSMutableDictionary *args = [[NSMutableDictionary alloc] init];
        NSDictionary *response = nil;
        NSArray *pathArgs = nil;
        
        [args addEntriesFromDictionary:(@{ @"network": p_network })];
        if(p_limit > 0) [args addEntriesFromDictionary:(@{ @"limit": [NSString stringWithFormat:@"%d", p_limit] })];
        if(p_offset > 0) [args addEntriesFromDictionary:(@{ @"offset": [NSString stringWithFormat:@"%d", p_offset] })];
        
        pathArgs = [NSArray arrayWithObject:sfAppAlbum.currentUser.idUser];
        
        
        response = [ws requestWebServiceWithURL:[sfAppAlbum.wsConfig getConfigPathWithKey:@"albumManagerAlbum"] withRequestType:@"GET" withPathArguments:pathArgs withArguments:args withIdSession:sfAppAlbum.settings[@"idSession"] withCache:NO];
        
        NSHTTPURLResponse *statusResponse = (NSHTTPURLResponse *) ws.responseMessage;
        if(response != nil && (statusResponse.statusCode == 200 || statusResponse.statusCode == 201))
        {
            if(response != nil && response[@"data"] != nil)
            {
                albums = [[NSMutableArray alloc] init];
                for(NSDictionary *var in response[@"data"]) {
                    [albums addObject:[AlbumSF customClassWithProperties:var]];
                }
            }
            return albums;
        }
        return albums;
    }
    @catch (NSException *ex)
    {
        return nil;
    }
}

+(AlbumSF *)getAlbumWithID:(NSString *)p_idAlbum
{
    AlbumSF *album = nil;
    @try
    {
        StayWS *ws = [[StayWS alloc] init];
        sfAppAlbum = [StayfilmApp sharedStayfilmAppSingleton];
        
        NSDictionary *args = nil;
        NSDictionary *response = nil;
        NSArray *pathArgs = nil;
        
        pathArgs = [NSArray arrayWithObjects:sfAppAlbum.settings[@"idUser"], p_idAlbum, nil];
        
        response = [ws requestWebServiceWithURL:[sfAppAlbum.wsConfig getConfigPathWithKey:@"getAlbum"] withRequestType:@"GET" withPathArguments:pathArgs withArguments:args withIdSession:sfAppAlbum.settings[@"idSession"] withCache:NO];
        
        NSHTTPURLResponse *statusResponse = (NSHTTPURLResponse *) ws.responseMessage;
        if(response != nil && (statusResponse.statusCode == 200 || statusResponse.statusCode == 201))
        {
            if(response != nil && response[@"data"] != nil)
            {
                //album = [[AlbumSF alloc] init];
                album = [AlbumSF customClassWithProperties:response[@"data"]];
            }
        }
        return album;
    }
    @catch (NSException *ex)
    {
        return [[AlbumSF alloc] init];
    }
}

+(AlbumSF *)createAlbumWithMedias:(NSArray *)p_medias withTitle:(NSString *)p_title
{
    AlbumSF *album = nil;
    @try
    {
        StayWS *ws = [[StayWS alloc] init];
        sfAppAlbum = [StayfilmApp sharedStayfilmAppSingleton];
        
        NSDictionary *args = nil;
        NSDictionary *response = nil;
        NSArray *pathArgs = nil;
        
        //NSString *medias = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:p_medias
        //                                                                                  options:0
        //                                                                                    error:nil] encoding:NSUTF8StringEncoding];
        NSString *medias = @"";
        for (NSString *media in p_medias) {
            medias = [NSString stringWithFormat:@"%@,%@", medias, media];
        }
        medias = [medias substringFromIndex:1];
        
        NSString *title = nil;
        if(p_title != nil)
        {
            title = p_title;
        }
        else
        {
            NSDate *currentDate = [[NSDate alloc] init];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd' 'HH:mm"];
            title = [dateFormatter stringFromDate:currentDate];
        }
        
        args = @{ @"idmedias" : medias,
                  @"title" : title };
        
        pathArgs = [NSArray arrayWithObject:sfAppAlbum.settings[@"idUser"]];
        
        response = [ws requestWebServiceWithURL:[sfAppAlbum.wsConfig getConfigPathWithKey:@"albumManagerAlbum"] withRequestType:@"POST" withPathArguments:pathArgs withArguments:args withIdSession:sfAppAlbum.settings[@"idSession"] withCache:NO];
        
        NSHTTPURLResponse *statusResponse = (NSHTTPURLResponse *) ws.responseMessage;
        if(response != nil && (statusResponse.statusCode == 200 || statusResponse.statusCode == 201))
        {
            if(response != nil && response[@"data"][@"album"] != nil)
            {
                //album = [[AlbumSF alloc] init];
                album = [AlbumSF customClassWithProperties:response[@"data"][@"album"]];
                album.idMedias = [p_medias mutableCopy];
            }
        }
        return album;
    }
    @catch (NSException *ex)
    {
        return [[AlbumSF alloc] init];
    }
}

-(BOOL)updateAlbumWithMedias:(NSArray *)p_medias
{
    @try
    {
        StayWS *ws = [[StayWS alloc] init];
        sfAppAlbum = [StayfilmApp sharedStayfilmAppSingleton];
        
        NSDictionary *args = nil;
        NSDictionary *response = nil;
        NSArray *pathArgs = nil;
        
        NSString *medias = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:p_medias
                                                                                          options:0
                                                                                            error:nil] encoding:NSUTF8StringEncoding];
        args = @{ @"album" : self.idAlbum,
                  @"medias" : medias };
        
        pathArgs = [NSArray arrayWithObject:sfAppAlbum.settings[@"idUser"]];
        
        response = [ws requestWebServiceWithURL:[sfAppAlbum.wsConfig getConfigPathWithKey:@"albumManagerAlbum"] withRequestType:@"PUT" withPathArguments:pathArgs withArguments:args withIdSession:sfAppAlbum.settings[@"idSession"] withCache:NO];
        
        NSHTTPURLResponse *statusResponse = (NSHTTPURLResponse *) ws.responseMessage;
        if(response != nil && (statusResponse.statusCode == 200 || statusResponse.statusCode == 201))
        {
            if(response != nil && response[@"data"][@"album"] != nil)
            {
                AlbumSF *album = [AlbumSF customClassWithProperties:response[@"data"][@"album"]];
                self.idMedias = album.idMedias;
                return YES;
            }
        }
        return NO;
    }
    @catch (NSException *ex)
    {
        return NO;
    }
}

-(BOOL)updateMediasList {
    @try {
        self.idMedias = [Media getMediasByAlbum:self.idAlbum];
        return YES;
    } @catch (NSException *ex) {
        NSLog(@"StayLog: EXCEPTION in AlbumSF updatedMediasList with error: %@", ex.description);
        return NO;
    }
}

@end


