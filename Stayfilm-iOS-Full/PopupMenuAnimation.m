//
//  PopupMenuAnimation.m
//  Stayfilm-iOS-Full
//
//  Created by Henrique Ormonde on 16/11/17.
//  Copyright © 2017 Stayfilm. All rights reserved.
//

#import "PopupMenuAnimation.h"

@implementation PopupMenuAnimation

@synthesize duration, isPresenting, originFrame;

- (void)animateTransition:(nonnull id<UIViewControllerContextTransitioning>)transitionContext {
    id containerView = transitionContext.containerView;
    id destinationView = self.isPresenting ? [transitionContext viewForKey:UITransitionContextToViewKey] : [transitionContext viewForKey:UITransitionContextFromViewKey];
    
    [containerView addSubview:destinationView];
    
    if(self.isPresenting) {
        ((UIView *)destinationView).alpha = 0.0;
        
        [UIView animateWithDuration:self.duration animations:^{
            ((UIView*)destinationView).alpha = 1.0;
        } completion:^(BOOL finished) {
            [transitionContext completeTransition:YES];
        }];
    } else {
        ((UIView *)destinationView).alpha = 1.0;
        
        [UIView animateWithDuration:self.duration animations:^{
            ((UIView*)destinationView).alpha = 0.0;
        } completion:^(BOOL finished) {
            [transitionContext completeTransition:YES];
        }];
    }
    
    
    
//    // Animations with Slide up
//    if(self.isPresenting) {
//        ((UIView *)destinationView).alpha = 0.0;
//        CGRect final = ((UIView *)destinationView).frame;
//        CGRect start = final;
//        start.origin.y += 812.0;
//        
//        ((UIView *)destinationView).frame = start;
//        
//        [UIView animateWithDuration:self.duration animations:^{
//            ((UIView*)destinationView).alpha = 1.0;
//            ((UIView *)destinationView).frame = final;
//        } completion:^(BOOL finished) {
//            [transitionContext completeTransition:YES];
//        }];
//    } else {
//        ((UIView *)destinationView).alpha = 1.0;
//        CGRect final = ((UIView *)destinationView).frame;
//        CGRect start = final;
//        start.origin.y += 812.0;
//        
//        [UIView animateWithDuration:self.duration animations:^{
//            ((UIView*)destinationView).alpha = 0.0;
//            ((UIView *)destinationView).frame = start;
//        } completion:^(BOOL finished) {
//            [transitionContext completeTransition:YES];
//        }];
//    }
        
}

- (NSTimeInterval)transitionDuration:(nullable id<UIViewControllerContextTransitioning>)transitionContext {
    return self.duration;
}

@end
