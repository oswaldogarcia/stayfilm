//
//  AppDelegate.m
//  Stayfilm-iOS-Full
//
//  Created by Henrique Ormonde on 18/05/17.
//  Copyright © 2017 Stayfilm. All rights reserved.
//

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
//#import <Appsee/Appsee.h>
#import <Google/Analytics.h>
#import <GoogleAnalytics/GAI.h>
#import <GoogleAnalytics/GAIDictionaryBuilder.h>
#import "StayfilmApp.h"
#import "Reachability.h"
#import "AppDelegate.h"
#import "IQKeyboardManager.h"
#import "AutoFilm.h"
#import "FilmStripButtonViewController.h"
#import "DeepLinkWatchViewController.h"
#import <StoreKit/StoreKit.h>
#import "SFDefinitions.h"
@import GoogleMobileAds;


@import Photos;
@import UserNotifications;
@interface AppDelegate ()
@property (nonatomic, weak) StayfilmApp *sfAppDelegate;
@property (strong, nonatomic) Reachability *reachability;
@property (strong, nonatomic) AutoFilm *autoFilm;
@end

@implementation AppDelegate

@synthesize deviceDeactivated=_deviceDeactivated;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    [Fabric with:@[[Crashlytics class]]];
//    [Appsee start];
    
    // Present Window before calling Harpy
    [self.window makeKeyAndVisible];
    
    [[Harpy sharedInstance] setPresentingViewController:_window.rootViewController];
    [[Harpy sharedInstance] setAlertType:HarpyAlertTypeForce];
//    [[Harpy sharedInstance] setAlertType:HarpyAlertTypeOption];
    NSString *appName = [[NSBundle bundleWithIdentifier:@"BundleIdentifier"] objectForInfoDictionaryKey:(id)kCFBundleExecutableKey];
    [[Harpy sharedInstance] setAppName:appName];
    
    //This parameter force update right after the Apple Aproval.
    [[Harpy sharedInstance]
     setShowAlertAfterCurrentVersionHasBeenReleasedForDays:0];
    [[Harpy sharedInstance] checkVersion];

    //OneSignal setup
     self.autoFilm = [[AutoFilm alloc]init];
    //  Create block the will fire when a notification is recieved while the app is in focus.
    id notificationRecievedBlock = ^(OSNotification *notification) {  
        NSLog(@"Received Notification - %@", notification.payload.additionalData);
        //[self.autoFilm generateAutomaticVideo];
    };
    
    // Create block that will fire when a notification is tapped on.
    id notificationOpenedBlock = ^(OSNotificationOpenedResult *result) {
        OSNotificationPayload* payload = result.notification.payload;
        
         NSLog(@"Received Notification - %@",payload.additionalData);
        
    };
    
    
    [OneSignal initWithLaunchOptions:launchOptions
                               appId:@"df768f18-6136-4d90-a760-46d8d4c8c625"
          handleNotificationReceived:notificationRecievedBlock
            handleNotificationAction:notificationOpenedBlock
                            settings:@{kOSSettingsKeyAutoPrompt: @false}];
    OneSignal.inFocusDisplayType = OSNotificationDisplayTypeNotification;
    
    // Recommend moving the below line to prompt for push after informing the user about
    //   how your app will use them.
    [OneSignal promptForPushNotificationsWithUserResponse:^(BOOL accepted) {
        NSLog(@"StayLog: User accepted notifications: %d", accepted);
    }];
    
    // Add AppDelegate as a subscription observer
    [OneSignal addSubscriptionObserver:self];
    [OneSignal addPermissionObserver:self];
    
    [IQKeyboardManager sharedManager].enable = YES;
    
    GAI *gai = [GAI sharedInstance];
    [gai trackerWithName:@"iOS Native" trackingId:@"UA-42284570-2"];
    
    // Optional: automatically report uncaught exceptions.
    gai.trackUncaughtExceptions = YES;
    
    // Optional: set Logger to VERBOSE for debug information.
    // Remove before app release.
//    gai.logger.logLevel = kGAILogLevelVerbose;
    
    // Call syncHashedEmail anywhere in your iOS app if you have the user's email.
    // This improves the effectiveness of OneSignal's "best-time" notification scheduling feature.
    // [OneSignal syncHashedEmail:userEmail];
    
    // debug to show all fonts in app
//    for (NSString *familyName in [UIFont familyNames]) {
//        NSLog(@"Family Name : %@", familyName);
//        for (NSString *fontName in [UIFont fontNamesForFamilyName:familyName]) {
//            NSLog(@"\tFont Name : %@", fontName);
//        }
//    }
    
//    self.autoFilm = [[AutoFilm alloc]init];
//    self.autoFilm.delegate = self;
//    [self.autoFilm autoFilmWihtValuesNeedCreate:NO needVerify:YES isFromStepOne:NO];   
    
   
   
    
    //Ads
    [GADMobileAds configureWithApplicationID:@"ca-app-pub-7053124358559686~5092014549"];

    // Register for remote notifications.
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    
    return YES;
}
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler {
   
    completionHandler(UNNotificationPresentationOptionNone);
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
 didReceiveNotificationResponse:(UNNotificationResponse *)response
          withCompletionHandler:(void (^)(void))completionHandler {
    
//    if(self.sfAppDelegate == nil)
//     self.sfAppDelegate = [StayfilmApp sharedStayfilmAppSingleton];
//
//    if(self.sfAppDelegate.selectedMediasToAutoFilm.count > 0){
//    self.autoFilm = [[AutoFilm alloc]init];
//    [self.autoFilm createAutomaticVideo];
//    }

    completionHandler();

}
-(void)application:(UIApplication *)application didReceiveLocalNotification:(NSDictionary *)userInfo {
    if(application.applicationState == UIApplicationStateInactive || application.applicationState == UIApplicationStateBackground) {
        
    }
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
            options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
    
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                  openURL:url
                                                        sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                               annotation:options[UIApplicationOpenURLOptionsAnnotationKey]
                    ];
    if([[url host] isEqualToString:@"watch"]) {
        if([url.path containsString:@"/"]) {
            NSString *idmovie = [url.path stringByReplacingOccurrencesOfString:@"/" withString:@""];
            DeepLinkWatchViewController *vc = [[DeepLinkWatchViewController alloc] init];
            vc.idMovie = idmovie;
            self.window.rootViewController = vc;
            
        }
    }
    
    return handled;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
}
- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    [[Harpy sharedInstance] checkVersion];
}
- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBSDKAppEvents activateApp];
    
    [[Harpy sharedInstance] checkVersionDaily];

}
- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"app"
                                                          action:@"closed"
                                                           label:@""
                                                           value:@1] build]];
}

-(UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    UIViewController *currentViewController = [self topViewController];
    
    // Check whether it implements a dummy methods called canRotate
    if ([currentViewController respondsToSelector:@selector(canRotate)]) {
        // Unlock landscape view orientations for this view controller
        return UIInterfaceOrientationMaskAllButUpsideDown;
    }
    
    // Only allow portrait (standard behaviour)
    return UIInterfaceOrientationMaskPortrait;
}

//OneSignal Push Notifications
- (void)onOSSubscriptionChanged:(OSSubscriptionStateChanges*)stateChanges {
    
    // Example of detecting subscribing to OneSignal
    if (!stateChanges.from.subscribed && stateChanges.to.subscribed) {
        if(self.sfAppDelegate == nil)
        {
            self.sfAppDelegate = [StayfilmApp sharedStayfilmAppSingleton];
        }
        [self.sfAppDelegate.settings setObject:stateChanges.to.userId forKey:@"pushIdDevice"];
        [self.sfAppDelegate saveSettings];
        NSLog(@"StayLog: Subscribed for OneSignal push notifications! id: %@", stateChanges.to.userId);
    }
    
    // prints out all properties
    NSLog(@"StayLog: SubscriptionStateChanges:\n%@", stateChanges);
}

-(void)onOSPermissionChanged:(OSPermissionStateChanges *)stateChanges {
    
    // Example of detecting anwsering the permission prompt
    if (stateChanges.from.status == OSNotificationPermissionNotDetermined) {
        if (stateChanges.to.status == OSNotificationPermissionAuthorized)
            NSLog(@"StayLog: Thanks for accepting notifications!");
        else if (stateChanges.to.status == OSNotificationPermissionDenied)
            NSLog(@"StayLog: Notifications not accepted. You can turn them on later under your iOS settings.");
    }
    
    // prints out all properties
    NSLog(@"StayLog: PermissionStateChanges:\n%@", stateChanges);
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    
    NSDictionary *addtionalData = userInfo[@"custom"][@"a"];
   
    NSLog(@"INFO:%@",addtionalData);
    if(self.sfAppDelegate == nil)
        self.sfAppDelegate = [StayfilmApp sharedStayfilmAppSingleton];
    
    if (addtionalData != nil){
        if(addtionalData[@"genre"]!=nil){
            int genreID = [addtionalData[@"genre"] intValue];
            for (Genres *genre in self.sfAppDelegate.sfConfig.genres){
                if(genre.idgenre == genreID){
                  
                    self.sfAppDelegate.selectedGenre = genre;
                    
                    if(addtionalData[@"template"]!=nil){
                        for( Template *template in self.sfAppDelegate.selectedGenre.templates){
                            if(template.idtemplate == [addtionalData[@"template"] intValue]){
                                
                                self.sfAppDelegate.selectedTemplate = template;
                                self.sfAppDelegate.canSkipStyles = YES;
                            }
                        }
                    }
                }
            }
        }
    }
   /* if(application.applicationState == UIApplicationStateInactive) {
        
        NSLog(@"Inactive");
        
        //Show the view with the content of the push
        
        completionHandler(UIBackgroundFetchResultNewData);
        
    } else if (application.applicationState == UIApplicationStateBackground) {
        
        NSLog(@"Background");
        
        //Refresh the local model
        
        completionHandler(UIBackgroundFetchResultNewData);
        
    } else {
        
        NSLog(@"Active");
        
        //Show an in-app banner
        
        completionHandler(UIBackgroundFetchResultNewData);
        
    }*/
}


/// Custom methods

- (UIViewController*)topViewController {
    return [self topViewControllerWithRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (UIViewController*)topViewControllerWithRootViewController: (UIViewController*)rootViewController
{
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController* tabBarController = (UITabBarController*)rootViewController;
        return [self topViewControllerWithRootViewController:tabBarController.selectedViewController];
    } else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController* navigationController = (UINavigationController*)rootViewController;
        return [self topViewControllerWithRootViewController:navigationController.visibleViewController];
    } else if (rootViewController.presentedViewController) {
        UIViewController* presentedViewController = rootViewController.presentedViewController;
        return [self topViewControllerWithRootViewController:presentedViewController];
    } else {
        return rootViewController;
    }
}

-(void)goToLoginPage
{
    for (UIView* view in self.window.subviews)
    {
        [view removeFromSuperview];
    }
    
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *vc = [story instantiateViewControllerWithIdentifier:@"IntroStory"];
    [self.window setRootViewController:vc];
    
    //    self.currentStoryboard = nil;
    //    self.currentViewController = nil;
    //    self.currentStoryboard = story;
    //    self.currentViewController = vc;
    
    //    self.window.rootViewController = vc;
}

-(void)resetMovieMakerStoryboard
{
    for (UIView* view in self.window.subviews)
    {
        [view removeFromSuperview];
    }
    
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"MovieMaker" bundle:nil];
    UIViewController *vc = [story instantiateInitialViewController];
    [self.window setRootViewController:vc];
    
    //    self.currentStoryboard = nil;
    //    self.currentViewController = nil;
    //    self.currentStoryboard = story;
    //    self.currentViewController = vc;
    //self.window.rootViewController = vc;
}


@end
