//
//  PhotoCollectionViewCell.h
//  fbmessenger
//
//  Created by Henrique Ormonde on 03/07/15.
//  Copyright (c) 2015 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface PhotoCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;
@property (weak, nonatomic) IBOutlet UIView *selectedView;
@property (weak, nonatomic) IBOutlet UIView *unselectedView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property (weak, nonatomic) IBOutlet UIImageView *vid_Gradient;
@property (weak, nonatomic) IBOutlet UIImageView *vid_Play;
@property (weak, nonatomic) IBOutlet UILabel *vid_Time;

@property (strong, nonatomic) NSString *source;
@property (strong, nonatomic) NSNumber *duration;
//@property (strong, nonatomic) PHAsset *asset;

@property (strong) NSURLSessionDataTask *task;

@end
