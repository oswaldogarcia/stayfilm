//
//  PlayerViewController.m
//  fbmessenger
//
//  Created by Henrique Ormonde on 24/06/15.
//  Copyright (c) 2015 Stayfilm. All rights reserved.
//
@import AVFoundation;
#import "PlayerViewController.h"
#import "StayfilmApp.h"

@interface PlayerViewController ()
@property (strong) id playerObserver;
@property (strong, nonatomic) NSArray *times;
@property (weak, nonatomic) StayfilmApp * sfApp;
@end

@implementation PlayerViewController

@synthesize currentMovie, videoUrl, idMovie, assetMovie, sfApp;

BOOL watched0 = NO;
BOOL watched10 = NO;
BOOL watched25 = NO;
BOOL watched50 = NO;
BOOL watched75 = NO;
BOOL watched100 = NO;

// orientation change enable
- (void)canRotate { }

- (void)viewDidLoad {
    [super viewDidLoad];
    
    watched0 = NO;
    watched10 = NO;
    watched25 = NO;
    watched50 = NO;
    watched75 = NO;
    watched100 = NO;
    
    self.delegate = self;
////    NSNumber *val = [NSNumber numberWithInt:UIInterfaceOrientationLandscapeLeft];
////    [[UIDevice currentDevice] setValue:val forKey:@"orientation"];
//
//    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    Float64 durationSeconds = CMTimeGetSeconds([self.player.currentItem duration]);
    CMTime percent0 = CMTimeMakeWithSeconds(1.0, 1);
    CMTime percent10 = CMTimeMakeWithSeconds(durationSeconds*10/100, 1);
    CMTime percent25 = CMTimeMakeWithSeconds(durationSeconds*25/100, 1);
    CMTime percent50 = CMTimeMakeWithSeconds(durationSeconds*50/100, 1);
    CMTime percent75 = CMTimeMakeWithSeconds(durationSeconds*75/100, 1);
    CMTime percent100 = CMTimeMakeWithSeconds(durationSeconds - 1, 1);
    self.times = @[[NSValue valueWithCMTime:percent0],
                   [NSValue valueWithCMTime:percent10],
                   [NSValue valueWithCMTime:percent25],
                   [NSValue valueWithCMTime:percent50],
                   [NSValue valueWithCMTime:percent75],
                   [NSValue valueWithCMTime:percent100]];
}

-(void)viewWillAppear:(BOOL)animated {
    if(self.sfApp == nil) {
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    }
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    
    if(self.videoUrl != nil && ![self.videoUrl isEqualToString:@""]) {
        self.player = [AVPlayer playerWithURL:[NSURL URLWithString:self.videoUrl]];
    }
    if(self.idMovie != nil && ![self.idMovie isEqualToString:@""] && self.currentMovie == nil) {
        self.currentMovie = [Movie getMovieWithID:self.idMovie];
        [self addWatchObserver];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:self.player];
    [self.player addObserver:self forKeyPath:@"rate" options:0 context:nil];
   
    if(self.currentMovie != nil || self.comeFromStory)
    {
        [self addWatchObserver];
    }
//    else if(self.assetMovie != nil) { // is Asset
//        //Already loaded AVAsset
//    }
    
    [self setShowsPlaybackControls:YES];
    [self.player play];
}

-(void)addWatchObserver {
    __weak typeof(self) selfDelegate = self;
    self.playerObserver = [self.player addBoundaryTimeObserverForTimes:selfDelegate.times queue:NULL usingBlock:^{
        @try{
            if(selfDelegate.currentMovie == nil ) {
                selfDelegate.currentMovie = [Movie getMovieWithID:selfDelegate.idMovie];
            }
            Float64 elapsed = CMTimeGetSeconds([selfDelegate.player.currentItem currentTime]);
            Float64 total = CMTimeGetSeconds([selfDelegate.player.currentItem duration]);
            if(!watched0 && elapsed >= 0.0)
            {
                watched0 = YES;
                [selfDelegate.currentMovie performSelectorInBackground:@selector(watchedPercentage:) withObject:@"0"];
                NSLog(@"StayLog: Watched 0%%");
            }
            else if(!watched10 && elapsed >= total*10.0/100)
            {
                watched10 = YES;
                [selfDelegate.currentMovie performSelectorInBackground:@selector(watchedPercentage:) withObject:@"10"];
                NSLog(@"StayLog: Watched 10%%");
            }
            else if(!watched25 && elapsed >= total*25.0/100)
            {
                watched25 = YES;
                [selfDelegate.currentMovie performSelectorInBackground:@selector(watchedPercentage:) withObject:@"25"];
                NSLog(@"StayLog: Watched 25%%");
            }
            else if(!watched50 && elapsed >= total*50.0/100)
            {
                watched50 = YES;
                [selfDelegate.currentMovie performSelectorInBackground:@selector(watchedPercentage:) withObject:@"50"];
                NSLog(@"StayLog: Watched 50%%");
            }
            else if(!watched75 && elapsed >= total*75.0/100)
            {
                watched75 = YES;
                [selfDelegate.currentMovie performSelectorInBackground:@selector(watchedPercentage:) withObject:@"75"];
                NSLog(@"StayLog: Watched 75%%");
            }
            else if(!watched100 && elapsed >= total - 1)
            {
                watched100 = YES;
                [selfDelegate.currentMovie performSelectorInBackground:@selector(watchedPercentage:) withObject:@"100"];
                NSLog(@"StayLog: Watched 100%% observer");
            }
        }
        @catch (NSException *ex)
        {
            NSLog(@"StayLog: EXCEPTION in player watch observer. Error: %@", ex.description);
        }
        
        NSString *timeDescription = (NSString *)CFBridgingRelease(CMTimeCopyDescription(NULL, [selfDelegate.player currentTime]));
        NSLog(@"StayLog: Passed a boundary at %@", timeDescription);
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:AVPlayerItemDidPlayToEndTimeNotification
                                                  object:self.player];
    [self.player removeObserver:self forKeyPath:@"rate"];
    [self.player removeTimeObserver:self.playerObserver];
    self.playerObserver = nil;
    //[[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.player pause];
    //self.player = nil;
    [self.player replaceCurrentItemWithPlayerItem:nil];
    
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:51.0/255 green:87.0/255 blue:113.0/255 alpha:1.0]; //dark blue
    [[UIDevice currentDevice] setValue:@(UIInterfaceOrientationPortrait) forKey:@"orientation"];
    [UINavigationController attemptRotationToDeviceOrientation];
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    if (self.comeFromStory){
        self.comeFromStory = NO;
        [self.playerExplorerDelegate playStoryAgain];
    }
    
}

- (void)itemDidFinishPlaying:(NSNotification *) notification
{
    if(!watched100 && self.currentMovie != nil)
    {
        watched100 = YES;
//        [self.currentMovie watchedPercentage:@"100"];
        [self.currentMovie performSelectorInBackground:@selector(watchedPercentage:) withObject:@"100"];
        NSLog(@"StayLog: Watched 100%% notification");
        if (self.comeFromStory){
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
}

- (void)observeValueForKeyPath:(NSString*)keyPath ofObject:(id)object change:(NSDictionary*)change context:(void*)context {
    if ([keyPath isEqualToString:@"rate"])
    {
        if (self.player.rate == 0.0) {
            NSTimeInterval timeSeconds = CMTimeGetSeconds(self.player.currentTime);
            NSTimeInterval durationSeconds = CMTimeGetSeconds(self.player.currentItem.duration);
            if (timeSeconds >= durationSeconds - 1.0) { // 1 sec epsilon for comparison
                [self performSelectorOnMainThread:@selector(itemDidFinishPlaying:) withObject:nil waitUntilDone:NO];
            }
        }
    }
}

-(BOOL)shouldAutorotate {
    return YES;
}

//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
//{
//    return YES;
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
