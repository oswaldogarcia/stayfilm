    //
//  StayWS.m
//  Stayfilm for Messenger
//
//  Created by Henrique Ormonde on 27/04/15.
//  Copyright (c) 2015 Stayfilm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StayWS.h"
#import "AppDelegate.h"

@interface StayWS()
@property (nonatomic, assign) NSInteger timesAttempted;

@end

@implementation StayWS

@synthesize wsURL=_wsURL;
@synthesize responseMessage=_responseMessage;
@synthesize responseBody=_responseBody;
@synthesize friendlyMessage=_friendlyMessage;
@synthesize maxRequestAttempts=_maxRequestAttempts;
@synthesize timesAttempted=_timesAttempted;

-(id)init
{
    self = [super init];
    if (0 != self) {
        _maxRequestAttempts = 6;
        _timesAttempted = 0;
    }
    return self;
}

-(id)initWithMaxAttemps:(int)maxAttempts
{
    self = [super init];
    if (0 != self) {
        _maxRequestAttempts = maxAttempts;
        _timesAttempted = 0;
    }
    return self;
}

-(NSDictionary *)requestWebServiceWithURL:(NSString *)wsURLFormat withRequestType:(NSString *)requestType withPathArguments:(NSArray *)pathArguments withArguments:(NSDictionary *)arguments withIdSession:(NSString *)idSession withCache:(BOOL)cache
{
    NSMutableURLRequest* request = nil;
    NSError *error = nil;
    BOOL retry = NO;
    
    @try {
        if(pathArguments != nil && pathArguments.count > 0) //TODO: for now only supporting 2 args
        {
            if(pathArguments.count > 1) {
                self.wsURL = [NSString stringWithFormat:wsURLFormat, pathArguments[0], pathArguments[1]];
            }
            else {
                self.wsURL = [NSString stringWithFormat:wsURLFormat, pathArguments[0]];
            }
        }
        else {
            self.wsURL = wsURLFormat;
        }
        NSLog(@"Sending %@ request: %@",requestType, self.wsURL);
        
        if([requestType isEqualToString:@"GET"])
        {
            if(arguments != nil)
            {
                self.wsURL = [self.wsURL stringByAppendingString:@"?"];
                for (NSString* key in arguments) {
                    id value = [arguments objectForKey:key];
                    self.wsURL = [self.wsURL stringByAppendingString:key];
                    self.wsURL = [self.wsURL stringByAppendingString:@"="];
                    self.wsURL = [self.wsURL stringByAppendingString:value];
                    self.wsURL = [self.wsURL stringByAppendingString:@"&"];
                }
                self.wsURL = [self.wsURL substringToIndex:[self.wsURL length] - 1];
            }
            request = [[NSMutableURLRequest alloc] initWithURL:[[NSURL alloc] initWithString:self.wsURL] cachePolicy:((cache)? NSURLRequestUseProtocolCachePolicy : NSURLRequestReloadIgnoringLocalCacheData) timeoutInterval:60];
            
            if(idSession != nil && idSession.length != 0)
            {
                [request setValue:idSession forHTTPHeaderField:@"idSession"];
                [request setValue:@"1" forHTTPHeaderField:@"useheader"];
            }
//            [request setValue:@"1" forHTTPHeaderField:@"useheader"];
            [request setValue:@"iOS" forHTTPHeaderField:@"appAgent"];
            [request setHTTPMethod:@"GET"];
            
            NSObject<OS_dispatch_semaphore> *semaphore = dispatch_semaphore_create(0);
            NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                self.responseBody = data;
                self.responseMessage = response;
                dispatch_semaphore_signal(semaphore);
            }];
            [task resume];
            dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        }
        else if([requestType isEqualToString:@"POST"])
        {
            request = [[NSMutableURLRequest alloc] initWithURL:[[NSURL alloc] initWithString:self.wsURL] cachePolicy:((cache)? NSURLRequestUseProtocolCachePolicy : NSURLRequestReloadIgnoringLocalCacheData) timeoutInterval:60];
            
            if(idSession != nil && idSession.length != 0)
            {
                [request setValue:idSession forHTTPHeaderField:@"idSession"];
                [request setValue:@"1" forHTTPHeaderField:@"useheader"];
            }
//            [request setValue:@"1" forHTTPHeaderField:@"useheader"];
            [request setValue:@"iOS" forHTTPHeaderField:@"appAgent"];
            [request setHTTPMethod:@"POST"];
            
            [request setValue:@"application/json; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
            NSData *requestData = [NSJSONSerialization dataWithJSONObject:arguments options:0 error:&error];
            [request setHTTPBody:requestData];

            NSObject<OS_dispatch_semaphore> *semaphore = dispatch_semaphore_create(0);
            NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                self.responseBody = data;
                self.responseMessage = response;
                dispatch_semaphore_signal(semaphore);
            }];
            [task resume];
            dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        }
        else if([requestType isEqualToString:@"PUT"])
        {
            request = [[NSMutableURLRequest alloc] initWithURL:[[NSURL alloc ] initWithString:self.wsURL] cachePolicy:((cache)? NSURLRequestUseProtocolCachePolicy : NSURLRequestReloadIgnoringLocalCacheData) timeoutInterval:60];
            
            if(idSession != nil && idSession.length != 0)
            {
                [request setValue:idSession forHTTPHeaderField:@"idSession"];
                [request setValue:@"1" forHTTPHeaderField:@"useheader"];
            }
//            [request setValue:@"1" forHTTPHeaderField:@"useheader"];
            [request setValue:@"iOS" forHTTPHeaderField:@"appAgent"];
            [request setHTTPMethod:@"PUT"];
            
            [request setValue:@"application/json; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
            NSData *requestData = [NSJSONSerialization dataWithJSONObject:arguments options:0 error:&error];
            [request setHTTPBody:requestData];
            
            NSObject<OS_dispatch_semaphore> *semaphore = dispatch_semaphore_create(0);
            NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                self.responseBody = data;
                self.responseMessage = response;
                dispatch_semaphore_signal(semaphore);
            }];
            [task resume];
            dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        }
        else if ([requestType isEqualToString:@"DELETE"])
        {
            request = [[NSMutableURLRequest alloc] initWithURL:[[NSURL alloc ] initWithString:self.wsURL] cachePolicy:((cache)? NSURLRequestUseProtocolCachePolicy : NSURLRequestReloadIgnoringLocalCacheData) timeoutInterval:60];
            
            if(idSession != nil && idSession.length != 0)
            {
                [request setValue:idSession forHTTPHeaderField:@"idSession"];
                [request setValue:@"1" forHTTPHeaderField:@"useheader"];
            }
//            [request setValue:@"1" forHTTPHeaderField:@"useheader"];
            [request setValue:@"iOS" forHTTPHeaderField:@"appAgent"];
            [request setHTTPMethod:@"DELETE"];
            
            NSObject<OS_dispatch_semaphore> *semaphore = dispatch_semaphore_create(0);
            NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                self.responseBody = data;
                self.responseMessage = response;
                dispatch_semaphore_signal(semaphore);
            }];
            [task resume];
            dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        }
        
        error = nil;
        
        NSString *json = [NSJSONSerialization JSONObjectWithData:self.responseBody
                                                         options:kNilOptions
                                                           error:&error];
        if ([self.responseBody length] != 0 && json == nil)
        {
            // INVALID JSON
            [NSException raise:@"Invalid JSON" format:@"Error message is: %@", error];
        }
        
        NSHTTPURLResponse *tempResponse = (NSHTTPURLResponse *) self.responseMessage;
        if(tempResponse.statusCode != 200 && tempResponse.statusCode != 201)
        {
            if(tempResponse.statusCode == 304 && ([self.wsURL containsString:@"rest/1.2/config"] || [self.wsURL containsString:@"rest/1.2/newgallery"]))
            {
                return (NSDictionary *) [NSJSONSerialization JSONObjectWithData:self.responseBody options:kNilOptions error:&error];
            }
            NSLog(@"StayLog: ERROR JSON: %@", json);
            [NSException raise:@"Invalid Response" format:@"Error status code: %ld", (long)tempResponse.statusCode];
        }
        
        return (NSDictionary *) [NSJSONSerialization JSONObjectWithData:self.responseBody options:kNilOptions error:&error];
    }
    @catch (NSException *exception) {
        NSLog(@"StayLog: EXCEPTION in requestWebServicewithURL with error: %@", exception.description);
        if(((AppDelegate *)[UIApplication sharedApplication].delegate).deviceDeactivated)
        {
            ((AppDelegate *)[UIApplication sharedApplication].delegate).deviceDeactivated = NO;
            retry = YES;
        }
        else
        {
            if([exception.name isEqualToString:@"Invalid JSON"])
            {
                return nil;
            }
            if(self.timesAttempted <= self.maxRequestAttempts)
            {
                retry = YES;
            }
            else
            {
                retry = NO;
                error = nil;
                if ([NSJSONSerialization JSONObjectWithData:self.responseBody
                                                    options:kNilOptions
                                                      error:&error] != nil)
                {
                    self.timesAttempted = 0;
                    NSDictionary *errorJSON = [NSJSONSerialization JSONObjectWithData:self.responseBody options:kNilOptions error:&error];
                    return errorJSON;
                }
            }
        }
        
        if(retry)
        {
            self.timesAttempted++;
            return [self requestWebServiceWithURL:wsURLFormat withRequestType:requestType withPathArguments:pathArguments withArguments:arguments withIdSession:idSession withCache:cache];
        }
        
        return nil;
    }

    
}

@end
