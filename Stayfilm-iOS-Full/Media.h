//
//  Media.h
//  Stayfilm Full iOS
//
//  Created by Henrique Ormonde on 22/05/15.
//  Copyright (c) 2015 Stayfilm. All rights reserved.
//
#ifndef Stayfilm_Full_Media_h
#define Stayfilm_Full_Media_h


#import <Foundation/Foundation.h>

@interface Media : NSObject

@property (nonatomic, strong) NSString *idMidia;
@property (nonatomic, strong) NSString *idAlbum;
@property (nonatomic, assign) NSInteger idSocialNetwork;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *cover;
@property (nonatomic, strong) NSString *source;
@property (nonatomic, strong) NSString *thumbnail;
@property (nonatomic, assign) NSInteger width;
@property (nonatomic, assign) NSInteger height;
@property (nonatomic, assign) BOOL isVideo;

+ (id)customClassWithProperties:(NSDictionary *)properties;
+(NSMutableArray *)getMediasByAlbum:(NSString *)p_idAlbum;
+(NSMutableArray *)createXMediasWithX:(int)p_mediasCount;
+(NSMutableArray *)confirmMedias:(NSArray *)p_medias WithIdAlbum:(NSString *)p_idAlbum andOverrideIdAlbum:(NSString **)p_idAlbumOverride;

@end

#endif
