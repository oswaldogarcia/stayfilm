//
//  ShareButton_CollectionViewCell.h
//  Stayfilm-iOS-Full
//
//  Created by Henrique on 6/29/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShareButton_CollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *title;


@end
