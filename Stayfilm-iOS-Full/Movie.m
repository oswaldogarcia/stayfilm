//
//  Movie.m
//  Stayfilm for Messenger
//
//  Created by Henrique Ormonde on 11/05/15.
//  Copyright (c) 2015 Stayfilm. All rights reserved.
//

#import "Movie.h"
//#import "AppDelegate.h"
#import "StayfilmApp.h"

@implementation Movie

@synthesize idMovie, title, synopsis, created, updated, publicated, prettyCreated, prettyPublicated, status, permission, idGenre, idTheme, idTemplate, likeCount, shareCount, viewCount, commentCount, shareComment, liked, idUser, user, sharer, shared, baseUrl, bestof, thumbnailUrl, videoUrl;

StayfilmApp *sfAppMovie;

-(id)init
{
    self = [super init];
    if (0 != self) {
        self.idMovie = nil;
        self.permission = MoviePermission_PRIVATE;
    }
    return self;
}

//- (void) setBaseUrl:(NSString*) new_value
//{
//    self.baseUrl = new_value;
//    self.videoUrl = [new_value stringByAppendingString:@"/video.mp4"];
//    self.thumbnailUrl = [new_value stringByAppendingString:@"/266x150_n.jpg"];
//}

+ (id)customClassWithProperties:(NSDictionary *)properties {
    if(properties != nil)
    {
        NSMutableDictionary *filteredObjs = [[NSMutableDictionary alloc] init];
        for (NSString* key in properties) {
            id value = [properties objectForKey:key];
            if ([key isEqualToString:@"idMovie"] || [key isEqualToString:@"idmovie"]) {
                [filteredObjs setValue:value forKey:@"idMovie"];
            }
            else if ([key isEqualToString:@"idUser"] || [key isEqualToString:@"iduser"]) {
                [filteredObjs setValue:value forKey:@"idUser"];
            }
            else if ([key isEqualToString:@"title"]) {
                [filteredObjs setValue:value forKey:@"title"];
            }
            else if ([key isEqualToString:@"synopsis"]) {
                [filteredObjs setValue:value forKey:@"synopsis"];
            }
            else if ([key isEqualToString:@"created"]) {
                [filteredObjs setValue:value forKey:@"created"];
            }
            else if ([key isEqualToString:@"updated"]) {
                [filteredObjs setValue:value forKey:@"updated"];
            }
            else if ([key isEqualToString:@"publicated"]) {
                [filteredObjs setValue:value forKey:@"publicated"];
            }
            else if ([key isEqualToString:@"prettyCreated"] || [key isEqualToString:@"prettycreated"]) {
                [filteredObjs setValue:value forKey:@"prettyCreated"];
            }
            else if ([key isEqualToString:@"prettyPublicated"] || [key isEqualToString:@"prettypublicated"]) {
                [filteredObjs setValue:value forKey:@"prettyPublicated"];
            }
            else if ([key isEqualToString:@"status"]) {
                [filteredObjs setValue:value forKey:@"status"];
            }
            else if ([key isEqualToString:@"permission"]) {
                [filteredObjs setValue:value forKey:@"permission"];
            }
            else if ([key isEqualToString:@"idGenre"] || [key isEqualToString:@"idgenre"]) {
                [filteredObjs setValue:value forKey:@"idGenre"];
            }
            else if ([key isEqualToString:@"idTheme"] || [key isEqualToString:@"idtheme"]) {
                [filteredObjs setValue:value forKey:@"idTheme"];
            }
            else if ([key isEqualToString:@"idTemplate"] || [key isEqualToString:@"idtemplate"] ) {
                [filteredObjs setValue:value forKey:@"idTemplate"];
            }
            else if ([key isEqualToString:@"likeCount"] || [key isEqualToString:@"likecount"]) {
                [filteredObjs setValue:value forKey:@"likeCount"];
            }
            else if ([key isEqualToString:@"shareCount"] ||[key isEqualToString:@"sharecount"] ) {
                [filteredObjs setValue:value forKey:@"shareCount"];
            }
            else if ([key isEqualToString:@"viewCount"] || [key isEqualToString:@"viewcount"]) {
                [filteredObjs setValue:value forKey:@"viewCount"];
            }
            else if ([key isEqualToString:@"commentCount"] ||[key isEqualToString:@"commentcount"]) {
                [filteredObjs setValue:value forKey:@"commentCount"];
            }
            else if ([key isEqualToString:@"shareComment"] || [key isEqualToString:@"sharecomment"]) {
                [filteredObjs setValue:value forKey:@"shareComment"];
            }
            else if ([key isEqualToString:@"liked"]) {
                [filteredObjs setValue:value forKey:@"liked"];
            }
            else if ([key isEqualToString:@"user"]) {
                [filteredObjs setValue:value forKey:@"user"];
            }
            else if ([key isEqualToString:@"sharer"]) {
                [filteredObjs setValue:value forKey:@"sharer"];
            }
            else if ([key isEqualToString:@"shared"]) {
                [filteredObjs setValue:value forKey:@"shared"];
            }
            else if ([key isEqualToString:@"baseUrl"] || [key isEqualToString:@"baseurl"]) {
                [filteredObjs setValue:value forKey:@"baseUrl"];
                [filteredObjs setValue:[value stringByAppendingString:@"/video.mp4"] forKey:@"videoUrl"];
                if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                    [filteredObjs setValue:[value stringByAppendingString:@"/640x360_n.jpg"] forKey:@"thumbnailUrl"];
                } else
                    [filteredObjs setValue:[value stringByAppendingString:@"/266x150_n.jpg"] forKey:@"thumbnailUrl"];
            }
            else if ([key isEqualToString:@"bestof"]) {
                [filteredObjs setValue:value forKey:@"bestof"];
            }
            else if ([key isEqualToString:@"thumbnailUrl"] || [key isEqualToString:@"thumbnailurl"]) {
                [filteredObjs setValue:value forKey:@"thumbnailUrl"];
            }
            else if ([key isEqualToString:@"videoUrl"] || [key isEqualToString:@"videourl"] ) {
                [filteredObjs setValue:value forKey:@"videoUrl"];
            }
        }
        return [[self alloc] initWithProperties:filteredObjs];
    }
    else
    {
        return nil;
    }
}

- (id)initWithProperties:(NSDictionary *)properties {
    if (self = [self init]) {
        @try {
            [self setValuesForKeysWithDictionary:properties];
        }
        @catch (NSException *ex) {
            return nil;
        }
    }
    return self;
}

+(Movie *)getMovieWithID:(NSString*)p_idMovie
{
    Movie *movie = [[Movie alloc] init];
    @try {
        StayWS *ws = [[StayWS alloc] init];
        sfAppMovie = [StayfilmApp sharedStayfilmAppSingleton];
        NSDictionary *args = nil;
        NSDictionary *response = nil;
        NSArray *pathArgs = nil;
        
        pathArgs = [NSArray arrayWithObject: p_idMovie];
        
        response = [ws requestWebServiceWithURL:[sfAppMovie.wsConfig getConfigPathWithKey:@"getMovie"] withRequestType:@"GET" withPathArguments:pathArgs withArguments:args withIdSession:sfAppMovie.settings[@"idSession"] withCache:NO];
        
        NSHTTPURLResponse *statusResponse = (NSHTTPURLResponse *) ws.responseMessage;
        if(response != nil && (statusResponse.statusCode == 200 || statusResponse.statusCode == 201))
        {
            movie = [Movie customClassWithProperties:response];
        }

        return movie;
    }
    @catch (NSException *exception) {
        return movie;
    }
}

-(BOOL)watchedPercentage:(NSString*)p_percent
{
    @try {
        StayWS *ws = [[StayWS alloc] init];
        sfAppMovie = [StayfilmApp sharedStayfilmAppSingleton];
        NSDictionary *args = nil;
        NSDictionary *response = nil;
        NSArray *pathArgs = nil;
        
        args = @{ @"percent" : p_percent };
        
        pathArgs = [NSArray arrayWithObject: self.idMovie];
        
        response = [ws requestWebServiceWithURL:[sfAppMovie.wsConfig getConfigPathWithKey:@"getMovie"] withRequestType:@"PUT" withPathArguments:pathArgs withArguments:args withIdSession:sfAppMovie.settings[@"idSession"] withCache:NO];
        
        NSHTTPURLResponse *statusResponse = (NSHTTPURLResponse *) ws.responseMessage;
        if(statusResponse.statusCode == 200 || statusResponse.statusCode == 201) //response comes nil even on status code 200
        {
            return YES;
        }
        else
        {
            return NO;
        }
    }
    @catch (NSException *ex) {
        return NO;
    }
}

-(BOOL)publishMovie
{
    @try
    {
        StayWS *ws = [[StayWS alloc] init];
        sfAppMovie = [StayfilmApp sharedStayfilmAppSingleton];
        NSDictionary *args = nil;
        NSDictionary *response = nil;
        NSArray *pathArgs = nil;
        
        args = @{ @"publish" : @"1",
                  @"permission" : [NSString stringWithFormat: @"%ld", (long)self.permission] };
        
        pathArgs = [NSArray arrayWithObject: self.idMovie];
        
        response = [ws requestWebServiceWithURL:[sfAppMovie.wsConfig getConfigPathWithKey:@"publishMovie"] withRequestType:@"PUT" withPathArguments:pathArgs withArguments:args withIdSession:sfAppMovie.settings[@"idSession"] withCache:NO];
        
        NSHTTPURLResponse *statusResponse = (NSHTTPURLResponse *) ws.responseMessage;
        if(statusResponse.statusCode == 200 || statusResponse.statusCode == 201)
        {
            return YES;
        }
        else
        {
            return NO;
        }
    }
    @catch (NSException *ex)
    {
        return NO;
    }
}
-(BOOL)changePermisssionMovie
{
    @try
    {
        StayWS *ws = [[StayWS alloc] init];
        sfAppMovie = [StayfilmApp sharedStayfilmAppSingleton];
        NSDictionary *args = nil;
        NSDictionary *response = nil;
        NSArray *pathArgs = nil;
        
        args = @{ @"permission" : [NSString stringWithFormat: @"%ld", (long)self.permission] };
        
        pathArgs = [NSArray arrayWithObject: self.idMovie];
        
        response = [ws requestWebServiceWithURL:[sfAppMovie.wsConfig getConfigPathWithKey:@"publishMovie"] withRequestType:@"PUT" withPathArguments:pathArgs withArguments:args withIdSession:sfAppMovie.settings[@"idSession"] withCache:NO];
        
        NSHTTPURLResponse *statusResponse = (NSHTTPURLResponse *) ws.responseMessage;
        if(statusResponse.statusCode == 200 || statusResponse.statusCode == 201)
        {
            return YES;
        }
        else
        {
            return NO;
        }
    }
    @catch (NSException *ex)
    {
        return NO;
    }
}

-(BOOL)publishMovieWithDescription:(NSString*)synopsis {
    @try
    {
        StayWS *ws = [[StayWS alloc] init];
        sfAppMovie = [StayfilmApp sharedStayfilmAppSingleton];
        NSDictionary *args = nil;
        NSDictionary *response = nil;
        NSArray *pathArgs = nil;
        
        if (self.permission != MoviePermission_PRIVATE) {
            args = @{ @"publish" : @"1",
                      @"synopsis" : synopsis,
                      @"permission" : [NSString stringWithFormat: @"%ld", (long)self.permission] };
        } else {
            args = @{ @"publish" : @"1",
                      @"synopsis" : synopsis }; // permission private
        }
        
        pathArgs = [NSArray arrayWithObject: self.idMovie];
        
        response = [ws requestWebServiceWithURL:[sfAppMovie.wsConfig getConfigPathWithKey:@"publishMovie"] withRequestType:@"PUT" withPathArguments:pathArgs withArguments:args withIdSession:sfAppMovie.settings[@"idSession"] withCache:NO];
        
        NSHTTPURLResponse *statusResponse = (NSHTTPURLResponse *) ws.responseMessage;
        if(response != nil && (statusResponse.statusCode == 200 || statusResponse.statusCode == 201))
        {
            return YES;
        }
        else
        {
            return NO;
        }
    }
    @catch (NSException *ex)
    {
        return NO;
    }
}

-(BOOL)shareMovieOnFacebook {
    @try {
        StayWS *ws = [[StayWS alloc] init];
        sfAppMovie = [StayfilmApp sharedStayfilmAppSingleton];
        NSDictionary *args = nil;
        NSDictionary *response = nil;
        NSArray *pathArgs = nil;
        
        pathArgs = [NSArray arrayWithObject: self.idMovie];
        
        args = @{ @"socialNetwork" : @"facebook" ,
                  @"facebookToken" : sfAppMovie.settings[@"facebookToken"] };
        
        response = [ws requestWebServiceWithURL:[sfAppMovie.wsConfig getConfigPathWithKey:@"shareMovieOnFacebook"] withRequestType:@"PUT" withPathArguments:pathArgs withArguments:args withIdSession:sfAppMovie.settings[@"idSession"] withCache:NO];
        
        NSHTTPURLResponse *statusResponse = (NSHTTPURLResponse *) ws.responseMessage;
        if(statusResponse.statusCode == 200 || statusResponse.statusCode == 201)
        {
            return YES;
        }
        else
        {
            return NO;
        }
    }
    @catch (NSException *ex) {
        return NO;
    }
}

-(BOOL)shareMovieOnTwitterWithMessage:(NSString*)p_message {
    @try {
        StayWS *ws = [[StayWS alloc] init];
        sfAppMovie = [StayfilmApp sharedStayfilmAppSingleton];
        NSDictionary *args = nil;
        NSDictionary *response = nil;
        NSArray *pathArgs = nil;
        
        pathArgs = [NSArray arrayWithObject: sfAppMovie.currentUser.idUser];
        
        if(p_message == nil || [NSString isEqual:@""]) {
            args = @{ @"networks" : @"twitter" ,
                      @"idmovie" : self.idMovie };
        }
        else {
            args = @{ @"networks" : @"twitter" ,
                      @"idmovie" : self.idMovie,
                      @"message" : p_message };
        }
        
        response = [ws requestWebServiceWithURL:[sfAppMovie.wsConfig getConfigPathWithKey:@"getUserNetworks"] withRequestType:@"POST" withPathArguments:pathArgs withArguments:args withIdSession:sfAppMovie.settings[@"idSession"] withCache:NO];
        
        NSHTTPURLResponse *statusResponse = (NSHTTPURLResponse *) ws.responseMessage;
        if(statusResponse.statusCode == 200 || statusResponse.statusCode == 201)
        {
            return YES;
        }
        else
        {
            return NO;
        }
    }
    @catch (NSException *ex) {
        return NO;
    }
}


-(BOOL)downloadedMovie {
    @try {
        StayWS *ws = [[StayWS alloc] init];
        sfAppMovie = [StayfilmApp sharedStayfilmAppSingleton];
        NSDictionary *args = nil;
        NSDictionary *response = nil;
        NSArray *pathArgs = nil;
        
        pathArgs = [NSArray arrayWithObject: self.idMovie];
        
        response = [ws requestWebServiceWithURL:[sfAppMovie.wsConfig getConfigPathWithKey:@"downloadedMovie"] withRequestType:@"POST" withPathArguments:pathArgs withArguments:args withIdSession:sfAppMovie.settings[@"idSession"] withCache:NO];
        
        NSHTTPURLResponse *statusResponse = (NSHTTPURLResponse *) ws.responseMessage;
        if(statusResponse.statusCode == 200 || statusResponse.statusCode == 201)
        {
            return YES;
        }
        else
        {
            return NO;
        }
    }
    @catch (NSException *ex) {
        return NO;
    }
}


-(BOOL)deleteMovie
{
    @try {
        StayWS *ws = [[StayWS alloc] init];
        sfAppMovie = [StayfilmApp sharedStayfilmAppSingleton];
        NSDictionary *args = nil;
        NSDictionary *response = nil;
        NSArray *pathArgs = nil;
        
        pathArgs = [NSArray arrayWithObject: self.idMovie];
        
        response = [ws requestWebServiceWithURL:[sfAppMovie.wsConfig getConfigPathWithKey:@"deleteMovie"] withRequestType:@"DELETE" withPathArguments:pathArgs withArguments:args withIdSession:sfAppMovie.settings[@"idSession"] withCache:NO];
        
        NSHTTPURLResponse *statusResponse = (NSHTTPURLResponse *) ws.responseMessage;
        if(statusResponse.statusCode == 200 || statusResponse.statusCode == 201)
        {
            return YES;
        }
        else
        {
            return NO;
        }
    }
    @catch (NSException *ex) {
        return NO;
    }
}



@end
