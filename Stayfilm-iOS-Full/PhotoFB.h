//
//  PhotoFB.h
//  Stayfilm for Messenger
//
//  Created by Henrique Ormonde on 15/05/15.
//  Copyright (c) 2015 Stayfilm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageFB : NSObject

@property (nonatomic, strong) NSNumber *width;
@property (nonatomic, strong) NSNumber *height;
@property (nonatomic, strong) NSString *source;

+ (id)customClassWithProperties:(NSDictionary *)properties;
- (id)initWithProperties:(NSDictionary *)properties;

@end

@interface PhotoFB : NSObject

@property (nonatomic, strong) NSString *idPhoto;
@property (nonatomic, strong) NSString *idAlbum;
@property (nonatomic, strong) NSNumber *width;
@property (nonatomic, strong) NSNumber *height;
@property (nonatomic, strong) NSString *source;
@property (nonatomic, strong) NSArray *images;

+ (id)customClassWithProperties:(NSDictionary *)properties;
- (id)initWithProperties:(NSDictionary *)properties;

@end
