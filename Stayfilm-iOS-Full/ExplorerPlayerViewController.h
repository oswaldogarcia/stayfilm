//
//  ExplorerPlayerViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 14/06/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//


#import <AVKit/AVKit.h>
#import "Movie.h"
#import "SegmentedProgressBar.h"
#import "SFHelper.h"
#import "StayfilmApp.h"

@protocol ExplorerDelegate<NSObject>

- (void)moveToNext;
- (void)moveToBack;
- (void)swipeDown;
- (void)storyButtonEnable:(BOOL) enable;
- (void)showSwipeUpView:(BOOL) show;
- (void)setViewedId:(NSNumber *)idStory onId:(NSNumber *)idconten;
- (void)changeStoryButtonText:(NSString*)text Color:(UIColor *)color andIcon:(NSString*) imageString;
- (void)setContentDescription:(NSString*)description;
- (void)setStoryLogo:(NSString*)urlImageLogo;
- (void)changedConnectivity:(BOOL)connected;
//- (void)setViewedIndex:(NSNumber *)idStory onIndex:(int)index;

@end


@interface ExplorerPlayerViewController : AVPlayerViewController  <StayfilmSingletonDelegate>
@property (nonatomic, strong) NSMutableArray *myFilms;
@property (nonatomic, strong) NSMutableArray *storiesContent;
@property (nonatomic, strong) NSMutableArray *myPlayers;
@property (nonatomic, strong) AVQueuePlayer *customPlayer;
@property (nonatomic, strong) UIImageView *backgroundImage;
@property (nonatomic, strong) UIButton *nextView;
@property (nonatomic, strong) UIButton *backView;
@property (nonatomic, strong) Story *story;
@property (nonatomic, strong) StoryContent *storyContent;
@property (nonatomic) int indexConten;
@property (nonatomic) int currentConten;
@property (nonatomic) float storyDuration;
@property (nonatomic, assign) BOOL isStillAvailable;
@property (nonatomic, retain) IBOutlet SegmentedProgressBar *segmentedProgressBar;
@property (nonatomic, assign) id<ExplorerDelegate> explorerDelegate;

@property (strong) id playerObserver;
@property (strong, nonatomic) NSArray *times;

@end
