//
//  Step3StylesViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Henrique on 1/5/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StayfilmApp.h"
#import "Uploader.h"
#import "StyleTableViewCell.h"
#import "TermsAndPolicyStyleViewController.h"
#import "SubscriptionModalViewController.h"

@protocol ChangeStyleDelegate<NSObject>

- (void)styleChanged:(Genres *)genre Template:(Template *)selectedTemplate;
@end


@interface Step3StylesViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, StayfilmUploaderDelegate,StyleDelegate,TermsStyleDelegate,SubscriptionDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *genresTableHeightConstraint;
@property (weak, nonatomic) IBOutlet UITableView *genresTable;
@property (weak, nonatomic) IBOutlet UIButton *but_nextStep;
@property (weak, nonatomic) IBOutlet UIImageView *img_butNextStep;
@property (weak, nonatomic) IBOutlet UILabel *txt_Title;
@property (weak, nonatomic) IBOutlet UIView *navigationView;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *changeStyleButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthCancelConstraint;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic) BOOL isFromExplorer;
@property (nonatomic) BOOL isEditing;
@property (nonatomic) BOOL changeEditing;
@property (nonatomic) NSIndexPath *indexSelectedGenre;
@property (nonatomic) StayfilmTypesFilmOrientation selectedOrientation;

@property (nonatomic, strong) id<ChangeStyleDelegate> delegate;
@end
