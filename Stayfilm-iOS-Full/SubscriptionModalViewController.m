//
//  SubscriptionModalViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 3/27/19.
//  Copyright © 2019 Stayfilm. All rights reserved.
//

#import "SubscriptionModalViewController.h"
#import "SFDefinitions.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "TermsSuscriptionViewController.h"
#import "ShellWebService.h"
#import "SubscriptionConditionsViewController.h"

@interface SubscriptionModalViewController ()<SKPaymentTransactionObserver,SubscriptionTermsDelegate>

@end

@implementation SubscriptionModalViewController

BOOL transactionInProgress;

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated{
    
    if (self.sfApp == nil){
    self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    }
    [self setViewInfo];
    [SKPaymentQueue.defaultQueue addTransactionObserver:self];
    
}

- (void)setViewInfo{
    
    [self.productImage sd_setImageWithURL:[NSURL URLWithString: self.currentTemplate.image]  placeholderImage:nil options:SDWebImageRefreshCached progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL * _Nullable targetURL) {/*...*/ } completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        if([self.productImage isLoaderShowing]){
            [self.productImage hideLoader];
        }
        if (image != nil){
            [self.productImage setImage:image];
        }else{
            [self.productImage setImage: [UIImage imageNamed:@"Select_VideoGradient"]];
        }
    }];
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numberFormatter setLocale:self.sfApp.product.priceLocale];
    
    NSString *buyButtonTitle = [NSString stringWithFormat:NSLocalizedString(@"PRICE_MONTH", nil),[numberFormatter stringFromNumber:self.sfApp.product.price]];
    
    [self.buyButton setTitle:buyButtonTitle forState:UIControlStateNormal];
    
    NSString *langcode = [[NSLocale currentLocale] objectForKey:NSLocaleLanguageCode];
    
    if([langcode isEqualToString:@"pt"]){
        self.titleSuscriptionImage.image = [UIImage imageNamed:@"titleSubscriptionPortuguese"];
    }else if([langcode isEqualToString:@"es"]){
        self.titleSuscriptionImage.image = [UIImage imageNamed:@"titleSubscriptionSpanish"];
    }else{
        self.titleSuscriptionImage.image = [UIImage imageNamed:@"titleSubscriptionEnglish"];
    }
    
    self.smallDescriptionLabel.text = NSLocalizedString(@"SUBSCRIBE_TO_UNLOCK", nil);
    self.offerLabel.text = NSLocalizedString(@"SPECIAL_OFFER", nil);
    self.descriptionLabel.text = NSLocalizedString(@"PRODUCT_DESCRIPTION", nil);
    
    [self.restorePurchaseButton setTitle:NSLocalizedString(@"RESTORE_PURCHASE", nil) forState:UIControlStateNormal];
    [self.termsButton setTitle:NSLocalizedString(@"PURCHASE_TERMS_TITLE", nil) forState:UIControlStateNormal];
    
    self.benefitsTitleLabel.text = NSLocalizedString(@"BENEFITS", nil);
    self.benefitsLabel.text = NSLocalizedString(@"BENEFITS_LIST", nil);
    
   self.conditionsLabel.text = NSLocalizedString(@"CONDITIONS_LABEL", nil);
    
    NSMutableAttributedString *titleString = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"CONDITIONS_BUTTON", nil)];
    
    [titleString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [titleString length])];
   
    [self.conditionsButton setAttributedTitle: titleString forState:UIControlStateNormal];
    
}

- (IBAction)cancelAction:(id)sender {
   
    [self dismissViewControllerAnimated:YES completion:nil];
   
}

- (IBAction)buyButtonPressed:(id)sender {
    [self.buyButton setEnabled:NO];
    [self.view setUserInteractionEnabled:NO];
    [self startPurchase];
 }

-(void)startPurchase{
    
    if(transactionInProgress){
        return;
    }
    // put this on alert
    if ([self.sfApp.product isEqual:[NSNull null]]) {
        NSLog(@"There are no product.");
    } else {
        
        //[[UIApplication sharedApplication].keyWindow makeToastActivity:CSToastPositionCenter];
        [self.view makeToastActivity:CSToastPositionCenter];
        SKPayment *payment = [SKPayment paymentWithProduct:self.sfApp.product];
        [SKPaymentQueue.defaultQueue addPayment:payment];
        transactionInProgress = YES;
    }
}

-(void)endPurchase{
    
    [self.view hideToastActivity];
    [self.buyButton setEnabled:YES];
    [self.view setUserInteractionEnabled:YES];
    transactionInProgress = NO;
}

- (void)restorePurchase{
    [self restorePurchaseAction:nil];
}

- (IBAction)conditionsAction:(id)sender {
    
    SubscriptionConditionsViewController * view = [[SubscriptionConditionsViewController alloc] init];
    
    view.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    view.view.frame = self.view.frame;
    
    [self presentViewController:view animated:YES completion:nil];
}


- (IBAction)termsAction:(id)sender {

    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://www.stayfilm.com/institutional/terms"]];
    
    /*
    TermsSuscriptionViewController * view = [[TermsSuscriptionViewController alloc] init];
    
    view.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    view.view.frame = self.view.frame;
    view.delegate = self;
    
    [self presentViewController:view animated:YES completion:nil];*/
    
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray<SKPaymentTransaction *> *)transactions{
   
        for(SKPaymentTransaction *transaction in transactions){
     
        switch (transaction.transactionState) {
            case SKPaymentTransactionStatePurchased:
                NSLog(@"Transaction completed successfully.");
                [self iapPurchaseOnApi];
                [SKPaymentQueue.defaultQueue finishTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                NSLog(@"Transaction Failed");
                [SKPaymentQueue.defaultQueue finishTransaction:transaction];
                 [self endPurchase];
                break;
                
            default:
                NSLog(@"%ld",(long)transaction.transactionState);
        
                break;
        }
        
    }

}
- (void) iapPurchaseOnApi{
    
    if(self.sfApp == nil)
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    
    
    NSURL * receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
    NSData * receipt = [NSData dataWithContentsOfURL:receiptURL];
    NSLog(@"Receipt:%@",[receipt base64EncodedStringWithOptions:0]);
    NSDictionary * parameters = @{
                                  @"purchasetoken": [receipt base64EncodedStringWithOptions:0],
                                  @"device":@"ios",
                                  @"user_id" : self.sfApp.settings[@"idUser"],
                                  @"idSession" : self.sfApp.settings[@"idSession"]
                                  };
    
    ShellWebService *service = [ShellWebService getSharedInstance];
    [service callService:parameters endpointName:endpointSubscriptionPurchase
     withCompletionBlock:^(NSDictionary *resultArray, NSError *error) {
        if(!error){
            
            NSLog(@"%@", resultArray);
            if(resultArray[@"isValidPurchase"] != nil){
                self.sfApp.currentUser.hasSubscription = [resultArray[@"isValidPurchase"] boolValue];
                BOOL subscription;
                
                if([resultArray[@"isValidPurchase"] boolValue]){
                    subscription = YES;
                }else{
                    subscription = NO;
                }
                
                [[NSUserDefaults standardUserDefaults] setBool:subscription forKey:@"hasSubscription"];
                [self endPurchase];
                [self dismissViewControllerAnimated:YES completion:nil];
                [self.delegate subscriptionConfirmed];
            }
          
        }else{
            [self endPurchase];
        }
    }];
}
- (IBAction)restorePurchaseAction:(id)sender {
    
    
    NSDictionary * parameters = @{
                                  @"user_id" : self.sfApp.settings[@"idUser"]
                                  };
    
    ShellWebService *service = [ShellWebService getSharedInstance];
    [service callService:parameters endpointName:endpointValidationPurchase
     withCompletionBlock:^(NSDictionary *resultArray, NSError *error) {
         if(!error){
             NSLog(@"%@", resultArray);
             if(resultArray[@"isValidPurchase"] != nil){
                 
                 self.sfApp.currentUser.hasSubscription = [resultArray[@"isValidPurchase"] boolValue];
                 
                 BOOL subscription;
                 
                 if([resultArray[@"isValidPurchase"] boolValue]){
                     subscription = YES;
                 }else{
                     subscription = NO;
                 }
                 
                 [[NSUserDefaults standardUserDefaults] setBool:subscription forKey:@"hasSubscription"];
                 
                 if([resultArray[@"isValidPurchase"] boolValue]){
                     
                     [self dismissViewControllerAnimated:YES completion:nil];
                     [self.delegate subscriptionConfirmed];
                 }else{
                     
                     
                     UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"STAYFILM", nil) message:NSLocalizedString(@"NO_SUBSCRIPTION", nil) preferredStyle:UIAlertControllerStyleAlert];
                     UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:nil];
                     
                     [alert addAction:defaultAction];
                     
                     
                     [self presentViewController:alert animated:YES completion:nil];
                     
                 }
             }
         }
     }];
    
}

- (void)subscriptionInTermsConfirmed{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.delegate subscriptionConfirmed];
}

@end
