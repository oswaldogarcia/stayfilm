//
//  Job.m
//  Stayfilm for Messenger
//
//  Created by Henrique Ormonde on 12/05/15.
//  Copyright (c) 2015 Stayfilm. All rights reserved.
//

#import "Job.h"

//#import "Utilities.h"
#import "StayfilmApp.h"

@implementation JobItem

@synthesize user, movieTitle, snTypes, idAlbumsList, tags, idGenre, idTheme, idSubtheme;

@end


@interface Job ()

@property (nonatomic, strong) Job* jobSent;

@end

@implementation Job

@synthesize jobSent, idJob, idUser, idMovie, jobType, source, notEnoughContent, uncertainContent, socialnetworkPendingJob, imageAnalyzerPendingJob, medias, created, updated, progress, status, data;

StayfilmApp *sfAppJob;

-(id)init
{
    self = [super init];
    if (0 != self) {
        self.idJob = nil;
        self.idMovie = nil;
        self.jobSent = nil;
        self.progress = nil;
        self.notEnoughContent = NO;
        self.uncertainContent = NO;
        self.socialnetworkPendingJob = NO;
        self.imageAnalyzerPendingJob = NO;
        //self.status = (StayfilmTypes_JobStatus *)JobStatus_PENDING;
        self.status = @"PENDING";
    }
    return self;
}

+ (id)customClassWithProperties:(NSDictionary *)properties {
    if(properties != nil)
    {
        NSMutableDictionary *filteredObjs = [[NSMutableDictionary alloc] init];
        for (NSString* key in properties) {
            id value = [properties objectForKey:key];
            if ([[key lowercaseString]isEqualToString:@"idjob"]) {
                [filteredObjs setValue:value forKey:@"idJob"];
            }
            else if ([key isEqualToString:@"idUser"]) {
                [filteredObjs setValue:value forKey:@"idUser"];
            }
            else if ([key isEqualToString:@"jobType"]) {
                [filteredObjs setValue:value forKey:@"jobType"];
            }
            else if ([key isEqualToString:@"source"]) {
                [filteredObjs setValue:value forKey:@"source"];
            }
            else if ([key isEqualToString:@"notEnoughContent"]) {
                [filteredObjs setValue:value forKey:@"notEnoughContent"];
            }
            else if ([key isEqualToString:@"uncertainContent"]) {
                [filteredObjs setValue:value forKey:@"uncertainContent"];
            }
            else if ([key isEqualToString:@"socialnetworkPendingJob"]) {
                [filteredObjs setValue:value forKey:@"socialnetworkPendingJob"];
            }
            else if ([key isEqualToString:@"imageAnalyzerPendingJob"]) {
                [filteredObjs setValue:value forKey:@"imageAnalyzerPendingJob"];
            }
            else if ([key isEqualToString:@"medias"]) {
                [filteredObjs setValue:value forKey:@"medias"];
            }
            else if ([key isEqualToString:@"created"]) {
                [filteredObjs setValue:value forKey:@"created"];
            }
            else if ([key isEqualToString:@"updated"]) {
                [filteredObjs setValue:value forKey:@"updated"];
            }
            else if ([key isEqualToString:@"status"]) {
                [filteredObjs setValue:value forKey:@"status"];
//                if([value isEqualToString:@"SUCCESS"])
//                {
//                    StayfilmTypes_JobStatus statvalue = JobStatus_SUCCESS;
//                    [filteredObjs setValue:[[NSNumber alloc] initWithInt:statvalue] forKey:@"status"];
//                }
//                else if([value isEqualToString:@"PENDING"])
//                {
//                    StayfilmTypes_JobStatus statvalue = JobStatus_PENDING;
//                    [filteredObjs setValue:[[NSNumber alloc] initWithInt:statvalue] forKey:@"status"];
//                }
//                else if([value isEqualToString:@"FAILURE"])
//                {
//                    StayfilmTypes_JobStatus statvalue = JobStatus_FAILURE;
//                    [filteredObjs setValue:[[NSNumber alloc] initWithInt:statvalue] forKey:@"status"];
//                }
            }
            else if ([key isEqualToString:@"data"]) {
                [filteredObjs setValue:value forKey:@"data"];
                NSDictionary *data = value;
                for (NSString* keyData in data) {
                    id valueData = [data objectForKey:keyData];
                    if ([keyData isEqualToString:@"progress"]) {
                        [filteredObjs setValue:valueData forKey:@"progress"];
                    }
                    if ([keyData isEqualToString:@"idmovie"]) {
                        [filteredObjs setValue:valueData forKey:@"idMovie"];
                    }
                }
            }
        }
        return [[self alloc] initWithProperties:filteredObjs];
    }
    else
    {
        return nil;
    }
}

- (id)initWithProperties:(NSDictionary *)properties {
    if (self = [self init]) {
        @try {
            [self setValuesForKeysWithDictionary:properties];
        }
        @catch (NSException *ex) {
            return nil;
        }
    }
    return self;
}

+(Job *)getJobWithID:(NSString *)p_idJob withUser:(User *)p_user
{
    Job *job = [[Job alloc] init];
    @try {
        StayWS *ws = [[StayWS alloc] init];
        sfAppJob = [StayfilmApp sharedStayfilmAppSingleton];
        NSDictionary *args = nil;
        NSDictionary *response = nil;
        NSArray *pathArgs = nil;
        
        pathArgs = [NSArray arrayWithObject: p_idJob];
        
        response = [ws requestWebServiceWithURL:[sfAppJob.wsConfig getConfigPathWithKey:@"getJob"] withRequestType:@"GET" withPathArguments:pathArgs withArguments:args withIdSession:sfAppJob.settings[@"idSession"] withCache:NO];
        
        NSHTTPURLResponse *statusResponse = (NSHTTPURLResponse *) ws.responseMessage;
        if(response != nil && (statusResponse.statusCode == 200 || statusResponse.statusCode == 201))
        {
            job = [Job customClassWithProperties:response];
        }
        
        return job;
    }
    @catch (NSException *exception) {
        return job;
    }
}

+(Job *)postMeliesJobwithMedias:(NSArray *)p_mediasFacebook withAlbum:(AlbumSF *)p_album withGenre:(Genres *)p_genre andIsUpload:(BOOL)isUpload
{
    Job *job = nil;
    @try {
        StayWS *ws = [[StayWS alloc] init];
        sfAppJob = [StayfilmApp sharedStayfilmAppSingleton];
        NSDictionary *args = nil;
        NSDictionary *response = nil;
        NSArray *pathArgs = nil;

        NSDate *currentDate = [[NSDate alloc] init];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd' 'HH:mm"];
        NSString* title = (sfAppJob.selectedTitle != nil)? sfAppJob.selectedTitle : [@"*NOTITLE*" stringByAppendingString:[dateFormatter stringFromDate:currentDate]];
        
        NSDictionary *languageDic = [NSLocale componentsFromLocaleIdentifier:[[NSLocale preferredLanguages] objectAtIndex:0]];
        NSString *lang = [languageDic objectForKey:@"kCFLocaleLanguageCodeKey"];
        NSString *orientation;
        if(sfAppJob.selectedGenre.orientation == VERTICAL){
            orientation = @"vertical";
        }else{
            orientation = @"horizontal";
        }
        
        if(sfAppJob.selectedTemplate == nil) {
            args = @{
                    @"slug" : @"stayfilm",
                    @"theme" : @"7",
                    @"subtheme" : @"40",
                    @"jobtype" : @"timeline",
                    @"genre" : [NSString stringWithFormat: @"%d", (int)p_genre.idgenre],
                    @"title" : title,
                    @"networks" : (isUpload) ? @"sf_upload" : @"",
                    @"medias" : (p_mediasFacebook != nil) ? p_mediasFacebook : @"",
                    @"language":lang,
                    @"orientation": orientation
                    };
        } else {
            args = @{ //@"slug" : @"fbmessenger",
                    @"slug" : @"stayfilm",
                    @"theme" : @"7",
                    @"subtheme" : @"40",
                    @"jobtype" : @"timeline",
                    @"genre" : [NSString stringWithFormat: @"%d", (int)p_genre.idgenre],
                    @"template":[NSString stringWithFormat: @"%d", (int)sfAppJob.selectedTemplate.idtemplate],
                    @"title" : title,
                    @"networks" : (isUpload) ? @"sf_upload" : @"",
                    //@"idalbum" : (p_album.idAlbum != nil) ? p_album.idAlbum : @"",
                    @"medias" : (p_mediasFacebook != nil) ? p_mediasFacebook : @"",
                    @"language":lang,
                    @"orientation": orientation
                    };
            
        }

        BOOL waitingStep = YES;
        while (waitingStep) {
            waitingStep = [sfAppJob isLoggedIn];
            waitingStep = !waitingStep;
        }

        response = [ws requestWebServiceWithURL:[sfAppJob.wsConfig getConfigPathWithKey:@"postJob"] withRequestType:@"POST" withPathArguments:pathArgs withArguments:args withIdSession:sfAppJob.settings[@"idSession"] withCache:NO];
        
        NSHTTPURLResponse *statusResponse = (NSHTTPURLResponse *) ws.responseMessage;
        if(response != nil && (statusResponse.statusCode == 200 || statusResponse.statusCode == 201))
        {
            job = [[Job alloc] init];
            
            if (response[@"notEnoughContent"] != nil)
            {
                job.notEnoughContent = (BOOL)response[@"notEnoughContent"];
            }
            else
            {
                job.notEnoughContent = NO;
            }
            
            if (response[@"socialnetworkPendingJob"] != nil)
            {
                job.socialnetworkPendingJob = (BOOL)response[@"socialnetworkPendingJob"];
            }
            else
            {
                job.socialnetworkPendingJob = NO;
            }
            
            if (response[@"imageAnalyzerPendingJob"] != nil)
            {
                job.imageAnalyzerPendingJob = (BOOL)response[@"imageAnalyzerPendingJob"];
            }
            else
            {
                job.imageAnalyzerPendingJob = NO;
            }
            
            if (response[@"medias"] != nil)
            {
                job.medias = response[@"medias"];
            }
            
            if (response[@"idjob"] != nil)
            {
                job = [Job customClassWithProperties:response];
            }
            else
            {
                job.created = [NSNumber numberWithInt:-1];
            }
        }
        else
        {
            NSLog(@"StayLog: EXCEPTION on JOB request with response: %@", response);
        }
        
        return job;
        
    }
    @catch (NSException *exception) {
        NSLog(@"StayLog: EXCEPTION on postMeliesJobwithMedias with error: %@", exception.description);
        return job;
    }
}

+(Job*)postImageAnalyzerWithAlbumID:(NSString*)p_idAlbum
{
    Job *job = nil;
    @try {
        StayWS *ws = [[StayWS alloc] init];
        sfAppJob = [StayfilmApp sharedStayfilmAppSingleton];
        NSDictionary *args = nil;
        NSDictionary *response = nil;
        NSArray *pathArgs = nil;
        
        args = @{ @"jobtype" : @"imageanalyzer",
                  @"idalbum" : p_idAlbum };
        
        
        response = [ws requestWebServiceWithURL:[sfAppJob.wsConfig getConfigPathWithKey:@"postJob"] withRequestType:@"POST" withPathArguments:pathArgs withArguments:args withIdSession:sfAppJob.settings[@"idSession"] withCache:NO];
        
        NSHTTPURLResponse *statusResponse = (NSHTTPURLResponse *) ws.responseMessage;
        if(response != nil && (statusResponse.statusCode == 200 || statusResponse.statusCode == 201))
        {
            job = [[Job alloc] init];
            
            if (response[@"idjob"] != nil)
            {
                job = [Job customClassWithProperties:response];
            }
            else
            {
                job.created = [NSNumber numberWithInt:-1];
            }
        }
        
        return job;
        
    }
    @catch (NSException *exception) {
        return job;
    }

}

+(Job*)postSocialNetwork:(NSString*)p_Network
{
    Job *job = nil;
    @try {
        StayWS *ws = [[StayWS alloc] init];
        sfAppJob = [StayfilmApp sharedStayfilmAppSingleton];
        NSDictionary *args = nil;
        NSDictionary *response = nil;
        NSArray *pathArgs = nil;
        
        args = @{ @"jobtype" : @"socialnetwork",
                  @"socialnetwork" : p_Network };
        
        response = [ws requestWebServiceWithURL:[sfAppJob.wsConfig getConfigPathWithKey:@"postJob"] withRequestType:@"POST" withPathArguments:pathArgs withArguments:args withIdSession:sfAppJob.settings[@"idSession"] withCache:NO];
        
        NSHTTPURLResponse *statusResponse = (NSHTTPURLResponse *) ws.responseMessage;
        if(response != nil && (statusResponse.statusCode == 200 || statusResponse.statusCode == 201))
        {
            job = [[Job alloc] init];
            
            if (response[@"idjob"] != nil)
            {
                job = [Job customClassWithProperties:response];
            }
            else
            {
                job.created = [NSNumber numberWithInt:-1];
            }
        }
        
        return job;
        
    }
    @catch (NSException *exception) {
        return job;
    }
    
}


@end
