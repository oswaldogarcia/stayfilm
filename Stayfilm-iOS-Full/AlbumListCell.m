//
//  AlbumListCell.m
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 2/4/19.
//  Copyright © 2019 Stayfilm. All rights reserved.
//

#import "AlbumListCell.h"

@implementation AlbumListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
