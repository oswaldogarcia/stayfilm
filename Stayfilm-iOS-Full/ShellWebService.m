//
//  ShellWebService.m
//  Stayfilm
//
//  Created by Victor Manuel Roldan on 5/16/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import "ShellWebService.h"
//#import "GenericModel.h"
#import "SFHelper.h"
#import "SFDefinitions.h"
#import "Endpoints.h"
//#import "UserModel.h"

@implementation ShellWebService
@synthesize endpoint, httpMethod, endpointEnum;


+ (ShellWebService*)getSharedInstance {
    static ShellWebService *sharedInstance = nil;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        sharedInstance = [[ShellWebService alloc] initWithBaseURL:[NSURL URLWithString: SERVER_API_URL_BASE]]; //baseUrl
    });
    
    return sharedInstance;
}

- (void)selectWebService :(enum Endpoints)service :(NSDictionary*)parameters{
    
    switch (service) {
        case endpointUploadProfilePicture:{
            httpMethod = httpPost;
            NSString *userId = [parameters objectForKey:@"user_id"];
            endpoint = [NSString stringWithFormat:uploadProfilePicture, userId];
            break;
        }
        case endpointGetTemplates:{
            httpMethod = httpGet;
            NSString *device = [parameters objectForKey:@"device"];
            endpoint = [NSString stringWithFormat:getTemplates, device];
            break;
        }case endpointGetUserMovies:{
            httpMethod = httpGet;
            NSString *idUser = [parameters objectForKey:@"idUser"];
            NSString *limit =  [parameters objectForKey:@"limit"];
            NSString *offset = [parameters objectForKey:@"offset"];
            //NSString *pending  = [parameters objectForKey:@"pending"];
            endpoint = [NSString stringWithFormat:getUserMovies, idUser ,limit, offset];
            break;
            
        }case endpointDeleteMovie:{
            httpMethod = httpDelete;
            NSString *idMovie = [parameters objectForKey:@"idMovie"];
            endpoint = [NSString stringWithFormat:deleteMovie, idMovie];
            break;
        }
        case endpointSubscriptionPurchase:{
            httpMethod = httpPost;
            NSString *userId = [parameters objectForKey:@"user_id"];
            endpoint = [NSString stringWithFormat:subscriptionPurchase, userId];
            break;
        }
        case endpointValidationPurchase:{
            httpMethod = httpGet;
            NSString *userId = [parameters objectForKey:@"user_id"];
            endpoint = [NSString stringWithFormat:subscriptionPurchase, userId];
            break;
        }
        default:
            break;
    }

    //Add BASE URL
    endpoint = [NSString stringWithFormat:@"%@%@",SERVER_API_URL_BASE, endpoint];
}

- (NSDictionary*)updateParams:(NSDictionary*)params{
    NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];

    if(!([httpMethod isEqualToString:httpGet] || [httpMethod isEqualToString:httpDelete])){
        if(params != nil){
            newDict = [[NSMutableDictionary alloc] initWithDictionary:params];
        }
    }

    NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSDictionary *languageDic = [NSLocale componentsFromLocaleIdentifier:language];
    NSString *languageCode = [languageDic objectForKey:@"kCFLocaleLanguageCodeKey"];
    newDict[@"lang"] = languageCode;
    //newDict[@"operative_system"] = @"ios";
    //newDict[@"token_device"] = @"randomValue";//TODO: change this value.
/*
    if([SFHelper getSessionObject:sessionUser] == nil){
        newDict[@"grant_type"] = GRANT_TYPE;
        newDict[@"client_id"] = CLIENT_ID;
        newDict[@"client_secret"] = CLIENT_SECRET;
    }else{
        NSObject *userObject = [SFHelper getSessionObject:sessionUser];
        UserModel *userSession = [[UserModel alloc] initWithNSObjectSession:userObject];
        if(userSession != nil){
            newDict[@"access_token"] = userSession.accessToken;
        }
    }
 */
    return newDict;
}

- (void)callService:(NSDictionary*)parameters endpointName:(enum Endpoints)endpointName withCompletionBlock:(void(^)(NSDictionary *resultArray, NSError *error))completionBlock {
    
    endpointEnum  = endpointName;
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:httpContentTypeJson];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setTimeoutInterval:15.00];
    [manager.requestSerializer setValue:httpContentTypeJson forHTTPHeaderField:@"Content-Type"];
    
    if([SFHelper getSessionObject:@"settings"] != nil){
        
        NSObject *userObject = [SFHelper getSessionObject:@"settings"];
        
        NSString *idSession = [userObject valueForKey:@"idSession"];
        
        //NSLog(@"** ID SESSION: %@  **", idSession); make sure no critical information goes into system log
        
        [manager.requestSerializer setValue:idSession forHTTPHeaderField:@"idSession"];
        
        [manager.requestSerializer setValue:@"1" forHTTPHeaderField:@"useheader"];
        [manager.requestSerializer setValue:@"iOS" forHTTPHeaderField:@"appAgent"];
        
    }
    
    
    [self selectWebService:endpointName:parameters];
    parameters = [self updateParams:parameters];
    NSMutableDictionary *newParameters = [[NSMutableDictionary alloc] initWithDictionary:parameters];
//    NSLog(@"\n\n-------------------------------------\n\nendpoint: %@ \n\n parameters: %@ \n\n-------------------------------------", endpoint, parameters);
    
    if([httpMethod isEqualToString:httpPost]){
        
        [manager POST:endpoint parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            NSDictionary *dict = [[NSDictionary alloc] initWithDictionary:responseObject];
            //GenericModel *response = [GenericModel genericModelWithDictionary:dict];
            
            completionBlock(dict, nil);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"StayLog: Error: %@", error);
            completionBlock(nil, error);
        }];
        
    }else if([httpMethod isEqualToString:httpGet]){
        //[newParameters removeObjectForKey:@"access_token"]; //removed from parameters
        [manager GET:endpoint parameters:newParameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            NSDictionary *dict = [[NSDictionary alloc] initWithDictionary:responseObject];
//            GenericModel *response = [[GenericModel alloc] initWithDictionary:dict];
            
            completionBlock(dict, nil);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [self convertNSErrorToGenericModel:error];
            completionBlock(nil, error);
        }];
    }else if([httpMethod isEqualToString:httpPut]){
        [newParameters removeObjectForKey:@"access_token"]; //removed from parameters
        
        [manager PUT:endpoint parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            NSDictionary *dict = [[NSDictionary alloc] initWithDictionary:responseObject];
//            GenericModel *response = [[GenericModel alloc] initWithDictionary:dict];
            completionBlock(dict, nil);
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [self convertNSErrorToGenericModel:error];
            completionBlock(nil, error);
            
        }];
    }else if([httpMethod isEqualToString:httpDelete]){
        [newParameters removeObjectForKey:@"access_token"]; //removed from parameters
        
        [manager DELETE:endpoint parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            NSDictionary *dict =@{};
             //[[NSDictionary alloc] initWithDictionary:responseObject];
//            GenericModel *response = [[GenericModel alloc] initWithDictionary:dict];
            completionBlock(dict, nil);
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [self convertNSErrorToGenericModel:error];
            completionBlock(nil, error);
            
        }];
    }
    
}


- (void)callServiceImage:(NSDictionary*)parameters endpointName:(enum Endpoints)endpointName withCompletionBlock:(void(^)(NSDictionary *resultArray, NSError *error))completionBlock {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:httpContentTypeMultipart];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];

    [manager.requestSerializer setValue:[parameters objectForKey:@"idSession"] forHTTPHeaderField:@"idSession"];
    [manager.requestSerializer setValue:@"1" forHTTPHeaderField:@"useheader"];
    [manager.requestSerializer setValue:@"iOS" forHTTPHeaderField:@"appAgent"];

    [self selectWebService:endpointName:parameters];
//    parameters = [self updateParams:parameters];
    
    if([SFHelper getSessionObject:sessionUser] != nil){
//        NSObject *userObject = [BPHelper getSessionObject:sessionUser];
//        UserModel *userSession = [[UserModel alloc] initWithNSObjectSession:userObject];
//        [manager.requestSerializer setValue:userSession.accessToken forHTTPHeaderField:@"Authorization"];
    }
    
    [manager POST:endpoint parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
        UIImage *img = [[UIImage alloc] init];
        img = [parameters objectForKey:@"image"];
        NSData *photoImageData = [[NSData alloc] init];
        photoImageData = UIImageJPEGRepresentation(img, 0.5);
        
        [formData appendPartWithFileData:photoImageData name:[NSString stringWithFormat:@"file_contents"] fileName:@"image.jpg" mimeType:@"image/png"];
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSError* error2;
        NSDictionary* json2 = [NSJSONSerialization JSONObjectWithData:responseObject
                                                             options:kNilOptions
                                                               error:&error2];
//        NSDictionary *dict = [[NSDictionary alloc] initWithDictionary:(NSDictionary*)responseObject];
//        GenericModel *response = [GenericModel genericModelWithDictionary:dict];
        
        completionBlock(json2, nil);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self convertNSErrorToGenericModel:error];
        completionBlock(nil, error);
    }];
    
}

- (void) convertNSErrorToGenericModel:(NSError*)error{
    
    NSString* errorMessage = [[NSString alloc] initWithData:(NSData *)error.userInfo[@"com.alamofire.serialization.response.error.data"] encoding:NSUTF8StringEncoding];
    NSData *objectData = [errorMessage dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData: objectData options:NSJSONReadingMutableLeaves error:nil];
    
        NSHTTPURLResponse *response = error.userInfo[@"com.alamofire.serialization.response.error.response"];
        //NSString *timeout = [error.userInfo valueForKey:@"NSLocalizedDescription"];
        NSInteger statusCode = response.statusCode;
        
    NSLog(@"StayLog: jsonError: %@", json);
    NSLog(@"StayLog: statusCode: %ld", (long)statusCode);
    
    
}

- (UIImage *)decodeBase64ToImage:(NSString *)strEncodeData {
    NSData *data = [[NSData alloc]initWithBase64EncodedString:strEncodeData options:NSDataBase64DecodingIgnoreUnknownCharacters];
    return [UIImage imageWithData:data];
}

@end
