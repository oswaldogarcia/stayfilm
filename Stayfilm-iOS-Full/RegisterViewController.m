//
//  RegisterViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Henrique on 1/29/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <Google/Analytics.h>
#import "RegisterViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Reachability.h"

@interface RegisterViewController ()

@property (nonatomic, assign) int loginAttempts;
@property (nonatomic, weak) StayfilmApp *sfAppRegister;

@property (nonatomic, strong) NSURLSession *thumbLoadSession;
@property (strong) NSURLSessionDataTask *task;

@property (nonatomic, assign) CGRect showingPoint;
@property (nonatomic, assign) CGRect hiddenPoint;
@property (nonatomic, assign) CGRect finishedPoint;

@property (strong, nonatomic) NSArray *pickerGenreData;

@property (nonatomic, strong) NSString *register_email;
@property (nonatomic, strong) NSString *register_password;
@property (nonatomic, strong) NSString *register_firstName;
@property (nonatomic, strong) NSString *register_lastName;
@property (nonatomic, strong) NSString *register_gender;
@property (nonatomic, strong) NSString *register_birthday;

@property (nonatomic, strong) UITapGestureRecognizer *termsTapGesture;

@property (nonatomic, assign) BOOL didComeBackFromFBLogin;

@property (nonatomic, assign) BOOL isStarting;

@end

@implementation RegisterViewController

@synthesize txt_Email, txt_PasswordStep1, lbl_PasswordStep1, lbl_Email, spinnerLoaderStep1, but_back, but_CreateAccount, but_TermsAgreements, loginFacebookButton, lbl_feedback;
@synthesize spinnerLoaderStep2, txt_FirstName, txt_Password, txt_LastName, lbl_FirstName, lbl_LastName, lbl_Password, lbl_Required, but_Continue;
@synthesize spinnerLoaderStep3, picker_Date, picker_Gender, but_Gender, but_Birthday, but_ContinueFinish, but_Skip;
@synthesize img_UserProfile, lbl_UserName, lbl_UserEmail, but_EditProfile, but_Start;
@synthesize showingPoint, hiddenPoint, finishedPoint, sfAppRegister, imageBackground, pickerGenreData;


/***  CONSIDERATIONS
 
 some  elements in this page were taken off due to lockdown (to make register steps easier) so pickers (Genre and Date) are not used. Only Step 1 is currently active the rest is hidden
 
 ***/

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.loginAttempts = 0;
    
    self.pickerGenreData = @[NSLocalizedString(@"UNDEFINED", nil;),
                             NSLocalizedString(@"MALE", nil),
                             NSLocalizedString(@"FEMALE", nil)];
    
    //self.picker_Gender.dataSource = self;
    //self.picker_Gender.delegate = self;
    
    self.img_UserProfile.layer.cornerRadius = self.img_UserProfile.frame.size.width / 2;
    self.img_UserProfile.clipsToBounds = YES;
    
    self.sfAppRegister = [StayfilmApp sharedStayfilmAppSingleton];
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    self.thumbLoadSession = [NSURLSession sessionWithConfiguration:config];
    
    //step1
    [self.txt_Email addTarget:self action:@selector(txt_Email_Ended) forControlEvents:UIControlEventEditingDidEndOnExit];
    [self.txt_PasswordStep1 addTarget:self action:@selector(txt_PasswordStep1_Ended) forControlEvents:UIControlEventEditingDidEndOnExit];
    //step2
    [self.txt_FirstName addTarget:self action:@selector(txt_FirstName_Ended) forControlEvents:UIControlEventEditingDidEndOnExit];
    [self.txt_LastName addTarget:self action:@selector(txt_LastName_Ended) forControlEvents:UIControlEventEditingDidEndOnExit];
    [self.txt_Password addTarget:self action:@selector(txt_Password_Ended) forControlEvents:UIControlEventEditingDidEndOnExit];
    //step3
    [self.picker_Gender setValue:[UIColor whiteColor] forKey:@"textColor"];
    [self.picker_Date setValue:[UIColor whiteColor] forKey:@"textColor"];
    if ([self.picker_Date respondsToSelector:sel_registerName("setHighlightsToday:")])
    {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundeclared-selector"
        
        [self.picker_Date performSelector:@selector(setHighlightsToday:) withObject:[NSNumber numberWithBool:NO]];
        
#pragma clang diagnostic pop
    }
    
    NSCalendar *cal = [NSCalendar currentCalendar];
    
    NSDate *date = [NSDate date];
    NSDateComponents *comps = [cal components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay)
                                     fromDate:date];
    [comps setYear:-13];
    NSDate *minus13 = [cal dateByAddingComponents:comps toDate:date options:0];
    
    self.picker_Date.maximumDate = minus13;
    
    self.isStarting = NO;
    
    self.showingPoint = [self.view_Step1 frame];
    self.hiddenPoint = CGRectMake(self.view_Step2.frame.origin.x, self.view_Step2.frame.origin.y, self.view_Step2.frame.size.width, self.view_Step2.frame.size.height);
    self.finishedPoint = CGRectMake(self.view_Step3.frame.origin.x, self.view_Step3.frame.origin.y, self.view_Step3.frame.size.width, self.view_Step3.frame.size.height);
    
    [self.view_Step3 setFrame:self.hiddenPoint];
    [self.view_Step3 setHidden:YES];
    [self.view_Step2 setHidden:YES];
    [self.view_Created setFrame:self.hiddenPoint];
    [self.view_Created setHidden:YES];
    self.view_Step2.alpha = 0.0;
    self.view_Step3.alpha = 0.0;
    self.view_Created.alpha = 0.0;
    
    self.loginFacebookButton.layer.cornerRadius = 5;
    self.loginFacebookButton.layer.masksToBounds = YES;
    self.but_CreateAccount.layer.cornerRadius = 5;
    self.but_CreateAccount.layer.masksToBounds = YES;
    self.but_Continue.layer.cornerRadius = 5;
    self.but_Continue.layer.masksToBounds = YES;
    self.but_ContinueFinish.layer.cornerRadius = 5;
    self.but_ContinueFinish.layer.masksToBounds = YES;
    
    [self.spinnerLoaderStep1 stopAnimating];
    [self.spinnerLoaderStep2 stopAnimating];
    [self.spinnerLoaderStep3 stopAnimating];
    
    self.loginFacebookButton.delegate = self;
    self.loginFacebookButton.readPermissions = @[@"public_profile", @"email", @"user_photos", @"user_videos"];
    
    UIInterpolatingMotionEffect *centerX = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.x" type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
    
    centerX.maximumRelativeValue = @60;
    centerX.minimumRelativeValue = @-60;
    
    UIInterpolatingMotionEffect *centerY = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.y" type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
    
    centerY.maximumRelativeValue = @50;
    centerY.minimumRelativeValue = @-50;
    
    UIMotionEffectGroup *group = [[UIMotionEffectGroup alloc] init];
    group.motionEffects = @[centerX, centerY];
    
    [self.imageBackground addMotionEffect:group];
    
    self.lbl_version.text = [NSString stringWithFormat:NSLocalizedString(@"VERSION_X", nil), [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
    
    [self setNeedsStatusBarAppearanceUpdate];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if(self.sfAppRegister.currentUser != nil) {
        if(self.sfAppRegister.currentUser.firstName != nil && ![self.sfAppRegister.currentUser.firstName isEqualToString:@""]) {
            NSString *aux = [NSString stringWithFormat:@" %@", self.sfAppRegister.currentUser.lastName];
            self.lbl_UserName.text = [self.sfAppRegister.currentUser.firstName stringByAppendingString:aux];
        } else {
            self.lbl_UserName.text = self.sfAppRegister.currentUser.username;
        }
        self.lbl_UserEmail.text = self.sfAppRegister.currentUser.email;
        
        __weak typeof(self) selfDelegate = self;
        [selfDelegate.img_UserProfile sd_setImageWithURL:[NSURL URLWithString: selfDelegate.sfAppRegister.currentUser.photo] placeholderImage:[UIImage imageNamed:@"Avatar_White"] options:SDWebImageRefreshCached completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            if(image == nil){
                //if something went wrong.
            }else{
                //success downloaded.
            }
        }];
    }
    
    //Google Analytics
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"register"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}


-(BOOL)connected {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

-(void)moveToStep1
{
    @try {
        // check Login   0 = not started  1 = true   2 = false
        [self.sfAppRegister.settings setObject:@"1" forKey:@"CHECK_Login"];
        
      //  [self.sfAppRegister.currentUser validateSubscription:^(BOOL hasSubscription) {
           
            //self.sfAppRegister.currentUser.hasSubscription = hasSubscription;
            
            [self.sfAppRegister saveSettings];
            [self performSegueWithIdentifier:@"RegisterToStep1" sender:nil];
      //  }];
        
       
    }
    @catch (NSException *exception) {
        NSLog(@"StayLog: MoveToStep1 exception with error: %@", exception.description);
    }
}


-(void)createStayfilmAppSingleton:(NSNumber *)state
{
    if(self.sfAppRegister == nil)
    {
        self.sfAppRegister = [StayfilmApp sharedStayfilmAppSingletonWithDelegate:self];
    }
}
-(void)connectivityChanged:(BOOL)isConnected {
    if(isConnected) {
        [self createStayfilmAppSingleton:@1];
    } else {
//        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"NO_INTERNET_CONNECTION", nil)
//                                                                       message:NSLocalizedString(@"NO_INTERNET_CONNECTION_MESSAGE", nil)
//                                                                preferredStyle:UIAlertControllerStyleAlert];
//        
//        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
//                                                              handler:^(UIAlertAction * action) {}];
//        
//        [alert addAction:defaultAction];
//        [self presentViewController:alert animated:YES completion:nil];
    }
}

#pragma mark - Button Events

- (IBAction)keyboardDismissal:(id)sender {
    [self.view endEditing:YES];
}

- (IBAction)backButton_tapped:(id)sender {
    [self.view endEditing:YES];
    //[self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

BOOL isCreatingAccount = NO;
- (IBAction)but_CreateAccount_Clicked:(id)sender {
    [self.view endEditing:YES];
    self.lbl_feedback.text = @"";
    if(!isCreatingAccount) {
        isCreatingAccount = YES;
        runOnMainQueueWithoutDeadlocking(^{
            BOOL filledEmail = NO;
            if(self.txt_Email.text != nil && ![self.txt_Email.text isEqual:@""]) {
                filledEmail = YES;
            }
            else
            {
                self.lbl_feedback.alpha = 0.0;
                self.lbl_feedback.text = NSLocalizedString(@"FILL_REQUIRED_FIELD", nil);
                self.lbl_Email.alpha = 0.0;
                [self.lbl_feedback setHidden:NO];
                [self.lbl_Email setHidden:NO];
                [UIView animateWithDuration:0.35 animations:^{
                    self.lbl_Email.alpha = 1.0;
                    self.lbl_feedback.alpha = 1.0;
                }];
                self.lbl_Email.text = [NSLocalizedString(@"EMAIL", nil) stringByAppendingString:@" *"];
            }
            if(self.txt_PasswordStep1 != nil && ![self.txt_PasswordStep1.text isEqual:@""]) {
                if(filledEmail) { //filled credentials
                    self.lbl_Email.text = NSLocalizedString(@"EMAIL", nil);
                    self.lbl_PasswordStep1.text = NSLocalizedString(@"PASSWORD", nil);
                    if(self.txt_PasswordStep1.text.length < 5) {
                        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"PASSWORD_MIN_ERROR", nil)
                                                                                       message:NSLocalizedString(@"PASSWORD_MIN_ERROR_MESSAGE", nil)
                                                                                preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                              handler:^(UIAlertAction * action) {}];
                        
                        [alert addAction:defaultAction];
                        [self presentViewController:alert animated:YES completion:nil];
                    }
                    else //GOOD TO REGISTER
                    {
                        [(UIActivityIndicatorView *)[self.view viewWithTag:10] startAnimating];
                        [(UIActivityIndicatorView *)[self.view viewWithTag:10] setHidden:NO];

                        [self performSelectorInBackground:@selector(doRegister) withObject:nil];
                        return;
                    }
                }
            }
            else
            {
                self.lbl_feedback.alpha = 0.0;
                self.lbl_feedback.text = NSLocalizedString(@"FILL_REQUIRED_FIELD", nil);
                [self.lbl_feedback setHidden:NO];
                self.lbl_PasswordStep1.alpha = 0.0;
                [self.lbl_PasswordStep1 setHidden:NO];
                [UIView animateWithDuration:0.35 animations:^{
                    self.lbl_PasswordStep1.alpha = 1.0;
                    self.lbl_feedback.alpha = 1.0;
                }];
                self.lbl_PasswordStep1.text = [NSLocalizedString(@"PASSWORD", nil) stringByAppendingString:@" *"];
            }
        });
        isCreatingAccount = NO;
    }
}

-(void)doRegister {
    @try {
        NSMutableDictionary *registerArgs = [[NSMutableDictionary alloc] init];
        
        if(self.sfAppRegister == nil)
            self.sfAppRegister = [StayfilmApp sharedStayfilmAppSingleton];
        
        //choosing default 1/1/1970
        [registerArgs setObject:[NSString stringWithFormat:@"%d", 1]forKey:@"birthdayDay"];
        [registerArgs setObject:[NSString stringWithFormat:@"%d", 1]forKey:@"birthdayMonth"];
        [registerArgs setObject:[NSString stringWithFormat:@"%d", 1970]forKey:@"birthdayYear"];
        
        [registerArgs setObject:self.txt_Email.text forKey:@"email"];
        [registerArgs setObject:@"n" forKey:@"gender"];
        
        [registerArgs setObject:self.txt_PasswordStep1.text forKey:@"password"];
        
        User *registered = [User registerUserWithArguments:registerArgs andSFAppSingleton:self.sfAppRegister]; // already saves to sfApp.settings
        
        runOnMainQueueWithoutDeadlocking(^{
            if(registered != nil && registered.isLogged)
            {
                self.sfAppRegister.settings = nil;
                self.sfAppRegister.settings = [[NSMutableDictionary alloc] init];
                
                //Google Analytics Event
                id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"user"
                                                                      action:@"register-completed"
                                                                       label:@"email"
                                                                       value:@1] build]];
                
                NSData *dataSave = [NSKeyedArchiver archivedDataWithRootObject:registered];
                [[NSUserDefaults standardUserDefaults] setObject:dataSave forKey:@"currentUser"];
                
                self.sfAppRegister.currentUser = registered;
                // check Login   0 = not started  1 = true   2 = false
                [self.sfAppRegister.settings setObject:@"1" forKey:@"CHECK_Login"];
                [self.sfAppRegister.settings setObject:@"1" forKey:@"SF_User_Registered"];
                [self.sfAppRegister.settings setObject:registered.idUser forKey:@"idUser"];
                [self.sfAppRegister.settings setObject:registered.idSession forKey:@"idSession"];
                [self.sfAppRegister saveSettings];
                
                if(registered.firstName != nil) {
                    NSString *aux = [NSString stringWithFormat:@" %@", registered.lastName];
                    self.lbl_UserName.text = [registered.firstName stringByAppendingString:aux];
                } else {
                    self.lbl_UserName.text = registered.username;
                }
                self.lbl_UserEmail.text = registered.email;
                
                if(registered.photo != nil || [registered.photo isEqualToString:@""]) {
                    self.task = [self.thumbLoadSession dataTaskWithURL:[NSURL URLWithString:registered.photo] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                        if(error)
                        {
                            NSLog(@"StayLog: Avatar request error: %@", error);
                        }
                        else {
                            runOnMainQueueWithoutDeadlocking(^{
                                self.img_UserProfile.image = [UIImage imageWithData:data];
                            });
                        }
                    }];
                    [self.task resume];
                }
                
                self.view_Step1.alpha = 1.0;
                self.view_Created.alpha = 0.0;
                [self.view_Created setHidden:NO];
                [self.view_Loader setHidden:YES];
                
                [self but_Start_Clicked:nil];
//                if(YES) { // This is to skip the current flow and go directly to Step1
//                    [self but_Start_Clicked:nil];
//                } else {
//                    [UIView animateWithDuration:0.5 animations:^{
//                        [self.view_Step1 setFrame:self.finishedPoint];
//                        self.view_Step1.alpha = 0.0;
//                        [self.view_Created setFrame:self.showingPoint];
//                        self.view_Created.alpha = 1.0;
//                    } completion:^(BOOL finished) {
//                        [self.view_Step1 setHidden:YES];
//                        [self.but_back setHidden:YES];
//                        //[self performSelector:@selector(moveToStep1) withObject:nil afterDelay:1.0];
//                    }];
//                }
            }
            else if(registered.responseErrorMessage != nil && ![registered.responseErrorMessage isEqualToString:@""]) {
                self.lbl_feedback.alpha = 0.0;
                self.lbl_feedback.text = registered.responseErrorMessage;
                [self.lbl_feedback setHidden:NO];
                [UIView animateWithDuration:0.35 animations:^{
                    self.lbl_feedback.alpha = 1.0;
                }];
            }
            else
            {
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"REGISTRATION_ERROR", nil)
                                                                               message:NSLocalizedString(@"REGISTRATION_ERROR_MESSAGE", nil)
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {}];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
            [(UIActivityIndicatorView *)[self.view viewWithTag:10] stopAnimating];
            [(UIActivityIndicatorView *)[self.view viewWithTag:10] setHidden:YES];
            isCreatingAccount = NO;
        });
        
    }
    @catch (NSException *ex){
        NSLog(@"StayLog: Error but_CreateAccount_Click with error: %@",ex.description);
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CONNECTION_ERROR", nil)
                                                                       message:NSLocalizedString(@"CONNECTION_ERROR_MESSAGE", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        isCreatingAccount = NO;
    }
}

BOOL isClickingTerms = NO;
- (IBAction)but_TermsConditions_Clicked:(id)sender {
    if(!isClickingTerms) {
        isClickingTerms = YES;
        
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"terms-and-conditions"
                                                              action:@"link"
                                                               label:@""
                                                               value:@1] build]];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://www.stayfilm.com/institutional/terms"]];
        isClickingTerms = NO;
    }
}

//Step 2

- (IBAction)but_ContinueStep2_Clicked:(id)sender {
    BOOL valid = YES;
    //check if fields ar filled
    if(self.txt_FirstName.text == nil || [self.txt_FirstName.text isEqualToString:@""]) {
        self.lbl_FirstName.text = [NSLocalizedString(@"FIRST_NAME", nil) stringByAppendingString:@" *"];
        [self.lbl_Required setHidden:NO];
        valid = NO;
    }
    if(self.txt_LastName.text == nil || [self.txt_LastName.text isEqualToString:@""]) {
        self.txt_LastName.text = [NSLocalizedString(@"LAST_NAME", nil) stringByAppendingString:@" *"];
        [self.lbl_Required setHidden:NO];
        valid = NO;
    }
    if(self.txt_Password.text == nil || [self.txt_Password.text isEqualToString:@""]) {
        self.txt_Password.text = [NSLocalizedString(@"PASSWORD", nil) stringByAppendingString:@" *"];
        [self.lbl_Required setHidden:NO];
        valid = NO;
    }
    
    if(!valid) {
        return;
    }
    
    self.view_Step2.alpha = 1.0;
    self.view_Step3.alpha = 0.0;
    [self.view_Step3 setHidden:NO];
    self.but_Skip.alpha = 0.0;
    [self.but_Skip setHidden:NO];
    [UIView animateWithDuration:0.5 animations:^{
        [self.view_Step2 setFrame:self.finishedPoint];
        self.view_Step2.alpha = 0.0;
        [self.view_Step3 setFrame:self.showingPoint];
        self.view_Step3.alpha = 1.0;
        self.but_Skip.alpha = 1.0;
    } completion:^(BOOL finished) {
        [self.view_Step2 setHidden:YES];
    }];
    [self.view endEditing:YES];
}

//Step 3

- (IBAction)but_Gender_Clicked:(id)sender {
    self.picker_Gender.alpha = 0.0;
    [self.picker_Gender setHidden:NO];
    [self.picker_Date setHidden:YES];
    [UIView animateWithDuration:0.4 animations:^{
        self.picker_Gender.alpha = 1.0;
    } completion:^(BOOL finished) {
        [self.picker_Gender becomeFirstResponder];
    }];
}

- (IBAction)but_Birthday_Clicked:(id)sender {
    self.picker_Date.alpha = 0.0;
    [self.picker_Date setHidden:NO];
    [self.picker_Gender setHidden:YES];
    [UIView animateWithDuration:0.4 animations:^{
        self.picker_Date.alpha = 1.0;
    } completion:^(BOOL finished) {
        [self.picker_Date becomeFirstResponder];
    }];
//    if([self.picker_Gender selectedRowInComponent:0] == nil) {
//        [self.but_Gender setTitle:NSLocalizedString(@"UNDEFINED", nil) forState:UIControlStateNormal];
//        self.register_gender = @"n";
//    }
}


- (IBAction)pickerDate_ValueChanged:(id)sender {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-YYYY"];
    NSString *dateString = [dateFormatter stringFromDate:self.picker_Date.date];
    [self.but_Birthday setTitle:dateString forState:UIControlStateNormal];
    self.but_Birthday.titleLabel.text = dateString;
}



- (IBAction)but_ContinueFinish_Clicked:(id)sender {
}

- (IBAction)but_Skip_Clicked:(id)sender {
}


- (IBAction)but_Start_Clicked:(id)sender {
    if(!self.isStarting) {
        self.isStarting = YES;
        [self moveToStep1];
    }
}

- (IBAction)but_EditProfile_Clicked:(id)sender {
    [self performSegueWithIdentifier:@"goToEditProfile" sender:self];
}


#pragma mark - Picker Genre Delegate Methods

// The number of columns of data
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return (int)self.pickerGenreData.count;
}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSAttributedString *attString = [[NSAttributedString alloc] initWithString:self.pickerGenreData[row] attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    return attString;
}
//// The data to return for the row and component (column) that's being passed in
//- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
//{
//    return self.pickerGenreData[row];
//}

// Catpure the picker view selection
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    // maybe select next field focus?
    if([self.picker_Gender selectedRowInComponent:0] >= 0 && [self.picker_Gender selectedRowInComponent:0] < 3)
    {
        if([self.picker_Gender selectedRowInComponent:0] == 0)
        {
            [self.but_Gender setTitle:NSLocalizedString(@"UNDEFINED", nil) forState:UIControlStateNormal];
            self.register_gender = @"n";
        }
        else if([self.picker_Gender selectedRowInComponent:0] == 1)
        {
            [self.but_Gender setTitle:NSLocalizedString(@"MALE", nil) forState:UIControlStateNormal];
            self.register_gender = @"m";
        }
        else
        {
            [self.but_Gender setTitle:NSLocalizedString(@"FEMALE", nil) forState:UIControlStateNormal];
            self.register_gender = @"f";
        }
    }
}

#pragma mark - Input Field Events

-(void)txt_Email_Ended {
    if(self.txt_Email.text != nil && ![self.txt_Email.text isEqualToString: @""])
    {
        NSError *error = NULL;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"^[A-Z0-9a-z._%+-]+@.+.[A-Za-z]{2}[A-Za-z]*$"
                                                                               options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:self.txt_Email.text
                                                            options:0
                                                              range:NSMakeRange(0, [self.txt_Email.text length])];
        if(numberOfMatches > 0)
        {
            [self.view endEditing:YES];
            [self.spinnerLoaderStep1 stopAnimating];
            //TODO: check email if is available
            [self.txt_PasswordStep1 becomeFirstResponder];
            
            return;
        }
        else
        {
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"EMAIL_ERROR", nil)
                                                                           message:NSLocalizedString(@"EMAIL_ERROR_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
            return;
        }
    }
    else
    {
        self.lbl_feedback.alpha = 0.0;
        self.lbl_feedback.text = NSLocalizedString(@"FILL_REQUIRED_FIELD", nil);
        [self.lbl_feedback setHidden:NO];
        self.lbl_Email.alpha = 0.0;
        [self.lbl_Email setHidden:NO];
        [UIView animateWithDuration:0.35 animations:^{
            self.lbl_Email.alpha = 1.0;
            self.lbl_feedback.alpha = 1.0;
        }];
        self.lbl_Email.text = [NSLocalizedString(@"EMAIL", nil) stringByAppendingString:@" *"];
        self.lbl_PasswordStep1.text = [NSLocalizedString(@"PASSWORD", nil) stringByAppendingString:@" *"];
    }
}

-(void)txt_PasswordStep1_Ended {
    if(self.txt_Email.text != nil && ![self.txt_Email.text isEqualToString: @""])
    {
        if(self.txt_PasswordStep1.text.length < 5) {
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"PASSWORD_MIN_ERROR", nil)
                                                                           message:NSLocalizedString(@"PASSWORD_MIN_ERROR_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
            return;
        }
    }
    else
    {
        self.lbl_feedback.alpha = 0.0;
        self.lbl_feedback.text = NSLocalizedString(@"FILL_REQUIRED_FIELD", nil);
        [self.lbl_feedback setHidden:NO];
        self.lbl_PasswordStep1.alpha = 0.0;
        [self.lbl_PasswordStep1 setHidden:NO];
        [UIView animateWithDuration:0.35 animations:^{
            self.lbl_PasswordStep1.alpha = 1.0;
            self.lbl_feedback.alpha = 1.0;
        }];
        self.lbl_PasswordStep1.text = [NSLocalizedString(@"PASSWORD", nil) stringByAppendingString:@" *"];
        self.lbl_Email.text = [NSLocalizedString(@"EMAIL", nil) stringByAppendingString:@" *"];
    }
}

-(void)txt_FirstName_Ended {
    if(self.txt_FirstName.text == nil || [self.txt_FirstName.text isEqualToString:@""]) {
        self.lbl_FirstName.text = [NSLocalizedString(@"FIRST_NAME", nil) stringByAppendingString:@" *"];
        [self.lbl_Required setHidden:NO];
    }
    [self.txt_LastName becomeFirstResponder];
}

-(void)txt_LastName_Ended {
    if(self.txt_LastName.text == nil || [self.txt_LastName.text isEqualToString:@""]) {
        self.txt_LastName.text = [NSLocalizedString(@"LAST_NAME", nil) stringByAppendingString:@" *"];
        [self.lbl_Required setHidden:NO];
    }
    [self.txt_Password becomeFirstResponder];
}

-(void)txt_Password_Ended {
    if(self.txt_Password.text == nil || [self.txt_Password.text isEqualToString:@""]) {
        self.txt_Password.text = [NSLocalizedString(@"PASSWORD", nil) stringByAppendingString:@" *"];
        [self.lbl_Required setHidden:NO];
    }
    [self.view endEditing:YES];
}

#pragma mark - FB Login Connect Delegate

- (IBAction)but_FacebookCustom_tapped:(id)sender {
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login
     logInWithReadPermissions: @[@"public_profile", @"email", @"user_photos", @"user_videos"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"StayLog: FB: Process error");
             
             UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"FACEBOOK_ERROR", nil)
                                                                            message:NSLocalizedString(@"FACEBOOK_ERROR_MESSAGE", nil)
                                                                     preferredStyle:UIAlertControllerStyleAlert];
             
             UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                   handler:^(UIAlertAction * action) {}];
             
             [alert addAction:defaultAction];
         } else if (result.isCancelled) {
             NSLog(@"StayLog: FB: Cancelled");
             
             UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"USER_CANCELLED", nil)
                                                                            message:NSLocalizedString(@"USER_CANCELLED_MESSAGE", nil)
                                                                     preferredStyle:UIAlertControllerStyleAlert];
             
             UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                   handler:^(UIAlertAction * action) {}];
             
             [alert addAction:defaultAction];
         } else {
             NSLog(@"StayLog: FB: Logged in");
             
             self.didComeBackFromFBLogin = YES;
             
             [self.view_Loader setHidden:NO];
             
             if(self.sfAppRegister.settings[@"LOGGEDIN_Email"] != nil) {
                 [self.sfAppRegister.settings removeObjectForKey:@"LOGGEDIN_Email"];
             }
             
             [self performSelectorInBackground:@selector(facebookLoginNowOnStayfilmWithToken:) withObject:result.token.tokenString];
             
             self.loginAttempts = 0;
         }
     }];
}

- (void) loginButtonDidLogOut:(FBSDKLoginButton *)loginButton {
    [self.sfAppRegister.settings removeObjectForKey:@"facebookToken"];
    [self.sfAppRegister.settings removeObjectForKey:@"FB_id"];
    //[self.sfAppLogin.settings removeObjectForKey:@"User"];
    [self.sfAppRegister.settings removeObjectForKey:@"idSession"];
    [self.sfAppRegister.settings removeObjectForKey:@"idUser"];
    [self.sfAppRegister saveSettings];
}


- (void)loginButton:(FBSDKLoginButton *)loginButton didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result error:(NSError *)error {
    if(error == nil && result.token != nil && ![result.token.tokenString isEqual: @""])
    {
        self.didComeBackFromFBLogin = YES;
        
        [self.view_Loader setHidden:NO];
        
        if(self.sfAppRegister.settings[@"LOGGEDIN_Email"] != nil) {
            [self.sfAppRegister.settings removeObjectForKey:@"LOGGEDIN_Email"];
        }
        
        [self performSelectorInBackground:@selector(facebookLoginNowOnStayfilmWithToken:) withObject:result.token.tokenString];
        
        self.loginAttempts = 0;
    }
    else
    {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"FACEBOOK_ERROR", nil)
                                                                       message:NSLocalizedString(@"FACEBOOK_ERROR_MESSAGE", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
    }
}

-(void)facebookLoginNowOnStayfilmWithToken:(NSString*)p_token
{
    if(self.sfAppRegister.sfConfig != nil)
    {
        User *appUser = [User loginWithFacebook:p_token andSFAppSingleton:self.sfAppRegister]; // already saves to sfApp.settings
        self.sfAppRegister.currentUser = appUser;
        if(appUser == nil)
        {
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CONNECTION_ERROR", nil)
                                                                           message:NSLocalizedString(@"CONNECTION_ERROR_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
            NSLog(@"StayLog: fbconn error 2");
            //exit(0);
        }
        else if(!appUser.userExists && !appUser.isResponseError) // register user
        {
            self.sfAppRegister.settings = nil;
            self.sfAppRegister.settings = [[NSMutableDictionary alloc] init];
            //static NSString *token = nil;
            [self.sfAppRegister.settings setObject:p_token forKey:@"facebookToken"];
            [self.sfAppRegister saveSettings];
            //colocar a 1 checklogin
            //token = result.token.tokenString;
            __weak typeof(self) selfDelegate = self;
            dispatch_async(dispatch_get_main_queue(), ^{
                [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{ @"fields": @"id,birthday,email,first_name,last_name,gender" }] //TODO: use config information
                 startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
                 {
                     if (!error)
                     {
                         NSMutableDictionary *registerArgs = [[NSMutableDictionary alloc] init];
                         [registerArgs setObject:self.sfAppRegister.settings[@"facebookToken"] forKey:@"facebookToken"];
                         
                         if(result[@"birthday"] != nil)
                         {
                             NSDateFormatter *dateformat = [[NSDateFormatter alloc] init];
                             [dateformat setDateFormat:@"MM/dd/yyyy"];
                             NSDate * date = [dateformat dateFromString:result[@"birthday"]];
                             NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:date];
                             [registerArgs setObject:[NSString stringWithFormat:@"%d", (int)[components day]]forKey:@"birthdayDay"];
                             [registerArgs setObject:[NSString stringWithFormat:@"%d", (int)[components month]]forKey:@"birthdayMonth"];
                             [registerArgs setObject:[NSString stringWithFormat:@"%d", (int)[components year]]forKey:@"birthdayYear"];
                         }
                         if(result[@"email"] != nil)
                         {
                             [registerArgs setObject:(NSString *)result[@"email"] forKey:@"email"];
                         }
                         if(result[@"first_name"] != nil)
                         {
                             [registerArgs setObject:(NSString *)result[@"first_name"] forKey:@"firstName"];
                         }
                         if(result[@"last_name"] != nil)
                         {
                             [registerArgs setObject:(NSString *)result[@"last_name"] forKey:@"lastName"];
                         }
                         if(result[@"gender"] != nil)
                         {
                             if([result[@"gender"] isEqualToString:@"male"])
                             {
                                 [registerArgs setObject:@"m"forKey:@"gender"];
                             }
                             else if([result[@"gender"] isEqualToString:@"female"])
                             {
                                 [registerArgs setObject:@"f" forKey:@"gender"];
                             }
                             else
                             {
                                 [registerArgs setObject:@"n" forKey:@"gender"];
                             }
                         }
                         
                         User *registered = [User registerUserWithArguments:registerArgs andSFAppSingleton:self.sfAppRegister]; // already saves to sfApp.settings
                         selfDelegate.sfAppRegister.currentUser = registered;
                         BOOL registeredSucessfuly = NO;
                         if(registered != nil && registered.isLogged)
                         {
                             if(result[@"id"] != nil)
                             {
                                 // check Login   0 = not started  1 = true   2 = false
                                 [selfDelegate.sfAppRegister.settings setObject:@"1" forKey:@"CHECK_Login"];
                                 [selfDelegate.sfAppRegister.settings setObject:(NSString *)result[@"id"] forKey:@"FB_id"];
                                 [selfDelegate.sfAppRegister.settings setObject:[[FBSDKAccessToken currentAccessToken] tokenString] forKey:@"facebookToken"];
                                 [selfDelegate.sfAppRegister saveSettings];
                                 
                                 //Google Analytics Event
                                 id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                                 [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"user"
                                                                                       action:@"register-completed"
                                                                                        label:@"facebook"
                                                                                        value:@1] build]];
                                 
                                 registeredSucessfuly = YES;
                                 [registered setUserFacebookToken:selfDelegate.sfAppRegister.settings[@"facebookToken"] andSFAppSingleton:selfDelegate.sfAppRegister];
                             }
                             else
                             {
                                 registeredSucessfuly = NO;
                             }
                         }
                         else
                         {
                             registeredSucessfuly = NO;
                         }
                         if(registeredSucessfuly == YES)
                         {
                             NSData *dataSave = [NSKeyedArchiver archivedDataWithRootObject:registered];
                             [[NSUserDefaults standardUserDefaults] setObject:dataSave forKey:@"currentUser"];
                             
                             selfDelegate.sfAppRegister.currentUser = registered;
                             [selfDelegate.sfAppRegister.settings setObject:@"1" forKey:@"SF_User_Registered"];
                             
                             selfDelegate.lbl_UserName.text = (registered.firstName != nil) ? [registered.firstName stringByAppendingString:[NSString stringWithFormat:@" %@", registered.lastName]] : registered.username;
                             selfDelegate.lbl_Email.text = registered.email;
                             
                             if(registered.photo != nil || [registered.photo isEqualToString:@""]) {
                                 selfDelegate.task = [selfDelegate.thumbLoadSession dataTaskWithURL:[NSURL URLWithString:registered.photo] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                     if(error)
                                     {
                                         NSLog(@"StayLog: fb Avatar request error: %@", error);
                                     }
                                     else {
                                         runOnMainQueueWithoutDeadlocking(^{
                                             selfDelegate.img_UserProfile.image = [UIImage imageWithData:data];
                                         });
                                     }
                                 }];
                                 [selfDelegate.task resume];
                             }
                             
                             selfDelegate.view_Created.alpha = 0.0;
                             [selfDelegate.view_Created setHidden:NO];
                             [selfDelegate.view_Loader setHidden:YES];
                             [self but_Start_Clicked:nil];
//                             if(YES) { // This is to skip the current flow and go directly to Step1
//                                 [self but_Start_Clicked:nil];
//                             } else {
//                             [UIView animateWithDuration:0.5 animations:^{
//                                 [selfDelegate.view_Step1 setFrame:selfDelegate.finishedPoint];
//                                 selfDelegate.view_Step1.alpha = 0.0;
//                                 [selfDelegate.view_Created setFrame:selfDelegate.showingPoint];
//                                 selfDelegate.view_Created.alpha = 1.0;
//                             } completion:^(BOOL finished) {
//                                 [selfDelegate.view_Step1 setHidden:YES];
//                                 //[selfDelegate moveToStep1];
//                             }];
//                             }
                         }
                         else
                         {
                             NSLog(@"StayLog: fb Error on Register %@",error);
                             UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"REGISTRATION_ERROR", nil)
                                                                                            message:NSLocalizedString(@"REGISTRATION_ERROR_MESSAGE", nil)
                                                                                     preferredStyle:UIAlertControllerStyleAlert];
                             
                             UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                                   handler:^(UIAlertAction * action) {}];
                             
                             [alert addAction:defaultAction];
                             [selfDelegate presentViewController:alert animated:YES completion:nil];
                             //[selfDelegate.viewLoader setHidden:YES];
                         }
                     }
                     else
                     {
                         NSLog(@"StayLog: fb Error %@",error);
                         UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CONNECTION_ERROR", nil)
                                                                                        message:NSLocalizedString(@"CONNECTION_ERROR_MESSAGE", nil)
                                                                                 preferredStyle:UIAlertControllerStyleAlert];
                         
                         UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                               handler:^(UIAlertAction * action) {}];
                         
                         [alert addAction:defaultAction];
                         [selfDelegate presentViewController:alert animated:YES completion:nil];
                     }
                 }];
            });
        }
        else if(!appUser.isLogged)
        {
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"LOGIN_FAILED", nil)
                                                                           message:NSLocalizedString(@"LOGIN_FAILED_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
            NSLog(@"StayLog: fbconn error 3");
            //exit(0);
            
            // check Login   0 = not started  1 = true   2 = false
            [self.sfAppRegister.settings setObject:@"0" forKey:@"CHECK_Login"];
            [self.sfAppRegister saveSettings];
        }
        else //user logged in successfully
        {
            self.sfAppRegister.settings = nil;
            self.sfAppRegister.settings = [[NSMutableDictionary alloc] init];
            
            [self.sfAppRegister.settings setObject:@"1" forKey:@"CHECK_Login"];
            
            if([[FBSDKAccessToken currentAccessToken] userID] != nil)
                [self.sfAppRegister.settings setObject:[[FBSDKAccessToken currentAccessToken] userID] forKey:@"FB_id"];
            if([[FBSDKAccessToken currentAccessToken] tokenString] != nil)
                [self.sfAppRegister.settings setObject:[[FBSDKAccessToken currentAccessToken] tokenString] forKey:@"facebookToken"];
            [self.sfAppRegister.settings setObject:appUser.idUser forKey:@"idUser"];
            [self.sfAppRegister.settings setObject:appUser.idSession forKey:@"idSession"];
            [self.sfAppRegister saveSettings];
            self.sfAppRegister.currentUser = appUser;
            
            NSData *dataSave = [NSKeyedArchiver archivedDataWithRootObject:appUser];
            [[NSUserDefaults standardUserDefaults] setObject:dataSave forKey:@"currentUser"];
            
            //update user in background
            __weak typeof(self) selfDelegate = self;
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^(void){
                [selfDelegate.sfAppRegister.currentUser setUserFacebookToken:selfDelegate.sfAppRegister.settings[@"facebookToken"] andSFAppSingleton:selfDelegate.sfAppRegister];
            });
            
            [self.view_Loader setHidden:NO];
            [self moveToStep1];
            
            //            //Google Analytics Event
            //            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            //            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Botao"
            //                                                                  action:@"Connect"
            //                                                                   label:@"Iphone_Nativo_evento_101_botao_Connect"
            //                                                                   value:@1] build]];
        }
    }
    else
    {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CONNECTION_ERROR", nil)
                                                                       message:NSLocalizedString(@"CONNECTION_ERROR_MESSAGE", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        NSLog(@"StayLog: fbconn error 4");
        //exit(0);
       //[self showFacebookLoginButton];
        
        // check Login   0 = not started  1 = true   2 = false
        [self.sfAppRegister.settings setObject:@"0" forKey:@"CHECK_Login"];
        [self.sfAppRegister saveSettings];
    }
}


#pragma mark - Login Operations Delegate


-(void)loginFacebookDidFinish:(LoginFacebookOperation *)loginOP {
    // switch codemessage
    
    switch (loginOP.messageCode) {
        case LOGIN_SUCCESSFUL:
        {
            self.loginAttempts = 0;
            self.view_Created.alpha = 0.0;
            [self.view_Created setHidden:NO];
            [self.view_Loader setHidden:YES];
            [UIView animateWithDuration:0.5 animations:^{
                [self.view_Step1 setFrame:self.finishedPoint];
                self.view_Step1.alpha = 0.0;
                [self.view_Created setFrame:self.showingPoint];
                self.view_Created.alpha = 1.0;
            } completion:^(BOOL finished) {
                [self.view_Step1 setHidden:YES];
                [self performSelector:@selector(moveToStep1) withObject:nil afterDelay:1.0];
            }];
        }
            break;
            
        default:
        {
            ++self.loginAttempts;
            [self.loginFacebookButton setHidden:NO];
            [self.spinnerLoaderStep1 stopAnimating];
        }
            break;
    }
    
}

-(void)loginFacebookDidFail:(LoginFacebookOperation *)loginOP {
    
    ++self.loginAttempts;
    if(self.loginAttempts >= 3)
    {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"FACEBOOK_FALIED", nil)
                                                                       message:NSLocalizedString(@"FACEBOOK_FALIED_MESSAGE", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        self.loginAttempts = 0;
        return;
    }
    
    BOOL retryLogin = NO;
    switch (loginOP.messageCode) {
        case CONNECTION_ERROR:
        {
            retryLogin = YES;
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CONNECTION_ERROR", nil)
                                                                           message:NSLocalizedString(@"CONNECTION_ERROR_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
            break;
        case USER_CANCELLED:
        {
            retryLogin = NO;
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"USER_CANCELLED", nil)
                                                                           message:NSLocalizedString(@"USER_CANCELLED_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
            break;
        case LOGIN_FAILED:
        {
            retryLogin = YES;
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"LOGIN_FAILED", nil)
                                                                           message:NSLocalizedString(@"LOGIN_FAILED_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
            break;
        case REGISTRATION_ERROR:
        {
            retryLogin = YES;
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"REGISTRATION_ERROR", nil)
                                                                           message:NSLocalizedString(@"REGISTRATION_ERROR_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
            break;
        case NO_CONFIG_LOADED:
        {
            retryLogin = YES;
            self.loginAttempts--;
            // say nothing, just try again
            if(self.sfAppRegister != nil)
                self.sfAppRegister.sfConfig = [SFConfig getAllConfigsFromWS:self.sfAppRegister.wsConfig];
            else
                self.sfAppRegister = [StayfilmApp sharedStayfilmAppSingletonWithDelegate:self];
            break;
        }
        case NO_INTERNET_CONNECTION:
        {
            retryLogin = NO;
            self.loginAttempts--;
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"NO_INTERNET_CONNECTION", nil)
                                                                           message:NSLocalizedString(@"NO_INTERNET_CONNECTION_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
            
            [NSTimer scheduledTimerWithTimeInterval:2.0 repeats:YES block:^(NSTimer * _Nonnull timer) {
                if([self connected])
                {
                    [timer invalidate];
                    timer = nil;
                    LoginFacebookOperation *loginOp = [[LoginFacebookOperation alloc] initWithDelegate:self andSFApp:self.sfAppRegister];
                    [self.sfAppRegister.operationsQueue addOperation:loginOp];
                }
            }];
            break;
        }
        case -1:
        {
            retryLogin = YES;
            NSLog(@"StayLog: loginDidFail with EXCEPTION (messageCode -1)");
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"LOGIN_FAILED", nil)
                                                                           message:NSLocalizedString(@"LOGIN_FAILED_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
            break;
        default:
        {
            retryLogin = YES;
        }
            break;
    }
    
    if(retryLogin)
    {
        LoginFacebookOperation *logOperation = [[LoginFacebookOperation alloc] initWithDelegate:self andSFApp:self.sfAppRegister];
        [self.sfAppRegister.operationsQueue addOperation:logOperation];
    }
    else
    {
        [self.loginFacebookButton setHidden:NO];
        [self.spinnerLoaderStep1 setHidden:YES];
        [self.spinnerLoaderStep1 stopAnimating];
    }
    
    [loginOP cancel];
}


#pragma mark - Navigation
BOOL isMovingToEdit;
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"goToEditProfile"] && !isMovingToEdit) {
        isMovingToEdit = YES;
        //[self.navigationController setNavigationBarHidden:NO animated:YES];
        isMovingToEdit = NO;
    }
    
}


@end
