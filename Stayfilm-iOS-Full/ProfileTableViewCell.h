//
//  ProfileTableViewCell.h
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 23/04/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Movie.h"
#import <SDWebImage/UIImageView+WebCache.h>

@protocol ProfileDelegate<NSObject>

- (void)didSelectShare:(NSInteger) position;
- (void)didSelectOptions:(NSInteger) position;
- (void)didTapPlay:(NSInteger) position;
- (void)didTapPrivacy:(NSInteger) position;

@end


@interface ProfileTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *movieTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *publishedTimeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *thumbnailImage;
@property (weak, nonatomic) IBOutlet UIImageView *permissionsImage;

@property (weak, nonatomic) IBOutlet UIButton *playButton;

@property (weak, nonatomic) IBOutlet UILabel *shareLabel;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (weak, nonatomic) IBOutlet UIButton *optionsButton;
@property (weak, nonatomic) IBOutlet UIImageView *logoImage;

@property (nonatomic, strong) Movie *movie;

- (void)initCell;
@property (nonatomic, assign) id<ProfileDelegate> delegate;
@end
