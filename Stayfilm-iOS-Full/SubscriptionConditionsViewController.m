//
//  ConditionsViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 5/3/19.
//  Copyright © 2019 Stayfilm. All rights reserved.
//

#import "SubscriptionConditionsViewController.h"
#import "Constants.h"

@interface SubscriptionConditionsViewController ()

@end

@implementation SubscriptionConditionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.conditionsLabel.text = NSLocalizedString(@"CONDITIONS_LIST", nil);
    self.conditionTitleLabel.text = NSLocalizedString(@"CONDITIONS_TITLE", nil);
    
    if([[UIScreen mainScreen] bounds].size.height == kIPhone5Height){
        UIFont *font = self.conditionsLabel.font;
        self.conditionsLabel.font = [font fontWithSize:13.5];
    }
    
}
- (IBAction)okAction:(id)sender {
    
     [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
