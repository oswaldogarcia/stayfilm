//
//  UploadBlob.m
//  Stayfilm for Messenger
//
//  Created by Henrique Ormonde on 20/05/15.
//  Copyright (c) 2015 Stayfilm. All rights reserved.
//

@import AssetsLibrary;
#import <UIKit/UIKit.h>
#import "UploadBlob.h"
#import "Media.h"
#import "Job.h"

@interface UploadBlob ()

@property (nonatomic, strong) AuthenticationCredential *credential;
@property (atomic, strong) CloudStorageClient *client;
@property (nonatomic, strong) NSArray *container;
@property (nonatomic) int mycontainerIndex;
@property (nonatomic, strong) NSMutableArray *idMediasAvailable;
@property (nonatomic, strong) NSMutableArray *confirmMedias;
@property (nonatomic, strong) NSMutableDictionary *uploadedMediaID;
//@property (nonatomic, strong) NSArray *blobArray;
@property (nonatomic) BOOL interrupted;

@end

@implementation UploadBlob

@synthesize isUploading, isPreparingUpload, listUploadMedias, listUploadedMedias;
@synthesize credential, client, container, mycontainerIndex, interrupted, confirmMedias, uploadedAlbum, idMediasAvailable, uploadedMediaID;//, blobArray;
@synthesize uploadedCount, totalUploadCount, allUploadedLock;

//static UploadBlob *singletonObject = nil;

+ (UploadBlob *) sharedUploadBlobSingleton
{
    static UploadBlob *singletonObject = nil;
    @synchronized(self)
    {
        if(singletonObject == nil)
        {
            singletonObject = [[self alloc] init];
        }
    }
    return singletonObject;
}

- (id)init
{
    if( self = [super init])
    {
        self.credential = [AuthenticationCredential credentialWithAzureServiceAccount:@"grabber" accessKey:@"gz1026zIglFIAGnHuKKGRHDrX4TmxpHT8L0G0bpxVYnIQ4JNtzg7slIT+cTr6XbSwXfhJgGfIvpmN21DoZRAoQ=="];
        self.client = [CloudStorageClient storageClientWithCredential:credential];
        self.client.delegate = self;
        
        self.isUploading = NO;
        self.isPreparingUpload = NO;
        self.listUploadMedias = [[NSMutableArray alloc] init];
        self.listUploadedMedias = [[NSMutableArray alloc] init];
        self.container = nil;
        //self.blobArray = nil;
        self.confirmMedias = [[NSMutableArray alloc] init];
        self.idMediasAvailable = [[NSMutableArray alloc] init];
        self.uploadedMediaID = [[NSMutableDictionary alloc] init];
        self.uploadedAlbum = nil;
        self.interrupted = NO;
        
        @try {
        // get all blob containers
            [self.client getBlobContainersWithBlock:^(NSArray *containers, NSError *error)
             {
                 if (error)
                 {
                     NSLog(@"%@",[error localizedDescription]);
                 }
                 
                 else
                 {
                     NSLog(@"%d containers were found…",(int)[containers count]);
                     if([containers count]!=0)
                     {
                         container = [[NSArray alloc]initWithArray:containers];
                         int index = 0;
                         for(BlobContainer *cont in containers)
                         {
                             if([[[cont.name lowercaseString] lowercaseString] isEqualToString:@"mycontainer"])
                             {
                                 self.mycontainerIndex = index;
                                 break;
                             }
                             ++index;
                         }
                     }
                 }
                 
             }];
        }
        @catch (NSException *exception) {
            
            self.client = nil;
            self = nil;
        }
    }
    return self;
}

//Receives media array from anywhere and adds it to the list to upload
- (BOOL)addMedias:(NSArray *)p_medias
{
    BOOL added = NO;
    int count = 0;
    self.isPreparingUpload = YES;
    for (id object in p_medias) {
        if(![self.listUploadMedias containsObject:object])
        {
            if(self.isUploading)
            {
                self.interrupted = YES;
            }
            [self.listUploadMedias addObject:object];
            added = YES;
            ++count;
        }
    }
    if(added)
    {
        if(self.idMediasAvailable == nil || self.idMediasAvailable.count == 0)
        {
            self.idMediasAvailable = [Media createXMediasWithX:count];
        }
        else
        {
            NSArray *medias = [Media createXMediasWithX:count];
            for (NSString *object in medias)
            {
                [self.idMediasAvailable addObject:object];
            }
        }
        if(!self.isUploading)
        {
            [self uploadAllMedias];
        }
    }
    self.isPreparingUpload = NO;
    return added;
}

// resets albums for new upload
- (BOOL)resetUpload
{
    self.uploadedAlbum = nil;
    self.listUploadedMedias = nil;
    self.listUploadedMedias = [[NSMutableArray alloc] init];
    self.listUploadMedias = nil;
    self.listUploadMedias = [[NSMutableArray alloc] init];
    self.idMediasAvailable = nil;
    self.idMediasAvailable = [[NSMutableArray alloc] init];
    self.confirmMedias = nil;
    self.confirmMedias = [[NSMutableArray alloc] init];
    self.uploadedMediaID = nil;
    self.uploadedMediaID = [[NSMutableDictionary alloc] init];
    self.interrupted = NO;
    self.isUploading = NO;
    self.isPreparingUpload = NO;
    
    self.uploadedCount = 0;
    self.totalUploadCount = 0;
    
    return YES;
}

- (BOOL)didUploadFinish
{
    if(self.uploadedAlbum == nil)
    {
        return NO;
    }
    else
    {
        return YES;
    }
}

//-(void)uploadMedia:(NSDictionary *)object
//{
//    @try {
//        ALAssetsLibrary *assetLibrary=[[ALAssetsLibrary alloc] init];
//        [assetLibrary assetForURL:[object valueForKey:UIImagePickerControllerReferenceURL] resultBlock:^(ALAsset *asset)
//         {
//             ALAssetRepresentation *rep = [asset defaultRepresentation];
//             Byte *buffer = (Byte*)malloc(rep.size);
//             NSUInteger buffered = [rep getBytes:buffer fromOffset:0.0 length:rep.size error:nil];
//             NSData *data = [NSData dataWithBytesNoCopy:buffer length:buffered freeWhenDone:YES];//this is NSData may be what you want
//             //[data writeToFile:photoFile atomically:YES];//you can save image later
//             UIImage *image = [UIImage imageWithData:data];
//             
//             CGFloat compression = 0.9f;
//             CGFloat maxCompression = 0.5f;
//             int maxFileSize = 1280*720; //ou usar 40960 = 40MB ?
//             
//             NSData *imageData = UIImageJPEGRepresentation(image, compression);
//             
//             while ([imageData length] > maxFileSize && compression > maxCompression)
//             {
//                 compression -= 0.1;
//                 imageData = UIImageJPEGRepresentation(image, compression);
//             }
//             
//             NSString *guid = [self.idMediasAvailable lastObject];
//             //NSString *guid = [[NSUUID UUID] UUIDString];
//             NSString *boundary = @"-endUploadBoundary-";
//             NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
//             
//             [client addBlobToContainer:[container objectAtIndex:self.mycontainerIndex] blobName:[NSString stringWithFormat:@"%@.jpg", guid] contentData:imageData contentType:contentType ];
//             
//             [uploadedMediaID setObject:object forKey:guid];
//             [confirmMedias addObject:guid];
//             [listUploadedMedias addObject:object];
//             [self.idMediasAvailable removeLastObject];
//             
////             [self.allUploadedLock lock];
////             [self.allUploadedLock unlockWithCondition:0];
//         }
//                         failureBlock:^(NSError *err) {
//                             NSLog(@"Error: %@",[err localizedDescription]);
//                             [self.allUploadedLock lock];
//                             [self.allUploadedLock unlockWithCondition:0];
//                         }];
//
//    }
//    @catch (NSException *exception) {
//        NSLog(@"Error uploadMedia: %@",[object valueForKey:UIImagePickerControllerReferenceURL]);
//    }
//}


-(void)addBlobToContainerHack:(BlobContainer *)p_container blobName:(NSString *)blobName contentData:(NSData *)contentData contentType:(NSString*)contentType
{
    [self.client addBlobToContainer:p_container blobName:blobName contentData:contentData contentType:contentType withBlock:^(NSError *error)
     {
         UploadBlob *this = [UploadBlob sharedUploadBlobSingleton];
         if(error)
         {
             [this storageClient:nil didFailRequest:nil withError:error];
             return;
         }
         
         [this storageClient:nil didAddBlobToContainer:p_container blobName:blobName];
     }];
}

// uploads all unuploaded medias
-(void)uploadAllMedias
{
    @try
    {
        if(!self.isUploading)
        {
            self.isUploading = YES;
            
            self.uploadedCount = self.listUploadedMedias.count;
            self.totalUploadCount = self.listUploadMedias.count;
            
            //NSAssert(![NSThread isMainThread], @"can't be called on the main thread due to ALAssetLibrary limitations");
            for (NSDictionary *object in self.listUploadMedias)
            {
                self.allUploadedLock = [[NSConditionLock alloc] initWithCondition:1];
                if([listUploadedMedias containsObject:object])
                {
                    continue;
                }
                else // file not uploaded yet
                {
                    //UIImage *imageAsset = nil;
                    //[self performSelectorInBackground:@selector(uploadMedia:) withObject:object];
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        @try {
                            ALAssetsLibrary *assetLibrary=[[ALAssetsLibrary alloc] init];
                            [assetLibrary assetForURL:[object valueForKey:UIImagePickerControllerReferenceURL] resultBlock:^(ALAsset *asset)
                             {
                                 ALAssetRepresentation *rep = [asset defaultRepresentation];
                                 Byte *buffer = (Byte*)malloc(rep.size);
                                 NSUInteger buffered = [rep getBytes:buffer fromOffset:0.0 length:rep.size error:nil];
                                 NSData *data = [NSData dataWithBytesNoCopy:buffer length:buffered freeWhenDone:YES];//this is NSData may be what you want
                                 //[data writeToFile:photoFile atomically:YES];//you can save image later
                                 UIImage *image = [UIImage imageWithData:data];
                                 
                                 CGFloat compression = 0.9f;
                                 CGFloat maxCompression = 0.5f;
                                 int maxFileSize = 1280*720; //TODO ou usar 40960 = 40MB ?
                                 
                                 NSData *imageData = UIImageJPEGRepresentation(image, compression);
                                 
                                 while ([imageData length] > maxFileSize && compression > maxCompression)
                                 {
                                     compression -= 0.1;
                                     imageData = UIImageJPEGRepresentation(image, compression);
                                 }
                                 
                                 NSString *guid = [self.idMediasAvailable lastObject];
                                 if(guid == nil)
                                 {
                                     guid = [[NSUUID UUID] UUIDString];
                                 }
                                 NSString *boundary = @"---endUploadBoundary---";
                                 NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
                                 
//                                 UploadBlob *this = [UploadBlob sharedUploadBlobSingleton];
//                                 [this addBlobToContainerHack:[container objectAtIndex:self.mycontainerIndex] blobName:[NSString stringWithFormat:@"%@.jpg", guid] contentData:imageData contentType:contentType];
                                 
//                                 [client addBlobToContainer:[container objectAtIndex:self.mycontainerIndex] blobName:[NSString stringWithFormat:@"%@.jpg", guid] contentData:imageData contentType:contentType ];
                                 [self.client addBlobToContainer:[container objectAtIndex:self.mycontainerIndex] blobName:[NSString stringWithFormat:@"%@.jpg", guid] contentData:imageData contentType:contentType withBlock:^(NSError *error)
                                  {
                                      UploadBlob *this = [UploadBlob sharedUploadBlobSingleton];
                                      if(error)
                                      {
                                          [this storageClient:nil didFailRequest:nil withError:error];
                                          return;
                                      }
                                      
                                      //[this storageClient:nil didAddBlobToContainer:nil blobName:[NSString stringWithFormat:@"%@.jpg", guid]];
                                      [this storageClientHackDidAddBlobToContainerWithID:guid andObject:object];
                                      
                                  }];
                                 
                                 //             [self.allUploadedLock lock];
                                 //             [self.allUploadedLock unlockWithCondition:0];

                             }
                                 failureBlock:^(NSError *err) {
                                     NSLog(@"Error: %@",[err localizedDescription]);
                                     [self.allUploadedLock lock];
                                     [self.allUploadedLock unlockWithCondition:0];
                                 }];
                            
                        }
                        @catch (NSException *exception) {
                            NSLog(@"Error uploadMedia: %@",[object valueForKey:UIImagePickerControllerReferenceURL]);
                        }
                    });
                    
                    [self.allUploadedLock lockWhenCondition:0];
                    [self.allUploadedLock unlock];

                    self.uploadedCount = self.listUploadedMedias.count;
                }
                if(self.interrupted)
                {
                    break;
                }
                self.allUploadedLock = nil;
            }
            if(self.interrupted)
            {
                self.interrupted = NO;
                [self uploadAllMedias];
            }
            else // not interrupted, then it finished all uploads
            {
                if(self.uploadedAlbum == nil)
                {
                    NSMutableArray *medias = [[NSMutableArray alloc] init];
                    for (NSString *object in self.confirmMedias)
                    {
                        NSDictionary *mediaDictionary = @{ @"idmedia" : object,
                                                           @"ext" : @"jpg" };
                        [medias addObject:mediaDictionary];
                    }
                    NSString *idAlbum = [[NSString alloc] init];
                    NSMutableArray *failedMedias = [Media confirmMedias:medias WithIdAlbum:nil andOverrideIdAlbum:&idAlbum];
                    BOOL gotAlbum = NO;
                    if(idAlbum != nil)
                    {
                        self.uploadedAlbum = [AlbumSF getAlbumWithID:idAlbum];
                        gotAlbum = YES;
                        self.uploadedAlbum.idMedias = self.confirmMedias;
                        Job* analyzer =[Job postImageAnalyzerWithAlbumID:idAlbum];
                    }
                    if(failedMedias == nil)
                    {
                        //TODO Exception on the request
                        self.interrupted = YES;
                        NSLog(@"Error uploadAllMedias: failedMedias exception");
                    }
                    else if([[failedMedias firstObject] isEqualToString:@"0"])
                    {
                        //TODO WebService response status with error
                        NSLog(@"Error uploadAllMedias: failedMedias response not OK");
                    }
                    else if([failedMedias count] > 0)
                    {
                        //TODO Some medias failed
                        self.interrupted = YES;
                        for (NSString *idMedia in failedMedias) {
                            if(self.uploadedMediaID[idMedia] != nil)
                            {
                                [listUploadMedias addObject:[self.uploadedMediaID objectForKey:idMedia]];
                                [listUploadedMedias removeObject:[self.uploadedMediaID objectForKey:idMedia]];
                            }
                        }
                    }
                    else if(!gotAlbum)
                    {
                        // Album created successfully
                        self.uploadedAlbum = [AlbumSF getAlbumWithID:idAlbum];
                        self.uploadedAlbum.idMedias = self.confirmMedias;
                        Job* analyzer = [Job postImageAnalyzerWithAlbumID:idAlbum];
                    }

                    //self.uploadedAlbum = [AlbumSF createAlbumWithMedias:confirmMedias withTitle:nil];
                    if (self.uploadedAlbum == nil || self.uploadedAlbum.idAlbum == nil)
                    {
                        //TODO error creating album...
                        NSLog(@"Error uploadAllMedias: uploadedAlbum is nil");
                    }
                    else
                    { // TODO Add all selected medias
                        self.uploadedAlbum = [AlbumSF createAlbumWithMedias:confirmMedias withTitle:nil];
                    }
                }
                else //Already have an Album, lets just add the new Medias
                {
                    //NSArray *failedMedias = [Media confirmMedias:confirmMedias WithIdAlbum:self.uploadedAlbum.idAlbum andOverrideIdAlbum:nil];
                    
                    NSMutableArray *medias = [[NSMutableArray alloc] init];
                    for (NSString *object in self.confirmMedias)
                    {
                        NSDictionary *mediaDictionary = @{ @"idmedia" : object,
                                                           @"ext" : @"jpg" };
                        
                        [medias addObject:mediaDictionary];
                    }
                    NSArray *failedMedias = [Media confirmMedias:medias WithIdAlbum:self.uploadedAlbum.idAlbum andOverrideIdAlbum:nil];
                    if(failedMedias == nil)
                    {
                        //TODO Exception on the request
                        self.interrupted = YES;
                        NSLog(@"Error uploadAllMedias: failedMedias exception");
                    }
                    else if([[failedMedias firstObject] isEqualToString:@"0"])
                    {
                        //TODO WebService response status with error
                        NSLog(@"Error uploadAllMedias: failedMedias response not OK");
                    }
                    else if([failedMedias count] > 0)
                    {
                        //TODO Some medias failed
                        self.interrupted = YES;
                        for (NSString *idMedia in failedMedias) {
                            if(self.uploadedMediaID[idMedia] != nil)
                            {
                                [listUploadMedias addObject:[self.uploadedMediaID objectForKey:idMedia]];
                                [listUploadedMedias removeObject:[self.uploadedMediaID objectForKey:idMedia]];
                            }
                        }
                    }
                    else
                    {
                        // Files added successfully
                        self.uploadedAlbum = [AlbumSF getAlbumWithID:self.uploadedAlbum.idAlbum];
                        self.uploadedAlbum.idMedias = self.confirmMedias;
                    }
                    
                    //self.uploadedAlbum = [AlbumSF createAlbumWithMedias:confirmMedias withTitle:nil];
                    if (self.uploadedAlbum == nil || self.uploadedAlbum.idAlbum == nil)
                    {
                        //TODO error creating album...
                        NSLog(@"Error uploadAllMedias: uploadedAlbum is nil");
                    }
                    else
                    {
                        Job* analyzer =[Job postImageAnalyzerWithAlbumID:self.uploadedAlbum.idAlbum ];
                    }
                }
                
                if(!self.interrupted)
                {
                    self.isUploading = NO;
                    self.interrupted = NO;
                }
                else
                {
                    self.isUploading = NO;
                    self.interrupted = NO;
                    self.allUploadedLock = nil;
                    [self uploadAllMedias];
                }
            }
        }
    }
    @catch ( NSException *ex )
    {
        if(self.interrupted)
        {
            self.interrupted = NO;
        }
        self.isUploading = NO;
        self.allUploadedLock = nil;
        [self uploadAllMedias];
    }
}


//-(IBAction)getBlob:(id)sender
//{
//    // get all blobs within a container
//    [client getBlobs:[container objectAtIndex:0] withBlock:^(NSArray *blobs, NSError *error)
//     {
//         if (error)
//         {
//             NSLog(@"%@",[error localizedDescription]);
//         }
//         else
//         {
//             NSLog(@"%d blobs were found in the images container…",(int)[blobs count]);
//             if([blobs count]!=0)
//             {
//                 blobArray = [[NSArray alloc]initWithArray:blobs];
//             }
//             
//             for (int i=0; i<[blobArray count]; i++)
//             {
//                 NSLog(@"%@",[blobArray objectAtIndex:i]);
//                 
//             }
//             
//         }
//     }];
//}
//
//-(IBAction)addBlob:(id)sender
//{
//    UIImage *image=[UIImage imageNamed:@"image.png"];
//    NSData *imageData = UIImagePNGRepresentation(image);
//    NSString *boundary = @"endUploadBoundary";
//    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
//    [client addBlobToContainer:[container objectAtIndex:0] blobName:@"image1.png" contentData:imageData contentType:contentType ];
//}
//
//-(IBAction)deleteBlob:(id)sender
//{
//    [client deleteBlob:[blobArray  objectAtIndex:[blobArray count]-3] withBlock:^(NSError * error)
//     {
//         if (error)
//         {
//             NSLog(@"%@",[error localizedDescription]);
//         }
//         else
//         {
//             NSLog(@"Delete Sucessfully...");
//         }
//     }];
//}

-(NSArray*)getUploadedMediasIDs {
    NSMutableArray *medias = nil;
    @try {
        medias = [[NSMutableArray alloc] init];
        
        for (NSString* key in self.uploadedMediaID) {
            [medias addObject:key];
        }
    }
    @catch (NSException *exception) {
        medias = nil;
    }
    @finally {
        return medias;
    }
}

-(void)storageClientHackDidAddBlobToContainerWithID:(NSString *)guid andObject:(NSDictionary *)object
{
    [self.uploadedMediaID setObject:object forKey:guid];
    [self.confirmMedias addObject:guid];
    [self.listUploadedMedias addObject:object];
    [self.idMediasAvailable removeLastObject];
    
    NSString *imageUrl=[NSString stringWithFormat:@"http://grabber.blob.core.windows.net/mycontainer/%@", [NSString stringWithFormat:@"%@.jpg", guid]];
    NSLog(@"blob uploaded %@",imageUrl);
    [self.allUploadedLock lock];
    [self.allUploadedLock unlockWithCondition:0];
}

- (void)storageClient:(CloudStorageClient *)client didAddBlobToContainer:(BlobContainer *)container blobName:(NSString *)blobName;
{
//    NSString *imageUrl=[NSString stringWithFormat:@"http://grabber.blob.core.windows.net/mycontainer/%@",blobName];
//    NSLog(@"blob %@",imageUrl);
//    [self.allUploadedLock lock];
//    [self.allUploadedLock unlockWithCondition:0];
    
}

- (void)storageClient:(CloudStorageClient *)client didFailRequest:(NSURLRequest*)request withError:(NSError *)error
{
    NSLog(@"blob upload ERROR: %@",error);
    [self.allUploadedLock lock];
    [self.allUploadedLock unlockWithCondition:0];
}



@end
