//
//  MovieMakerStep1ViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Henrique Ormonde on 20/09/17.
//  Copyright © 2017 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StayfilmApp.h"
#import "LoginFacebookOperation.h"
#import "LoginToSocialNetworkOperation.h"
#import "Uploader.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Popover_PhotoAccessPermission_ViewController.h"
#import "StoryState.h"
#import "AutoFilm.h"

//@protocol UINavigationControllerBackButtonDelegate <NSObject>
///**
// * Indicates that the back button was pressed.
// * If this message is implemented the pop logic must be manually handled.
// */
//- (void)backButtonPressed;
//@end
//
//@interface UINavigationController(BackButtonHandler)
//@end
//
//@implementation UINavigationController(BackButtonHandler)
//- (BOOL)navigationBar:(UINavigationBar *)navigationBar shouldPopItem:(UINavigationItem *)item
//{
//    UIViewController *topViewController = self.topViewController;
//    BOOL wasBackButtonClicked = topViewController.navigationItem == item;
//    SEL backButtonPressedSel = @selector(backButtonPressed);
//    if (wasBackButtonClicked && [topViewController respondsToSelector:backButtonPressedSel]) {
//        [topViewController performSelector:backButtonPressedSel];
//        return NO;
//    }
//    else if(wasBackButtonClicked){
//        [self popViewControllerAnimated:YES];
//        return YES;
//    }
//    return YES;
//}
//@end


@interface MovieMakerStep1ViewController : UIViewController <StayfilmUploaderDelegate, LoginFacebookOperationDelegate, LoginToSocialNetworkOperationDelegate, Popover_PhotoPermissionAccess_ViewControllerDelegate, StoryStateDelegate, UIViewControllerTransitioningDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UIImagePickerControllerDelegate>

@property (nonatomic, strong) NSString *titleText;
@property (weak, nonatomic) IBOutlet UIButton *profileButton;
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UILabel *txt_Title;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lowerViewConstraint;
@property (weak, nonatomic) IBOutlet UIView *view_Editing;
@property (weak, nonatomic) IBOutlet UIButton *but_edit_Back;
@property (weak, nonatomic) IBOutlet UIButton *but_edit_Cancel;
@property (weak, nonatomic) IBOutlet UIButton *but_edit_Confirm;

@property (weak, nonatomic) IBOutlet UICollectionView *suggestionCollection;
@property (weak, nonatomic) IBOutlet UIView *but_cameraRoll;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_socialHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_socialFacebook;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_socialInstagram;

@property (weak, nonatomic) IBOutlet UIImageView *camera_arrow;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *camera_spinner;
@property (weak, nonatomic) IBOutlet UIView *tapToOpenView;
@property (weak, nonatomic) IBOutlet UIImageView *tapToOpenImage;

@property (strong, nonatomic) NSMutableArray *initialEditedMedias;

@property (strong, nonatomic) AutoFilm *autoFilm;
@property (weak, nonatomic) IBOutlet UIImageView *autoFilmImage;
@property (weak, nonatomic) IBOutlet UILabel *autoFilmLabel;
@property (weak, nonatomic) IBOutlet UIButton *autoFilmButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *autoFilmConstraint;
@property (weak, nonatomic) IBOutlet UIView *autoFilmView;



-(void)reloadAlbumsCounters;
-(void)changeSelectedMedias:(NSMutableArray *)p_selected;
-(void)changeEditedMedias:(NSMutableArray *)p_selected;
-(void)pickedSelectedMedias:(NSMutableArray *)p_selected;
-(void)clearAlbumViewController;
-(void)dismissFromAlbumViewer;

@end
