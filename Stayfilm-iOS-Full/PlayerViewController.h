//
//  PlayerViewController.h
//  fbmessenger
//
//  Created by Henrique Ormonde on 24/06/15.
//  Copyright (c) 2015 Stayfilm. All rights reserved.
//
@import Photos;
#import <AVKit/AVKit.h>
#import "Movie.h"


@protocol PlayerExplorerDelegate<NSObject>

-(void)playStoryAgain;


@end

@interface PlayerViewController : AVPlayerViewController <AVPlayerViewControllerDelegate>

@property (strong, nonatomic) PHAsset *assetMovie;
@property (strong, nonatomic) Movie *currentMovie;
@property (strong, nonatomic) NSString *videoUrl;
@property (strong, nonatomic) NSString *idMovie;
@property (nonatomic) BOOL comeFromStory;

- (void)canRotate;
@property (nonatomic, assign) id<PlayerExplorerDelegate> playerExplorerDelegate;
@end
