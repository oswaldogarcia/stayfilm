//
//  ProgressBarViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 16/05/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import "ProgressBarViewController.h"

@interface ProgressBarViewController ()

@end

@implementation ProgressBarViewController

@synthesize isModal;

-(id)initWithDelegate:(id<ProgressBarViewControllerDelegate>)p_delegate
{
    if(self = [super init]) {
        self.delegate = p_delegate;
        self.isModal = NO;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    if(!self.isModal) {
//        UITapGestureRecognizer *modalTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOutside)];
//        modalTap.numberOfTapsRequired = 1;
//        [self.view addGestureRecognizer:modalTap];
//    }
    
}

-(void)tapOutside {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cancelAction:(id)sender {
    [self tapOutside];
    [self.delegate performSelector:@selector(chooseOption:) withObject:@"cancelDownload"];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
