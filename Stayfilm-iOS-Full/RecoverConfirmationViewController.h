//
//  RecoverConfirmationViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Henrique on 5/2/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecoverConfirmationViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lbl_Text;
@property (weak, nonatomic) IBOutlet UIImageView *img_Check;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner_loader;

@property (nonatomic, strong) NSString * email;
@property (nonatomic, strong) NSString * password;

@end
