//
//  Popover_ConfirmationMoreOptions_ViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Henrique Ormonde on 08/11/17.
//  Copyright © 2017 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol Popover_ConfirmationMoreOptions_ViewControllerDelegate <NSObject>
@required
-(void)decidedOption:(NSString *)p_option;
-(void)dismissModalMenu;
@end


@interface Popover_ConfirmationMoreOptions_ViewController : UIViewController <UIActionSheetDelegate, Popover_ConfirmationMoreOptions_ViewControllerDelegate>

@property(nonatomic, weak) id<Popover_ConfirmationMoreOptions_ViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIButton *but_addMore;
@property (weak, nonatomic) IBOutlet UIButton *but_reorder;
@property (weak, nonatomic) IBOutlet UIButton *but_remove;
@property (weak, nonatomic) IBOutlet UIButton *but_cancel;

@property (strong, nonatomic) IBOutlet UIView *blur_view;
@property (weak, nonatomic) IBOutlet UIView *contentView;

-(id)initWithDelegate:(id<Popover_ConfirmationMoreOptions_ViewControllerDelegate>)p_delegate;

@end
