//
//  AlbumSocialCollectionViewCell.h
//  Stayfilm-iOS-Full
//
//  Created by Henrique Ormonde on 25/09/17.
//  Copyright © 2017 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlbumSN.h"

@interface AlbumSocialCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageBig;
@property (weak, nonatomic) IBOutlet UIImageView *imageLeft;
@property (weak, nonatomic) IBOutlet UIImageView *imageRight;
@property (weak, nonatomic) IBOutlet UIImageView *imageSOLO;
@property (weak, nonatomic) IBOutlet UILabel *txt_Title;
@property (weak, nonatomic) IBOutlet UILabel *txt_Total;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loaderSpinner;

@property (strong, nonatomic) AlbumSN *album;

@property (strong) NSURLSessionDataTask *task;

-(void)initCell;

@end
