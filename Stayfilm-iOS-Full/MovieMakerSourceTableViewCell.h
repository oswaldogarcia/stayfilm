//
//  MovieMakerSourceTableViewCell.h
//  Stayfilm-iOS-Full
//
//  Created by Henrique Ormonde on 21/09/17.
//  Copyright © 2017 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlbumFB.h"

@interface IndexedCollectionView : UICollectionView  //this is so we can use collection view inside table cell

@property (nonatomic, strong) NSIndexPath *indexPath;

@end

@interface MovieMakerSourceTableViewCell : UITableViewCell <UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *img_SocialSource;
@property (weak, nonatomic) IBOutlet UIImageView *expandArrow;
@property (weak, nonatomic) IBOutlet UILabel *txt_Title;
@property (weak, nonatomic) IBOutlet UILabel *txt_NoAlbums;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingSpinner;
@property (weak, nonatomic) IBOutlet UIImageView *but_showMoreLess;
@property (weak, nonatomic) IBOutlet UILabel *txt_showMoreLess;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_AlbumsHeight;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *expandTapGesture;
@property (nonatomic, strong) NSArray *albums;
@property (nonatomic, assign) BOOL isExpanded;

@property (nonatomic, strong) NSURLSession *thumbConnection;

@property (nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizer;

@property (strong, nonatomic) IBOutlet IndexedCollectionView *albumCollection;

- (void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate indexPath:(NSIndexPath *)indexPath;

@end
