//
//  StoriesPageViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 13/06/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import "StoriesPageViewController.h"
#import "Movie.h"
#import <AVKit/AVKit.h>
#import "ExplorerPlayerViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <Google/Analytics.h>


@interface StoriesPageViewController (){
}
@property (assign, nonatomic) NSInteger index;

@end

@implementation StoriesPageViewController{
    //NSMutableArray *myViewControllers;
  
}
@synthesize isSwipeUP;
- (void)viewWillDisappear:(BOOL)animated{
    if(!self.isSwipeUP){
    [self removeFromParentViewController];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    
    [self prefersStatusBarHidden];
    [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    
    self.delegate = self;
    self.dataSource = self;
    
    self.myViewControllers = [[NSMutableArray alloc]init];
    
     for (int i = 0; i < self.storiesArray.count; i++){
        
        Story *story = self.storiesArray[i];
         UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Explorer" bundle:[NSBundle mainBundle]];
        
        StoriesContentViewController *playerVC = [storyboard instantiateViewControllerWithIdentifier:@"storyContentView"];
       
        playerVC.delegate = self;
        playerVC.story = story;
        playerVC.storiesContent = [[NSMutableArray alloc]initWithArray:story.content];
        playerVC.playerVC = [[AVPlayerViewController alloc]init];
        
//         if(story.myPlayers > 0){
//             playerVC.myPlayers = [[NSMutableArray alloc]initWithArray:story.myPlayers];
//         }

         [playerVC setCurrentConten:[story.viewedIDContent intValue]];
        
        NSIndexSet *indexset=[story.content indexesOfObjectsWithOptions:NSEnumerationConcurrent passingTest:^BOOL(StoryContent* obj, NSUInteger idx, BOOL *stop) {
            BOOL match = NO;
            if( obj.idContent == story.viewedIDContent ){
                match=YES;
            }
            return match;
        }];
         
        NSArray *content = [story.content objectsAtIndexes: indexset];
         
         if(content.count > 0){
            [playerVC setIndexConten:((int)[story.content indexOfObject:content[0]])];
         }else{
            [playerVC setIndexConten:-1];
         }
             
        [self.myViewControllers addObject:playerVC];
    }
    
    self.index = self.storySelected;
    
//    Story *story = self.storiesArray[self.storySelected];
//    if(story.logoUrl != nil){
//        if(self.storyDelegate && [self.storyDelegate respondsToSelector:@selector(changeLogo:)])
//            [self.storyDelegate changeLogo: story.logoUrl];
//    }else{
//        if(self.storyDelegate && [self.storyDelegate respondsToSelector:@selector(changeLogo:)])
//            [self.storyDelegate changeLogo: @""];
//    }
    dispatch_async(dispatch_get_main_queue(), ^{
    [self setViewControllers:@[self.myViewControllers[self.storySelected]]
                   direction:UIPageViewControllerNavigationDirectionForward
                   animated:YES completion:nil];
    });
    
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (UIViewController *)viewControllerAtIndex:(NSUInteger)index {
    return self.myViewControllers[index];
}

-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController
     viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger currentIndex = [self.myViewControllers indexOfObject:viewController];
    
    // get the index of the current view controller on display
    self.index = (int)currentIndex;
    
    if (self.index > 0)
    {
        
        return [self.myViewControllers objectAtIndex:currentIndex-1];
        
        // return the previous viewcontroller
    } else
    {
        //[self dismissViewControllerAnimated:YES completion:nil];
        return nil;
    }
}
-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController
      viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger currentIndex = [self.myViewControllers indexOfObject:viewController];
    
    // get the index of the current view controller on display
    // check if we are at the end and decide if we need to present
    // the next viewcontroller
    self.index = (int)currentIndex;
    
    if (self.index < [self.myViewControllers count]-1)
    {
        
        return [self.myViewControllers objectAtIndex:currentIndex+1];
        
    } else
    {
        //[self dismissViewControllerAnimated:YES completion:nil];
        return nil;
       
    }
}

NSInteger proxIndex = -1;
- (void)pageViewController:(UIPageViewController *)pvc didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed
{
    // If the page did not turn
    if (!completed)
    {
        // still same page
        return;
    }

    if(self.index < proxIndex) {
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:self.sfApp.analyticsTag
                                                              action:@"swipe-right"
                                                               label:@""
                                                               value:@1] build]];
    } else {
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:self.sfApp.analyticsTag
                                                              action:@"swipe-left"
                                                               label:@""
                                                               value:@1] build]];
    }
}

#pragma mark - Explorer Delegate Methods


- (void)moveToNext{
    self.index = self.index + 1;
    if (self.index < self.myViewControllers.count){
    
        @try{
            for (UIScrollView *view in self.view.subviews) {
                if ([view isKindOfClass:[UIScrollView class]]) {
                    view.scrollEnabled = NO;
                }
            }
            __weak typeof (self) selfDelegate = self;
            dispatch_async(dispatch_get_main_queue(), ^{
                [self setViewControllers:@[self.myViewControllers[self.index]]
                               direction:UIPageViewControllerNavigationDirectionForward
                                animated:YES completion:^(BOOL finished) {
                                    if(finished) {
                                        for (UIScrollView *view in selfDelegate.view.subviews) {
                                            if ([view isKindOfClass:[UIScrollView class]]) {
                                                view.scrollEnabled = YES;
                                            }
                                        }
                                    }
                                }];
            });
       
        }
        @catch (NSException *exception)
        {
            NSLog(@"StayLog: EXCEPTION moveToNext %@", exception.description);
        }
        
    }else{
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}



- (void)moveToBack{
    self.index = self.index - 1;
    if ((self.index < self.myViewControllers.count) && (self.index >= 0)) {
        
        @try{
            for (UIScrollView *view in self.view.subviews) {
                if ([view isKindOfClass:[UIScrollView class]]) {
                    view.scrollEnabled = NO;
                }
            }
            __weak typeof (self) selfDelegate = self;
            dispatch_async(dispatch_get_main_queue(), ^{
                [self setViewControllers:@[self.myViewControllers[self.index]]
                               direction:UIPageViewControllerNavigationDirectionReverse
                                animated:YES completion:^(BOOL finished) {
                                    if(finished) {
                                        for (UIScrollView *view in selfDelegate.view.subviews) {
                                            if ([view isKindOfClass:[UIScrollView class]]) {
                                                view.scrollEnabled = YES;
                                            }
                                        }
                                    }
                                }];
            });
        }
        @catch (NSException *exception)
        {
            NSLog(@"StayLog: EXCEPTION moveToBack %@", exception.description);
        }
    }else{
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)changedConnectivity:(BOOL)connected {
    if(self.storyDelegate && [self.storyDelegate respondsToSelector:@selector(changedConnectivity:)])
        [self.storyDelegate changedConnectivity:connected];
}

-(void)makeAfilmButtonTapped{
    
    [self.storyDelegate makeAFilmAction];
    
}

- (void)setViewedId:(nonnull NSNumber *)idStory onId:(nonnull NSNumber *)idconten {
    //
}

@end
