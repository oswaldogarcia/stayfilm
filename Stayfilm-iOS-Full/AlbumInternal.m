//
//  AlbumInternal.m
//  Stayfilm-iOS-Full
//
//  Created by Henrique Ormonde on 16/10/17.
//  Copyright © 2017 Stayfilm. All rights reserved.
//

#import "AlbumInternal.h"

@implementation AlbumInternal

@synthesize  title, count, medias; //imageUP, imageDOWN, imageBIG,


+ (id)customClassWithProperties:(NSDictionary *)properties {
    if(properties != nil)
    {
        NSMutableDictionary *filteredObjs = [[NSMutableDictionary alloc] init];
        for (NSString* key in properties) {
            id value = [properties objectForKey:key];
            if ([key isEqualToString:@"title"]) {
                [filteredObjs setValue:value forKey:@"title"];
            }
            else if ([key isEqualToString:@"cover_photo"]) {
                [filteredObjs setValue:value forKey:@"imageBIG"];
            }
            else if ([key isEqualToString:@"imageLEFT"]) {
                [filteredObjs setValue:value forKey:@"imageUP"];
            }
            else if ([key isEqualToString:@"imageRIGHT"]) {
                [filteredObjs setValue:value forKey:@"imageDOWN"];
            }
            else if ([key isEqualToString:@"picture"]) {
                id valueData = [value objectForKey:@"data"];
                [filteredObjs setValue:valueData[@"url"] forKey:@"imageBIG"];
            }
            else if ([key isEqualToString:@"count"]) {
                [filteredObjs setValue:value forKey:@"count"];
            }
        }
        return [[self alloc] initWithProperties:filteredObjs];
    }
    else
    {
        return nil;
    }
}

- (id)initWithProperties:(NSDictionary *)properties {
    if (self = [self init]) {
        @try {
            [self setValuesForKeysWithDictionary:properties];
        }
        @catch (NSException *ex) {
            return nil;
        }
    }
    return self;
}

-(id)init {
    self = [super init];
    if (0 != self) {
        self.title = nil;
//        self.imageBIG = nil;
//        self.imageUP = nil;
//        self.imageDOWN = nil;
        self.count = 0;
        self.medias = [[NSMutableArray alloc] init];
    }
    return self;
}

@end

