//
//  ProfileViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 23/04/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

@import KALoader;
#import "AppDelegate.h"
#import <UIKit/UIKit.h>
#import "Feed.h"
#import "StayWS.h"
#import "StayfilmApp.h"
#import "StoryState.h"
#import "Movie.h"
#import "ProfileSettingsViewController.h"
#import "StorieViewController.h"
#import "VideoOptionsViewController.h"
#import "PopupMenuAnimation.h"
#import "Downloader.h"
#import "Popover_ShareVideo_ViewController.h"
#import "EditProfileViewController.h"
#import "ChangePrivacyViewController.h"
#import "AFNetworking.h"
#import <QuartzCore/QuartzCore.h>
#import "UIView+Toast.h"
#import "Popover_Delete_ViewController.h"

@interface ProfileViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,UIViewControllerTransitioningDelegate,Popover_ShareVideo_ViewControllerDelegate, ProfileStoryDelegate, StayfilmSingletonDelegate>


@property (nonatomic, strong) Feed *myFeed;
@property (nonatomic, strong) NSMutableArray *myFilms;
@property (nonatomic, strong) NSMutableArray *storiesArray;
@property (nonatomic, strong) Movie *filmToEdit;
@property (nonatomic, weak) StayfilmApp *sfAppProfile;
@property (nonatomic, strong) NSMutableArray *genres;

@property (nonatomic, strong) AFURLSessionManager *manager;
@property (nonatomic, strong) NSURLSessionDownloadTask *downloadTask;
@property (nonatomic, assign) NSURL* downloadVideopath;
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UILabel *profileLastName;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *userEmailLabel;

@property (weak, nonatomic) IBOutlet UIView *loadingView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;
@property (nonatomic, strong) PopupMenuAnimation *transitionAnimation;
@property (weak, nonatomic) IBOutlet UITableView *userFilmsTable;
@property (weak, nonatomic) IBOutlet UIView *noFilmsView;
@property (weak, nonatomic) IBOutlet UILabel *noFilmsLabel;
@property (weak, nonatomic) IBOutlet UIView *view_connectionStatus;
@property (weak, nonatomic) IBOutlet UILabel *lbl_connectionStatus;

@property (weak, nonatomic) IBOutlet UIButton *imageProfileButtom;
@property (weak, nonatomic) IBOutlet UIButton *profileNameButtom;
@property (weak, nonatomic) IBOutlet UIButton *profileLastNameButtom;

@property (nonatomic, strong) Popover_ShareVideo_ViewController * popoverShareView;
@property (nonatomic, assign) BOOL needPagination;


@property (weak, nonatomic) IBOutlet UICollectionView *exploreCollectionView;

@end
