//
//  LoginFacebookOperation.h
//  Stayfilm-iOS-Full
//
//  Created by Henrique Ormonde on 04/09/17.
//  Copyright © 2017 Stayfilm. All rights reserved.
//
#ifndef Stayfilm_Full_LoginFacebookOperation_h
#define Stayfilm_Full_LoginFacebookOperation_h

#import <Foundation/Foundation.h>
#import "StayfilmApp.h"

enum {
    LOGIN_SUCCESSFUL,
    CONNECTION_ERROR,
    USER_CANCELLED,
    LOGIN_FAILED,
    REGISTRATION_ERROR,
    DIFFERENT_USER,
    NO_CONFIG_LOADED,
    LOGIN_EXCEPTION,
    NO_INTERNET_CONNECTION
};
typedef NSInteger MessageCode;


@protocol LoginFacebookOperationDelegate;


@interface LoginFacebookOperation : NSOperation

@property (nonatomic, assign) id <LoginFacebookOperationDelegate> delegate;
@property (nonatomic, weak) StayfilmApp *sfAppLogin;
@property (nonatomic, assign) NSInteger messageCode;
@property (nonatomic, assign) BOOL overrideUser;

-(id)initWithDelegate:(id<LoginFacebookOperationDelegate>)p_delegate andSFApp:(StayfilmApp *)p_sfApp;
-(id)initWithoutUserOverrideWithDelegate:(id<LoginFacebookOperationDelegate>)p_delegate andSFApp:(StayfilmApp *)p_sfApp;
-(id)initWithDelegate:(id<LoginFacebookOperationDelegate>)p_delegate andSFApp:(StayfilmApp *)p_sfApp andPermissions:(NSArray *)p_permissions;
-(void)loginToStayfilmWithToken:(NSString *)p_token;

-(BOOL)connected;

@end


@protocol LoginFacebookOperationDelegate <NSObject>
- (void)loginFacebookDidFinish:(LoginFacebookOperation *)loginOP;
- (void)loginFacebookDidFail:(LoginFacebookOperation *)loginOP;
@end

#endif
