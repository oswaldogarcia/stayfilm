//
//  AutomaticVideoViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 9/26/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "StayfilmApp.h"
#import "LoginFacebookOperation.h"
#import "LoginToSocialNetworkOperation.h"
#import "Uploader.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Popover_PhotoAccessPermission_ViewController.h"
#import "StoryState.h"


NS_ASSUME_NONNULL_BEGIN

@interface AutomaticVideoViewController : UIViewController 

@property (nonatomic, strong) NSString *titleText;
@property (weak, nonatomic) IBOutlet UIButton *profileButton;
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UILabel *txt_Title;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lowerViewConstraint;
@property (weak, nonatomic) IBOutlet UIView *view_Editing;
@property (weak, nonatomic) IBOutlet UIButton *but_edit_Back;
@property (weak, nonatomic) IBOutlet UIButton *but_edit_Cancel;
@property (weak, nonatomic) IBOutlet UIButton *but_edit_Confirm;

@property (weak, nonatomic) IBOutlet UICollectionView *suggestionCollection;
@property (weak, nonatomic) IBOutlet UIView *but_cameraRoll;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_socialHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_socialFacebook;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_socialInstagram;

@property (weak, nonatomic) IBOutlet UIImageView *camera_arrow;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *camera_spinner;
@property (weak, nonatomic) IBOutlet UIView *tapToOpenView;
@property (weak, nonatomic) IBOutlet UIImageView *tapToOpenImage;

@property (strong, nonatomic) NSMutableArray *initialEditedMedias;

@end

NS_ASSUME_NONNULL_END
