//
//  WSConfig.h
//  Stayfilm for Messenger
//
//  Created by Henrique Ormonde on 24/04/15.
//  Copyright (c) 2015 Stayfilm. All rights reserved.
//

#ifndef Stayfilm_Full_WSConfig_h
#define Stayfilm_Full_WSConfig_h


@interface WSConfig : NSObject

@property (nonatomic, readwrite, strong) NSString *wsAddress;
@property (nonatomic, strong) NSDictionary *configPaths;
@property (nonatomic, strong) NSString *facebookID;

-(id)init;
-(NSString *)getConfigPathWithKey:(NSString *)key;

@end


#endif
