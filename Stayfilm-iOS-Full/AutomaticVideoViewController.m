//
//  AutomaticVideoViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 9/26/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import "AutomaticVideoViewController.h"

@import Pods_Stayfilm_iOS_Full;
@import BSImagePicker;
@import Photos;

#import "MovieMakerStep1ViewController.h"
#import "Step2TitleViewController.h"
#import "Step1ConfirmationViewController.h"
#import "Step3StylesViewController.h"
#import "MovieMakerSuggestionAlbumCollectionViewCell.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Google/Analytics.h>
#import "Reachability.h"
#import "PopupMenuAnimation.h"
#import "ProfileViewController.h"

#import "Job.h"
#import "AlbumSF.h"
#import "AlbumSN.h"
#import "AlbumInternal.h"
#import "Media.h"
#import "VideoFB.h"
#import "SocialAlbumsView.h"
#import "AlbumSocialCollectionViewCell.h"

#import "AlbumViewerViewController.h"

@interface AutomaticVideoViewController ()

@property (nonatomic, weak) StayfilmApp *sfAppStep1;

@property (nonatomic, strong) Uploader* uploaderStayfilm;
@property (nonatomic, strong) NSConditionLock* lockAlbum;
@property (nonatomic, weak) AlbumViewerViewController *albumVC;

//@property (nonatomic, strong) NSMutableArray *currentlyLoadingAlbums;
@property (nonatomic, strong) NSMutableArray *albunsFacebook;
@property (nonatomic, strong) NSMutableArray *albunsInstagram;
@property (nonatomic, strong) NSMutableArray *albunsGoogle;
@property (nonatomic, strong) NSMutableArray *albunsStayfilm;
@property (nonatomic, strong) NSMutableArray *albunsSuggestion;
//@property (nonatomic, strong) NSArray<MovieMakerSuggestionAlbumCollectionViewCell *> *suggestionAlbumsCellList;
@property (nonatomic, strong) NSString *facebookNextPage;
@property (nonatomic, strong) NSString *facebookVideoNextPage;
@property (nonatomic, assign) BOOL loadedAllFacebookAlbums;

@property (nonatomic, assign) int facebookloginAttempts;
@property (nonatomic, assign) int socialLoginAttempts;

@property (nonatomic, strong) NSMutableDictionary *contentOffsetDictionary;
@property (nonatomic, assign) int indexSelectedAlbum;

@property (nonatomic, strong) NSURLSession *thumbConnection;

@property (nonatomic, assign) BOOL isSelectingAlbum;
@property (nonatomic, assign) BOOL isSelectingSocial;
@property (nonatomic, assign) BOOL isOpeningUploader;

@property(nonatomic , strong) PHFetchResult *assetsFetchResults;
@property(nonatomic , strong) PHCachingImageManager *imageManager;

@property (nonatomic, strong) UITapGestureRecognizer *cameraTapGesture;

@property (nonatomic, strong) PopupMenuAnimation *transitionAnimation;
@property (weak, nonatomic) IBOutlet UIView *loadingView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;

@property(strong, nonatomic) NSObject<OS_dispatch_semaphore> *albumSemaphore;

@property(atomic, strong) NSMutableDictionary *albumReadAttempts;


@end

@implementation AutomaticVideoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(NSDictionary*)metadataFromImageData:(NSData*)imageData{
    CGImageSourceRef imageSource = CGImageSourceCreateWithData((__bridge CFDataRef)(imageData), NULL);
    if (imageSource) {
        NSDictionary *options = @{(NSString *)kCGImageSourceShouldCache : [NSNumber numberWithBool:NO]};
        CFDictionaryRef imageProperties = CGImageSourceCopyPropertiesAtIndex(imageSource, 0, (__bridge CFDictionaryRef)options);
        if (imageProperties) {
            NSDictionary *metadata = (__bridge NSDictionary *)imageProperties;
            CFRelease(imageProperties);
            CFRelease(imageSource);
            NSLog(@"Metadata of selected image%@",metadata);// It will display the metadata of image after converting NSData into NSDictionary
            return metadata;
            
        }
        CFRelease(imageSource);
    }
    
    NSLog(@"Can't read metadata");
    return nil;
}

-(void)selectMedias{
    
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            switch (status) {
                case PHAuthorizationStatusAuthorized:
                {
                    runOnMainQueueWithoutDeadlocking(^{
                        [self.camera_spinner startAnimating];
                        [self.camera_spinner setHidden:NO];
                        
                        AlbumInternal *album = [[AlbumInternal alloc] init];
                        
                        if(self.imageManager == nil)
                        {
                            self.imageManager = [[PHCachingImageManager alloc] init];
                        }
                        PHFetchOptions *options = [[PHFetchOptions alloc] init];
                        options.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];
                        self.assetsFetchResults = [PHAsset fetchAssetsWithOptions:options];
                        
                        album.title = NSLocalizedString(@"CAMERA_ROLL", nil);
                        
                        for (PHAsset *item in self.assetsFetchResults) {
                            [album.medias addObject:item];
                            
                            if (item) {
                                // get photo info from this asset
                                PHImageRequestOptions * imageRequestOptions = [[PHImageRequestOptions alloc] init];
                                imageRequestOptions.synchronous = YES;
                                [[PHImageManager defaultManager]
                                 requestImageDataForAsset:item
                                 options:imageRequestOptions
                                 resultHandler:^(NSData *imageData, NSString *dataUTI,
                                                 UIImageOrientation orientation,
                                                 NSDictionary *info)
                                 {
                                     NSDictionary *dict = [self metadataFromImageData:imageData];
                                     
                                     NSLog(@"METADATA:%@",dict);
                                 }];
                            }
                        }
                        
                        self.albumVC = nil;
                        self.albumVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AlbumViewer"];
                        self.albumVC.selectedMedias = nil;
                        if(self.sfAppStep1.isEditingMode) {
                            self.albumVC.selectedMedias = [self.sfAppStep1.editedMedias mutableCopy];
                        } else {
                            self.albumVC.selectedMedias = [self.sfAppStep1.finalMedias mutableCopy];
                        }
                        self.albumVC.albumFB = nil;
                        self.albumVC.albumGeneric = album;
                        
                        [self.navigationController showViewController:self.albumVC sender:self];
                    });
                }
                    //[self performSelectorOnMainThread:@selector(openBSImagePicker) withObject:nil waitUntilDone:NO];
                    break;
                case PHAuthorizationStatusDenied:
                case PHAuthorizationStatusRestricted:
                case PHAuthorizationStatusNotDetermined:
//                {
//                    runOnMainQueueWithoutDeadlocking(^{
//                        Popover_PhotoAccessPermission_ViewController * view = [[Popover_PhotoAccessPermission_ViewController alloc] initWithDelegate:self];
//                        view.modalPresentationStyle = UIModalPresentationOverFullScreen;
//                        view.transitioningDelegate = self;
//                        view.view.frame = self.view.frame;
//                        self.transitionAnimation.duration = 0.3;
//                        self.transitionAnimation.isPresenting = YES;
//                        self.transitionAnimation.originFrame = self.view.frame;
//
//                        [self presentViewController:view animated:YES completion:nil];
//                    });
//                }
                    break;
                    
                default:
                    break;
            }
            self.isOpeningUploader = NO;
        }];
   
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
 */
@end
