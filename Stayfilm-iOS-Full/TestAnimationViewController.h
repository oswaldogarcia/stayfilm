//
//  TestAnimationViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Henrique Ormonde on 29/08/17.
//  Copyright © 2017 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestAnimationViewController : UIViewController
    
@property (weak, nonatomic) IBOutlet UIView *animationView;
@property (weak, nonatomic) IBOutlet UIButton *but_Exit;
@property (weak, nonatomic) IBOutlet UIButton *but_Next;

@end
