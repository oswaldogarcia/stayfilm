//
//  Popover_Delete_ViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Henrique on 5/16/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Google/Analytics.h>
#import "StayfilmApp.h"

@protocol Popover_DeleteDelegate<NSObject>
@required
- (void)chooseOption:(NSString*)option;
@end

@interface Popover_Delete_ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *but_delete;
@property (weak, nonatomic) IBOutlet UIButton *but_cancel;
@property (nonatomic, strong) NSString *thumbnailUrl;
@property (weak, nonatomic) IBOutlet UIImageView *thubVideoImage;
@property (weak, nonatomic) StayfilmApp *sfApp;
@property (nonatomic, strong) id<Popover_DeleteDelegate> delegate;

@property(nonatomic, strong) NSString *analyticTag;

@end
