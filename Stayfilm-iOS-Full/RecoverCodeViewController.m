//
//  RecoverCodeViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Henrique on 4/26/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <Google/Analytics.h>
#import "RecoverCodeViewController.h"
#import "StayfilmApp.h"
#import "Reachability.h"
#import "User.h"
#import "RecoverResetViewController.h"

@interface RecoverCodeViewController ()

@property (nonatomic, weak) StayfilmApp *sfApp;
@property (nonatomic, assign) BOOL isMovingToReset;
@property (nonatomic, assign) BOOL isSendingCode;
@property (nonatomic, assign) BOOL isResendingEmail;
@property (nonatomic, strong) UITapGestureRecognizer *tapGest;

@end

@implementation RecoverCodeViewController

@synthesize lbl_TextTitle, txt_Code, but_EnterCode, but_ResendEmail, lbl_Status, spinner_loading, img_Background, email;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.txt_Code.layer.borderColor = [UIColor colorWithRed:51.0/255.0 green:87.0/255.0 blue:113.0/255.0 alpha:1.0].CGColor;
    self.txt_Code.layer.borderWidth = 1.0;
    
    self.lbl_TextTitle.text = [NSString stringWithFormat:NSLocalizedString(@"RECOVER_CODE_TITLE", nil), self.email];
    
    [self.txt_Code addTarget:self action:@selector(txt_Code_Ended) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    self.isMovingToReset = NO;
    self.isResendingEmail = NO;
    self.isSendingCode = NO;
    
    self.tapGest = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backgroundTapped:)];
    self.tapGest.numberOfTapsRequired = 1;
    [self.tapGest setCancelsTouchesInView:NO];
    [self.img_Background addGestureRecognizer:self.tapGest];
}

-(void)backgroundTapped:(UIGestureRecognizer *)gesture {
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backButton_Tapped:(id)sender {
    [self.spinner_loading setHidden:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    
    //Google Analytics
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"reset_password_code"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

-(BOOL)connected {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

- (IBAction)but_EnterCode_Clicked:(id)sender {
    if(![self connected])
    {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CONNECTION_ERROR", nil)
                                                                       message:NSLocalizedString(@"CONNECTION_ERROR_MESSAGE", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    if(self.sfApp == nil) {
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    }
    [self.view endEditing:YES];
    self.lbl_Status.text = @"";
    
    if(self.txt_Code.text == nil || [self.txt_Code.text isEqualToString:@""]) {
        self.lbl_Status.alpha = 0.0;
        [self.lbl_Status setHidden:NO];
        self.lbl_Status.text = NSLocalizedString(@"INVALID_RECOVER_CODE_EMPTY", nil);
        [UIView animateWithDuration:0.3 animations:^{
            self.lbl_Status.alpha = 1.0;
        }];
        [self.spinner_loading setHidden:YES];
        return;
    }
    else {
        if(!self.isSendingCode) {
            self.isSendingCode = YES;
            [self.spinner_loading setHidden:NO];
            [self performSelectorInBackground:@selector(sendCode) withObject:nil];
        }
    }
}

- (IBAction)but_ResendEmail_Clicked:(id)sender {
    if(![self connected])
    {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CONNECTION_ERROR", nil)
                                                                       message:NSLocalizedString(@"CONNECTION_ERROR_MESSAGE", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    if(self.sfApp == nil) {
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    }
    [self.view endEditing:YES];
    self.lbl_Status.text = @"";
    
    if(!self.isResendingEmail) {
        self.isResendingEmail = YES;
        [self.spinner_loading setHidden:NO];
        self.lbl_TextTitle.text = [NSString stringWithFormat:NSLocalizedString(@"RECOVER_EMAIL_RESENT_TITLE", nil), self.email];
        [self performSelectorInBackground:@selector(resendEmail) withObject:nil];
    }
}

-(void)resendEmail {
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"password"
                                                          action:@"resend"
                                                           label:@"email"
                                                           value:@1] build]];
    
    NSString *response = [User sendRecoveryEmail:self.email andSFAppSingleton:self.sfApp];
    if(response == nil) { //Valid response
        runOnMainQueueWithoutDeadlocking(^{
            //self.lbl_Status.text = [NSString stringWithFormat:NSLocalizedString(@"RECOVER_EMAIL_RESENT_MESSAGE", nil), self.email];
            
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"RECOVER_EMAIL_RESENT", nil)
                                                                           message:[NSString stringWithFormat:NSLocalizedString(@"RECOVER_EMAIL_RESENT_MESSAGE", nil), self.email]
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        });
    }
    else if ([response isEqualToString:@"exception"]) {
        runOnMainQueueWithoutDeadlocking(^{
            //self.lbl_Status.text = NSLocalizedString(@"CONNECTION_ERROR_MESSAGE", nil);
            
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CONNECTION_ERROR", nil)
                                                                           message:NSLocalizedString(@"CONNECTION_ERROR_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        });
    }
    //    else { //TODO: We should send a Damn email to the user even if the user isn't registered... No email found on our database... no email will be sent... but we still going to next step to fool the user.... (yes I'm against this solution)
    //        [self performSegueWithIdentifier:@"EmailToCodeSegue" sender:self];
    //    }
    runOnMainQueueWithoutDeadlocking(^{
        [self.spinner_loading setHidden:YES];
    });
    self.isResendingEmail = NO;
}

-(void)sendCode {
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"password"
                                                          action:@"add"
                                                           label:@"code"
                                                           value:@1] build]];
    NSString *response = [User sendRecoveryToken:self.txt_Code.text withEmail:self.email andSFAppSingleton:self.sfApp];
    
    if(response == nil) { //Valid response
        [self performSelectorOnMainThread:@selector(moveToNextStep) withObject:nil waitUntilDone:NO];
        //[self performSegueWithIdentifier:@"CodeToResetSegue" sender:self];
    }
    else if ([response isEqualToString:@"exception"]) {
        runOnMainQueueWithoutDeadlocking(^{
            //self.lbl_Status.text = NSLocalizedString(@"CONNECTION_ERROR_MESSAGE", nil);
            
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CONNECTION_ERROR", nil)
                                                                           message:NSLocalizedString(@"CONNECTION_ERROR_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) { }];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        });
    }
    else {
        runOnMainQueueWithoutDeadlocking(^{
            self.lbl_Status.alpha = 0.0;
            [self.lbl_Status setHidden:NO];
            self.lbl_Status.text = NSLocalizedString(@"INVALID_RECOVER_CODE_ERROR", nil);
            [UIView animateWithDuration:0.3 animations:^{
                self.lbl_Status.alpha = 1.0;
            }];
        });
    }
    runOnMainQueueWithoutDeadlocking(^{
        [self.spinner_loading setHidden:YES];
    });
    self.isSendingCode = NO;
}

-(void)txt_Code_Ended {
    [self.view endEditing:YES];
}

-(void)moveToNextStep {
    [self performSegueWithIdentifier:@"CodeToResetSegue" sender:self];
}

#pragma mark - Navigation


// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"CodeToResetSegue"] && !self.isMovingToReset) {
        self.isMovingToReset = YES;
        RecoverResetViewController *recoverReset = segue.destinationViewController;
        recoverReset.email = self.email;
        recoverReset.code = self.txt_Code.text;
        self.isMovingToReset = NO;
    }
}

@end
