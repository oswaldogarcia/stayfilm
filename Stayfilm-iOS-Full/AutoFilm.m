//
//  AutoFilm.m
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 10/3/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//
#define kGoogleAPIKey @"AIzaSyAeamdH2Y5f8xOIDgUXQA7Lu8y3VnwQ3R0"

#import "AutoFilm.h"
@import MapKit;

@implementation AutoFilm

BOOL isAutofilmsRunning;
BOOL isProducingJob;
//float progress;
int strike; //exception counter
BOOL isTicked;
int totalImages;

int biggerArrayCount;
int biggerArrayPosition;

-(id)init{
    self = [super init];
    self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    totalImages = 0;
    return self;
}

//-(void)autoFilmWihtValuesNeedCreate:(BOOL)needCreateAFilm needVerify:(BOOL)needVerify isFromStepOne:(BOOL)isFromStep1{
//
//    self.needCreateAFilm = needCreateAFilm;
//    self.needVerify = needVerify;
//    self.isFromStep1 = isFromStep1;
//    [self verifyImagesForAutoFilm];
//}


-(void)verifyImagesForAutoFilm{
    
    [self selectImagesToGenerateAutomaticVideo];
    
}

- (void)selectImagesToGenerateAutomaticVideo{
    
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        switch (status) {
            case PHAuthorizationStatusAuthorized:
            {
                runOnMainQueueWithoutDeadlocking(^{
                    
                    if(self.imageManager == nil)
                    {
                        self.imageManager = [[PHCachingImageManager alloc] init];
                    }
                    
                    self.imageToAutoVideo = [[NSMutableArray alloc]init];
                    self.locations = [[NSMutableArray alloc]init];
                    self.imagesFromLocations = [[NSMutableArray alloc]init];
                    
                    PHFetchOptions *options = [[PHFetchOptions alloc] init];
                    
                    options.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];
                    options.fetchLimit = 25;
                    
                    self.assetsFetchResults = [PHAsset fetchAssetsWithOptions:options];
                    
                    for (PHAsset *item in self.assetsFetchResults) {
                        if (item){
                            [self filterByLocation:item];
                        }
                    }
                });
            }
            break;
            case PHAuthorizationStatusDenied:
            case PHAuthorizationStatusRestricted:
            case PHAuthorizationStatusNotDetermined:
            
            default:
                break;
        }
    }];
    
}

-(void)autoFilmWihtValuesNeedCreate:(BOOL)needCreateAFilm needVerify:(BOOL)needVerify isFromStepOne:(BOOL)isFromStep1{
    self.needCreateAFilm = needCreateAFilm;
        self.needVerify = needVerify;
        self.isFromStep1 = isFromStep1;
   
        [self verifyImagesForAutoFilm];
}

-(void)filterByLocation:(PHAsset*)item {
    
    CLLocation* eventLocation = item.location;
    
    [self getAddressFromLocation:eventLocation complationBlock:^(NSString * address) {
        if(address) {
            
            if(![self.locations containsObject:address]){
                
                [self.locations addObject:address];
                NSMutableArray *newAddress = [[NSMutableArray alloc]init];
                [newAddress addObject:item];
                [self.imagesFromLocations addObject:newAddress];
            }else{
                
                int indexValue =(int)[self.locations indexOfObject:address];
                NSMutableArray *addressArray = self.imagesFromLocations[indexValue];
                [addressArray addObject:item];
            }
            
            totalImages++;
            
            // NSLog(@" %d / %lu",totalImages,(unsigned long)self.assetsFetchResults.count);
            if (totalImages == self.assetsFetchResults.count){
                [self getLocationWithMoreImages];
            }
            
        }
    }];
    
}



-(void)getLocationWithMoreImages{
    
    if([self.locations containsObject:@"noAddress"]){
        [self.imagesFromLocations removeObjectAtIndex: [self.locations indexOfObject:@"noAddress"]];
        [self.locations removeObject:@"noAddress"];
    }
    if(self.locations.count > 0){
        for (int i = 0;i<self.locations.count;i++){
            
            NSMutableArray *tempArray = [[NSMutableArray alloc]initWithArray:self.imagesFromLocations[i]];
            
            if(tempArray.count > biggerArrayCount){
                biggerArrayCount = (int)tempArray.count;
                biggerArrayPosition = i;
            }
            
        }
        
        if(self.imagesFromLocations.count > 0){
            NSArray *images = self.imagesFromLocations[biggerArrayPosition];
            //validate if array have iamge
            [self.sfApp.selectedMediasToAutoFilm setArray:images];
            self.sfApp.selectedTitleToAutoFilm = self.locations[biggerArrayPosition];
            
            [self.delegate showAutoFilmOption];
        }
        
        
    }

    
}


-(void)getPhotosInfoFromAsset{
    
    for (PHAsset *item in self.imagesFromLocations[biggerArrayPosition]) {
        if (item){
            // get photo info from this asset
            PHImageRequestOptions * imageRequestOptions = [[PHImageRequestOptions alloc] init];
            imageRequestOptions.networkAccessAllowed = YES;
            imageRequestOptions.synchronous = YES;
            [[PHImageManager defaultManager]
             requestImageDataForAsset:item
             options:imageRequestOptions
             resultHandler:^(NSData *imageData, NSString *dataUTI,
                             UIImageOrientation orientation,
                             NSDictionary *info)
             {
                 NSDictionary *dict = [self metadataFromImageData:imageData];
                 NSDictionary  *GPSData = dict[@"{GPS}"];
                 NSString  *latitude = GPSData[@"Latitude"];
                 NSString  *longitude = GPSData[@"Longitude"];
                 if ([GPSData[@"LatitudeRef"] isEqualToString:@"S"]){
                     latitude = [NSString stringWithFormat:@"-%@",latitude];
                 }
                 if ([GPSData[@"LongitudeRef"] isEqualToString:@"W"]){
                     longitude = [NSString stringWithFormat:@"-%@",longitude];
                 }
                 //
                 if (latitude && longitude)
                     [self getNearbyPlaces:latitude and:longitude];
                 
             }];
            
        }
    }
    
}

- (void)createAutomaticVideo{
    
    // Set values on StayFilm singleton
    self.sfApp.isAutomaticMode = YES;
    self.sfApp.currentSelectionCount = (int)self.sfApp.selectedMediasToAutoFilm.count;
    self.sfApp.selectedMedias = self.sfApp.selectedMediasToAutoFilm;
    self.sfApp.finalMedias = self.sfApp.selectedMediasToAutoFilm;
    
    //selecting a ramdon Genre and Template
    /*if ([self.sfApp.sfConfig.genres count] > 0) {
        
        self.sfApp.selectedGenre =
        self.sfApp.sfConfig.genres[arc4random_uniform((UInt32)[self.sfApp.sfConfig.genres count])];
        
        self.sfApp.selectedTemplate =
        self.sfApp.selectedGenre.templates[arc4random_uniform((UInt32)[self.sfApp.selectedGenre.templates count])];
    }*/
    
    self.sfApp.selectedTitle = self.sfApp.selectedTitleToAutoFilm;
    
    self.uploaderStayfilm = [Uploader sharedUploadStayfilmSingletonWithDelegate:self];
    [self.uploaderStayfilm addMedias:self.sfApp.finalMedias];
    
    UINavigationController *navController = (UINavigationController *)[[[UIApplication sharedApplication] keyWindow] rootViewController];
    
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"MovieMaker" bundle:nil];
    if(self.isFromStep1){
        [self.delegate redirecToProgressView];
    }else{
        Step1ConfirmationViewController *vc = [story instantiateViewControllerWithIdentifier:@"Step1Confirmation"];
        [navController pushViewController:vc animated:YES];
    }
    
    //Auto Film
   /* if(self.sfApp.selectedMediasToAutoFilm.count >= 5){
        //Redirect to Progress ViewController
        if(self.isFromStep1){
            [self.delegate redirecToProgressView];
        }else{
        ProgressViewController *vc = [story instantiateViewControllerWithIdentifier:@"Progress"];
        [navController pushViewController:vc animated:YES];
        }
    }else{
        //Redirect to Step 1 ViewController
        if(self.isFromStep1){
            [self.delegate redirecToProgressView];
        }else{
        Step1ConfirmationViewController *vc = [story instantiateViewControllerWithIdentifier:@"Step1Confirmation"];
        [navController pushViewController:vc animated:YES];
        }
    }*/
    
}

-(void)getNearbyPlaces:(NSString*) latitude and:(NSString*)longitude{
    
    CLLocationCoordinate2D coord = CLLocationCoordinate2DMake([latitude doubleValue], [longitude doubleValue]);

    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(coord, 100, 100);
    
    MKLocalSearchRequest *request = [[MKLocalSearchRequest alloc] init];
    request.region = region;
    request.naturalLanguageQuery = @"Restaurants";
    MKLocalSearch *localSearch = [[MKLocalSearch alloc] initWithRequest:request];
    [localSearch startWithCompletionHandler:^(MKLocalSearchResponse *response, NSError *error) {
        
        
        
        for(MKMapItem *mapItem in response.mapItems){

            CLLocationCoordinate2D coordItem = CLLocationCoordinate2DMake(mapItem.placemark.coordinate.latitude, mapItem.placemark.coordinate.longitude);
            
            CLLocation *loc1 = [[CLLocation alloc] initWithLatitude:coord.latitude longitude:coord.longitude];
            
            CLLocation *loc2 = [[CLLocation alloc] initWithLatitude:coordItem.latitude longitude:coordItem.longitude];
            
            NSLog(@"%@",[mapItem name]);
            NSLog(@"Coord Image: %f / %f ",coord.latitude,coord.longitude);
            NSLog(@"Coord Place: %f / %f ",coordItem.latitude,coordItem.longitude);
            
            NSLog(@"Distance: %f", [loc1 distanceFromLocation:loc2]);
            
         
            if (coord.longitude == coordItem.latitude && coord.longitude == coordItem.longitude){
                
                NSLog(@"%@",[mapItem placemark]);
            }
            
           
        }
        
    }];
    
    
    
    //google way
   /* NSString *urlString= [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=%@,%@&radius=2000&key=%@",latitude,longitude,kGoogleAPIKey];
    
    NSLog(@"%@", urlString);
    
    StayWS *ws = [[StayWS alloc] init];
    NSDictionary *args = nil;
    NSDictionary *response = nil;
    NSArray *pathArgs = nil;
    
    response = [ws requestWebServiceWithURL:urlString withRequestType:@"GET" withPathArguments:pathArgs withArguments:args withIdSession:@"" withCache:NO];
    
    
    NSHTTPURLResponse *statusResponse = (NSHTTPURLResponse *) ws.responseMessage;
    if(response != nil && (statusResponse.statusCode == 200 || statusResponse.statusCode == 201))
    {
        NSLog(@"RESPONSE:%@",response );
        //             NSArray *arrayPlaces = [[NSArray alloc] initWithArray:[response objectForKey:@"results"]];
        //             NSLog(@"Place %@",arrayPlaces);

    }*/
}


-(NSDictionary*)metadataFromImageData:(NSData*)imageData{
    CGImageSourceRef imageSource = CGImageSourceCreateWithData((__bridge CFDataRef)(imageData), NULL);
    if (imageSource) {
        NSDictionary *options = @{(NSString *)kCGImageSourceShouldCache : [NSNumber numberWithBool:NO]};
        CFDictionaryRef imageProperties = CGImageSourceCopyPropertiesAtIndex(imageSource, 0, (__bridge CFDictionaryRef)options);
        if (imageProperties) {
            NSDictionary *metadata = (__bridge NSDictionary *)imageProperties;
            CFRelease(imageProperties);
            CFRelease(imageSource);
            //NSLog(@"Metadata of selected image%@",metadata);// It will display the metadata of image after converting NSData into NSDictionary
            
            return metadata;
            
        }
        CFRelease(imageSource);
    }
    
    NSLog(@"Can't read metadata");
    return nil;
}

typedef void(^addressCompletion)(NSString *);

-(void)getAddressFromLocation:(CLLocation *)location complationBlock:(addressCompletion)completionBlock
{
    __block CLPlacemark* placemark;
    __block NSString *address = nil;
    
    
    
    
    CLGeocoder* geocoder = [CLGeocoder new];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (error == nil && [placemarks count] > 0)
         {
             placemark = [placemarks lastObject];
             
//             NSArray *areasOfInterest = placemark.areasOfInterest;
//             NSLog(@"%@",areasOfInterest);
             
             
             address =  placemark.locality;
             //              address =
             //NSLog(@"Address: %@",[NSString stringWithFormat:@"%@, %@ %@", placemark.name, placemark.subLocality, placemark.locality]);
             
             completionBlock(address);
         }else{
             //NSLog(@"Error: %@",error);
             completionBlock(@"noAddress");
         }
     }];
}

-(BOOL)getDays:(NSString*)dateString{
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //2018:10:03 14:28:15
    [dateFormatter setDateFormat:@"yyyy:MM:dd HH:mm:ss"];
    NSDate *date = [dateFormatter dateFromString:dateString];
    
    
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:[[NSDate alloc] init]];
    
    [components setHour:-[components hour]];
    [components setMinute:-[components minute]];
    [components setSecond:-[components second]];
    //NSDate *today = [cal dateByAddingComponents:components toDate:[[NSDate alloc] init] options:0]; //This variable should now be pointing at a date object that is the start of today (midnight);
    
    [components setHour:-24];
    [components setMinute:0];
    [components setSecond:0];
    //NSDate *yesterday = [cal dateByAddingComponents:components toDate: today options:0];
    
    components = [cal components:NSCalendarUnitWeekday | NSCalendarUnitYear | NSCalendarUnitMonth| NSCalendarUnitDay fromDate:[[NSDate alloc] init]];
    
    [components setDay:([components day] - ([components weekday] - 1))];
    NSDate *thisWeek  = [cal dateFromComponents:components];
    
    [components setDay:([components day] - 7)];
    //NSDate *lastWeek  = [cal dateFromComponents:components];
    
    [components setDay:([components day] - ([components day] -1))];
   // NSDate *thisMonth = [cal dateFromComponents:components];
    
    [components setMonth:([components month] - 1)];
    //NSDate *lastMonth = [cal dateFromComponents:components];
    
//    NSLog(@"today=%@",today);
//    NSLog(@"yesterday=%@",yesterday);
//    NSLog(@"thisWeek=%@",thisWeek);
//    NSLog(@"lastWeek=%@",lastWeek);
//    NSLog(@"thisMonth=%@",thisMonth);
//    NSLog(@"lastMonth=%@",lastMonth);
    
   return [self date:date isBetweenDate:thisWeek andDate:[NSDate date]];
    
    
}

-(BOOL)date:(NSDate*)date isBetweenDate:(NSDate*)beginDate andDate:(NSDate*)endDate
{
    if ([date compare:beginDate] == NSOrderedAscending)
        return NO;
    
    if ([date compare:endDate] == NSOrderedDescending)
        return NO;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yyyy"];
    
    //NSDate *currentDate = date;
    // NSString *dateString = [formatter stringFromDate:currentDate];
    //NSLog(@"Date:%@",dateString);
    return YES;
}
#pragma mark - Uploader Delegates


-(void)uploadProgressChanged:(NSNumber *)progress
{
    // Not important on this page.
}

- (void)converterDidFailMedia:(NSString *)type
{
    
}
-(void)uploadDidFailMedia:(NSString *)type
{
    
}

-(void)uploadDidFail:(Uploader *)uploader
{
    NSLog(@"StayLog: Autofilms UPLOAD did Fail");
}

-(void)uploadDidStartSendingConfirmationMedias:(Uploader *)uploader
{
    // not important on this page.
}

-(void)uploadDidStart
{
    // not important here
}

-(void)uploadDidFinish:(Uploader *)uploader
{

}

-(void)uploadFinishedMedia:(PHAsset *)asset {
    // not important on this view
}



@end
