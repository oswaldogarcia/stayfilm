//
//  AutoFilm.h
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 10/3/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//
#import "Movie.h"
#import "StayfilmApp.h"
#import "StayWS.h"
#import "Uploader.h"
//#import "AlbumSF.h"
//#import "AlbumSN.h"
#import "AlbumInternal.h"
#import "Job.h"
#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "ProgressViewController.h"
#import "Step1ConfirmationViewController.h"
@import Photos;

NS_ASSUME_NONNULL_BEGIN

@protocol AutoFilmDelegate<NSObject>
- (void)setImage:(UIImage*)image andTitle:(NSString*)title;
- (void)redirecToProgressView;
- (void)canCreateAutoFilm:(BOOL)canCreate;
- (void)showAutoFilmOption;

@end

@interface AutoFilm : NSObject <StayfilmUploaderDelegate>

@property (nonatomic, weak) StayfilmApp *sfApp;
@property (nonatomic, strong) AlbumInternal *album;
@property (nonatomic, strong) Uploader* uploaderStayfilm;
@property (nonatomic, strong) ProgressViewController *progressVC;
@property (nonatomic, strong) NSTimer *progressChecker;
@property (nonatomic, assign) BOOL isSendingMeliesJob;
@property (nonatomic, strong) Job *jobMelies;
@property (nonatomic, assign) BOOL needCreateAFilm;
@property (nonatomic, assign) BOOL isFromStep1;
@property (nonatomic, assign) BOOL haveImagesForAutoFilm;
@property (nonatomic, assign) BOOL needVerify;
@property(nonatomic , strong) PHFetchResult *assetsFetchResults;
@property(nonatomic , strong) PHCachingImageManager *imageManager;
@property (strong, nonatomic) NSMutableArray *imageToAutoVideo;
@property (strong, nonatomic) NSMutableArray *locations;
@property (strong, nonatomic) NSMutableArray *imagesFromLocations;
@property (nonatomic, assign) id<AutoFilmDelegate> delegate;

- (void)autoFilmWihtValuesNeedCreate:(BOOL)needCreateAFilm needVerify:(BOOL)needVerify isFromStepOne:(BOOL)isFromStep1;
- (void)selectImagesToGenerateAutomaticVideo;
- (void)createAutomaticVideo;
@end

NS_ASSUME_NONNULL_END
