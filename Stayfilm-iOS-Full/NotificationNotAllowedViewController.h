//
//  NotificationNotAllowedViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 12/10/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol NotificationNotAllowedDelegate <NSObject>
-(void)goToAllowNotification;
-(void)closeModal;
@end

@interface NotificationNotAllowedViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *allowNotificationButton;
@property (weak, nonatomic) IBOutlet UIButton *notNowButton;
@property (weak, nonatomic) IBOutlet UILabel *yesLabel;
@property (weak, nonatomic) IBOutlet UILabel *whenLabel;
@property (weak, nonatomic) IBOutlet UILabel *nowLabel;
@property (nonatomic, assign) id <NotificationNotAllowedDelegate> delegate;
@end

NS_ASSUME_NONNULL_END
