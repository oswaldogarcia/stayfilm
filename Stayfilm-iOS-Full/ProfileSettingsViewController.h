//
//  ProfileSettingsViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 30/04/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StayfilmApp.h"


@protocol ProfileSettingsDelegate<NSObject>

- (void)didSelectEditProfile:(User*)user;


@end

@interface ProfileSettingsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *editProfileButton;
@property (weak, nonatomic) IBOutlet UIButton *logoutButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;

@property StayfilmApp *sfAppProfile;
@property (nonatomic, assign) id<ProfileSettingsDelegate> delegate;
@end
