//
//  Story.m
//  Stayfilm-iOS-Full
//
//  Created by Henrique on 6/13/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import "Story.h"
#import "StayfilmApp.h"


@implementation StoryContent

@synthesize videoUrl, thumbnailUrl, duration, imgUrl, idGenre, idTemplate, idMovie, type ,buttonColor, buttonText,contentDescription;

-(id)init
{
    self = [super init];
    if (0 != self) {
        self.idContent = nil;
        self.videoUrl = nil;
        self.thumbnailUrl = nil;
        self.duration = nil;
        self.imgUrl = nil;
        self.idGenre = nil;
        self.idTemplate = nil;
        self.idMovie = nil;
        self.type = ExploreStoryContent_MovieLANDSCAPE;
        self.buttonText = nil;
        self.buttonColor = nil;
        self.contentDescription = nil;
      
    }
    return self;
}

+ (id)customClassWithProperties:(NSDictionary *)properties {
    if(properties != nil)
    {
        NSMutableDictionary *filteredObjs = [[NSMutableDictionary alloc] init];
        int i = 0;
        for (NSString* key in properties) {
            id value = [properties objectForKey:key];
            if ([key isEqualToString:@"type"]) {
                [filteredObjs setValue:value forKey:@"type"];
            }
            else if ([key isEqualToString:@"idcontent"]) {
                [filteredObjs setValue:value forKey:@"idContent"];
                [filteredObjs setValue:[NSNumber numberWithInt:i] forKey:@"index"];
                i++;
            }
            else if ([key isEqualToString:@"baseurl"]) {
                [filteredObjs setValue:[value stringByAppendingString:@"/video.mp4"] forKey:@"videoUrl"];
                [filteredObjs setValue:[value stringByAppendingString:@"/266x150_n.jpg"] forKey:@"thumbnailUrl"];
            }
            else if ([key isEqualToString:@"duration"]) {
                [filteredObjs setValue:value forKey:@"duration"];
            }
            else if ([key isEqualToString:@"imgurl"]) {
                [filteredObjs setValue:value forKey:@"imgUrl"];
            }
            else if ([key isEqualToString:@"idgenre"] && ![value isEqualToString:@""]) {
                [filteredObjs setValue:value forKey:@"idGenre"];
            }
            else if ([key isEqualToString:@"idtemplate"] && ![value isEqualToString:@""]) {
                [filteredObjs setValue:value forKey:@"idTemplate"];
            }
            else if ([key isEqualToString:@"idmovie"]) {
                [filteredObjs setValue:value forKey:@"idMovie"];
            }
            else if ([key isEqualToString:@"videourl"]) {
                [filteredObjs setValue:value forKey:@"videoUrl"];
            }
            else if ([key isEqualToString:@"buttontext"]) {
                [filteredObjs setValue:value forKey:@"buttonText"];
            }
            else if ([key isEqualToString:@"buttoncolor"]) {
                [filteredObjs setValue:value forKey:@"buttonColor"];
            }
            else if ([key isEqualToString:@"description"]) {
                [filteredObjs setValue:value forKey:@"contentDescription"];
            }
            else if ([key isEqualToString:@"buttonicon"]) {
                [filteredObjs setValue:value forKey:@"buttonIcon"];
            }
            
            
        }
        return [[self alloc] initWithProperties:filteredObjs];
    }
    else
    {
        return nil;
    }
}

- (id)initWithProperties:(NSDictionary *)properties {
    if (self = [self init]) {
        @try {
            [self setValuesForKeysWithDictionary:properties];
        }
        @catch (NSException *ex) {
            return nil;
        }
    }
    return self;
}

@end



@implementation Story

@synthesize idStory, thumbnailUrl, logoUrl, isTest, title, content, isStoryViewed, viewedIDContent;

StayfilmApp *sfApp;

-(id)init
{
    self = [super init];
    if (0 != self) {
        self.idStory = nil;
        self.isTest = NO;
        self.thumbnailUrl = nil;
        self.logoUrl = nil;
        self.title = nil;
        self.content = [[NSMutableArray alloc] init];
        self.myPlayers = [[NSMutableArray alloc]init];
        self.isStoryViewed = NO;
        self.viewedIDContent = @-1;
    }
    return self;
}

+ (id)customClassWithProperties:(NSDictionary *)properties {
    if(properties != nil)
    {
        NSMutableDictionary *filteredObjs = [[NSMutableDictionary alloc] init];
        for (NSString* key in properties) {
            id value = [properties objectForKey:key];
            if ([key isEqualToString:@"idstory"]) {
                [filteredObjs setValue:value forKey:@"idStory"];
            }
            else if ([key isEqualToString:@"istest"]) {
                [filteredObjs setValue:value forKey:@"isTest"];
            }
            else if ([key isEqualToString:@"timestamp"]) {
                [filteredObjs setValue:value forKey:@"timestamp"];
            }
            else if ([key isEqualToString:@"thumbnailurl"]) {
                [filteredObjs setValue:value forKey:@"thumbnailUrl"];
            }
            else if ([key isEqualToString:@"title"]) {
                [filteredObjs setValue:value forKey:@"title"];
            }
            else if ([key isEqualToString:@"logourl"]) {
                [filteredObjs setValue:value forKey:@"logoUrl"];
            }

        }
        return [[self alloc] initWithProperties:filteredObjs];
    }
    else
    {
        return nil;
    }
}

- (id)initWithProperties:(NSDictionary *)properties {
    if (self = [self init]) {
        @try {
            [self setValuesForKeysWithDictionary:properties];
        }
        @catch (NSException *ex) {
            NSLog(@"StayLog: EXCEPTION in Story - initWithProperties with error: %@",ex.description);
            return nil;
        }
    }
    return self;
}

+ (BOOL)getSavedStories {
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"Stories"] != nil)
    {
        @synchronized (sfApp.sfConfig.stories) {
            sfApp.sfConfig.stories = nil;
            NSMutableArray *array = [[NSMutableArray alloc] init];
            NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"Stories"];
            NSDictionary *story = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            
            if(story[@"stories"] != nil) {
                for(NSDictionary *var in story[@"stories"]) {
                    [array addObject:[Story customClassWithProperties:var]];
                    
                    if (var[@"content"] != nil) {
                        for (NSDictionary *var2 in var[@"content"]) {
                            [((Story *)[array lastObject]).content addObject:[StoryContent customClassWithProperties:var2]];
                        }
                    }
                }
            }
            sfApp.sfConfig.stories = [array copy];
            return YES;
        }
    }
    return NO;
}

+(NSDictionary *)getStories
{
    NSMutableArray *array = nil;
    @try {
        StayWS *ws = [[StayWS alloc] init];
        sfApp = [StayfilmApp sharedStayfilmAppSingleton];
        NSDictionary *args = nil;
        NSDictionary *response = nil;
        NSArray *pathArgs = nil;
        
        NSString *isChanged = @"0";
        
        NSDictionary *languageDic = [NSLocale componentsFromLocaleIdentifier:[[NSLocale preferredLanguages] objectAtIndex:0]];
        NSString *lang = [languageDic objectForKey:@"kCFLocaleLanguageCodeKey"];
        
        if([[NSUserDefaults standardUserDefaults] objectForKey:@"hash_stories"] != nil)
        {
            NSString * hash = [[NSUserDefaults standardUserDefaults] stringForKey:@"hash_stories"];
            args = @{ @"device" : @"ios",
                      @"language" : lang,
                      @"hash" : hash  };
        }
        else {
            args = @{ @"device" : @"ios",
                      @"language" : lang };
        }
        
        response = [ws requestWebServiceWithURL:[sfApp.wsConfig getConfigPathWithKey:@"newGallery"] withRequestType:@"GET" withPathArguments:pathArgs withArguments:args withIdSession:sfApp.settings[@"idSession"] withCache:NO];
        
        NSHTTPURLResponse *statusResponse = (NSHTTPURLResponse *) ws.responseMessage;
        if(statusResponse.statusCode == 304)  // same stories
        {
            if(sfApp.sfConfig.stories == nil || sfApp.sfConfig.stories.count == 0)
            {
                // this next line will load saved stories into memory
                BOOL readStory = [Story getSavedStories];
                if(!readStory) { // just in case Stories is not set
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"hash_stories"];
                    return [Story getStories];
                } else {
                    return @{ @"isChanged" : isChanged };
                }
            }
            else {
                return @{ @"isChanged" : isChanged };
            }
        }
        else if(response != nil && (statusResponse.statusCode == 200 || statusResponse.statusCode == 201))
        {
            if(response[@"stories"] != nil)
            {
                array = [[NSMutableArray alloc] init];
                for(NSDictionary *var in response[@"stories"]) {
                    Story *s = [Story customClassWithProperties:var];
                    [array addObject:s];
                    
                    if (var[@"content"] != nil) {
                        for (NSDictionary *var2 in var[@"content"]) {
                            [((Story *)[array lastObject]).content addObject:[StoryContent customClassWithProperties:var2]];
                        }
                    }
                }
                isChanged = @"1";
                
                NSString *md5string = [[NSString alloc] initWithData:ws.responseBody encoding:NSUTF8StringEncoding];
                NSString *md5 = [StayfilmApp MD5String:md5string];
                [[NSUserDefaults standardUserDefaults] setObject:md5 forKey:@"hash_stories"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
        }
        else {
            return nil;
        }
        
        return @{ @"isChanged" : isChanged,
                  @"stories" : [array copy],
                  @"response" : response };
    }
    @catch (NSException *exception) {
        NSLog(@"StayLog: EXCEPTION in getStoriesWS with error: %@", exception.description);
        return nil;
    }
}

- (AVAsset*)getVideoFromContent:(StoryContent *)content{
    
    NSURL *url = [NSURL URLWithString:[content.videoUrl stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet]];
    
    NSURL *tmpDirURL = [NSURL fileURLWithPath:NSTemporaryDirectory() isDirectory:YES];
    NSString *idFile;
    if(content.idMovie != nil){
        idFile = content.idMovie;
    }else{
        idFile = @"video";
    }
    NSURL *fileURL = [[tmpDirURL URLByAppendingPathComponent:idFile] URLByAppendingPathExtension:@"mov"];
    //NSLog(@"fileURL: %@", [fileURL path]);
   
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:[fileURL path]]){
        //NSLog(@"fileURL: %@", [fileURL path]);
        NSData *urlData = [NSData dataWithContentsOfURL:url];
        [urlData writeToURL:fileURL options:NSAtomicWrite error:nil];
    }
    AVAsset *asset = [AVAsset assetWithURL:fileURL];
    return asset;
    
    
}
-(void)setPlayersfromPlayer:(int)fromPlayer toPlayer:(int)toPlayer{
    
    if(toPlayer >= self.content.count)
        toPlayer = (int)self.content.count;
    
    for (int i = fromPlayer; i < toPlayer; i++){
        
        StoryContent *content = self.content[i];
        //NSLog (@"StayLog: VIDEO URL %@",content.videoUrl);
        
        AVPlayerItem *playerItem;
        if (content.videoUrl != nil){
            AVAsset *asset;
            if(content.idMovie != nil){
             asset = [self getVideoFromContent:content]; //[AVAsset assetWithURL:url];
            playerItem = [AVPlayerItem playerItemWithAsset:asset];
            }else{
                asset = [AVAsset assetWithURL:[NSURL URLWithString:content.videoUrl]];
                playerItem = [AVPlayerItem playerItemWithAsset:asset];
            }
            
        }else{
            NSURL *url = [NSURL URLWithString:[content.imgUrl stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet]];
            playerItem = [[AVPlayerItem alloc]initWithURL:url];
        }
        
        AVPlayer *player = [AVPlayer playerWithPlayerItem:playerItem];
        [self.myPlayers addObject:player];
        
    }

    
}

-(void)setPlayersWithCompletion: (void (^)(void))completion {
    
    [self setPlayersfromPlayer:(int)self.myPlayers.count toPlayer:(int)self.content.count];
    
    completion();
}

-(void)setPlayer:(StoryContent*)content withCompletion:(CompletionBlock)completion {
    
    AVPlayer *player = [AVPlayer playerWithPlayerItem:[AVPlayerItem playerItemWithAsset:[self getVideoFromContent:content]]];
    
     completion (player);
}


@end
