//
//  VideoFB.h
//  Stayfilm-iOS-Full
//
//  Created by Henrique on 5/3/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VideoFB : NSObject

@property (nonatomic, strong) NSString *idVideo;
@property (nonatomic, strong) NSNumber *length;
@property (nonatomic, strong) NSString *source;
@property (nonatomic, strong) NSString *picture;
@property (nonatomic, strong) NSArray *format;
@property (nonatomic, strong) NSString *privacy;

+ (id)customClassWithProperties:(NSDictionary *)properties;
- (id)initWithProperties:(NSDictionary *)properties;

@end
