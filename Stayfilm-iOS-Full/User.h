//
//  User.h
//  Stayfilm for Messenger
//
//  Created by Henrique Ormonde on 08/05/15.
//  Copyright (c) 2015 Stayfilm. All rights reserved.
//

#ifndef Stayfilm_Full_User_h
#define Stayfilm_Full_User_h

#import <Foundation/Foundation.h>
#import "StayfilmApp.h"
@class StayfilmApp;

@interface User : NSObject

@property (nonatomic, strong) NSString *idUser;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *fullName;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *gender;
@property (nonatomic, strong) NSNumber *lastAccess;
@property (nonatomic, strong) NSNumber *created;
@property (nonatomic, strong) NSString *photo;
@property (nonatomic, strong) NSString *birthday;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *locale;
@property (nonatomic, strong) NSString *languages;
@property (nonatomic, strong) NSString *idCoverMovie;
@property (nonatomic, strong) NSString *idFacebook;
@property (nonatomic, strong) NSNumber *likeCount;
@property (nonatomic, strong) NSString *role;
@property (nonatomic, strong) NSNumber *friendCount;
@property (nonatomic, strong) NSNumber *countPendingRequests;
@property (nonatomic, strong) NSNumber *countPendingMovies;
@property (nonatomic, strong) NSNumber *movieCount;
@property (nonatomic, strong) NSNumber *notifications;
@property (nonatomic, strong) NSMutableArray *networks;
@property BOOL userExists;
@property BOOL isLogged;
@property BOOL isResponseError;
@property (nonatomic, strong) NSString *responseErrorMessage;
@property (nonatomic, strong) NSString *idSession;
@property BOOL hasSubscription;

+ (id)customClassWithProperties:(NSDictionary *)properties;
- (id)initWithProperties:(NSDictionary *)properties;
+(User *)loginWithUsername:(NSString *)p_username withPassword:(NSString *)p_password andSFAppSingleton:(StayfilmApp *)p_sfSingleton;
+(User *)loginWithIdSession:(NSString *)p_idSession andSFAppSingleton:(StayfilmApp *)p_sfSingleton;
+(User *)loginWithFacebook:(NSString *)p_token andSFAppSingleton:(StayfilmApp *)p_sfSingleton;
+(User *)loginWithFacebookWithoutUserOverride:(NSString *)p_token andSFAppSingleton:(StayfilmApp *)p_sfSingleton;
+(User *)registerUserWithArguments:(NSDictionary *)p_args andSFAppSingleton:(StayfilmApp *)p_sfSingleton;
-(BOOL)setUserFacebookToken:(NSString *)p_token andSFAppSingleton:(StayfilmApp *)p_sfSingleton;
-(BOOL)setUserInstagramToken:(NSString *)p_token andSFAppSingleton:(StayfilmApp *)p_sfSingleton;
-(BOOL)setUserGoogleToken:(NSString *)p_token andSFAppSingleton:(StayfilmApp *)p_sfSingleton;
-(BOOL)setUserTwitterToken:(NSString *)p_token andSFAppSingleton:(StayfilmApp *)p_sfSingleton;

-(BOOL)getUserNetworksWithSFAppSingleton:(StayfilmApp *)p_sfSingleton;
-(NSString *)getRelationshipStatus:(User *)p_friend andSFAppSingleton:(StayfilmApp *)p_sfSingleton;
-(BOOL)setRelationship:(User *)p_friend withAction:(NSString *)p_action andSFAppSingleton:(StayfilmApp *)_p_sfSingleton;

+(User *)getUserWithId:(NSString *)p_idUser andSFAppSingleton:(StayfilmApp *)p_sfSingleton;

+(NSString *)sendRecoveryEmail:(NSString *)p_email andSFAppSingleton:(StayfilmApp *)p_sfSingleton;
+(NSString *)sendRecoveryCode:(NSString *)p_code withEmail:(NSString *)p_email withNewPassword:(NSString *)p_password andSFAppSingleton:(StayfilmApp *)p_sfSingleton;
+(NSString *)sendRecoveryToken:(NSString *)p_code withEmail:(NSString *)p_email andSFAppSingleton:(StayfilmApp *)p_sfSingleton;
+(BOOL)updateProfile:(NSString *)userName LastName:(NSString *)userLastName andSFAppSingleton:(StayfilmApp *)sfSingleton;


typedef void(^subscriptionCompletion)(BOOL);

-(void)validateSubscription:(subscriptionCompletion)completionBlock;
@end

#endif
