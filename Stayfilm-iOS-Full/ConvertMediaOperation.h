//
//  ConvertMediaOperation.h
//  Stayfilm for Messenger
//
//  Created by Henrique Ormonde on 27/10/15.
//  Copyright © 2015 Stayfilm. All rights reserved.
//

@import Photos;
#import <Foundation/Foundation.h>
#import "Media.h"

@protocol ConvertMediaDelegate;

@interface ConvertMediaOperation : NSOperation

@property (nonatomic, assign) id <ConvertMediaDelegate> delegate;
@property (nonatomic, strong) PHAsset *asset;
@property (nonatomic, strong) NSString *mediaPath;
@property (nonatomic, strong) NSString *pathConvertedFile;


- (id)initWithAsset:(PHAsset *)p_asset withMediaPath:(NSString *)p_mediaPath delegate:(id<ConvertMediaDelegate>) theDelegate;

@end

@protocol ConvertMediaDelegate <NSObject>
- (void)converterDidFinish:(ConvertMediaOperation *)converter;
- (void)converterDidFail:(ConvertMediaOperation *)converter;
@end
