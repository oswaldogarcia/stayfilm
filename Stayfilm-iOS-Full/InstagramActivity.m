//
//  InstagramActivity.m
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 15/05/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import "InstagramActivity.h"


NSString *const kAQSInstagramURLScheme = @"instagram://app";

@interface InstagramActivity () <ProgressBarViewControllerDelegate>

@property (nonatomic, strong) NSArray *activityItems;

@property (nonatomic, strong) UIDocumentInteractionController *controller;
@property (nonatomic, assign) BOOL isPerformed;
@property (nonatomic, assign) NSURL* path;
@end


@implementation InstagramActivity


- (void)prepareWithActivityItems:(NSArray *)activityItems {
    [super prepareWithActivityItems:activityItems];
   
}

+ (UIActivityCategory)activityCategory {
    return UIActivityCategoryShare;
}

//- (NSString *)activityType {
//    return @"org.instagram";
//}

- (UIActivityType)activityType{
    return @"Instagram";
}

- (NSString *)activityTitle {
    return @"Instagram";
}

- (UIImage *)activityImage {
    
    return [UIImage imageNamed:@"SHARE_Instagram"];
    
}

- (BOOL)canPerformWithActivityItems:(NSArray *)activityItems {
    return [self isInstagramInstalled];
}

- (void)               video: (NSString *) videoPath
    didFinishSavingWithError: (NSError *) error
                 contextInfo: (void *) contextInfo
{
    
    NSString * urlBase = [NSString stringWithFormat:@"instagram://library?AssetPath=%@&InstagramCaption=%@",[videoPath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet alphanumericCharacterSet]], [self.movie.title stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet alphanumericCharacterSet]]];
    NSURL *instagramURL = [NSURL URLWithString:urlBase];
    
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error1;
    BOOL fileExists = [fileManager fileExistsAtPath:videoPath];
//    NSLog(@"Path to file: %@", videoPath);
//    NSLog(@"File exists: %d", fileExists);
//    NSLog(@"Is deletable file at path: %d", [fileManager isDeletableFileAtPath:videoPath]);
    if (fileExists)
    {
        BOOL success = [fileManager removeItemAtPath:videoPath error:&error1];
        if (!success) NSLog(@"Error: %@", [error1 localizedDescription]);
    }
    
    if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
        [[UIApplication sharedApplication] openURL:instagramURL];
    
    }
   
    
}


- (void)performActivity {
    
   
    self.progressView = [[ProgressBarViewController alloc] init];
    self.progressView.downloadProgressBar.progress = 0;
    self.progressView.lbl_downloading.text = NSLocalizedString(@"DOWNLOADING", nil);
    self.progressView.delegate = self;
   
    if (self.profileView != nil){
        
        self.progressView.transitioningDelegate = self.profileView;
        self.progressView.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        self.progressView.view.frame = [UIScreen mainScreen].bounds;
        self.profileView.transitionAnimation.duration = 0.1;
        self.profileView.transitionAnimation.isPresenting = YES;
        self.profileView.transitionAnimation.originFrame = [UIScreen mainScreen].bounds;
        
         [self.profileView presentViewController:self.progressView animated:YES completion:nil];
        
    }else{
        self.progressView.transitioningDelegate = self.videoFinishView;
        self.progressView.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        self.progressView.view.frame = [UIScreen mainScreen].bounds;
        self.videoFinishView.transitionAnimation.duration = 0.1;
        self.videoFinishView.transitionAnimation.isPresenting = YES;
        self.videoFinishView.transitionAnimation.originFrame = [UIScreen mainScreen].bounds;
        
         [self.videoFinishView presentViewController:self.progressView animated:YES completion:nil];
    }
    
    [self downloadVideoFromURL:self.movie.videoUrl withProgress:^(CGFloat progress) {
       
        runOnMainQueueWithoutDeadlocking(^{
            
            //NSLog(@"%f", progress * 100);
            self.progressView.progressLabel.text = [NSString stringWithFormat:@"%d%%", (int)(progress * 100)];
            self.progressView.downloadProgressBar.progress = progress ;
            
            if (progress >= 1.0){
                 self.progressView.lbl_downloading.text = NSLocalizedString(@"DOWNLOADED", nil);
                 [self activityDidFinish:YES];
                [self.progressView dismissViewControllerAnimated:YES completion:nil];
                
            }
        });
        
        
    } completion:^(NSURL *filePath) {
      
        self.path = filePath;
        
       if(UIVideoAtPathIsCompatibleWithSavedPhotosAlbum([filePath path])) {
            UISaveVideoAtPathToSavedPhotosAlbum([filePath path],self,@selector(video:didFinishSavingWithError:contextInfo:),nil);
        }

    } onError:^(NSError *error) {
        NSLog(@"StayLog: ERROR: %@", error);
    }];

}


- (void)activityDidFinish:(BOOL)completed{
    
//     NSLog(@"activityDidFinish");
    
}
- (void)chooseOption:(NSString *)option{
    
    if ([option isEqualToString:@"cancelDownload"]){
        
         [self.downloadTask cancel];
        
        [self.manager.operationQueue cancelAllOperations];
        
        [self removeVideoAtPath:self.path];
    }
    
}

# pragma mark - Helpers (Instagram)

- (BOOL)isInstagramInstalled {
    return [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:kAQSInstagramURLScheme]];
}


- (void) downloadVideoFromURL: (NSString *) URL withProgress:(void (^)(CGFloat progress))progressBlock completion:(void (^)(NSURL *filePath))completionBlock onError:(void (^)(NSError *error))errorBlock
{
    //Configuring the session manager
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    self.manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    //Most URLs I come across are in string format so to convert them into an NSURL and then instantiate the actual request
    NSURL *formattedURL = [NSURL URLWithString:URL];
    NSURLRequest *request = [NSURLRequest requestWithURL:formattedURL];
    
    //Watch the manager to see how much of the file it's downloaded
    [self.manager setDownloadTaskDidWriteDataBlock:^(NSURLSession *session, NSURLSessionDownloadTask *downloadTask, int64_t bytesWritten, int64_t totalBytesWritten, int64_t totalBytesExpectedToWrite) {
        //Convert totalBytesWritten and totalBytesExpectedToWrite into floats so that percentageCompleted doesn't get rounded to the nearest integer
        CGFloat written = totalBytesWritten;
        CGFloat total = totalBytesExpectedToWrite;
        CGFloat percentageCompleted = written/total;
        
        //Return the completed progress so we can display it somewhere else in app
        progressBlock(percentageCompleted);
    }];
    
    //Start the download
    self.downloadTask = [self.manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        
        //Getting the path of the document directory
        NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
        /*NSURL *dataPath = [documentsDirectoryURL URLByAppendingPathComponent:@"StayFilms"];
      
//        Create Stayfilm folder if it does not exist
       NSError *error;
        if (![[NSFileManager defaultManager] fileExistsAtPath:[dataPath path]])
        {
            [[NSFileManager defaultManager] createDirectoryAtPath:[dataPath path] withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
        }*/
        
        NSURL *fullURL = [documentsDirectoryURL URLByAppendingPathComponent:[NSString stringWithFormat:@"%@.mp4",self.movie.idMovie]];
        
        self.path = fullURL;
        //If we already have a video file saved, remove it from the phone
        //[self removeVideoAtPath:fullURL];
        return fullURL;
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        if (!error) {
            //If there's no error, return the completion block
            completionBlock(filePath);
        } else {
            //Otherwise return the error block
            errorBlock(error);
        }
        
    }];
    
    [self.downloadTask resume];
}


- (void)removeVideoAtPath:(NSURL *)filePath
{
    NSString *stringPath = filePath.path;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:stringPath]) {
        [fileManager removeItemAtPath:stringPath error:NULL];
    }
}


@end
