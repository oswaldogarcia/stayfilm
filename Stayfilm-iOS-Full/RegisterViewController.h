//
//  RegisterViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Henrique on 1/29/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "StayfilmApp.h"
#import "LoginFacebookOperation.h"


@interface RegisterViewController : UIViewController <FBSDKLoginButtonDelegate, StayfilmSingletonDelegate, LoginFacebookOperationDelegate, UIPickerViewDataSource, UIPickerViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imageBackground;
@property (weak, nonatomic) IBOutlet UILabel *lbl_version;

@property (weak, nonatomic) IBOutlet UIView *view_Step1;

@property (weak, nonatomic) IBOutlet UIButton *but_back;
@property (weak, nonatomic) IBOutlet FBSDKLoginButton *loginFacebookButton;
@property (weak, nonatomic) IBOutlet UIButton *but_FacebookCustom;

@property (weak, nonatomic) IBOutlet UILabel *lbl_Email;
@property (weak, nonatomic) IBOutlet UITextField *txt_Email;
@property (weak, nonatomic) IBOutlet UITextField *txt_PasswordStep1;
@property (weak, nonatomic) IBOutlet UILabel *lbl_PasswordStep1;
@property (weak, nonatomic) IBOutlet UIButton *but_CreateAccount;
@property (weak, nonatomic) IBOutlet UIButton *but_TermsAgreements;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinnerLoaderStep1;
@property (weak, nonatomic) IBOutlet UILabel *lbl_feedback;

@property (weak, nonatomic) IBOutlet UIView *view_Step2;

@property (weak, nonatomic) IBOutlet UILabel *lbl_FirstName;
@property (weak, nonatomic) IBOutlet UITextField *txt_FirstName;
@property (weak, nonatomic) IBOutlet UILabel *lbl_LastName;
@property (weak, nonatomic) IBOutlet UITextField *txt_LastName;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Password;
@property (weak, nonatomic) IBOutlet UITextField *txt_Password;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinnerLoaderStep2;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Required;
@property (weak, nonatomic) IBOutlet UIButton *but_Continue;

@property (weak, nonatomic) IBOutlet UIView *view_Step3;

@property (weak, nonatomic) IBOutlet UIPickerView *picker_Gender;
@property (weak, nonatomic) IBOutlet UIDatePicker *picker_Date;
@property (weak, nonatomic) IBOutlet UIButton *but_Gender;
@property (weak, nonatomic) IBOutlet UIButton *but_Birthday;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinnerLoaderStep3;
@property (weak, nonatomic) IBOutlet UIButton *but_ContinueFinish;
@property (weak, nonatomic) IBOutlet UIButton *but_Skip;

@property (weak, nonatomic) IBOutlet UIView *view_Created;
@property (weak, nonatomic) IBOutlet UIView *view_Loader;

@property (weak, nonatomic) IBOutlet UIImageView *img_UserProfile;
@property (weak, nonatomic) IBOutlet UILabel *lbl_UserName;
@property (weak, nonatomic) IBOutlet UILabel *lbl_UserEmail;
@property (weak, nonatomic) IBOutlet UIButton *but_EditProfile;
@property (weak, nonatomic) IBOutlet UIButton *but_Start;


@end
