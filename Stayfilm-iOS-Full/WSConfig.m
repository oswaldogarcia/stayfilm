//
//  WSConfig.m
//  Stayfilm for Messenger
//
//  Created by Henrique Ormonde on 27/04/15.
//  Copyright (c) 2015 Stayfilm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WSConfig.h"


@interface WSConfig ()

@property (nonatomic, strong) NSString *host;

@end

@implementation WSConfig

@synthesize wsAddress=_wsAddress, configPaths=_configPaths, facebookID=_facebookID, host;


-(id)init
{
    self = [super init];
    if (0 != self) {
        
        /// Environment
        self.host = @"https://www.stayfilm.com/";
        //self.host = @"https://staging.stayfilm.com/";
//        self.host = @"http://lucas.office.stayfilm.com.br/";
//        self.host = @"https://testelucas.stayfilm.com/";
        //NSString *host = @"http://192.168.10.73/"; //lucas
        //self.host = @"http://10.0.0.157/";  //steve
        //NSString *host = @"http://fabiano.stayfilm.com/";
        //NSString *host = @"http://artur.office.stayfilm.com.br/cool/";
        //NSString *host = @"http://test.stayfilm.com/";
        
        NSString *APIversion = @"1.2/";
        
        self.wsAddress = [self.host stringByAppendingString:@"rest/"];
        self.wsAddress = [self.wsAddress stringByAppendingString:APIversion];
        self.facebookID = @"129339127276735"; // prod
        self.configPaths = @{
                             @"login"                     : @"session",
                             @"loginFacebook"             : @"user/%@",
                             @"loginSocial"               : @"network/%@?deviceType=mobile&idsession=%@",
                             @"feed"                      : @"user/%@/feed",
                             @"userConfig"                : @"user/config",
                             @"getConfig"                 : @"config",
                             @"getTerms"                  : @"term",
                             @"getGenre"                  : @"genre/%@",
                             @"gallery"                   : @"gallery",
                             @"yesOfTheWeek"              : @"bestof",
                             @"getUser"                   : @"user/%@",
                             @"getPushNotification"       : @"pushnotification/%@",
                             @"getUserNetworks"           : @"user/%@/network",
                             @"getRelationshipStatus"     : @"user/%@",
                             @"setRelationship"           : @"user/%@/friend",
                             @"setCoverMovie"             : @"user/%@",
                             @"getFriendRequests"         : @"user/%@/friend",
                             @"facebookFriends"           : @"user/%@/network",
                             @"updateUserSettings"        : @"user/%@",
                             @"updateUser"                : @"user/%@",
                             @"updateUserPicture"         : @"user/%@/profile",
                             @"myFilms"                   : @"user/%@/movie",
                             @"filmates"                  : @"user/%@/friend",
                             @"notifications"             : @"user/%@/notification",
                             @"notificationMarkAsRead"    : @"user/%@/notification",
                             @"getMovie"                  : @"movie/%@",
                             @"setPrivacy"                : @"movie/%@",
                             @"likeMovie"                 : @"movie/%@/like",
                             @"publishMovie"              : @"movie/%@",
                             @"shareMovie"                : @"user/%@/feed",
                             @"shareMovieOnFacebook"      : @"movie/%@",
                             @"downloadedMovie"           : @"movie/%@/download",
                             @"userRegister"              : @"user",
                             @"userLatestYes"             : @"user/%@/like",
                             @"checkSession"              : @"user/checkSession",
                             @"postJob"                   : @"job",
                             @"getJob"                    : @"job/%@",
                             @"postMovieComment"          : @"movie/%@/comment",
                             @"deleteMovieComment"        : @"movie/%@/comment/%@",
                             @"deleteMovie"               : @"movie/%@",
                             @"getMedia"                  : @"media/%@",
                             @"deleteMedia"               : @"media/%@",
                             @"createMedia"               : @"media",
                             @"getThemes"                 : @"theme",
                             @"newGallery"                : @"newgallery",
                             @"search"                    : @"search",
                             @"networkResource"           : @"user/%@/token/%@",
                             @"getUserEmailConfiguration" : @"user/%@/config",
                             @"albumManagerMedia"         : @"user/%@/media",
                             @"albumManagerAlbum"         : @"user/%@/album",
                             @"getAlbum"                  : @"user/%@/album/%@",
                             @"passwordRecovery"          : @"password",
                             @"token"                     : @"token",
                             @"exception"                 : @"exception"
                            };
    }
    return self;
}

-(NSString *)getConfigPathWithKey:(NSString *)key {
    
    NSString *value = [[NSString alloc] init];
    
    if(self.configPaths[key] == nil){
        NSLog(@"StayLog: Error getting WebService path with key: nil");
        return value;
    }
    
    if([self.configPaths[key] length] == 0)
    {
        NSLog(@"StayLog: Error getting WebService path with key: %@", key);
        return value;
    }
    else
    {
        return [self.wsAddress stringByAppendingString:self.configPaths[key]];
    }
}

@end
