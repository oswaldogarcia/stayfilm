//
//  BPHelper.m
//  Stayfilm
//
//  Created by Victor Manuel Roldan on 5/16/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import "BPHelper.h"
#import "BPDefinitions.h"

@implementation BPHelper

#define LIGHT_CONTENT_SECTION_LEFT 1
#define LIGHT_CONTENT_SECTION_RIGHT 2
#define LIGHT_CONTENT_SECTION_TOP 3
#define LIGHT_CONTENT_SECTION_BOTTOM 4

#pragma mark - Session methods

+ (void) saveInSession:(NSObject*)object forKey:(NSString*)key {
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    
    if ([object isKindOfClass:[NSDictionary class]]) {
        
        NSDictionary *entityUser = [self removeNullValues:(NSDictionary*)object];
        [user setObject:entityUser forKey: key];
    }
    else {
        [user setObject:object forKey:key];
    }
    
    [user synchronize];
    
}

+ (NSObject*) getSessionObject:(NSString*)key {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    return [userDefaults valueForKey:key];
    
}

+ (void) removeFromSession:(NSString*)key {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults removeObjectForKey:key];
    [userDefaults synchronize];
}


#pragma mark - Dictionary methods

+ (NSDictionary*)removeNullValues:(NSDictionary*) dictionary {
    
    NSMutableDictionary *mutableDict = [dictionary mutableCopy];
    NSMutableArray *keysToDelete = [NSMutableArray array];
    
    [mutableDict enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        if (obj == [NSNull null]) {
            [keysToDelete addObject:key];
        }
    }];
    [mutableDict removeObjectsForKeys:keysToDelete];
    return [mutableDict copy];
}

+ (NSDictionary*) convertJSonToDictionary:(NSData*)json {
    
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:json options:kNilOptions error:nil];
    
    return dictionary;
}

#pragma mark - String formats methods

+ (BOOL) isValidEmail:(NSString*)emailString {
    
    if([emailString length]==0){
        return NO;
    }
    
    NSString *regExPattern = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    
    NSRegularExpression *regEx = [[NSRegularExpression alloc]
                                  initWithPattern:regExPattern
                                  options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
    
    NSLog(@"%lu", (unsigned long)regExMatches);
    if (regExMatches == 0) {
        return NO;
    } else {
        return YES;
    }
    
}
+(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

+ (void) showAlert:(UIViewController*)controller withTitle:(NSString*)title withMessage:(NSString*)message{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:title
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"Ok"
                               style:UIAlertActionStyleDefault
                               handler:nil];
    [alert addAction:okButton];
    [controller presentViewController:alert animated:YES completion:nil];
}

+ (void) showAlert:(UIViewController*)controller
         withTitle:(NSString*)title
       withMessage:(NSString*)message
 withMessageButton:(NSString*) messageButton
         withBlock:(void (^ _Nullable)(UIAlertAction * _Nonnull __strong)) block{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:title
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* button = [UIAlertAction
                               actionWithTitle: messageButton
                               style:UIAlertActionStyleDefault
                               handler: block];
    [alert addAction:button];
    [controller presentViewController:alert animated:YES completion:nil];
}

+ (void) showAlertWithCancel:(UIViewController*)controller
         withTitle:(NSString*)title
       withMessage:(NSString*)message
 withMessageButton:(NSString*) messageButton
         withBlock:(void (^ _Nullable)(UIAlertAction * _Nonnull __strong)) block{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:title
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                             actionWithTitle: messageButton
                             style:UIAlertActionStyleDefault
                               handler:block];
    
    UIAlertAction* cancelButton = [UIAlertAction
                             actionWithTitle: @"cancel".capitalizedString
                             style:UIAlertActionStyleDefault
                             handler: nil];
    
    [alert addAction:okButton];
    [alert addAction:cancelButton];
    [controller presentViewController:alert animated:YES completion:nil];
}

+ (void) showAlertWithBackRedirection:(UIViewController*)controller withTitle:(NSString*)title withMessage:(NSString*)message{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:title
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"Ok"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action){
                                   [controller.navigationController popToRootViewControllerAnimated:YES];
                               }];
    [alert addAction:okButton];
    [controller presentViewController:alert animated:YES completion:nil];
}


+ (GenericModel *) convertNSErrorToGenericModel:(NSError*)error{
    
    NSString* errorMessage = [[NSString alloc] initWithData:(NSData *)error.userInfo[@"com.alamofire.serialization.response.error.data"] encoding:NSUTF8StringEncoding];
    NSData *objectData = [errorMessage dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData: objectData options:NSJSONReadingMutableLeaves error:nil];
    GenericModel *generic = [[GenericModel alloc] initWithDictionary:json];
    
    if(error.code == NSURLErrorNotConnectedToInternet){
        generic.message = @"Turn Off Airplane mode or Use Wi-Fi to Access Data.";
        generic.code = [NSNumber numberWithInt:9999999];
        generic.title = @"No Internet Connection";
        return generic;
    }
    
    if(generic.code == nil || [generic.statusCode isEqualToNumber:[NSNumber numberWithInteger:500]]){
        NSHTTPURLResponse *response = error.userInfo[@"com.alamofire.serialization.response.error.response"];
        NSString *timeout = [error.userInfo valueForKey:@"NSLocalizedDescription"];
        NSInteger statusCode = response.statusCode;
        
        generic.message = [timeout containsString:@"timed out"] ? timeout : ERROR_SERVER_DEFAULT_MESSAGE;
        generic.code = [NSNumber numberWithInteger:statusCode];
        generic.title = ERROR_SERVER_DEFAULT_KEY;
    }
    
    return generic;
}

+ (NSString *) clenWhiteSpacesWithString:(NSString*)text{
    NSString *newString = text;
    [newString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *str2 = newString;
    return str2;
}

// Validate the input string with the given pattern and
// return the result as a boolean
+ (BOOL)validateString:(NSString *)string withPattern:(NSString *)pattern
{
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:&error];
    
    NSAssert(regex, @"Unable to create regular expression");
    
    NSRange textRange = NSMakeRange(0, string.length);
    NSRange matchRange = [regex rangeOfFirstMatchInString:string options:NSMatchingReportProgress range:textRange];
    
    BOOL didValidate = NO;
    
    // Did we find a matching range
    if (matchRange.location != NSNotFound)
        didValidate = YES;
    
    return didValidate;
}

//format date: from yyyy-MM-dd HH:mm:ss to Custom Format
+(NSString*)changeDateFormatWithDateString:(NSString*)dateString withFormat:(NSString*)format{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *originalDate = [dateFormatter dateFromString:dateString];
    [dateFormatter setDateFormat:format];
    NSString *formattedDateString = [dateFormatter stringFromDate:originalDate];
    //[dateFormatter release];
    return formattedDateString;
    
}


//format date: from MM-dd-yyyy to Custom Format
+(NSString*)changeDateFormatWithDateString2:(NSString*)dateString withFormat:(NSString*)format{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd-yyyy"];
    NSDate *originalDate = [dateFormatter dateFromString:dateString];
    [dateFormatter setDateFormat:format];
    NSString *formattedDateString = [dateFormatter stringFromDate:originalDate];
    //[dateFormatter release];
    return formattedDateString;
    
}


//format time: from 13:10:10 to 01:10 PM 
+(NSString*)changeHourFormat:(NSString*)timeString{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    if ([timeString containsString:@"PM"] || [timeString containsString:@"AM"]) {
        [dateFormatter setDateFormat:@"HH:mm a"];
    } else {
        [dateFormatter setDateFormat:@"HH:mm:ss"];
    }
    NSDate *originalDate = [dateFormatter dateFromString:timeString];
    [dateFormatter setDateFormat:@"hh:mm a"];
    NSString *formattedDateString = [dateFormatter stringFromDate:originalDate];
    //[dateFormatter release];
    return formattedDateString;
}

//format time: from 01:10 PM to 13:10:10
+(NSString*)changeHourFormat2:(NSString*)timeString{
    NSLocale *local = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:local];
    [dateFormatter setDateFormat:@"hh:mm a"];
    NSDate *originalDate = [dateFormatter dateFromString:timeString];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSString *formattedDateString = [dateFormatter stringFromDate:originalDate];
    //[dateFormatter release];
    return formattedDateString;
}

+(void)errorHandlingWithError:(NSError*)error withViewController:(UIViewController*)ViewController{
    GenericModel * response = [self convertNSErrorToGenericModel:error];
    if([response.code isEqualToNumber:[NSNumber numberWithInt:401]]){
        [self showAlert:ViewController withTitle:@"Unauthorized" withMessage:@"Bad credentials API"];
    }else if([response.code isEqualToNumber:[NSNumber numberWithInt:9999999]]){

        UIAlertController * alert = [UIAlertController alertControllerWithTitle:response.title message:response.message preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction * actionOk = [UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self okButtonPressedGoSettings];
        }];
        UIAlertAction * actionCancel = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alert addAction:actionOk];
        [alert addAction:actionCancel];
        [ViewController presentViewController:alert animated:YES completion:nil];

    }else{
        if([response.title isEqualToString:ERROR_SERVER_DEFAULT_KEY]){
            [self showAlert:ViewController withTitle:ERROR_SERVER_DEFAULT_TITLE withMessage:response.message];
        }else{
            NSLog(@"validation error: %@", response.message);
            [self showAlert:ViewController withTitle:VALIDATION_DEFAULT_TITLE withMessage:response.message];
        }
    }
}

+(void)okButtonPressedGoSettings{
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString:@"App-Prefs:root=WIFI"]];
}


+(NSNumber*)convertToNSNumberFromString:(NSString*)stringNumber{
    NSNumberFormatter *format = [[NSNumberFormatter alloc] init];
    format.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *numberValue;
    if([stringNumber isEqualToString:@""]){
        numberValue = 0;
    }else{
        if([BPHelper validateString:stringNumber withPattern:@"^[0-9]+(.{1}[0-9]+)?$"]){
            numberValue = [format numberFromString:stringNumber];
        }else{
            numberValue = 0;
        }
    }
    
    return numberValue;
}

+(double)convertToFloatFromString:(NSString*)stringNumber{
    double val;
    if([BPHelper validateString:stringNumber withPattern:@"^[0-9]+(.{1}[0-9]+)?$"]){
        val = [stringNumber floatValue];
    }else{
        val = 0.0;
    }
    
    return val;
}

+(BOOL)isPlusMember: (UserModel*)user{
    BOOL isPlus;
    if([user.userLevel isEqualToNumber:[NSNumber numberWithInteger:0]]){
        isPlus = NO;
    }else{
        isPlus = YES;
    }
    return isPlus;
}

+(void)changePlusMember: (BOOL )isPlus{
    NSObject *userObject = [BPHelper getSessionObject:sessionUser];
    NSMutableDictionary * dict =  [[NSMutableDictionary alloc]initWithDictionary: (NSDictionary*)userObject];
    int level = isPlus ? 1 : 0;
    [dict setValue: [NSNumber numberWithInt:level] forKey:@"user_level"];
    [BPHelper saveInSession:(NSDictionary*)dict forKey:sessionUser];

}

+(NSInteger)compareDatesWithDate:(NSString*)dateString{
    NSDateFormatter *dateFormatte = [[NSDateFormatter alloc] init];
    [dateFormatte setDateFormat:@"yyyy-MM-dd"];
    NSDate *date2 = [dateFormatte dateFromString:dateString];
    NSDate *date1 = [NSDate date];
    NSInteger value;
    switch ([date2 compare:date1]) {
        case NSOrderedAscending:
            //Do your logic when date1 > date2
            NSLog(@"date1 > date2");
            value = 1;
            break;
            
        case NSOrderedDescending:
            //Do your logic when date1 < date2
            NSLog(@"date1 < date2");
            value = 2;
            break;
            
        case NSOrderedSame:
            //Do your logic when date1 = date2
            NSLog(@"date1 = date2");
            value = 3;
            break;
    }
    
    return value;
}

+(int)getRandomNumberBetween:(int)from to:(int)to {
    
    return (int)from + arc4random() % (to-from+1);
}


+(NSString *)hexStringFromColor:(UIColor *)color {
    if(color == nil){
        return @"";
    }
    const CGFloat *components = CGColorGetComponents(color.CGColor);
    
    CGFloat r = components[0];
    CGFloat g = components[1];
    CGFloat b = components[2];
    
    NSString *stringColor;
    stringColor = [NSString stringWithFormat:@"#%02lX%02lX%02lX",
            lroundf(r * 255),
            lroundf(g * 255),
            lroundf(b * 255)];
    
    return stringColor;
}

+(UIColor *) colorWithHexString: (NSString *)hexString{
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    
    NSLog(@"colorString :%@",colorString);
    CGFloat alpha, red, blue, green;
    // #RGB
    alpha = 1.0f;
    red   = [self colorComponentFrom: colorString start: 0 length: 2];
    green = [self colorComponentFrom: colorString start: 2 length: 2];
    blue  = [self colorComponentFrom: colorString start: 4 length: 2];
    
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}

+ (CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length {
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

+(NSString *)getRGBFromUIColor:(UIColor*)color{
    CGFloat red, green, blue, alpha;
    [color getRed:&red green:&green blue:&blue alpha:&alpha];
    NSString *rgbColor = [NSString stringWithFormat:@"%f,%f,%f,", floorf(red), floorf(green), floorf(blue)];
    return rgbColor;
}
+(NSString *)getRGBFromOverlayColor:(UIColor*)color{
    
    if([color isEqual: [UIColor whiteColor]]){
        return @"255,255,255,";
    }else if([color isEqual:[UIColor blackColor]]){
        return @"0,0,0," ;
    }else{
        return @"119,119,119,";
    }
}

+(NSString*)getRoundedValueFromCGFloat:(NSString*)string{
    NSArray *splitVal = [string componentsSeparatedByString:@"."];
    NSString *roundedValue=@"";
    if(splitVal.count>0){
        roundedValue = [splitVal objectAtIndex:0];
    }else{
        roundedValue = string;
    }
    return roundedValue;
}

/*
Transform values from web to iOS
currentSlide: is the value from salid. values between 121 and 484
collageTotalWidth: is the value of width overlay.
 */
+(CGFloat)overlayWidthTransform:(int)currentSlide withCollageWidth:(CGFloat)collageTotalWidth{
    //calculate web percent width
    CGFloat currentSlidePercent = (currentSlide * 100) / 484;
    
    //calculate mobile width
    CGFloat overlayWidth = (currentSlidePercent * collageTotalWidth) / 100;
    
    return overlayWidth;
}

/*
 Transform values from iOS to Web
 currentCollageWidth: this is the current width
 collageTotalWidth: this is the total width of collage area
 */
+(CGFloat)overlayWidthTransformFromiOS:(int)currentCollageWidth withCollageWidth:(CGFloat)collageTotalWidth{
    //calculate ios percent in the screen
    CGFloat currentOverlayPercent = (currentCollageWidth * 100) / collageTotalWidth;
    //calculate web percent, slider.
    CGFloat sliderValue = (currentOverlayPercent * 484) / 100;
    return sliderValue;
}

#pragma mark - EditBabyPage methods Start

+ (int) getCurrentAlignmentFromString: (NSString *) alignment{
    
    if([alignment isEqualToString:@"left"]){
        return ALLIGNMENT_LEFT;
    }else if([alignment isEqualToString: @"right"]){
        return ALLIGNMENT_RIGHT;
    }else{
        return  ALLIGNMENT_CENTER;
    }
    
}

+ (UIColor *)getCurrentColorFromString: (NSString *) fontColor{
    
    if([fontColor isEqualToString:@"white"]){
        return [UIColor whiteColor];
    }else if([fontColor isEqualToString: @"black"]){
        return [UIColor blackColor];
    }else{
        return  [UIColor blackColor];
    }
    
}
+ (UIColor *)getColorFromString: (NSString *) overlayColor{
    
    if([overlayColor isEqualToString:@"255,255,255,"]){
        return [UIColor whiteColor];
    }else if([overlayColor isEqualToString: @"0,0,0,"]){
        return [UIColor blackColor];
    }if([overlayColor isEqualToString: @"119,119,119,"]){
        return [UIColor grayColor];
    }else{
        return  [UIColor blackColor];
    }
    
}

+ (float)getCurrentSizeFromString: (NSString *) fontSize{
    
    return [fontSize floatValue];
    
}

+ (int)getCurrentOverlayPositionfromString: (NSString *) position{
    
    if([position isEqualToString:@"left"]){
        return LIGHT_CONTENT_SECTION_LEFT;
    }else if ([position isEqualToString:@"right"]){
        return LIGHT_CONTENT_SECTION_RIGHT;
    }else if ([position isEqualToString:@"top"]) {
        return LIGHT_CONTENT_SECTION_TOP;
    }else if ([position isEqualToString:@"bottom"]) {
        return LIGHT_CONTENT_SECTION_BOTTOM;
    }else{
        return LIGHT_CONTENT_SECTION_LEFT;
    }

}

+ (int) convertOuncesToPounds: (int) ounces {
    return ounces / 16;
}

+ (int) convertOuncesToLb: (int) ounces {
    return ounces % 16;
    
}

+ (int) convertPoundsToOunces: (int) ounces {
    return ounces * 16;
    
}
#pragma mark - EditBabyPage methods End

#pragma mark - Custom pop up
+ (void) subscriptionPopUpWith:(UIView *_Nonnull) view
                      delegate:(id <BabyPagePopUpProtocol> _Nonnull) delegate{
    PlusFeaturePopUp *plusFeaturePopUp = [[[NSBundle mainBundle] loadNibNamed:@"PlusFeaturePopUp" owner:self options:nil] objectAtIndex:0];
    [plusFeaturePopUp setDelegate:delegate];
    plusFeaturePopUp.center = view.center;
    UIView *viewOverlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0,
                                                                   view.bounds.size.width,
                                                                   view.bounds.size.height)];
    [viewOverlay setBackgroundColor:[UIColor blackColor]];
    [viewOverlay setAlpha:0.5];
    [view addSubview:viewOverlay];
    [view addSubview:plusFeaturePopUp];
}

//widthAndHeight paramter is an array [width, height] 
+ (void) subscriptionPopUpWith:(UIView *_Nonnull) view
                      delegate:(id <BabyPagePopUpProtocol> _Nonnull) delegate
                         title:(NSString *_Nonnull) title
                       content:(NSString *_Nonnull) content
                   buttonTitle:(NSString *_Nonnull) buttonTitle
                widthAndHeight:(NSArray*)widthAndHeight{
    
    
    CGFloat height;
    
    PlusFeaturePopUp *plusFeaturePopUp = [[[NSBundle mainBundle] loadNibNamed:@"PlusFeaturePopUp" owner:self options:nil] objectAtIndex:0];
    [plusFeaturePopUp.titleLabel setText:title];
    if ([content isEqualToString:PLUS_WELCOME_MESSAGE]) {

        UIFont *currentFont = plusFeaturePopUp.contentLabel.font;
        UIFont *newFont = [UIFont fontWithName:[NSString stringWithFormat:@"%@-Bold", currentFont.fontName]
                                          size:currentFont.pointSize];
        [plusFeaturePopUp.contentLabel setFont: newFont];

    }
    [plusFeaturePopUp.contentLabel setText:content];
    if ([buttonTitle isEqualToString:NO_BUTTON]) {
        plusFeaturePopUp.acceptButton.hidden = YES;
        plusFeaturePopUp.buttonView.hidden = YES;
        height = (widthAndHeight.count>0) ? [[widthAndHeight objectAtIndex:1] floatValue] : 200;
    } else {
        [plusFeaturePopUp.acceptButton setTitle:buttonTitle forState:UIControlStateNormal];
        height = (widthAndHeight.count>0) ? [[widthAndHeight objectAtIndex:1] floatValue] : 300;
    }
    [plusFeaturePopUp setDelegate:delegate];
    plusFeaturePopUp.center = view.center;
    UIView *viewOverlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0,
                                                                   view.bounds.size.width,
                                                                   view.bounds.size.height)];
    [viewOverlay setBackgroundColor:[UIColor blackColor]];
    [viewOverlay setAlpha:0.5];
    [view addSubview:viewOverlay];
    [view addSubview:plusFeaturePopUp];

    [plusFeaturePopUp setTranslatesAutoresizingMaskIntoConstraints:NO];
    [viewOverlay setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    NSString *finalHeight = [NSString stringWithFormat:@"V:[plusFeaturePopUp(==%f)]", height];
    CGFloat width = plusFeaturePopUp.frame.size.width - ((widthAndHeight.count>0)?[[widthAndHeight objectAtIndex:0] floatValue] : 100);
    NSString *finalWidth = [NSString stringWithFormat:@"H:[plusFeaturePopUp(==%f)]", width];
    
    // center plusFeaturePopUp horizontally in view
    [view addConstraint:[NSLayoutConstraint constraintWithItem:plusFeaturePopUp attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];
    
    // center plusFeaturePopUp vertically in view
    [view addConstraint:[NSLayoutConstraint constraintWithItem:plusFeaturePopUp attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
    
    // width constraint
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:finalWidth options:0 metrics:nil views:NSDictionaryOfVariableBindings(plusFeaturePopUp)]];

    // height constraint
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:finalHeight options:0 metrics:nil views:NSDictionaryOfVariableBindings(plusFeaturePopUp)]];
    
    // align viewOverlay from the left and right
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[viewOverlay]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(viewOverlay)]];
    
    // align viewOverlay from the top and bottom
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[viewOverlay]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(viewOverlay)]];

    [plusFeaturePopUp layoutIfNeeded];
    [viewOverlay layoutIfNeeded];

    [view layoutIfNeeded];
}

+ (void) limitTextOverlay:(UIView *_Nonnull) view
                      delegate:(id <BabyPagePopUpProtocol> _Nonnull) delegate
                         title:(NSString *_Nonnull) title
                       content:(NSString *_Nonnull) content
                   buttonTitle:(NSString *_Nonnull) buttonTitle
                widthAndHeight:(NSArray*)widthAndHeight{
    
    
    CGFloat height;
    
    PlusFeaturePopUp *plusFeaturePopUp = [[[NSBundle mainBundle] loadNibNamed:@"PlusFeaturePopUp" owner:self options:nil] objectAtIndex:0];
    if(![buttonTitle isEqualToString:NO_BUTTON]){
        [plusFeaturePopUp setBackgroundColor:[UIColor redColor]];
        [plusFeaturePopUp.acceptButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    }
    [plusFeaturePopUp.titleLabel setText:title];
    if ([content isEqualToString:PLUS_WELCOME_MESSAGE]) {
        
        UIFont *currentFont = plusFeaturePopUp.contentLabel.font;
        UIFont *newFont = [UIFont fontWithName:[NSString stringWithFormat:@"%@-Bold", currentFont.fontName]
                                          size:currentFont.pointSize];
        [plusFeaturePopUp.contentLabel setFont: newFont];
        
    }
    [plusFeaturePopUp.contentLabel setText:content];
    if ([buttonTitle isEqualToString:NO_BUTTON]) {
        plusFeaturePopUp.acceptButton.hidden = YES;
        plusFeaturePopUp.buttonView.hidden = YES;
        height = (widthAndHeight.count>0) ? [[widthAndHeight objectAtIndex:1] floatValue] : 200;
    } else {
        [plusFeaturePopUp.acceptButton setTitle:buttonTitle forState:UIControlStateNormal];
        height = (widthAndHeight.count>0) ? [[widthAndHeight objectAtIndex:1] floatValue] : 300;
    }
    [plusFeaturePopUp setDelegate:delegate];
    plusFeaturePopUp.center = view.center;
    UIView *viewOverlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0,
                                                                   view.bounds.size.width,
                                                                   view.bounds.size.height)];
    [viewOverlay setBackgroundColor:[UIColor blackColor]];
    [viewOverlay setAlpha:0.5];
    [view addSubview:viewOverlay];
    [view addSubview:plusFeaturePopUp];
    
    [plusFeaturePopUp setTranslatesAutoresizingMaskIntoConstraints:NO];
    [viewOverlay setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    NSString *finalHeight = [NSString stringWithFormat:@"V:[plusFeaturePopUp(==%f)]", height];
    CGFloat width = plusFeaturePopUp.frame.size.width - ((widthAndHeight.count>0)?[[widthAndHeight objectAtIndex:0] floatValue] : 100);
    NSString *finalWidth = [NSString stringWithFormat:@"H:[plusFeaturePopUp(==%f)]", width];
    
    // center plusFeaturePopUp horizontally in view
    [view addConstraint:[NSLayoutConstraint constraintWithItem:plusFeaturePopUp attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];
    
    // center plusFeaturePopUp vertically in view
    [view addConstraint:[NSLayoutConstraint constraintWithItem:plusFeaturePopUp attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
    
    // width constraint
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:finalWidth options:0 metrics:nil views:NSDictionaryOfVariableBindings(plusFeaturePopUp)]];
    
    // height constraint
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:finalHeight options:0 metrics:nil views:NSDictionaryOfVariableBindings(plusFeaturePopUp)]];
    
    // align viewOverlay from the left and right
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[viewOverlay]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(viewOverlay)]];
    
    // align viewOverlay from the top and bottom
    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[viewOverlay]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(viewOverlay)]];
    
    [plusFeaturePopUp layoutIfNeeded];
    [viewOverlay layoutIfNeeded];
    
    [view layoutIfNeeded];
}


+ (void) closePopUp:(UIView *_Nonnull) view{
    NSInteger index = [view.subviews count] - 1;
    UIView *popupView = [view.subviews objectAtIndex:index];
    UIView *viewBackgraound = [view.subviews objectAtIndex:index - 1];
    [popupView removeFromSuperview];
    [viewBackgraound removeFromSuperview];
}

//+ (void) subscriptionInfoPopUp:(UIView * _Nonnull) view delegate:(id <BabyPagePopUpProtocol> _Nonnull) delegate widthAndHeight:(NSArray*)widthAndHeight {
//
//    CGFloat height;
//
//    static NSDictionary *inst = nil;
//
//    NSMutableArray * plans = [BabyPageUserDefaults getPlans];
//
//    for (NSDictionary * plan in plans) {
//
//        if ([[plan objectForKey:@"id"] isEqualToString:@"one year"]) {
//
//            NSNumber * price = [NSNumber numberWithInt:[[plan objectForKey:@"amount"] intValue]];
//
//            inst = @{
//                     @"subscriptionId": [NSNumber numberWithInt:ONE_YEAR_SUBSCRIPTION],
//                     @"title": [[plan objectForKey:@"name"] uppercaseString],
//                     @"price": [NSString stringWithFormat:@"%.2f", [price doubleValue]/100],
//                     @"period":[NSString stringWithFormat:@"per 1 %@", [plan objectForKey:@"interval"]],
//                     @"saveRate": @"SAVE 47%",
//                     @"description":@"along with all features above, you also get:",
//                     @"extraFeatures":@[@"1 large soft cover book per year", @"10% off all orders", @"Free shipping on all orders", @"The yearly option offers a big discount and great perks including a free book!"],
//                     @"buttonTitle" : @"ADD TO CART"
//                     };
//        }
//    }
//
//    SubscriptionView * plusFeaturePopUp = [[[NSBundle mainBundle] loadNibNamed:@"SubscriptionView" owner:self options:nil] objectAtIndex:0];
//    [plusFeaturePopUp initWithSubscription:inst];
//    [plusFeaturePopUp.addToCartButton setHidden:YES];
//    [plusFeaturePopUp.addToCartButtonHeight setConstant:0.0];
//    [plusFeaturePopUp.closeButton setHidden:NO];
//
//    height = (widthAndHeight.count>0) ? [[widthAndHeight objectAtIndex:1] floatValue] : 420;
//    [plusFeaturePopUp setDelegate:delegate];
//    plusFeaturePopUp.center = view.center;
//    UIView *viewOverlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0,
//                                                                   view.bounds.size.width,
//                                                                   view.bounds.size.height)];
//    [viewOverlay setBackgroundColor:[UIColor blackColor]];
//    [viewOverlay setAlpha:0.5];
//    [view addSubview:viewOverlay];
//    [view addSubview:plusFeaturePopUp];
//
//    [plusFeaturePopUp setTranslatesAutoresizingMaskIntoConstraints:NO];
//    [viewOverlay setTranslatesAutoresizingMaskIntoConstraints:NO];
//
//    NSString *finalHeight = [NSString stringWithFormat:@"V:[plusFeaturePopUp(==%f)]", height];
//    CGFloat width = plusFeaturePopUp.frame.size.width - ((widthAndHeight.count > 0) ? [[widthAndHeight objectAtIndex:0] floatValue] : 50);
//    NSString *finalWidth = [NSString stringWithFormat:@"H:[plusFeaturePopUp(==%f)]", width];
//
//    // center plusFeaturePopUp horizontally in view
//    [view addConstraint:[NSLayoutConstraint constraintWithItem:plusFeaturePopUp attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];
//
//    // center plusFeaturePopUp vertically in view
//    [view addConstraint:[NSLayoutConstraint constraintWithItem:plusFeaturePopUp attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
//
//    // width constraint
//    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:finalWidth options:0 metrics:nil views:NSDictionaryOfVariableBindings(plusFeaturePopUp)]];
//
//    // height constraint
//    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:finalHeight options:0 metrics:nil views:NSDictionaryOfVariableBindings(plusFeaturePopUp)]];
//
//    // align viewOverlay from the left and right
//    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[viewOverlay]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(viewOverlay)]];
//
//    // align viewOverlay from the top and bottom
//    [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[viewOverlay]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(viewOverlay)]];
//
//    [plusFeaturePopUp layoutIfNeeded];
//    [viewOverlay layoutIfNeeded];
//
//    [view layoutIfNeeded];
//}

+ (NSDictionary*_Nullable)parametersToAddToCart{
    NSDictionary *parameters = @{ @"id":@"monthly",
                                        @"object":@"plan",
                                        @"type":@"subscription"};
    return parameters;
}

+ (SKProduct * _Nullable) findProductBy: (NSString * _Nonnull) identifier {
    for (int i = 0; i < [[IAPShare sharedHelper].iap.products count]; i++) {
        SKProduct* product =[[IAPShare sharedHelper].iap.products objectAtIndex:i];
        NSString* productId = product.productIdentifier;
        if ([productId isEqualToString:identifier]) {
            return product;
        }
    }
    return nil;
}

@end
