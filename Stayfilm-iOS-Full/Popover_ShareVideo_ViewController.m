//
//  Popover_ShareVideo_ViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Henrique on 2/7/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import "Popover_ShareVideo_ViewController.h"
#import <FBSDKMessengerShareKit/FBSDKMessengerShareKit.h>
#import <Google/Analytics.h>
#import "StayfilmApp.h"
#import "InstagramActivity.h"
#import "DownloadActivity.h"
#import "Reachability.h"
#import "Job.h"
#import "Utilities.h"
#import "AFNetworking.h"
#import "Constants.h"
@interface Popover_ShareVideo_ViewController ()<ProgressBarViewControllerDelegate>

@property (nonatomic, strong) NSMutableArray *socialButtons;
@property (nonatomic, strong) NSMutableArray *moreButtons;

@property (nonatomic, weak) StayfilmApp *sfApp;
@property (nonatomic, strong) Downloader *downloaderStayfilm;
@property (nonatomic, strong) NSString *movieFile;
@property (nonatomic, strong) NSString *movieFileInPhotos;
@property (nonatomic, strong) NSString *movieFileInPhotosID;
@property (nonatomic, assign) BOOL downloading;

@property (nonatomic, assign) BOOL whatsappSetToShare;
@property (nonatomic, assign) BOOL instagramSetToShare;
@property (nonatomic, assign) BOOL messengerSetToShare;
@property (nonatomic, assign) int socialLoginAttempts;
@property (nonatomic, assign) int facebookloginAttempts;

@property (nonatomic, assign) BOOL isSharingClicked;
@property (nonatomic, assign) BOOL isDownloaded;

@property (nonatomic, strong) AFURLSessionManager *manager;
@property (nonatomic, strong) NSURLSessionDownloadTask *downloadTask;
@property (nonatomic, assign) NSURL* downloadVideopath;

@property (nonatomic, assign) BOOL downloadIsSelected;
@end

@implementation Popover_ShareVideo_ViewController

@synthesize but_done, but_help, collectionSocial, collectionMore, switchShowGallery, blur_view, view_Download, progress_Download, txt_Downloading, txt_DownloadProgress, movie, movieFile, movieFileInPhotos, spinner_Loading, isModal;

static NSString *cellIdentifier1 = @"ShareButtonCell1";
static NSString *cellIdentifier2 = @"ShareButtonCell2";


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.collectionSocial registerNib:[UINib nibWithNibName:@"ShareButton_CollectionViewCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:cellIdentifier1];
    [self.collectionMore registerNib:[UINib nibWithNibName:@"ShareButton_CollectionViewCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:cellIdentifier2];
//    [self.collectionSocial registerClass:[ShareButton_CollectionViewCell class] forCellWithReuseIdentifier:cellIdentifier1];
//    [self.collectionMore registerClass:[ShareButton_CollectionViewCell class] forCellWithReuseIdentifier:cellIdentifier2];
    self.collectionSocial.delegate = self;
    self.collectionSocial.dataSource = self;
    self.collectionMore.delegate = self;
    self.collectionMore.dataSource = self;
    
    self.isSharingClicked = NO;
    self.isDownloaded = NO;
    self.instagramSetToShare = NO;
    self.whatsappSetToShare = NO;
    self.downloading = NO;
    
    self.socialButtons = [[NSMutableArray alloc] init];
    self.moreButtons = [[NSMutableArray alloc] init];
        
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if(self.isModal){
        [self.downloadSwitch setOn:YES];
        [self.downloadSwitch setHidden:NO];
        [self.downloadLabel setHidden:NO];
        [self.switchConstraint setConstant:38];
        
        
    }else{
        self.downloadIsSelected = NO;
        [self.downloadSwitch setOn:NO];
        [self.downloadSwitch setHidden:YES];
        [self.downloadLabel setHidden:YES];
        [self.switchConstraint setConstant:-30];
    }
    
    if(self.movie.permission == MoviePermission_PUBLIC) {
        [self.switchShowGallery setOn:YES];
    }
    
    self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    
    BOOL isInstalled = NO;
    //Facebook
    isInstalled = [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"fb://"]];
    if (isInstalled) {
    //if ([FBSDKAccessToken currentAccessToken]) {
        NSMutableDictionary *facebook = [[NSMutableDictionary alloc] init];
        [facebook setObject:@"SHARE_Facebook" forKey:@"image"];
        [facebook setObject:NSLocalizedString(@"FACEBOOK", nil) forKey:@"title"];
        [self.socialButtons addObject:facebook];
    }
    
    //Instagram
    isInstalled = [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"instagram://"]];
    if (isInstalled) {
        NSMutableDictionary *instagram = [[NSMutableDictionary alloc] init];
        [instagram setObject:@"SHARE_Instagram" forKey:@"image"];
        [instagram setObject:NSLocalizedString(@"INSTAGRAM", nil) forKey:@"title"];
        [self.socialButtons addObject:instagram];
    }
    
    //WhatsApp
    isInstalled = [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"whatsapp://"]];
    if (isInstalled) {
        NSMutableDictionary *whatsapp = [[NSMutableDictionary alloc] init];
        [whatsapp setObject:@"SHARE_WhatsApp" forKey:@"image"];
        [whatsapp setObject:NSLocalizedString(@"WHATSAPP", nil) forKey:@"title"];
        [self.socialButtons addObject:whatsapp];
    }
    
    //Messenger
    isInstalled = [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"fb-messenger://"]];
    if (isInstalled) {
        NSMutableDictionary *messenger = [[NSMutableDictionary alloc] init];
        [messenger setObject:@"SHARE_Messenger" forKey:@"image"];
        [messenger setObject:NSLocalizedString(@"MESSENGER", nil) forKey:@"title"];
        [self.socialButtons addObject:messenger];
    }
    
//    //Twitter
//    isInstalled = [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"twitter://"]];
//    if (isInstalled) {
//        NSMutableDictionary *twitter = [[NSMutableDictionary alloc] init];
//        [twitter setObject:@"SHARE_Twitter" forKey:@"image"];
//        [twitter setObject:NSLocalizedString(@"TWITTER", nil) forKey:@"title"];
//        [self.socialButtons addObject:twitter];
//    }
    
    if(self.movie != nil && [self fileExists:self.movie.idMovie]){
        self.isDownloaded = YES;
    }
    //Now for the More Buttons
    if (self.moreButtons.count >0){
        [self.moreButtons removeAllObjects];
    }
    
    NSMutableDictionary *download = [[NSMutableDictionary alloc] init];
    if(self.isDownloaded) {
        [download setObject:NSLocalizedString(@"DOWNLOADED", nil) forKey:@"title"];
        [download setObject:@"SHARE_Downloaded" forKey:@"image"];
    } else {
        [download setObject:NSLocalizedString(@"DOWNLOAD", nil) forKey:@"title"];
        [download setObject:@"SHARE_Download" forKey:@"image"];
    }
    [self.moreButtons addObject:download];
    
    NSMutableDictionary *copyLink = [[NSMutableDictionary alloc] init];
    [copyLink setObject:@"SHARE_CopyLink" forKey:@"image"];
    [copyLink setObject:NSLocalizedString(@"COPY_LINK", nil) forKey:@"title"];
    [self.moreButtons addObject:copyLink];
    
    NSMutableDictionary *more = [[NSMutableDictionary alloc] init];
    [more setObject:@"SHARE_More" forKey:@"image"];
    [more setObject:NSLocalizedString(@"MORE", nil) forKey:@"title"];
    [self.moreButtons addObject:more];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if(self.isModal) {
        //Google Analytics
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:@"share-screen"];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    } else {
        //Google Analytics
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:@"profile_share-screen"];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    }
    
    self.transitionAnimation = [[PopupMenuAnimation alloc] init];
}

-(id)initWithDelegate:(id<Popover_ShareVideo_ViewControllerDelegate>)p_delegate
{
    if(self = [super init]) {
        self.delegate = p_delegate;
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)connected {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

-(void)backgroundTapped:(UIGestureRecognizer *)gesture
{
    if(self.downloading) {
        runOnMainQueueWithoutDeadlocking(^{
            UIAlertController *alertc = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"BACK_TAP_CANCEL_DOWNLOAD", nil) message:NSLocalizedString(@"BACK_TAP_CANCEL_DOWNLOAD_MESSAGE", nil) preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"NO", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                [alertc dismissViewControllerAnimated:YES completion:nil];
            }];
            UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"CANCEL", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                if(self.downloaderStayfilm != nil) {
                    [self.downloaderStayfilm resetDownload];
                }
                [self.navigationController dismissViewControllerAnimated:YES completion:nil];
            }];
            [alertc addAction:defaultAction];
            [alertc addAction:cancelAction];
            [self presentViewController:alertc animated:YES completion:nil];
        });
        return;
    }
    //    NSLog(@"popover %@",self.navigationController.viewControllers);
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - Interface changes

-(void)hideDownloadProcessWithoutAnimation {
    runOnMainQueueWithoutDeadlocking(^{
        self.view_Download.alpha = 0.0;
        [self.view_Download setHidden:YES];

        [self.but_done setTitle:NSLocalizedString(@"IM_DONE", nil) forState:UIControlStateNormal];
        [self.but_done setTitle:NSLocalizedString(@"IM_DONE", nil) forState:UIControlStateSelected];
        [self.but_done setImage:[UIImage imageNamed:@"Icon_xCheck"] forState:UIControlStateNormal];
        [self.but_done setImage:[UIImage imageNamed:@"Icon_xCheck"] forState:UIControlStateSelected];
    });
}

-(void)hideDownloadProgressSettingDownloadedButton {
    runOnMainQueueWithoutDeadlocking(^{
        self.isDownloaded = YES;
        [self.collectionMore reloadData];
        [UIView animateWithDuration:0.3 animations:^{
            self.view_Download.alpha = 0.0;
        } completion:^(BOOL finished) {
            [self.view_Download setHidden:YES];

            [self.but_done setTitle:NSLocalizedString(@"IM_DONE", nil) forState:UIControlStateNormal];
            [self.but_done setTitle:NSLocalizedString(@"IM_DONE", nil) forState:UIControlStateSelected];
            [self.but_done setImage:[UIImage imageNamed:@"Icon_xCheck"] forState:UIControlStateNormal];
            [self.but_done setImage:[UIImage imageNamed:@"Icon_xCheck"] forState:UIControlStateSelected];
//            [UIView animateWithDuration:0.3 animations:^{
//                self.view_Buttons.alpha = 1.0;
//            } completion:^(BOOL finished) {
//
//                [self.but_download setTitle:NSLocalizedString(@"DOWNLOADED", nil) forState:UIControlStateNormal];
//                [self.but_download setTitle:NSLocalizedString(@"DOWNLOADED", nil) forState:UIControlStateSelected];
//                [self.but_download setBackgroundImage:[UIImage imageNamed:@"SHARE_Downloaded"] forState:UIControlStateNormal];
//                [self.but_download setBackgroundImage:[UIImage imageNamed:@"SHARE_Downloaded"] forState:UIControlStateSelected];
//            }];
            self.txt_DownloadProgress.text = @"0%";
            self.txt_Downloading.text = NSLocalizedString(@"DOWNLOADING", nil);
//            [self.view_Download layoutIfNeeded];
//            [self.txt_Downloading sizeToFit];
            self.progress_Download.progressTintColor = [UIColor colorWithRed:99.0f/255.0f green:192.0f/255.0f blue:152.0f/255.0f alpha:1.0]; // green
            //                self.progress_Download.progressTintColor = [UIColor colorWithRed:221.0 green:219.0 blue:222.0 alpha:1.0]; // grey
            self.progress_Download.progress = 0.0;
            self.isSharingClicked = NO;
        }];
    });
}

-(void)showAlreadyDownloaded {
    runOnMainQueueWithoutDeadlocking(^{
     /*  self.view_Download.alpha = 0.0;
        [self.view_Download setHidden:NO];
        self.txt_Downloading.text = NSLocalizedString(@"ALREADY_DOWNLOADED", nil);
//        [self.view_Download layoutIfNeeded];
//        [self.txt_Downloading sizeToFit];
        self.txt_DownloadProgress.alpha = 1.0;
        self.txt_DownloadProgress.text = @"100%";
        self.progress_Download.progressTintColor = [UIColor colorWithRed:99.0f/255.0f green:192.0f/255.0f blue:152.0f/255.0f alpha:1.0]; // green
        //self.progress_Download.progressTintColor = [UIColor colorWithRed:221.0 green:219.0 blue:222.0 alpha:1.0]; // grey
        self.progress_Download.progress = 1.0;
        self.but_done.alpha = 0.0;
        [self.but_done setTitle:NSLocalizedString(@"OK", nil) forState:UIControlStateNormal];
        [self.but_done setTitle:NSLocalizedString(@"OK", nil) forState:UIControlStateSelected];
        [self.but_done setImage:[UIImage imageNamed:@"Icon_xCheck"] forState:UIControlStateNormal];
        [self.but_done setImage:[UIImage imageNamed:@"Icon_xCheck"] forState:UIControlStateSelected];*/
        self.isDownloaded = YES;
        [self.collectionMore reloadData];
//            [self.but_download setTitle:NSLocalizedString(@"DOWNLOADED", nil) forState:UIControlStateNormal];
//            [self.but_download setTitle:NSLocalizedString(@"DOWNLOADED", nil) forState:UIControlStateSelected];
//            [self.but_download setBackgroundImage:[UIImage imageNamed:@"SHARE_Downloaded"] forState:UIControlStateNormal];
//            [self.but_download setBackgroundImage:[UIImage imageNamed:@"SHARE_Downloaded"] forState:UIControlStateSelected];
//        [UIView animateWithDuration:0.3 animations:^{
//            self.view_Download.alpha = 1.0;
//            self.but_done.alpha = 1.0;
//        }];
        self.isSharingClicked = NO;
    });
}

-(void)showDownloadFailed {
    runOnMainQueueWithoutDeadlocking(^{
        self.txt_Downloading.alpha = 0.0;
        self.txt_Downloading.text =  NSLocalizedString(@"DOWNLOAD_FAILED", nil);
//        [self.view_Download layoutIfNeeded];
//        [self.txt_Downloading sizeToFit];
        self.progress_Download.alpha = 0.0;
        self.progress_Download.progressTintColor = [UIColor colorWithRed:217.0f/255.0f green:77.0f/255.0f blue:79.0f/255.0f alpha:1.0]; // red
        self.progress_Download.progress = 1.0;
        [UIView animateWithDuration:0.3 animations:^{
            self.txt_Downloading.alpha = 1.0;
            self.progress_Download.alpha = 1.0;
            self.txt_DownloadProgress.alpha = 0.0;
        } completion:^(BOOL finished) {
            self.isSharingClicked = NO;
        }];
    });
}

-(void)showDownloadProgress {
    runOnMainQueueWithoutDeadlocking(^{
        self.view_Download.alpha = 0.0;
        [self.view_Download setHidden:NO];
        self.txt_Downloading.text = NSLocalizedString(@"DOWNLOADING", nil);
//        [self.view_Download layoutIfNeeded];
//        [self.txt_Downloading sizeToFit];
        self.txt_DownloadProgress.alpha = 1.0;
        self.txt_DownloadProgress.text = @"0%";
        self.progress_Download.progressTintColor = [UIColor colorWithRed:99.0f/255.0f green:192.0f/255.0f blue:152.0f/255.0f alpha:1.0]; // green
        //self.progress_Download.progressTintColor = [UIColor colorWithRed:221.0 green:219.0 blue:222.0 alpha:1.0]; // grey
        self.progress_Download.progress = 0.0;
        self.but_done.alpha = 0.0;
        [self.but_done setTitle:NSLocalizedString(@"CANCEL", nil) forState:UIControlStateNormal];
        [self.but_done setTitle:NSLocalizedString(@"CANCEL", nil) forState:UIControlStateSelected];
        [self.but_done setImage:[UIImage imageNamed:@"Icon_xCancel"] forState:UIControlStateNormal];
        [self.but_done setImage:[UIImage imageNamed:@"Icon_xCancel"] forState:UIControlStateSelected];
        [UIView animateWithDuration:0.3 animations:^{
            self.view_Download.alpha = 1.0;
            self.but_done.alpha = 1.0;
        }];
        self.isSharingClicked = NO;
    });
}

-(void)hideDownloadProgress {
    runOnMainQueueWithoutDeadlocking(^{
        [UIView animateWithDuration:0.3 animations:^{
            self.view_Download.alpha = 0.0;
        } completion:^(BOOL finished) {
            [self.view_Download setHidden:YES];
            [self.but_done setTitle:NSLocalizedString(@"IM_DONE", nil) forState:UIControlStateNormal];
            [self.but_done setTitle:NSLocalizedString(@"IM_DONE", nil) forState:UIControlStateSelected];
            [self.but_done setImage:[UIImage imageNamed:@"Icon_xCheck"] forState:UIControlStateNormal];
            [self.but_done setImage:[UIImage imageNamed:@"Icon_xCheck"] forState:UIControlStateSelected];

            self.txt_DownloadProgress.text = @"0%";
            self.txt_Downloading.text = NSLocalizedString(@"DOWNLOADING", nil);
//            [self.view_Download layoutIfNeeded];
//            [self.txt_Downloading sizeToFit];
            self.progress_Download.progressTintColor = [UIColor colorWithRed:99.0f/255.0f green:192.0f/255.0f blue:152.0f/255.0f alpha:1.0]; // green
            //                self.progress_Download.progressTintColor = [UIColor colorWithRed:221.0 green:219.0 blue:222.0 alpha:1.0]; // grey
            self.progress_Download.progress = 0.0;
            self.isSharingClicked = NO;
        }];
    });
}


#pragma mark - Facebook Share Delegate

- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results
{
    NSLog(@"StayLog: Successfully shared in Facebook");
    self.isSharingClicked = NO;
    
    if(self.isModal){
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"facebook-share"
                                                              action:@"ok"
                                                               label:@""
                                                               value:@1] build]];
    } else {
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"share_film-in-profile"
                                                              action:@"facebook-share"
                                                               label:@"ok"
                                                               value:@1] build]];
    }
}

- (void)sharerDidCancel:(id<FBSDKSharing>)sharer
{
    NSLog(@"StayLog: Facebook share canceled!");
    self.isSharingClicked = NO;
    
    if(self.isModal){
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"facebook-share"
                                                              action:@"cancel"
                                                               label:@""
                                                               value:@1] build]];
    } else {
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"share_film-in-profile"
                                                              action:@"facebook-share"
                                                               label:@"cancel"
                                                               value:@1] build]];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController *alertc = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"FACEBOOK_LOGGEDOUT", nil)
                                                                        message:NSLocalizedString(@"FACEBOOK_LOGGEDOUT_MESSAGE", nil)
                                                                 preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {
                                                                  [alertc dismissViewControllerAnimated:YES completion:nil];
                                                              }];
        [alertc addAction:defaultAction];
        [self presentViewController:alertc animated:YES completion:nil];
    });
}

- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error
{
    NSLog(@"StayLog: Facebook sharing error:%@", error.localizedDescription);
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController *alertc = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"FACEBOOK_ERROR", nil)
                                                                        message:NSLocalizedString(@"FACEBOOK_ERROR_MESSAGE", nil)
                                                                 preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {
                                                                  [alertc dismissViewControllerAnimated:YES completion:nil];
                                                              }];
        [alertc addAction:defaultAction];
        [self presentViewController:alertc animated:YES completion:nil];
    });
    
    self.isSharingClicked = NO;
    
    if(self.isModal){
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"facebook-share"
                                                              action:@"fail"
                                                               label:@""
                                                               value:@1] build]];
       
    } else {
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"share_film-in-profile"
                                                              action:@"facebook-share"
                                                               label:@"fail"
                                                               value:@1] build]];
    }
}


#pragma mark - Login to Social Operation Delegates

-(void)loginFacebookDidFinish:(LoginFacebookOperation *)loginOP {
    
    switch (loginOP.messageCode) {
        case LOGIN_SUCCESSFUL:
        {
            self.facebookloginAttempts = 0;
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                //Background Thread
                [self.sfApp.currentUser setUserFacebookToken:self.sfApp.settings[@"facebookToken"] andSFAppSingleton:self.sfApp];
            });
            [self shareToFacebook];
//            self.isSharingClicked = NO;
        }
            break;
        case DIFFERENT_USER:
        {
            runOnMainQueueWithoutDeadlocking(^{
                dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                    //save facebook token to this user
                    BOOL success = [self.sfApp.currentUser setUserFacebookToken:self.sfApp.settings[@"facebookToken"] andSFAppSingleton:self.sfApp];
                    
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        //Run UI Updates
                        if(!success) {
                            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"FACEBOOK_FAILED_TOKEN", nil)
                                                                                           message:NSLocalizedString(@"FACEBOOK_FAILED_TOKEN_MESSAGE", nil)
                                                                                    preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                                  handler:^(UIAlertAction * action) {}];
                            
                            [alert addAction:defaultAction];
                            [self presentViewController:alert animated:YES completion:nil];
                            self.isSharingClicked = NO;
                            return;
                        }
                        self.facebookloginAttempts = 0;
                        [self shareToFacebook];
                    });
                });
            });
        }
            break;
            
        default:
        {
            ++self.facebookloginAttempts;
        }
            break;
    }
}

-(void)loginFacebookDidFail:(LoginFacebookOperation *)loginOP {
    
    ++self.facebookloginAttempts;
    if(self.facebookloginAttempts >= 3)
    {
        runOnMainQueueWithoutDeadlocking(^{
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"FACEBOOK_FALIED", nil)
                                                                           message:NSLocalizedString(@"FACEBOOK_FALIED_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
            self.facebookloginAttempts = 0;
        });
        self.isSharingClicked = NO;
        [self.spinner_Loading stopAnimating];
        return;
    }
    
    BOOL retryLogin = NO;
    switch (loginOP.messageCode) {
        case CONNECTION_ERROR:
        {
            retryLogin = YES;
            runOnMainQueueWithoutDeadlocking(^{
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CONNECTION_ERROR", nil)
                                                                               message:NSLocalizedString(@"CONNECTION_ERROR_MESSAGE", nil)
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {}];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
            });
            self.isSharingClicked = NO;
            [self.spinner_Loading stopAnimating];
        }
            break;
        case USER_CANCELLED:
        {
            retryLogin = NO;
            runOnMainQueueWithoutDeadlocking(^{
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"USER_CANCELLED", nil)
                                                                               message:NSLocalizedString(@"USER_CANCELLED_MESSAGE", nil)
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {}];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
            });
            self.isSharingClicked = NO;
            [self.spinner_Loading stopAnimating];
        }
            break;
        case LOGIN_FAILED:
        {
            retryLogin = YES;
            runOnMainQueueWithoutDeadlocking(^{
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"LOGIN_FAILED", nil)
                                                                               message:NSLocalizedString(@"LOGIN_FAILED_MESSAGE", nil)
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {}];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
            });
            self.isSharingClicked = NO;
            [self.spinner_Loading stopAnimating];
        }
            break;
        case REGISTRATION_ERROR:
        {
            retryLogin = YES;
            runOnMainQueueWithoutDeadlocking(^{
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"REGISTRATION_ERROR", nil)
                                                                               message:NSLocalizedString(@"REGISTRATION_ERROR_MESSAGE", nil)
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {}];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
            });
            self.isSharingClicked = NO;
            [self.spinner_Loading stopAnimating];
        }
            break;
        case NO_CONFIG_LOADED:
        {
            retryLogin = YES;
            self.facebookloginAttempts--;
            // say nothing, just try again
            if(self.sfApp == nil)
                self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
            
            self.sfApp.sfConfig = [SFConfig getAllConfigsFromWS:self.sfApp.wsConfig];
            break;
        }
        case NO_INTERNET_CONNECTION:
        {
            retryLogin = NO;
            self.facebookloginAttempts--;
            runOnMainQueueWithoutDeadlocking(^{
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"NO_INTERNET_CONNECTION", nil)
                                                                               message:NSLocalizedString(@"NO_INTERNET_CONNECTION_MESSAGE", nil)
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {}];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
            });
            self.isSharingClicked = NO;
            
            [NSTimer scheduledTimerWithTimeInterval:2.0 repeats:YES block:^(NSTimer * _Nonnull timer) {
                if([self connected])
                {
                    [timer invalidate];
                    timer = nil;
                    LoginFacebookOperation *loginOp = [[LoginFacebookOperation alloc] initWithDelegate:self andSFApp:self.sfApp];
                    [self.sfApp.operationsQueue addOperation:loginOp];
                }
            }];
            break;
        }
        case -1:
        {
            retryLogin = YES;
            NSLog(@"StayLog: loginDidFail with EXCEPTION (messageCode -1)");
            runOnMainQueueWithoutDeadlocking(^{
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"LOGIN_FAILED", nil)
                                                                               message:NSLocalizedString(@"LOGIN_FAILED_MESSAGE", nil)
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {}];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
            });
            self.isSharingClicked = NO;
            [self.spinner_Loading stopAnimating];
        }
            break;
        default:
        {
            retryLogin = YES;
        }
            break;
    }
    
    if(retryLogin)
    {
        LoginFacebookOperation *logOperation = [[LoginFacebookOperation alloc] initWithDelegate:self andSFApp:self.sfApp];
        [self.sfApp.operationsQueue addOperation:logOperation];
    }
    else
    {
        self.isSharingClicked = NO;
        [self.spinner_Loading stopAnimating];
    }
    
    [loginOP cancel];
}

- (void)loginToSocialNetworkDidSucceed:(LoginToSocialNetworkOperation *)loginOP; {
    switch (loginOP.messageCode) {
        case LOGIN_SUCCESSFUL:
        {
            self.socialLoginAttempts = 0;
            if(loginOP.socialNetwork == SOCIAL_INSTAGRAM) {
                [self.delegate decidedOption:@"instagram"];
            }
            else if(loginOP.socialNetwork == SOCIAL_TWITTER) {
                [self.delegate decidedOption:@"twitter"];
            }
        }
            break;
            
        default:
        {
            ++self.socialLoginAttempts;
        }
            break;
    }
}

- (void)loginToSocialNetworkDidFail:(LoginToSocialNetworkOperation *)loginOP {
    ++self.socialLoginAttempts;
    if(self.socialLoginAttempts >= 3)
    {
        runOnMainQueueWithoutDeadlocking(^{
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"SOCIAL_FALIED", nil)
                                                                           message:NSLocalizedString(@"SOCIAL_FALIED_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                    style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        });
        
        self.socialLoginAttempts = 0;
        self.isSharingClicked = NO;
        [self.spinner_Loading stopAnimating];
        return;
    }
    
    BOOL retryLogin = NO;
    switch (loginOP.messageCode) {
        case CONNECTION_ERROR:
        {
            retryLogin = YES;
            runOnMainQueueWithoutDeadlocking(^{
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CONNECTION_ERROR", nil)
                                                                               message:NSLocalizedString(@"CONNECTION_ERROR_MESSAGE", nil)
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                        style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {}];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
            });
        }
            break;
        case USER_CANCELLED:
        {
            retryLogin = NO;
            runOnMainQueueWithoutDeadlocking(^{
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"USER_CANCELLED", nil)
                                                                               message:NSLocalizedString(@"USER_CANCELLED_MESSAGE", nil)
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                        style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {}];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
            });
            
        }
            break;
        case LOGIN_FAILED:
        {
            retryLogin = YES;
            runOnMainQueueWithoutDeadlocking(^{
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"LOGIN_FAILED", nil) message:NSLocalizedString(@"LOGIN_FAILED_MESSAGE", nil) preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                        style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {}];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
            });
        }
            break;
        case REGISTRATION_ERROR:
        {
            retryLogin = YES;
            runOnMainQueueWithoutDeadlocking(^{
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"REGISTRATION_ERROR", nil) message:NSLocalizedString(@"REGISTRATION_ERROR_MESSAGE", nil) preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                        style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {}];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
            });
        }
            break;
        case NO_CONFIG_LOADED:
        {
            retryLogin = YES;
            self.socialLoginAttempts--;
            // say nothing, just try again
            if(self.sfApp == nil)
                self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
            
            self.sfApp.sfConfig = [SFConfig getAllConfigsFromWS:self.sfApp.wsConfig];
            break;
        }
        case NO_INTERNET_CONNECTION:
        {
            retryLogin = NO;
            self.socialLoginAttempts--;
            runOnMainQueueWithoutDeadlocking(^{
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"NO_INTERNET_CONNECTION", nil) message:NSLocalizedString(@"NO_INTERNET_CONNECTION_MESSAGE", nil) preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                        style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {}];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
            });
            
            if(loginOP.socialNetwork == SOCIAL_INSTAGRAM) {
                [NSTimer scheduledTimerWithTimeInterval:2.0 repeats:YES block:^(NSTimer * _Nonnull timer) {
                    if([self connected])
                    {
                        [timer invalidate];
                        timer = nil;
                        LoginToSocialNetworkOperation *loginOp = [[LoginToSocialNetworkOperation alloc] initWithDelegate:self andSFApp:self.sfApp forSocialNetwork:SOCIAL_INSTAGRAM];
                        [self.sfApp.operationsQueue addOperation:loginOp];
                    }
                }];
            } else if (loginOP.socialNetwork == SOCIAL_GOOGLE) {
                [NSTimer scheduledTimerWithTimeInterval:2.0 repeats:YES block:^(NSTimer * _Nonnull timer) {
                    if([self connected])
                    {
                        [timer invalidate];
                        timer = nil;
                        LoginToSocialNetworkOperation *loginOp = [[LoginToSocialNetworkOperation alloc] initWithDelegate:self andSFApp:self.sfApp forSocialNetwork:SOCIAL_GOOGLE];
                        [self.sfApp.operationsQueue addOperation:loginOp];
                    }
                }];
            }
            break;
        }
        case -1:
        {
            retryLogin = YES;
            NSLog(@"StayLog: loginDidFail with EXCEPTION (messageCode -1)");
            runOnMainQueueWithoutDeadlocking(^{
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"LOGIN_FAILED", nil) message:NSLocalizedString(@"LOGIN_FAILED_MESSAGE", nil) preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                        style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {}];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
            });
        }
            break;
        default:
        {
            retryLogin = YES;
        }
            break;
    }
    
    if(retryLogin)
    {
        LoginToSocialNetworkOperation *logOperation = [[LoginToSocialNetworkOperation alloc] initWithDelegate:self andSFApp:self.sfApp forSocialNetwork:loginOP.socialNetwork];
        [self.sfApp.operationsQueue addOperation:logOperation];
    }
    else
    {
        self.isSharingClicked = NO;
        [self.spinner_Loading stopAnimating];
        
        switch (loginOP.socialNetwork) {
            case SOCIAL_INSTAGRAM:
            {
                // do something ? maybe show a friendly message or suggestion?
            }
                break;
            case SOCIAL_GOOGLE:
            {
                //                    SocialAlbumsView *google = (SocialAlbumsView *)[self.view viewWithTag:3];
                //                    google.albums = self.albunsStayfilm;
                //                    google.isExpanded = NO;
                //                    google.isLoaded = NO;
            }
                break;
            case SOCIAL_TWITTER:
            {
                
            }
                break;
            default:
                break;
        }
    }
    
    [loginOP cancel];
}

-(void)loginSocialWebviewDidSucceed:(NSArray *)p_token {
    @try {
        if(self.sfApp == nil)
            self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
        
        if ([[p_token objectAtIndex:0] isEqualToString:@"instagram"])
        {
            BOOL success = [self.sfApp.currentUser setUserInstagramToken:[p_token objectAtIndex:1] andSFAppSingleton:self.sfApp];
            if(success)
            {
                [Job postSocialNetwork:@"instagram"];
                self.socialLoginAttempts = 0;
                
                self.instagramSetToShare = YES;
                if(UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(self.movieFile)) {
//                    UISaveVideoAtPathToSavedPhotosAlbum(self.movieFile,self,@selector(video:didFinishSavingWithError:contextInfo:), nil);
                }
            }
            else
            {
                if(self.socialLoginAttempts < 3) {
                    ++self.socialLoginAttempts;
                    LoginToSocialNetworkOperation *loginOp = [[LoginToSocialNetworkOperation alloc] initWithDelegate:self andSFApp:self.sfApp forSocialNetwork:SOCIAL_INSTAGRAM];
                    [self.sfApp.operationsQueue addOperation:loginOp];
                }
                else
                {
                    self.socialLoginAttempts = 0;
                    runOnMainQueueWithoutDeadlocking(^{
                        [self.spinner_Loading stopAnimating];
                        runOnMainQueueWithoutDeadlocking(^{
                            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"LOGIN_FAILED", nil)
                                                                                           message:NSLocalizedString(@"LOGIN_FAILED_MESSAGE", nil)
                                                                                    preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                                    style:UIAlertActionStyleDefault
                                                                                  handler:^(UIAlertAction * action) {}];
                            
                            [alert addAction:defaultAction];
                            [self presentViewController:alert animated:YES completion:nil];
                        });
                    });
                    self.isSharingClicked = NO;
                    return;
                }
            }
        }
        else if ([[p_token objectAtIndex:0] isEqualToString:@"twitter"])
        {
            
            NSString  *tokenVerifier = [[p_token objectAtIndex:1] stringByAppendingString:@":"];
            tokenVerifier = [tokenVerifier stringByAppendingString:[p_token objectAtIndex:2]];
            BOOL success = [self.sfApp.currentUser setUserTwitterToken:tokenVerifier  andSFAppSingleton:self.sfApp];
            if(success)
            {
                self.socialLoginAttempts = 0;
                BOOL successShare = [self.movie shareMovieOnTwitterWithMessage:nil];
                if(successShare) {
                    runOnMainQueueWithoutDeadlocking(^{
                        [self.spinner_Loading stopAnimating];
                        runOnMainQueueWithoutDeadlocking(^{
                            UIAlertController *alertc = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"TWITTER_SHARED", nil)
                                                                                            message:NSLocalizedString(@"TWITTER_SHARED_MESSAGE", nil)
                                                                                     preferredStyle:UIAlertControllerStyleAlert];
                            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                                    style:UIAlertActionStyleDefault
                                                                                  handler:^(UIAlertAction * action) {
                                                                                      [alertc dismissViewControllerAnimated:YES completion:nil];
                                                                                  }];
                            [alertc addAction:defaultAction];
                            [self presentViewController:alertc animated:YES completion:nil];
                        });
                    });
                    self.isSharingClicked = NO;
                    return;
                }
                else {
                    runOnMainQueueWithoutDeadlocking(^{
                        [self.spinner_Loading stopAnimating];
                        runOnMainQueueWithoutDeadlocking(^{
                            UIAlertController *alertc = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"TWITTER_ERROR", nil)
                                                                                            message:NSLocalizedString(@"TWITTER_ERROR_MESSAGE", nil)
                                                                                     preferredStyle:UIAlertControllerStyleAlert];
                            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                                    style:UIAlertActionStyleDefault
                                                                                  handler:^(UIAlertAction * action) {
                                                                                      [alertc dismissViewControllerAnimated:YES completion:nil];
                                                                                  }];
                            [alertc addAction:defaultAction];
                            [self presentViewController:alertc animated:YES completion:nil];
                        });
                    });
                    self.isSharingClicked = NO;
                    return;
                }
            }
            else
            {
                if(self.socialLoginAttempts < 3) {
                    ++self.socialLoginAttempts;
                    LoginToSocialNetworkOperation *loginOp = [[LoginToSocialNetworkOperation alloc] initWithDelegate:self andSFApp:self.sfApp forSocialNetwork:SOCIAL_TWITTER];
                    [self.sfApp.operationsQueue addOperation:loginOp];
                }
                else
                {
                    self.socialLoginAttempts = 0;
                    runOnMainQueueWithoutDeadlocking(^{
                        [self.spinner_Loading stopAnimating];
                        runOnMainQueueWithoutDeadlocking(^{
                            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"LOGIN_FAILED", nil)     message:NSLocalizedString(@"LOGIN_FAILED_MESSAGE", nil)  preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                                    style:UIAlertActionStyleDefault
                                                                                  handler:^(UIAlertAction * action) {}];
                            
                            [alert addAction:defaultAction];
                            [self presentViewController:alert animated:YES completion:nil];
                        });
                    });
                    self.isSharingClicked = NO;
                }
            }
        }
    } @catch (NSException *ex) {
        NSLog(@"StayLog: EXCEPTION in loginSocialWebViewDidSucceed with error: %@", ex.description);
        self.isSharingClicked = NO;
        [self.spinner_Loading stopAnimating];
    }
}

-(void)loginSocialWebviewDidFail:(NSArray *)error {
    //
    [self dismissViewControllerAnimated:YES completion:nil];
    self.isSharingClicked = NO;
    
    runOnMainQueueWithoutDeadlocking(^{
        [self.spinner_Loading stopAnimating];
        runOnMainQueueWithoutDeadlocking(^{
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"LOGIN_FAILED", nil)
                                                                           message:NSLocalizedString(@"LOGIN_FAILED_MESSAGE", nil)  preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                    style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        });
    });
}


#pragma mark - Popover delegate

-(void)chooseOption:(NSString *)option {
    if([option containsString:@"continueShare-"]) {
        self.movie.permission = MoviePermission_UNLISTED;
        runOnMainQueueWithoutDeadlocking(^{
            [self.spinner_Loading setHidden:NO];
            [self.spinner_Loading startAnimating];
        });
        [self.movie publishMovie];
        
        option = [option stringByReplacingOccurrencesOfString:@"continueShare-" withString:@""];
        if([option containsString:@"facebook"]) {
            [self shareToFacebook];
        } else if([option isEqualToString:@"instagram"]) {
            [self shareToInstagram];
        } else if([option isEqualToString:@"messenger"]) {
            [self shareToMessenger];
        } else if([option isEqualToString:@"whatsapp"]) {
            [self shareToWhatsApp];
        } else if([option isEqualToString:@"copylink"]) {
            [self copyClipboard];
        } else if([option isEqualToString:@"more"]) {
            [self moreOptions];
        }
        
        if(self.delegate != nil && [self.delegate respondsToSelector:@selector(decidedOption:)]) {
            [self.delegate performSelector:@selector(decidedOption:) withObject:@"changedPrivacy"];
        }
    }
    else if([option isEqualToString:@"cancelShare"]) {
        self.isSharingClicked = NO;
        runOnMainQueueWithoutDeadlocking(^{
            [self.spinner_Loading stopAnimating];
        });
        NSLog(@"StayLog: User cancelled sharing a private film.");
    }else if ([option isEqualToString:@"cancelDownload"]){
        
        self.instagramSetToShare = NO;
        self.downloadIsSelected = NO;
        self.messengerSetToShare = NO;
        
        
        [self.downloadTask cancel];
        
        [self.manager.operationQueue cancelAllOperations];
        
        [self removeVideoAtPath:self.downloadVideopath];
        
        self.isSharingClicked = NO;
        [self.spinner_Loading stopAnimating];
    }
}


#pragma mark - Social Custom shares

-(void)shareToFacebook {
    if(self.isModal){
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"film"
                                                              action:@"shared"
                                                               label:@"facebook"
                                                               value:@1] build]];
    } else {
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"share_film-in-profile"
                                                              action:@"shared"
                                                               label:@"facebook"
                                                               value:@1] build]];
    }
    
    
    if ([FBSDKAccessToken currentAccessToken]) {
        FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
        content.contentURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.stayfilm.com/movie/watch/%@", self.movie.idMovie]];

        [FBSDKShareDialog showFromViewController:self withContent:content delegate:self];
        self.isSharingClicked = NO;
        [self.spinner_Loading stopAnimating];
    }
    else {
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"share_film-in-profile"
                                                              action:@"shared"
                                                               label:@"facebook"
                                                               value:@1] build]];
        LoginFacebookOperation *loginOp = [[LoginFacebookOperation alloc] initWithoutUserOverrideWithDelegate:self andSFApp:self.sfApp];
        [self.sfApp.operationsQueue addOperation:loginOp];
    }
}

-(void)shareToInstagram {
    
//    BOOL stillShare = YES;
//    if(self.downloaderStayfilm == nil) {
//        self.downloaderStayfilm = [Downloader sharedDownloaderStayfilmSingletonWithDelegate:self];
//    }
//    if([self.downloaderStayfilm isMovieAlreadyDownloaded:self.movie] || [self fileExists:self.movie.idMovie]) {
//        stillShare = YES;
//    } else if([self.downloaderStayfilm isMovieDownloading:self.movie]) {
//        stillShare = NO;
//        runOnMainQueueWithoutDeadlocking(^{
//            UIAlertController *alertc = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"DOWNLOADING_MOVIE_TITLE_STILL", nil)
//                                                                            message:NSLocalizedString(@"DOWNLOADING_MOVIE_MESSAGE_STILL", nil)
//                                                                     preferredStyle:UIAlertControllerStyleAlert];
//            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
//                                                                    style:UIAlertActionStyleDefault
//                                                                  handler:^(UIAlertAction * action) {
//                                                                      [alertc dismissViewControllerAnimated:YES completion:nil];
//                                                                  }];
//            [alertc addAction:defaultAction];
//            [self presentViewController:alertc animated:YES completion:nil];
//        });
//    } else {
//        self.instagramSetToShare = YES;
//        runOnMainQueueWithoutDeadlocking(^{
//            [self showDownloadProgress];
//            [self downloadMovie];
//        });
//        stillShare = NO;
//    }
//
//    if(!stillShare) {
//        return;
//    }
    
    if(self.isModal){
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"film"
                                                              action:@"shared"
                                                               label:@"instagram"
                                                               value:@1] build]];
    } else {
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"share_film-in-profile"
                                                              action:@"shared"
                                                               label:@"instagram"
                                                               value:@1] build]];
    }
    
    NSURL *instagramURL = [NSURL URLWithString:@"instagram://"];
    if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) { //check if Instagram is installed
    
        if(self.movieFileInPhotosID == nil) { // has value if already saved to Photos
            self.movieFile = [self.downloaderStayfilm getPathForDownloadedMovie:self.movie];
            if(self.movieFile == nil) {
                self.movieFile = [self getFilePath:self.movie.idMovie];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                __block PHAssetChangeRequest *_mChangeRequest = nil;
                __block PHObjectPlaceholder *placeholder;
                NSURL *vidURL = [NSURL URLWithString:self.movieFile];
                [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
                    _mChangeRequest = [PHAssetChangeRequest creationRequestForAssetFromVideoAtFileURL:vidURL];
                    placeholder = _mChangeRequest.placeholderForCreatedAsset;
                } completionHandler:^(BOOL success, NSError * _Nullable error) {
                    if (success) {
                        self.movieFileInPhotosID = [placeholder localIdentifier];
                        
                        NSURL *instagramURL = [NSURL URLWithString:[NSString stringWithFormat:@"instagram://library?LocalIdentifier=\%@", self.movieFileInPhotosID]];
                        
                        if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
                            [[UIApplication sharedApplication] openURL:instagramURL options:@{} completionHandler:nil];
                        }
                        self.instagramSetToShare = NO;
                        self.isSharingClicked = NO;
                        [self.spinner_Loading stopAnimating];
                        return;
                    }
                    else {
                        NSLog(@"StayLog: Error copying to Photos. error: %@", error.description);
                        self.isSharingClicked = NO;
                        [self.spinner_Loading stopAnimating];
                        return;
                    }
                }];
            });
            
        } else {
            NSURL *instagramURL = [NSURL URLWithString:[NSString stringWithFormat:@"instagram://library?LocalIdentifier=\%@", self.movieFileInPhotosID]];
            
            if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
                [[UIApplication sharedApplication] openURL:instagramURL options:@{} completionHandler:nil];
            }
            self.instagramSetToShare = NO;
            self.isSharingClicked = NO;
            [self.spinner_Loading stopAnimating];
            
            return;
        }
    }
    else
    {
        runOnMainQueueWithoutDeadlocking(^{
            UIAlertController *alertc = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"INSTAGRAM_NOT_FOUND", nil)
                                                                            message:NSLocalizedString(@"INSTAGRAM_NOT_FOUND_MESSAGE", nil)
                                                                     preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                    style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {
                                                                      [alertc dismissViewControllerAnimated:YES completion:nil];
                                                                  }];
            [alertc addAction:defaultAction];
            [self presentViewController:alertc animated:YES completion:nil];
            
            self.isSharingClicked = NO;
            [self.spinner_Loading stopAnimating];
        });
        return;
    }
}
-(void)shareInstagram{
    
    if(self.isModal){
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"film"
                                                              action:@"shared"
                                                               label:@"instagram"
                                                               value:@1] build]];
    } else {
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"share_film-in-profile"
                                                              action:@"shared"
                                                               label:@"instagram"
                                                               value:@1] build]];
    }
//    self.instagramSetToShare = NO;
//    NSURL *instagramURL = [NSURL URLWithString:@"instagram://"];
//    if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) { //check if Instagram is installed
//
//            NSURL *instagramURL = [NSURL URLWithString:[NSString stringWithFormat:@"instagram://library?LocalIdentifier=\%@", self.movieFileInPhotosID]];
//
//            if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
//                [[UIApplication sharedApplication] openURL:instagramURL options:@{} completionHandler:nil];
//            }
//            self.instagramSetToShare = NO;
//            self.isSharingClicked = NO;
//            [self.spinner_Loading stopAnimating];
//
//            return;
//        }
    
    
    
    
    
    NSURL *instagramURL = [NSURL URLWithString:@"instagram://"];
    if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) { //check if Instagram is installed
        
        runOnMainQueueWithoutDeadlocking(^{
            UIAlertController *alertc = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"SHARE_INSTAGRAM", nil)
                                                    message:NSLocalizedString(@"OPTIONS_INSTAGRAM", nil)
                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* storiesAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"STORIES", nil)
                                                                    style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {
                                                                      [alertc dismissViewControllerAnimated:YES completion:nil];
                                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                                          // Verify app can open custom URL scheme. If able,
                                                                          // assign assets to pasteboard, open scheme.
                                                                          NSURL *urlScheme = [NSURL URLWithString:@"instagram-stories://share"];
                                                                          if ([[UIApplication sharedApplication] canOpenURL:urlScheme]) {
                                                                              
                                                                              NSData *videoData = [NSData dataWithContentsOfFile:self.movieFileInPhotos];
                                                                              //NSData *stickerData = UIImagePNGRepresentation([UIImage imageNamed:@"Login_Logo60"]);
                                                                              
                                                                              // Assign background and sticker image assets and
                                                                              // attribution link URL to pasteboard
                                                                              NSString  *attributionURL = [NSString stringWithFormat:@"https://www.stayfilm.com/movie/watch/%@", self.movie.idMovie];
//                                                                              NSLog(@"%@",attributionURL);
                                                                              NSArray *pasteboardItems = @[@{@"com.instagram.sharedSticker.backgroundVideo" : videoData,
//                                                                                                             @"com.instagram.sharedSticker.stickerImage" :stickerData,
                                                                                                             @"com.instagram.sharedSticker.contentURL" : attributionURL
                                                                                                            }];
                                                                              
                                                                              NSDictionary *pasteboardOptions = @{UIPasteboardOptionExpirationDate : [[NSDate date] dateByAddingTimeInterval:60 * 5]};
                                                                              // This call is iOS 10+, can use 'setItems' depending on what versions you support
                                                                              [[UIPasteboard generalPasteboard] setItems:pasteboardItems options:pasteboardOptions];
                                                                              
                                                                              [[UIApplication sharedApplication] openURL:urlScheme options:@{} completionHandler:nil];
                                                                              
                                                                              self.instagramSetToShare = NO;
                                                                              self.isSharingClicked = NO;
                                                                              [self.spinner_Loading stopAnimating];
                                                                              return;
                                                                          } else {
                                                                              // Handle older app versions or app not installed case
                                                                              NSLog(@"StayLog: INSTAGRAM SHARE ERROR could not share in stories.");
                                                                              self.instagramSetToShare = NO;
                                                                              self.isSharingClicked = NO;
                                                                              [self.spinner_Loading stopAnimating];
                                                                              return;
                                                                          }
                                                                      });
                                                                  }];
            UIAlertAction* feedAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"FEED", nil)
                                                                 style:UIAlertActionStyleDefault
                                                               handler:^(UIAlertAction * action) {
                                                                   [alertc dismissViewControllerAnimated:YES completion:nil];
                                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                                       
                                                                       if(self.movieFileInPhotos == nil) {
                                                                           __block PHAssetChangeRequest *_mChangeRequest = nil;
                                                                           __block PHObjectPlaceholder *placeholder;
                                                                           NSURL *vidURL = [NSURL URLWithString:self.movieFile];
                                                                           [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
                                                                               _mChangeRequest = [PHAssetChangeRequest creationRequestForAssetFromVideoAtFileURL:vidURL];
                                                                               placeholder = _mChangeRequest.placeholderForCreatedAsset;
                                                                           } completionHandler:^(BOOL success, NSError * _Nullable error) {
                                                                               if (success) {
                                                                                   self.movieFileInPhotos = [placeholder localIdentifier];
                                                                                   
                                                                                   NSURL *instagramURL = [NSURL URLWithString:[NSString stringWithFormat:@"instagram://library?LocalIdentifier=\%@", self.movieFileInPhotos]];
                                                                                   
                                                                                   if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
                                                                                       [[UIApplication sharedApplication] openURL:instagramURL options:@{} completionHandler:nil];
                                                                                   }
                                                                                   
                                                                                   self.instagramSetToShare = NO;
                                                                                   self.isSharingClicked = NO;
                                                                                   [self.spinner_Loading stopAnimating];
                                                                                   return;
                                                                               }
                                                                               else {
                                                                                   NSLog(@"StayLog: Error copying to Photos. error: %@", error.description);
                                                                                   self.instagramSetToShare = NO;
                                                                                   self.isSharingClicked = NO;
                                                                                   [self.spinner_Loading stopAnimating];
                                                                                   return;
                                                                               }
                                                                           }];
                                                                       } else {
                                                                           
                                                                           
                                                                           NSURL *instagramURL = [NSURL URLWithString:@"instagram://"];
                                                                           if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) { //check if Instagram is installed
                                                                               
                                                                               NSURL *instagramURL = [NSURL URLWithString:[NSString stringWithFormat:@"instagram://library?LocalIdentifier=\%@", self.movieFileInPhotosID]];
                                                                               
                                                                               if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
                                                                                   [[UIApplication sharedApplication] openURL:instagramURL options:@{} completionHandler:nil];
                                                                               }
                                                                               self.instagramSetToShare = NO;
                                                                               self.isSharingClicked = NO;
                                                                               [self.spinner_Loading stopAnimating];
                                                                               
                                                                               return;
                                                                           }
                                                                           
                                                                       }
                                                                   });
                                                               }];
            [alertc addAction:storiesAction];
            [alertc addAction:feedAction];
            [self presentViewController:alertc animated:YES completion:nil];
        });
    }
    else
    {
        runOnMainQueueWithoutDeadlocking(^{
            UIAlertController *alertc = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"INSTAGRAM_NOT_FOUND", nil)
                                                                            message:NSLocalizedString(@"INSTAGRAM_NOT_FOUND_MESSAGE", nil)
                                                                     preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                    style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {
                                                                      [alertc dismissViewControllerAnimated:YES completion:nil];
                                                                  }];
            [alertc addAction:defaultAction];
            [self presentViewController:alertc animated:YES completion:nil];
        });
        self.instagramSetToShare = NO;
        self.isSharingClicked = NO;
        [self.spinner_Loading stopAnimating];
        return;
    }

    
}
-(void)shareToWhatsApp {
//    BOOL stillShare = YES;
//    if(self.downloaderStayfilm == nil) {
//        self.downloaderStayfilm = [Downloader sharedDownloaderStayfilmSingletonWithDelegate:self];
//    }
//    if([self.downloaderStayfilm isMovieAlreadyDownloaded:self.movie]) {
//        stillShare = YES;
//    } else if([self.downloaderStayfilm isMovieDownloading:self.movie]) {
//        stillShare = NO;
//        runOnMainQueueWithoutDeadlocking(^{
//            UIAlertController *alertc = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"DOWNLOADING_MOVIE_TITLE_STILL", nil)
//                                                                            message:NSLocalizedString(@"DOWNLOADING_MOVIE_MESSAGE_STILL", nil)
//                                                                     preferredStyle:UIAlertControllerStyleAlert];
//            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
//                                                                    style:UIAlertActionStyleDefault
//                                                                  handler:^(UIAlertAction * action) {
//                                                                      [alertc dismissViewControllerAnimated:YES completion:nil];
//                                                                  }];
//            [alertc addAction:defaultAction];
//            [self presentViewController:alertc animated:YES completion:nil];
//        });
//    } else {
//        self.whatsappSetToShare = YES;
//        runOnMainQueueWithoutDeadlocking(^{
//            [self showDownloadProgress];
//            [self downloadMovie];
//        });
//        stillShare = NO;
//    }
//
//    if(!stillShare) {
//        return;
//    }
//
//    self.movieFile = [self.downloaderStayfilm getPathForDownloadedMovie:self.movie];
    
    if(self.isModal){
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"film"
                                                              action:@"shared"
                                                               label:@"whatsapp"
                                                               value:@1] build]];
    } else {
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"share_film-in-profile"
                                                              action:@"shared"
                                                               label:@"whatsapp"
                                                               value:@1] build]];
    }
    
    NSString * msg = [NSString stringWithFormat:NSLocalizedString(@"WHATSAPP_SHARE_MESSAGE", nil),[@"https://www.stayfilm.com/movie/watch/" stringByAppendingString:self.movie.idMovie]];
    NSString * urlWhats = [NSString stringWithFormat:@"whatsapp://send?text=%@",msg];
    NSURL * whatsappURL = [NSURL URLWithString:[urlWhats stringByAddingPercentEncodingWithAllowedCharacters:
                                               [NSCharacterSet URLFragmentAllowedCharacterSet]]];
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {

            [[UIApplication sharedApplication] openURL:whatsappURL];

    } else {
        // Cannot open whatsapp
        runOnMainQueueWithoutDeadlocking(^{
            UIAlertController *alertc = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"WHATSAPP_NOT_FOUND", nil)
                                                                            message:NSLocalizedString(@"WHATSAPP_NOT_FOUND_MESSAGE", nil)
                                                                     preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                    style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {
                                                                      [alertc dismissViewControllerAnimated:YES completion:nil];
                                                                  }];
            [alertc addAction:defaultAction];
            [self presentViewController:alertc animated:YES completion:nil];
        });
    }
    self.isSharingClicked = NO;
    [self.spinner_Loading stopAnimating];
}

-(void)shareToTwitter {
    if(self.sfApp == nil)
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    
    if([self.sfApp.currentUser.networks containsObject:@"twitter"]) {
        BOOL success = [self.movie shareMovieOnTwitterWithMessage:nil];
        if(success) {
            runOnMainQueueWithoutDeadlocking(^{
                UIAlertController *alertc = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"TWITTER_SHARED", nil)
                                                                                message:NSLocalizedString(@"TWITTER_SHARED_MESSAGE", nil)
                                                                         preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                        style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {
                                                                          [alertc dismissViewControllerAnimated:YES completion:nil];
                                                                      }];
                [alertc addAction:defaultAction];
                [self presentViewController:alertc animated:YES completion:nil];
            });
            self.isSharingClicked = NO;
            [self.spinner_Loading stopAnimating];
            return;
        }
        else {
            runOnMainQueueWithoutDeadlocking(^{
                UIAlertController *alertc = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"TWITTER_ERROR", nil)
                                                                                message:NSLocalizedString(@"TWITTER_ERROR_MESSAGE", nil)
                                                                         preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                        style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {
                                                                          [alertc dismissViewControllerAnimated:YES completion:nil];
                                                                      }];
                [alertc addAction:defaultAction];
                [self presentViewController:alertc animated:YES completion:nil];
            });
            self.isSharingClicked = NO;
            [self.spinner_Loading stopAnimating];
            return;
        }
    }
    else
    {
        // login twitter
        LoginToSocialNetworkOperation *loginOp = [[LoginToSocialNetworkOperation alloc] initWithDelegate:self andSFApp:self.sfApp forSocialNetwork:SOCIAL_TWITTER];
        [self.sfApp.operationsQueue addOperation:loginOp];
        return;
    }
    
}

-(void)shareToMessenger {
    
    self.messengerSetToShare = YES;
    [self downloadFilm];
    
   /* if(self.sfApp == nil)
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    
    long seconds = -1;
    
    if(self.movieFileInPhotos) {
        AVURLAsset *sourceAsset = [AVURLAsset URLAssetWithURL:[NSURL fileURLWithPath:self.movieFileInPhotos] options:nil];
        seconds = CMTimeGetSeconds(sourceAsset.duration);
    }
    else {
        AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:[NSURL URLWithString:self.movie.videoUrl]
                                                    options:[NSDictionary dictionaryWithObjectsAndKeys:
                                                             [NSNumber numberWithBool:YES],
                                                             AVURLAssetPreferPreciseDurationAndTimingKey,
                                                             nil]];
        
        if (asset)
            seconds = CMTimeGetSeconds(asset.duration);
    }
    
    
    if(seconds != -1 && seconds <= 40) {
        BOOL stillShare = YES;
        if(self.downloaderStayfilm == nil) {
            self.downloaderStayfilm = [Downloader sharedDownloaderStayfilmSingletonWithDelegate:self];
        }
        if([self.downloaderStayfilm isMovieAlreadyDownloaded:self.movie] || [self fileExists:self.movie.idMovie]) {
            stillShare = YES;
        } else if([self.downloaderStayfilm isMovieDownloading:self.movie]) {
            stillShare = NO;
            runOnMainQueueWithoutDeadlocking(^{
                UIAlertController *alertc = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"DOWNLOADING_MOVIE_TITLE_STILL", nil)
                                                                                message:NSLocalizedString(@"DOWNLOADING_MOVIE_MESSAGE_STILL", nil)
                                                                         preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                        style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {
                                                                          [alertc dismissViewControllerAnimated:YES completion:nil];
                                                                      }];
                [alertc addAction:defaultAction];
                [self presentViewController:alertc animated:YES completion:nil];
            });
        } else {
            self.messengerSetToShare = YES;
            runOnMainQueueWithoutDeadlocking(^{
                [self showDownloadProgress];
                [self downloadMovie];
            });
            stillShare = NO;
        }
        
        if(!stillShare) {
            return;
        }
        
        if(self.isModal){
            //Google Analytics Event
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"film"
                                                                  action:@"shared"
                                                                   label:@"messenger"
                                                                   value:@1] build]];
        } else {
            //Google Analytics Event
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"share_film-in-profile"
                                                                  action:@"shared"
                                                                   label:@"messenger"
                                                                   value:@1] build]];
        }
        
        if(self.movieFileInPhotos == nil) { // has value if already saved to Photos
            self.movieFile = [self.downloaderStayfilm getPathForDownloadedMovie:self.movie];
        } else {
            self.movieFile = self.movieFileInPhotos;
        }
        
        if(self.movieFile == nil) { // this is for the case share is opened with file on cache folder
            self.movieFile = [self getFilePath:self.movie.idMovie];
            self.messengerSetToShare = YES;
            if(UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(self.movieFile)) {
//                UISaveVideoAtPathToSavedPhotosAlbum(self.movieFile,self,@selector(video:didFinishSavingWithError:contextInfo:), nil);
            }
            self.isSharingClicked = NO;
            [self.spinner_Loading stopAnimating];
            return;
        }
        
        NSData *videoData = [NSData dataWithContentsOfFile:self.movieFile];
        [FBSDKMessengerSharer shareVideo:videoData withOptions:nil];
    }
    else  // Video is over 30 seconds, So we need to send the URL and not the video file
    {
        if(self.isModal){
            //Google Analytics Event
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"film"
                                                                  action:@"shared"
                                                                   label:@"messenger"
                                                                   value:@1] build]];
        } else {
            //Google Analytics Event
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"share_film-in-profile"
                                                                  action:@"shared"
                                                                   label:@"messenger"
                                                                   value:@1] build]];
        }
        
        FBSDKShareMessengerURLActionButton *urlButton = [[FBSDKShareMessengerURLActionButton alloc] init];
        urlButton.title = @"Watch it on Stayfilm";
        urlButton.url = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.stayfilm.com/movie/watch/%@", self.movie.idMovie]];
        
        FBSDKShareMessengerGenericTemplateElement *element = [[FBSDKShareMessengerGenericTemplateElement alloc] init];
        element.title = self.movie.title;
        element.subtitle = @"Created using Stayfilm.";
        element.imageURL = [NSURL URLWithString:self.movie.thumbnailUrl];
        element.button = urlButton;
        
        FBSDKShareMessengerGenericTemplateContent *content = [[FBSDKShareMessengerGenericTemplateContent alloc] init];
        content.pageID = @"285328458170391"; // Your page ID, required for attribution
        //content.pageID = @"783218328381399";
        //content.pageID = @"InsideStayfilm";
        content.element = element;
        
        FBSDKMessageDialog *messageDialog = [[FBSDKMessageDialog alloc] init];
        messageDialog.shareContent = content;
        
        if ([messageDialog canShow]) {
            [messageDialog show];
        }
    }
    
    self.isSharingClicked = NO;
    [self.spinner_Loading stopAnimating];
    */
}

-(void)shareInMessenger{
    long seconds = -1;
    self.messengerSetToShare = NO;
    if(self.movieFileInPhotos) {
        AVURLAsset *sourceAsset = [AVURLAsset URLAssetWithURL:[NSURL fileURLWithPath:self.movieFileInPhotos] options:nil];
        seconds = CMTimeGetSeconds(sourceAsset.duration);
//        if(seconds != -1 && seconds <= 40) {
        
            if(self.isModal){
                //Google Analytics Event
                id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"film"
                                                                      action:@"shared"
                                                                       label:@"messenger"
                                                                       value:@1] build]];
            } else {
                //Google Analytics Event
                id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"share_film-in-profile"
                                                                      action:@"shared"
                                                                       label:@"messenger"
                                                                       value:@1] build]];
            }
            
            
            NSData *videoData = [NSData dataWithContentsOfFile:self.movieFileInPhotos];
            [FBSDKMessengerSharer shareVideo:videoData withOptions:nil];
            
            
            
//        }
//        else{// Video is over 30 seconds, So we need to send the URL and not the video file
//            
//                if(self.isModal){
//                    //Google Analytics Event
//                    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//                    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"film"
//                                                                          action:@"shared"
//                                                                           label:@"messenger"
//                                                                           value:@1] build]];
//                } else {
//                    //Google Analytics Event
//                    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//                    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"share_film-in-profile"
//                                                                          action:@"shared"
//                                                                           label:@"messenger"
//                                                                           value:@1] build]];
//                }
//                
//                FBSDKShareMessengerURLActionButton *urlButton = [[FBSDKShareMessengerURLActionButton alloc] init];
//                urlButton.title = @"Watch it on Stayfilm";
//                urlButton.url = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.stayfilm.com/movie/watch/%@", self.movie.idMovie]];
//                
//                FBSDKShareMessengerGenericTemplateElement *element = [[FBSDKShareMessengerGenericTemplateElement alloc] init];
//                element.title = self.movie.title;
//                element.subtitle = @"Created using Stayfilm.";
//                element.imageURL = [NSURL URLWithString:self.movie.thumbnailUrl];
//                element.button = urlButton;
//                
//                FBSDKShareMessengerGenericTemplateContent *content = [[FBSDKShareMessengerGenericTemplateContent alloc] init];
//                content.pageID = @"285328458170391"; // Your page ID, required for attribution
//                content.element = element;
//                
//                FBSDKMessageDialog *messageDialog = [[FBSDKMessageDialog alloc] init];
//                messageDialog.shareContent = content;
//                
//                if ([messageDialog canShow]) {
//                    [messageDialog show];
//                }
//            }
            
        
        
    }
    self.isSharingClicked = NO;
    [self.spinner_Loading stopAnimating];
}



-(void)copyClipboard {
    if(self.isModal){
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"film"
                                                              action:@"shared"
                                                               label:@"copy-link"
                                                               value:@1] build]];
    } else {
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"share_film-in-profile"
                                                              action:@"shared"
                                                               label:@"copy-link"
                                                               value:@1] build]];
    }
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = [@"https://www.stayfilm.com/movie/watch/" stringByAppendingString:self.movie.idMovie];
    
    runOnMainQueueWithoutDeadlocking(^{
        UIAlertController *alertc = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"COPIED_TO_CLIPBOARD_TITLE", nil)
                                                                        message:NSLocalizedString(@"COPIED_TO_CLIPBOARD_MESSAGE", nil)
                                                                 preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {
                                                                  [alertc dismissViewControllerAnimated:YES completion:nil];
                                                              }];
        [alertc addAction:defaultAction];
        [self presentViewController:alertc animated:YES completion:nil];
    });
    self.isSharingClicked = NO;
    [self.spinner_Loading stopAnimating];
}

-(void)moreOptions {
    if(self.isModal){
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"film"
                                                              action:@"shared"
                                                               label:@"more"
                                                               value:@1] build]];
    } else {
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"share_film-in-profile"
                                                              action:@"shared"
                                                               label:@"more"
                                                               value:@1] build]];
    }
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.stayfilm.com/movie/watch/%@", self.movie.idMovie]];
    
    NSArray * shareItems = @[url];
    
    //                InstagramActivity *instaActivity = [[InstagramActivity alloc] init];
    //                instaActivity.movie = movie;
    //                instaActivity.profileView = self;
    //                instaActivity.videoFinishView = nil;
    //                instaActivity.view = self.view;
    //                instaActivity.sf = sfAppProfile;
    //
    //
    //                DownloadActivity *downloadActivity = [[DownloadActivity alloc] init];
    //                downloadActivity.movie = movie;
    //                downloadActivity.view = self;
    //                downloadActivity.profileView = self;
    //                downloadActivity.videoFinishView = nil;
    
    //                UIActivityViewController * avc = [[UIActivityViewController alloc] initWithActivityItems:shareItems applicationActivities:@[instaActivity,downloadActivity]];
    UIActivityViewController * avc = [[UIActivityViewController alloc] initWithActivityItems:shareItems applicationActivities:nil];
    
    
    NSArray *excludeActivities = @[UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypeAirDrop,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo,
                                   UIActivityTypePostToTwitter,
                                   UIActivityTypePostToFacebook,
                                   UIActivityTypePostToTencentWeibo,
                                   UIActivityTypePostToWeibo ];
    
    avc.excludedActivityTypes = excludeActivities;
    
    __weak typeof(avc) selfDelegate = avc;
    [avc setCompletionWithItemsHandler:^(UIActivityType  _Nullable activityType, BOOL completed, NSArray * _Nullable returnedItems, NSError * _Nullable activityError) {
        
//        NSLog(@"TYPE: %@", activityType);
//        NSLog(@"returnedItems: %@", returnedItems);
//        NSLog(@"Error: %@", activityError);
        /*net.whatsapp.WhatsApp.ShareExtension
         //DownloadActivity */
        if ([activityType isEqual:@"com.apple.UIKit.activity.CopyToPasteboard"]){
            
            [self.view makeToast:NSLocalizedString(@"LINK_COPIED", nil)];
            
        }else if([activityType isEqual:@"com.apple.UIKit.activity.PostToTwitter"]){
            
            [self.view makeToast:NSLocalizedString(@"SHARED_LOWERED", nil)];
            
        }
        
        selfDelegate.completionWithItemsHandler = nil;
    }];
    
    if([[UIScreen mainScreen] bounds].size.height >= kIPadHeight){
        UIPopoverPresentationController *popOver = avc.popoverPresentationController;
        popOver.sourceView = self.collectionMore.subviews.firstObject;
        [self presentViewController:avc animated:YES completion:nil];
    }else{
        [self presentViewController:avc animated:YES completion:nil];
    }
    
    self.isSharingClicked = NO;
    [self.spinner_Loading stopAnimating];
}

#pragma mark - Collection Delegates


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if(collectionView.tag == 1) {
        return self.socialButtons.count;
    }
    else if(collectionView.tag == 2) {
        return self.moreButtons.count;
    } else {
        return 0;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView.tag == 1)
    {
        ShareButton_CollectionViewCell *cell = (ShareButton_CollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier1 forIndexPath:indexPath];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ShareButton_CollectionViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        cell.image.image = [UIImage imageNamed:[((NSMutableDictionary *)[self.socialButtons objectAtIndex:(int)indexPath.item]) objectForKey:@"image"]];
        cell.title.text = [((NSMutableDictionary *)[self.socialButtons objectAtIndex:(int)indexPath.item]) objectForKey:@"title"];
        
        return cell;
    }
    else if(collectionView.tag == 2)
    {
        ShareButton_CollectionViewCell *cell = (ShareButton_CollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier2 forIndexPath:indexPath];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ShareButton_CollectionViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        cell.image.image = [UIImage imageNamed:[((NSMutableDictionary *)[self.moreButtons objectAtIndex:(int)indexPath.item]) objectForKey:@"image"]];
        cell.title.text = [((NSMutableDictionary *)[self.moreButtons objectAtIndex:(int)indexPath.item]) objectForKey:@"title"];
        
        if([cell.title.text isEqualToString:NSLocalizedString(@"DOWNLOAD", nil)] || [cell.title.text isEqualToString:NSLocalizedString(@"DOWNLOADED", nil)]) {
            if (self.isDownloaded) {
                cell.title.text = NSLocalizedString(@"DOWNLOADED", nil);
                cell.image.image = [UIImage imageNamed:@"SHARE_Downloaded"];
            } else {
                cell.title.text = NSLocalizedString(@"DOWNLOAD", nil);
                cell.image.image = [UIImage imageNamed:@"SHARE_Download"];
            }
        }
        
        return cell;
    }
    return  nil;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if(!self.isSharingClicked) {
        self.isSharingClicked = YES;
        [self.spinner_Loading startAnimating];
        
        if(collectionView.tag == 1) {
            ShareButton_CollectionViewCell *cell = (ShareButton_CollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier1 forIndexPath:indexPath];
            
            if(cell == nil) {
                self.isSharingClicked = NO;
                return;
            }
            NSDictionary *dict = [self.socialButtons objectAtIndex:indexPath.item];
            NSString *platform = [dict objectForKey:@"title"];
            
            if([platform isEqualToString:NSLocalizedString(@"FACEBOOK", nil)])
            {
                if(self.movie.permission == MoviePermission_PRIVATE) {
                    runOnMainQueueWithoutDeadlocking(^{
                        Popover_ConfirmPrivacyShareViewController *view = [[Popover_ConfirmPrivacyShareViewController alloc] init];
                        view.delegate = self;
                        view.option = @"facebook";
                        view.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                        view.transitioningDelegate = self;
                        view.view.frame = self.view.frame;
                        
                        self.transitionAnimation.duration = 0.3;
                        self.transitionAnimation.isPresenting = YES;
                        self.transitionAnimation.originFrame = self.view.frame;
                        
                        [self presentViewController:view animated:YES completion:nil];
                    });
                } else {
                    [self shareToFacebook];
                }
            }
            else if([platform isEqualToString:NSLocalizedString(@"INSTAGRAM", nil)])
            {
                if(self.movie.permission == MoviePermission_PRIVATE) {
                    runOnMainQueueWithoutDeadlocking(^{
                        Popover_ConfirmPrivacyShareViewController *view = [[Popover_ConfirmPrivacyShareViewController alloc] init];
                        view.delegate = self;
                        view.option = @"instagram";
                        view.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                        view.transitioningDelegate = self;
                        view.view.frame = self.view.frame;
                        
                        self.transitionAnimation.duration = 0.3;
                        self.transitionAnimation.isPresenting = YES;
                        self.transitionAnimation.originFrame = self.view.frame;
                        
                        [self presentViewController:view animated:YES completion:nil];
                    });
                } else {
                    self.instagramSetToShare = YES;
                    [self downloadFilm];
                }
            }
            else if([platform isEqualToString:NSLocalizedString(@"WHATSAPP", nil)])
            {
                if(self.movie.permission == MoviePermission_PRIVATE) {
                    runOnMainQueueWithoutDeadlocking(^{
                        Popover_ConfirmPrivacyShareViewController *view = [[Popover_ConfirmPrivacyShareViewController alloc] init];
                        view.delegate = self;
                        view.option = @"whatsapp";
                        view.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                        view.transitioningDelegate = self;
                        view.view.frame = self.view.frame;
                        
                        self.transitionAnimation.duration = 0.3;
                        self.transitionAnimation.isPresenting = YES;
                        self.transitionAnimation.originFrame = self.view.frame;
                        
                        [self presentViewController:view animated:YES completion:nil];
                    });
                } else {
                    [self shareToWhatsApp];
                }
            }
            else if([platform isEqualToString:NSLocalizedString(@"MESSENGER", nil)]) {
                if(self.movie.permission == MoviePermission_PRIVATE) {
                    runOnMainQueueWithoutDeadlocking(^{
                        Popover_ConfirmPrivacyShareViewController *view = [[Popover_ConfirmPrivacyShareViewController alloc] init];
                        view.delegate = self;
                        view.option = @"messenger";
                        view.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                        view.transitioningDelegate = self;
                        view.view.frame = self.view.frame;
                        
                        self.transitionAnimation.duration = 0.3;
                        self.transitionAnimation.isPresenting = YES;
                        self.transitionAnimation.originFrame = self.view.frame;
                        
                        [self presentViewController:view animated:YES completion:nil];
                    });
                } else {
                    [self shareToMessenger];
//                    NSString *filepath = [[NSBundle mainBundle] pathForResource:@"selfie_vid" ofType:@"mp4"];
//                    NSData *videoData = [NSData dataWithContentsOfFile:filepath];
//                    [FBSDKMessengerSharer shareVideo:videoData withOptions:nil];
//                    [self.delegate performSelector:@selector(decidedOption:) withObject:@"messenger"];
//                    self.isSharingClicked = NO;
                }
            }
            else if([platform isEqualToString:NSLocalizedString(@"TWITTER", nil)]) {
                if(self.movie.permission == MoviePermission_PRIVATE) {
                    runOnMainQueueWithoutDeadlocking(^{
                        Popover_ConfirmPrivacyShareViewController *view = [[Popover_ConfirmPrivacyShareViewController alloc] init];
                        view.delegate = self;
                        view.option = @"twitter";
                        view.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                        view.transitioningDelegate = self;
                        view.view.frame = self.view.frame;
                        
                        self.transitionAnimation.duration = 0.3;
                        self.transitionAnimation.isPresenting = YES;
                        self.transitionAnimation.originFrame = self.view.frame;
                        
                        [self presentViewController:view animated:YES completion:nil];
                    });
                } else {
                    [self shareToTwitter];
                }
            }
            else {
                self.isSharingClicked = NO;
                [self.spinner_Loading stopAnimating];
            }
        }
        else if (collectionView.tag == 2) {
            ShareButton_CollectionViewCell *cell = (ShareButton_CollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier2 forIndexPath:indexPath];
            
            if(cell == nil) {
                self.isSharingClicked = NO;
                return;
            }
            NSDictionary *dict = [self.moreButtons objectAtIndex:indexPath.item];
            NSString *platform = [dict objectForKey:@"title"];
            
            if([platform isEqualToString:NSLocalizedString(@"DOWNLOAD", nil)]) {
//                [self downloadMovie];
                [self downloadFilm];
                [self.spinner_Loading stopAnimating];
                
                if(self.isModal){
                    //Google Analytics Event
                    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"film"
                                                                          action:@"shared"
                                                                           label:@"download"
                                                                           value:@1] build]];
                } else {
                    //Google Analytics Event
                    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"share_film-in-profile"
                                                                          action:@"shared"
                                                                           label:@"download"
                                                                           value:@1] build]];
                }
            }
            else if([platform isEqualToString:NSLocalizedString(@"DOWNLOADED", nil)])
            {
                [self downloadFilm];
                [self.spinner_Loading stopAnimating];
                /*if(self.movieFileInPhotos == nil || [self.movieFileInPhotos isEqualToString:@""]) {
                    self.movieFile = [self getFilePath:self.movie.idMovie];
                    if(UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(self.movieFile)) {
//                        UISaveVideoAtPathToSavedPhotosAlbum(self.movieFile,self,@selector(video:didFinishSavingWithError:contextInfo:), nil);
                    }
                    [self showAlreadyDownloaded];
                    self.isSharingClicked = NO;
                    [self.spinner_Loading stopAnimating];
                }
                else { // Already saved to Photos
                    [self showAlreadyDownloaded];
                    self.isSharingClicked = NO;
                    [self.spinner_Loading stopAnimating];
                }*/
            }
            else if([platform isEqualToString:NSLocalizedString(@"COPY_LINK", nil)])
            {
                if(self.movie.permission == MoviePermission_PRIVATE) {
                    runOnMainQueueWithoutDeadlocking(^{
                        Popover_ConfirmPrivacyShareViewController *view = [[Popover_ConfirmPrivacyShareViewController alloc] init];
                        view.delegate = self;
                        view.option = @"copylink";
                        view.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                        view.transitioningDelegate = self;
                        view.view.frame = self.view.frame;
                        
                        self.transitionAnimation.duration = 0.3;
                        self.transitionAnimation.isPresenting = YES;
                        self.transitionAnimation.originFrame = self.view.frame;
                        
                        [self presentViewController:view animated:YES completion:nil];
                    });
                } else {
                    [self copyClipboard];
                }
            }
            else if([platform isEqualToString:NSLocalizedString(@"MORE", nil)])
            {
                if(self.movie == nil) {
                    NSLog(@"StayLog: ERROR... movie is nil on share didselectitem");
                    self.isSharingClicked = NO;
                    [self.spinner_Loading stopAnimating];
                    return;
                }
                if(self.movie.permission == MoviePermission_PRIVATE) {
                    runOnMainQueueWithoutDeadlocking(^{
                        Popover_ConfirmPrivacyShareViewController *view = [[Popover_ConfirmPrivacyShareViewController alloc] init];
                        view.delegate = self;
                        view.option = @"more";
                        view.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                        view.transitioningDelegate = self;
                        view.view.frame = self.view.frame;
                        
                        self.transitionAnimation.duration = 0.3;
                        self.transitionAnimation.isPresenting = YES;
                        self.transitionAnimation.originFrame = self.view.frame;
                        
                        [self presentViewController:view animated:YES completion:nil];
                    });
                } else {
                    [self moreOptions];
                }
            }
            else {
                self.isSharingClicked = NO;
                [self.spinner_Loading stopAnimating];
            }
        }
    }
}



#pragma mark - Buttons

BOOL isHelping = NO;
- (IBAction)but_help_Clicked:(id)sender {
    if(!isHelping) {
        isHelping = YES;
        
        if(self.isModal){
            //Google Analytics Event
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"whats-gallery-info"
                                                                  action:@"ok"
                                                                   label:@""
                                                                   value:@1] build]];
        } else {
            //Google Analytics Event
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"share_film-in-profile"
                                                                  action:@"whats-gallery-info"
                                                                   label:@"ok"
                                                                   value:@1] build]];
        }
        
        runOnMainQueueWithoutDeadlocking(^{
            self.view_Help.alpha = 0.0;
            [self.view_Help setHidden:NO];
            
            self.but_done.alpha = 0.0;
            [self.but_done setTitle:NSLocalizedString(@"OK", nil) forState:UIControlStateNormal];
            [self.but_done setTitle:NSLocalizedString(@"OK", nil) forState:UIControlStateSelected];
            [self.but_done setImage:[UIImage imageNamed:@"Icon_xCheck"] forState:UIControlStateNormal];
            [self.but_done setImage:[UIImage imageNamed:@"Icon_xCheck"] forState:UIControlStateSelected];
            [UIView animateWithDuration:0.3 animations:^{
                self.view_Help.alpha = 1.0;
                self.but_done.alpha = 1.0;
            }];
            //self.isSharingClicked = NO;
        });
    }
}

- (IBAction)but_done_Clicked:(id)sender {
    if(isHelping) {
        runOnMainQueueWithoutDeadlocking(^{
            self.but_done.alpha = 0.0;
            [self.but_done setTitle:NSLocalizedString(@"IM_DONE", nil) forState:UIControlStateNormal];
            [self.but_done setTitle:NSLocalizedString(@"IM_DONE", nil) forState:UIControlStateSelected];
            [self.but_done setImage:[UIImage imageNamed:@"Icon_xCheck"] forState:UIControlStateNormal];
            [self.but_done setImage:[UIImage imageNamed:@"Icon_xCheck"] forState:UIControlStateSelected];
            [UIView animateWithDuration:0.3 animations:^{
                self.view_Help.alpha = 0.0;
                self.but_done.alpha = 1.0;
            } completion:^(BOOL finished) {
                [self.view_Help setHidden:YES];
                isHelping = NO;
            }];
        });
        return;
    }
    if(!self.isSharingClicked) {
        self.isSharingClicked = YES;
        if([self.txt_Downloading.text isEqualToString:NSLocalizedString(@"ALREADY_DOWNLOADED", nil)]) {
            [self hideDownloadProgress];
            return;
        }
        if(!self.view_Download.isHidden) {
            if(self.downloaderStayfilm == nil) {
                self.downloaderStayfilm = [Downloader sharedDownloaderStayfilmSingletonWithDelegate:self];
            }
            [self.downloaderStayfilm resetDownload];
            self.instagramSetToShare = NO;
            self.whatsappSetToShare = NO;
            self.isSharingClicked = NO;
            [self hideDownloadProgress];
            return;
        }

//        if(!self.view_Download.isHidden || buttonsview.hidden) {
//            [self hideDownloadProgress];
//        } else {
        if(self.switchShowGallery.isOn && self.movie.permission != MoviePermission_PUBLIC) {
            self.movie.permission = MoviePermission_PUBLIC;
            
            if(self.isModal){
                //Google Analytics Event
                id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"film"
                                                                      action:@"shared"
                                                                       label:@"stayfilm-gallery"
                                                                       value:@1] build]];
            } else {
                //Google Analytics Event
                id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"share_film-in-profile"
                                                                      action:@"shared"
                                                                       label:@"stayfilm-gallery"
                                                                       value:@1] build]];
            }
            
            
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
               // [self.movie publishMovie];
                [self.movie changePermisssionMovie];
            });
        }
        else if(self.movie.permission == MoviePermission_PUBLIC && !self.switchShowGallery.isOn) {
            self.movie.permission = MoviePermission_UNLISTED;
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                //[self.movie publishMovie];
                [self.movie changePermisssionMovie];
            });
        }
        
        if(self.isModal){
            //Google Analytics Event
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"share"
                                                                  action:@"done"
                                                                   label:@""
                                                                   value:@1] build]];
        } else {
            //Google Analytics Event
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"share_film-in-profile"
                                                                  action:@"share"
                                                                   label:@"done"
                                                                   value:@1] build]];
        }
        if ( [self.downloadSwitch isOn]){
            
            self.downloadIsSelected = YES;
            [self downloadFilm];
            
        }else{
            [self.delegate performSelector:@selector(decidedOption:) withObject:@"done"];
        }
        //[self.delegate performSelector:@selector(dismissModalMenu) withObject:nil];
            self.isSharingClicked = NO;
//        }
    }
}

- (IBAction)tapGesture_Clicked:(id)sender {

    if(self.isModal) {
        return;
    }
    
    if(self.downloading) {
        UIAlertController *alertc = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"BACK_TAP_CANCEL_DOWNLOAD", nil) message:NSLocalizedString(@"BACK_TAP_CANCEL_DOWNLOAD_MESSAGE", nil) preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"NO", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [alertc dismissViewControllerAnimated:YES completion:nil];
        }];
        UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"CANCEL", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
            if(self.downloaderStayfilm != nil) {
                [self.downloaderStayfilm resetDownload];
            }
            runOnMainQueueWithoutDeadlocking(^{
                [self hideDownloadProgress];
            });
            [alertc dismissViewControllerAnimated:YES completion:nil];
        }];
        [alertc addAction:defaultAction];
        [alertc addAction:cancelAction];
        [self presentViewController:alertc animated:YES completion:nil];
        return;
    }
    
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"share_film-in-profile"
                                                          action:@"cancel-share"
                                                           label:@"tap-out"
                                                           value:@1] build]];
    
    [self.delegate performSelector:@selector(dismissModalMenu) withObject:nil];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:self.contentView]) {
        return NO;
    }
    return YES;
}

- (IBAction)switch_ShowGallery_changed:(id)sender {
    if(self.switchShowGallery.isOn) {
        if(self.isModal){
            //Google Analytics Event
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"stayfilm-gallery"
                                                                  action:@"on"
                                                                   label:@""
                                                                   value:@1] build]];
        } else {
            //Google Analytics Event
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"share_film-in-profile"
                                                                  action:@"stayfilm-gallery"
                                                                   label:@"on"
                                                                   value:@1] build]];
        }
    } else {
        if(self.isModal){
            //Google Analytics Event
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"stayfilm-gallery"
                                                                  action:@"off"
                                                                   label:@""
                                                                   value:@1] build]];
        } else {
            //Google Analytics Event
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"share_film-in-profile"
                                                                  action:@"stayfilm-gallery"
                                                                   label:@"off"
                                                                   value:@1] build]];
        }
    }
}


#pragma mark - Download Operations Delegate

- (BOOL)fileExists:(NSString*)idMovie
{
    @try
    {
        NSString* filePath;
        
        if (idMovie != nil && [idMovie length] > 0)
        {
            NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString  *documentsDirectory = ([paths objectAtIndex:0] != nil)? [paths objectAtIndex:0] : nil;
            
            NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/StayFilms"];
            
            //Check Stayfilm folder if it does exists
            if ([[NSFileManager defaultManager] fileExistsAtPath:dataPath])
            {
                filePath = [NSString stringWithFormat:@"%@/%@", dataPath, [idMovie stringByAppendingString:@".mp4"]];
                //Checks if file exists
                if ([[NSFileManager defaultManager] fileExistsAtPath:filePath])
                {
                    return YES;
                }
            }
            return NO;
        }
        else
        {
            NSLog(@"StayLog: Error: idMovie missing.");
        }
    }
    @catch (NSException *exception)
    {
        NSLog(@"StayLog: fileExists exception with error: %@", exception.description);
    }
}

-(NSString*)getFilePath:(NSString*)idMovie {
    @try
    {
        NSString* filePath;
        
        if (idMovie != nil && [idMovie length] > 0)
        {
            NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString  *documentsDirectory = ([paths objectAtIndex:0] != nil)? [paths objectAtIndex:0] : nil;
            
            NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/StayFilms"];
            
            //Check Stayfilm folder if it does exists
            if ([[NSFileManager defaultManager] fileExistsAtPath:dataPath])
            {
                filePath = [NSString stringWithFormat:@"%@/%@", dataPath, [idMovie stringByAppendingString:@".mp4"]];
                //Checks if file exists
                if ([[NSFileManager defaultManager] fileExistsAtPath:filePath])
                {
                    return filePath;
                }
            }
            return nil;
        }
        else
        {
            NSLog(@"StayLog: Error: idMovie missing.");
        }
    }
    @catch (NSException *exception)
    {
        NSLog(@"StayLog: fileExists exception with error: %@", exception.description);
    }
}




- (void)downloadMovie {
    
    if([self fileExists:self.movie.idMovie]){
        self.isDownloaded = YES;
    }
    
    @try {
        if(self.isDownloaded)
        {
            if(self.movieFileInPhotos == nil || [self.movieFileInPhotos isEqualToString:@""]) {
                self.movieFile = [self getFilePath:self.movie.idMovie];
                if(UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(self.movieFile)) {
//                    UISaveVideoAtPathToSavedPhotosAlbum(self.movieFile,self,@selector(video:didFinishSavingWithError:contextInfo:), nil);
                }
            }
            else {  // File already in Photos

                [self showAlreadyDownloaded];

                self.isSharingClicked = NO;
                return;
            }
        }
        if(self.downloading)
            return;
        self.downloading = YES;
        NSLog(@"StayLog: downloadMovie invoked");
        [self showDownloadProgress];
        
        if(self.downloaderStayfilm == nil) {
            self.downloaderStayfilm = [Downloader sharedDownloaderStayfilmSingletonWithDelegate:self];
        }
        if(self.instagramSetToShare) {
            [self.downloaderStayfilm addMovieWithoutSavingToPhotosAlbum:self.movie];
        } else {
            [self.downloaderStayfilm addMovie:self.movie];
        }
    }
    @catch (NSException *ex) {
        NSLog(@"StayLog: EXCEPTION downloadMovie with error: %@", ex.description);
        self.downloading = NO;
    }
}

- (void)downloadDidCancel:(Downloader *)downloader {
    self.downloading = NO;
    
    self.instagramSetToShare = NO;
    self.whatsappSetToShare = NO;
    self.messengerSetToShare = NO;
    
    self.isSharingClicked = NO;
    [self.spinner_Loading stopAnimating];
}

- (void)downloadDidFail:(Downloader *)downloader {
    self.downloading = NO;
    [self showDownloadFailed];
    
    self.isSharingClicked = NO;
    [self.spinner_Loading stopAnimating];
}

- (void)downloadDidFailMedia:(NSString *)movieID {
    self.downloading = NO;
    [self showDownloadFailed];
    
    self.isSharingClicked = NO;
    [self.spinner_Loading stopAnimating];
}

- (void)downloadDidFinish:(Downloader *)downloader {
    //    [self.popoverShareView hideDownloadProgress];
    //    [self.popoverShareView setDownloadedButton];
    self.downloading = NO;
}

- (void)alreadyDownloadedMedia:(Movie *)movie {
    self.downloading = NO;
    [self showAlreadyDownloaded];
}

- (void)downloadDidStart {
    // do nothing
}

- (void)downloadFinishMedia:(Movie *)movie {
    self.downloading = NO;
    [self hideDownloadProgressSettingDownloadedButton];
    if(self.instagramSetToShare) {
        [self shareToInstagram];
    }
    if(self.whatsappSetToShare) {
        [self shareToWhatsApp];
    }
    if(self.messengerSetToShare) {
        [self shareToMessenger];
    }
}

- (void)downloadOverallProgressChanged:(NSNumber *)progress {
    // do nothing
}

- (void)downloadProgressChanged:(NSArray *)movie_Progress {

    self.txt_DownloadProgress.text = [NSString stringWithFormat:NSLocalizedString(@"DOWNLOADING_PROGRESS", nil),((NSNumber *)movie_Progress[1]).floatValue * 100.0 ];
    self.progress_Download.progress = ((NSNumber *)movie_Progress[1]).floatValue;
}

/// Callback for copying to camera roll
- (void)               video: (NSString *) videoPath
    didFinishSavingWithError: (NSError *) error
                 contextInfo: (void *) contextInfo
{
    if (!error) {
        self.movieFileInPhotos = videoPath;
        if(self.instagramSetToShare) {
            NSString * urlBase = [NSString stringWithFormat:@"instagram://library?AssetPath=%@&InstagramCaption=%@",[videoPath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet alphanumericCharacterSet]], [self.movie.title stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet alphanumericCharacterSet]]];
            NSURL *instagramURL = [NSURL URLWithString:urlBase];
            
            if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
                if(self.movieFile != nil) {
                    [[UIApplication sharedApplication] openURL:instagramURL];
                }
            }
            self.instagramSetToShare = NO;
            self.isSharingClicked = NO;
            [self.spinner_Loading stopAnimating];
        }
        if(self.messengerSetToShare) {
            NSData *videoData = [NSData dataWithContentsOfFile:videoPath];
            [FBSDKMessengerSharer shareVideo:videoData withOptions:nil];
            
            self.messengerSetToShare = NO;
            self.isSharingClicked = NO;
            [self.spinner_Loading stopAnimating];
        }
    }
    else {
        NSLog(@"StayLog: Error in video:didFinishSavingWithError:contextInfo with message: %@", error.localizedDescription);
        self.isSharingClicked = NO;
        [self.spinner_Loading stopAnimating];
    }
}

#pragma mark - Download Film
-(void)downloadFilm{
    
    //Getting the path of the document directory
    NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
    NSURL *dataPath = [documentsDirectoryURL URLByAppendingPathComponent:@"StayFilms"];
    
    //Create Stayfilm folder if it does not exist
    NSError *error;
    if (![[NSFileManager defaultManager] fileExistsAtPath:[dataPath path]])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:[dataPath path] withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
    }
    
    NSURL *fullURL = [dataPath URLByAppendingPathComponent:[NSString stringWithFormat:@"%@.mp4",self.movie.idMovie]];
    
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:[fullURL path]])
    {
        ProgressBarViewController *progressView = [[ProgressBarViewController alloc] init];
        progressView.downloadProgressBar.progress = 0;
        progressView.lbl_downloading.text = NSLocalizedString(@"DOWNLOADING", nil);
        progressView.delegate = self;
        
        
        progressView.transitioningDelegate = self;
        progressView.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        progressView.view.frame = [UIScreen mainScreen].bounds;
        self.transitionAnimation.duration = 0.1;
        self.transitionAnimation.isPresenting = YES;
        self.transitionAnimation.originFrame = [UIScreen mainScreen].bounds;
        
        [self presentViewController:progressView animated:YES completion:nil];
        
        
        
        
        [self downloadVideoFromURL:self.movie.videoUrl withProgress:^(CGFloat progress) {
            
            runOnMainQueueWithoutDeadlocking(^{
                
                //NSLog(@"%f", progress * 100);
                progressView.progressLabel.text = [NSString stringWithFormat:@"%2.0f%%", (progress * 100)];
                progressView.downloadProgressBar.progress = progress ;
                
                if (progress >= 1.0){
                    progressView.lbl_downloading.text = NSLocalizedString(@"DOWNLOADED", nil);
                    [progressView dismissViewControllerAnimated:YES completion:nil];
                    
                }
            });
            
        } completion:^(NSURL *filePath) {
            
            
            
            NSString *collectionTitle = @"Stayfilm";
            // Find the album
            PHFetchOptions *fetchOptions = [[PHFetchOptions alloc] init];
            fetchOptions.predicate = [NSPredicate predicateWithFormat:@"title = %@", collectionTitle];
            PHAssetCollection *collection = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum subtype:PHAssetCollectionSubtypeAny options:fetchOptions].firstObject;
            
            // Combine (possible) album creation and adding in one change block
            [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
                PHAssetCollectionChangeRequest *albumRequest;
                if (collection == nil) {
                    // create the album if it doesn't exist
                    albumRequest = [PHAssetCollectionChangeRequest creationRequestForAssetCollectionWithTitle:collectionTitle];
                } else {
                    // otherwise request to change the existing album
                    albumRequest = [PHAssetCollectionChangeRequest changeRequestForAssetCollection:collection];
                }
                // add the asset to the album
                [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
                    PHAssetChangeRequest *assetChangeRequest = [PHAssetChangeRequest creationRequestForAssetFromVideoAtFileURL:filePath];
                    
                    // add asset
                    PHAssetCollectionChangeRequest *assetCollectionChangeRequest = [PHAssetCollectionChangeRequest changeRequestForAssetCollection:collection];
                    [assetCollectionChangeRequest addAssets:@[[assetChangeRequest placeholderForCreatedAsset]]];
                    PHObjectPlaceholder *placeholder = assetChangeRequest.placeholderForCreatedAsset;
                   self.movieFileInPhotosID = [placeholder localIdentifier];
                    
                    
                } completionHandler:^(BOOL success, NSError *error) {
                    if (!success) {
                        NSLog(@"Error: %@", error);
                    }else{
                        self.downloading = NO;
                        [self showAlreadyDownloaded];
                        
                        if(self.instagramSetToShare){
                            self.movieFileInPhotos = [filePath path];
                            [self shareInstagram];
                        }else if (self.downloadIsSelected){
                            self.downloadIsSelected = NO;
                          dispatch_async(dispatch_get_main_queue(), ^{
                            [self.delegate performSelector:@selector(decidedOption:) withObject:@"done"];
                          });
                        }else if (self.messengerSetToShare){
                            self.movieFileInPhotos = [filePath path];
                            [self shareInMessenger];
                        }
                    }
                }];
                
            } completionHandler:^(BOOL success, NSError *error) {
                if (!success) {
                    NSLog(@"error creating or adding to album: %@", error);
                }
            }];
            
            
            
            
            
        } onError:^(NSError *error) {
            NSLog(@"StayLog: didselectdownloadvideo %@", error);
        }];
        
    }else{
        [self showAlreadyDownloaded];
        
        
        if(self.instagramSetToShare){
            
            
            [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
                PHAssetChangeRequest *assetChangeRequest = [PHAssetChangeRequest creationRequestForAssetFromVideoAtFileURL:[NSURL URLWithString:[fullURL path]]];
            
                PHObjectPlaceholder *placeholder = assetChangeRequest.placeholderForCreatedAsset;
                self.movieFileInPhotosID = [placeholder localIdentifier];
                
            } completionHandler:^(BOOL success, NSError *error) {
                if (!success) {
                    NSLog(@"Error: %@", error);
                }else{
                    self.movieFileInPhotos = [fullURL path];
                    [self shareInstagram];
                }
            }];
            
           
        }else if (self.downloadIsSelected){
            self.downloadIsSelected = NO;
            [self.delegate performSelector:@selector(decidedOption:) withObject:@"done"];
        }else if (self.messengerSetToShare){
            self.movieFileInPhotos = [fullURL path];
            [self shareInMessenger];
            
        }else{
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ALREADY_DOWNLOADED", nil) message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:nil];
        
        [alert addAction:defaultAction];
        
        
        [self presentViewController:alert animated:YES completion:nil];
        }
    }
    
    
}




- (void) downloadVideoFromURL: (NSString *) URL withProgress:(void (^)(CGFloat progress))progressBlock completion:(void (^)(NSURL *filePath))completionBlock onError:(void (^)(NSError *error))errorBlock
{
    //Configuring the session manager
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    self.manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    //Most URLs I come across are in string format so to convert them into an NSURL and then instantiate the actual request
    NSURL *formattedURL = [NSURL URLWithString:URL];
    NSURLRequest *request = [NSURLRequest requestWithURL:formattedURL];
    
    //Watch the manager to see how much of the file it's downloaded
    [self.manager setDownloadTaskDidWriteDataBlock:^(NSURLSession *session, NSURLSessionDownloadTask *downloadTask, int64_t bytesWritten, int64_t totalBytesWritten, int64_t totalBytesExpectedToWrite) {
        //Convert totalBytesWritten and totalBytesExpectedToWrite into floats so that percentageCompleted doesn't get rounded to the nearest integer
        CGFloat written = totalBytesWritten;
        CGFloat total = totalBytesExpectedToWrite;
        CGFloat percentageCompleted = written/total;
        
        //Return the completed progress so we can display it somewhere else in app
        progressBlock(percentageCompleted);
    }];
    
    //Start the download
    self.downloadTask = [self.manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        
        //Getting the path of the document directory
        NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
        NSURL *dataPath = [documentsDirectoryURL URLByAppendingPathComponent:@"StayFilms"];
        
        //Create Stayfilm folder if it does not exist
        NSError *error;
        if (![[NSFileManager defaultManager] fileExistsAtPath:[dataPath path]])
        {
            [[NSFileManager defaultManager] createDirectoryAtPath:[dataPath path] withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
        }
        
        NSURL *fullURL = [dataPath URLByAppendingPathComponent:[NSString stringWithFormat:@"%@.mp4",self.movie.idMovie]];
        
        self.downloadVideopath = fullURL;
        
        //        //If we already have a video file saved, remove it from the phone
        //        [self removeVideoAtPath:fullURL];
        return fullURL;
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        if (!error) {
            //If there's no error, return the completion block
            
            completionBlock(filePath);
        } else {
            //Otherwise return the error block
            errorBlock(error);
        }
        
    }];
    
    [self.downloadTask resume];
}
- (void)removeVideoAtPath:(NSURL *)filePath
{
    NSString *stringPath = filePath.path;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:stringPath]) {
        [fileManager removeItemAtPath:stringPath error:NULL];
    }
}


@end
