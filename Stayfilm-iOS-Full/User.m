      //
//  User.m
//  Stayfilm for Messenger
//
//  Created by Henrique Ormonde on 08/05/15.
//  Copyright (c) 2015 Stayfilm. All rights reserved.
//

#import "User.h"
#import "SFConfig.h"
#import "AppDelegate.h"
#import <OneSignal/OneSignal.h>

@interface User()

@end

@implementation User

@synthesize idUser, username, firstName, lastName, fullName, email, gender, lastAccess, created, photo, birthday, country, city, locale, languages, idCoverMovie, idFacebook, likeCount, role, friendCount, countPendingRequests, countPendingMovies, movieCount, notifications, networks, userExists, isLogged, isResponseError, responseErrorMessage, idSession;


+ (id)customClassWithProperties:(NSDictionary *)properties {
    @try {
        if(properties != nil)
        {
            NSMutableDictionary *filteredObjs = [[NSMutableDictionary alloc] init];
            for (NSString* key in properties[@"user"])
            {
                id value = [properties[@"user"] objectForKey:key];
                if ([key isEqualToString:@"idUser"]) {
                    [filteredObjs setValue:value forKey:@"idUser"];
                }
                else if ([key isEqualToString:@"username"]) {
                    [filteredObjs setValue:value forKey:@"username"];
                }
                else if ([key isEqualToString:@"firstName"]) {
                    if([value isKindOfClass:[NSNumber class]]) {
                        NSString *aux = nil;
                        [filteredObjs setValue:aux forKey:@"firstName"];
                    } else {
                        [filteredObjs setValue:value forKey:@"firstName"];
                    }
                }
                else if ([key isEqualToString:@"lastName"]) {
                    if([value isKindOfClass:[NSNumber class]]) {
                        NSString *aux = nil;
                        [filteredObjs setValue:aux forKey:@"lastName"];
                    } else {
                        [filteredObjs setValue:value forKey:@"lastName"];
                    }
                }
                else if ([key isEqualToString:@"fullName"]) {
                    [filteredObjs setValue:value forKey:@"fullName"];
                }
                else if ([key isEqualToString:@"email"]) {
                    [filteredObjs setValue:value forKey:@"email"];
                }
                else if ([key isEqualToString:@"gender"]) {
                    [filteredObjs setValue:value forKey:@"gender"];
                }
                else if ([key isEqualToString:@"lastAccess"]) {
                    [filteredObjs setValue:value forKey:@"lastAccess"];
                }
                else if ([key isEqualToString:@"photo"]) {
                    [filteredObjs setValue:value forKey:@"photo"];
                }
                else if ([key isEqualToString:@"birthday"]) {
                    [filteredObjs setValue:value forKey:@"birthday"];
                }
                else if ([key isEqualToString:@"country"]) {
                    [filteredObjs setValue:value forKey:@"country"];
                }
                else if ([key isEqualToString:@"city"]) {
                    [filteredObjs setValue:value forKey:@"city"];
                }
                else if ([key isEqualToString:@"locale"]) {
                    [filteredObjs setValue:value forKey:@"locale"];
                }
                else if ([key isEqualToString:@"languages"]) {
                    [filteredObjs setValue:value forKey:@"languages"];
                }
                else if ([key isEqualToString:@"idCoverMovie"]) {
                    [filteredObjs setValue:value forKey:@"idCoverMovie"];
                }
                else if ([key isEqualToString:@"idFacebook"]) {
                    [filteredObjs setValue:value forKey:@"idFacebook"];
                }
                else if ([key isEqualToString:@"likeCount"]) {
                    [filteredObjs setValue:value forKey:@"likeCount"];
                }
                else if ([key isEqualToString:@"role"]) {
                    [filteredObjs setValue:value forKey:@"role"];
                }
                else if ([key isEqualToString:@"friendCount"]) {
                    [filteredObjs setValue:value forKey:@"friendCount"];
                }
                else if ([key isEqualToString:@"countPendingRequests"]) {
                    [filteredObjs setValue:value forKey:@"countPendingRequests"];
                }
                else if ([key isEqualToString:@"countPendingMovies"]) {
                    [filteredObjs setValue:value forKey:@"countPendingMovies"];
                }
                else if ([key isEqualToString:@"movieCount"]) {
                    [filteredObjs setValue:value forKey:@"movieCount"];
                }
                else if ([key isEqualToString:@"notifications"]) {
                    [filteredObjs setValue:value forKey:@"notifications"];
                }
                else if ([key isEqualToString:@"networks"]) {
                    [filteredObjs setValue:value forKey:@"networks"];
                }
                else if ([key isEqualToString:@"created"]) {
                    [filteredObjs setValue:value forKey:@"created"];
                }
                else if ([key isEqualToString:@"notifications"]) {
                    [filteredObjs setValue:value forKey:@"notifications"];
                }
            }
            for (NSString* key in properties)
            {
                id value = [properties objectForKey:key];
                if ([key isEqualToString:@"idSession"]) {
                    [filteredObjs setValue:value forKey:@"idSession"];
                }
            }
            return [[self alloc] initWithProperties:filteredObjs];
        }
        else
        {
            return nil;
        }

    } @catch (NSException *exception) {
        NSLog(@"StayLog: EXCEPTION in User customClassWithProperties with error: %@", exception.description);
    }
}

- (id)initWithProperties:(NSDictionary *)properties {
    if (self = [self init]) {
        @try {
            [self setValuesForKeysWithDictionary:properties];
        }
        @catch (NSException *ex) {
            return nil;
        }
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.idUser forKey:@"USER_idUser"];
    [aCoder encodeObject:self.username forKey:@"USER_username"];
    [aCoder encodeObject:self.firstName forKey:@"USER_firstName"];
    [aCoder encodeObject:self.lastName forKey:@"USER_lastName"];
    [aCoder encodeObject:self.fullName forKey:@"USER_fullName"];
    [aCoder encodeObject:self.email forKey:@"USER_email"];
    [aCoder encodeObject:self.gender forKey:@"USER_gender"];
    [aCoder encodeObject:self.lastAccess forKey:@"USER_lastAccess"];
    [aCoder encodeObject:self.created forKey:@"USER_created"];
    [aCoder encodeObject:self.photo forKey:@"USER_photo"];
    [aCoder encodeObject:self.birthday forKey:@"USER_birthday"];
    [aCoder encodeObject:self.country forKey:@"USER_country"];
    [aCoder encodeObject:self.city forKey:@"USER_city"];
    [aCoder encodeObject:self.locale forKey:@"USER_locale"];
    [aCoder encodeObject:self.languages forKey:@"USER_languages"];
    [aCoder encodeObject:self.idCoverMovie forKey:@"USER_idCoverMovie"];
    [aCoder encodeObject:self.idFacebook forKey:@"USER_idFacebook"];
    [aCoder encodeObject:self.likeCount forKey:@"USER_likeCount"];
    [aCoder encodeObject:self.role forKey:@"USER_role"];
    [aCoder encodeObject:self.friendCount forKey:@"USER_friendCount"];
    [aCoder encodeObject:self.countPendingRequests forKey:@"USER_countPendingRequests"];
    [aCoder encodeObject:self.countPendingMovies forKey:@"USER_countPendingMovies"];
    [aCoder encodeObject:self.movieCount forKey:@"USER_movieCount"];
    [aCoder encodeObject:self.notifications forKey:@"USER_notifications"];
    [aCoder encodeObject:self.networks forKey:@"USER_networks"];
    [aCoder encodeObject:[NSNumber numberWithBool:self.userExists] forKey:@"USER_userExists"];
    [aCoder encodeObject:[NSNumber numberWithBool:self.isLogged] forKey:@"USER_isLogged"];
    [aCoder encodeObject:[NSNumber numberWithBool:self.isResponseError] forKey:@"USER_isResponseError"];
    [aCoder encodeObject:self.idSession forKey:@"USER_idSession"];
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if(self = [super init]){
        self.idUser = [aDecoder decodeObjectForKey:@"USER_idUser"];
        self.username = [aDecoder decodeObjectForKey:@"USER_username"];
        self.firstName = [aDecoder decodeObjectForKey:@"USER_firstName"];
        self.lastName = [aDecoder decodeObjectForKey:@"USER_lastName"];
        self.fullName = [aDecoder decodeObjectForKey:@"USER_fullName"];
        self.email = [aDecoder decodeObjectForKey:@"USER_email"];
        self.gender = [aDecoder decodeObjectForKey:@"USER_gender"];
        self.lastAccess = [aDecoder decodeObjectForKey:@"USER_lastAccess"];
        self.created = [aDecoder decodeObjectForKey:@"USER_created"];
        self.photo = [aDecoder decodeObjectForKey:@"USER_photo"];
        self.birthday = [aDecoder decodeObjectForKey:@"USER_birthday"];
        self.country = [aDecoder decodeObjectForKey:@"USER_country"];
        self.city = [aDecoder decodeObjectForKey:@"USER_city"];
        self.locale = [aDecoder decodeObjectForKey:@"USER_locale"];
        self.languages = [aDecoder decodeObjectForKey:@"USER_languages"];
        self.idCoverMovie = [aDecoder decodeObjectForKey:@"USER_idCoverMovie"];
        self.idFacebook = [aDecoder decodeObjectForKey:@"USER_idFacebook"];
        self.likeCount = [aDecoder decodeObjectForKey:@"USER_likeCount"];
        self.role = [aDecoder decodeObjectForKey:@"USER_role"];
        self.friendCount = [aDecoder decodeObjectForKey:@"USER_friendCount"];
        self.countPendingRequests = [aDecoder decodeObjectForKey:@"USER_countPendingRequests"];
        self.countPendingMovies = [aDecoder decodeObjectForKey:@"USER_countPendingMovies"];
        self.movieCount = [aDecoder decodeObjectForKey:@"USER_movieCount"];
        self.notifications = [aDecoder decodeObjectForKey:@"USER_notifications"];
        self.networks = (NSMutableArray*)[aDecoder decodeObjectForKey:@"USER_networks"];
        self.idSession = [aDecoder decodeObjectForKey:@"USER_idSession"];
        self.userExists = ((NSNumber*)[aDecoder decodeObjectForKey:@"USER_userExists"]).boolValue;
        self.isLogged = ((NSNumber*)[aDecoder decodeObjectForKey:@"USER_isLogged"]).boolValue;
        self.isResponseError = ((NSNumber*)[aDecoder decodeObjectForKey:@"USER_isResponseError"]).boolValue;
    }
    return self;
}


+(User *)loginWithUsername:(NSString *)p_username withPassword:(NSString *)p_password andSFAppSingleton:(StayfilmApp *)p_sfSingleton
{
    @try {
        User *result = [[User alloc] init];
        StayWS *ws = [[StayWS alloc] init];
        
        NSDictionary *args = nil;
        NSDictionary *response = nil;
        NSArray *pathArgs = nil;
        
        OSPermissionSubscriptionState* status = [OneSignal getPermissionSubscriptionState];
        
        NSDictionary *languageDic = [NSLocale componentsFromLocaleIdentifier:[[NSLocale preferredLanguages] objectAtIndex:0]];
        NSString *lang = [languageDic objectForKey:@"kCFLocaleLanguageCodeKey"];
        
        if(p_sfSingleton.settings == nil) {
            p_sfSingleton.settings = [[NSMutableDictionary alloc] init];
        }
        
        args = @{ @"username": p_username,
                  @"password": p_password,
                  @"lang": lang,
                  @"iddevice": ((p_sfSingleton.settings[@"pushIdDevice"] != nil && ![p_sfSingleton.settings[@"pushIdDevice"] isEqualToString:@""])?
                                p_sfSingleton.settings[@"pushIdDevice"] :
                                (status.subscriptionStatus.userId != nil && ![status.subscriptionStatus.userId isEqualToString:@""])?
                                status.subscriptionStatus.userId :
                                @""),
                //  @"iddevice" : [[[UIDevice currentDevice] identifierForVendor] UUIDString], //[[UIDevice currentDevice] model],
                  @"app_version_ios" : [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] };
        
        response = [ws requestWebServiceWithURL:[p_sfSingleton.wsConfig getConfigPathWithKey:@"login"] withRequestType:@"POST" withPathArguments:pathArgs withArguments:args withIdSession:nil withCache:NO];
        
        NSHTTPURLResponse *statusResponse = (NSHTTPURLResponse *) ws.responseMessage;
        if(response != nil && (statusResponse.statusCode == 200 || statusResponse.statusCode == 201))
        {
            if(response[@"user"] != nil)
            {
                result = [User customClassWithProperties:response];
            }
            if(response[@"user"] != nil && result != nil)
            {
                result.userExists = [response[@"userExists"] isEqualToNumber:@1];
                if (result.userExists)
                {
                    result.idSession = (NSString *)response[@"idSession"];
                    result.isLogged = YES;
                    result.isResponseError = NO;
                    
                    if(result.idSession != nil) {
                        [p_sfSingleton.settings setObject:(NSString *)response[@"idSession"] forKey:@"idSession"];
                    }
                    [p_sfSingleton saveSettings];
                }
                else
                {
                    result.idSession = nil;
                    result.isLogged = NO;
                    result.isResponseError = NO;
                }
            }
            return result;
        }
        else {
            result.idSession = nil;
            result.isLogged = NO;
            result.isResponseError = YES;
            result.responseErrorMessage = response[@"friendlyMessage"];
        }
        
        return result;
    }
    @catch (NSException *ex)
    {
        NSLog(@"StayLog: EXCEPTION in User loginWithUsername andPassword with error: %@", ex.description);
        return nil;
    }
}


+(User *)loginWithIdSession:(NSString *)p_idSession andSFAppSingleton:(StayfilmApp *)p_sfSingleton
{
    @try {
        User *result = [[User alloc] init];
        StayWS *ws = [[StayWS alloc] init];
        
        NSDictionary *args = nil;
        NSDictionary *response = nil;
        NSArray *pathArgs = nil;
        
        NSDictionary *languageDic = [NSLocale componentsFromLocaleIdentifier:[[NSLocale preferredLanguages] objectAtIndex:0]];
        NSString *lang = [languageDic objectForKey:@"kCFLocaleLanguageCodeKey"];
        
        OSPermissionSubscriptionState* status = [OneSignal getPermissionSubscriptionState];
        
        if(p_sfSingleton.settings == nil) {
            p_sfSingleton.settings = [[NSMutableDictionary alloc] init];
        }
        
        args = @{ @"include": @"user-bare",
                  @"lang": lang,
                  @"iddevice": ((p_sfSingleton.settings[@"pushIdDevice"] != nil && ![p_sfSingleton.settings[@"pushIdDevice"] isEqualToString:@""])?
                                p_sfSingleton.settings[@"pushIdDevice"] :
                                (status.subscriptionStatus.userId != nil && ![status.subscriptionStatus.userId isEqualToString:@""])?
                                status.subscriptionStatus.userId :
                                @""),
                  //@"iddevice" : [[[UIDevice currentDevice] identifierForVendor] UUIDString], //[[UIDevice currentDevice] model],
                  @"app_version_ios" : [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] };
        
        response = [ws requestWebServiceWithURL:[p_sfSingleton.wsConfig getConfigPathWithKey:@"login"] withRequestType:@"POST" withPathArguments:pathArgs withArguments:args withIdSession:p_idSession withCache:NO];
        
        NSHTTPURLResponse *statusResponse = (NSHTTPURLResponse *) ws.responseMessage;
        if(response != nil && (statusResponse.statusCode == 200 || statusResponse.statusCode == 201))
        {
            if(response[@"userExists"] != nil && response[@"user"] != nil)
            {
                result = [User customClassWithProperties:response];
                result.userExists = [response[@"userExists"] isEqualToNumber:@1];
                if (result.userExists)
                {
                    result.idSession = (NSString *)response[@"idSession"];
                    result.isLogged = YES;
                    result.isResponseError = NO;
                    
                    [p_sfSingleton.settings setObject:(NSString *)response[@"idSession"] forKey:@"idSession"];
                    [p_sfSingleton saveSettings];
                }
                else
                {
                    result.idSession = nil;
                    result.isLogged = NO;
                    result.isResponseError = NO;
                }
            }
            return result;
        }
        else {
            result.idSession = nil;
            result.isLogged = NO;
            result.isResponseError = YES;
            result.responseErrorMessage = response[@"friendlyMessage"];
        }
        
        return result;
    }
    @catch (NSException *ex)
    {
        NSLog(@"StayLog: EXCEPTION in User loginWithIdSession with error: %@", ex.description);
        return nil;
    }
}


+(User *)loginWithFacebook:(NSString *)p_token andSFAppSingleton:(StayfilmApp *)p_sfSingleton
{
    @try {
        User *result = [[User alloc] init];
        StayWS *ws = [[StayWS alloc] init];
        
        NSDictionary *args = nil;
        NSDictionary *response = nil;
        NSArray *pathArgs = nil;
        
        NSDictionary *languageDic = [NSLocale componentsFromLocaleIdentifier:[[NSLocale preferredLanguages] objectAtIndex:0]];
        NSString *lang = [languageDic objectForKey:@"kCFLocaleLanguageCodeKey"];
        
        OSPermissionSubscriptionState* status = [OneSignal getPermissionSubscriptionState];
        
        if(p_sfSingleton.settings == nil) {
            p_sfSingleton.settings = [[NSMutableDictionary alloc] init];
        }
        
        args = @{ @"facebookToken": p_token,
                  @"appid" : p_sfSingleton.wsConfig.facebookID,
                  @"lang": lang,
                  @"iddevice" : ((p_sfSingleton.settings[@"pushIdDevice"] != nil && ![p_sfSingleton.settings[@"pushIdDevice"] isEqualToString:@""])?
                                 p_sfSingleton.settings[@"pushIdDevice"] :
                                 (status.subscriptionStatus.userId != nil && ![status.subscriptionStatus.userId isEqualToString:@""])?
                                 status.subscriptionStatus.userId :
                                 @""),
                 // @"iddevice" : [[[UIDevice currentDevice] identifierForVendor] UUIDString], //[[UIDevice currentDevice] model],
                  @"app_version_ios" : [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] };
        
        response = [ws requestWebServiceWithURL:[p_sfSingleton.wsConfig getConfigPathWithKey:@"login"] withRequestType:@"POST" withPathArguments:pathArgs withArguments:args withIdSession:nil withCache:NO];
        
        NSHTTPURLResponse *statusResponse = (NSHTTPURLResponse *) ws.responseMessage;
        if(response != nil && (statusResponse.statusCode == 200 || statusResponse.statusCode == 201))
        {
            if(response[@"userExists"] != nil)
            {
                result.userExists = [response[@"userExists"] isEqualToNumber:@1];
                if (result.userExists && response[@"user"] != nil)
                {
                    result = [User customClassWithProperties:response];
                    result.idSession = (NSString *)response[@"idSession"];
                    result.isLogged = YES;
                    result.userExists = YES;
                    result.isResponseError = NO;
                    if(p_sfSingleton.settings[@"idSession"] == nil || [p_sfSingleton.settings[@"idSession"] isEqualToString:@""] || ![p_sfSingleton.settings[@"idSession"] isEqualToString:result.idSession]) {
                        if(result.idSession != nil) [p_sfSingleton.settings setObject:result.idSession forKey:@"idSession"];
                        if(result.idUser != nil) [p_sfSingleton.settings setObject:result.idUser forKey:@"idUser"];
                        if(result.fullName != nil) [p_sfSingleton.settings setObject:result.fullName forKey:@"USER_Name"];
                        if(result.photo != nil) [p_sfSingleton.settings setObject:result.photo forKey:@"USER_Photo"];
                    }
                    if(p_token != nil) {
                        [p_sfSingleton.settings setObject:p_token forKey:@"facebookToken"];
                    }
//                    [p_sfSingleton.settings setObject:@"1" forKey:@"CHECK_Login"];
                    [p_sfSingleton saveSettings];
                    
                    //TODO: save facebook token in stayfilm?
                    //[User ]
                }
                else
                {
                    result.idSession = nil;
                    result.isLogged = NO;
                    result.isResponseError = NO;
                }
            }
        }
        else {
            result.idSession = nil;
            result.isLogged = NO;
            result.isResponseError = YES;
            result.responseErrorMessage = response[@"friendlyMessage"];
        }
        
        return result;
    }
    @catch (NSException *ex)
    {
        NSLog(@"StayLog: EXCEPTION in User loginWithFacebook with error: %@", ex.description);
        return nil;
    }

}

+(User *)loginWithFacebookWithoutUserOverride:(NSString *)p_token andSFAppSingleton:(StayfilmApp *)p_sfSingleton
{
    @try {
        User *result = [[User alloc] init];
        StayWS *ws = [[StayWS alloc] init];
        
        NSDictionary *args = nil;
        NSDictionary *response = nil;
        NSArray *pathArgs = nil;
        
        NSDictionary *languageDic = [NSLocale componentsFromLocaleIdentifier:[[NSLocale preferredLanguages] objectAtIndex:0]];
        NSString *lang = [languageDic objectForKey:@"kCFLocaleLanguageCodeKey"];
        
        OSPermissionSubscriptionState* status = [OneSignal getPermissionSubscriptionState];
        
        if(p_sfSingleton.settings == nil) {
            p_sfSingleton.settings = [[NSMutableDictionary alloc] init];
        }
        
        args = @{ @"facebookToken": p_token,
                  @"appid" : p_sfSingleton.wsConfig.facebookID,
                  @"lang": lang,
                  @"iddevice" : ((p_sfSingleton.settings[@"pushIdDevice"] != nil && ![p_sfSingleton.settings[@"pushIdDevice"] isEqualToString:@""])?
                                 p_sfSingleton.settings[@"pushIdDevice"] :
                                 (status.subscriptionStatus.userId != nil && ![status.subscriptionStatus.userId isEqualToString:@""])?
                                 status.subscriptionStatus.userId :
                                 @""),
                  // @"iddevice" : [[[UIDevice currentDevice] identifierForVendor] UUIDString], //[[UIDevice currentDevice] model],
                  @"app_version_ios" : [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] };
        
        response = [ws requestWebServiceWithURL:[p_sfSingleton.wsConfig getConfigPathWithKey:@"login"] withRequestType:@"POST" withPathArguments:pathArgs withArguments:args withIdSession:nil withCache:NO];
        
        NSHTTPURLResponse *statusResponse = (NSHTTPURLResponse *) ws.responseMessage;
        if(response != nil && (statusResponse.statusCode == 200 || statusResponse.statusCode == 201))
        {
            if(response[@"userExists"] != nil)
            {
                result.userExists = [response[@"userExists"] isEqualToNumber:@1];
                if (result.userExists && response[@"user"] != nil)
                {
                    result = [User customClassWithProperties:response];
                    result.idSession = (NSString *)response[@"idSession"];
                    result.isLogged = YES;
                    result.userExists = YES;
                    result.isResponseError = NO;
                    
                    if(p_token != nil) {
                        [p_sfSingleton.settings setObject:p_token forKey:@"facebookToken"];
                    }
                }
                else
                {
                    result.idSession = nil;
                    result.isLogged = NO;
                    result.isResponseError = NO;
                }
            }
        }
        else {
            result.idSession = nil;
            result.isLogged = NO;
            result.isResponseError = YES;
            result.responseErrorMessage = response[@"friendlyMessage"];
        }
        
        return result;
    }
    @catch (NSException *ex)
    {
        NSLog(@"StayLog: EXCEPTION in User loginWithFacebook with error: %@", ex.description);
        return nil;
    }
    
}


+(User *)registerUserWithArguments:(NSMutableDictionary *)p_args andSFAppSingleton:(StayfilmApp *)p_sfSingleton
{
    @try {
        User *result = [[User alloc] init];
        StayWS *ws = [[StayWS alloc] init];
        OSPermissionSubscriptionState* status = [OneSignal getPermissionSubscriptionState];

        NSDictionary *response = nil;
        NSArray *pathArgs = nil;
        NSDictionary *languageDic = [NSLocale componentsFromLocaleIdentifier:[[NSLocale preferredLanguages] objectAtIndex:0]];
        NSString *lang = [languageDic objectForKey:@"kCFLocaleLanguageCodeKey"];
        
        [p_args setObject:lang forKey:@"lang"];
        
        if(p_sfSingleton.wsConfig.facebookID != nil)
            [p_args setObject:p_sfSingleton.wsConfig.facebookID forKey:@"appid"];
        
        [p_args setObject:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] forKey:@"app_version_ios"];
        
        if((p_sfSingleton.settings[@"pushIdDevice"] != nil && ![p_sfSingleton.settings[@"pushIdDevice"] isEqualToString:@""]) || (status.subscriptionStatus.userId != nil && ![status.subscriptionStatus.userId isEqualToString:@""]))
        {
            if(p_sfSingleton.settings[@"pushIdDevice"] != nil && ![p_sfSingleton.settings[@"pushIdDevice"] isEqualToString:@""])
                [p_args setObject:p_sfSingleton.settings[@"pushIdDevice"] forKey:@"iddevice"];
            else
                [p_args setObject:status.subscriptionStatus.userId forKey:@"iddevice"];
        }
        
        response = [ws requestWebServiceWithURL:[p_sfSingleton.wsConfig getConfigPathWithKey:@"userRegister"] withRequestType:@"POST" withPathArguments:pathArgs withArguments:p_args withIdSession:p_sfSingleton.settings[@"idSession"] withCache:NO];
        
        NSHTTPURLResponse *statusResponse = (NSHTTPURLResponse *) ws.responseMessage;
        if(response != nil && (statusResponse.statusCode == 200 || statusResponse.statusCode == 201))
        {
            if(response[@"idSession"] != nil && response[@"user"] != nil)
            {
                result = [User customClassWithProperties:response];
                result.idSession = (NSString *)response[@"idSession"];
                result.isLogged = YES;
                result.isResponseError = NO;
                result.userExists = YES;
                if(result.idSession != nil) [p_sfSingleton.settings setObject:result.idSession forKey:@"idSession"];
                if(result.idUser != nil) [p_sfSingleton.settings setObject:result.idUser forKey:@"idUser"];
                if(result.fullName != nil) [p_sfSingleton.settings setObject:result.fullName forKey:@"USER_Name"];
//                [p_sfSingleton.settings setObject:@"1" forKey:@"CHECK_Login"];
                [p_sfSingleton saveSettings];
            }
            else
            {
                result.isResponseError = NO;
            }
        }
        else {
            result.idSession = nil;
            result.isLogged = NO;
            result.isResponseError = YES;
            result.userExists = NO;
            result.responseErrorMessage = response[@"friendlyMessage"];
        }
        
        if(p_args[@"facebookToken"] != nil)
        {
            //result.isFacebookLogged = YES;
            [p_sfSingleton.settings setObject:(NSString *)p_args[@"facebookToken"] forKey:@"facebookToken"];
            [p_sfSingleton saveSettings];
        }
        
        return result;
    }
    @catch (NSException *ex)
    {
        NSLog(@"StayLog: EXCEPTION in registerUserWithArguments:andSFAppStayfilm with error: %@", ex.description);
        return nil;
    }
}


-(BOOL)setUserFacebookToken:(NSString *)p_token andSFAppSingleton:(StayfilmApp *)p_sfSingleton
{
    @try
    {
        StayWS *ws = [[StayWS alloc] init];
        NSDictionary *args = nil;
        NSDictionary *response = nil;
        NSArray *pathArgs = nil;
        NSString *idsession = nil;
        
        args = @{ @"token" : p_token,
                  @"appid" : p_sfSingleton.wsConfig.facebookID };

        pathArgs = [NSArray arrayWithObjects: self.idUser, @"facebook", nil ];
        
        if(p_sfSingleton.settings[@"idSession"] == nil || [p_sfSingleton.settings[@"idSession"] isEqualToString:@""]) {
            idsession = self.idSession;
        } else {
            idsession = p_sfSingleton.settings[@"idSession"];
        }
        
        response = [ws requestWebServiceWithURL:[p_sfSingleton.wsConfig getConfigPathWithKey:@"networkResource"] withRequestType:@"POST" withPathArguments:pathArgs withArguments:args withIdSession:idsession withCache:NO];
        
        NSHTTPURLResponse *statusResponse = (NSHTTPURLResponse *) ws.responseMessage;
        if(response != nil && (statusResponse.statusCode == 200 || statusResponse.statusCode == 201))
        {
            if(self.networks == nil) {
                self.networks = [[NSMutableArray alloc] init];
            }
            if(![self.networks containsObject:@"facebook"]) {
                [self.networks addObject:@"facebook"];
            }
            if(p_sfSingleton.currentUser.networks == nil) {
                p_sfSingleton.currentUser.networks = [[NSMutableArray alloc] init];
            }
            if(![p_sfSingleton.currentUser.networks containsObject:@"facebook"]) {
                [p_sfSingleton.currentUser.networks addObject:@"facebook"];
            }
            return YES;
        }
        else
        {
            return NO;
        }
    }
    @catch (NSException *ex)
    {
        NSLog(@"StayLog: EXCEPTION in User setUserFacebookToken with error: %@", ex.description);
        return NO;
    }
}

-(BOOL)setUserInstagramToken:(NSString *)p_token andSFAppSingleton:(StayfilmApp *)p_sfSingleton
{
    @try
    {
        StayWS *ws = [[StayWS alloc] init];
        NSDictionary *args = nil;
        NSDictionary *response = nil;
        NSArray *pathArgs = nil;
        
        args = @{ @"frob" : p_token };
        
        pathArgs = [NSArray arrayWithObjects: self.idUser, @"instagram", nil ];
        
        response = [ws requestWebServiceWithURL:[p_sfSingleton.wsConfig getConfigPathWithKey:@"networkResource"] withRequestType:@"POST" withPathArguments:pathArgs withArguments:args withIdSession:p_sfSingleton.settings[@"idSession"] withCache:NO];
        
        NSHTTPURLResponse *statusResponse = (NSHTTPURLResponse *) ws.responseMessage;
        if(response != nil && (statusResponse.statusCode == 200 || statusResponse.statusCode == 201))
        {
            [self.networks addObject:@"instagram"];
            return YES;
        }
        else
        {
            return NO;
        }
    }
    @catch (NSException *ex)
    {
        NSLog(@"StayLog: EXCEPTION in User setUserInstagramToken with error: %@", ex.description);
        return NO;
    }
}

-(BOOL)setUserGoogleToken:(NSString *)p_token andSFAppSingleton:(StayfilmApp *)p_sfSingleton
{
    @try
    {
        StayWS *ws = [[StayWS alloc] init];
        NSDictionary *args = nil;
        NSDictionary *response = nil;
        NSArray *pathArgs = nil;
        
        args = @{ @"frob" : p_token };
        
        pathArgs = [NSArray arrayWithObjects: self.idUser, @"gplus", nil ];
        
        response = [ws requestWebServiceWithURL:[p_sfSingleton.wsConfig getConfigPathWithKey:@"networkResource"] withRequestType:@"POST" withPathArguments:pathArgs withArguments:args withIdSession:p_sfSingleton.settings[@"idSession"] withCache:NO];
        
        NSHTTPURLResponse *statusResponse = (NSHTTPURLResponse *) ws.responseMessage;
        if(response != nil && (statusResponse.statusCode == 200 || statusResponse.statusCode == 201))
        {
            [self.networks addObject:@"gplus"];
            return YES;
        }
        else
        {
            return NO;
        }
    }
    @catch (NSException *ex)
    {
        NSLog(@"StayLog: EXCEPTION in User setUserGoogleToken with error: %@", ex.description);
        return NO;
    }
}

-(BOOL)setUserTwitterToken:(NSString *)p_token andSFAppSingleton:(StayfilmApp *)p_sfSingleton
{
    @try
    {
        StayWS *ws = [[StayWS alloc] init];
        NSDictionary *args = nil;
        NSDictionary *response = nil;
        NSArray *pathArgs = nil;
        
        args = @{ @"frob" : p_token };
        
        pathArgs = [NSArray arrayWithObjects: self.idUser, @"twitter", nil ];
        
        response = [ws requestWebServiceWithURL:[p_sfSingleton.wsConfig getConfigPathWithKey:@"networkResource"] withRequestType:@"POST" withPathArguments:pathArgs withArguments:args withIdSession:p_sfSingleton.settings[@"idSession"] withCache:NO];
        
        NSHTTPURLResponse *statusResponse = (NSHTTPURLResponse *) ws.responseMessage;
        if(response != nil && (statusResponse.statusCode == 200 || statusResponse.statusCode == 201))
        {
            [self.networks addObject:@"twitter"];
            return YES;
        }
        else
        {
            return NO;
        }
    }
    @catch (NSException *ex)
    {
        NSLog(@"StayLog: EXCEPTION in User setUserTwitterToken with error: %@", ex.description);
        return NO;
    }
}




#pragma mark - 
#pragma mark - User Information Custom Methods

+(User *)getUserWithId:(NSString *)p_idUser andSFAppSingleton:(StayfilmApp *)p_sfSingleton {
    
    @try
    {
        StayWS *ws = [[StayWS alloc] init];
        NSDictionary *args = nil;
        NSDictionary *response = nil;
        NSArray *pathArgs = nil;
        NSString *idsession = nil;
        User *user = [[User alloc] init];
        
        pathArgs = [NSArray arrayWithObjects: p_idUser, nil];
        
        if(p_sfSingleton.settings[@"idSession"] == nil || [p_sfSingleton.settings[@"idSession"] isEqualToString:@""]) {
            if(p_sfSingleton.currentUser != nil)
               idsession = p_sfSingleton.currentUser.idSession;
        } else {
            idsession = p_sfSingleton.settings[@"idSession"];
        }
        
        response = [ws requestWebServiceWithURL:[p_sfSingleton.wsConfig getConfigPathWithKey:@"getUser"] withRequestType:@"GET" withPathArguments:pathArgs withArguments:args withIdSession:idsession withCache:NO];
        
        NSHTTPURLResponse *statusResponse = (NSHTTPURLResponse *) ws.responseMessage;
        if(response != nil && (statusResponse.statusCode == 200 || statusResponse.statusCode == 201))
        {
            if(response != nil)
            {
                NSMutableDictionary *auxdic = [[NSMutableDictionary alloc] init];
                [auxdic setObject:response forKey:@"user"];
                user = [User customClassWithProperties:auxdic];
            }
            return user;
        }
        else
        {
            return nil;
        }
    }
    @catch (NSException *ex)
    {
        NSLog(@"StayLog: EXCEPTION in User getUserWithId with error: %@", ex.description);
        return nil;
    }
}

-(BOOL)getUserNetworksWithSFAppSingleton:(StayfilmApp *)p_sfSingleton
{
    @try
    {
        StayWS *ws = [[StayWS alloc] init];
        NSDictionary *args = nil;
        NSDictionary *response = nil;
        NSArray *pathArgs = nil;
        NSString *idsession = nil;
        
        args = @{ @"network" : @"1" };
        
        pathArgs = [NSArray arrayWithObjects: self.idUser, nil ];
        
        if(p_sfSingleton.settings[@"idSession"] == nil || [p_sfSingleton.settings[@"idSession"] isEqualToString:@""]) {
            idsession = self.idSession;
        } else {
            idsession = p_sfSingleton.settings[@"idSession"];
        }
        
        response = [ws requestWebServiceWithURL:[p_sfSingleton.wsConfig getConfigPathWithKey:@"getUserNetworks"] withRequestType:@"GET" withPathArguments:pathArgs withArguments:args withIdSession:idsession withCache:NO];
        
        NSHTTPURLResponse *statusResponse = (NSHTTPURLResponse *) ws.responseMessage;
        if(response != nil && (statusResponse.statusCode == 200 || statusResponse.statusCode == 201))
        {
            if(response[@"data"] != nil)
            {
                self.networks = [[NSMutableArray alloc] initWithArray:response[@"data"]];
            }
            return YES;
        }
        else
        {
            return NO;
        }
    }
    @catch (NSException *ex)
    {
        NSLog(@"StayLog: EXCEPTION in User getUserNetworks with error: %@", ex.description);
        return NO;
    }
}


-(NSString *)getRelationshipStatus:(User *)p_friend andSFAppSingleton:(StayfilmApp *)p_sfSingleton
{
    @try
    {
        StayWS *ws = [[StayWS alloc] init];
        NSDictionary *args = nil;
        NSDictionary *response = nil;
        NSArray *pathArgs = nil;
        
        args = @{ @"include" : @"friendship",
                  @"iduser" : p_friend.idUser };
        
        pathArgs = [NSArray arrayWithObjects: self.idUser, nil ];
        
        response = [ws requestWebServiceWithURL:[p_sfSingleton.wsConfig getConfigPathWithKey:@"getRelationshipStatus"] withRequestType:@"GET" withPathArguments:pathArgs withArguments:args withIdSession:p_sfSingleton.settings[@"idSession"] withCache:NO];
        
        NSHTTPURLResponse *statusResponse = (NSHTTPURLResponse *) ws.responseMessage;
        if(response != nil && (statusResponse.statusCode == 200 || statusResponse.statusCode == 201))
        {
            if(response[@"friendshipStatus"] != nil)
            {
                return [[NSString alloc] initWithString:response[@"friendshipStatus"]];
            }
        }
        
        return nil;
    }
    @catch (NSException *ex)
    {
        NSLog(@"StayLog: EXCEPTION in User getRelationshipStatus with error: %@", ex.description);
        return nil;
    }
}


-(BOOL)setRelationship:(User *)p_friend withAction:(NSString *)p_action andSFAppSingleton:(StayfilmApp *)p_sfSingleton
{
    @try
    {
        StayWS *ws = [[StayWS alloc] init];
        NSDictionary *args = nil;
        NSDictionary *response = nil;
        NSArray *pathArgs = nil;
        
        args = @{ @"idfriend" : p_friend.idUser,
                  @"action" : p_action };
        
        pathArgs = [NSArray arrayWithObjects: self.idUser, nil ];
        
        response = [ws requestWebServiceWithURL:[p_sfSingleton.wsConfig getConfigPathWithKey:@"setRelationship"] withRequestType:@"POST" withPathArguments:pathArgs withArguments:args withIdSession:p_sfSingleton.settings[@"idSession"] withCache:NO];
        
        NSHTTPURLResponse *statusResponse = (NSHTTPURLResponse *) ws.responseMessage;
        if(response != nil && (statusResponse.statusCode == 200 || statusResponse.statusCode == 201))
        {
            return YES;
        }
        else
        {
            return NO;
        }
    }
    @catch (NSException *ex)
    {
        NSLog(@"StayLog: EXCEPTION in User setRelationship with error: %@", ex.description);
        return NO;
    }
}



#pragma mark - Password Recovery calls

/// Returns nil if OK  "exception" if there's an exception or error message
+(NSString *)sendRecoveryEmail:(NSString *)p_email andSFAppSingleton:(StayfilmApp *)p_sfSingleton {
    @try
    {
        StayWS *ws = [[StayWS alloc] init];
        NSDictionary *args = nil;
        NSDictionary *response = nil;
        NSArray *pathArgs = nil;
        
        args = @{ @"email" : p_email };
        
        pathArgs = nil;
        
        response = [ws requestWebServiceWithURL:[p_sfSingleton.wsConfig getConfigPathWithKey:@"passwordRecovery"] withRequestType:@"POST" withPathArguments:pathArgs withArguments:args withIdSession:nil withCache:NO];
        
        NSHTTPURLResponse *statusResponse = (NSHTTPURLResponse *) ws.responseMessage;
        if(response != nil && (statusResponse.statusCode == 200 || statusResponse.statusCode == 201))
        {
            return nil;
        }
        else
        {
            return response[@"message"];
        }
    }
    @catch (NSException *ex)
    {
        NSLog(@"StayLog: EXCEPTION in User sendRecoveryEmail with error: %@", ex.description);
        return @"exception";
    }
}

/// Returns nil if OK  "exception" if there's an exception or error message
+(NSString *)sendRecoveryCode:(NSString *)p_code withEmail:(NSString *)p_email withNewPassword:(NSString *)p_password andSFAppSingleton:(StayfilmApp *)p_sfSingleton {
    @try
    {
        StayWS *ws = [[StayWS alloc] init];
        NSDictionary *args = nil;
        NSDictionary *response = nil;
        NSArray *pathArgs = nil;
        
        args = @{ @"idtoken" : p_code,
                  @"email": p_email,
                  @"password" : p_password,
                  @"passwordConfirm": p_password };
        
        pathArgs = nil;
        
        response = [ws requestWebServiceWithURL:[p_sfSingleton.wsConfig getConfigPathWithKey:@"passwordRecovery"] withRequestType:@"PUT" withPathArguments:pathArgs withArguments:args withIdSession:nil withCache:NO];
        
        NSHTTPURLResponse *statusResponse = (NSHTTPURLResponse *) ws.responseMessage;
        if(response != nil && (statusResponse.statusCode == 200 || statusResponse.statusCode == 201))
        {
            return nil;
        }
        else
        {
            return response[@"message"];
        }
    }
    @catch (NSException *ex)
    {
        NSLog(@"StayLog: EXCEPTION in User sendRecoveryCode with error: %@", ex.description);
        return @"exception";
    }
}

/// Returns nil if OK  "exception" if there's an exception or error message
+(NSString *)sendRecoveryToken:(NSString *)p_code withEmail:(NSString *)p_email andSFAppSingleton:(StayfilmApp *)p_sfSingleton {
    @try
    {
        StayWS *ws = [[StayWS alloc] init];
        NSDictionary *args = nil;
        NSDictionary *response = nil;
        NSArray *pathArgs = nil;
        
        args = @{ @"idtoken" : p_code,
                  @"email" : p_email };
        
        pathArgs = nil;
        
        response = [ws requestWebServiceWithURL:[p_sfSingleton.wsConfig getConfigPathWithKey:@"token"] withRequestType:@"GET" withPathArguments:pathArgs withArguments:args withIdSession:nil withCache:NO];
        
        NSHTTPURLResponse *statusResponse = (NSHTTPURLResponse *) ws.responseMessage;
        if(response != nil && (statusResponse.statusCode == 200 || statusResponse.statusCode == 201))
        {
            return nil;
        }
        else
        {
            return response[@"message"];
        }
    }
    @catch (NSException *ex)
    {
        NSLog(@"StayLog: EXCEPTION in User sendRecoveryCode with error: %@", ex.description);
        return @"exception";
    }
}

#pragma mark - Update Profile calls

+(BOOL)updateProfile:(NSString *)userName LastName:(NSString *)userLastName andSFAppSingleton:(StayfilmApp *)sfSingleton {
    @try
    {
        StayWS *ws = [[StayWS alloc] init];
        NSDictionary *args = nil;
        NSDictionary *response = nil;
        NSArray *pathArgs = nil;
        
        args = @{ @"firstName" : userName,
                  @"lastName" : userLastName };
        
        pathArgs = [NSArray arrayWithObject:sfSingleton.settings[@"idUser"]];
        
        response = [ws requestWebServiceWithURL:[sfSingleton.wsConfig getConfigPathWithKey:@"updateUser"]
                                withRequestType:@"PUT" withPathArguments:pathArgs withArguments:args withIdSession:sfSingleton.settings[@"idSession"] withCache:NO];
        
        User *user = sfSingleton.currentUser;
        
        NSHTTPURLResponse *statusResponse = (NSHTTPURLResponse *) ws.responseMessage;
        if(response != nil && (statusResponse.statusCode == 200 || statusResponse.statusCode == 201))
        {
            user.firstName = userName;
            user.lastName = userLastName;
            user.fullName = [NSString stringWithFormat:@"%@ %@",userName,userLastName];
            
            sfSingleton.currentUser = user;
            NSData *dataSave = [NSKeyedArchiver archivedDataWithRootObject:user];
            [[NSUserDefaults standardUserDefaults] setObject:dataSave forKey:@"currentUser"];
            
            return YES;
        }
        else
        {
            return NO;
        }
    }
    @catch (NSException *ex)
    {
         NSLog(@"StayLog: EXCEPTION in User updateProfile with error: %@", ex.description);
         return NO;
    }
}


-(void)validateSubscription:(subscriptionCompletion)completionBlock{
    
    NSDictionary * parameters = @{
                                  @"user_id" : self.idUser
                                 };
    
    ShellWebService *service = [ShellWebService getSharedInstance];
    [service callService:parameters endpointName:endpointValidationPurchase
     withCompletionBlock:^(NSDictionary *resultArray, NSError *error) {
         if(!error){
             NSLog(@"%@", resultArray);
             if(resultArray[@"isValidPurchase"] != nil){
                 
                 BOOL subscription;
                 
                 if([resultArray[@"isValidPurchase"] boolValue]){
                     subscription = YES;
                 }else{
                     subscription = NO;
                 }
                 
                 [[NSUserDefaults standardUserDefaults] setBool:subscription forKey:@"hasSubscription"];

                 
        
                 
                 completionBlock([resultArray[@"isValidPurchase"] boolValue]);
             }
         }
     }];
}

@end
