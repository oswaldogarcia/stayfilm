//
//  Constants.h
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 1/18/19.
//  Copyright © 2019 Stayfilm. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

/*Select Style*/
static int const kSmallIPhoneCellSize = 145;
static int const kIPhoneCellSize = 170;
static int const kIPadCellSize = 300;
static int const kIPhone5Height =  568.0;
static int const kIPadHeight =  1024.0;
static int const kTableAdjust =  300;
static int const kSelectedCell =  460;
static int const kExtraScrollValidation =  400;

/*Template*/


static int const kSelectedTemplateSmallImageHeight = 132;
static int const kSelectedTemplateImageHeight = 160;
static int const kSelectedTemplateHeightConstraint = 200.0;

#endif /* Constants_h */
