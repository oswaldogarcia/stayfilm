//
//  ProgressBarViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 16/05/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ProgressBarViewControllerDelegate <NSObject>
@required
-(void)chooseOption:(NSString*)option;
@end

@interface ProgressBarViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *but_cancel;
@property (weak, nonatomic) IBOutlet UILabel *progressLabel;
@property (weak, nonatomic) IBOutlet UILabel *lbl_downloading;
@property (weak, nonatomic) IBOutlet UIProgressView *downloadProgressBar;

@property (nonatomic, assign) BOOL isModal;

@property (nonatomic, strong) id<ProgressBarViewControllerDelegate> delegate;

-(id)initWithDelegate:(id<ProgressBarViewControllerDelegate>)p_delegate;

@end
