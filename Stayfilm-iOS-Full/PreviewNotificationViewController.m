//
//  PreviewNotificationViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 12/7/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import "PreviewNotificationViewController.h"
#import <Google/Analytics.h>

@interface PreviewNotificationViewController ()

@end

@implementation PreviewNotificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setlanguage];
}

-(void)setlanguage{

    self.savedLabel.text = NSLocalizedString(@"SAVED_FILM_LABEL", nil);
    self.verifyingLabel.text = NSLocalizedString(@"VERIFYING_FILM", nil);
    self.notificationLabel.text = NSLocalizedString(@"TOLD_NOTIFICATION", nil);
    
}

- (void)viewDidAppear:(BOOL)animated{
 
    //Google Analytics
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"share-screen_curation_film"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
}



- (IBAction)okAction:(id)sender {
    
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"film"
                                                          action:@"waiting_approval"
                                                           label:@"ok"
                                                           value:@1] build]];
    
    [self.delegate closeModal];
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
