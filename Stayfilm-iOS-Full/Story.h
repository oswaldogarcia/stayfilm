//
//  Story.h
//  Stayfilm-iOS-Full
//
//  Created by Henrique on 6/13/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Utilities.h"
#import <AVKit/AVKit.h>

@interface StoryContent : NSObject

@property (nonatomic, strong) NSNumber  *index;
@property (nonatomic, strong) NSNumber *idContent;
@property (nonatomic, strong) NSString *videoUrl;
@property (nonatomic, strong) NSString *thumbnailUrl;
@property (nonatomic, strong) NSNumber *duration;
@property (nonatomic, strong) NSString *imgUrl;
@property (nonatomic, strong) NSNumber *idGenre;
@property (nonatomic, strong) NSNumber *idTemplate;
@property (nonatomic, strong) NSString *idMovie;
@property (nonatomic, assign) StayfilmTypes_ExploreStoryContent type;
@property (nonatomic, strong) NSString *buttonText;
@property (nonatomic, strong) NSString *buttonColor;
@property (nonatomic, strong) NSString *buttonIcon;
@property (nonatomic, strong) NSString *contentDescription;
//@property (nonatomic, strong) NSNumber  *active;

+(id)customClassWithProperties:(NSDictionary *)properties;

@end

@interface Story : NSObject

@property (nonatomic, strong) NSNumber *idStory;
@property (nonatomic, assign) BOOL isTest;
@property (nonatomic, strong) NSNumber *timestamp;
@property (nonatomic, strong) NSString *thumbnailUrl;
@property (nonatomic, strong) NSString *logoUrl;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSNumber *viewedIDContent;
@property (nonatomic, assign) BOOL isStoryViewed;
@property (nonatomic, strong) NSMutableArray<StoryContent *> *content;
@property (nonatomic, strong) NSMutableArray *myPlayers;

+(id)customClassWithProperties:(NSDictionary *)properties;


+(NSDictionary *)getStories;
+(BOOL)getSavedStories;
-(void)setPlayersfromPlayer:(int)fromPlayer toPlayer:(int)toPlayer;
-(void)setPlayersWithCompletion: (void (^)(void))completion;

typedef void (^CompletionBlock)(AVPlayer* playerAsset);
-(void)setPlayer:(StoryContent*)content withCompletion:(CompletionBlock)completion;

@end



