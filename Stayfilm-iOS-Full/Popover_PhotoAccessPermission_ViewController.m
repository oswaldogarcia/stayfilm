//
//  Popover_PhotoAccessPermission_ViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Henrique on 3/20/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import "Popover_PhotoAccessPermission_ViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <Google/Analytics.h>
@implementation CALayer (Additions)

- (void)setBorderColorFromUIColor:(UIColor *)color
{
    self.borderColor = color.CGColor;
}

@end

@interface Popover_PhotoAccessPermission_ViewController ()

@property (nonatomic, assign) BOOL isOpeningSettingsClicked;

@end

@implementation Popover_PhotoAccessPermission_ViewController

@synthesize but_cancel, but_GoSettings, blur_view, contentView, isOpeningSettingsClicked;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.isOpeningSettingsClicked = NO;

}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    //Google Analytics
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"moviemaker_content_camera-roll_not-allowed"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(id)initWithDelegate:(id<Popover_PhotoPermissionAccess_ViewControllerDelegate>)p_delegate
{
    if(self = [super init]) {
        self.delegate = p_delegate;
    }
    return self;
}

- (IBAction)but_GoSettings_Clicked:(id)sender {
    if(!self.isOpeningSettingsClicked) {
        self.isOpeningSettingsClicked = YES;
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        self.isOpeningSettingsClicked = NO;
    }
}

-(void)backgroundTapped:(UIGestureRecognizer *)gesture
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)but_cancel_Clicked:(id)sender {
    [self.delegate performSelector:@selector(dismissModalMenu) withObject:nil];
}
- (IBAction)tapGesture_Clicked:(id)sender {
    [self.delegate performSelector:@selector(dismissModalMenu) withObject:nil];
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:self.contentView]) {
        return NO;
    }
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
