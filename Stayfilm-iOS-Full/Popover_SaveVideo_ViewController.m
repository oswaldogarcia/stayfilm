//
//  Popover_SaveVideo_ViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Henrique on 5/17/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import "Popover_SaveVideo_ViewController.h"

@interface Popover_SaveVideo_ViewController ()

@end

@implementation Popover_SaveVideo_ViewController

@synthesize but_Download, but_Cancel;

-(id)initWithDelegate:(id<Popover_SaveVideo_ViewControllerDelegate>)p_delegate
{
    if(self = [super init]) {
        self.delegate = p_delegate;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *modalTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOutside)];
    modalTap.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:modalTap];
    
    self.but_Cancel.layer.borderWidth = 1.0f;
    self.but_Cancel.layer.borderColor = [[UIColor colorWithRed:80.0/255.0 green:128.0/255.0 blue:146.0/255.0 alpha:1.0] CGColor];
    self.but_Cancel.layer.cornerRadius = 5;
    self.but_Cancel.clipsToBounds = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tapOutside {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)but_Download_Clicked:(id)sender {
    [self tapOutside];
    [self.delegate performSelector:@selector(chooseOption:) withObject:@"confirmDownload"];
}

- (IBAction)but_Cancel:(id)sender {
    [self tapOutside];
     [self.delegate performSelector:@selector(chooseOption:) withObject:@"noDownload"];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
