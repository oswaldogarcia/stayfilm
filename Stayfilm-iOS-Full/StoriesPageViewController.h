//
//  StoriesPageViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 13/06/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//



#import <UIKit/UIKit.h>
#import "StayfilmApp.h"
#import "ExplorerPlayerViewController.h"
#import "StoriesContentViewController.h"
#import "SFHelper.h"
#import <Foundation/Foundation.h>
//#import "SegmentedProgressBar.h"

@protocol StoryDelegate<NSObject>
- (void)makeAFilmAction;
- (void)changeLogo:(NSString*)urlImageLogo;
- (void)buttonIsEnable:(BOOL) enable;
- (void)closeButtonIsEnable:(BOOL) enable;
- (void)showSwipeUpToast:(BOOL) show;
- (void)changeButtonText:(NSString *)text Color:(UIColor *)color andIcon:(NSString*) imageString;
- (void)changeDescription:(NSString*)description;
- (void)setCurrentIDGenre:(int)p_genre andIDTemplate:(int)p_template;
- (void)setAUXNavigationController:(UINavigationController*)nav;
- (void)changedConnectivity:(BOOL)connected;

@end


@interface StoriesPageViewController : UIPageViewController <UIPageViewControllerDelegate, UIPageViewControllerDataSource,StoriesContentDelegate>

//@property (nonatomic, strong) NSMutableArray *myFilms;
@property (nonatomic, strong) NSMutableArray *storiesArray;
@property (nonatomic, strong) StayfilmApp *sfApp;
@property (nonatomic) int storySelected;
@property (nonatomic, assign) id<StoryDelegate> storyDelegate;
@property (weak, nonatomic) UINavigationController *auxNavigation;
@property (nonatomic, strong)NSMutableArray *myViewControllers;
@end
