 //
//  MovieMakerStep1ViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Henrique Ormonde on 20/09/17.
//  Copyright © 2017 Stayfilm. All rights reserved.
//

@import Photos;

#import "MovieMakerStep1ViewController.h"
#import "Step2TitleViewController.h"
#import "Step1ConfirmationViewController.h"
#import "Step3StylesViewController.h"
#import "MovieMakerSuggestionAlbumCollectionViewCell.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Google/Analytics.h>
#import "Reachability.h"
#import "PopupMenuAnimation.h"
#import "ProfileViewController.h"

#import "Job.h"
#import "AlbumSF.h"
#import "AlbumSN.h"
#import "AlbumInternal.h"
#import "Media.h"
#import "VideoFB.h"
#import "SocialAlbumsView.h"
#import "AlbumSocialCollectionViewCell.h"

#import "AlbumViewerViewController.h"
#import <CommonCrypto/CommonDigest.h>

@interface UIView (IBRotateView)
@property (nonatomic, assign) CGFloat rotation;

@end

@implementation UIView (IBRotateView)
@dynamic rotation;

- (void)setRotation:(CGFloat)deg
{
    CGFloat rad = M_PI * deg / 180.0;
    CGAffineTransform rot = CGAffineTransformMakeRotation(rad);
    [self setTransform:rot];
}

@end


@interface MovieMakerStep1ViewController ()<AutoFilmDelegate>

@property (nonatomic, weak) StayfilmApp *sfAppStep1;

@property (nonatomic, strong) Uploader* uploaderStayfilm;
@property (nonatomic, strong) NSConditionLock* lockAlbum;
@property (nonatomic, weak) AlbumViewerViewController *albumVC;

@property (nonatomic, strong) NSMutableArray *albunsFacebook;
@property (nonatomic, strong) NSMutableArray *albunsInstagram;
@property (nonatomic, strong) NSMutableArray *albunsGoogle;
@property (nonatomic, strong) NSMutableArray *albunsStayfilm;
@property (nonatomic, strong) NSMutableArray *albunsSuggestion;
@property (nonatomic, strong) NSString *facebookNextPage;
@property (nonatomic, strong) NSString *facebookVideoNextPage;
@property (nonatomic, assign) BOOL loadedAllFacebookAlbums;
@property (nonatomic, assign) BOOL calledAlbumsFistTime;
@property (nonatomic, assign) int facebookloginAttempts;
@property (nonatomic, assign) int socialLoginAttempts;

@property (nonatomic, strong) NSMutableDictionary *contentOffsetDictionary;
@property (nonatomic, assign) int indexSelectedAlbum;

@property (nonatomic, strong) NSURLSession *thumbConnection;

@property (nonatomic, assign) BOOL isSelectingAlbum;
@property (nonatomic, assign) BOOL isSelectingSocial;
@property (nonatomic, assign) BOOL isOpeningUploader;

@property(nonatomic , strong) PHFetchResult *assetsFetchResults;
@property(nonatomic , strong) PHCachingImageManager *imageManager;

@property (nonatomic, strong) UITapGestureRecognizer *cameraTapGesture;

@property (nonatomic, strong) PopupMenuAnimation *transitionAnimation;
@property (weak, nonatomic) IBOutlet UIView *loadingView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;

@property(strong, nonatomic) NSObject<OS_dispatch_semaphore> *albumSemaphore;

@property(atomic, strong) NSMutableDictionary *albumReadAttempts;

@property (strong, nonatomic) NSMutableArray *imageToAutoVideo;

-(void)reloadAlbumsCounters;

@end



@implementation MovieMakerStep1ViewController
@synthesize cameraTapGesture, camera_arrow, camera_spinner, albunsFacebook, albunsInstagram, albunsGoogle, albunsStayfilm, facebookNextPage, facebookVideoNextPage, loadedAllFacebookAlbums, uploaderStayfilm, lockAlbum, albumVC, initialEditedMedias, sfAppStep1;
@synthesize indexSelectedAlbum, thumbConnection, contentOffsetDictionary;
@synthesize assetsFetchResults, imageManager, albunsSuggestion, suggestionCollection, constraint_socialFacebook, constraint_socialInstagram, txt_Title;

static int statusFBalbums = 0;
BOOL isResizing = NO;

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
   
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [self.profileImage setUserInteractionEnabled:YES];
    [self.profileImage setExclusiveTouch:YES];
    
    [self setImageProfile];
    
    // Camera Roll
    self.camera_arrow.layer.anchorPoint = CGPointMake(0.5, 0.5);
    self.camera_arrow.transform = CGAffineTransformMakeRotation(-90 * M_PI / 180.0);

    SocialAlbumsView *facebook = (SocialAlbumsView *)[self.view viewWithTag:1];
    if(!facebook.isLoaded) {
        facebook.expandArrow.layer.anchorPoint = CGPointMake(0.5, 0.5);
        facebook.expandArrow.transform = CGAffineTransformMakeRotation(-90 * M_PI / 180.0);
    }
    
    if(self.sfAppStep1 == nil)
        self.sfAppStep1 = [StayfilmApp sharedStayfilmAppSingleton];
    
    [self.sfAppStep1.currentUser validateSubscription:^(BOOL hasSubscription) {
        
        self.sfAppStep1.currentUser.hasSubscription = hasSubscription;
        BOOL subscription;
        
        if(hasSubscription){
            subscription = YES;
        }else{
            subscription = NO;
        }
        
        [[NSUserDefaults standardUserDefaults] setBool:subscription forKey:@"hasSubscription"];
    }];
    
    if(self.sfAppStep1.isEditingMode) {
        [self.view_Editing setHidden:NO];
        UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
        
        if([statusBar respondsToSelector:@selector(setBackgroundColor:)]){
            statusBar.backgroundColor = [UIColor colorWithRed:0.20 green:0.34 blue:0.44 alpha:1.0];
            self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
        }
    }else{
        UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
        
        if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
            statusBar.backgroundColor = [UIColor clearColor];
        }
    }
    
    NSString *langcode = [[NSLocale currentLocale] objectForKey:NSLocaleLanguageCode];
    
    if([langcode isEqualToString:@"pt"]){
        self.tapToOpenImage.image = [UIImage imageNamed:@"messageCollapsedPortuguese"];
    }else if([langcode isEqualToString:@"es"]){
        self.tapToOpenImage.image = [UIImage imageNamed:@"messageCollapsedSpanish"];
    }

   self.autoFilmView.alpha = 0;
   self.autoFilmConstraint.constant = 0;

//   self.autoFilm = [[AutoFilm alloc]init];
//   self.autoFilm.delegate = self;
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
  
    [self.sfAppStep1.filmStrip connectivityViewUpdate];
}

- (void)viewDidLoad{
    [super viewDidLoad];

    self.automaticallyAdjustsScrollViewInsets = NO;
    
    //Bypass Audio Lock
    [self performSelectorInBackground:@selector(bypassAudioLock) withObject:nil];
    
    self.assetsFetchResults = nil;
    self.imageManager = nil;
    
    self.transitionAnimation = [[PopupMenuAnimation alloc] init];
    
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:self.txt_Title.text];
    //the value paramenter defines your spacing amount, and range is the range of characters in your string the spacing will apply to. Here we want it to apply to the whole string so we take it from 0 to text.length.
    [text addAttribute:NSKernAttributeName value:[NSNumber numberWithDouble:2.0] range:NSMakeRange(0, text.length)];
    [self.txt_Title setAttributedText:text];

    self.isOpeningUploader = NO;
    self.isSelectingAlbum = NO;
    
    self.albunsFacebook = [[NSMutableArray alloc] init];
    self.albunsInstagram = [[NSMutableArray alloc] init];
    self.albunsGoogle = [[NSMutableArray alloc] init];
    self.albunsStayfilm = [[NSMutableArray alloc] init];
    self.loadedAllFacebookAlbums = NO;
    self.facebookNextPage = nil;
    self.facebookVideoNextPage = nil;
    
    self.albumReadAttempts = [[NSMutableDictionary alloc] init];
    
    self.cameraTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cameraTapped:)];
    self.cameraTapGesture.numberOfTapsRequired = 1;
    [self.cameraTapGesture setCancelsTouchesInView:NO];
    [self.but_cameraRoll addGestureRecognizer:self.cameraTapGesture];
    
    //seting facebook albums
    SocialAlbumsView *facebook = (SocialAlbumsView *)[self.view viewWithTag:1];
    facebook.txt_Title.font = [UIFont fontWithName:@"ProximaNova-Regular" size:22];
    facebook.txt_Title.text = NSLocalizedString(@"FACEBOOK", nil);
    facebook.img_SocialSource.image = [UIImage imageNamed:@"ICON_Facebook"];
    [facebook.albumCollection setTag:1];
    UINib *albumNib = [UINib nibWithNibName:@"AlbumSocialCollectionViewCell" bundle:[NSBundle mainBundle]];
    [facebook.albumCollection registerNib:albumNib forCellWithReuseIdentifier:@"cellAlbumsSN"];
    facebook.albumCollection.delegate = self;
    facebook.albumCollection.dataSource = self;
    [facebook.loadingSpinner setHidden:YES];
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    facebook.thumbConnection = [NSURLSession sessionWithConfiguration:config];
    facebook.tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(facebookTapped:)];
    facebook.tapGestureRecognizer.numberOfTapsRequired = 1;
    [facebook.tapGestureRecognizer setCancelsTouchesInView:NO];
    [facebook addGestureRecognizer:facebook.tapGestureRecognizer];
    
    
   
    [self.view reloadInputViews];
    
    
    
}

-(void)bypassAudioLock {
    @try {
        AVAudioSession *audioSession = [AVAudioSession sharedInstance];
        NSError *error = nil;
        BOOL result = NO;
        if ([audioSession respondsToSelector:@selector(setActive:withOptions:error:)]) {
            result = [audioSession setActive:YES withOptions:0 error:&error];
        }
        if (!result && error) {
            // deal with the error
            NSLog(@"StayLog: AVAudioSession Error: %@", error);
        }
        
        error = nil;
        result = [audioSession setCategory:AVAudioSessionCategoryPlayback error:&error];
        
        if (!result && error) {
            // deal with the error
            NSLog(@"StayLog: AVAudioSession setCategory Error: %@", error);
        }
    }
    @catch (NSException *exception) {
        NSLog(@"StayLog: EXCEPTION on Bypass Audio Lock with error: %@", exception.description);
    }
}

-(void)setImageProfile{
    
    self.profileImage.layer.cornerRadius = self.profileButton.frame.size.width / 2;
    self.profileImage.clipsToBounds = YES;
    
    [self.profileImage sd_setImageWithURL:[NSURL URLWithString: self.sfAppStep1.currentUser.photo] placeholderImage:[UIImage imageNamed:@"Avatar_None"] options:SDWebImageRefreshCached];
}


-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    isMovingToConfirmation = NO; // clear button click
    
    if(self.sfAppStep1 == nil)
        self.sfAppStep1 = [StayfilmApp sharedStayfilmAppSingleton];
    self.uploaderStayfilm = [Uploader sharedUploadStayfilmSingletonWithDelegate:self];
    
    self.sfAppStep1.storyState = [StoryState sharedStoryStateSingletonWithDelegate:self];
//    [self.sfAppStep1.filmStrip resetSingletonDelegate];
    [self.sfAppStep1.filmStrip connectivityViewUpdate];
    
    if(self.sfAppStep1.isEditingMode) {
        [self setEditingMode:YES animated:NO];
    }
    else {
        [self setEditingMode:NO animated:NO];
        if(self.sfAppStep1.finalMedias.count >0)
            [self.sfAppStep1.filmStrip enableButton];
    }
    
    if(self.sfAppStep1.finalMedias.count > 0 || self.sfAppStep1.selectedMedias.count > 0) {
        [self.sfAppStep1.filmStrip setHidden:NO];
    }
    self.sfAppStep1.filmStrip.auxNavigation = self.navigationController;
    
    [self.sfAppStep1.filmStrip.but_filmStrip removeTarget:nil action:nil forControlEvents:UIControlEventAllEvents];
    [self.sfAppStep1.filmStrip.but_filmStrip addTarget:self action:@selector(but_filmStrip_Clicked) forControlEvents:UIControlEventTouchUpInside];
    [self.sfAppStep1.filmStrip.but_NextStep removeTarget:nil action:nil forControlEvents:UIControlEventAllEvents];
    [self.sfAppStep1.filmStrip.but_NextStep addTarget:self action:@selector(but_NextStep_Clicked) forControlEvents:UIControlEventTouchUpInside];
    
    if(self.sfAppStep1.finalMedias.count > 0 || self.sfAppStep1.selectedMedias.count > 0)
       [self.sfAppStep1.filmStrip updateFilmStrip];
    
    [self setImageProfile];
    
//    if(![FBSDKAccessToken currentAccessToken] && (self.sfAppStep1.currentUser.networks == nil && self.sfAppStep1.currentUser != nil))
    if((self.sfAppStep1.currentUser.networks == nil && self.sfAppStep1.currentUser != nil))
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^(void){
            [self.sfAppStep1.currentUser getUserNetworksWithSFAppSingleton:self.sfAppStep1];
            
            runOnMainQueueWithoutDeadlocking(^{
                [self prepareAlbums];
            });
        });
        
    }
    else {
        [self prepareAlbums];
    }

    [self.camera_spinner stopAnimating];
    
    // Disable swipe gesture to go back to previous viewcontroller
    if (self.sfAppStep1.isEditingMode && self.navigationController != nil && [self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
    if(self.sfAppStep1.isEditingMode) {
        //Google Analytics
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:@"film-editing_content_add"];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    } else {
        //Google Analytics
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:@"moviemaker_content_sources"];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    }
    //Autofilm
//    if(!self.sfAppStep1.isEditingMode && !self.sfAppStep1.isFromExplorer){
//        if(self.sfAppStep1.selectedMediasToAutoFilm.count > 0 && (self.sfAppStep1.selectedMediasToAutoFilm != self.sfAppStep1.mediasFromAutoFilmCreated)){
//
//            [self showAutoFilmOption];
//
//        }else{
//            if(!self.sfAppStep1.autoFilmCreated){
//            [self.autoFilm selectImagesToGenerateAutomaticVideo];
//            }else{
//                self.sfAppStep1.autoFilmCreated = NO;
//            }
//        }
//    }
    
}

-(void)prepareAlbums {
    if((self.sfAppStep1.currentUser.networks != nil && self.sfAppStep1.currentUser.networks > 0) || [FBSDKAccessToken currentAccessToken]){
        //CREATE SOCIAL NETWORK SOURCES
        
        //Facebook
        [self readFacebookAlbumsFirstTimeWithLogin:NO];
    }else{ // No networks connected
   
        SocialAlbumsView *social = (SocialAlbumsView *)[self.view viewWithTag:1];
        social.isExpanded = NO;
        social.isLoaded = NO;

        [self resizeSocial:social];
    }
    
    // FACEBOOK
    SocialAlbumsView *social = (SocialAlbumsView *)[self.view viewWithTag:1];
    if([FBSDKAccessToken currentAccessToken] != nil) { // logged in facebook
        social.isLoaded = YES;
    }else{
        social.isLoaded = NO;
    }
    
    if(self.sfAppStep1.currentUser.networks != nil && [self.sfAppStep1.currentUser.networks containsObject:@"facebook"] && [FBSDKAccessToken currentAccessToken] != nil){ // Already connected to Facebook in Stayfilm
        social.albums = self.albunsFacebook;
    }else{ //Not connected yet
        social.isExpanded = NO;
        [social.txt_noAlbums setHidden:YES];
        [social.albumCollection setHidden:YES];
    }
    
    [self resizeSocial:social];
}


-(void)resizeSocial:(SocialAlbumsView *)view{
    if(!isResizing) {
        isResizing = YES;
        
        runOnMainQueueWithoutDeadlocking(^{
            @try {
                if(view.isLoaded){
                    
                    if(!view.isExpanded) {
                        view.isExpanded = YES;
                        runOnMainQueueWithoutDeadlocking(^{
                            [UIView animateWithDuration:0.3 animations:^{ // Logged in
                                [view.expandArrow setTransform:CGAffineTransformIdentity];
                                
                            }completion:^(BOOL finished) {
                                if(finished){
                                    [UIView animateWithDuration:0.3 animations:^{
                                        view.expandArrow.alpha = 0;
                                    } completion:^(BOOL finished) {
                                        [view.expandArrow setHidden:YES];
                                    }];
                                }
                            }];
                        });
                    }
                    
                    if(view.isLoadingInfo) {
                        [view.txt_noAlbums setHidden:YES];
                        [view.loadingSpinner startAnimating];
                        [view.loadingSpinner setHidden:NO];
                    }else{
                        [view.loadingSpinner stopAnimating];
                        [view.albumCollection setHidden:NO];
                        [self.tapToOpenView setHidden:YES];
                        [view.constraint_AlbumsHeight setActive:YES];
                        CGFloat estimatedHeight = [view getAlbumHeight];
                        estimatedHeight += 40;   // compensation for gratientWhite
                        [UIView animateWithDuration:0.3 animations:^{
                            [view.constraint_AlbumsHeight setConstant:estimatedHeight];
                        }];
                        [view.albumCollection reloadData];
                    }
                }else{
                    if(view.expandArrow.alpha != 1.0){
                        [view.expandArrow setHidden:NO];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [UIView animateWithDuration:0.3 animations:^{  // show expand icon
                                view.expandArrow.alpha = 1.0;
                            }];
                        });
                    }
                    
                    if(view.isExpanded){
                        view.isExpanded = NO;
                        runOnMainQueueWithoutDeadlocking(^{
                            [UIView animateWithDuration:0.3 animations:^{ // Logged in
                                view.expandArrow.transform = CGAffineTransformMakeRotation(-90 * M_PI / 180.0);
                            }];
                        });
                    }
                    
                    if(view.isLoadingInfo){
                        [view.txt_noAlbums setHidden:YES];
                        [view.loadingSpinner startAnimating];
                        [view.loadingSpinner setHidden:NO];
                    }else{
                        [view.loadingSpinner stopAnimating];
                    }
                    [view.albumCollection setHidden:YES];
                    [self.tapToOpenView setHidden:NO];
                }
            } @catch (NSException *ex) {
                NSLog(@"StayLog: EXCEPTION in resizeSocial with error: %@", ex.description);
            }
            
        });
        isResizing = NO;
    }
}

-(BOOL)connected {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

-(void)clearAlbumViewController{
    self.albumVC = nil;
}

-(void)setEditingMode:(BOOL)isEditing animated:(BOOL)animated{
    if(isEditing) {
        [self.view_Editing setHidden:NO];
        if([StayfilmApp isArray:self.sfAppStep1.editedMedias equalToArray:self.initialEditedMedias]){
            
            self.but_edit_Back.alpha = 0.0;
            [self.but_edit_Back setHidden:NO];
            [self.but_edit_Cancel setHidden:YES];
            [self.but_edit_Confirm setHidden:YES];
            
            if(animated){
                [UIView animateWithDuration:0.4 animations:^{
                    self.but_edit_Back.alpha = 1.0;
                }];
            }else{
                self.but_edit_Back.alpha = 1.0;
            }
        }else{
            self.but_edit_Cancel.alpha = 0.0;
            self.but_edit_Confirm.alpha = 0.0;
            [self.but_edit_Back setHidden:YES];
            [self.but_edit_Cancel setHidden:NO];
            [self.but_edit_Confirm setHidden:NO];
            
            if(animated) {
                [UIView animateWithDuration:0.4 animations:^{
                    self.but_edit_Cancel.alpha = 1.0;
                    self.but_edit_Confirm.alpha = 1.0;
                }];
            }else {
                self.but_edit_Cancel.alpha = 1.0;
                self.but_edit_Confirm.alpha = 1.0;
            }
        }
    } else {
        [self.view_Editing setHidden:YES];
    }
}

#pragma mark - Automatic Films
- (void)canCreateAutoFilm:(BOOL)canCreate{
    
    if (canCreate){
        self.autoFilm = [[AutoFilm alloc]init];
        self.autoFilm.delegate = self;
        [self createLocalNotification];
    }else{
        self.autoFilmView.alpha = 0;
        self.autoFilmConstraint.constant = 0;
    }
}

- (IBAction)makeAutoFilmAction:(id)sender{
    
    self.autoFilm = [[AutoFilm alloc]init];
    self.autoFilm.delegate = self;
    self.autoFilm.isFromStep1 = YES;
    [self.autoFilm createAutomaticVideo];
   
}
- (void)setImage:(UIImage *)image andTitle:(NSString *)title{
    
    [UIView animateWithDuration:0.5 animations:^{
        self.autoFilmView.alpha = 1.0;
        self.autoFilmConstraint.constant = 240;
        
        [self.autoFilmImage setImage:image];
        self.autoFilmLabel.text = title;
    }];
    
    
}

- (void)showAutoFilmOption{
    
    PHImageRequestOptions *requestOptions  = [[PHImageRequestOptions alloc] init];
    requestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
    requestOptions.synchronous = YES;
    
    PHImageManager *manager = [PHImageManager defaultManager];
    [manager requestImageForAsset:self.sfAppStep1.selectedMediasToAutoFilm[0]
                       targetSize: CGSizeMake([[UIScreen mainScreen] bounds].size.width - 30, 240)
                      contentMode:PHImageContentModeAspectFill
                          options:requestOptions
                    resultHandler:^void(UIImage *image, NSDictionary *info) {
                        
                        [UIView animateWithDuration:0.5 animations:^{
                            self.autoFilmView.alpha = 1.0;
                            self.autoFilmConstraint.constant = 240;
                            [self.autoFilmButton setTitle:NSLocalizedString(@"MAKE_FILM_BUTTON",nil) forState:UIControlStateNormal];
                            [self.autoFilmImage setImage:image];
                            self.autoFilmLabel.text = self.sfAppStep1.selectedTitleToAutoFilm;
                            [self createLocalNotification];
                        }];
                        
                    }];
    
    
}

- (void)redirecToProgressView{
    
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"MovieMaker" bundle:nil];
    Step1ConfirmationViewController *vc = [story instantiateViewControllerWithIdentifier:@"Step1Confirmation"];
    [self.navigationController pushViewController:vc animated:YES];
    /*if(self.sfAppStep1.selectedMediasToAutoFilm.count >= 5){
        //Redirect to Progress ViewController
        ProgressViewController *vc = [story instantiateViewControllerWithIdentifier:@"Progress"];
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        //Redirect to Step 1 ViewController
        Step1ConfirmationViewController *vc = [story instantiateViewControllerWithIdentifier:@"Step1Confirmation"];
        [self.navigationController pushViewController:vc animated:YES];
    }*/
    
    
}
-(void)createLocalNotification{
    
    UIApplication *app = [UIApplication sharedApplication];
    NSArray *eventArray = [app scheduledLocalNotifications];
    
    if(eventArray.count <= 0){
        //Create Local Notification
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        UNAuthorizationOptions options = UNAuthorizationOptionAlert + UNAuthorizationOptionSound;
        [center requestAuthorizationWithOptions:options
                              completionHandler:^(BOOL granted, NSError * _Nullable error) {
                                  if (!granted) {
                                      NSLog(@"Something went wrong");
                                  }
                              }];
        [center getNotificationSettingsWithCompletionHandler:^(UNNotificationSettings * _Nonnull settings) {
        }];
        
        
        PHImageRequestOptions * imageRequestOptions = [[PHImageRequestOptions alloc] init];
        imageRequestOptions.networkAccessAllowed = YES;
        imageRequestOptions.synchronous = YES;
        [[PHImageManager defaultManager]
         requestImageDataForAsset:self.sfAppStep1.selectedMediasToAutoFilm[0]
         options:nil
         resultHandler:^(NSData *imageData, NSString *dataUTI,
                         UIImageOrientation orientation,
                         NSDictionary *info){
             
             if ([info objectForKey:@"PHImageFileURLKey"]) {
                 
                 NSURL *tmpDirURL = [NSURL fileURLWithPath:NSTemporaryDirectory() isDirectory:YES];
                 NSURL *fileURL = [[tmpDirURL URLByAppendingPathComponent:@"AutoFilmImg"] URLByAppendingPathExtension:@"png"];
                 
                 NSFileManager *fileManager = [NSFileManager defaultManager];
                 if ([fileManager fileExistsAtPath:[fileURL path]]){
                     [fileManager removeItemAtPath:[fileURL path] error:nil];
                 }
                 [imageData writeToURL:fileURL options:NSAtomicWrite error:nil];
                 
                 UNNotificationAttachment *imageAttachment = [UNNotificationAttachment                                                    attachmentWithIdentifier:@"image" URL:fileURL options:nil error:nil];
                 
                 UNMutableNotificationContent *content = [UNMutableNotificationContent new];
                 content.title = [NSString stringWithFormat: NSLocalizedString(@"AF_NOTIFICATION_TITLE",nil), self.sfAppStep1.selectedTitleToAutoFilm];
                 content.body = NSLocalizedString(@"AF_NOTIFICATION_BODY",nil);
                 content.badge = [NSNumber numberWithInt:(int)[[UIApplication sharedApplication] applicationIconBadgeNumber] + 1];
                 content.sound = [UNNotificationSound defaultSound];
                 content.launchImageName = @"notificationImage";
                 
                 content.attachments = [NSArray arrayWithObjects:imageAttachment, nil];
                 
                 UNTimeIntervalNotificationTrigger* trigger = [UNTimeIntervalNotificationTrigger
                                                               triggerWithTimeInterval:(30) repeats: NO];
                 
                 NSString *identifier = @"LocalNotification";
                 UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:identifier
                                                                                       content:content trigger:trigger];
                 [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
                     if (error != nil) {
                         NSLog(@"Something went wrong: %@",error);
                     }
                 }];
             }
         }];
    }
}

#pragma mark - Read Albums

-(void)didLoadFacebookAlbums{
    
    // Update current albuns
    SocialAlbumsView *social = (SocialAlbumsView *)[self.view viewWithTag:1];

    if(self.loadedAllFacebookAlbums){
        social.isLoadingInfo = NO;
        [social.loadingSpinner stopAnimating];
        [self resizeSocial:social];
    }
    
    if(social.albums.count > 0) {
        social.isLoaded = YES;
        [social removeGestureRecognizer:social.tapGestureRecognizer];
    }
    [social.albumCollection reloadData];
}

-(void)readFacebookAlbumsFirstTimeWithLogin:(BOOL)p_doLogin {
    @try {
        SocialAlbumsView *facebook = (SocialAlbumsView *)[self.view viewWithTag:1];
        facebook.albums = self.albunsFacebook;
        if([FBSDKAccessToken currentAccessToken] != nil){ // check facebook logged in the app
            facebook.isLoaded = YES;
            
            if(facebook.albums.count == 0 && facebook.isLoaded){
                [facebook.albumCollection setHidden:NO];
                if(facebook.isLoadingInfo){
                    [facebook.txt_noAlbums setHidden:YES];
                }else{
                    [facebook.txt_noAlbums setHidden:NO];
                }
            }else{
                facebook.isLoaded = YES;
                [facebook.constraint_AlbumsHeight setActive:YES];
                [facebook.txt_noAlbums setHidden:YES];
                [facebook.albumCollection setHidden:NO];
            }
            
        }else{
            facebook.isLoaded = NO;
        }
        
        if([FBSDKAccessToken currentAccessToken] != nil){ // Already connected to Facebook in Stayfilm and App

            if(self.sfAppStep1.currentUser.networks == nil || ![self.sfAppStep1.currentUser.networks containsObject:@"facebook"]){
                [self.sfAppStep1.settings setObject:[[FBSDKAccessToken currentAccessToken] tokenString] forKey:@"facebookToken"];
                [self.sfAppStep1 saveSettings];
                [self.sfAppStep1.currentUser setUserFacebookToken:[[FBSDKAccessToken currentAccessToken] tokenString] andSFAppSingleton:self.sfAppStep1];
            }
            
            if(self.albunsFacebook.count == 0 && facebook.isLoaded){ // haven't loaded facebook albums yet
                
                [facebook.loadingSpinner setHidden:NO];
                facebook.isLoadingInfo = YES;
                __weak typeof(self) selfDelegate = self;
                self.lockAlbum = [[NSConditionLock alloc] initWithCondition:1];

                [self.lockAlbum lock];
                [self.lockAlbum unlockWithCondition:0];
                self.albumSemaphore = dispatch_semaphore_create(0); // semaphore to make video loading wait
                
                // Request to load images from Facebook
                if((!self.isSelectingSocial) || (!self.calledAlbumsFistTime)) {
                    self.isSelectingSocial = YES;
                    self.calledAlbumsFistTime = YES;
                    
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                                  initWithGraphPath:@"/me/albums"
                                                  parameters:@{@"fields": @"id, name, picture, count"}
                                                  HTTPMethod:@"GET"];
                    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                                          id result,
                                                          NSError *error) {
                if(error){
                    NSLog(@"StayLog: Error in readFacebookAlbumsFirstTime wirh error: %@", error.description);
                    
                      if([error.description containsString:@"OAuthException"]) { //needs new facebook token
                          
                        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"FACEBOOK_ERROR_AUTH", nil)                                                                                     message:NSLocalizedString(@"FACEBOOK_ERROR_AUTH_MESSAGE", nil)                                                                                        preferredStyle:UIAlertControllerStyleAlert];
                                
                        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault                                                                                      handler:^(UIAlertAction * action){
                                    
                            // login after Ok click
                            LoginFacebookOperation *fbLogin = [[LoginFacebookOperation alloc] initWithDelegate:self andSFApp:self.sfAppStep1];
                            [self.sfAppStep1.operationsQueue addOperation:fbLogin];
                        }];
                                
                    [alert addAction:defaultAction];
                          
                    [self presentViewController:alert animated:YES completion:nil];
                                
                    facebook.isLoaded = NO;
                    facebook.isLoadingInfo = NO;
                    [facebook reloadInputViews];
                          
                        }else{
                                
                        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"FACEBOOK_ERROR", nil)                                                                                               message:NSLocalizedString(@"FACEBOOK_ERROR_MESSAGE", nil)                                                                                        preferredStyle:UIAlertControllerStyleAlert];
                                
                        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault                                                                                    handler:^(UIAlertAction * action) {}];
                                
                                [alert addAction:defaultAction];
                                [self presentViewController:alert animated:YES completion:nil];
                                
                                facebook.isLoaded = NO;
                                facebook.isLoadingInfo = NO;
                                [facebook reloadInputViews];
                        }
                            dispatch_semaphore_signal(selfDelegate.albumSemaphore);
                            [self.lockAlbum lock];
                            [self.lockAlbum unlockWithCondition:1];
                }else{
                            
                    if(result[@"data"] != nil){
                        BOOL found = NO;
                        
                        for(NSDictionary *var in result[@"data"]) {
                            if(var[@"count"] != nil && var[@"count"] != [NSNumber numberWithLong:0])
                            {
                                found = YES;
                                AlbumSN *album = [AlbumSN customClassWithProperties:var];
                                if(![selfDelegate.albunsFacebook containsObject:album]) {
                                    [selfDelegate.albunsFacebook addObject:album];
                                    ++statusFBalbums;
                                }
                            }
                        }
                        for(AlbumSN *album in selfDelegate.albunsFacebook)
                        {
                            if(album.imageBIG != nil) {
                                if(result[@"picture"] != nil && result[@"picture"][@"data"] != nil){
                                    album.imageBIG = result[@"picture"][@"data"][@"url"];
                                }
                                //now get 2 other photos
                                @try {
                                    __weak typeof(self) selfDelegate = self;
                                    NSString *url = [NSString stringWithFormat:@"/%@/photos" , [album idAlbum]];
                                    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                                                  initWithGraphPath:url
                                                                  parameters:@{@"fields": @"id, source, width, height, images"
                                                                               }
                                                                  HTTPMethod:@"GET"];
                                    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                                                          id result,
                                                                          NSError *error) {
                                    if(error){
                                            NSLog(@"StayLog: Request for photos in Facebook Album failed. MovieMakerStep1 didappear with error: %@", error.description);
                                        }else{
                                            @synchronized (selfDelegate.albunsFacebook) {
                                                @try{
                                                    if(result[@"data"] != nil){
                                                        for(NSDictionary *var in result[@"data"]){
                                                            [album.idMedias addObject:[PhotoFB customClassWithProperties:var]];
                                                            ((PhotoFB*)[album.idMedias lastObject]).idAlbum = album.idAlbum;
                                                        }
                                                        if(album.idMedias.count > 0){ // get lowest quality thumb for LEFT thumb image
                                                            if(album.idMedias.count >= 3){
                                                                album.imageLEFT = ((ImageFB *)[((PhotoFB *)[album.idMedias objectAtIndex:2]).images lastObject]).source;
                                                            }else{
                                                                album.imageLEFT = ((ImageFB *)[((PhotoFB *)[album.idMedias objectAtIndex:0]).images lastObject]).source;
                                                            }
                                                            
                                                            if(album.idMedias.count > 1){ // get thumb for RIGHT image
                                                                album.imageRIGHT = ((ImageFB *)[((PhotoFB *)[album.idMedias objectAtIndex:1]).images lastObject]).source;
                                                            }
                                                        }
                                                        [selfDelegate didLoadFacebookAlbums];
                                                    }
                                                }
                                                @catch (NSException *exception) {
                                                    NSLog(@"StayLog: EXCEPTION creating Facebook Photo view Collection. MovieMakerStep1 readFacebookAlbumsfirstTime Exception: %@", exception.description);
                                                    facebook.isLoadingInfo = NO;
                                                }
                                            }
                                        }
                                        statusFBalbums--;
                                        if(statusFBalbums == 0)
                                            dispatch_semaphore_signal(selfDelegate.albumSemaphore);
                                    }];
                                }
                                @catch (NSException *ex){
                                    NSLog(@"StayLog: EXCEPTION creating Facebook Photo view Collection. MovieMakerStep1 readFacebookAlbumsfirstTime Exception 2: %@", ex.description);
                                    facebook.isLoadingInfo = NO;
                                }
                            }
                        }
                        
                        if (!found) { // NO ALBUMS
                            [facebook.txt_noAlbums setHidden:NO];
                            selfDelegate.loadedAllFacebookAlbums = YES;
                            runOnMainQueueWithoutDeadlocking(^{
                                NSArray *constraints = [facebook constraints];
                                NSLayoutConstraint *constraint = nil;
                                for (NSLayoutConstraint *object in constraints) {
                                    if ( [object.identifier isEqualToString:@"heightFacebook"] ) {
                                        constraint = object;
                                        break;
                                    }
                                }
                                if(constraint != nil) {
                                    [constraint setConstant:80];
                                }
                            });
                            
                            [selfDelegate didLoadFacebookAlbums];
                            [self callFacebookVideos];
                            
                        }else { // FOUND ALBUMS
                            runOnMainQueueWithoutDeadlocking(^{
                                [facebook.txt_noAlbums setHidden:YES];
                                [facebook.albumCollection setHidden:NO];
                                
                                NSArray *constraints = [facebook constraints];
                                NSLayoutConstraint *constraint = nil;
                                for (NSLayoutConstraint *object in constraints) {
                                    if ( [object.identifier isEqualToString:@"heightFacebook"] ) {
                                        constraint = object;
                                        break;
                                    }
                                }
                                if(constraint != nil) {
                                    [constraint setActive:NO];  // unlocking Facebook Albuns contraint
                                    [facebook.constraint_AlbumsHeight setActive:YES];
                                    CGFloat estimatedHeight = [facebook getAlbumHeight];
                                    estimatedHeight += 40;   // compensation for gradientWhite
                                    [UIView animateWithDuration:0.4 animations:^{
                                        [facebook.constraint_AlbumsHeight setConstant:estimatedHeight];
                                    }];
                                }
                                [selfDelegate didLoadFacebookAlbums];
                                [self callFacebookVideos];
                            });
                        }
                        
                    }
                            
                            if(result[@"paging"] != nil)
                            {
                                if(result[@"paging"][@"next"] != nil)
                                {
                                    selfDelegate.facebookNextPage = result[@"paging"][@"cursors"][@"after"];
                                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                                        [selfDelegate readAllFacebookAlbums];
                                    });
                                }
                                else // finished reading albums
                                {
                                    selfDelegate.loadedAllFacebookAlbums = YES;
                                    facebook.isLoadingInfo = NO;
                                    [self.lockAlbum lockWhenCondition:2];
                                    [self.lockAlbum lock];
                                    [self.lockAlbum unlockWithCondition:1];
                                }
                            }else{
                                selfDelegate.loadedAllFacebookAlbums = YES;
                                facebook.isLoadingInfo = NO;
                                [self.lockAlbum lockWhenCondition:2];
                                [self.lockAlbum lock];
                                [self.lockAlbum unlockWithCondition:1];
                            }
                            [selfDelegate resizeSocial:facebook];
                        }
                    }];
                });
                    
                }else{
                    NSLog(@"Called");
                    selfDelegate.loadedAllFacebookAlbums = YES;
                    facebook.isLoadingInfo = NO;
                }
           
            }
            facebook.albums = self.albunsFacebook;
        }else if(p_doLogin){
            LoginFacebookOperation *loginOp = [[LoginFacebookOperation alloc] initWithoutUserOverrideWithDelegate:self andSFApp:self.sfAppStep1];
            [sfAppStep1.operationsQueue addOperation:loginOp];
            
            //Google Analytics Event
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"source"
                                                                  action:@"facebook"
                                                                   label:@"continue-permission"
                                                                   value:@1] build]];
        }else{ // Not Connected to facebook
            SocialAlbumsView *facebook =(SocialAlbumsView *)[self.view viewWithTag:1];
            facebook.isExpanded = NO;
            facebook.isLoaded = NO;
            [self resizeSocial:facebook];
        }
    }
    @catch (NSException *ex){
        NSLog(@"StayLog: EXCEPTION in readFacebookAlbumsFirstTimeWithLogin with error: %@", ex.description);
    }
}

-(void)callFacebookVideos{
    
    SocialAlbumsView *facebook = (SocialAlbumsView *)[self.view viewWithTag:1];
    facebook.albums = self.albunsFacebook;
    __weak typeof(self) selfDelegate = self;
    [self.lockAlbum lockWhenCondition:0];
    [self.lockAlbum unlock];
    self.lockAlbum = nil;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        dispatch_semaphore_wait(self.albumSemaphore, DISPATCH_TIME_FOREVER);
        //Now another request for Videos
        FBSDKGraphRequest *requestVideos = [[FBSDKGraphRequest alloc]
                                            initWithGraphPath:@"/me/videos/uploaded"
                                            parameters:@{@"fields": @"id,source,picture,length"}
                                            HTTPMethod:@"GET"];
        [requestVideos startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                                    id result,
                                                    NSError *error) {
            if(error)
            {
                NSLog(@"StayLog: Error in readFacebookAlbumsFirstTime VIDEOS wirh error: %@", error.description);
                if([error.description containsString:@"OAuthException"]) { //needs new facebook token
                    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"FACEBOOK_ERROR_AUTH", nil)
                                                                                   message:NSLocalizedString(@"FACEBOOK_ERROR_AUTH_MESSAGE", nil)
                                                                            preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                          handler:^(UIAlertAction * action) {
                                                                              // login after Ok click
                                                                              LoginFacebookOperation *fbLogin = [[LoginFacebookOperation alloc] initWithDelegate:self andSFApp:selfDelegate.sfAppStep1];
                                                                              [selfDelegate.sfAppStep1.operationsQueue addOperation:fbLogin];
                                                                          }];
                    
                    [alert addAction:defaultAction];
                    [selfDelegate presentViewController:alert animated:YES completion:nil];
                    
                    [facebook reloadInputViews];
                }else{
                    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"FACEBOOK_ERROR", nil)
                                                                                   message:NSLocalizedString(@"FACEBOOK_ERROR_MESSAGE", nil)
                                                                            preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                          handler:^(UIAlertAction * action) {}];
                    
                    [alert addAction:defaultAction];
                    [selfDelegate presentViewController:alert animated:YES completion:nil];
                    
                    [facebook reloadInputViews];
                }
            }
            
            if(result[@"data"] != nil){
                @synchronized (selfDelegate.albunsFacebook) {
                    @try {
                        BOOL hasVideos = NO;
                        int counter = 0;
                        long indexVideoAlbum = -1;
                        for(AlbumSN* alb in selfDelegate.albunsFacebook){
                            if([alb.idAlbum isEqualToString:@"videos"]) {
                                indexVideoAlbum = [selfDelegate.albunsFacebook indexOfObject:alb];
                                break;
                            }
                        }
                        for(NSDictionary *var in result[@"data"]){
                            hasVideos = YES;
                            ++counter;
                            
                            VideoFB *video = [VideoFB customClassWithProperties:var];
                            
                            if(indexVideoAlbum != -1) {
                                AlbumSN* alb = [selfDelegate.albunsFacebook objectAtIndex:indexVideoAlbum];
                                if(![alb.idMedias containsObject:video]) {
                                    [alb.idMedias addObject:video];
                                }
                            }else{ // if still no videos loaded
                                AlbumSN *albumVideo = [[AlbumSN alloc] init];
                                albumVideo.name = NSLocalizedString(@"VIDEOS", nil);
                                albumVideo.idAlbum = @"videos";
                                [albumVideo.idMedias addObject:video];
                                [selfDelegate.albunsFacebook insertObject:albumVideo atIndex:0];
                                indexVideoAlbum = 0;
                            }
                        }
                        
                    if(hasVideos){
                            ((AlbumSN*)[selfDelegate.albunsFacebook objectAtIndex:indexVideoAlbum]).count = [NSNumber numberWithInt:counter];
                        if( ((AlbumSN*)[selfDelegate.albunsFacebook objectAtIndex:indexVideoAlbum]).idMedias.count > 0 ){  //thumb for BIG image
                                ((AlbumSN*)selfDelegate.albunsFacebook[indexVideoAlbum]).imageBIG = ((VideoFB*)((AlbumSN*)[selfDelegate.albunsFacebook objectAtIndex:indexVideoAlbum]).idMedias[0]).picture;
                            }
                        if( [[[selfDelegate.albunsFacebook objectAtIndex:indexVideoAlbum] idMedias] count] > 1 ){ // get lowest quality thumb for LEFT thumb image
                        
                                ((AlbumSN*)selfDelegate.albunsFacebook[indexVideoAlbum]).imageLEFT = ((VideoFB *)[[[selfDelegate.albunsFacebook objectAtIndex:indexVideoAlbum] idMedias] objectAtIndex:1]).picture;
                            
                            if([[[selfDelegate.albunsFacebook objectAtIndex:indexVideoAlbum] idMedias] count] > 2 ){ // get thumb for RIGHT image
                                    ((AlbumSN*)selfDelegate.albunsFacebook[indexVideoAlbum]).imageRIGHT = ((VideoFB *)[[[selfDelegate.albunsFacebook objectAtIndex:indexVideoAlbum] idMedias] objectAtIndex:2]).picture;
                                }
                            }
                        }
                        
                        if(selfDelegate.loadedAllFacebookAlbums == YES && facebook.albums.count == 0 && hasVideos){ // USER WITH NO PHOTOS
                
                            runOnMainQueueWithoutDeadlocking(^{
                                [facebook.txt_noAlbums setHidden:YES];
                                [facebook.albumCollection setHidden:NO];
                              
                                NSArray *constraints = [facebook constraints];
                                NSLayoutConstraint *constraint = nil;
                                for (NSLayoutConstraint *object in constraints) {
                                    if([object.identifier isEqualToString:@"heightFacebook"]){
                                        constraint = object;
                                        break;
                                    }
                                }
                                
                                if(constraint != nil){
                                    [constraint setActive:NO];  // unlocking Facebook Albuns contraint
                                    [facebook.constraint_AlbumsHeight setActive:YES];
                                    CGFloat estimatedHeight = [facebook getAlbumHeight];
                                    estimatedHeight += 40;   // compensation for gradientWhite
                                    [UIView animateWithDuration:0.4 animations:^{
                                        [facebook.constraint_AlbumsHeight setConstant:estimatedHeight];
                                    }];
                                }
                                [selfDelegate didLoadFacebookAlbums];
                            });
                        }else if(selfDelegate.loadedAllFacebookAlbums == YES && facebook.albums.count > 0 && hasVideos){
                            [selfDelegate didLoadFacebookAlbums];
                        }
                    }
                    @catch (NSException *ex) {
                        NSLog(@"StayLog: EXCEPTION in readFacebookAlbumsFirstTImeWithLogin VIDEOS with error: %@", ex.description);
                    }
                }
            }
            
            if(result[@"paging"] != nil){
                
                if(result[@"paging"][@"next"] != nil){
                    
                    selfDelegate.facebookVideoNextPage = result[@"paging"][@"cursors"][@"after"];
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                        [selfDelegate readAllFacebookVideos];
                    });
                }else{ // finished reading albums
        
                    selfDelegate.loadedAllFacebookAlbums = YES;
                    facebook.isLoadingInfo = NO;
                    [self.lockAlbum lockWhenCondition:2];
                    [self.lockAlbum lock];
                    [self.lockAlbum unlockWithCondition:1];
                    [selfDelegate resizeSocial:facebook];
                }
            }else{
                selfDelegate.loadedAllFacebookAlbums = YES;
                facebook.isLoadingInfo = NO;
                [self.lockAlbum lockWhenCondition:2];
                [self.lockAlbum lock];
                [self.lockAlbum unlockWithCondition:1];
                [selfDelegate resizeSocial:facebook];
            }
        }];
    });
}

-(void) readAllFacebookAlbums{
    
    runOnMainQueueWithoutDeadlocking(^{
        @try {
            SocialAlbumsView *facebook = (SocialAlbumsView *)[self.view viewWithTag:1];
            __weak typeof(self) selfDelegate = self;
            NSDictionary *params = @{ @"after" : self.facebookNextPage,
                                      @"fields": @"id, name, picture, count" };
            FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                          initWithGraphPath:@"/me/albums"
                                          parameters:params
                                          HTTPMethod:@"GET"];
            [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                                  id result,
                                                  NSError *error) {
                if(error){
                    NSLog(@"StayLog: Facebook request failed readAllFacebookAlbuns with error: %@", error.description);
                }
                
                if(result[@"data"] != nil){
                    for(NSDictionary *var in result[@"data"]) {
                        if(var[@"count"] != nil && var[@"count"] != [NSNumber numberWithLong:0])
                        {
                            @synchronized (selfDelegate.albunsFacebook) {
                                AlbumSN *album = [AlbumSN customClassWithProperties:var];
                                if(![selfDelegate.albunsFacebook containsObject:album]) {
                                    [selfDelegate.albunsFacebook addObject:album];
                                    ++statusFBalbums;
                                }
                                
                                if (album.imageBIG != nil) {
                                    if(result[@"picture"] != nil && result[@"picture"][@"data"] != nil)
                                    {
                                        album.imageBIG = result[@"picture"][@"data"][@"url"];
                                    }
                                    //now get 2 other photos
                                    @try {
                                        __weak typeof(self) selfDelegate = self;
                                        NSString *url = [NSString stringWithFormat:@"/%@/photos" , [album idAlbum]];
                                        FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                                                      initWithGraphPath:url
                                                                      parameters:@{@"fields": @"id, source, width, height, images"}
                                                                      HTTPMethod:@"GET"];
                                        [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                                                              id result,
                                                                              NSError *error) {
                                            if(error){
                                                NSLog(@"StayLog: Request for photos in Facebook Album failed. MovieMakerStep1 didappear with error: %@", error.description);
                                            }else{
                                                @synchronized (selfDelegate.albunsFacebook) {
                                                    @try {
                                                        if(result[@"data"] != nil){
                                                            for(NSDictionary *var in result[@"data"]) {
                                                                [album.idMedias addObject:[PhotoFB customClassWithProperties:var]];
                                                                ((PhotoFB*)[album.idMedias lastObject]).idAlbum = album.idAlbum;
                                                            }
                                                            if(album.idMedias.count > 0) // get lowest quality thumb for LEFT thumb image
                                                            {
                                                                album.imageLEFT = ((ImageFB *)[((PhotoFB *)[album.idMedias objectAtIndex:0]).images lastObject]).source;
                                                                
                                                                if(album.idMedias.count > 1) // get thumb for RIGHT image
                                                                {
                                                                    album.imageRIGHT = ((ImageFB *)[((PhotoFB *)[album.idMedias objectAtIndex:1]).images lastObject]).source;
                                                                }
                                                            }
                                                           [selfDelegate didLoadFacebookAlbums];
                                                        }
                                                    }
                                                    @catch (NSException *exception) {
                                                        NSLog(@"StayLog: EXCEPTION creating Facebook Photo view Collection. MovieMakerStep1 didappear Exception: %@", exception.description);
                                                        facebook.isLoadingInfo = NO;
                                                    }
                                                }
                                            }
                                            statusFBalbums--;
                                            if(statusFBalbums == 0)
                                                dispatch_semaphore_signal(selfDelegate.albumSemaphore);
                                        }];
                                    }
                                    @catch (NSException *ex)
                                    {
                                        NSLog(@"StayLog: EXCEPTION creating Facebook Photo view Collection.MovieMakerStep1 didappear Exception 2: %@", ex.description);
                                        facebook.isLoadingInfo = NO;
                                    }
                                }
                            }
                        }
                    }
                }
                if(result[@"paging"] != nil)
                {
                    if(result[@"paging"][@"next"] != nil)
                    {
                        selfDelegate.facebookNextPage = result[@"paging"][@"cursors"][@"after"];
                        [selfDelegate didLoadFacebookAlbums];
                        
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                            [selfDelegate readAllFacebookAlbums];
                        });
                    }
                    else
                    {
                        selfDelegate.loadedAllFacebookAlbums = YES;
                    }
                    
                    [selfDelegate didLoadFacebookAlbums];
                }
            }];
        }
        @catch (NSException *exception) {
            NSLog(@"StayLog: EXCEPTION in readAllFacebookAlbums with error: %@", exception.description);
        }
    });
}

-(void)readAllFacebookVideos
{
    @try {
        __weak typeof(self) selfDelegate = self;
        NSDictionary *params = @{ @"after" : self.facebookVideoNextPage,
                                  @"fields": @"id,source,picture,length",
                                  @"limit" : @"50" };
        FBSDKGraphRequest *requestVideos = [[FBSDKGraphRequest alloc]
                                            initWithGraphPath:@"/me/videos/uploaded"
                                            parameters:params
                                            HTTPMethod:@"GET"];
        [requestVideos startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                              id result,
                                              NSError *error) {
            if(error) {
                NSLog(@"Staylog: ERROR in readAllFacebookVideos with error: %@", error.description);
                return;
            }
            if(result[@"data"] != nil)
            {
                BOOL hasVideos = NO;
                int counter = 0;
                AlbumSN *album = nil;
                
                for (AlbumSN* alb in selfDelegate.albunsFacebook) {
                    if([alb.idAlbum isEqualToString:@"videos"]) {
                        album = alb;
                        break;
                    }
                }
                for(NSDictionary *var in result[@"data"]) {
                    hasVideos = YES;
                    
                    VideoFB *video = [VideoFB customClassWithProperties:var];
                    
                    if(![album.idMedias containsObject:video]) {
                        [album.idMedias addObject:video];
                        ++counter;
                    }
                }
                if(hasVideos) {
                    album.count = [NSNumber numberWithInt:([album.count intValue] + counter)];
                }
            }
            if(result[@"paging"] != nil)
            {
                if(result[@"paging"][@"next"] != nil)
                {
                    selfDelegate.facebookVideoNextPage = result[@"paging"][@"cursors"][@"after"];
                    [selfDelegate didLoadFacebookAlbums];
                    
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        [selfDelegate readAllFacebookVideos];
                    });
                }
                else
                {
                    selfDelegate.loadedAllFacebookAlbums = YES;
                }
                [selfDelegate didLoadFacebookAlbums];
            }
        }];
    }
    @catch (NSException *exception) {
        NSLog(@"StayLog: EXCEPTION in readAllFacebookVideos with error: %@", exception.description);
    }
}


-(void)readAllPhotosFromFacebookAlbum:(AlbumSN *)p_album andAfter:(NSString *)p_after {
    @try {
        if(self.albumReadAttempts[p_album.idAlbum] != nil)
            self.albumReadAttempts[p_album.idAlbum] = [NSString stringWithFormat:@"%d",(([self.albumReadAttempts[p_album.idAlbum] intValue]) + 1)];
        else
            self.albumReadAttempts[p_album.idAlbum] = @"1";
        __weak typeof(self) selfDelegate = self;
        NSString *url = [NSString stringWithFormat:@"/%@/photos" , [p_album idAlbum]];
        long int index = [self.albunsFacebook indexOfObject:p_album];
        NSDictionary *params = @{ @"after" : p_after,
                                  @"fields": @"id, source, width, height, images",
                                  @"limit" : @"50" };
        FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                      initWithGraphPath:url
                                      parameters:params
                                      HTTPMethod:@"GET"];
        [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                              id result,
                                              NSError *error) {
            if(error)
            {
                NSLog(@"StayLog: Request for photos in Facebook Album failed. FBSDKGraphAPI error: %@", error.description);
                int attempts = 0;
                if([self.albumReadAttempts objectForKey:p_album.idAlbum] != nil)
                    attempts = [self.albumReadAttempts[p_album.idAlbum] intValue];
                if(attempts > 3) {
                    if([self.albumReadAttempts objectForKey:p_album.idAlbum] != nil)
                        [self.albumReadAttempts removeObjectForKey:p_album.idAlbum];
                    [self.lockAlbum lock];
                    [self.lockAlbum unlockWithCondition:1];
                } else {
                    [selfDelegate readAllPhotosFromFacebookAlbum:p_album andAfter:p_after];
                }
            }
            else
            {
                @try {
                    if([self.albumReadAttempts objectForKey:p_album.idAlbum] != nil)
                        [self.albumReadAttempts removeObjectForKey:p_album.idAlbum];
                    if(result[@"data"] != nil)
                    {
                        for(NSDictionary *var in result[@"data"]) {
                            [[[selfDelegate.albunsFacebook objectAtIndex:index] idMedias] addObject:[PhotoFB customClassWithProperties:var]];
                            ((PhotoFB*)([[[selfDelegate.albunsFacebook objectAtIndex:index] idMedias] lastObject])).idAlbum = [[selfDelegate.albunsFacebook objectAtIndex:index] idAlbum];
                        }
                        if(result[@"paging"] != nil) {
                            if(result[@"paging"][@"next"] != nil && result[@"paging"][@"cursors"][@"after"] != nil) {
                                dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                                    //Background Thread
                                   // [selfDelegate readAllPhotosFromFacebookAlbum:p_album andAfter:result[@"paging"][@"cursors"][@"after"]];
                                });
                            }
                            else {
                                //NSLog(@"StayLog: Finished reading album: %@",p_album.name);
                                if(self.albumVC != nil) {
                                    if([self.albumVC.albumFB.idAlbum isEqualToString:((AlbumSN *)[selfDelegate.albunsFacebook objectAtIndex:index]).idAlbum]) {
                                        self.albumVC.albumFB = [selfDelegate.albunsFacebook objectAtIndex:index];
                                        [self.albumVC realoadAlbum];
                                    }
                                }
                                [self.lockAlbum lock];
                                [self.lockAlbum unlockWithCondition:1];
                            }
                        }
                        else {
                            [self.lockAlbum lock];
                            [self.lockAlbum unlockWithCondition:1];
                        }
                    }
                }
                @catch (NSException *exception) {
                    NSLog(@"StayLog: EXCEPTION in readAllPhotosFromFacebookAlbum with error: %@", exception.description);
                    SocialAlbumsView *facebook =(SocialAlbumsView *)[self.view viewWithTag:1];
                    facebook.isLoadingInfo = NO;
                    if([self.albumReadAttempts objectForKey:p_album.idAlbum] != nil)
                        [self.albumReadAttempts removeObjectForKey:p_album.idAlbum];
                    [self.lockAlbum lock];
                    [self.lockAlbum unlockWithCondition:1];
                }
            }
        }];
        
    }
    @catch (NSException *ex)
    {
        NSLog(@"StayLog: EXCEPTION in readAllPhotosFromFacebookAlbum error 2: %@", ex.description);
        SocialAlbumsView *facebook =(SocialAlbumsView *)[self.view viewWithTag:1];
        facebook.isLoadingInfo = NO;
        if([self.albumReadAttempts objectForKey:p_album.idAlbum] != nil)
            [self.albumReadAttempts removeObjectForKey:p_album.idAlbum];
        [self.lockAlbum lock];
        [self.lockAlbum unlockWithCondition:1];
    }
}

-(void)readInstagramAlbumsFirstTimeWithLogin:(BOOL)p_doLogin {
    @try {
        if(self.sfAppStep1.currentUser.networks != nil && [self.sfAppStep1.currentUser.networks containsObject:@"instagram"]) // Already connected to Instagram in Stayfilm
        {
            SocialAlbumsView *instagram = (SocialAlbumsView *)[self.view viewWithTag:2];
            
            instagram.isExpanded = NO;
            instagram.isLoaded = YES;
            [instagram.loadingSpinner setHidden:NO];
            [instagram.loadingSpinner startAnimating];
            
            if([self.albunsInstagram count] == 0) // haven't loaded instagram album yet
            {
                __weak typeof(self) selfDelegate = self;
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    NSMutableArray *albums = [AlbumSF getAlbumsFromUser:selfDelegate.sfAppStep1.currentUser fromNetwork:@"instagram" withOffset:0 withLimit:0];
                    if(albums != nil && albums.count > 0 && [albums objectAtIndex:0] != nil)
                    {
                        runOnMainQueueWithoutDeadlocking(^{
                            [instagram.txt_noAlbums setHidden:YES];
                            NSArray *constraints = [[self.view viewWithTag:2] constraints];
                            NSLayoutConstraint *constraint = nil;
                            for (NSLayoutConstraint *object in constraints) {
                                if ( [object.identifier isEqualToString:@"heightInstagram"] ) {
                                    constraint = object;
                                    break;
                                }
                            }
                            if(constraint != nil) {
                                //[constraint setConstant:265];
                                [constraint setActive:NO];
                            }
                        });
                        
                        if([((AlbumSF *)[albums objectAtIndex:0]) updateMediasList]) {  //get medias for the album
                            AlbumSF * album = (AlbumSF *)[albums objectAtIndex:0];
                            AlbumSN * albumSocial = [[AlbumSN alloc] init];
                            albumSocial.idAlbum = album.idAlbum;
                            albumSocial.name = album.name;
                            albumSocial.count = album.mediaCount;
                            albumSocial.idMedias = album.idMedias;
                            albumSocial.imageBIG = album.cover;
                            
                            for (Media *media in album.idMedias) {
                                
                                if(albumSocial.imageLEFT == nil) {
                                    albumSocial.imageLEFT = media.thumbnail;
                                }
                                else if(albumSocial.imageRIGHT == nil) {
                                    albumSocial.imageRIGHT = media.thumbnail;
                                }
                                else {
                                    break; // only want first 2
                                }
                            }
                            
                            [selfDelegate.albunsInstagram addObject:albumSocial];
                            [selfDelegate didReadAllInstagramAlbums];
                        }
                        else {
                            [instagram.txt_noAlbums setHidden:NO];
                            runOnMainQueueWithoutDeadlocking(^{
                                NSArray *constraints = [[self.view viewWithTag:2] constraints];
                                NSLayoutConstraint *constraint = nil;
                                for (NSLayoutConstraint *object in constraints) {
                                    if ( [object.identifier isEqualToString:@"heightInstagram"] ) {
                                        constraint = object;
                                        break;
                                    }
                                }
                                if(constraint != nil) {
                                    [constraint setConstant:80];
                                }
                            });
                            
                            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"NO_MEDIAS", nil)
                                                                                           message:NSLocalizedString(@"NO_MEDIAS_MESSAGE", nil)
                                                                                    preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                                  handler:^(UIAlertAction * action) {}];
                            
                            [alert addAction:defaultAction];
                            [selfDelegate presentViewController:alert animated:YES completion:nil];
                        }
                        
                    }
                });
            }
            instagram.albums = self.albunsInstagram;
        }
        else if(p_doLogin)
        {
            SocialAlbumsView *instagram = (SocialAlbumsView *)[self.view viewWithTag:2];
            [instagram.loadingSpinner setHidden:NO];
            [instagram.loadingSpinner startAnimating];
            
            LoginToSocialNetworkOperation *loginOp = [[LoginToSocialNetworkOperation alloc] initWithDelegate:self andSFApp:self.sfAppStep1 forSocialNetwork:SOCIAL_INSTAGRAM];
            self.sfAppStep1.operationsQueue.qualityOfService = NSQualityOfServiceUserInitiated;
            [self.sfAppStep1.operationsQueue addOperation:loginOp];
        }
        else // Not Connected to instagram
        {
            SocialAlbumsView *instagram = (SocialAlbumsView *)[self.view viewWithTag:2];
            instagram.isExpanded = NO;
            instagram.isLoaded = NO;
        }
        
        [self resizeSocial:((SocialAlbumsView *)[self.view viewWithTag:2])];
        
    } @catch (NSException *ex) {
        NSLog(@"StayLog: EXCEPTION readInstagramAlbumsFirstTimeWithLogin with error %@", ex.description);
    }
}

-(void)didReadAllInstagramAlbums {
    SocialAlbumsView *instagram = (SocialAlbumsView *)[self.view viewWithTag:2];
    
    [instagram.loadingSpinner stopAnimating];
    
    if(instagram.albums.count > 0)
    {
        [instagram removeGestureRecognizer:instagram.tapGestureRecognizer];
        [instagram.txt_noAlbums setHidden:YES];
        
        runOnMainQueueWithoutDeadlocking(^{
            NSArray *constraints = [[self.view viewWithTag:2] constraints];
            NSLayoutConstraint *constraint = nil;
            for (NSLayoutConstraint *object in constraints) {
                if ( [object.identifier isEqualToString:@"heightInstagram"] ) {
                    constraint = object;
                    break;
                }
            }
            if(constraint != nil) {
                [constraint setActive:NO];
            }
        });
    }
    
    [instagram.albumCollection reloadData];
    [self resizeSocial:instagram];
}

-(void)readGoogleAlbumsFirstTimeWithLogin:(BOOL)p_doLogin {
    @try {
        if(self.sfAppStep1.currentUser.networks != nil && [self.sfAppStep1.currentUser.networks containsObject:@"gplus"]) // Already connected to Google in Stayfilm
        {
            SocialAlbumsView *google = (SocialAlbumsView *)[self.view viewWithTag:3];
            
            google.isExpanded = NO;
            google.isLoaded = YES;
            [google.loadingSpinner setHidden:NO];
            [google.loadingSpinner startAnimating];
            
            if([self.albunsGoogle count] == 0) // haven't loaded instagram album yet
            {
                __weak typeof(self) selfDelegate = self;
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    NSMutableArray *albums = [AlbumSF getAlbumsFromUser:selfDelegate.sfAppStep1.currentUser fromNetwork:@"gplus" withOffset:0 withLimit:0];
                    if(albums != nil)
                    {
                        if([((AlbumSF *)[albums objectAtIndex:0]) updateMediasList]) {  //get medias for the album
                            AlbumSF * album = (AlbumSF *)[albums objectAtIndex:0];
                            AlbumSN * albumSocial = [[AlbumSN alloc] init];
                            albumSocial.idAlbum = album.idAlbum;
                            albumSocial.name = album.name;
                            albumSocial.idMedias = album.idMedias;
                            albumSocial.imageBIG = album.cover;
                            
                            for (Media *media in album.idMedias) {
                                
                                if(albumSocial.imageLEFT == nil) {
                                    albumSocial.imageLEFT = media.thumbnail;
                                }
                                else if(albumSocial.imageRIGHT == nil) {
                                    albumSocial.imageRIGHT = media.thumbnail;
                                }
                                else {
                                    break; // only want first 2
                                }
                            }
                            
                            [selfDelegate.albunsGoogle addObject:albumSocial];
                            [google.loadingSpinner stopAnimating];
                        }
                        else {
                            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"NO_MEDIAS", nil)
                                                                                           message:NSLocalizedString(@"NO_MEDIAS_MESSAGE", nil)
                                                                                    preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                                  handler:^(UIAlertAction * action) {}];
                            
                            [alert addAction:defaultAction];
                            [selfDelegate presentViewController:alert animated:YES completion:nil];
                        }
                        
                    }
                });
            }
            google.albums = self.albunsGoogle;
        }
        else if(p_doLogin)
        {
            SocialAlbumsView *google = (SocialAlbumsView *)[self.view viewWithTag:3];
            
            [google.loadingSpinner setHidden:NO];
            [google.loadingSpinner startAnimating];
            
            LoginToSocialNetworkOperation *loginOp = [[LoginToSocialNetworkOperation alloc] initWithDelegate:self andSFApp:self.sfAppStep1 forSocialNetwork:SOCIAL_GOOGLE];
            self.sfAppStep1.operationsQueue.qualityOfService = NSQualityOfServiceUserInitiated;
            [self.sfAppStep1.operationsQueue addOperation:loginOp];
        }
        else // Not Connected to gplus
        {
            SocialAlbumsView *google = (SocialAlbumsView *)[self.view viewWithTag:3];
            google.isExpanded = NO;
            google.isLoaded = NO;
        }
        
        SocialAlbumsView *google = (SocialAlbumsView *)[self.view viewWithTag:3];
        [self resizeSocial:google];
        
    } @catch (NSException *ex) {
        NSLog(@"StayLog: EXCEPTION readGoogleAlbumsFirstTimeWithLogin with error %@", ex.description);
    }
}

-(void)readStayfilmAlbums {
    @try {
        if(self.sfAppStep1.currentUser.networks != nil && [self.sfAppStep1.currentUser.networks containsObject:@"sf_album_manager"]) // Already connected to Google in Stayfilm
        {
            SocialAlbumsView *stayfilm = (SocialAlbumsView *)[self.view viewWithTag:4];
            
            stayfilm.isExpanded = NO;
            stayfilm.isLoaded = YES;
            [stayfilm.loadingSpinner setHidden:NO];
            [stayfilm.loadingSpinner startAnimating];
            
            if([self.albunsGoogle count] == 0) // haven't loaded instagram album yet
            {
                __weak typeof(self) selfDelegate = self;
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    NSMutableArray *albums = [AlbumSF getAlbumsFromUser:selfDelegate.sfAppStep1.currentUser fromNetwork:@"sf_album_manager" withOffset:0 withLimit:0];
                    if(albums != nil && albums.count > 0)
                    {
                        BOOL update = [((AlbumSF *)[albums objectAtIndex:0]) updateMediasList];
                        if(update) {  //get medias for the album
                            AlbumSF * album = (AlbumSF *)[albums objectAtIndex:0];
                            AlbumSN * albumSocial = [[AlbumSN alloc] init];
                            albumSocial.idAlbum = album.idAlbum;
                            albumSocial.name = album.name;
                            albumSocial.idMedias = album.idMedias;
                            albumSocial.imageBIG = album.cover;
                            
                            for (Media *media in album.idMedias) {
                                
                                if(albumSocial.imageLEFT == nil) {
                                    albumSocial.imageLEFT = media.thumbnail;
                                }
                                else if(albumSocial.imageRIGHT == nil) {
                                    albumSocial.imageRIGHT = media.thumbnail;
                                }
                                else {
                                    break; // only want first 2
                                }
                            }
                            
                            [selfDelegate.albunsStayfilm addObject:albumSocial];
                            [stayfilm.loadingSpinner stopAnimating];
                        }
                        else {
                            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"NO_MEDIAS", nil)
                                                                                           message:NSLocalizedString(@"NO_MEDIAS_MESSAGE", nil)
                                                                                    preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                                  handler:^(UIAlertAction * action) {}];
                            
                            [alert addAction:defaultAction];
                            [selfDelegate presentViewController:alert animated:YES completion:nil];
                        }
                    }
                    else {
                        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"NO_MEDIAS", nil)
                                                                                       message:NSLocalizedString(@"NO_MEDIAS_MESSAGE", nil)
                                                                                preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                              handler:^(UIAlertAction * action) {}];
                        
                        [alert addAction:defaultAction];
                        [selfDelegate presentViewController:alert animated:YES completion:nil];
                    }
                });
            }
            stayfilm.albums = self.albunsStayfilm;
        }
        else // Not Connected to stayfilm
        {
            SocialAlbumsView *stayfilm = (SocialAlbumsView *)[self.view viewWithTag:4];
            stayfilm.isExpanded = NO;
            stayfilm.isLoaded = NO;
        }
        
        SocialAlbumsView *stayfilm = (SocialAlbumsView *)[self.view viewWithTag:4];
        [self resizeSocial:stayfilm];
        
    } @catch (NSException *ex) {
        NSLog(@"StayLog: EXCEPTION readStayfilmAlbums with error %@", ex.description);
    }
}


#pragma mark - Album Viewer delegate calls

-(void)reloadAlbumsCounters {
    //TODO: realod info like self.title = [NSString stringWithFormat:NSLocalizedString(@"SELECTED_PHOTOS_COUNT", nil), [self countSelectedPhotos]];
    //[self.albumCollectionView reloadData];
}

-(void)changeSelectedMedias:(NSMutableArray *)p_selected
{
    if(self.sfAppStep1 == nil) {
        self.sfAppStep1 = [StayfilmApp sharedStayfilmAppSingleton];
    }
    self.sfAppStep1.selectedMedias = nil;
    self.sfAppStep1.selectedMedias = [p_selected mutableCopy];
    self.sfAppStep1.finalMedias = [p_selected mutableCopy];
    self.albumVC = nil;
    
    [self updateFilmStrip];
//    NSIndexSet *toBeRemoved = [self.sfAppStep1.finalMedias indexesOfObjectsPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
//        // The block is called for each object in the finalMedias array.
//        //BOOL removeIt = [obj isKindOfClass:[PhotoFB class]] || [obj isKindOfClass:[VideoFB class]] || [obj isKindOfClass:[PHAsset class]] || [obj isKindOfClass:[Media class]];
//        // YES if the element should be removed and NO otherwise
//        return removeIt;
//    }];
//    [self.sfAppStep1.finalMedias removeObjectsAtIndexes:toBeRemoved];
    
//    for (id media in self.sfAppStep1.selectedMedias) {
//        [self.sfAppStep1.finalMedias addObject:media];
//    }
}

-(void)changeEditedMedias:(NSMutableArray *)p_selected {
    if(self.sfAppStep1 == nil) {
        self.sfAppStep1 = [StayfilmApp sharedStayfilmAppSingleton];
    }
    self.sfAppStep1.editedMedias = nil;
    self.sfAppStep1.editedMedias = [p_selected mutableCopy];
    self.albumVC = nil;
    
    [self updateFilmStrip];
    
    self.initialEditedMedias = nil;
    
    if(self.uploaderStayfilm == nil) {
        self.uploaderStayfilm = [Uploader sharedUploadStayfilmSingletonWithDelegate:self];
    }
    int alreadyAddedFilesCount = [self.uploaderStayfilm addMedias:self.sfAppStep1.editedMedias];
    if(alreadyAddedFilesCount > 0 && self.sfAppStep1.isDebugMode)
    {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_ALREADY_ADDED_TITLE", nil)
                                                                       message:[NSString stringWithFormat:NSLocalizedString(@"UPLOADER_ALREADY_ADDED_MESSAGE", nil), alreadyAddedFilesCount]
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }

    //[self.navigationController popViewControllerAnimated:NO];
}

-(void)dismissFromAlbumViewer {
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)pickedSelectedMedias:(NSMutableArray *)p_selected {
    if(self.sfAppStep1 == nil) {
        self.sfAppStep1 = [StayfilmApp sharedStayfilmAppSingleton];
    }
    self.sfAppStep1.selectedMedias = nil;
    self.sfAppStep1.selectedMedias = [p_selected mutableCopy];
    self.sfAppStep1.finalMedias = [p_selected mutableCopy];
    self.albumVC = nil;
    
    [self updateFilmStrip];
    
    if(self.uploaderStayfilm == nil) {
        self.uploaderStayfilm = [Uploader sharedUploadStayfilmSingletonWithDelegate:self];
    }
    int alreadyAddedFilesCount = [self.uploaderStayfilm addMedias:self.sfAppStep1.finalMedias];
    if(alreadyAddedFilesCount > 0 && self.sfAppStep1.isDebugMode)
    {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_ALREADY_ADDED_TITLE", nil)
                                                                       message:[NSString stringWithFormat:NSLocalizedString(@"UPLOADER_ALREADY_ADDED_MESSAGE", nil), alreadyAddedFilesCount]
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

-(void)updateFilmStrip {
    [self.sfAppStep1.filmStrip updateFilmStrip];
}

#pragma mark - Login Operations Delegate

-(void)loginFacebookDidFinish:(LoginFacebookOperation *)loginOP {
    
    switch (loginOP.messageCode) {
        case LOGIN_SUCCESSFUL:
        {
            self.facebookloginAttempts = 0;
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                //Background Thread
                [self.sfAppStep1.currentUser setUserFacebookToken:self.sfAppStep1.settings[@"facebookToken"] andSFAppSingleton:self.sfAppStep1];
            });
            SocialAlbumsView *facebook = (SocialAlbumsView *)[self.view viewWithTag:1];
//            facebook.expandArrow.layer.anchorPoint = CGPointMake(0.5, 0.5);
//            [UIView animateWithDuration:0.3 animations:^{
//                facebook.expandArrow.transform = CGAffineTransformMakeRotation(90 * M_PI / 180.0);
//            }];
            [facebook.txt_noAlbums setHidden:YES];
            facebook.isLoadingInfo = YES;
            [self readFacebookAlbumsFirstTimeWithLogin:NO];
            
        }
            break;
        case DIFFERENT_USER:
        {
            runOnMainQueueWithoutDeadlocking(^{
                SocialAlbumsView *facebook = (SocialAlbumsView *)[self.view viewWithTag:1];
                [facebook.loadingSpinner setHidden:NO];
                
                
                dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                    //save facebook token to this user
                    BOOL success = [self.sfAppStep1.currentUser setUserFacebookToken:self.sfAppStep1.settings[@"facebookToken"] andSFAppSingleton:self.sfAppStep1];
                    
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        //Run UI Updates
                        [facebook.loadingSpinner setHidden:YES];
                        if(!success) {
                            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"FACEBOOK_FAILED_TOKEN", nil)
                                                                                           message:NSLocalizedString(@"FACEBOOK_FAILED_TOKEN_MESSAGE", nil)
                                                                                    preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                                  handler:^(UIAlertAction * action) {}];
                            
                            [alert addAction:defaultAction];
                            [self presentViewController:alert animated:YES completion:nil];
                            [self readFacebookAlbumsFirstTimeWithLogin:YES];
                            return;
                        }
                        self.facebookloginAttempts = 0;
                        [self readFacebookAlbumsFirstTimeWithLogin:NO];
                    });
                });
            });
        }
            break;
            
        default:
        {
            ++self.facebookloginAttempts;
        }
            break;
    }
}

-(void)loginFacebookDidFail:(LoginFacebookOperation *)loginOP {
    
    ++self.facebookloginAttempts;
    if(self.facebookloginAttempts >= 3)
    {
        runOnMainQueueWithoutDeadlocking(^{
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"FACEBOOK_FALIED", nil)
                                                                           message:NSLocalizedString(@"FACEBOOK_FALIED_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
            self.facebookloginAttempts = 0;
        });
        return;
    }
    
    BOOL retryLogin = NO;
    switch (loginOP.messageCode) {
        case CONNECTION_ERROR:
        {
            retryLogin = YES;
            runOnMainQueueWithoutDeadlocking(^{
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CONNECTION_ERROR", nil)
                                                                               message:NSLocalizedString(@"CONNECTION_ERROR_MESSAGE", nil)
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {}];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
            });
        }
            break;
        case USER_CANCELLED:
        {
            retryLogin = NO;
            runOnMainQueueWithoutDeadlocking(^{
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"USER_CANCELLED", nil)
                                                                               message:NSLocalizedString(@"USER_CANCELLED_MESSAGE", nil)
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {}];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
            });
        }
            break;
        case LOGIN_FAILED:
        {
            retryLogin = YES;
            runOnMainQueueWithoutDeadlocking(^{
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"LOGIN_FAILED", nil)
                                                                               message:NSLocalizedString(@"LOGIN_FAILED_MESSAGE", nil)
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {}];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
            });
        }
            break;
        case REGISTRATION_ERROR:
        {
            retryLogin = YES;
            runOnMainQueueWithoutDeadlocking(^{
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"REGISTRATION_ERROR", nil)
                                                                               message:NSLocalizedString(@"REGISTRATION_ERROR_MESSAGE", nil)
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {}];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
            });
        }
            break;
        case NO_CONFIG_LOADED:
        {
            retryLogin = YES;
            self.facebookloginAttempts--;
            // say nothing, just try again
            if(self.sfAppStep1 == nil)
                self.sfAppStep1 = [StayfilmApp sharedStayfilmAppSingleton];
            
            self.sfAppStep1.sfConfig = [SFConfig getAllConfigsFromWS:self.sfAppStep1.wsConfig];
            break;
        }
        case NO_INTERNET_CONNECTION:
        {
            retryLogin = NO;
            self.facebookloginAttempts--;
            runOnMainQueueWithoutDeadlocking(^{
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"NO_INTERNET_CONNECTION", nil)
                                                                               message:NSLocalizedString(@"NO_INTERNET_CONNECTION_MESSAGE", nil)
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {}];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
            });
            
            [NSTimer scheduledTimerWithTimeInterval:2.0 repeats:YES block:^(NSTimer * _Nonnull timer) {
                if([self connected])
                {
                    [timer invalidate];
                    timer = nil;
                    LoginFacebookOperation *loginOp = [[LoginFacebookOperation alloc] initWithDelegate:self andSFApp:self.sfAppStep1];
                    [self.sfAppStep1.operationsQueue addOperation:loginOp];
                }
            }];
            break;
        }
        case -1:
        {
            retryLogin = YES;
            NSLog(@"StayLog: loginDidFail with EXCEPTION (messageCode -1)");
            runOnMainQueueWithoutDeadlocking(^{
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"LOGIN_FAILED", nil)
                                                                               message:NSLocalizedString(@"LOGIN_FAILED_MESSAGE", nil)
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {}];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
            });
        }
            break;
        default:
        {
            retryLogin = YES;
        }
            break;
    }
    
    if(retryLogin)
    {
        LoginFacebookOperation *logOperation = [[LoginFacebookOperation alloc] initWithDelegate:self andSFApp:self.sfAppStep1];
        [self.sfAppStep1.operationsQueue addOperation:logOperation];
    }
    else
    {
        SocialAlbumsView *facebook = (SocialAlbumsView *)[self.view viewWithTag:1];
        facebook.isExpanded = NO;
        facebook.isLoaded = NO;
    }
    
    [loginOP cancel];
}


- (void)loginToSocialNetworkDidSucceed:(LoginToSocialNetworkOperation *)loginOP; {
    switch (loginOP.messageCode) {
        case LOGIN_SUCCESSFUL:
        {
            self.socialLoginAttempts = 0;
            if(loginOP.socialNetwork == SOCIAL_INSTAGRAM)
                [self readInstagramAlbumsFirstTimeWithLogin:NO];
            else if(loginOP.socialNetwork == SOCIAL_GOOGLE)
                [self readGoogleAlbumsFirstTimeWithLogin:NO];
        }
            break;
            
        default:
        {
            ++self.socialLoginAttempts;
        }
            break;
    }
}
- (void)loginToSocialNetworkDidFail:(LoginToSocialNetworkOperation *)loginOP {
    ++self.socialLoginAttempts;
    if(self.socialLoginAttempts >= 3)
    {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"SOCIAL_FALIED", nil)
                                                                       message:NSLocalizedString(@"SOCIAL_FALIED_MESSAGE", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        self.socialLoginAttempts = 0;
        return;
    }
    
    BOOL retryLogin = NO;
    switch (loginOP.messageCode) {
        case CONNECTION_ERROR:
        {
            retryLogin = YES;
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CONNECTION_ERROR", nil)
                                                                           message:NSLocalizedString(@"CONNECTION_ERROR_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
            break;
        case USER_CANCELLED:
        {
            retryLogin = NO;
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"USER_CANCELLED", nil)
                                                                           message:NSLocalizedString(@"USER_CANCELLED_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
            break;
        case LOGIN_FAILED:
        {
            retryLogin = YES;
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"LOGIN_FAILED", nil)
                                                                           message:NSLocalizedString(@"LOGIN_FAILED_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
            break;
        case REGISTRATION_ERROR:
        {
            retryLogin = YES;
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"REGISTRATION_ERROR", nil)
                                                                           message:NSLocalizedString(@"REGISTRATION_ERROR_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
            break;
        case NO_CONFIG_LOADED:
        {
            retryLogin = YES;
            self.socialLoginAttempts--;
            // say nothing, just try again
            if(self.sfAppStep1 == nil)
                self.sfAppStep1 = [StayfilmApp sharedStayfilmAppSingleton];
            
            self.sfAppStep1.sfConfig = [SFConfig getAllConfigsFromWS:self.sfAppStep1.wsConfig];
            break;
        }
        case NO_INTERNET_CONNECTION:
        {
            retryLogin = NO;
            self.socialLoginAttempts--;
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"NO_INTERNET_CONNECTION", nil)
                                                                           message:NSLocalizedString(@"NO_INTERNET_CONNECTION_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
            
            if(loginOP.socialNetwork == SOCIAL_INSTAGRAM) {
                [NSTimer scheduledTimerWithTimeInterval:2.0 repeats:YES block:^(NSTimer * _Nonnull timer) {
                    if([self connected])
                    {
                        [timer invalidate];
                        timer = nil;
                        LoginToSocialNetworkOperation *loginOp = [[LoginToSocialNetworkOperation alloc] initWithDelegate:self andSFApp:self.sfAppStep1 forSocialNetwork:SOCIAL_INSTAGRAM];
                        [self->sfAppStep1.operationsQueue addOperation:loginOp];
                    }
                }];
            } else if (loginOP.socialNetwork == SOCIAL_GOOGLE) {
                [NSTimer scheduledTimerWithTimeInterval:2.0 repeats:YES block:^(NSTimer * _Nonnull timer) {
                    if([self connected])
                    {
                        [timer invalidate];
                        timer = nil;
                        LoginToSocialNetworkOperation *loginOp = [[LoginToSocialNetworkOperation alloc] initWithDelegate:self andSFApp:self.sfAppStep1 forSocialNetwork:SOCIAL_GOOGLE];
                        [self->sfAppStep1.operationsQueue addOperation:loginOp];
                    }
                }];
            }
            break;
        }
        case -1:
        {
            retryLogin = YES;
            NSLog(@"StayLog: loginDidFail with EXCEPTION (messageCode -1)");
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"LOGIN_FAILED", nil)
                                                                           message:NSLocalizedString(@"LOGIN_FAILED_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
            break;
        default:
        {
            retryLogin = YES;
        }
            break;
    }
    
    if(retryLogin)
    {
        LoginToSocialNetworkOperation *logOperation = [[LoginToSocialNetworkOperation alloc] initWithDelegate:self andSFApp:self.sfAppStep1 forSocialNetwork:loginOP.socialNetwork];
        [self.sfAppStep1.operationsQueue addOperation:logOperation];
    }
    else
    {
        switch (loginOP.socialNetwork) {
            case SOCIAL_INSTAGRAM:
                {
                    SocialAlbumsView *instagram = (SocialAlbumsView *)[self.view viewWithTag:2];
                    instagram.albums = self.albunsInstagram;
                    instagram.isExpanded = NO;
                    instagram.isLoaded = NO;
                }
                break;
            case SOCIAL_GOOGLE:
                {
                    SocialAlbumsView *google = (SocialAlbumsView *)[self.view viewWithTag:3];
                    google.albums = self.albunsStayfilm;
                    google.isExpanded = NO;
                    google.isLoaded = NO;
                }
            default:
                break;
        }
    }
    
    [loginOP cancel];
}

-(void)loginSocialWebviewDidSucceed:(NSArray *)p_token {
    @try {
        if ([[p_token objectAtIndex:0] isEqualToString:@"instagram"])
        {
            BOOL success = [self.sfAppStep1.currentUser setUserInstagramToken:[p_token objectAtIndex:1] andSFAppSingleton:self.sfAppStep1];
            if(success)
            {
                [Job postSocialNetwork:@"instagram"];
                [self readInstagramAlbumsFirstTimeWithLogin:NO];
                self.socialLoginAttempts = 0;
            }
            else
            {
                if(self.socialLoginAttempts < 3) {
                    ++self.socialLoginAttempts;
                    [self readInstagramAlbumsFirstTimeWithLogin:YES];
                }
                else
                {
                    self.socialLoginAttempts = 0;
                    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"LOGIN_FAILED", nil)
                                                                                   message:NSLocalizedString(@"LOGIN_FAILED_MESSAGE", nil)
                                                                            preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                          handler:^(UIAlertAction * action) {}];
                    
                    [alert addAction:defaultAction];
                    [self presentViewController:alert animated:YES completion:nil];
                }
            }
        } else if ([[p_token objectAtIndex:0] isEqualToString:@"gplus"])
        {
            BOOL success = [self.sfAppStep1.currentUser setUserGoogleToken:[p_token objectAtIndex:1]  andSFAppSingleton:self.sfAppStep1];
            if(success)
            {
                [self readGoogleAlbumsFirstTimeWithLogin:NO];
                self.socialLoginAttempts = 0;
            }
            else
            {
                if(self.socialLoginAttempts < 3) {
                    ++self.socialLoginAttempts;
                    [self readGoogleAlbumsFirstTimeWithLogin:YES];
                }
                else
                {
                    self.socialLoginAttempts = 0;
                    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"LOGIN_FAILED", nil)
                                                                                   message:NSLocalizedString(@"LOGIN_FAILED_MESSAGE", nil)
                                                                            preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                          handler:^(UIAlertAction * action) {}];
                    
                    [alert addAction:defaultAction];
                    [self presentViewController:alert animated:YES completion:nil];
                }
            }
        }
    } @catch (NSException *ex) {
        NSLog(@"StayLog: EXCEPTION in loginSocialWebViewDidSucceed with error: %@", ex.description);
    }
}

-(void)loginSocialWebviewDidFail:(NSArray *)error {
    //TODO: still need to do this
    
}

#pragma mark - Popover Delegates


- (void)dismissModalMenu {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Animation Delegates

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source {
    return self.transitionAnimation;
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    self.transitionAnimation.isPresenting = NO;
    return self.transitionAnimation;
}


#pragma mark - Gesture Recognizers

- (IBAction)but_CameraRoll_Clicked:(UITapGestureRecognizer *)sender {
    [self cameraTapped:sender];
}


-(void)cameraTapped:(UIGestureRecognizer *)gesture
{
    if(!self.isOpeningUploader)
    {
        self.isOpeningUploader = YES;
        
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"source"
                                                              action:@"camera_roll"
                                                               label:@""
                                                               value:@1] build]];
        
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            switch (status) {
                case PHAuthorizationStatusAuthorized:
                {
                    runOnMainQueueWithoutDeadlocking(^{
                        [self.camera_spinner startAnimating];
                        [self.camera_spinner setHidden:NO];
                        
                        AlbumInternal *album = [[AlbumInternal alloc] init];
                        
                        if(self.imageManager == nil)
                        {
                            self.imageManager = [[PHCachingImageManager alloc] init];
                        }
                        PHFetchOptions *options = [[PHFetchOptions alloc] init];
                        
                        options.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];
                        self.assetsFetchResults = [PHAsset fetchAssetsWithOptions:options];

                        album.title = NSLocalizedString(@"CAMERA_ROLL", nil);
            
                        for (PHAsset *item in self.assetsFetchResults) {
                            [album.medias addObject:item];
                        }

                        self.albumVC = nil;
                        self.albumVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AlbumViewer"];
                        self.albumVC.selectedMedias = nil;
                        if(self.sfAppStep1.isEditingMode) {
                            self.albumVC.selectedMedias = [self.sfAppStep1.editedMedias mutableCopy];
                        } else {
                            self.albumVC.selectedMedias = [self.sfAppStep1.finalMedias mutableCopy];
                        }
                        self.albumVC.albumFB = nil;
                        self.albumVC.albumGeneric = album;
                        
                        [self.navigationController showViewController:self.albumVC sender:self];
                    });
                }
                    break;
                case PHAuthorizationStatusDenied:
                case PHAuthorizationStatusRestricted:
                case PHAuthorizationStatusNotDetermined:
                {
                    runOnMainQueueWithoutDeadlocking(^{
                        Popover_PhotoAccessPermission_ViewController * view = [[Popover_PhotoAccessPermission_ViewController alloc] initWithDelegate:self];
                        view.modalPresentationStyle = UIModalPresentationOverFullScreen;
                        view.transitioningDelegate = self;
                        view.view.frame = self.view.frame;
                        self.transitionAnimation.duration = 0.3;
                        self.transitionAnimation.isPresenting = YES;
                        self.transitionAnimation.originFrame = self.view.frame;
                        
                        [self presentViewController:view animated:YES completion:nil];
                    });
                }
                    break;
                    
                default:
                    break;
            }
            self.isOpeningUploader = NO;
        }];
    }
}

BOOL isSegueActivated = NO;
- (void)but_filmStrip_Clicked {
    if(!isSegueActivated) {
        isSegueActivated = YES;
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"film-strip"
                                                              action:@"content-sources"
                                                               label:@""
                                                               value:@1] build]];
        
        [self performSegueWithIdentifier:@"Step1ToConfirmationSegue" sender:self];
        isSegueActivated = NO;
    }
}

- (void)but_NextStep_Clicked {
    if(!isSegueActivated) {
        isSegueActivated = YES;
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"film-production"
                                                              action:@"next"
                                                               label:@"content-sources"
                                                               value:@1] build]];
        if(self.sfAppStep1.selectedGenre != nil && self.sfAppStep1.selectedTemplate != nil && self.sfAppStep1.canSkipStyles){
            [self pickedSelectedMedias:self.sfAppStep1.selectedMedias];
            self.uploaderStayfilm = [Uploader sharedUploadStayfilmSingletonWithDelegate:self];
            [self.sfAppStep1.filmStrip setHidden:YES];
            [self performSegueWithIdentifier:@"Step1ToProgressSegue" sender:self];
        }else{
        //[self performSegueWithIdentifier:@"Step1ToStyleSegue" sender:self];
          [self performSegueWithIdentifier:@"Step1ToConfirmationSegue" sender:self];
            
        }
        isSegueActivated = NO;
    }
}

- (IBAction)but_edit_Cancel_Clicked:(id)sender {
    self.sfAppStep1.editedMedias = self.initialEditedMedias;
    
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"edit-film"
                                                          action:@"add-media"
                                                           label:@"cancel"
                                                           value:@1] build]];
    
    [self.navigationController popViewControllerAnimated:YES];
    
//    for (UIViewController *view in self.flowNavigationController.viewControllers) {
//        if([view isKindOfClass:[Step1ConfirmationViewController class]])
//        {
//            [view performSelectorOnMainThread:@selector(clearAlbumViewController) withObject:nil waitUntilDone:NO];
//            break;
//        }
//    }
//    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)but_edit_Confirm_Clicked:(id)sender {
    //self.sfAppStep1.finalMedias = [self.sfAppStep1.editedMedias mutableCopy];
    self.initialEditedMedias = nil;
    
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"edit-film"
                                                          action:@"add-media"
                                                           label:@"ok"
                                                           value:@1] build]];
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)facebookTapped:(UIGestureRecognizer *)gesture
{
    if((!self.isSelectingSocial) || (!self.calledAlbumsFistTime)) {
        self.isSelectingSocial = YES;
        self.calledAlbumsFistTime = YES;
        SocialAlbumsView *facebook = (SocialAlbumsView *)[self.view viewWithTag:1];
        if(!facebook.isLoaded) {
            [self readFacebookAlbumsFirstTimeWithLogin:YES];
            //Google Analytics Event
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"source"
                                                                  action:@"facebook"
                                                                   label:@""
                                                                   value:@1] build]];
            self.isSelectingSocial = NO;
            return;
        }
        if(facebook.isLoaded)
        {
            [self resizeSocial:facebook];
        }
        else if(self.sfAppStep1.currentUser.networks != nil && [self.sfAppStep1.currentUser.networks containsObject:@"facebook"]) // already has token
        {
            [self readFacebookAlbumsFirstTimeWithLogin:NO];
        }
        else
        {
            [self readFacebookAlbumsFirstTimeWithLogin:YES];
        }
        self.isSelectingSocial = NO;
    }
}

-(void)instagramTapped:(UIGestureRecognizer *)gesture
{
    if(!self.isSelectingSocial) {
        self.isSelectingSocial = YES;
        SocialAlbumsView *instagram = (SocialAlbumsView *)[self.view viewWithTag:2];
        if(instagram.isLoaded)
        {
            //TODO: Expand all the way
        }
        else if(self.sfAppStep1.currentUser.networks != nil && [self.sfAppStep1.currentUser.networks containsObject:@"instagram"]) // already has token
        {
            [self readInstagramAlbumsFirstTimeWithLogin:NO];
        }
        else
        {
            [self readInstagramAlbumsFirstTimeWithLogin:YES];
        }
        self.isSelectingSocial = NO;
    }
}

-(void)googleTapped:(UIGestureRecognizer *)gesture
{
    if(!self.isSelectingSocial) {
        self.isSelectingSocial = YES;
        SocialAlbumsView *google = (SocialAlbumsView *)[self.view viewWithTag:3];
        if(google.isLoaded)
        {
            //TODO: Expand all the way
        }
        else if(self.sfAppStep1.currentUser.networks != nil && [self.sfAppStep1.currentUser.networks containsObject:@"gplus"]) // already has token
        {
            [self readGoogleAlbumsFirstTimeWithLogin:NO];
        }
        else
        {
            [self readGoogleAlbumsFirstTimeWithLogin:YES];
        }
        self.isSelectingSocial = NO;
    }
}

-(void)stayfilmTapped:(UIGestureRecognizer *)gesture
{
    if(!self.isSelectingSocial) {
        self.isSelectingSocial = YES;
        SocialAlbumsView *stayfilm = (SocialAlbumsView *)[self.view viewWithTag:4];
        if(stayfilm.isLoaded)
        {
            //TODO: Expand all the way and check Stayfilm network tag
        }
        else if(self.sfAppStep1.currentUser.networks != nil && [self.sfAppStep1.currentUser.networks containsObject:@"sf_album_manager"]) // already has token
        {
            [self readStayfilmAlbums];
        }
        else
        {
            //TODO: No albums
        }
        self.isSelectingSocial = NO;
    }
}

#pragma mark - Albums Collection Delegates

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if(collectionView.tag == 1)
        return [self.albunsFacebook count];
    else if (collectionView.tag == 2)
        return self.albunsInstagram.count;
    else if (collectionView.tag == 3)
        return self.albunsGoogle.count;
    else if (collectionView.tag == 4)
        return self.albunsStayfilm.count;
    else if(collectionView.tag == 100)
        return [self.albunsSuggestion count];
    return 0;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    @try {
        if((collectionView.tag != 100)) // Socials  /  Tag 100 is Suggestion Collection View
        {
            AlbumSocialCollectionViewCell * cell = (AlbumSocialCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cellAlbumsSN" forIndexPath:indexPath];
            if(cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AlbumSocialCollectionViewCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            int index = (int)indexPath.item;
            
            NSArray *albums = nil;
            SocialAlbumsView *collection = nil;
            if(collectionView.tag == 1){
                albums = self.albunsFacebook;
                collection = (SocialAlbumsView *)[self.view viewWithTag:1];
            }
//            else if(collectionView.tag == 2)
//            {
//                albums = self.albunsInstagram;
//                collection = (SocialAlbumsView *)[self.view viewWithTag:2];
//            }
//            else if(collectionView.tag == 3)
//            {
//                albums = self.albunsGoogle;
//                collection = (SocialAlbumsView *)[self.view viewWithTag:3];
//            }
//            else if(collectionView.tag == 4)
//            {
//                albums = self.albunsStayfilm;
//                collection = (SocialAlbumsView *)[self.view viewWithTag:4];
//            }
//            else
//                return cell;
            
            cell.album = [albums objectAtIndex:index];
            [cell initCell];
            
            return cell;
        }
    
    } @catch (NSException *ex) {
        NSLog(@"StayLog: EXCEPTION in moviemakerSourceTableCell collectionView tag %ld cellforitematindexpath with error: %@", (long)collectionView.tag, ex.description);
    }
    return nil;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    @try
    {
        if(collectionView.tag != 100) { //Album Social Network
            if (!self.isSelectingAlbum)
            {
                self.isSelectingAlbum = YES;
                AlbumSocialCollectionViewCell * cell = (AlbumSocialCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cellAlbumsSN" forIndexPath:indexPath];
                if(cell == nil)
                {
                    self.isSelectingAlbum = NO;
                    return;
                }
                
                int index = (int)indexPath.item;
                
                SocialAlbumsView *collection = nil;
                if(collectionView.tag == 1)
                {
                    collection = (SocialAlbumsView *)[self.view viewWithTag:1];
                }
                else if(collectionView.tag == 2)
                {
                    collection = (SocialAlbumsView *)[self.view viewWithTag:2];
                }
                else if(collectionView.tag == 3)
                {
                    collection = (SocialAlbumsView *)[self.view viewWithTag:3];
                }
                else if(collectionView.tag == 4)
                {
                    collection = (SocialAlbumsView *)[self.view viewWithTag:4];
                }
                else {
                    self.isSelectingAlbum = NO;
                    return;
                }
                
                self.albumVC = nil;
                self.albumVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AlbumViewer"];
                self.albumVC.selectedMedias = nil;
                if(self.sfAppStep1.isEditingMode) {
                    self.albumVC.selectedMedias = [self.sfAppStep1.editedMedias mutableCopy];
                } else {
                    self.albumVC.selectedMedias = [self.sfAppStep1.finalMedias mutableCopy];
                }
                self.albumVC.albumFB = [collection.albums objectAtIndex:index];
                self.albumVC.albumGeneric = nil;
                self.albumVC.albumFB.after = nil;
                if(![self.albumVC.albumFB.idAlbum isEqualToString:@"videos"]){
                [self.albumVC getAllPhotosFromFacebookAlbum];
                }
                [self.navigationController showViewController:self.albumVC sender:self];
                
                self.isSelectingAlbum = NO;
            }
        }
//        else if(collectionView.tag == 100) // Albums Suggestion
//        {
//            if (!self.isSelectingAlbum)
//            {
//                self.isSelectingAlbum = YES;
//                static NSString *cellIdentifier = @"AlbumInternalCellIdentifier";
//                MovieMakerSuggestionAlbumCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
//                if(cell == nil)
//                {
//                    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MovieMakerSuggestionAlbumCollectionViewCell" owner:self options:nil];
//                    cell = [nib objectAtIndex:0];
//                }
//
//                self.albumVC = nil;
//                self.albumVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AlbumViewer"];
//                self.albumVC.selectedMedias = [self.sfAppStep1.selectedMedias mutableCopy];
//                self.albumVC.albumGeneric = [self.albunsSuggestion objectAtIndex:indexPath.item];
//                self.albumVC.albumFB = nil;
//
//                //self.comeBackFromAlbum = YES;
//                [self.navigationController showViewController:self.albumVC sender:self];
//                self.isSelectingAlbum = NO;
//            }
//
//        }
    } @catch (NSException *ex) {
        NSLog(@"StayLog: EXCEPTION in MovieMakerStep1 collectionView:didSelectItem with error %@", ex.description);
    }
}
-(void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    SocialAlbumsView *collection = nil;
    if(collectionView.tag == 1)
    {
        collection = (SocialAlbumsView *)[self.view viewWithTag:1];
    }
    else if(collectionView.tag == 2)
    {
        collection = (SocialAlbumsView *)[self.view viewWithTag:2];
    }
    else if(collectionView.tag == 3)
    {
        collection = (SocialAlbumsView *)[self.view viewWithTag:3];
    }
    else if(collectionView.tag == 4)
    {
        collection = (SocialAlbumsView *)[self.view viewWithTag:4];
    }
    else {
        self.isSelectingAlbum = NO;
        return;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    //Dinamically adjust albums cell size to fit all screens
    
    CGFloat width  = self.view.frame.size.width;
    width -= width*0.2;
    width /= 2;
    
    // Using size 150x209 as size reference
    
    CGFloat proportionPercent = width * 100 / 150;
    return CGSizeMake(width,209*(proportionPercent/100));
}


#pragma mark - Uploader Delegates


-(void)uploadProgressChanged:(NSNumber *)progress
{
    // Not important on this page.
}

- (void)converterDidFailMedia:(NSString *)type
{
    NSString *message = nil;
    if(self.sfAppStep1 == nil)
    {
        self.sfAppStep1 = [StayfilmApp sharedStayfilmAppSingleton];
    }
    if (self.uploaderStayfilm.listUploadMedias.count + self.sfAppStep1.selectedMedias.count < [self.sfAppStep1.sfConfig.min_photos intValue]) {
        message = [NSString stringWithFormat:NSLocalizedString(@"UPLOADER_CONVERT_FAILED_UPLOAD_AGAIN_MESSAGE", nil), [self.sfAppStep1.sfConfig.min_photos intValue] - (self.uploaderStayfilm.listUploadMedias.count + self.sfAppStep1.selectedMedias.count)];
    }
    else if ([type isEqualToString:@"image"]) {
        message = NSLocalizedString(@"UPLOADER_CONVERT_FAILED_IMAGE_MESSAGE", nil);
    }
    else
    {
        message = NSLocalizedString(@"UPLOADER_CONVERT_FAILED_VIDEO_MESSAGE", nil);
    }
    
    if(self.sfAppStep1.isEditingMode) {
        @synchronized (self.sfAppStep1.editedMedias) {
            for (id obj in self.sfAppStep1.editedMedias) {
                if([obj isKindOfClass:[PHAsset class]]) {
                    BOOL find = NO;
                    for (PHAsset *asset in self.uploaderStayfilm.listUploadMedias) {
                        if(asset == obj) {
                            find = YES;
                            break;
                        }
                    }
                    if(!find) {
                        NSUInteger i = [self.sfAppStep1.editedMedias indexOfObject:obj];
                        [self.sfAppStep1.editedMedias removeObjectAtIndex:i];
                        break;
                    }
                }
            }
        }
    } else {
        @synchronized (self.sfAppStep1.finalMedias) {
            for (id obj in self.sfAppStep1.finalMedias) {
                if([obj isKindOfClass:[PHAsset class]]) {
                    BOOL find = NO;
                    for (PHAsset *asset in self.uploaderStayfilm.listUploadMedias) {
                        if(asset == obj) {
                            find = YES;
                            break;
                        }
                    }
                    if(!find) {
                        NSUInteger i = [self.sfAppStep1.finalMedias indexOfObject:obj];
                        [self.sfAppStep1.finalMedias removeObjectAtIndex:i];
                        break;
                    }
                }
            }
        }
    }
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_CONVERT_FAILED_TITLE", nil)
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)uploadDidFailMedia:(NSString *)type
{
    NSString *message = nil;
    if(self.sfAppStep1 == nil)
    {
        self.sfAppStep1 = [StayfilmApp sharedStayfilmAppSingleton];
    }
    if (self.uploaderStayfilm.listUploadMedias.count + self.sfAppStep1.selectedMedias.count < [self.sfAppStep1.sfConfig.min_photos intValue]) {
        message = [NSString stringWithFormat:NSLocalizedString(@"UPLOADER_UPLOAD_FAILED_UPLOAD_AGAIN_MESSAGE", nil), [self.sfAppStep1.sfConfig.min_photos intValue] - (self.uploaderStayfilm.listUploadMedias.count + self.sfAppStep1.selectedMedias.count)];
    }
    else if ([type isEqualToString:@"image"]) {
        message = NSLocalizedString(@"UPLOADER_UPLOAD_FAILED_IMAGE_MESSAGE", nil);
    }
    else
    {
        message = NSLocalizedString(@"UPLOADER_UPLOAD_FAILED_VIDEO_MESSAGE", nil);
    }
    
    if(self.sfAppStep1.isEditingMode) {
        @synchronized (self.sfAppStep1.editedMedias) {
            for (id obj in self.sfAppStep1.editedMedias) {
                if([obj isKindOfClass:[PHAsset class]]) {
                    BOOL find = NO;
                    for (PHAsset *asset in self.uploaderStayfilm.listUploadMedias) {
                        if(asset == obj) {
                            find = YES;
                            break;
                        }
                    }
                    if(!find) {
                        NSUInteger i = [self.sfAppStep1.editedMedias indexOfObject:obj];
                        [self.sfAppStep1.editedMedias removeObjectAtIndex:i];
                        break;
                    }
                }
            }
        }
    } else {
        @synchronized (self.sfAppStep1.finalMedias) {
            for (id obj in self.sfAppStep1.finalMedias) {
                if([obj isKindOfClass:[PHAsset class]]) {
                    BOOL find = NO;
                    for (PHAsset *asset in self.uploaderStayfilm.listUploadMedias) {
                        if(asset == obj) {
                            find = YES;
                            break;
                        }
                    }
                    if(!find) {
                        NSUInteger i = [self.sfAppStep1.finalMedias indexOfObject:obj];
                        [self.sfAppStep1.finalMedias removeObjectAtIndex:i];
                        break;
                    }
                }
            }
        }
    }
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_UPLOAD_FAILED_TITLE", nil)
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)uploadDidFail:(Uploader *)uploader
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_ERROR_TITLE", nil)
                                                                   message:NSLocalizedString(@"UPLOADER_ERROR_MESSAGE", nil)
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    
    [self reloadAlbumsCounters];
}

-(void)uploadDidStartSendingConfirmationMedias:(Uploader *)uploader
{
    // not important on this page.
}

-(void)uploadDidStart
{
    [self reloadAlbumsCounters];
}

-(void)uploadDidFinish:(Uploader *)uploader
{
    if(self.sfAppStep1 == nil) {
        self.sfAppStep1 = [StayfilmApp sharedStayfilmAppSingleton];
    }
    if(self.uploaderStayfilm.listUploadedMedias != nil && self.uploaderStayfilm.listUploadedMedias.count > 0) {
        for (PHAsset * asset in self.uploaderStayfilm.listUploadedMedias.allValues) {
            if(self.sfAppStep1.isEditingMode) {
                if(![self.sfAppStep1.editedMedias containsObject:asset]) {
                    [self.sfAppStep1.editedMedias addObject:asset];
                }
            } else {
                if(![self.sfAppStep1.finalMedias containsObject:asset]) {
                    [self.sfAppStep1.finalMedias addObject:asset];
                }
            }
        }
    }
    
    if(self.sfAppStep1.isDebugMode) {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_FINISHED", nil)
                                                                       message:[[NSString alloc] initWithFormat:NSLocalizedString(@"UPLOADED_X_MEDIAS", nil), uploader.listUploadedMedias.count]
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
    [self reloadAlbumsCounters];
}

-(void)uploadFinishedMedia:(PHAsset *)asset {
    // not important on this view
}


#pragma mark - StoryState delegate

-(void)storiesChanged {
    
    // Do nothing
}


#pragma mark - Navigation


BOOL isMovingToConfirmation = NO;
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if ([segue.identifier isEqualToString:@"Step1ConfirmationSegue"] && !isMovingToConfirmation) {
        isMovingToConfirmation = YES;
        Step1ConfirmationViewController *step1confirm = segue.destinationViewController;
        step1confirm.videoImageManager = self.imageManager;
        isMovingToConfirmation = NO;
    }
}

- (IBAction)goToProfile:(id)sender {
    [self.profileImage setUserInteractionEnabled:NO];
    
    UIStoryboard *tutorialStoryboard = [UIStoryboard storyboardWithName:@"Profile" bundle:[NSBundle mainBundle]];
    
    
    ProfileViewController *profileVC = [tutorialStoryboard instantiateViewControllerWithIdentifier:@"profile"];
    profileVC.sfAppProfile = self.sfAppStep1;
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:profileVC];
    
    [self presentViewController:navigationController animated:YES completion:nil];
    
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"profile"
                                                          action:@"access"
                                                           label:@""
                                                           value:@1] build]];
    
    //[self performSegueWithIdentifier:@"goToProfile" sender:nil];
}

- (IBAction)backButtonPrassed:(id)sender {
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"back"
                                                          action:@" "
                                                           label:@""
                                                           value:@1] build]];
    
//    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
