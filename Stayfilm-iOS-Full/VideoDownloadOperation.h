//
//  VideoDownloadOperation.h
//  Stayfilm for Messenger
//
//  Created by Henrique Ormonde on 10/11/15.
//  Copyright © 2015 Stayfilm. All rights reserved.
//

@import Foundation;
#import "Movie.h"


@protocol VideoDownloaderDelegate;

@interface VideoDownloadOperation : NSOperation <NSURLSessionDelegate, NSURLSessionTaskDelegate, NSURLSessionDataDelegate>

@property (nonatomic, assign) id <VideoDownloaderDelegate> delegate;
@property (nonatomic, strong) NSURLSession *sessionURLBackground;
@property (nonatomic, strong) Movie *movie;
@property (nonatomic, strong) NSString *pathFile;
@property (nonatomic, assign) float progress;
@property (nonatomic, assign) BOOL saveToPhotosFolder;


- (id)initWithMovie:(Movie *)p_movie withBackgroundSessionIdentifier:(NSString *)identifier savingToPhotosFolder:(BOOL)p_savePhotos delegate:(id<VideoDownloaderDelegate>) theDelegate;

@end

@protocol VideoDownloaderDelegate <NSObject>
- (void)downloadDidFinish:(VideoDownloadOperation *)downloader;
- (void)downloadDidFail:(VideoDownloadOperation *)downloader;
- (void)downloadDidCancel:(VideoDownloadOperation *)downloader;
- (void)downloadProgressChanged:(VideoDownloadOperation *)downloader;
@end
