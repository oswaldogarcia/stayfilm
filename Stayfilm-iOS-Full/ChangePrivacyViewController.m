//
//  ChangePrivacyViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 8/05/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <Google/Analytics.h>
#import "ChangePrivacyViewController.h"

@interface ChangePrivacyViewController ()

@end

@implementation ChangePrivacyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setLanguage];
    
    UITapGestureRecognizer *modalTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOutside)];
    modalTap.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:modalTap];
    
    switch ((int)_currentPrivacy) {
        case 3:
            [self.publicView setBackgroundColor:[UIColor colorWithRed:0.97 green:0.97 blue:0.97 alpha:1.0]];
            [self.friendsView setBackgroundColor:[UIColor whiteColor]];
            [self.privateView setBackgroundColor:[UIColor whiteColor]];
            break;
        case 2:
            [self.publicView setBackgroundColor:[UIColor whiteColor]];
            [self.friendsView setBackgroundColor:[UIColor colorWithRed:0.97 green:0.97 blue:0.97 alpha:1.0]];
            [self.privateView setBackgroundColor:[UIColor whiteColor]];
            break;
        case 1:
            [self.publicView setBackgroundColor:[UIColor whiteColor]];
            [self.friendsView setBackgroundColor:[UIColor whiteColor]];
            [self.privateView setBackgroundColor:[UIColor colorWithRed:0.97 green:0.97 blue:0.97 alpha:1.0]];
            break;
            
        default:
            [self.publicView setBackgroundColor:[UIColor whiteColor]];
            [self.friendsView setBackgroundColor:[UIColor whiteColor]];
            [self.privateView setBackgroundColor:[UIColor whiteColor]];
            break;
    }
    
}
- (void)setLanguage{
    
    self.whoCanSeeLabel.text = NSLocalizedString(@"WHO_CAN_SEE", nil);
    
    self.publicLabel.text = NSLocalizedString(@"PUBLIC", nil);
    self.publicDescriptionLabel.text = NSLocalizedString(@"PUBLIC_DESCRIPTION", nil);
    
    self.friendsLabel.text = NSLocalizedString(@"LINK", nil);
    self.friendsDescriptionLabel.text = NSLocalizedString(@"LINK_DESCRIPTION", nil);
    
    self.privateLabel.text = NSLocalizedString(@"PRIVATE", nil);
    self.privateDescriptionLabel.text = NSLocalizedString(@"PRIVATE_DESCRIPTION", nil);
    
    
    [self.cancelButton setTitle:NSLocalizedString(@"CANCEL", nil) forState:normal];
    
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    //Google Analytics
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"change-privacy_film-in-profile"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}


-(void)tapOutside{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
- (IBAction)setPublic:(id)sender {
    if(self.videoPosition == -1) {
        [self.delegate changePrivacy:MoviePermission_PUBLIC];
        
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"changing-privacy_film-in-profile"
                                                              action:@"public"
                                                               label:@""
                                                               value:@1] build]];
    }
    else {
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"changing-privacy_film-in-profile"
                                                              action:@"public"
                                                               label:@""
                                                               value:@1] build]];
        
        [self.delegate changePrivacy:(int)3 position:self.videoPosition];
    }
    [self tapOutside];
}
- (IBAction)setFriends:(id)sender {
    if(self.videoPosition == -1) {
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"changing-privacy_film-in-profile"
                                                              action:@"link"
                                                               label:@""
                                                               value:@1] build]];
        
        [self.delegate changePrivacy:MoviePermission_UNLISTED];
    }
    else {
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"changing-privacy_film-in-profile"
                                                              action:@"link"
                                                               label:@""
                                                               value:@1] build]];
        
        [self.delegate changePrivacy:(int)2 position:self.videoPosition];
    }
    [self tapOutside];
}
- (IBAction)setPrivate:(id)sender {
    if(self.videoPosition == -1) {
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"changing-privacy_film-in-profile"
                                                              action:@"private"
                                                               label:@""
                                                               value:@1] build]];
        
        [self.delegate changePrivacy:MoviePermission_PRIVATE];
    }
    else {
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"changing-privacy_film-in-profile"
                                                              action:@"private"
                                                               label:@""
                                                               value:@1] build]];
        
        [self.delegate changePrivacy:(int)1 position:self.videoPosition];
    }
    [self tapOutside];
}

- (IBAction)cancelAction:(id)sender {
    
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"changing-privacy_film-in-profile"
                                                          action:@"cancel"
                                                           label:@""
                                                           value:@1] build]];
    
    [self tapOutside];
    
}
@end
