//
//  VideoOptionsViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 2/05/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VideoOptionsDelegate<NSObject>

- (void)didSelectDownloadVideo:(NSInteger)videoPosition;
- (void)didSelectChangePrivacy:(NSInteger)videoPosition;
- (void)didSelectDeleteVideo:(NSInteger)videoPosition;

@end

@interface VideoOptionsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *downloadButton;
@property (weak, nonatomic) IBOutlet UIButton *changePrivacyButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteButtom;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;




@property (nonatomic, assign) NSInteger videoPosition;
@property (nonatomic, assign) id<VideoOptionsDelegate> delegate;
@end
