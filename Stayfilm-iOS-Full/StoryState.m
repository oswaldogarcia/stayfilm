//
//  StoryState.m
//  Stayfilm-iOS-Full
//
//  Created by Henrique on 6/14/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import "StoryState.h"
#import "StayfilmApp.h"


@interface StoryState ()

@property (nonatomic, weak) StayfilmApp *sfApp;

@end


@implementation StoryState

@synthesize sfApp;
static StoryState *storySingletonObject = nil;


+ (StoryState *) sharedStoryStateSingletonWithDelegate:(id<StoryStateDelegate>)theDelegate
{
    @synchronized(self)
    {
        if(storySingletonObject == nil)
        {
            storySingletonObject = [[self alloc] init];
        }
        storySingletonObject.delegate = theDelegate;
        storySingletonObject.delegateCreation = nil;
    }
    return storySingletonObject;
}

+ (StoryState *) sharedStoryStateSingletonWithCreationDelegate:(id<StoryStateCreationDelegate>)theDelegate
{
    @synchronized(self)
    {
        if(storySingletonObject == nil)
        {
            storySingletonObject = [[self alloc] init];
        }
        storySingletonObject.delegateCreation = theDelegate;
    }
    return storySingletonObject;
}

+ (StoryState *) sharedStoryStateSingleton
{
    @synchronized(self)
    {
        if(storySingletonObject == nil)
        {
            storySingletonObject = [[self alloc] init];
        }
    }
    return storySingletonObject;
}

- (id)init
{
    if(self = [super init])
    {
        self.viewState = [[NSMutableDictionary alloc] init];
        @try
        {
            if([[NSUserDefaults standardUserDefaults] objectForKey:@"StoryState"] != nil)
            {
                NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"StoryState"];
                NSDictionary *story = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                self.viewState = [story mutableCopy];
            }
            
            [self updateStories];
            

        }
        @catch (NSException *exception) {
            self.viewState = nil;
            self = nil;
        }
    }
    return self;
}



-(void)updateStories
{
    // Background thread so doesn't deadlock when it's called
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^(void){
        @try
        {
            if(self.sfApp == nil)
                self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
            
            NSDictionary *response = [Story getStories];
            
            // if Stories has changed let's update the view state
            if(response != nil && [response[@"isChanged"] isEqualToString:@"1"] && response[@"stories"] != nil && response[@"response"] != nil)
            {
                //save stories
                NSData *dataSave = [NSKeyedArchiver archivedDataWithRootObject:response[@"response"]];
                [[NSUserDefaults standardUserDefaults] setObject:dataSave forKey:@"Stories"];
                
                NSArray *newStories = response[@"stories"];
                
                self.sfApp.sfConfig.stories = [self applyViewedStateOnArray:[newStories copy]];
                if(storySingletonObject.delegate != nil && [storySingletonObject.delegate respondsToSelector:@selector(storiesChanged)])
                    [(NSObject *)storySingletonObject.delegate performSelectorOnMainThread:@selector(storiesChanged) withObject:nil waitUntilDone:NO];
                
              [self getStoriesPlayers];
            }
            else if(response != nil && [response[@"isChanged"] isEqualToString:@"0"]) // if Stories didn't change lets load them if not loaded yet
            {
                if(self.sfApp.sfConfig.stories == nil || self.sfApp.sfConfig.stories.count == 0)
                {
                    //read stories
                    NSData *dataSaved = [[NSUserDefaults standardUserDefaults] objectForKey:@"Stories"];
                    NSDictionary *stories = [NSKeyedUnarchiver unarchiveObjectWithData:dataSaved];
                    
                    if(stories != nil)
                    {
                        NSMutableArray *array = [[NSMutableArray alloc] init];
                        for(NSDictionary *var in stories) {
                            Story *s = [Story customClassWithProperties:var];
                            if(s.isTest == NO) { // is not test Story
                                [array addObject:s];
                            } else if([self.sfApp.currentUser.role isEqualToString:@"admin"] || [self.sfApp.currentUser.role isEqualToString:@"superadmin"]) {
                                [array addObject:s];  // is test story
                            } else {
                                continue;
                            }
                            
                            if (var[@"content"] != nil) {
                                for (NSDictionary *var2 in var[@"content"]) {
                                    [((Story *)[array lastObject]).content addObject:[StoryContent customClassWithProperties:var2]];
                                }
                            }
                        }
                        self.sfApp.sfConfig.stories = [self applyViewedStateOnArray:[array copy]];
                        if(storySingletonObject.delegate != nil && [storySingletonObject.delegate respondsToSelector:@selector(storiesChanged)])
                            [(NSObject *)storySingletonObject.delegate performSelectorOnMainThread:@selector(storiesChanged) withObject:nil waitUntilDone:NO];
                        
                     [self getStoriesPlayers];
                    }
                }
                else {
                    self.sfApp.sfConfig.stories = [self applyViewedStateOnArray:self.sfApp.sfConfig.stories];
                   [self getStoriesPlayers];
                }
            }
            else {
                NSLog(@"StayLog: getStories returned an ERROR status code");
            }
            if(storySingletonObject.delegateCreation != nil  && [storySingletonObject.delegateCreation respondsToSelector:@selector(storiesLoaded)])
                [(NSObject *)storySingletonObject.delegateCreation performSelectorOnMainThread:@selector(storiesLoaded) withObject:nil waitUntilDone:NO];
        }
        @catch (NSException *ex) {
            NSLog(@"StayLog: EXCEPTION in updateStories with error: %@", ex.description);
        }
    });
}
-(void)getStoriesPlayers{
  
    for(int i = 0 ; i < self.sfApp.sfConfig.stories.count;i++){
 
        Story *story = self.sfApp.sfConfig.stories[i];
    
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            
            [story setPlayersWithCompletion:^{
                
               // NSLog(@"StayLog: players for story %d ready!",i);
                
            }];
            
        });
    }
}


-(NSArray *)applyViewedStateOnArray:(NSArray *)newStories
{
    @try
    {
        for (NSString *idStory in self.viewState) {  //clear all stories that are no longer in the saved state list
            BOOL found = NO;
            for (Story *newStory in newStories) {
                if([[newStory.idStory stringValue] isEqualToString:idStory]) {
                    found = YES;
                    break;
                }
            }
            if(!found) {
                [self.viewState removeObjectForKey:idStory];
                break;
            }
        }
        
        //now fill the newStories with the state information
        for (Story *newStory in newStories) {
            if(self.viewState[[newStory.idStory stringValue]] != nil ) { // story in saved state
                
                NSNumber* idSeen = self.viewState[[newStory.idStory stringValue]];
                NSNumber* i = newStory.content.lastObject.idContent;

                BOOL found = NO;
                BOOL foundBigger = NO;
                for (StoryContent* content in newStory.content) {
                    if(idSeen == content.idContent) {
                        found = YES;
                        break;
                    }
                    if(idSeen < content.idContent) { // case idSeen was deleted by the admin
                        foundBigger = YES;
                        newStory.viewedIDContent = content.idContent;
                        break;
                    }
                }
                if(!found) {
                    if(!foundBigger) {
                        newStory.viewedIDContent = newStory.content.firstObject.idContent;
                    } // else  already set viewIDContent
                } else {
                    newStory.viewedIDContent = idSeen;
                }
                if(idSeen >= i) {
                    newStory.isStoryViewed = YES;
                } else {
                    newStory.isStoryViewed = NO;
                }
            }
        }
    }
    @catch (NSException *ex) {
        NSLog(@"StayLog: EXCEPTION in applyViewedStateOnArray with error: %@", ex.description);
    }
    return newStories;
}

-(void)setViewedStoryID:(NSNumber *)idStory withIDContent:(NSNumber *)idcontent
{
    @try
    {
        if(self.viewState[[idStory stringValue]] != nil) {
            NSNumber* idSeen = self.viewState[[idStory stringValue]];
            if(idcontent > idSeen) {
                [self.viewState setObject:idcontent forKey:[idStory stringValue]];
                
                for (Story *story in self.sfApp.sfConfig.stories) {
                    if(story.idStory == idStory) {
                        if(story.content.lastObject.idContent == idcontent) {  //if last content of the story was seen
                            story.isStoryViewed = YES;
                            if(storySingletonObject.delegate != nil)
                                [(NSObject *)storySingletonObject.delegate performSelectorOnMainThread:@selector(storiesChanged) withObject:nil waitUntilDone:NO];
                        }
                        story.viewedIDContent = idcontent;
                        break;
                    }
                }
            }
        }
        else {
            [self.viewState setObject:idcontent forKey:[idStory stringValue]];
            
            for (Story *story in self.sfApp.sfConfig.stories) {
                if(story.idStory == idStory) {
                    if(story.content.lastObject.idContent == idcontent) {  //if last content of the story was seen
                        story.isStoryViewed = YES;
                        if(storySingletonObject.delegate != nil)
                            [(NSObject *)storySingletonObject.delegate performSelectorOnMainThread:@selector(storiesChanged) withObject:nil waitUntilDone:NO];
                    }
                    story.viewedIDContent = idcontent;
                    break;
                }
            }
        }
        
        NSData *dataSave = [NSKeyedArchiver archivedDataWithRootObject:self.viewState];
        [[NSUserDefaults standardUserDefaults] setObject:dataSave forKey:@"StoryState"];
    }
    @catch (NSException *ex)
    {
        NSLog(@"StayLog: EXCEPTION in setViewedStoryID:withIndexContent with error: %@", ex.description);
    }
}

-(void)setCurrentViewedIDGenre:(int)p_genre andIDTemplate:(int)p_template {
    if(self.sfApp == nil) {
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    }
    
    if(p_genre != -1) {
        self.sfApp.storyState.currentGenre = [Genres getGenreByID:p_genre];
    } else {
        self.sfApp.storyState.currentGenre = nil;
    }
    
    if(p_template != -1) {
        BOOL found = NO;
        for (Template *t in self.sfApp.storyState.currentGenre.templates) {
            if(((int)t.idtemplate) == p_template) {
                self.sfApp.storyState.currentTemplate = t;
                found = YES;
                break;
            }
        }
        if(!found) {
            self.sfApp.storyState.currentTemplate = nil;
        }
    } else {
        self.sfApp.storyState.currentTemplate = nil;
    }
    
}

-(void)resetStories {
    self.viewState = [[NSMutableDictionary alloc] init];
}

@end
