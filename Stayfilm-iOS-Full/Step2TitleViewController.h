//
//  Step2TitleViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Henrique Ormonde on 21/09/17.
//  Copyright © 2017 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StayfilmApp.h"
#import "Uploader.h"

@interface Step2TitleViewController : UIViewController <UIPageViewControllerDataSource, UITextFieldDelegate, StayfilmUploaderDelegate>

@property (weak, nonatomic) IBOutlet UITextField *txt_Title;
@property (weak, nonatomic) IBOutlet UIButton *but_NextStep;

@property (weak, nonatomic) IBOutlet UIPageControl *pageController;
@property (nonatomic, assign) NSUInteger pageIndex;

@end
