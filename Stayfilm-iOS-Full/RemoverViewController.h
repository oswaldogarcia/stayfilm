//
//  RemoverViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Henrique Ormonde on 17/11/17.
//  Copyright © 2017 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlbumFB.h"
#import "PhotoFB.h"
#import "AlbumInternal.h"
#import "Uploader.h"
#import <SDWebImage/UIImageView+WebCache.h>


@interface RemoverViewController : UIViewController <StayfilmUploaderDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *photosCollectionView;
@property (weak, nonatomic) IBOutlet UIImageView *img_SelectAll;
@property (weak, nonatomic) IBOutlet UILabel *txt_SelectAll;
@property (weak, nonatomic) IBOutlet UIButton *but_DeleteSelection;
@property (weak, nonatomic) IBOutlet UIView *view_Remove;
@property (weak, nonatomic) IBOutlet UIButton *but_SelectAll;
@property (weak, nonatomic) IBOutlet UIButton *but_Cancel;

@property (weak, nonatomic) UINavigationController *navigationController;
@property (nonatomic , strong) PHCachingImageManager *imageManager;
@property (nonatomic, strong) NSURLSession *thumbConnection;
@property (nonatomic, strong) NSMutableArray *removeMedias;
@property (nonatomic, strong) NSArray *initialSelectedMedias;

- (IBAction)SelectAllTapped:(id)sender;

@end
