//
//  Uploader.h
//  Stayfilm for Messenger
//
//  Created by Henrique Ormonde on 26/10/15.
//  Copyright © 2015 Stayfilm. All rights reserved.
//

@import Photos;
@import Foundation;
//#import <Foundation/Foundation.h>
#import "StayfilmApp.h"
#import "AlbumSF.h"
#import "ConvertMediaOperation.h"
#import "ImageUploadOperation.h"
#import "VideoUploadOperation.h"
#import "ConfirmMediasOperation.h"


@protocol StayfilmUploaderDelegate;

@interface Uploader : NSObject <NSURLSessionDelegate, ImageUploaderDelegate, VideoUploaderDelegate, ConvertMediaDelegate, ConfirmMediaDelegate>

@property (nonatomic, assign) id <StayfilmUploaderDelegate> delegate;

@property (nonatomic, strong) NSMutableArray *listUploadMedias;
@property (atomic, strong) NSMutableDictionary *listUploadedMedias;
@property (atomic, strong) NSMutableDictionary *listUploadingMedias;
@property (nonatomic, strong) NSOperationQueue *uploadQueue;
@property (nonatomic, strong) AlbumSF *uploadedAlbum;
@property (nonatomic, assign) BOOL isUploading;
//@property (nonatomic, assign) BOOL maxErrorAttemptsReached;

@property (nonatomic, strong) NSMutableDictionary *dictionaryMediaIDwithMediaPath;
@property (nonatomic, strong) NSMutableArray *listConfirmMedias;

@property (nonatomic, copy) void (^backgroundSessionCompletionHandler)(void);

+ (Uploader *) sharedUploadStayfilmSingletonWithDelegate:(id<StayfilmUploaderDelegate>)theDelegate;
+ (Uploader *) sharedUploadStayfilmSingleton;
- (int)addMedias:(NSArray *)p_medias;
- (void)startOperations;
- (BOOL)resetUpload;
- (BOOL)isUploadFinished;
- (int)getUploadPercentage;
- (BOOL)isAssetUploaded:(PHAsset *)p_asset;
- (BOOL)isAssetUploading:(PHAsset *)p_asset;
- (BOOL)isAssetOnUploadList:(PHAsset *)p_asset;
- (BOOL)removeAssetFromUpload:(id)p_asset;

@end

@protocol StayfilmUploaderDelegate <NSObject>
- (void)uploadProgressChanged:(NSNumber *)progress;
//- (void)requestUIupdate;
- (void)uploadDidStart;
- (void)uploadDidStartSendingConfirmationMedias:(Uploader *)uploader;
- (void)converterDidFailMedia:(NSString *)type;
- (void)uploadDidFailMedia:(NSString *)type;
- (void)uploadDidFinish:(Uploader *)uploader;
- (void)uploadDidFail:(Uploader *)uploader;
- (void)uploadFinishedMedia:(PHAsset *)asset;
@end
