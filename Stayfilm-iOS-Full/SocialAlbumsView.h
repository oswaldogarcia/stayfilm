//
//  SocialAlbumsView.h
//  Stayfilm-iOS-Full
//
//  Created by Henrique on 12/21/17.
//  Copyright © 2017 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "StayfilmApp.h"


@interface SocialAlbumsView : UIView <UICollectionViewDelegate, UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UIImageView *img_SocialSource;
@property (weak, nonatomic) IBOutlet UILabel *txt_Title;
@property (weak, nonatomic) IBOutlet UIImageView *expandArrow;
@property (weak, nonatomic) IBOutlet UICollectionView *albumCollection;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingSpinner;
@property (weak, nonatomic) IBOutlet UILabel *txt_noAlbums;
//@property (weak, nonatomic) IBOutlet UILabel *txt_showMoreLess;
//@property (weak, nonatomic) IBOutlet UIImageView *img_showMoreLess;
//@property (weak, nonatomic) IBOutlet UIImageView *img_gradientWhite;
//@property (weak, nonatomic) IBOutlet UIView *view_ShowMore;


@property (nonatomic, strong) NSURLSession *thumbConnection;
@property (nonatomic, strong) NSArray *albums;
@property (nonatomic, assign) BOOL isExpanded;
@property (nonatomic, assign) BOOL isLoaded;
@property (nonatomic, assign) BOOL isLoadingInfo;

@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_AlbumsHeight;

@property (nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizer;
//@property (nonatomic, strong) UITapGestureRecognizer *expandTapGestureRecognizer;

- (void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate indexPath:(NSIndexPath *)indexPath;

-(CGFloat)getAlbumHeight;

@end
