//
//  SocialAlbumsView.m
//  Stayfilm-iOS-Full
//
//  Created by Henrique on 12/21/17.
//  Copyright © 2017 Stayfilm. All rights reserved.
//

#import "SocialAlbumsView.h"
#import "AlbumSocialCollectionViewCell.h"

@interface SocialAlbumsView ()

@property (nonatomic, weak) StayfilmApp *sfAppSource;

@end

@implementation SocialAlbumsView

static NSString *cellIdentifier = @"AlbumSocialNetworkSourceItemCell";

@synthesize img_SocialSource, txt_Title, expandArrow, constraint_AlbumsHeight, loadingSpinner, txt_noAlbums, albumCollection, albums;
/*
// Only overllectionView ride drawRect: if you perform custom drawing.
// An empty yIndicatorView implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

//- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
//    [super setSelected:selected animated:animated];
//    // Configure the view for the selected state
//    if(!isExpanded)
//    {
//        [UIView animateWithDuration:0.35 animations:^{
//            self.expandArrow.alpha = 1;
//        }];
//    }
//}

-(instancetype)init {
    self = [super init];
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    self.thumbConnection = [NSURLSession sessionWithConfiguration:config];
    
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        [self customInit];
    }
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if(self) {
        [self customInit];
    }
    return self;
}

-(void)customInit {
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"SocialAlbumsView" owner:self options:nil];
    self.contentView = [subviewArray objectAtIndex:0];
    [self addSubview:self.contentView];
    self.contentView.frame = self.bounds;
    self.isLoaded = NO;
    self.isExpanded = NO;
    self.isLoadingInfo = NO;
}

-(CGFloat)getAlbumHeight {
    return self.albumCollection.collectionViewLayout.collectionViewContentSize.height;
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return (self.albums != nil)? [self.albums count] : 0;
}


#pragma mark - IndexedCollectionView Delegates


-(void)layoutSubviews
{
    [super layoutSubviews];
}

- (void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDataSource, UICollectionViewDelegate>)dataSourceDelegate indexPath:(NSIndexPath *)indexPath
{
    self.albumCollection.dataSource = dataSourceDelegate;
    self.albumCollection.delegate = dataSourceDelegate;
    
    [self.albumCollection setContentOffset:self.albumCollection.contentOffset animated:NO];
    
    [self.albumCollection reloadData];
}

@end
