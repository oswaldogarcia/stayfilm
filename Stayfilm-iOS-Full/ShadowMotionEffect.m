//
//  ShadowMotionEffect.m
//  fbmessenger
//
//  Created by Henrique Ormonde on 02/07/15.
//  Copyright (c) 2015 Stayfilm. All rights reserved.
//

#import "ShadowMotionEffect.h"

@implementation ShadowMotionEffect

-(NSDictionary *)keyPathsAndRelativeValuesForViewerOffset:(UIOffset)viewerOffset
{
    NSNumber *shadowWidth = @(viewerOffset.horizontal * -30);
    NSNumber *shadowHeight = @(viewerOffset.vertical * -30);
    
    return @{
             @"layer.shadowOffset.width" : shadowWidth,
             @"layer.shadowOffset.height" : shadowHeight
             };
}

@end
