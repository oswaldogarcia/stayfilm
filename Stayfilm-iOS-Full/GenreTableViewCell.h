//
//  GenreTableViewCell.h
//  Stayfilm for Messenger
//
//  Created by Henrique Ormonde on 29/04/15.
//  Copyright (c) 2015 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SFConfig.h"

@interface GenreTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *genreImageView;
@property (weak, nonatomic) IBOutlet UIImageView *selection;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingSpinner;

@property (nonatomic, strong) NSURLSessionDataTask *task;
@property (nonatomic, strong) Genres *selectedGenre;

-(void)setSelect:(BOOL)select;

@end
