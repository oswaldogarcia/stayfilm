//
//  Popover_ConfirmPrivacyShareViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Henrique on 8/29/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol Popover_ConfirmPrivacyShareDelegate<NSObject>
@required
- (void)chooseOption:(NSString*)option;
@end

@interface Popover_ConfirmPrivacyShareViewController : UIViewController

@property (nonatomic, strong) id<Popover_ConfirmPrivacyShareDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIButton *but_continue;
@property (weak, nonatomic) IBOutlet UIButton *but_cancel;

@property (nonatomic, strong) NSString* option;

@end
