//
//  Downloader.h
//  Stayfilm for Messenger
//
//  Created by Henrique Ormonde on 10/11/15.
//  Copyright © 2015 Stayfilm. All rights reserved.
//

@import Photos;
@import Foundation;
#import "StayfilmApp.h"
#import "Movie.h"
#import "VideoDownloadOperation.h"


@protocol StayfilmDownloaderDelegate;

@interface Downloader : NSObject <NSURLSessionDelegate, VideoDownloaderDelegate>

@property (nonatomic, assign) id <StayfilmDownloaderDelegate> delegate;

@property (nonatomic, strong) NSMutableArray *listDownloadMedias;
@property (atomic, strong) NSMutableDictionary *listDownloadedMedias;
@property (atomic, strong) NSMutableDictionary *listDownloadingMedias;
@property (nonatomic, strong) NSOperationQueue *downloadQueue;
@property (nonatomic, assign) BOOL isDownloading;

@property (nonatomic, copy) void (^backgroundSessionCompletionHandler)(void);

+ (Downloader *) sharedDownloaderStayfilmSingletonWithDelegate:(id<StayfilmDownloaderDelegate>)theDelegate;
+ (Downloader *) sharedDownloaderStayfilmSingleton;
- (BOOL)addMovie:(Movie *)p_movie;
- (int)addMedias:(NSArray *)p_medias;
- (BOOL)addMovieWithoutSavingToPhotosAlbum:(Movie *)p_movie;
- (void)startOperations;
- (BOOL)resetDownload;
- (BOOL)isDownloadFinished;
- (BOOL)isMovieAlreadyDownloaded:(Movie *)p_movie;
- (BOOL)isMovieDownloading:(Movie *)p_movie;
- (NSString *)getPathForDownloadedMovie:(Movie *)p_movie;
- (int)getTotalDownloadPercentage;
- (int)getDownloadPercentageForIDMovie:(NSString *)p_movieID;

@end

@protocol StayfilmDownloaderDelegate <NSObject>
- (void)downloadOverallProgressChanged:(NSNumber *)progress;
- (void)downloadProgressChanged:(NSArray *)movie_Progress;
- (void)downloadDidStart;
- (void)downloadDidFailMedia:(NSString *)movieID;
- (void)downloadDidFinish:(Downloader *)downloader;
- (void)downloadDidFail:(Downloader *)downloader;
- (void)downloadDidCancel:(Downloader *)downloader;
- (void)downloadFinishMedia:(Movie *)movie;
- (void)alreadyDownloadedMedia:(Movie *)movie;
@end

