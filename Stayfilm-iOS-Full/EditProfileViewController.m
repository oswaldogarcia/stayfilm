//
//  EditProfileViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 7/05/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#define ACCEPTABLE_CHARACTERS @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'_."


#import "EditProfileViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <Google/Analytics.h>
#import "EditImageOptionsViewController.h"
#import "ShellWebService.h"
#import "Reachability.h"

@interface EditProfileViewController ()<UINavigationControllerDelegate,UIImagePickerControllerDelegate,EditImageOptionsDelegate,UITextFieldDelegate>

@property(nonatomic, strong) NSTimer *connectionStatusTimer;

@end

@implementation EditProfileViewController



- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setLanguage];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.sfAppProfile = [StayfilmApp sharedStayfilmAppSingletonWithDelegate:self];
    
    //Google Analytics
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"edit-profile"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    
    if (networkStatus != NotReachable) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self connectivityChanged:YES];
        });
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self connectivityChanged:NO];
        });
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.sfAppProfile = [StayfilmApp sharedStayfilmAppSingleton];
    
    if(self.sfAppProfile.currentUser.firstName != nil && ![self.sfAppProfile.currentUser.firstName isEqualToString:@""]){
        
        self.nameTextField.text =  self.sfAppProfile.currentUser.firstName;
        self.lastNameTextField.text =  self.sfAppProfile.currentUser.lastName;
        
    }else{
        self.nameTextField.text = @" ";
        self.lastNameTextField.text =  @" ";
    }
    
    self.profileImage.layer.cornerRadius = self.profileImage.frame.size.width / 2;
    self.profileImage.clipsToBounds = YES;
  
    [self.profileImage sd_setImageWithURL:[NSURL URLWithString:self.sfAppProfile.currentUser.photo] placeholderImage:[UIImage imageNamed:@"Avatar_None"] options:SDWebImageRefreshCached];
    
     self.transitionAnimation = [[PopupMenuAnimation alloc] init];
    
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0.20 green:0.34 blue:0.44 alpha:1.0];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setLanguage{
    
    self.nameLabel.text = NSLocalizedString(@"NAME", nil);
    self.lastNameLabel.text = NSLocalizedString(@"LAST_NAME", nil);
    [self.saveButton setTitle:NSLocalizedString(@"SAVE_CHANGE", nil) forState:normal];
    
}

- (IBAction)editImage:(id)sender {
    
    EditImageOptionsViewController * view = [[EditImageOptionsViewController alloc] init];
    view.delegate = self;
    view.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    view.transitioningDelegate = self;
    view.view.frame = self.view.frame;
    self.transitionAnimation.duration = 0.3;
    self.transitionAnimation.isPresenting = YES;
    self.transitionAnimation.originFrame = self.view.frame;
    
    [self presentViewController:view animated:YES completion:nil];
    
}

- (void)didSelectTakePicture{
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"edit-profile"
                                                          action:@"change-photo"
                                                           label:@"take-photo"
                                                           value:@1] build]];
    [self takePhoto];
}

-(void)didSelectChoosePicture{
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"edit-profile"
                                                          action:@"change-photo"
                                                           label:@"choose-photo"
                                                           value:@1] build]];
    
    [self selectPhoto];
}

- (void)takePhoto {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
}
- (void)selectPhoto {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    [self.profileImage setImage:chosenImage];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (IBAction)editProfile:(id)sender {
    
    [self.navigationItem.backBarButtonItem setEnabled:NO];
    [self.loadingView setHidden:NO];
    [self.loadingIndicator startAnimating];
    
    if(self.sfAppProfile == nil)
        self.sfAppProfile = [StayfilmApp sharedStayfilmAppSingleton];
    
    //service to upload phpto
    NSDictionary *dict = @{ @"image" : self.profileImage.image,
                            @"user_id" : self.sfAppProfile.settings[@"idUser"],
                            @"idSession" : self.sfAppProfile.settings[@"idSession"]
                          };
    
    ShellWebService *service = [ShellWebService getSharedInstance];
    [service callServiceImage:dict endpointName:endpointUploadProfilePicture withCompletionBlock:^(NSDictionary *resultArray, NSError *error) {
        if(!error){
            NSLog(@"StayLog: uploaded profile photo");
            if(resultArray[@"code"] != nil && [[resultArray[@"code"] stringValue] isEqualToString:@"0"]) {
                self.sfAppProfile.currentUser.photo = resultArray[@"content"];
                [self updateProfile];
            }
            else {
                [self.loadingView setHidden:YES];
                [self.loadingIndicator stopAnimating];
            }
        }else{
            //TODO: try again automatically, then case of failing again, show some kind of error message to the user
            NSLog(@"StayLog: failed to upload profile photo");
            [self.loadingView setHidden:YES];
            [self.loadingIndicator stopAnimating];
            
        }
    }];
}

- (void)updateProfile {
    
    [self.loadingView setHidden:NO];
    [self.loadingIndicator startAnimating];
    
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"edit-profile"
                                                          action:@"saved"
                                                           label:@""
                                                           value:@1] build]];
    
    if([self.sfAppProfile.currentUser.firstName isEqualToString:self.nameTextField.text] && [self.sfAppProfile.currentUser.lastName isEqualToString:self.lastNameTextField.text])
    {
        // nothing changed
        [self.navigationController popViewControllerAnimated:YES];
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        if(![self.sfAppProfile.currentUser.firstName isEqualToString:self.nameTextField.text]) {
            //Google Analytics Event
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"edit-profile"
                                                                  action:@"change-name"
                                                                   label:@""
                                                                   value:@1] build]];
        }
        if(![self.sfAppProfile.currentUser.lastName isEqualToString:self.lastNameTextField.text]) {
            //Google Analytics Event
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"edit-profile"
                                                                  action:@"change-lastname"
                                                                   label:@""
                                                                   value:@1] build]];
        }

        [self performSelectorInBackground:@selector(update)
                               withObject:nil];
    }
}

-(void) update {
    
    BOOL success = NO;
    
    success = [User updateProfile:self.nameTextField.text LastName:self.lastNameTextField.text andSFAppSingleton:self.sfAppProfile];
    
    if(success)
    {
        runOnMainQueueWithoutDeadlocking(^{
            
            [self.loadingView setHidden:YES];
            [self.loadingIndicator stopAnimating];
            
            [self.view makeToast:NSLocalizedString(@"CHANGE_SAVED", nil) duration:3.0 position:CSToastPositionBottom ];
            [self.navigationController popViewControllerAnimated:YES];
            [self dismissViewControllerAnimated:YES completion:nil];
        });
    }else{
        
        runOnMainQueueWithoutDeadlocking(^{
            [self.view makeToast:NSLocalizedString(@"CHANGE_NOT_SAVED", nil)];

            [self.loadingView setHidden:YES];
            [self.loadingIndicator stopAnimating];
        });
    }
  
}

#pragma mark - StayfilmApp delegates

-(void)sfSingletonCreated {
    // nothing
}
-(void)connectivityChanged:(BOOL)isConnected {
    self.lbl_connectionStatus.text = NSLocalizedString(@"NO_INTERNET_RECONNECTING", nil);
    [self.view bringSubviewToFront:self.view_connectionStatus];
    if(!isConnected) {
        self.view_connectionStatus.alpha = 0.0;
        [self.view_connectionStatus setHidden:NO];
        [UIView animateWithDuration:0.4 animations:^{
            self.view_connectionStatus.alpha = 1.0;
        }];
        if(self.connectionStatusTimer == nil) {
            __weak typeof(self) selfDelegate = self;
            self.connectionStatusTimer = [NSTimer timerWithTimeInterval:1.0 repeats:YES block:^(NSTimer * _Nonnull timer) {
                Reachability *reachability = [Reachability reachabilityForInternetConnection];
                NetworkStatus networkStatus = [reachability currentReachabilityStatus];
                
                if (networkStatus != NotReachable) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [selfDelegate connectivityChanged:YES];
                    });
                    [timer invalidate];
                    selfDelegate.connectionStatusTimer = nil;
                }
            }];
        }
    } else if(!self.view_connectionStatus.isHidden){
        self.view_connectionStatus.alpha = 1.0;
        [self.view_connectionStatus setHidden:NO];
        [UIView animateWithDuration:0.4 animations:^{
            self.view_connectionStatus.alpha = 0.0;
        } completion:^(BOOL finished) {
            [self.view_connectionStatus setHidden:YES];
        }];
    }
}



#pragma mark - UItextFiled Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if ([textField isEqual:self.nameTextField]){
        
         [self.lastNameTextField becomeFirstResponder];
        
    }else if([textField isEqual:self.lastNameTextField]) {
        
        [textField resignFirstResponder];
    }
    
    return YES;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:
(NSString *)string {

    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
    
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    
    
    if ((textField.text.length < 20 || string.length == 0) && ([string isEqualToString:filtered])){
        return YES;
    }
    else{
        return NO;
    }
    
 
}


#pragma mark - Animation Delegates

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source {
    return self.transitionAnimation;
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    self.transitionAnimation.isPresenting = NO;
    return self.transitionAnimation;
}


#pragma mark - Navigation

- (IBAction)backButton_Clicked:(id)sender {
    //[self.navigationController popViewControllerAnimated:YES];
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"back"
                                                          action:@" "
                                                           label:@""
                                                           value:@1] build]];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}



@end
