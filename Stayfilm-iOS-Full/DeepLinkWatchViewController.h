//
//  DeepLinkWatchViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Rick on 17/10/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DeepLinkWatchViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *movieThumb;
@property (weak, nonatomic) IBOutlet UIButton *but_Play;
@property (weak, nonatomic) IBOutlet UIImageView *img_userImage;
@property (weak, nonatomic) IBOutlet UILabel *txt_movieTitle;
@property (weak, nonatomic) IBOutlet UILabel *txt_Name;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner_Loading;

@property (nonatomic, strong) NSString *idMovie;

@end

NS_ASSUME_NONNULL_END
