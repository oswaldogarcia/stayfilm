//
//  AlbumSN.h
//  Stayfilm-iOS-Full
//
//  Created by Henrique Ormonde on 28/09/17.
//  Copyright © 2017 Stayfilm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import "PhotoFB.h"

// Album Social Network
@interface AlbumSN : NSObject

@property (nonatomic, strong) NSString *idAlbum;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *link;
@property (nonatomic, strong) NSString *imageLEFT;
@property (nonatomic, strong) NSString *imageRIGHT;
@property (nonatomic, strong) NSString *imageBIG;
@property (nonatomic, strong) NSNumber *count;
@property (nonatomic, strong) NSMutableArray *idMedias;
@property (nonatomic, strong) NSString *after;

+ (id)customClassWithProperties:(NSDictionary *)properties;
- (id)initWithProperties:(NSDictionary *)properties;


@end

