//
//  TestAnimationViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Henrique Ormonde on 29/08/17.
//  Copyright © 2017 Stayfilm. All rights reserved.
//

#import <Lottie/Lottie.h>
#import "TestAnimationViewController.h"

@interface TestAnimationViewController ()

@property (strong, nonatomic) NSArray *animationNames;
@property (strong, nonatomic) LOTAnimationView *animation;
    
@end


@implementation TestAnimationViewController
    
int selected = 0;
    
@synthesize animationView,but_Exit,but_Next, animationNames, animation;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    animationNames = @[@"teste1",
                       @"teste2",
                       @"teste3",
                       @"relogio",
                       @"van",
                       @"windowsloading",
                       @"data"];
    selected = 0;
    
    //but_Next.titleLabel.text = [NSString stringWithFormat:@"Próximo %d/%d",selected+1,[animationNames count]];
    [but_Next setTitle:[NSString stringWithFormat:@"Próximo %d/%d",selected+1,[animationNames count]] forState:UIControlStateNormal];
    
    animation = [LOTAnimationView animationNamed:animationNames[selected]];
    //animation.frame = CGRectMake(0, 0, self.view.frame.size.width, 150);
    animation.frame = CGRectMake(0, 0, self.animationView.frame.size.width, self.animationView.frame.size.height);
    animation.contentMode = UIViewContentModeScaleAspectFit;
    animation.loopAnimation = YES;

    //[self.view addSubview:animation];
    [self.animationView addSubview:animation];

}
    
-(void)viewDidAppear:(BOOL)animated
    {
         [animation play];
    }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
    
- (IBAction)but_Exit_clicked:(id)sender {
}
    
- (IBAction)but_Next_Clicked:(id)sender
{
    [self.animation removeFromSuperview];
    
    if(selected + 1 >= (int)[animationNames count])
    {
        selected = 0;
    }
    else
    {
        selected += 1;
    }
    
    [but_Next setTitle:[NSString stringWithFormat:@"Próximo %d/%d",selected+1,[animationNames count]] forState:UIControlStateNormal];
    
    animation = [LOTAnimationView animationNamed:animationNames[selected]];
    animation.frame = CGRectMake(0, 0, self.animationView.frame.size.width, self.animationView.frame.size.height);
    animation.contentMode = UIViewContentModeScaleAspectFit;
    animation.loopAnimation = YES;
    
    [self.animationView addSubview:animation];
    [animation play];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
