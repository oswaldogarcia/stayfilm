//
//  Feed.h
//  fbmessenger
//
//  Created by Henrique Ormonde on 26/06/15.
//  Copyright (c) 2015 Stayfilm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Feed : NSObject

@property (nonatomic, strong) NSMutableArray *itemsList;
@property (nonatomic, strong) NSDictionary *rawResult;
@property (nonatomic, strong) NSString *offset;
@property (nonatomic, strong) NSString *offsetPrevious;
@property (nonatomic, strong) NSString *timestampNow;
@property (nonatomic, assign) NSInteger feedPage;

+(Feed *)getMyFilmsWithLimit:(int)p_limit withOffset:(NSString *)p_offset andIsRefresh:(BOOL)p_refreshNew andIsCacheEnabled:(BOOL)p_cache;

@end
