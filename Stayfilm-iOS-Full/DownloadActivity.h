//
//  DownloadActivity.h
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 16/05/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "Movie.h"
#import "ProgressBarViewController.h"
#import "UIView+Toast.h"
#import "ProgressBarViewController.h"
#import "ProfileViewController.h"
#import "VideoFinishedViewController.h"

@interface DownloadActivity : UIActivity <ProgressBarViewControllerDelegate>
@property (nonatomic, strong) Movie *movie;
@property (nonatomic, assign) NSURL* path;
@property (nonatomic, strong) ProgressBarViewController *progressView;
@property (nonatomic, strong) UIViewController *view;
@property (nonatomic, strong) ProfileViewController *profileView;
@property (nonatomic, strong) VideoFinishedViewController *videoFinishView;
@property (nonatomic, strong) AFURLSessionManager *manager;
@property (nonatomic, strong) NSURLSessionDownloadTask *downloadTask;
@end
