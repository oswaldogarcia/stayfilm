//
//  AlbumInternal.h
//  Stayfilm-iOS-Full
//
//  Created by Henrique Ormonde on 16/10/17.
//  Copyright © 2017 Stayfilm. All rights reserved.
//

@import Photos;
#import <Foundation/Foundation.h>

@interface AlbumInternal : NSObject

@property (nonatomic, strong) NSString *title;
//@property (nonatomic, strong) PHAsset *imageUP;
//@property (nonatomic, strong) PHAsset *imageDOWN;
//@property (nonatomic, strong) PHAsset *imageBIG;
@property (nonatomic, assign) int count;
@property (nonatomic, strong) NSMutableArray *medias;

+ (id)customClassWithProperties:(NSDictionary *)properties;
- (id)initWithProperties:(NSDictionary *)properties;

@end
