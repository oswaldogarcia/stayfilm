//
//  SegmentedProgressBar.m
//  SegmentedProgressBar
//
//  Created by ;;; on 9/14/15.
//  Copyright © 2015 kurtu. All rights reserved.
//

#import "SegmentedProgressBar.h"

@implementation SegmentedProgressBar

#pragma mark - UIView

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self initializeDefaults];
        [self updateSegments];
    }
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self updateSegments];
}

#pragma mark - SegmentedProgressBar

- (void)setSegmentColor:(UIColor *)segmentColor {
    _segmentColor = segmentColor;
    [self setNeedsLayout];
}

- (void)setCompletedSegmentColor:(UIColor *)completedSegmentColor {
    _completedSegmentColor = completedSegmentColor;
    [self setNeedsLayout];
}

- (void)setSegmentSeperatorWidth:(CGFloat)segmentSeperatorWidth {
    _segmentSeperatorWidth = segmentSeperatorWidth;
    [self setNeedsLayout];
}

- (void)setNumberOfSegments:(NSInteger)numberOfSegments {
    _numberOfSegments = numberOfSegments;
    [self setNeedsLayout];
}

- (void)setNumberOfCompletedSegments:(NSInteger)numberOfCompletedSegments {
    _numberOfCompletedSegments = numberOfCompletedSegments;
    [self setNeedsLayout];
}
- (void)setSegmentPrgress:(CGFloat)progress {
    _segmentProgress = progress;
    [self updateSegments];
}
- (void)updateSegmentPrgress:(CGFloat)progress {
    _segmentProgress = progress;
//    [self updateSegments];
    
    [self updateSubviews];
}
#pragma mark - Private Methods

- (void)initializeDefaults {
    self.backgroundColor = [UIColor clearColor];
    _segmentSeperatorWidth = 2.0;
    _numberOfSegments = 3;
    _numberOfCompletedSegments = 0;
    _completedSegmentColor = [UIColor blueColor];
    _segmentColor = [_completedSegmentColor colorWithAlphaComponent:0.4];
}

- (void)updateSegments {
    [self removeSubviews];
    
    CGFloat segmentWidth = (self.bounds.size.width - ((self.numberOfSegments - 1) * self.segmentSeperatorWidth)) / self.numberOfSegments;
    
    for (NSInteger i = 0; i < self.numberOfSegments; i++) {
        UIProgressView *segment = [[UIProgressView alloc] initWithFrame:CGRectMake(i * (segmentWidth + self.segmentSeperatorWidth), 0, segmentWidth, self.bounds.size.height)];
        segment.tintColor = _segmentColor;
        segment.progressTintColor = _completedSegmentColor;
        if (i < self.numberOfCompletedSegments) {
            //segment.backgroundColor = self.completedSegmentColor;
            segment.progress = 1;
            
        }
//        else  if (i == self.numberOfCompletedSegments) {
//            NSLog(@"%f",_segmentProgress);
//
//            [UIView animateWithDuration:1.0 animations:^{
//                segment.progress = self.segmentProgress;
//            }];
//
//            //segment.progress = _segmentProgress;
//        }
        else {
            //segment.backgroundColor = self.segmentColor;
            segment.progress = 0;
        }
        
        if (i == 0) {
            UIBezierPath *bezierPath = [UIBezierPath bezierPathWithRoundedRect:segment.bounds
                                                             byRoundingCorners:UIRectCornerTopLeft | UIRectCornerBottomLeft
                                                                   cornerRadii:CGSizeMake(segment.frame.size.height / 2, segment.frame.size.height / 2)];
            CAShapeLayer *shapeLayer = [[CAShapeLayer alloc] init];
            shapeLayer.frame = segment.bounds;
            shapeLayer.path = bezierPath.CGPath;
            segment.layer.mask = shapeLayer;
        } else if (i == (self.numberOfSegments - 1)) {
            UIBezierPath *bezierPath = [UIBezierPath bezierPathWithRoundedRect:segment.bounds
                                                             byRoundingCorners:UIRectCornerTopRight | UIRectCornerBottomRight
                                                                   cornerRadii:CGSizeMake(segment.frame.size.height / 2, segment.frame.size.height / 2)];
            CAShapeLayer *shapeLayer = [[CAShapeLayer alloc] init];
            shapeLayer.frame = segment.bounds;
            shapeLayer.path = bezierPath.CGPath;
            segment.layer.mask = shapeLayer;
        }
        
        [self addSubview:segment];
    }
}

- (void)removeSubviews {
    for (UIView *subview in self.subviews) {
        [subview removeFromSuperview];
    }
}

- (void)updateSubviews {
    
    for (NSInteger i = 0; i < self.subviews.count; i++) {

          UIProgressView *segment = self.subviews[i];
        if (i == self.numberOfCompletedSegments) {
        //NSLog(@"%f",_segmentProgress);
            
            [segment setProgress:_segmentProgress animated:YES];
            
        }
     
    }
}

@end
