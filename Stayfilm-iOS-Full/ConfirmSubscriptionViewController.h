//
//  ConfirmSubscriptionViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 4/10/19.
//  Copyright © 2019 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ConfirmSubscriptionViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *confirmLabel;

@end

NS_ASSUME_NONNULL_END
