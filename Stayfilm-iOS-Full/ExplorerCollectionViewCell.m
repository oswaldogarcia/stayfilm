//
//  ExplorerCollectionViewCell.m
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 13/06/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import "ExplorerCollectionViewCell.h"

@implementation ExplorerCollectionViewCell


- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.storieImage.layer.cornerRadius = 10.0;
    self.storieImage.clipsToBounds = YES;
    
    self.borderImage.layer.cornerRadius = 10.0;
    self.borderImage.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected{
    
//    if(selected){
//        
//        [UIView animateWithDuration:0.8 animations:^{
//            [self.borderImage setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
//        }];
//    }
   
}
@end


