//
//  StoriesContentViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 1/10/19.
//  Copyright © 2019 Stayfilm. All rights reserved.
//

#import "StoriesContentViewController.h"

@interface StoriesContentViewController ()<PlayerExplorerDelegate>
@property(atomic, strong) NSTimer *timer;
@end

@implementation StoriesContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self prefersStatusBarHidden];
    [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    self.playerVC.showsPlaybackControls = NO;
    self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    [self.avplayerContainer setFrame:self.view.frame];
    [self.playerVC.view setFrame:self.view.frame];
    [self getAnalyticString];
    [self.storyLogoImage sd_setImageWithURL:[NSURL URLWithString:self.story.logoUrl]];
    [self buttonIsEnable:NO];
    [self.swipeUpView setHidden:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(becomeActive) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resignActive) name:UIApplicationWillResignActiveNotification object:nil];
}
- (void)becomeActive {
    NSLog(@"active");
    [self starTimer];
    [self.playerVC.player play];
}

- (void)resignActive {
    NSLog(@"resign active");
    [self stopTimer];
    [self.playerVC.player pause];
}

- (void)viewDidAppear:(BOOL)animated{
    
    if (self.story.isStoryViewed){
        self.indexConten = -1;
    }
    
    self.isExiting = NO;
    
    [self resetPlayer];
    
    [self setPlayers];
    
    [self initialConfiguration];
    
    [self nextAction:nil];
    
    //Google Analytics
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:[@"explore_gallery-" stringByAppendingString:[self.story.title stringByReplacingOccurrencesOfString:@" " withString:@"_"]]];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)viewWillDisappear:(BOOL)animated{
  
    self.isExiting = YES;
    
}
- (void)dealloc{
    self.isExiting = YES;
    [self resetPlayer];
}
-(NSString *)getAnalyticString {
    @try {
        StoryContent *content = self.storiesContent[self.indexConten];
        
        NSString *analytic = nil;
        switch (content.type) {
            case ExploreStoryContent_IMAGE:
                analytic = @"image";
                break;
            case ExploreStoryContent_MoviePORTRAIT:
            case ExploreStoryContent_MovieLANDSCAPE:
                if(content.idMovie == nil || [content.idMovie isEqualToString:@""]) {
                    analytic = @"film";
                } else {
                    analytic = @"video";
                }
                break;
                
            default:
                break;
        }
        self.sfApp.analyticsTag = analytic;
        return analytic;
    }@catch(NSException *ex) {
        NSLog(@"StayLog: EXCEPTION in getAnalyticstring with error: %@",ex.description);
        return nil;
    }
}

- (void)setPlayers{
    @try {
        if(self.story.myPlayers.count == self.story.content.count){
            self.myPlayers = self.story.myPlayers;
        }
    } @catch (NSException *exception) {
         NSLog(@"StayLog: EXCEPTION in setPlayers with error: %@", exception.description);
    }
}

/*-(void)callAllPlayers{
    __weak typeof(self) weakSelf = self;
    
    [self.view makeToastActivity:CSToastPositionCenter];

    [self.story setPlayersWithCompletion:^{

        weakSelf.myPlayers = weakSelf.story.myPlayers;
        [weakSelf.view hideToastActivity];
        [weakSelf nextAction:nil];
    }];
}*/

- (void)initialConfiguration{
    
    /* configuration of the segmented bar */
    if (@available(iOS 11.0, *)) {
        UIWindow *mainWindow = [[[UIApplication sharedApplication] delegate] window];
        if (mainWindow.safeAreaInsets.top > 0.0) {  // is iPhone X
            self.segmentedProgressBar = [[SegmentedProgressBar alloc] initWithFrame:CGRectMake(10, 35, self.view.frame.size.width - 20, 7)];
        } else {
            self.segmentedProgressBar = [[SegmentedProgressBar alloc] initWithFrame:CGRectMake(10, 15, self.view.frame.size.width - 20, 7)];
        }
    }
    else {
        self.segmentedProgressBar = [[SegmentedProgressBar alloc] initWithFrame:CGRectMake(10, 15, self.view.frame.size.width - 20, 7)];
    }
    
    self.segmentedProgressBar.completedSegmentColor = [UIColor whiteColor];
    self.segmentedProgressBar.segmentColor = [UIColor lightGrayColor];
    self.segmentedProgressBar.numberOfSegments = (int)self.storiesContent.count;
    self.segmentedProgressBar.numberOfCompletedSegments = self.indexConten;
    [self.segmentedProgressBar setSegmentProgress:0.0];
    
    [self.view addSubview:self.segmentedProgressBar];
    [self.segmentedProgressBar.layer setZPosition:1];
    /* end configuration of the segmented bar */
    
    /* Add Swipe Down Gesture*/
    UISwipeGestureRecognizer *swipeDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeDownGesture)];
    [swipeDown setDirection:UISwipeGestureRecognizerDirectionDown];
    [self.view addGestureRecognizer:swipeDown];

    
    // Add Swipe Up Gesture
    UISwipeGestureRecognizer *swipeUp = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeUpGesture)];
    [swipeUp setDirection:UISwipeGestureRecognizerDirectionUp];
    [self.view addGestureRecognizer:swipeUp];
    
    
    // Add Long Press Gesture
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc]
                                               initWithTarget:self
                                               action:@selector(handleLongPress:)];
    longPress.minimumPressDuration = 0.5;
    [self.view addGestureRecognizer:longPress];
    
    // Add double tap Gesture to replace the native double tap zoom gesture on avplayer
//    UITapGestureRecognizer *dobleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dobleTapGesture)];
//    [dobleTap setNumberOfTapsRequired:2];
//    [self.view addGestureRecognizer:dobleTap];
    
}

// Content duration
#pragma mark - Timer

- (void)starTimer{
    [self stopTimer];
    self.timer = nil;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(myTicker) userInfo:nil repeats:YES];//0,001
}
-(void)stopTimer{
    if(self.timer){
        [self.timer invalidate];
    }
}

-(void)myTicker{
    
    if(!self.isExiting){
        //increment the timer
        self.timeTick++;

        CGFloat progress = (float)((self.timeTick)/self.storyDuration)/10;
    
        if (!_isImage) {
            [self showLoading];
        }
        [self updateProgressBar:progress];
        //if we want the timer to stop after a certain number of seconds we can do
        if(self.timeTick / 10 == self.storyDuration){ //stop the timer after n seconds
            [self stopTimer];
            [self.playerVC.player pause];
            [self nextAction:nil];
        }
    }else{
        [self stopTimer];
        [self.playerVC.player pause];
    }
}

-(void)showLoading{
    
    switch (self.playerVC.player.timeControlStatus) {
        case AVPlayerTimeControlStatusPaused:
            
            if(!_isLongPress){
                [self.timer invalidate];
                dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                    //Background Thread
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        //Run UI Updates
                        [self.view makeToastActivity:CSToastPositionCenter];
                        [self.playerVC.player play];
                    });
                });
            }
            break;
        case  AVPlayerTimeControlStatusWaitingToPlayAtSpecifiedRate:
            [self.view makeToastActivity:CSToastPositionCenter];
            self.timeTick = self.timeTick - 1;
            break;
        case  AVPlayerTimeControlStatusPlaying:
            [self.view hideToastActivity];
            [self starTimer];
            break;
        default:
            break;
    }
    
}
#pragma mark - Navgation Stories

- (IBAction)nextAction:(id)sender {
    
    self.indexConten = self.indexConten + 1;
    
    if (self.indexConten < (int)self.storiesContent.count){
        
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:[self getAnalyticString]
                                                              action:@"tap-right"
                                                               label:@""
                                                               value:@1] build]];
        [self setStoryContent];
        [self setContentViewed];
    }else{
        //next Story
        @try {
            [self setContentViewed];
            self.indexConten = -1;
            [self.delegate moveToNext];
            
        } @catch (NSException *exception) {
            NSLog(@"StayLog: EXCEPTION in nextAction with error: %@", exception.description);
        }
        
    }
    
}
- (IBAction)backAction:(id)sender {
    
    self.indexConten = self.indexConten - 1;
    if ((self.indexConten < (int)self.storiesContent.count) &&  (self.indexConten >= 0)){
        
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:[self getAnalyticString]
                                                              action:@"tap-left"
                                                               label:@""
                                                               value:@1] build]];
        [self setStoryContent];
    }else if(self.indexConten < 0){
        //prev Story
        @try {
            self.indexConten = 0;
            [self.delegate moveToBack];
        } @catch (NSException *exception) {
            NSLog(@"StayLog: EXCEPTION in backAction with error: %@", exception.description);
        }
        
    }
    
}


-(void)updateProgressBar:(CGFloat)progress{
    [self.segmentedProgressBar updateSegmentPrgress:progress];
}

-(void)resetPlayer{
    
    [self.playerVC.player pause];
    [self.playerVC.player seekToTime:CMTimeMake(0, 1)];
    
    
    [self.segmentedProgressBar setSegmentProgress:0.0];
    
    self.timeTick = 0;
    [self stopTimer];
    
    if(self.playerVC != nil){
    [self.playerVC willMoveToParentViewController:nil];
    [self.playerVC.view removeFromSuperview];
    [self.playerVC removeFromParentViewController];
    }
}

-(void)setStoryContent{
    
    [self resetPlayer];
    
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"explore-gallery"
                                                          action:[self.story.title stringByReplacingOccurrencesOfString:@" " withString:@"_"]
                                                           label:[self getAnalyticString]
                                                           value:@1] build]];
    
    StoryContent *content = self.storiesContent[self.indexConten];
    
    self.segmentedProgressBar.numberOfCompletedSegments = self.indexConten;
    
    [self.segmentedProgressBar setNumberOfCompletedSegments:self.indexConten];
    
    if(content.contentDescription != nil){
        [self.contentDescriptionLabel setHidden:NO];
        [self.contentDescriptionLabel setText:content.contentDescription];
    }else{
        [self.contentDescriptionLabel setHidden:YES];
        [self.contentDescriptionLabel setText:@""];
    }
    
    
    if(content.idGenre != nil) {
        [self buttonIsEnable:YES];
        [self.sfApp.storyState setCurrentViewedIDGenre:[content.idGenre intValue] andIDTemplate:((content.idTemplate != nil)? [content.idTemplate intValue] : -1)];
    } else {
        [self buttonIsEnable:NO];
        [self.sfApp.storyState setCurrentViewedIDGenre:-1 andIDTemplate:-1];
    }
    
    [self changeButtonText:content.buttonText Color:[SFHelper colorFromHexString:content.buttonColor] andIcon:content.buttonIcon];
    
    
    
    if (content.type != 3 ){
        @try {
            self.isImage = NO;
            [self showSwipeUpToast:YES];
            [self setBackgroundImage];
            
            [self setStoryDuration:[content.duration floatValue]];
            
          if(self.myPlayers != nil && self.myPlayers.count == self.story.content.count ){
                self.playerVC.player = self.myPlayers[self.indexConten];
          }else{
            
            if(self.story.myPlayers.count > self.indexConten && [self.story.myPlayers[self.indexConten] isKindOfClass:[AVPlayer class]]){
    
                self.playerVC.player = self.story.myPlayers[self.indexConten];
              
            }else{
                self.playerVC.player = [AVPlayer playerWithURL:[NSURL URLWithString:content.videoUrl]];
            }
          }
            [self addChildViewController:self.playerVC];
            [self.avplayerContainer addSubview:self.playerVC.view];
            [self.playerVC didMoveToParentViewController:self];
            
            [self.playerVC.player play];
            
            [self starTimer];
        } @catch (NSException *exception) {
             NSLog(@"StayLog: EXCEPTION in setStoryContent [video] with error: %@", exception.description);
        }
        
    }else{
        self.isImage = YES;
        [self showSwipeUpToast:NO];
        [self setBackgroundImage];
        
    }
}
-(void)setBackgroundImage{

    StoryContent *content = self.storiesContent[self.indexConten];
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    imageView.tag = 99;
    
    if(content.type != 3){
    for (UIView *view in self.playerVC.view.subviews){
        if (view.tag == 99){
            [view removeFromSuperview];
        }
    }
   
    [imageView sd_setImageWithURL:[NSURL URLWithString:content.imgUrl] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        
        [imageView setImage:[SFHelper blurredImageWithImage:image]];
        
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.clipsToBounds = YES;
        
        CIContext *context = [CIContext contextWithOptions:nil];
        CIImage *inputImage = [[CIImage alloc] initWithImage:imageView.image]; //your input image
        
        CIFilter *filter= [CIFilter filterWithName:@"CIColorControls"];
        [filter setValue:inputImage forKey:@"inputImage"];
        [filter setValue:[NSNumber numberWithFloat:-0.05] forKey:@"inputBrightness"];
        
        // Your output image
        [imageView setImage:[UIImage imageWithCGImage:[context createCGImage:filter.outputImage fromRect:filter.outputImage.extent]]];
        
        [self.playerVC.view addSubview:imageView];
        [self.playerVC.view sendSubviewToBack:imageView];
    }];
    }else{
    
        for (UIView *view in self.avplayerContainer.subviews){
            if (view.tag == 99){
                [view removeFromSuperview];
            }
        }
        [self setStoryDuration:[content.duration floatValue]];
        
        [self.view makeToastActivity:CSToastPositionCenter];
        [self stopTimer];
        
         [imageView sd_setImageWithURL:[NSURL URLWithString:content.imgUrl] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
             
             [imageView setImage:image];
             
             imageView.contentMode = UIViewContentModeScaleAspectFill;
             imageView.clipsToBounds = YES;
             
             CIContext *context = [CIContext contextWithOptions:nil];
             CIImage *inputImage = [[CIImage alloc] initWithImage:imageView.image]; //your input image
             
             CIFilter *filter= [CIFilter filterWithName:@"CIColorControls"];
             [filter setValue:inputImage forKey:@"inputImage"];
             [filter setValue:[NSNumber numberWithFloat:-0.05] forKey:@"inputBrightness"];
             
             // Your output image
             [imageView setImage:[UIImage imageWithCGImage:[context createCGImage:filter.outputImage fromRect:filter.outputImage.extent]]];
             
             [self.avplayerContainer addSubview:imageView];
             [self.view hideToastActivity];
             [self starTimer];
         }];
    
    }
}

- (void)setContentViewed{
    
    StoryContent *contentToSetViewed;
    
    int contentPosition = self.indexConten - 1;
    
    if (contentPosition < 0){
        contentToSetViewed = self.storiesContent[0];
    }else{
        contentToSetViewed = self.storiesContent[contentPosition];
    }
    if(!self.story.isStoryViewed){
        @try {
            StoryState *storyState = [StoryState sharedStoryStateSingleton];
            [storyState setViewedStoryID:self.story.idStory withIDContent:contentToSetViewed.idContent];
        } @catch (NSException *exception) {
            NSLog(@"StayLog: EXCEPTION setContentViewed: %@", exception.description);
        }
    }
}

-(void)showSwipeUpToast:(BOOL)show {
    
    if(show){
        [self.swipeUpView setHidden:NO];
        [UIView animateWithDuration:5.0 animations:^{
            [self.swipeUpView setAlpha:1.0];
        } completion:^(BOOL finished) {
            
            [UIView animateWithDuration:5.0 animations:^{
                [self.swipeUpView setAlpha:0.0];
            } completion:nil];
            
            
        }];
        
    }else{
        [self.swipeUpView setHidden:YES];
        [self.swipeUpView setAlpha:0.0];
    }
}

-(void)buttonIsEnable:(BOOL)enable{
    [self.makeAFilmView setHidden:!enable];
    [self.makeAFilmButton setHidden:!enable];
}

- (void)changeButtonText:(NSString *)text Color:(UIColor *)color andIcon:(NSString*) imageString{
    
    if(!self.makeAFilmView.isHidden){
        self.makeAFilmLabel.text = text;
        
        [self.makeAFilmView setBackgroundColor:color];
        
        if(imageString != nil){
            [self.makeAFilmImage sd_setImageWithURL:[NSURL URLWithString:imageString]];
        }else{
            [self.makeAFilmImage setImage:[UIImage imageNamed:@"clapper"]];
        }
    }
}
- (IBAction)makeAFilmAction:(id)sender {
    
    if(self.sfApp == nil)
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    
    self.sfApp.selectedGenre = self.sfApp.storyState.currentGenre;
    self.sfApp.selectedTemplate = self.sfApp.storyState.currentTemplate;
    self.sfApp.isFromExplorer = YES;
    
    if(self.sfApp.currentUser == nil || self.sfApp.currentUser.idUser == nil || [self.sfApp.currentUser.idUser isEqualToString:@""]) {
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:self.sfApp.analyticsTag
                                                              action:@"make-film"
                                                               label:@"create_account"
                                                               value:@1] build]];
    } else {
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:self.sfApp.analyticsTag
                                                              action:@"make-film"
                                                               label:@"film_producing"
                                                               value:@1] build]];
    }
    
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"choice"
                                                          action:self.sfApp.selectedGenre.slug
                                                           label:[NSString stringWithFormat:@"%li", self.sfApp.selectedTemplate.idtemplate]
                                                           value:@1] build]];
    
//    self.playerVC.player = nil;
//    [self resetPlayer];
//    [self dismissViewControllerAnimated:YES completion:nil];
    [self closeAction:nil];
    [self.delegate makeAfilmButtonTapped];
}

- (IBAction)closeAction:(id)sender {
    
    
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:self.sfApp.analyticsTag
                                                          action:@"close"
                                                           label:@"button-x"
                                                           value:@1] build]];

    [self resetPlayer];
    self.playerVC.player = nil;
    
    [self.playerVC removeFromParentViewController];
   // [self.view removeFromSuperview];
    self.isExiting = YES;
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Gesture Action

-(void)swipeDownGesture{

    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:[self getAnalyticString]
                                                          action:@"close"
                                                           label:@"swipe-down"
                                                           value:@1] build]];
    
    [self closeAction:nil];
}

-(void)swipeUpGesture{
    
    if (!self.isImage){
        
        self.isSwipeUP = YES;
        self.delegate.isSwipeUP = YES;
        
        [self stopTimer];
        [self.playerVC.player pause];
        
        StoryContent *content = self.storiesContent[self.indexConten];
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue]  < 11.0 ){
            
            AVPlayerViewController *newPlayerVC = [[AVPlayerViewController alloc]init];
                newPlayerVC.player = [AVPlayer playerWithURL:[NSURL URLWithString:content.videoUrl]];
            [newPlayerVC setShowsPlaybackControls:YES];
            newPlayerVC.player.automaticallyWaitsToMinimizeStalling = NO;
            [newPlayerVC.player play];
            [self presentViewController:newPlayerVC animated:YES completion:nil];
            
        }else{
    
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"Player" owner:self options:nil];
            PlayerViewController *moviePlayerViewController = [topLevelObjects objectAtIndex:0];
            moviePlayerViewController.player.automaticallyWaitsToMinimizeStalling = NO;
            moviePlayerViewController.currentMovie = nil;
            moviePlayerViewController.comeFromStory = YES;
            moviePlayerViewController.showsPlaybackControls = YES;
            moviePlayerViewController.playerExplorerDelegate = self;
            
            moviePlayerViewController.player = [AVPlayer playerWithURL:[NSURL URLWithString:content.videoUrl]];
            [moviePlayerViewController.player seekToTime: self.playerVC.player.currentTime];
            
            moviePlayerViewController.view.frame = self.view.frame;

           
            
            [self presentViewController:moviePlayerViewController animated:YES completion:nil];
            
        }
    }
    
    //Google Analytics Event
   id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:[self getAnalyticString]
                                                          action:@"swipe-up"
                                                           label:@""
                                                           value:@1] build]];
}

-  (void)handleLongPress:(UILongPressGestureRecognizer*)sender {
    if (sender.state == UIGestureRecognizerStateEnded) {
        if (self.playerVC.player != nil){
            self.isLongPress = NO;
            [self.playerVC.player play];
            [self starTimer];
        }else{
            [self starTimer];
        }
    }
    else if (sender.state == UIGestureRecognizerStateBegan){
        if (self.playerVC.player != nil){
            self.isLongPress = YES;
            [self.playerVC.player pause];
            [self.timer invalidate];
        }else{
            [self.timer invalidate];
        }
    }
}

-(void)dobleTapGesture{
        NSLog(@"DOUBLE TAP");
}

#pragma mark - Player Explore Delegate

- (void)playStoryAgain{
    
    if (self.playerVC.player != nil){
        
        [self.playerVC.player play];
        [self starTimer];
        self.delegate.isSwipeUP = NO;
        self.isSwipeUP = NO;
    }
    
}


@end
