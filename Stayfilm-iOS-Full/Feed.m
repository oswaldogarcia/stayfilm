//
//  Feed.m
//  fbmessenger
//
//  Created by Henrique Ormonde on 26/06/15.
//  Copyright (c) 2015 Stayfilm. All rights reserved.
//

#import "Feed.h"
#include "StayWS.h"
#import "StayfilmApp.h"
#import "Movie.h"

@implementation Feed

@synthesize itemsList, rawResult, offset, offsetPrevious, timestampNow, feedPage;

StayfilmApp *sfAppFeed;

//+ (id)customClassWithProperties:(NSDictionary *)properties {
//    if(properties != nil)
//    {
//        NSMutableDictionary *filteredObjs = [[NSMutableDictionary alloc] init];
//        for (NSString* key in properties) {
//            id value = [properties objectForKey:key];
//            if ([key isEqualToString:@"id"] || [key isEqualToString:@"idAlbum"]) {
//                [filteredObjs setValue:value forKey:@"idAlbum"];
//            }
//            else if ([key isEqualToString:@"idMedias"]) {
//                [filteredObjs setValue:value forKey:@"idMedias"];
//            }
//            else if ([key isEqualToString:@"name"]) {
//                [filteredObjs setValue:value forKey:@"name"];
//            }
//            else if ([key isEqualToString:@"description"]) {
//                [filteredObjs setValue:value forKey:@"description"];
//            }
//            else if ([key isEqualToString:@"cover"]) {
//                [filteredObjs setValue:value forKey:@"cover"];
//            }
//            else if ([key isEqualToString:@"network"]) {
//                [filteredObjs setValue:value forKey:@"network"];
//            }
//            else if ([key isEqualToString:@"mediaCount"]) {
//                [filteredObjs setValue:value forKey:@"mediaCount"];
//            }
//        }
//        return [[self alloc] initWithProperties:filteredObjs];
//    }
//    else
//    {
//        return nil;
//    }
//}
//
//- (id)initWithProperties:(NSDictionary *)properties {
//    if (self = [self init]) {
//        @try {
//            [self setValuesForKeysWithDictionary:properties];
//        }
//        @catch (NSException *ex) {
//            return nil;
//        }
//    }
//    return self;
//}

-(id)init {
    self = [super init];
    if (0 != self) {
        self.itemsList = [[NSMutableArray alloc] init];
    }
    return self;
}

+(Feed *)getMyFilmsWithLimit:(int)p_limit withOffset:(NSString *)p_offset andIsRefresh:(BOOL)p_refreshNew andIsCacheEnabled:(BOOL)p_cache
{
    Feed *feed = [[Feed alloc] init];
    @try
    {
        StayWS *ws = [[StayWS alloc] init];
        sfAppFeed = [StayfilmApp sharedStayfilmAppSingleton];
        NSMutableDictionary *args = [[NSMutableDictionary alloc] init];
        NSDictionary *response = nil;
        NSArray *pathArgs = nil;
        
        //[args setValue:@"fbmessenger" forKey:@"slug"];
        
        if(p_refreshNew)
        {
            [args setValue:@"1" forKey:@"new"];
        }
        if(p_offset != nil)
        {
            [args setValue:p_offset forKey:@"offset"];
        }
        if(p_limit > 0)
        {
            [args setValue:[[NSString alloc] initWithFormat:@"%d", p_limit] forKey:@"limit"];
        }
        
        pathArgs = [NSArray arrayWithObject:sfAppFeed.settings[@"idUser"]];
        
        response = [ws requestWebServiceWithURL:[sfAppFeed.wsConfig getConfigPathWithKey:@"myFilms"] withRequestType:@"GET" withPathArguments:pathArgs withArguments:args withIdSession:sfAppFeed.settings[@"idSession"] withCache:p_cache];
        
        NSHTTPURLResponse *statusResponse = (NSHTTPURLResponse *) ws.responseMessage;
        if(response != nil && (statusResponse.statusCode == 200 || statusResponse.statusCode == 201))
        {
            feed.rawResult = response;
            feed.offset = (response[@"offset"] == nil) ? @"" : response[@"offset"];
            feed.timestampNow = (response[@"timestampNow"] == nil) ? @"" : response[@"timestampNow"];
            
            if(response != nil && response[@"data"] != nil)
            {
                for (NSDictionary* object in response[@"data"]) {
                    [feed.itemsList addObject:[Movie customClassWithProperties:object]];
                }
            }
            if(feed.itemsList.firstObject != nil && [(Movie *)feed.itemsList.firstObject created] != nil)
            {
                feed.offsetPrevious = [(Movie *)feed.itemsList.firstObject created];
            }
        }
        return feed;
    }
    @catch (NSException *ex)
    {
        return feed;
    }
}

@end
