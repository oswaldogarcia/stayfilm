//
//  AlbumFB.h
//  Stayfilm for Messenger
//
//  Created by Henrique Ormonde on 14/05/15.
//  Copyright (c) 2015 Stayfilm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import "PhotoFB.h"

// Album Facebook
@interface AlbumFB : NSObject

@property (nonatomic, strong) NSString *idAlbum;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *link;
@property (nonatomic, strong) NSString *cover_photo;
@property (nonatomic, strong) NSNumber *count;
@property (nonatomic, strong) NSString *after;
@property (nonatomic, strong) NSMutableArray *idMedias;

+ (id)customClassWithProperties:(NSDictionary *)properties;
- (id)initWithProperties:(NSDictionary *)properties;


@end
