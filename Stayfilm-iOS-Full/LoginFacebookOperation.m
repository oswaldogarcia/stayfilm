//
//  LoginFacebookOperation.m
//  Stayfilm-iOS-Full
//
//  Created by Henrique Ormonde on 04/09/17.
//  Copyright © 2017 Stayfilm. All rights reserved.
//

#import "LoginFacebookOperation.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Google/Analytics.h>
#import "Reachability.h"
#import <SystemConfiguration/SystemConfiguration.h>

@interface LoginFacebookOperation ()

@property (nonatomic, strong) NSArray *facebookPermissions;

@end


@implementation LoginFacebookOperation

@synthesize sfAppLogin, facebookPermissions, messageCode;


#pragma mark -
#pragma mark - Callable Methods

-(id)initWithDelegate:(id<LoginFacebookOperationDelegate>)p_delegate andSFApp:(StayfilmApp *)p_sfApp {
    if( self = [super init] ) {
        self.delegate = p_delegate;
        self.sfAppLogin = p_sfApp;
        self.facebookPermissions = @[@"public_profile", @"email", @"user_photos", @"user_videos"];
        self.messageCode = -1;
        self.overrideUser = YES;
    }
    return self;
}

-(id)initWithoutUserOverrideWithDelegate:(id<LoginFacebookOperationDelegate>)p_delegate andSFApp:(StayfilmApp *)p_sfApp {
    if( self = [super init] ) {
        self.delegate = p_delegate;
        self.sfAppLogin = p_sfApp;
        self.facebookPermissions = @[@"public_profile", @"email", @"user_photos", @"user_videos"];
        self.messageCode = -1;
        self.overrideUser = NO;
    }
    return self;
}

-(id)initWithDelegate:(id<LoginFacebookOperationDelegate>)p_delegate andSFApp:(StayfilmApp *)p_sfApp andPermissions:(NSArray *)p_permissions {
    if( self = [super init] ) {
        self.delegate = p_delegate;
        self.sfAppLogin = p_sfApp;
        self.facebookPermissions = p_permissions;
        self.messageCode = -1;
    }
    return self;
}


-(BOOL)connected {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

#pragma mark - Custom Methods

-(void)loginToStayfilmWithToken:(NSString *)p_token {
    @try {
        if(self.sfAppLogin.sfConfig != nil) //check if config is loaded
        {
            User *appUser = nil;
            if(self.overrideUser)
                appUser = [User loginWithFacebook:p_token andSFAppSingleton:self.sfAppLogin]; // already saves to sfApp.settings
            else
                appUser = [User loginWithFacebookWithoutUserOverride:p_token andSFAppSingleton:self.sfAppLogin];
            
            if(self.sfAppLogin.currentUser != nil && self.sfAppLogin.currentUser.idUser != nil && appUser != nil && appUser.isLogged && appUser.idUser != nil && [self.sfAppLogin.currentUser.idUser isEqualToString:appUser.idUser])
            { //this will check if user login in via email and got different user from facebook connect
                [self.sfAppLogin.settings setObject:[[FBSDKAccessToken currentAccessToken] userID] forKey:@"FB_id"];
                [self.sfAppLogin.settings setObject:[[FBSDKAccessToken currentAccessToken] tokenString] forKey:@"facebookToken"];
                [self.sfAppLogin saveSettings];
                self.messageCode = LOGIN_SUCCESSFUL;
                [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginFacebookDidFinish:) withObject:self waitUntilDone:NO];
                return;
            }
            else if(self.sfAppLogin.currentUser != nil && self.sfAppLogin.currentUser.idUser != nil && appUser != nil && appUser.isLogged && appUser.idUser != nil && ![self.sfAppLogin.currentUser.idUser isEqualToString:appUser.idUser])
            {
                [self.sfAppLogin.settings setObject:[[FBSDKAccessToken currentAccessToken] userID] forKey:@"FB_id"];
                [self.sfAppLogin.settings setObject:[[FBSDKAccessToken currentAccessToken] tokenString] forKey:@"facebookToken"];
                [self.sfAppLogin saveSettings];
                self.messageCode = DIFFERENT_USER;
                [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginFacebookDidFinish:) withObject:self waitUntilDone:NO];
                return;
            }
            if(self.overrideUser) {
                self.sfAppLogin.currentUser = appUser;
            }
            if(appUser == nil)
            {
                //Connection error
                self.messageCode = CONNECTION_ERROR;
                [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginFacebookDidFail:) withObject:self waitUntilDone:NO];
            }
            else if(!appUser.userExists && !appUser.isResponseError) // register user
            {
                [self.sfAppLogin.settings setObject:p_token forKey:@"facebookToken"];
                [self.sfAppLogin saveSettings];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id,birthday,email,first_name,last_name,gender"}]
                     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                         if (!error)
                         {
                             NSMutableDictionary *registerArgs = [[NSMutableDictionary alloc] init];
                             [registerArgs setObject:self.sfAppLogin.settings[@"facebookToken"] forKey:@"facebookToken"];
                             
                             if(result[@"birthday"] != nil)
                             {
                                 NSDateFormatter *dateformat = [[NSDateFormatter alloc] init];
                                 [dateformat setDateFormat:@"MM/dd/yyyy"];
                                 NSDate * date = [dateformat dateFromString:result[@"birthday"]];
                                 NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:date];
                                 [registerArgs setObject:[NSString stringWithFormat:@"%d", (int)[components day]]forKey:@"birthdayDay"];
                                 [registerArgs setObject:[NSString stringWithFormat:@"%d", (int)[components month]]forKey:@"birthdayMonth"];
                                 [registerArgs setObject:[NSString stringWithFormat:@"%d", (int)[components year]]forKey:@"birthdayYear"];
                             }
                             if(result[@"email"] != nil)
                             {
                                 [registerArgs setObject:(NSString *)result[@"email"] forKey:@"email"];
                             }
                             if(result[@"first_name"] != nil)
                             {
                                 [registerArgs setObject:(NSString *)result[@"first_name"] forKey:@"firstName"];
                             }
                             if(result[@"last_name"] != nil)
                             {
                                 [registerArgs setObject:(NSString *)result[@"last_name"] forKey:@"lastName"];
                             }
                             if(result[@"gender"] != nil)
                             {
                                 if([result[@"gender"] isEqualToString:@"male"])
                                 {
                                     [registerArgs setObject:@"m"forKey:@"gender"];
                                 }
                                 else if([result[@"gender"] isEqualToString:@"female"])
                                 {
                                     [registerArgs setObject:@"f" forKey:@"gender"];
                                 }
                                 else
                                 {
                                     [registerArgs setObject:@"n" forKey:@"gender"];
                                 }
                             }
                             
                             if(result[@"id"] != nil)
                             {
                                 User *registered = [User registerUserWithArguments:registerArgs andSFAppSingleton:self.sfAppLogin]; // already saves to sfApp.settings
                                 if(self.overrideUser) {
                                     self.sfAppLogin.currentUser = registered;
                                 }
                                 
                                 if(registered != nil && registered.isLogged)
                                 {
                                     // check Login   0 = not started  1 = true   2 = false
                                     //[self.sfAppLogin.settings setObject:@"1" forKey:@"CHECK_Login"];
                                     [self.sfAppLogin.settings setObject:@"1" forKey:@"SF_User_Registered"];
                                     [self.sfAppLogin.settings setObject:(NSString *)result[@"id"] forKey:@"FB_id"];
                                     [self.sfAppLogin.settings setObject:[[FBSDKAccessToken currentAccessToken] tokenString] forKey:@"facebookToken"];
                                     [self.sfAppLogin saveSettings];
                                     
                                     //Google Analytics Event
                                     //                                 id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                                     //                                 [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Botao"
                                     //                                                                                       action:@"Register"
                                     //                                                                                        label:@"Iphone_Nativo_evento_108_botao_Register"
                                     //                                                                                        value:@1] build]];
                                     self.messageCode = LOGIN_SUCCESSFUL;
                                     if(self.overrideUser) {
                                         self.sfAppLogin.currentUser = registered;
                                         NSData *dataSave = [NSKeyedArchiver archivedDataWithRootObject:registered];
                                         [[NSUserDefaults standardUserDefaults] setObject:dataSave forKey:@"currentUser"];
                                     }
                                     
                                     [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginFacebookDidFinish:) withObject:self waitUntilDone:NO];
                                     [self.sfAppLogin.currentUser setUserFacebookToken:self.sfAppLogin.settings[@"facebookToken"] andSFAppSingleton:self.sfAppLogin];
                                 }
                                 else
                                 {
                                     if(registered.isResponseError) {
                                         self.sfAppLogin.settings[@"facebookToken"] = nil;
                                         self.sfAppLogin.settings[@"FB_id"] = nil;
                                     }
                                     NSLog(@"StayLog: ERROR on Register: %@",error.description);
                                     self.messageCode = REGISTRATION_ERROR;
                                     [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginFacebookDidFail:) withObject:self waitUntilDone:NO];
                                 }
                             }
                             else
                             {
                                 NSLog(@"StayLog: ERROR on Register: %@",error.description);
                                 self.messageCode = REGISTRATION_ERROR;
                                 [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginFacebookDidFail:) withObject:self waitUntilDone:NO];
                             }
                         }
                         else
                         {
                             NSLog(@"StayLog: ERROR on Register with error: %@",error.description);
                             self.messageCode = CONNECTION_ERROR;
                             [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginFacebookDidFail:) withObject:self waitUntilDone:NO];
                         }
                     }];
                });
            }
            else if(!appUser.isLogged)
            {
                self.messageCode = LOGIN_FAILED;
                [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginFacebookDidFail:) withObject:self waitUntilDone:NO];
            }
            else
            {
                [self.sfAppLogin.settings setObject:[[FBSDKAccessToken currentAccessToken] userID] forKey:@"FB_id"];
                [self.sfAppLogin.settings setObject:[[FBSDKAccessToken currentAccessToken] tokenString] forKey:@"facebookToken"];
                [self.sfAppLogin saveSettings];
                //TODO check if need this [registered setUserFacebookToken:selfDelegate.sfAppLogin.settings[@"facebookToken"]];
                
                self.messageCode = LOGIN_SUCCESSFUL;
                if(self.overrideUser) {
                    self.sfAppLogin.currentUser = appUser;
                    NSData *dataSave = [NSKeyedArchiver archivedDataWithRootObject:appUser];
                    [[NSUserDefaults standardUserDefaults] setObject:dataSave forKey:@"currentUser"];
                }
                
                //update user in background
                __weak typeof(self) selfDelegate = self;
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^(void){
                    [selfDelegate.sfAppLogin.currentUser setUserFacebookToken:selfDelegate.sfAppLogin.settings[@"facebookToken"] andSFAppSingleton:selfDelegate.sfAppLogin];
                });
                
                [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginFacebookDidFinish:) withObject:self waitUntilDone:NO];
            }
        }
        else
        {
            self.sfAppLogin.sfConfig = [SFConfig getAllConfigsFromWS:self.sfAppLogin.wsConfig];
            [self loginToStayfilmWithToken:p_token];
        }

    }
    @catch (NSException *exception)
    {
        NSLog(@"StayLog: EXCEPTION in LoginFacebookOperation loginToStayfilmWithToken with error: %@", exception.description);
        [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginFacebookDidFail:) withObject:self waitUntilDone:NO];
        [self cancel];
    }
}


#pragma mark -
#pragma mark - Main


-(void)main
{
    if(self.isCancelled)
        return;
    
    if(![self connected])
    {
        self.messageCode = NO_INTERNET_CONNECTION;
        [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginFacebookDidFail:) withObject:self waitUntilDone:NO];
        return;
    }
    
    @try
    {
        if(self.sfAppLogin == nil)
            self.sfAppLogin = [StayfilmApp sharedStayfilmAppSingleton];
        
        if([FBSDKAccessToken currentAccessToken])
        {
            // User logged in Facebook
            if([[[FBSDKAccessToken currentAccessToken] expirationDate] laterDate:[NSDate date]] == [NSDate date])  // check if token has expired
            {
                //Completly logout user
                FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
                [loginManager logOut];
                [FBSDKProfile setCurrentProfile:nil];
                [FBSDKAccessToken setCurrentAccessToken:nil];
                
                self.sfAppLogin.currentUser = nil;
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"currentUser"];
                [self.sfAppLogin.settings removeAllObjects];
            }
            
            if(self.sfAppLogin.currentUser == nil)
            {
                // User logged in facebook but haven't logged in the app yet
                if(self.sfAppLogin.settings[@"facebookToken"] == nil || [self.sfAppLogin.settings[@"facebookToken"] isEqualToString:@""])
                {
                    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
                    [login logInWithReadPermissions:self.facebookPermissions fromViewController:nil handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                        if(error)
                        {
                            NSLog(@"StayLog: FACEBOOK ERROR on logInWithReadPermissions with description: %@", error.description);
                            
                            self.messageCode = CONNECTION_ERROR;
                            [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginFacebookDidFail:) withObject:self waitUntilDone:NO];
                        }
                        else if (result.isCancelled) {
                            // Handle cancellations
                            NSLog(@"StayLog: FACEBOOK Cancelled on logInWithReadPermissions");
                            
                            self.messageCode = USER_CANCELLED;
                            [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginFacebookDidFail:) withObject:self waitUntilDone:NO];
                        }
                        else {
                            // Logged in... check if specific permissions missing?
                            NSLog(@"StayLog: FACEBOOK Logged in SUCCESSFULY in logInWithReadPermissions");
                            
                            [self loginToStayfilmWithToken:result.token.tokenString];
                        }
                    }];
                }
                else // already logged in
                {
                    if([self.facebookPermissions containsObject:@"publish_actions"])
                    {
                        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
                        [login logInWithPublishPermissions:self.facebookPermissions fromViewController:nil handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                            if (error) {
                                NSLog(@"StayLog: FACEBOOK ERROR on logInWithReadPermissions with description: %@", error.description);
                                
                                self.messageCode = CONNECTION_ERROR;
                                [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginFacebookDidFail:) withObject:self waitUntilDone:NO];
                            } else if (result.isCancelled) {
                                // Handle cancellations
                                NSLog(@"StayLog: FACEBOOK Cancelled on logInWithReadPermissions");
                                
                                self.messageCode = USER_CANCELLED;
                                [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginFacebookDidFail:) withObject:self waitUntilDone:NO];
                            } else {
                                // all permissions OK
                                //                                    if ([result.grantedPermissions containsObject:@"user_birthday"]) {
                                //                                        // Do work
                                //
                                //                                        NSLog(@"Permission  2: %@",result.grantedPermissions);
                                //                                    }
                                [self loginToStayfilmWithToken:result.token.tokenString];
                            }
                        }];
                    }
                    else
                    {
                        [self loginToStayfilmWithToken:self.sfAppLogin.settings[@"facebookToken"]];
                        
                        //fast login.. but without token verification
                        
                        if([[FBSDKAccessToken currentAccessToken] userID] != nil && [[[FBSDKAccessToken currentAccessToken] userID] isEqualToString:self.sfAppLogin.settings[@"FB_id"]])
                        {
                            self.messageCode = LOGIN_SUCCESSFUL;
                            [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginFacebookDidFinish:) withObject:self waitUntilDone:NO];
                            
                            return;
                        }
                    }
                }
            }
            else // User not logged in Stayfilm
            {
                FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
                
                if([self.facebookPermissions containsObject:@"publish_actions"])
                {
                    [login logInWithPublishPermissions:self.facebookPermissions fromViewController:nil handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                        if (error) {
                            NSLog(@"StayLog: FACEBOOK ERROR on logInWithReadPermissions with description: %@", error.description);
                            
                            self.messageCode = CONNECTION_ERROR;
                            [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginFacebookDidFail:) withObject:self waitUntilDone:NO];
                        } else if (result.isCancelled) {
                            // Handle cancellations
                            NSLog(@"StayLog: FACEBOOK Cancelled on logInWithReadPermissions");
                            
                            self.messageCode = USER_CANCELLED;
                            [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginFacebookDidFail:) withObject:self waitUntilDone:NO];
                        } else {
                            // all permissions OK
                            //                                    if ([result.grantedPermissions containsObject:@"user_birthday"]) {
                            //                                        // Do work
                            //
                            //                                        NSLog(@"Permission  2: %@",result.grantedPermissions);
                            //                                    }
                            [self loginToStayfilmWithToken:result.token.tokenString];
                        }
                    }];
                }
                else
                {
                    [login logInWithReadPermissions:self.facebookPermissions fromViewController:nil handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                        if(error)
                        {
                            NSLog(@"StayLog: FACEBOOK ERROR on logInWithReadPermissions with description: %@", error.description);
                            
                            self.messageCode = CONNECTION_ERROR;
                            [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginFacebookDidFail:) withObject:self waitUntilDone:NO];
                        }
                        else if (result.isCancelled) {
                            // Handle cancellations
                            NSLog(@"StayLog: FACEBOOK Cancelled on logInWithReadPermissions");
                            
                            self.messageCode = USER_CANCELLED;
                            [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginFacebookDidFail:) withObject:self waitUntilDone:NO];
                        }
                        else {
                            // Logged in... check if specific permissions missing?
                            NSLog(@"StayLog: FACEBOOK Logged in SUCCESSFULY in logInWithReadPermissions");
                            
                            [self loginToStayfilmWithToken:result.token.tokenString];
                        }
                    }];
                }
            }
        }
        else // User not logged in Facebook
        {
            FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
            
            if([self.facebookPermissions containsObject:@"publish_actions"])
            {
                [login logInWithPublishPermissions:self.facebookPermissions fromViewController:nil handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                    if (error) {
                        NSLog(@"StayLog: FACEBOOK ERROR on logInWithReadPermissions with description: %@", error.description);
                        
                        self.messageCode = CONNECTION_ERROR;
                        [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginFacebookDidFail:) withObject:self waitUntilDone:NO];
                    } else if (result.isCancelled) {
                        // Handle cancellations
                        NSLog(@"StayLog: FACEBOOK Cancelled on logInWithReadPermissions");
                        
                        self.messageCode = USER_CANCELLED;
                        [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginFacebookDidFail:) withObject:self waitUntilDone:NO];
                    } else {
                        // all permissions OK
                        //                                    if ([result.grantedPermissions containsObject:@"user_birthday"]) {
                        //                                        // Do work
                        //
                        //                                        NSLog(@"Permission  2: %@",result.grantedPermissions);
                        //                                    }
                        [self loginToStayfilmWithToken:result.token.tokenString];
                    }
                }];
            }
            else
            {
                [login logInWithReadPermissions:self.facebookPermissions fromViewController:nil handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                    if(error)
                    {
                        NSLog(@"StayLog: FACEBOOK ERROR on logInWithReadPermissions with description: %@", error.description);
                        
                        self.messageCode = CONNECTION_ERROR;
                        [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginFacebookDidFail:) withObject:self waitUntilDone:NO];
                    }
                    else if (result.isCancelled) {
                        // Handle cancellations
                        NSLog(@"StayLog: FACEBOOK Cancelled on logInWithReadPermissions");
                        
                        self.messageCode = USER_CANCELLED;
                        [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginFacebookDidFail:) withObject:self waitUntilDone:NO];
                    }
                    else {
                        // Logged in... check if specific permissions missing?
                        NSLog(@"StayLog: FACEBOOK Logged in SUCCESSFULY in logInWithReadPermissions");
                        
                        [self loginToStayfilmWithToken:result.token.tokenString];
                    }
                }];
            }
        }
    } @catch (NSException *exception) {
        NSLog(@"StayLog: EXCEPTION in LoginFacebookOperation main with error: %@", exception.description);
        [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginFacebookDidFail:) withObject:self waitUntilDone:NO];
        [self cancel];
    }
}


@end
