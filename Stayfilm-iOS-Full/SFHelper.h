//
//  SFHelper.h
//  Stayfilm
//
//  Created by Victor Manuel Roldan on 5/16/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
//#import "GenericModel.h"
//#import "UserModel.h"
//#import "BabyPageUserDefaults.h"

@interface SFHelper : NSObject

/* SESSION */
+ (void) saveInSession:(NSObject*)object forKey:(NSString*)key;
+ (NSObject*) getSessionObject:(NSString*)key;
+ (void) removeFromSession:(NSString*)key;
/* SESSION */

/* BLURRED IMAGE*/
+ (UIImage *)blurredImageWithImage:(UIImage *)sourceImage;
/*HEX COLOR*/
+ (UIColor *)colorFromHexString:(NSString *)hexString;

@end
