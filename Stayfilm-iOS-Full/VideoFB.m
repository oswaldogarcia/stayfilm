//
//  VideoFB.m
//  Stayfilm-iOS-Full
//
//  Created by Henrique on 5/3/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import "VideoFB.h"

@implementation VideoFB

@synthesize idVideo, length, source, picture;

+ (id)customClassWithProperties:(NSDictionary *)properties {
    @try {
        if(properties != nil)
        {
            NSMutableDictionary *filteredObjs = [[NSMutableDictionary alloc] init];
            for (NSString* key in properties) {
                id value = [properties objectForKey:key];
                if ([key isEqualToString:@"id"]) {
                    [filteredObjs setValue:value forKey:@"idVideo"];
                }
//                else if ([key isEqualToString:@"title"]) {
//                    [filteredObjs setValue:value forKey:@"title"];
//                }
                else if ([key isEqualToString:@"length"]) {
                    [filteredObjs setValue:value forKey:@"length"];
                }
                else if ([key isEqualToString:@"source"]) {
                    [filteredObjs setValue:value forKey:@"source"];
                }
                else if ([key isEqualToString:@"picture"]) {
                    [filteredObjs setValue:value forKey:@"picture"];
                }
//                else if ([key isEqualToString:@"format"]) {
//                    NSMutableArray *aux = [[NSMutableArray alloc] init];
//                    for(NSDictionary *var in value) {
//                        [aux addObject:[VideoFB customClassWithProperties:var]];
//                    }
//                    NSArray * images = [aux copy];
//                    [filteredObjs setValue:images forKey:@"format"];
//                }
            }
            return [[self alloc] initWithProperties:filteredObjs];
        }
        else
        {
            return nil;
        }
        
    }
    @catch (NSException *exception) {
        return nil;
    }
    
}

- (id)initWithProperties:(NSDictionary *)properties {
    if (self = [self init]) {
        @try {
            [self setValuesForKeysWithDictionary:properties];
        }
        @catch (NSException *ex) {
            return nil;
        }
    }
    return self;
}

-(id)init {
    self = [super init];
    if (0 != self) {
        self.idVideo = nil;
        self.source = nil;
        self.picture = nil;
        self.length = @0;
    }
    return self;
}

@end
