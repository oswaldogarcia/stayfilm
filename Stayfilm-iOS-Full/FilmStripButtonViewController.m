//
//  FilmStripButtonViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Henrique on 6/22/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import "FilmStripButtonViewController.h"
#import "AlbumViewerViewController.h"
#import "MovieMakerStep1ViewController.h"
#import "Step1ConfirmationViewController.h"
#import "Step3StylesViewController.h"
#import "ProgressViewController.h"
#import "Popover_MoreOptionsViewController.h"
#import "StayfilmApp.h"
#import "Reachability.h"

@interface FilmStripButtonViewController () <StayfilmSingletonDelegate>

@property (nonatomic, weak) StayfilmApp *sfApp;


@property (nonatomic, assign) BOOL isStatusBarWhite;

@property (nonatomic, strong) NSTimer *connectionStatusTimer;

@end

@implementation FilmStripButtonViewController


-(UIStatusBarStyle)preferredStatusBarStyle
{
    if(self.isStatusBarWhite) {
        return UIStatusBarStyleLightContent;
    }
    else {
        return UIStatusBarStyleDefault;
    }
}

-(void)changeTopStatusBarColorToWhite:(BOOL)isWhite {

    self.isStatusBarWhite = isWhite;

    [self setNeedsStatusBarAppearanceUpdate];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.sfApp = [StayfilmApp sharedStayfilmAppSingletonWithDelegate:self];
    self.sfApp.filmStrip = self;
    
    self.isEditingMode = self.sfApp.isEditingMode;
    self.isInsideFilmStrip = self.sfApp.isEditingMode;
    self.isStatusBarWhite = NO;
    self.isAnimating = NO;
    
    [self disableButton];

}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self recheckConnectivity];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)sfSingletonCreated {
    // nothing
}

-(void)recheckConnectivity {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    
    if (networkStatus != NotReachable) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self connectivityChanged:YES];
        });
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self connectivityChanged:NO];
        });
    }
}

-(void)connectivityChanged:(BOOL)isConnected {
    self.lbl_connectionStatus.text = NSLocalizedString(@"NO_INTERNET_RECONNECTING", nil);
    if(!isConnected) {
        self.view_connectionStatus.alpha = 0.0;
        [self.view_connectionStatus setHidden:NO];
        [UIView animateWithDuration:0.4 animations:^{
            self.view_connectionStatus.alpha = 1.0;
        }];
        if(self.connectionStatusTimer == nil) {
            __weak typeof(self) selfDelegate = self;
            self.connectionStatusTimer = [NSTimer timerWithTimeInterval:1.0 repeats:YES block:^(NSTimer * _Nonnull timer) {
                Reachability *reachability = [Reachability reachabilityForInternetConnection];
                NetworkStatus networkStatus = [reachability currentReachabilityStatus];
                
                if (networkStatus != NotReachable) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [selfDelegate connectivityChanged:YES];
                    });
                    [timer invalidate];
                    selfDelegate.connectionStatusTimer = nil;
                }
            }];
        }
    } else if(!self.view_connectionStatus.isHidden){
        self.view_connectionStatus.alpha = 1.0;
        [self.view_connectionStatus setHidden:NO];
        [UIView animateWithDuration:0.4 animations:^{
            self.view_connectionStatus.alpha = 0.0;
        } completion:^(BOOL finished) {
            [self.view_connectionStatus setHidden:YES];
        }];
    }
}

-(void)connectivityViewUpdate {
    //[self.view bringSubviewToFront:self.view_connectionStatus];
    if([self.auxNavigation.viewControllers.lastObject isKindOfClass:[Step1ConfirmationViewController class]]) {
        Step1ConfirmationViewController *vc = self.auxNavigation.viewControllers.lastObject;
        [vc.view layoutIfNeeded];
        
        [UIView animateWithDuration:0.4
                         animations:^{
                             self.constraintConnectionStatusHeight.constant = 78;
                             [self.view layoutIfNeeded]; // Called on parent view
                         }];
    }
    else if ([self.auxNavigation.viewControllers.lastObject isKindOfClass:[MovieMakerStep1ViewController class]] && self.isEditingMode) {
        MovieMakerStep1ViewController *vc = self.auxNavigation.viewControllers.lastObject;
        [vc.view layoutIfNeeded];
        
        [UIView animateWithDuration:0.4
                         animations:^{
                             self.constraintConnectionStatusHeight.constant = 78;
                             [self.view layoutIfNeeded]; // Called on parent view
                         }];
    }
    else if ([self.auxNavigation.viewControllers.lastObject isKindOfClass:[AlbumViewerViewController class]] && self.isEditingMode) {
        AlbumViewerViewController *vc = self.auxNavigation.viewControllers.lastObject;
        [vc.view layoutIfNeeded];
        
        [UIView animateWithDuration:0.4
                         animations:^{
                             self.constraintConnectionStatusHeight.constant = 78;
                             [self.view layoutIfNeeded]; // Called on parent view
                         }];
    }
    else if ([self.auxNavigation.viewControllers.lastObject isKindOfClass:[Step3StylesViewController class]] && self.isEditingMode) {
        Step3StylesViewController *vc = self.auxNavigation.viewControllers.lastObject;
        [vc.view layoutIfNeeded];
        
        [UIView animateWithDuration:0.4
                         animations:^{
                             self.constraintConnectionStatusHeight.constant = 78;
                             [self.view layoutIfNeeded]; // Called on parent view
                         }];
    }
    else if ([self.auxNavigation.viewControllers.lastObject isKindOfClass:[ProgressViewController class]]) {
        ProgressViewController *vc = self.auxNavigation.viewControllers.lastObject;
        [vc.view layoutIfNeeded];
        
        [UIView animateWithDuration:0.4
                         animations:^{
                             self.constraintConnectionStatusHeight.constant = 78;
                             [self.view layoutIfNeeded]; // Called on parent view
                         }];
    }
    else if ([self.auxNavigation.viewControllers.lastObject isKindOfClass:[Popover_MoreOptionsViewController class]]) {
        [UIView animateWithDuration:0.4
                         animations:^{
                             self.constraintConnectionStatusHeight.constant = 78;
                             [self.view layoutIfNeeded]; // Called on parent view
                         }];
    } else {
        [UIView animateWithDuration:0.4
                         animations:^{
                             self.constraintConnectionStatusHeight.constant = 58;
                             [self.view layoutIfNeeded]; // Called on parent view
                         }];
    }
    
    [self recheckConnectivity];
}

-(void)resetSingletonDelegate {
    self.sfApp = [StayfilmApp sharedStayfilmAppSingletonWithDelegate:self];
}


-(void)enableButton {
    [self.but_filmStrip setEnabled:YES];
    [self.but_filmStrip setHidden:NO];
    [self.view bringSubviewToFront:self.but_filmStrip];
}

-(void)disableButton {
    [self.but_filmStrip setEnabled:NO];
    [self.but_filmStrip setHidden:YES];
}

-(void)setEditingMode:(BOOL)isEditing {
    self.isEditingMode = isEditing;
    
    //    if(isEditing) {
    //        [self.but_NextStep setTitle:NSLocalizedString(@"EDIT_FILM", nil) forState:UIControlStateSelected];
    //        [self.but_NextStep setTitle:NSLocalizedString(@"EDIT_FILM", nil) forState:UIControlStateNormal];
    //        [self.but_NextStep setTitle:NSLocalizedString(@"EDIT_FILM", nil) forState:UIControlStateFocused];
    //        [self.but_NextStep setTitle:NSLocalizedString(@"EDIT_FILM", nil) forState:UIControlStateHighlighted];
    //    } else {
    //        [self.but_NextStep setTitle:NSLocalizedString(@"NEXT", nil) forState:UIControlStateSelected];
    //        [self.but_NextStep setTitle:NSLocalizedString(@"NEXT", nil) forState:UIControlStateNormal];
    //        [self.but_NextStep setTitle:NSLocalizedString(@"NEXT", nil) forState:UIControlStateFocused];
    //        [self.but_NextStep setTitle:NSLocalizedString(@"NEXT", nil) forState:UIControlStateHighlighted];
    //    }
}

-(void)showEditConfirmationButton:(BOOL)didEdit {
    
    if(didEdit) {
        [self.but_NextStep setTitle:NSLocalizedString(@"EDIT_FILM", nil) forState:UIControlStateSelected];
        [self.but_NextStep setTitle:NSLocalizedString(@"EDIT_FILM", nil) forState:UIControlStateNormal];
        [self.but_NextStep setTitle:NSLocalizedString(@"EDIT_FILM", nil) forState:UIControlStateFocused];
        [self.but_NextStep setTitle:NSLocalizedString(@"EDIT_FILM", nil) forState:UIControlStateHighlighted];
        [self.but_NextStep setHidden:NO];
        [self.view_NextStepArrow setHidden:NO];
    } else {
        [self.but_NextStep setTitle:NSLocalizedString(@"NEXT", nil) forState:UIControlStateSelected];
        [self.but_NextStep setTitle:NSLocalizedString(@"NEXT", nil) forState:UIControlStateNormal];
        [self.but_NextStep setTitle:NSLocalizedString(@"NEXT", nil) forState:UIControlStateFocused];
        [self.but_NextStep setTitle:NSLocalizedString(@"NEXT", nil) forState:UIControlStateHighlighted];
        [self.but_NextStep setHidden:YES];
        [self.view_NextStepArrow setHidden:YES];
    }
}

-(void)setUpdateButtonPositionHigher:(BOOL)raiseButton {
    // default position 20
    if(raiseButton) {
        [self.constraint_but_Next_height setConstant:70.f];
    } else {
        [self.constraint_but_Next_height setConstant:20.f];
    }
}

-(void)setViewInsideFilmStrip {
    self.isInsideFilmStrip = YES;
    [self.viewAll setHidden:YES];
    
    self.isStatusBarWhite = YES;
    [self setNeedsStatusBarAppearanceUpdate];
    
    self.txt_stripCounterBig.alpha = 0.0;
    self.txt_stripCounter.alpha = 1.0;
    self.img_stripCounter.alpha = 1.0;
    BOOL isMinimumVisible = !self.viewEditing.isHidden;
    if(isMinimumVisible) {
        self.viewEditing.alpha = 0.0;
        [self.viewEditing setHidden:NO];
    }
    
    [UIView animateWithDuration:0.4 animations:^{
        //self.constraintTrailing.constant = 35.0;
        self.txt_stripCounterBig.alpha = 1.0;
        self.txt_stripCounter.alpha = 0.0;
        self.img_stripCounter.alpha = 0.0;
        if(isMinimumVisible) {
            self.viewEditing.alpha = 1.0;
            self.txt_stripMinimum.alpha = 0.0;
            self.view_stripArrow.alpha = 0.0;
        }
        
    } completion:^(BOOL finished) {
        [self.txt_stripCounterBig setHidden:NO];
        [self.txt_stripCounter setHidden:YES];
        [self.img_stripCounter setHidden:YES];
        [self.but_filmStrip setEnabled:NO];
        if(isMinimumVisible) {
            [self.txt_stripMinimum setHidden:YES];
            [self.view_stripArrow setHidden:YES];
            self.txt_stripMinimum.alpha = 1.0;
            self.view_stripArrow.alpha = 1.0;
        }
    }];
}

-(void)setViewOutsideFilmStrip {
    self.isInsideFilmStrip = NO;
    self.isStatusBarWhite = NO;
    [self setNeedsStatusBarAppearanceUpdate];
    
    self.txt_stripCounterBig.alpha = 1.0;
    self.txt_stripCounter.alpha = 0.0;
    self.img_stripCounter.alpha = 0.0;
    BOOL isMinimumVisible = !self.viewEditing.isHidden;
    if(isMinimumVisible) {
        self.txt_stripMinimum.alpha = 0.0;
        self.view_stripArrow.alpha = 0.0;
        [self.view_stripArrow setHidden:NO];
        [self.txt_stripMinimum setHidden:NO];
    }
    
    [UIView animateWithDuration:0.4 animations:^{
        //self.constraintTrailing.constant = 0.0;
        self.txt_stripCounterBig.alpha = 0.0;
        self.txt_stripCounter.alpha = 1.0;
        self.img_stripCounter.alpha = 1.0;
        if(isMinimumVisible) {
            self.txt_stripMinimum.alpha = 1.0;
            self.view_stripArrow.alpha = 1.0;
            self.viewEditing.alpha = 0.0;
        }
        
    } completion:^(BOOL finished) {
        [self.txt_stripCounterBig setHidden:YES];
        [self.txt_stripCounter setHidden:NO];
        [self.img_stripCounter setHidden:NO];
        [self.but_filmStrip setEnabled:YES];
        if(isMinimumVisible) {
            [self.viewEditing setHidden:YES];
            self.viewEditing.alpha = 1.0;
        }
    }];
}

-(void)setHidden:(BOOL)hidden {
    [self.viewAll setHidden:hidden];
    
    if(hidden) {
        [self.but_NextStep setHidden:hidden];
        [self.view_NextStepArrow setHidden:hidden];
        [self.txt_stripCounter setHidden:NO];
        [self.txt_stripCounterBig setHidden:hidden];
        [self.but_filmStrip setHidden:hidden];
        [self.viewEditing setHidden:hidden];
    }
    //    else {
    //        if(self.isEditingMode) {
    //            [self.viewEditing setHidden:hidden];
    //        }
    //    }
    //[self updateFilmStrip];
}

-(void)setButtonNextHidden:(BOOL)hidden {
    [self.but_NextStep setHidden:hidden];
    [self.view_NextStepArrow setHidden:hidden];
}

-(void)adjustChildViewsToLowerPosition:(BOOL)isLowered {
    if(isLowered) {
        if([self.auxNavigation.viewControllers.lastObject isKindOfClass:[Step1ConfirmationViewController class]]) {
            Step1ConfirmationViewController *vc = self.auxNavigation.viewControllers.lastObject;
            [vc.view layoutIfNeeded];
            
            [UIView animateWithDuration:0.4
                             animations:^{
                                 vc.lowerViewConstraint.constant = 12;
                                 [vc.view layoutIfNeeded]; // Called on parent view
                             }];
        }
        else if ([self.auxNavigation.viewControllers.lastObject isKindOfClass:[MovieMakerStep1ViewController class]] && self.isEditingMode) {
            MovieMakerStep1ViewController *vc = self.auxNavigation.viewControllers.lastObject;
            [vc.view layoutIfNeeded];
            
            [UIView animateWithDuration:0.4
                             animations:^{
                                 vc.lowerViewConstraint.constant = 12;
                                 [vc.view layoutIfNeeded]; // Called on parent view
                             }];
        }
        else if ([self.auxNavigation.viewControllers.lastObject isKindOfClass:[AlbumViewerViewController class]] && self.isEditingMode) {
            AlbumViewerViewController *vc = self.auxNavigation.viewControllers.lastObject;
            [vc.view layoutIfNeeded];
            
            [UIView animateWithDuration:0.4
                             animations:^{
                                 vc.lowerViewConstraint.constant = 22;
                                 [vc.view layoutIfNeeded]; // Called on parent view
                             }];
        }
        else if ([self.auxNavigation.viewControllers.lastObject isKindOfClass:[Popover_MoreOptionsViewController class]]) {
            
        }
    } else {
        if([self.auxNavigation.viewControllers.lastObject isKindOfClass:[Step1ConfirmationViewController class]]) {
            Step1ConfirmationViewController *vc = self.auxNavigation.viewControllers.lastObject;
            [vc.view layoutIfNeeded];
            
            [UIView animateWithDuration:0.4
                             animations:^{
                                 vc.lowerViewConstraint.constant = 0;
                                 [vc.view layoutIfNeeded]; // Called on parent view
                             }];
        }
        else if ([self.auxNavigation.viewControllers.lastObject isKindOfClass:[MovieMakerStep1ViewController class]] && self.isEditingMode) {
            MovieMakerStep1ViewController *vc = self.auxNavigation.viewControllers.lastObject;
            [vc.view layoutIfNeeded];
            
            [UIView animateWithDuration:0.4
                             animations:^{
                                 vc.lowerViewConstraint.constant = 0;
                                 [vc.view layoutIfNeeded]; // Called on parent view
                             }];
        }
        else if ([self.auxNavigation.viewControllers.lastObject isKindOfClass:[AlbumViewerViewController class]] && self.isEditingMode) {
            AlbumViewerViewController *vc = self.auxNavigation.viewControllers.lastObject;
            [vc.view layoutIfNeeded];
            
            [UIView animateWithDuration:0.4
                             animations:^{
                                 vc.lowerViewConstraint.constant = 8;
                                 [vc.view layoutIfNeeded]; // Called on parent view
                             }];
        }
        else if ([self.auxNavigation.viewControllers.lastObject isKindOfClass:[Popover_MoreOptionsViewController class]]) {
            
        }
    }
}

BOOL previouslyOverMin = NO;
-(void)updateFilmStrip {
    //    if(!self.isInsideFilmStrip) {
    //        [self.view bringSubviewToFront:self.viewAll];
    //    }
    runOnMainQueueWithoutDeadlocking(^{
        unsigned long count = 0;
        if(self.sfApp.isEditingMode) {
            count = self.sfApp.editedMedias.count;
        } else {
            count = self.sfApp.finalMedias.count;
        }
        self.txt_stripCounter.text = [NSString stringWithFormat:@"%lu", count];
        self.txt_stripCounterBig.text = [NSString stringWithFormat:NSLocalizedString(@"X_ITEMS", nil), self.txt_stripCounter.text];
        if(count >= [self.sfApp.sfConfig.min_photos intValue] && count < [self.sfApp.sfConfig.max_photos intValue]) {
            [self.viewEditing setHidden:YES];
            [self adjustChildViewsToLowerPosition:NO];
            if(count == [self.sfApp.sfConfig.min_photos intValue] && !self.isEditingMode) {
                if(!self.isAnimating) {
                    self.isAnimating = YES;
                    self.txt_stripMinimum.text = [NSString stringWithFormat:@"%@     ",NSLocalizedString(@"MORE_BETTER", nil)];
                    if (!previouslyOverMin) {
                        [self.but_NextStep setHidden:YES];
                        [self.view_NextStepArrow setHidden:YES];
                    }
                    
                    [self.txt_stripMinimum setHidden:NO];
                    [self.view_stripArrow setHidden:NO];
                    if(!self.sfApp.isEditingMode){
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                            self.txt_stripMinimum.alpha = 1.0;
                            self.view_stripArrow.alpha = 1.0;
                            unsigned long newcount = -1;
                            if(self.sfApp.isEditingMode) {
                                newcount = self.sfApp.editedMedias.count;
                            } else {
                                newcount = self.sfApp.finalMedias.count;
                            }
                            if(newcount >= [self.sfApp.sfConfig.min_photos intValue] && newcount < [self.sfApp.sfConfig.max_photos intValue]) {
                                if(!self.isEditingMode && self.but_NextStep.isHidden && !previouslyOverMin && self.sfApp.currentSelectionCount >= [self.sfApp.sfConfig.min_photos intValue]) {
                                    self.but_NextStep.alpha = 0.0;
                                    self.view_NextStepArrow.alpha = 0.0;
                                    [self.but_NextStep setHidden:NO];
                                    [self.view_NextStepArrow setHidden:NO];
                                }
                                if(self.sfApp.currentSelectionCount >= [self.sfApp.sfConfig.min_photos intValue]) {
                                    [UIView animateWithDuration:0.2 animations:^{
                                        self.view_stripArrow.alpha = 0.0;
                                        self.txt_stripMinimum.alpha = 0.0;
                                        self.but_NextStep.alpha = 1.0;
                                        self.view_NextStepArrow.alpha = 1.0;
                                    } completion:^(BOOL finished) {
                                        [self.txt_stripMinimum setHidden:YES];
                                        [self.view_stripArrow setHidden:YES];
                                        self.txt_stripMinimum.alpha = 1.0;
                                        self.view_stripArrow.alpha = 1.0;
                                        self.txt_stripMinimum.text = [NSString stringWithFormat:@"%@  ",[NSString stringWithFormat:NSLocalizedString(@"X_AT_LEAST", nil),[self.sfApp.sfConfig.min_photos stringValue]]];
                                        self.isAnimating = NO;
                                    }];
                                } else {
                                    self.isAnimating = NO;
                                }
                            }
                        });
                    }
                }
                
            }  else {
                [self.txt_stripMinimum setHidden:YES];
                [self.view_stripArrow setHidden:YES];
            }
            previouslyOverMin = YES;
            [self.txt_stripCounter setHidden:NO];
            [self.img_stripCounter setHidden:NO];
            [self.but_filmStripConstraint setConstant:50.0];
            
            if(self.isEditingMode) {
                [self.but_NextStep setHidden:YES];
                [self.view_NextStepArrow setHidden:YES];
            } else if(count >= [self.sfApp.sfConfig.min_photos intValue] && self.sfApp.currentSelectionCount >= [self.sfApp.sfConfig.min_photos intValue]) {
                [self.but_NextStep setHidden:NO];
                [self.view_NextStepArrow setHidden:NO];
            }
        }
        else
        {
            previouslyOverMin = NO;
            self.isAnimating = NO;
            if(self.isEditingMode) {
                [self.txt_stripMinimum setHidden:YES];
                [self.view_stripArrow setHidden:YES];
                [self.viewEditing setHidden:NO];
                [self adjustChildViewsToLowerPosition:YES];
                
                if (count >= [self.sfApp.sfConfig.max_photos intValue]){
                    
                    self.txt_stripBigMinimum.text = [NSString stringWithFormat:NSLocalizedString(@"LIMIT_X_REACHED", nil),[self.sfApp.sfConfig.max_photos intValue]];
                }else{
                    self.txt_stripBigMinimum.text = [NSString stringWithFormat:NSLocalizedString(@"LIMIT_REACHED_X", nil),[self.sfApp.sfConfig.min_photos intValue]];
                }
                
            } else {
                if(self.isInsideFilmStrip) {
                    [self.txt_stripMinimum setHidden:YES];
                    [self.view_stripArrow setHidden:YES];
                    [self.viewEditing setHidden:NO];
                    [self adjustChildViewsToLowerPosition:YES];
                    [self.viewAll setHidden:NO];
                    
                    if (count >= [self.sfApp.sfConfig.max_photos intValue]){
                        self.txt_stripMinimum.text = [NSString stringWithFormat:@"%@     ",NSLocalizedString(@"LIMIT_REACHED", nil)];
                        
                        self.txt_stripBigMinimum.text = [NSString stringWithFormat:NSLocalizedString(@"LIMIT_X_REACHED", nil),[self.sfApp.sfConfig.max_photos intValue]];
                    }else{
                        self.txt_stripBigMinimum.text = [NSString stringWithFormat:NSLocalizedString(@"LIMIT_REACHED_X", nil),[self.sfApp.sfConfig.min_photos intValue]];
                    }
                    
                } else {
                    if (count >= [self.sfApp.sfConfig.max_photos intValue]){
                        self.txt_stripMinimum.text = [NSString stringWithFormat:@"%@     ",NSLocalizedString(@"LIMIT_REACHED", nil)];
                        
                    }else{
                        self.txt_stripMinimum.text = [NSString stringWithFormat:@"%@  ",[NSString stringWithFormat:NSLocalizedString(@"X_AT_LEAST", nil),[self.sfApp.sfConfig.min_photos stringValue]]];
                    }
                    [self.viewEditing setHidden:YES];
                    [self adjustChildViewsToLowerPosition:NO];
                    [self.txt_stripMinimum setHidden:NO];
                    [self.view_stripArrow setHidden:NO];
                    
                    //[self.viewAll setHidden:YES];
                }
            }
            
            [self.but_NextStep setHidden:YES];
            [self.view_NextStepArrow setHidden:YES];
            
            if ((count == [self.sfApp.sfConfig.max_photos intValue]) && (!self.sfApp.isEditingMode) && self.sfApp.currentSelectionCount >= [self.sfApp.sfConfig.min_photos intValue]){
                
                [self.but_NextStep setHidden:NO];
                [self.view_NextStepArrow setHidden:NO];
            }
            
            [self.txt_stripCounter setHidden:NO];
            [self.img_stripCounter setHidden:NO];
            [self.but_filmStripConstraint setConstant:165.0];
        }
    });
}


-(void)updateFilmStripWithCounter:(NSString *)counter andCondition:(BOOL)condition {
    runOnMainQueueWithoutDeadlocking(^{
        self.txt_stripCounter.text = counter;
        self.txt_stripCounterBig.text = [NSString stringWithFormat:NSLocalizedString(@"X_ITEMS", nil), counter];;
        if(condition) {
            int count = [counter intValue];
            if(self.isEditingMode) {
                [self.but_NextStep setHidden:YES];
                [self.view_NextStepArrow setHidden:YES];
            } else if(count != [self.sfApp.sfConfig.min_photos intValue]) {
                [self.but_NextStep setHidden:NO];
                [self.view_NextStepArrow setHidden:NO];
            }
            [self.viewEditing setHidden:YES];
            [self adjustChildViewsToLowerPosition:NO];
            
            if(count == [self.sfApp.sfConfig.min_photos intValue]) {
                if(!self.isAnimating) {
                    self.isAnimating = YES;
                    
                    self.txt_stripMinimum.text = [NSString stringWithFormat:@"%@     ",NSLocalizedString(@"MORE_BETTER", nil)];
                    
                    if (!previouslyOverMin) {
                        [self.but_NextStep setHidden:YES];
                        [self.view_NextStepArrow setHidden:YES];
                    }
                    [self.view_NextStepArrow setHidden:YES];
                    
                    if(!self.isEditingMode) {
                        self.view_stripArrow.alpha = 1.0;
                        [self.view_stripArrow setHidden:NO];
                        self.txt_stripMinimum.alpha = 1.0;
                        [self.txt_stripMinimum setHidden:NO];
                    }
                    
                    if(!self.isEditingMode && self.but_NextStep.isHidden) {
                        self.but_NextStep.alpha = 1.0;
                        self.view_NextStepArrow.alpha = 1.0;
                        [self.but_NextStep setHidden:NO];
                        [self.view_NextStepArrow setHidden:NO];
                    }
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        if(self.manualCounter >= [self.sfApp.sfConfig.min_photos intValue] &&
                           self.manualCounter < [self.sfApp.sfConfig.max_photos intValue] &&
                           ([self.auxNavigation.viewControllers.lastObject isKindOfClass:[AlbumViewerViewController class]] ||
                            [self.auxNavigation.viewControllers.lastObject isKindOfClass:[MovieMakerStep1ViewController class]] ||
                            [self.auxNavigation.viewControllers.lastObject isKindOfClass:[Step1ConfirmationViewController class]] )) {//||
                               
                               [UIView animateWithDuration:0.2 animations:^{
                                   self.view_stripArrow.alpha = 0.0;
                                   self.txt_stripMinimum.alpha = 0.0;
//                                   self.but_NextStep.alpha = 1.0;
//                                   self.view_NextStepArrow.alpha = 1.0;
                               } completion:^(BOOL finished) {
                                   [self.txt_stripMinimum setHidden:YES];
                                   [self.view_stripArrow setHidden:YES];
                                   self.view_stripArrow.alpha = 1.0;
                                   self.txt_stripMinimum.alpha = 1.0;
                                   self.txt_stripMinimum.text = [NSString stringWithFormat:@"%@  ",[NSString stringWithFormat:NSLocalizedString(@"X_AT_LEAST", nil),[self.sfApp.sfConfig.min_photos stringValue]]];
                                   self.isAnimating = NO;
                               }];
                           }
                    });
                    
                }
            }else if (count >= [self.sfApp.sfConfig.max_photos intValue]){
                
                self.txt_stripMinimum.text = [NSString stringWithFormat:@"%@     ",NSLocalizedString(@"LIMIT_REACHED", nil)];
                
                if (count > [self.sfApp.sfConfig.max_photos intValue]){
                    
                    
                    [self.but_NextStep setHidden:YES];
                    [self.view_NextStepArrow setHidden:YES];
                    
                }
                self.isAnimating = NO;
                if(self.isEditingMode) {
                    self.view_stripArrow.alpha = 0.0;
                    [self.view_stripArrow setHidden:YES];
                    self.txt_stripMinimum.alpha = 0.0;
                    [self.txt_stripMinimum setHidden:YES];
                    
                    
                }else{
                    self.view_stripArrow.alpha = 1.0;
                    [self.view_stripArrow setHidden:NO];
                    self.txt_stripMinimum.alpha = 1.0;
                    [self.txt_stripMinimum setHidden:NO];
                }
                
                
            }else {
                [self.txt_stripMinimum setHidden:YES];
                [self.view_stripArrow setHidden:YES];
            }
            previouslyOverMin = YES;
            [self.txt_stripCounter setHidden:NO];
            [self.img_stripCounter setHidden:NO];
            [self.but_filmStripConstraint setConstant:50.0];
        }
        else
        {
            previouslyOverMin = NO;
            self.isAnimating = NO;
            if(self.isEditingMode) {
                [self.viewEditing setHidden:NO];
                [self adjustChildViewsToLowerPosition:YES];
                [self.txt_stripMinimum setHidden:YES];
                [self.view_stripArrow setHidden:YES];
                
                self.txt_stripBigMinimum.text = [NSString stringWithFormat:NSLocalizedString(@"LIMIT_REACHED_X", nil),[self.sfApp.sfConfig.min_photos intValue]];
                
                
            } else {
                [self.viewEditing setHidden:YES];
                [self adjustChildViewsToLowerPosition:NO];
                [self.txt_stripMinimum setHidden:NO];
                [self.view_stripArrow setHidden:NO];
            }
            
            self.txt_stripMinimum.text = [NSString stringWithFormat:@"%@  ",[NSString stringWithFormat:NSLocalizedString(@"X_AT_LEAST", nil),[self.sfApp.sfConfig.min_photos stringValue]]];
            [self.txt_stripCounter setHidden:NO];
            [self.img_stripCounter setHidden:NO];
            [self.but_filmStripConstraint setConstant:165.0];
            [self.but_NextStep setHidden:YES];
            [self.view_NextStepArrow setHidden:YES];
        }
    });
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


@end
