//
//  AlbumSocialCollectionViewCell.m
//  Stayfilm-iOS-Full
//
//  Created by Henrique Ormonde on 25/09/17.
//  Copyright © 2017 Stayfilm. All rights reserved.
//

@import KALoader;
#import "AlbumSocialCollectionViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation AlbumSocialCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)initCell{
    
    self.txt_Title.text = self.album.name;
    self.txt_Total.text =[NSString stringWithFormat:@"%@",[self.album.count stringValue]];
    self.imageBig.image = nil;
    self.imageLeft.image = nil;
    self.imageRight.image = nil;
    self.imageSOLO.image = nil;
    [self.imageSOLO setHidden:YES];
    [self.loaderSpinner startAnimating];
    [self.loaderSpinner setHidden:NO];
    
    [self setImagesOfCell];
}

-(void)setImagesOfCell{
    
    NSString *coverURL = self.album.imageBIG;
    
    if ([self.album.count intValue] >= 3){
        if(coverURL != nil){
            
            //NSLog(@"URL imageBIG: %@",coverURL);
            [self.imageBig sd_setImageWithURL:[NSURL URLWithString:coverURL] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                if(!error){
                    
                    [self.imageBig setImage:image];
                    [self.loaderSpinner stopAnimating];
                    [self.loaderSpinner setHidden:YES];
                    
                    [self.imageRight showLoader];
                    [self.imageLeft showLoader];
                }
            }];
        }
        
        NSString *leftURL = self.album.imageLEFT;
        if(leftURL != nil){
            
            // NSLog(@"URL leftURL: %@",leftURL);
            
            [self.imageLeft  sd_setImageWithURL:[NSURL URLWithString:leftURL] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                if(!error){
                    
                    if([self.imageLeft isLoaderShowing]){
                        [self.imageLeft hideLoader];
                    }
                    [self.imageLeft setImage:image];
                }
            }];
        }
        
        NSString *rightURL = self.album.imageRIGHT;
        if(rightURL != nil){
            
            //NSLog(@"URL rightURL: %@",rightURL);
            
            [self.imageRight sd_setImageWithURL:[NSURL URLWithString:rightURL] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                if(!error){
                    if([self.imageRight isLoaderShowing]){
                        [self.imageRight hideLoader];
                    }
                    [self.imageRight setImage:image];
                }
            }];
        }
        
    }else{
        
        if(coverURL != nil){
            
            //NSLog(@"URL imageBIG: %@",coverURL);
            [self.imageSOLO setHidden:NO];
            [self.imageSOLO  sd_setImageWithURL:[NSURL URLWithString:coverURL] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                if(!error){
                    [self.imageSOLO setImage:image];
                    [self.loaderSpinner stopAnimating];
                    [self.loaderSpinner setHidden:YES];
                }
            }];
        }
        
        
    }
    
}

@end
