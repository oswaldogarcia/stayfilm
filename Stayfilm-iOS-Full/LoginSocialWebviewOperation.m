//
//  LoginSocialWebviewOperation.m
//  Stayfilm-iOS-Full
//
//  Created by Henrique Ormonde on 18/09/17.
//  Copyright © 2017 Stayfilm. All rights reserved.
//

#import "LoginSocialWebviewOperation.h"
#import "LoginWebView.h"

@interface LoginSocialWebviewOperation ()

@property (nonatomic, strong) NSArray *facebookPermissions;

@end

@implementation LoginSocialWebviewOperation

@synthesize delegate, loginResultToken, messageCode, loginSocialNetwork, sfAppLogin;


#pragma mark -
#pragma mark - Callable Methods

-(id)initWithDelegate:(id<LoginSocialWebviewOperationDelegate>)p_delegate andSFApp:(StayfilmApp *)p_sfApp forSocialNetwork:(NSString *)p_socialNetwork {
    if( self = [super init] ) {
        self.delegate = p_delegate;
        self.sfAppLogin = p_sfApp;
        self.loginResultToken = nil;
        self.loginSocialNetwork = p_socialNetwork;
        self.messageCode = -1;
    }
    return self;
}

#pragma mark -
#pragma mark - Main

-(void)main
{
    if(self.cancelled)
        return;
    
    @try {
        LoginWebView *loginWebView = [[LoginWebView alloc] initWithNibName:@"LoginWebView" bundle:[NSBundle mainBundle]];
        loginWebView.loginURL = [[NSURL alloc] initWithString:[[NSString alloc] initWithString:([NSString stringWithFormat:[self.sfAppLogin.wsConfig getConfigPathWithKey:@"loginSocial"], self.loginSocialNetwork, sfAppLogin.currentUser.idSession])]];
        [((UIViewController *)((LoginToSocialNetworkOperation *)delegate).delegate).navigationController pushViewController:loginWebView animated:YES];
    } @catch (NSException *exception) {
        NSLog(@"StayLog: EXCEPTION in LoginSocialWebViewOperation with error: %@", exception.description);
    }
}

@end
