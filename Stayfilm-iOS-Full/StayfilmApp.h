//
//  StayfilmApp.h
//  Stayfilm for Messenger
//
//  Created by Henrique Ormonde on 11/05/15.
//  Copyright (c) 2015 Stayfilm. All rights reserved.
//

#ifndef Stayfilm_Full_StayfilmApp_h
#define Stayfilm_Full_StayfilmApp_h
/* IAP Plans identifiers */
#define IAP_MONTHLY_SUBSCRIPTION @"com.stayfilm.fun.MonthlySubscription"

#import <Foundation/Foundation.h>
#import "WSConfig.h"
#import "SFConfig.h"
#import "AlbumSF.h"
#import "User.h"
#import "StoryState.h"
#import <UIKit/UIKit.h>
#import "FilmStripButtonViewController.h"
#import <StoreKit/StoreKit.h>


@class AlbumSF;
@class User;

@protocol StayfilmSingletonDelegate;

@interface StayfilmApp : NSObject

@property (nonatomic, assign) id <StayfilmSingletonDelegate> delegate;
@property (nonatomic, strong) NSOperationQueue *operationsQueue;

@property (nonatomic, strong) FilmStripButtonViewController *filmStrip;

@property (nonatomic, strong) WSConfig *wsConfig;
@property (nonatomic, strong) SFConfig *sfConfig;
@property (nonatomic, strong) NSMutableDictionary *settings;
@property (nonatomic, strong) NSCache *imagesCache;
@property (nonatomic, strong) User *currentUser;
@property (nonatomic, assign) StoryState *storyState;
@property (nonatomic, strong) NSString *analyticsTag;

@property (nonatomic, assign) BOOL isDebugMode;

//Gallery Stories Helper
@property (nonatomic, strong) NSMutableArray *playerObserverArray;
@property (nonatomic, strong) NSMutableArray *playerArray;

//Selection Movie Maker
@property (nonatomic, assign) BOOL isRemake;
@property (nonatomic, assign) int currentSelectionCount;
@property (nonatomic, strong) NSMutableArray *selectedMedias;
@property (nonatomic, strong) NSMutableArray *finalMedias;
@property (nonatomic, strong) AlbumSF *uploadedAlbum;
@property (nonatomic, strong) Genres *selectedGenre;
@property (nonatomic, strong) Template *selectedTemplate;
@property (nonatomic, strong) NSString *selectedTitle;
//@property (nonatomic, assign) StayfilmTypesFilmOrientation selectedOrientation;
//Editing Movie Maker
@property (nonatomic, assign) BOOL isEditingMode;
@property (nonatomic, strong) Genres *editedGenre;
@property (nonatomic, strong) Template *editedTemplate;
@property (nonatomic, strong) NSString *editedTitle;
@property (nonatomic, strong) NSMutableArray *editedMedias;
//@property (nonatomic, assign) StayfilmTypesFilmOrientation editedOrientation;


@property (nonatomic, strong) NSMutableArray *deletedFilms;

//Automatic Film
@property (nonatomic, assign) BOOL isAutomaticMode;
@property (nonatomic, strong) NSMutableArray *selectedMediasToAutoFilm;
@property (nonatomic, strong) NSString *selectedTitleToAutoFilm;
@property (nonatomic, assign) BOOL autoFilmCreated;
@property (nonatomic, strong) NSMutableArray *mediasFromAutoFilmCreated;
@property (nonatomic, assign) BOOL isFromExplorer;
//skip style
@property (nonatomic, assign) BOOL canSkipStyles;

//In-App purchase
@property (nonatomic) SKProduct * product;

+ (StayfilmApp *) sharedStayfilmAppSingletonWithDelegate:(id<StayfilmSingletonDelegate>)theDelegate;
+ (StayfilmApp *) sharedStayfilmAppSingleton;
- (id)init;
- (id)reloadSettings;
- (void)saveSettings;
- (int)loginUserWithFacebook;
- (void)loginToFacebookWithPermissions:(NSArray *)p_permissions;
- (void)cleanRemakeState;
- (void)cleanSettings;
- (BOOL)cleanFilmFolder;
- (BOOL)deleteMovieFromDevice:(NSString*)idMovie;
- (BOOL)isLoggedIn;
- (void)setEditingMode:(BOOL)isEditing;
+ (BOOL)isArray:(NSArray*)array1 equalToArray:(NSArray *)array2;
+ (NSString *)MD5String:(NSString*)input;
- (BOOL)logout;

- (void)clearGalleryPlayerObservers;

- (void)setOperationsQueueMaxCuncurrency:(int)p_max;
- (void)setOperationsQueueQoS:(NSQualityOfService)p_quality;

void runOnMainQueueWithoutDeadlocking(void (^block)(void));

@end

@protocol StayfilmSingletonDelegate <NSObject>
- (void)sfSingletonCreated;
- (void)connectivityChanged:(BOOL)isConnected;
@end

#endif
