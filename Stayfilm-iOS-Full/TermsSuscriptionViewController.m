//
//  TermsSuscriptionViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 4/10/19.
//  Copyright © 2019 Stayfilm. All rights reserved.
//

#import "TermsSuscriptionViewController.h"
#import "SFDefinitions.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "TermsSuscriptionViewController.h"
#import "ShellWebService.h"

@interface TermsSuscriptionViewController ()

@end

@implementation TermsSuscriptionViewController
BOOL transactionProgress;

- (void)viewWillAppear:(BOOL)animated{
    
    if (self.sfApp == nil){
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    }
        
    [self setViewInfo];
    
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
}

- (void)setViewInfo{
    
    self.termsTitleLabel.text = NSLocalizedString(@"PURCHASE_TERMS_TITLE", nil);
    
//     self.termsDescriptionLabel.text = NSLocalizedString(@"PURCHASE_TERMS_TITLE", nil);
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numberFormatter setLocale:self.sfApp.product.priceLocale];
    
    NSString *buyButtonTitle = [NSString stringWithFormat:NSLocalizedString(@"PRICE_MONTH", nil),[numberFormatter stringFromNumber:self.sfApp.product.price]];
    
    [self.buyButton setTitle:buyButtonTitle forState:UIControlStateNormal];
    
    NSString *langcode = [[NSLocale currentLocale] objectForKey:NSLocaleLanguageCode];
    
    if([langcode isEqualToString:@"pt"]){
        self.termsImage.image = [UIImage imageNamed:@"beyondPortuguese"];
    }else if([langcode isEqualToString:@"es"]){
        self.termsImage.image = [UIImage imageNamed:@"beyondSpanish"];
    }else{
        self.termsImage.image = [UIImage imageNamed:@"beyondEnglish"];
    }
    
    self.productShortDescription.text = NSLocalizedString(@"SUBSCRIBE_TO_UNLOCK", nil);
    self.offertLabel.text = NSLocalizedString(@"SPECIAL_OFFER", nil);
    self.productDescription.text = NSLocalizedString(@"PRODUCT_DESCRIPTION", nil);
    
    [self.restorePurchaseButton setTitle:NSLocalizedString(@"RESTORE_PURCHASE", nil) forState:UIControlStateNormal];
   
    
    
}

- (IBAction)backAction:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
- (IBAction)buyButtonPressed:(id)sender {
    
    [self.delegate startPurchase];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)restorePurchaseAction:(id)sender {
    
    
    [self.delegate restorePurchase];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
