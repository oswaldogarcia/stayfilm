//
//  StyleTableViewCell.h
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 21/05/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SFConfig.h"
#import "Utilities.h"
#import "TemplateCollectionViewCell.h"


@protocol StyleDelegate<NSObject>

- (void)didSelectTemplate:(Genres*)genre Template:(Template*)selectedTemplate;
- (void)didDeselectTemplate;
- (void)noAvailableOrientation;
- (void)setFilmOrintation:(StayfilmTypesFilmOrientation)orientation;
- (void) unlockStyle:(Template*)selectedTemplate;

@end

@interface StyleTableViewCell : UITableViewCell <TemplateDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *templatesCollectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *templateHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageTopConstaint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageheight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageTrailingConstaint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageLeadingConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *templateArrowImage;

@property (nonatomic, weak) IBOutlet UIImageView *genreImageView;
@property (weak, nonatomic) IBOutlet UIImageView *selection;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingSpinner;

@property (weak, nonatomic) IBOutlet UIButton *horizontalButton;
@property (weak, nonatomic) IBOutlet UIButton *verticalButton;
@property (weak, nonatomic) IBOutlet UIImageView *premiumTagImage;


//@property (nonatomic, strong) NSURLSessionDataTask *task;
@property (nonatomic, strong) Genres *selectedGenre;
@property (nonatomic) BOOL isSelectedByUser;
@property (nonatomic) int currentTemplateSelection;
@property (nonatomic) int currentRow;
@property (nonatomic) BOOL ipadTime;
@property (nonatomic) BOOL isEditing;
@property (nonatomic) BOOL needScroll;
@property (nonatomic) BOOL changeEditing;
@property (nonatomic) BOOL isVertical;
@property (nonatomic) BOOL isVerticalAvailable;
@property (nonatomic) BOOL isPremium;
@property (nonatomic) BOOL isBlocked;
- (void)initCell;
- (void)scrollToCurrentTemplate;
- (IBAction)changeToVertical:(id)sender;
- (IBAction)changeToHorizontal:(id)sender;
@property (nonatomic, assign) id<StyleDelegate> delegate;

@end
