//
//  LoginWebView.m
//  Stayfilm-iOS-Full
//
//  Created by Henrique Ormonde on 18/09/17.
//  Copyright © 2017 Stayfilm. All rights reserved.
//

#import "LoginWebView.h"

@interface LoginWebView ()

@property (nonatomic, strong) WKWebView* webView;

@end

@implementation LoginWebView

@synthesize webView, loginURL, socialNetwork, delegate;

-(void)loadView {
    [super loadView];
    
    WKWebViewConfiguration *webConfig = [[WKWebViewConfiguration alloc] init];
    [webConfig.userContentController addScriptMessageHandler:self name:@"interOp"];
    self.webView = [[WKWebView alloc] initWithFrame:self.view.frame configuration:webConfig];
    self.webView.autoresizingMask =  UIViewAutoresizingFlexibleTopMargin| UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.webView.UIDelegate = self;
    self.webView.navigationDelegate = self;
    [self.view addSubview:self.webView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:self.loginURL]; [self.webView loadRequest:request];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Delegate Methods

- (void)webView:(WKWebView *)webView didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential *))completionHandler {
    return completionHandler(NSURLSessionAuthChallengePerformDefaultHandling, nil);
}


- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message {
//    NSDictionary *body = (NSDictionary*)message.body;
//    NSLog(@"RECEIVED JAVASCRIPT: %@", body);
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
//    NSLog(@"StayLog: finished: %@",webView.URL.absoluteString);
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler {
    @try
    {
        if([webView.URL.absoluteString containsString:@"http://www.stayfilm.com"] || [webView.URL.absoluteString containsString:@"https://www.stayfilm.com"])
        {
            if ([navigationResponse.response isKindOfClass:[NSHTTPURLResponse class]]) {
                
                NSHTTPURLResponse * response = (NSHTTPURLResponse *)navigationResponse.response;
                if (response.statusCode == 200 || response.statusCode == 201)
                {
                    @try {
                        if([self.socialNetwork isEqualToString:@"instagram"] || [self.socialNetwork isEqualToString:@"gplus"] || [self.socialNetwork isEqualToString:@"twitter"])
                        {
                            if([navigationResponse.response.URL.query containsString:@"code="]) {
                                NSInteger startIndex = [navigationResponse.response.URL.query rangeOfString:@"code="].location + 5;
                                NSString *frob = [navigationResponse.response.URL.query substringFromIndex:startIndex];
                                
                                if(frob != nil)
                                {
                                    NSArray *param = @[self.socialNetwork, frob];
                                    [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginSocialWebviewDidSucceed:) withObject:param waitUntilDone:NO];
                                }
                                else
                                {
                                    NSArray *param = @[self.socialNetwork, frob];
                                    [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginSocialWebviewDidFail:) withObject:param waitUntilDone:NO];
                                }
                            }
                            else if([navigationResponse.response.URL.absoluteString containsString:@"oauth/authorize?client_id="]) {
                                return;
                            }
                            else if([navigationResponse.response.URL.absoluteString containsString:@"twitter?oauth_token="] && [navigationResponse.response.URL.absoluteString containsString:@"&oauth_verifier="]) {
                                NSInteger startIndex = [navigationResponse.response.URL.query rangeOfString:@"oauth_token="].location + 12;
                                NSInteger endIndex = [navigationResponse.response.URL.query rangeOfString:@"&oauth_verifier="].location;
                                NSString *token = [navigationResponse.response.URL.query substringWithRange:NSMakeRange(startIndex, endIndex-startIndex)];
                                startIndex = [navigationResponse.response.URL.query rangeOfString:@"&oauth_verifier="].location + 16;
                                NSString *verifier = [navigationResponse.response.URL.query substringFromIndex:startIndex];
                                
                                if(token != nil && verifier != nil) {
                                    NSArray *param = @[self.socialNetwork, token, verifier];
                                    [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginSocialWebviewDidSucceed:) withObject:param waitUntilDone:NO];
                                }
                                else
                                {
                                    NSArray *param = @[self.socialNetwork, token, verifier];
                                    [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginSocialWebviewDidFail:) withObject:param waitUntilDone:NO];
                                    [self.navigationController popViewControllerAnimated:YES];
                                }
                            }
                            else {
                                [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginSocialWebviewDidFail:) withObject:nil waitUntilDone:NO];
                                [self.navigationController popViewControllerAnimated:YES];
                            }
                            [self.navigationController popViewControllerAnimated:YES];
                        }
                        if([self.socialNetwork isEqualToString:@"flickr"]) // depricated? maybe
                        {
                            if([navigationResponse.response.URL.query containsString:@"frob="])
                            {
                                NSInteger startIndex = [navigationResponse.response.URL.query indexOfAccessibilityElement:@"frob="] + 5;
                                NSString *frob = [navigationResponse.response.URL.query substringFromIndex:startIndex];
                                
                                if(frob != nil)
                                {
                                    NSArray *param = @[self.socialNetwork, frob];
                                    [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginSocialWebviewDidSucceed:) withObject:param waitUntilDone:NO];
                                }
                                else
                                {
                                    NSArray *param = @[self.socialNetwork, frob];
                                    [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginSocialWebviewDidFail:) withObject:param waitUntilDone:NO];
                                }
                            }
                            else
                            {
                                [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginSocialWebviewDidFail:) withObject:nil waitUntilDone:NO];
                            }
                            [self.navigationController popViewControllerAnimated:YES];
                        }
                    } @catch (NSException *exception) {
                        NSLog(@"StayLog: EXCEPTION in LoginWebView evaluateJavaScript with error: %@", exception.description);
                        
                        NSArray *param = @[self.socialNetwork, exception.description];
                        [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginSocialWebviewDidFail:) withObject:param waitUntilDone:NO];
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                }
                else
                {
                    NSLog(@"StayLog: ERROR Code in navigation response LoginWebView with code: %ld", (long)response.statusCode);
                    
                    NSArray *param = @[self.socialNetwork, [NSString stringWithFormat:@"%ld",(long)response.statusCode]];
                    [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginSocialWebviewDidFail:) withObject:param waitUntilDone:NO];
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }
        }
    } @catch (NSException *exception) {
        NSLog(@"StayLog: EXCEPTION in LoginWebView decidePolicyForNavigationResponse with error: %@", exception.description);
        
        NSArray *param = @[self.socialNetwork, exception.description];
        [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginSocialWebviewDidFail:) withObject:param waitUntilDone:NO];
         [self.navigationController popViewControllerAnimated:YES];
    } @finally {
        decisionHandler(WKNavigationResponsePolicyAllow);
    }
}


- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error
{
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
