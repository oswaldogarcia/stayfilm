//
//  Popover_Delete_ViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Henrique on 5/16/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import "Popover_Delete_ViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface Popover_Delete_ViewController ()

@end

@implementation Popover_Delete_ViewController

@synthesize but_delete, but_cancel;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *modalTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOutside)];
    modalTap.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:modalTap];
    
    self.but_cancel.layer.borderWidth = 1.0f;
    self.but_cancel.layer.borderColor = [[UIColor colorWithRed:80.0/255.0 green:128.0/255.0 blue:146.0/255.0 alpha:1.0] CGColor];
    self.but_cancel.layer.cornerRadius = 5;
    self.but_cancel.clipsToBounds = YES;
    
    [self.thubVideoImage sd_setImageWithURL:[NSURL URLWithString:self.thumbnailUrl]];
    CGFloat degrees = 15.0f; //the value in degrees
    CGFloat radians = degrees * M_PI/180;
    self.thubVideoImage.transform = CGAffineTransformMakeRotation(radians);
    
}
- (void)viewDidAppear:(BOOL)animated{
    
    if(self.sfApp.isEditingMode && [self.analyticTag isEqualToString:@"delete-film"]) {
        //Google Analytics
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:@"more-options_delete-film"];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    }
    if([self.analyticTag isEqualToString:@"delete_film-in-profile"]) {
        //Google Analytics
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:self.analyticTag];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];

    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tapOutside {
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:self.analyticTag
                                                          action:@"cancel"
                                                           label:@""
                                                           value:@1] build]];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)but_delete_Clicked:(id)sender {
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:self.analyticTag
                                                          action:@"ok"
                                                           label:@""
                                                           value:@1] build]];
    
    [self tapOutside];
    [self.delegate performSelector:@selector(chooseOption:) withObject:@"confirmDelete"];
}

- (IBAction)but_cancel_Clicked:(id)sender {
    
    [self tapOutside];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
