//
//  GoToSettingsNotificationViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 12/10/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol GoToSettingsNotificationDelegate <NSObject>
-(void)goToSettings;
-(void)closeModal;
@end

@interface GoToSettingsNotificationViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *pleaseAllowLabel;
@property (weak, nonatomic) IBOutlet UIButton *goToSettingsButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;


@property (nonatomic, assign) id <GoToSettingsNotificationDelegate> delegate;
@end

NS_ASSUME_NONNULL_END
