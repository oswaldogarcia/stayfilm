//
//  ConfirmSubscriptionViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 4/10/19.
//  Copyright © 2019 Stayfilm. All rights reserved.
//

#import "ConfirmSubscriptionViewController.h"

@interface ConfirmSubscriptionViewController ()

@end

@implementation ConfirmSubscriptionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.confirmLabel.text =  NSLocalizedString(@"CONFIRM_PURCHASE", nil);
}
- (IBAction)okAction:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
