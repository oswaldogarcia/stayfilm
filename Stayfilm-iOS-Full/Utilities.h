//
//  Utilities.h
//  Stayfilm
//
//  Created by Henrique Ormonde on 18/05/17.
//  Copyright (c) 2017 Stayfilm. All rights reserved.
//

#ifndef Stayfilm_Utilities_h
#define Stayfilm_Utilities_h

typedef NS_ENUM(NSInteger, StayfilmTypes_Genre)
{
    Genre_all,
    Genre_psychedelicRide,
    Genre_afterMidnight,
    Genre_arcade,
    Genre_onTheGo,
    Genre_together,
    Genre_funkNSoul,
    Genre_carnival,
    Genre_br2014,
    Genre_durex,
    Genre_dayDream
};

/// <summary>
/// Stayfilm Genre configuration isactive property
/// </summary>
typedef NS_ENUM(NSInteger, StayfilmTypes_ConfigIsActive)
{
    ConfigIsActive_inactive,
    ConfigIsActive_prod,
    ConfigIsActive_staging,
    ConfigIsActive_staging_prod
};

/// <summary>
/// Stayfilm job types.
/// </summary>
typedef NS_ENUM(NSInteger, StayfilmTypes_JobType)
{
    JobType_timeline,
    JobType_socialnetwork,
    JobType_imageanalyzer
};

/// <summary>
/// Stayfilm possible job status.
/// </summary>
typedef NS_ENUM(NSInteger, StayfilmTypes_JobStatus)
{
    JobStatus_FAILURE,
    JobStatus_PENDING,
    JobStatus_SUCCESS
};

/// <summary>
/// Stayfilm socialnetworks. This is the same order as it is in the database.
/// </summary>
typedef NS_ENUM(NSInteger, StayfilmTypes_SocialNetwork)
{
    SocialNetwork_sf_upload,
    SocialNetwork_facebook,
    SocialNetwork_twitter,
    SocialNetwork_instagram,
    SocialNetwork_flickr,
    SocialNetwork_vimeo,
    SocialNetwork_sf_album_manager,
    SocialNetwork_googleplus,
    SocialNetwork_tumblr
};

/// <summary>
/// Stayfilm possible film status.
/// </summary>
typedef NS_ENUM(NSInteger, StayfilmTypes_MovieStatus)
{
    MovieStatus_DELETED,
    MovieStatus_ACTIVE,
    MovieStatus_DENOUNCE,
    MovieStatus_PENDING
};

/// <summary>
/// Stayfilm possible film permission.
/// </summary>
typedef NS_ENUM(NSInteger, StayfilmTypes_MoviePermission)
{
    MoviePermission_PRIVATE = 1,
    MoviePermission_FRIEND = 2,
    MoviePermission_PUBLIC = 3,
    MoviePermission_AD = 4,
    MoviePermission_UNLISTED = 7
};

///// <summary>
///// Movie thumbnails. the "_t" termination is the red thumb.
///// </summary>
//typedef enum
//{
//    s_266x150_n,
//    s_266x150_t,
//    s_572x322_n,
//    s_572x322_t,
//    s_640x360_n,
//    s_640x360_t
//}StayfilmTypes_MovieThumb;

/// <summary>
/// Notification status. 0 is unread; 1 is read;
/// </summary>
typedef NS_ENUM(NSInteger, StayfilmTypes_NotificationStatus)
{
    NotificationStatus_UNREAD,
    NotificationStatus_READ
};

/// <summary>
/// Stayfilm Explore Story Types.
/// </summary>
typedef NS_ENUM(NSInteger, StayfilmTypes_ExploreStoryContent)
{
    ExploreStoryContent_MovieLANDSCAPE = 1,
    ExploreStoryContent_MoviePORTRAIT = 2,
    ExploreStoryContent_IMAGE = 3
};
typedef NS_ENUM(NSInteger, StayfilmTypesFilmStatus)
{
    STATUS_DELETED    = 0,
    STATUS_ACTIVE     = 1,
    STATUS_DENOUNCE   = 2,
    STATUS_PENDING    = 3,
    STATUS_ONAPPROVAL = 4,
    STATUS_UNAPPROVED = 5,
    STATUS_UNLISTED_PENDING   = 6,
    STATUS_UNLISTED_PUBLISHED = 7,
    STATUS_DELETE_BY_SCRIPT = 8
};

typedef NS_ENUM(NSInteger, StayfilmTypesFilmOrientation)
{
    NO_ORIENTATION = 0,
    HORIZONTAL = 1,
    VERTICAL = 2
};


#endif
