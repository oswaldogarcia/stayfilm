//
//  WaitingTableViewCell.h
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 12/11/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Movie.h"
#import <SDWebImage/UIImageView+WebCache.h>

@import KALoader;

NS_ASSUME_NONNULL_BEGIN

@interface WaitingTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *movieTitleLabel;

@property (weak, nonatomic) IBOutlet UIImageView *thumbnailImage;
@property (weak, nonatomic) IBOutlet UIImageView *statusFlagImage;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UIImageView *logoImage;


@property (nonatomic, strong) Movie *movie;

- (void)initCell;



@end

NS_ASSUME_NONNULL_END
