//
//  ConfirmMediasOperation.m
//  Stayfilm for Messenger
//
//  Created by Henrique Ormonde on 26/10/15.
//  Copyright © 2015 Stayfilm. All rights reserved.
//

#import "ConfirmMediasOperation.h"
#import "Job.h"

@implementation ConfirmMediasOperation

@synthesize listConfirmMedias, confirmedAlbum, albumID, delegate;

#pragma mark -
#pragma mark - Life Cycle

- (id)initWithMedias:(NSArray *)p_medias withAlbum:(AlbumSF *)p_album withAlbumID:(NSString *)p_albumID delegate:(id<ConfirmMediaDelegate>) theDelegate
{
    if (self = [super init]) {
        self.delegate = theDelegate;
        self.albumID = p_albumID;
        self.listConfirmMedias = p_medias;
        self.confirmedAlbum = p_album;
    }
    return self;
}

#pragma mark -
#pragma mark - Medias Confirmation


- (void)main
{
    @try {
        if(self.confirmedAlbum == nil)
        {
            NSMutableArray *medias = [[NSMutableArray alloc] init];
            for (Media *object in self.listConfirmMedias)
            {
                NSDictionary *mediaDictionary = nil;
                if(object.isVideo)
                {
                    mediaDictionary = @{ @"idmedia" : object.idMidia,
                                         @"ext" : @"mov" };
                }
                else
                {
                    mediaDictionary = @{ @"idmedia" : object.idMidia,
                                         @"ext" : @"jpg" };
                }
                [medias addObject:mediaDictionary];
            }
            NSString *idAlbum = [[NSString alloc] init];
            NSArray *failedMedias = [Media confirmMedias:medias WithIdAlbum:nil andOverrideIdAlbum:&idAlbum];
            BOOL gotAlbum = NO;
            self.albumID = idAlbum;
            if(idAlbum != nil && failedMedias != nil && ![((NSString*)[failedMedias firstObject]) isEqualToString:@"0"] )
            {
                self.confirmedAlbum = [AlbumSF getAlbumWithID:idAlbum];
                gotAlbum = YES;
                NSMutableArray *mediasList = [[NSMutableArray alloc] init];
                for (Media *object in self.listConfirmMedias) {
                    [mediasList addObject:object.idMidia];
                }
                self.confirmedAlbum.idMedias = mediasList;
                [Job postImageAnalyzerWithAlbumID:idAlbum];
            }
            if(failedMedias == nil)
            {
                //Exception on the request
                [(NSObject *)self.delegate performSelectorOnMainThread:@selector(confirmMediasDidFail:) withObject:self waitUntilDone:NO];
                NSLog(@"StayLog: Error ConfirmMediaOperation: failedMedias exception");
                return;
            }
            else if([[failedMedias firstObject] isEqualToString:@"0"])
            {
                //WebService response status with error
                [(NSObject *)self.delegate performSelectorOnMainThread:@selector(confirmMediasDidFail:) withObject:self waitUntilDone:NO];
                NSLog(@"StayLog: Error ConfirmMediaOperation: failedMedias response not OK");
                return;
            }
            else if([failedMedias count] > 0)
            {
                // Some medias Failed
                [(NSObject *)self.delegate performSelectorOnMainThread:@selector(confirmMediasDidFailWithTheseMedias:) withObject:failedMedias waitUntilDone:NO];
                return;
            }
            else if(gotAlbum)
            {
                // Album created successfully
                self.confirmedAlbum = [AlbumSF getAlbumWithID:idAlbum];
                if(self.confirmedAlbum != nil) {
                    for (Media *media in self.listConfirmMedias) {
                        [self.confirmedAlbum.idMedias addObject:media.idMidia];
                    }
                }
                
                if (self.confirmedAlbum == nil || self.confirmedAlbum.idAlbum == nil)
                {
                    //Error creating album...
                    NSLog(@"StayLog: Error ConfirmMediaOperation: confirmedAlbum is nil");
                    [(NSObject *)self.delegate performSelectorOnMainThread:@selector(confirmMediasDidFail:) withObject:self waitUntilDone:NO];
                    return;
                }
                else
                { // Add all selected medias
                    self.confirmedAlbum = [AlbumSF createAlbumWithMedias:self.confirmedAlbum.idMedias withTitle:nil];
                    self.albumID = self.confirmedAlbum.idAlbum;
                    [Job postImageAnalyzerWithAlbumID:self.confirmedAlbum.idAlbum];
                    [(NSObject *)self.delegate performSelectorOnMainThread:@selector(confirmMediasDidFinish:) withObject:self waitUntilDone:NO];
                }
            }
            else
            {
                [(NSObject *)self.delegate performSelectorOnMainThread:@selector(confirmMediasDidFail:) withObject:self waitUntilDone:NO];
                NSLog(@"StayLog: Error ConfirmMediaOperation: failed to get Album");
                return;
            }
            
        }
        else //Already have an Album, lets just add the new Medias
        {
            NSMutableArray *medias = [[NSMutableArray alloc] init];
            for (Media *object in self.listConfirmMedias)
            {
                NSDictionary *mediaDictionary = nil;
                if(object.isVideo)
                {
                    mediaDictionary = @{ @"idmedia" : object.idMidia,
                                         @"ext" : @"mov" };
                }
                else
                {
                    mediaDictionary = @{ @"idmedia" : object.idMidia,
                                         @"ext" : @"jpg" };
                }
                [medias addObject:mediaDictionary];
            }
            NSArray *failedMedias = [Media confirmMedias:medias WithIdAlbum:self.confirmedAlbum.idAlbum andOverrideIdAlbum:nil];
            if(failedMedias == nil)
            {
                //Exception on the request
                [(NSObject *)self.delegate performSelectorOnMainThread:@selector(confirmMediasDidFail:) withObject:self waitUntilDone:NO];
                NSLog(@"StayLog: Error ConfirmMediaOperation: failedMedias exception");
                return;
            }
            else if([[failedMedias firstObject] isEqualToString:@"0"])
            {
                //WebService response status with error
                [(NSObject *)self.delegate performSelectorOnMainThread:@selector(confirmMediasDidFail:) withObject:self waitUntilDone:NO];
                NSLog(@"StayLog: Error ConfirmMediaOperation: failedMedias response not OK");
                return;
            }
            else if([failedMedias count] > 0)
            {
                // Some medias Failed
                [(NSObject *)self.delegate performSelectorOnMainThread:@selector(confirmMediasDidFailWithTheseMedias:) withObject:failedMedias waitUntilDone:NO];
                return;
            }
            else
            {
                // Files added successfully
                self.confirmedAlbum = [AlbumSF getAlbumWithID:self.confirmedAlbum.idAlbum];
                self.confirmedAlbum.idMedias = [self.listConfirmMedias mutableCopy];
            }
            
            if (self.confirmedAlbum == nil || self.confirmedAlbum.idAlbum == nil)
            {
                //Error creating album...
                [(NSObject *)self.delegate performSelectorOnMainThread:@selector(confirmMediasDidFail:) withObject:self waitUntilDone:NO];
                NSLog(@"StayLog: Error ConfirmMediaOperation: confirmedAlbum is nil");
                return;
            }
            else
            {
                [Job postImageAnalyzerWithAlbumID:self.confirmedAlbum.idAlbum];
                [(NSObject *)self.delegate performSelectorOnMainThread:@selector(confirmMediasDidFinish:) withObject:self waitUntilDone:NO];
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"StayLog: EXCEPTION sendConfirmationMedias with error: %@", exception.description);
        [(NSObject *)self.delegate performSelectorOnMainThread:@selector(confirmMediasDidFail:) withObject:self waitUntilDone:NO];
        return;
    }
}

@end
