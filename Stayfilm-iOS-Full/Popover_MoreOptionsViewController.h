//
//  Popover_MoreOptionsViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Henrique on 5/16/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Movie.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Popover_Delete_ViewController.h"
#import "Uploader.h"
#import <Google/Analytics.h>

@protocol Popover_MoreOptionsDelegate<NSObject>
@required
- (void)chooseOption:(NSString*)option;
@end

@interface Popover_MoreOptionsViewController : UIViewController <StayfilmUploaderDelegate>

@property (nonatomic, strong) id<Popover_MoreOptionsDelegate> delegate;

@property (nonatomic, strong) Movie *movie;
@property (weak, nonatomic) StayfilmApp *sfApp;
@property (weak, nonatomic) IBOutlet UIImageView *styleImage;
@property (weak, nonatomic) IBOutlet UIImageView *templateImage;
@property (weak, nonatomic) IBOutlet UILabel *styleNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberOfImageLabel;
@property (weak, nonatomic) IBOutlet UILabel *currentFilmTitleLabel;
@property (nonatomic, strong) NSString *movieFile;


@property (weak, nonatomic) IBOutlet UILabel *numberItemsLabel;

@property (weak, nonatomic) IBOutlet UIImageView *collageImage1;
@property (weak, nonatomic) IBOutlet UIImageView *collageImage2;
@property (weak, nonatomic) IBOutlet UIImageView *collageImage3;
@property (weak, nonatomic) IBOutlet UIImageView *collageImage4;

@property (weak, nonatomic) IBOutlet UIButton *updateFilmButton;

@property (weak, nonatomic) IBOutlet UIImageView *updateArrowImage;

@property (weak, nonatomic) IBOutlet UIView *view_Loading;


@property (weak, nonatomic) IBOutlet UILabel *filmOrientationLabel;

@property (weak, nonatomic) IBOutlet UILabel *currentOrientationLabel;
@property (weak, nonatomic) IBOutlet UIImageView *orientationImage;

@property (nonatomic) StayfilmTypesFilmOrientation selectedOrientation;
@end
