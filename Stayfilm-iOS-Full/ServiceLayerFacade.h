//
//  ServiceLayerFacade.h
//  Stayfilm
//
//  Created by Victor Manuel Roldan on 5/16/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Service.h"
#import "ShellWebService.h"

@interface ServiceLayerFacade : NSObject

+ (ServiceLayerFacade *)getSharedInstance;

- (id<Service>)getServiceCall;

@end
