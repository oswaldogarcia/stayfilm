//
//  LoginToSocialNetworkOperation.h
//  Stayfilm-iOS-Full
//
//  Created by Henrique Ormonde on 15/09/17.
//  Copyright © 2017 Stayfilm. All rights reserved.
//
#ifndef Stayfilm_Full_LoginToSocialNetworkOperation_h
#define Stayfilm_Full_LoginToSocialNetworkOperation_h

#import <Foundation/Foundation.h>
#import "StayfilmApp.h"
#import "LoginFacebookOperation.h"
//#import "LoginSocialWebviewOperation.h"
#import "LoginWebView.h"

enum {
    SOCIAL_FACEBOOK,
    SOCIAL_INSTAGRAM,
    SOCIAL_GOOGLE,
    SOCIAL_TWITTER
};
typedef NSInteger SocialNetwork;


@protocol LoginToSocialNetworkOperationDelegate;


@interface LoginToSocialNetworkOperation : NSOperation <LoginFacebookOperationDelegate>

@property (nonatomic, assign) id <LoginToSocialNetworkOperationDelegate> delegate;
@property (nonatomic, weak) StayfilmApp *sfAppLogin;
@property (nonatomic, assign) NSInteger socialNetwork;
@property (nonatomic, assign) NSInteger messageCode;

-(id)initWithDelegate:(id<LoginToSocialNetworkOperationDelegate>)p_delegate andSFApp:(StayfilmApp *)p_sfApp forSocialNetwork:(NSInteger )p_socialNetwork;

@end


@protocol LoginToSocialNetworkOperationDelegate <NSObject>
- (void)loginToSocialNetworkDidSucceed:(LoginToSocialNetworkOperation *)loginOP;
- (void)loginToSocialNetworkDidFail:(LoginToSocialNetworkOperation *)loginOP;

- (void)loginSocialWebviewDidSucceed:(NSArray *)token;
- (void)loginSocialWebviewDidFail:(NSArray *)error;
@end


#endif
