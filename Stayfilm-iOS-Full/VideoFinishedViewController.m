//
//  VideoFinishedViewController.m
//  fbmessenger
//
//  Created by Henrique Ormonde on 18/06/15.
//  Copyright (c) 2015 Stayfilm. All rights reserved.
//

@import AVKit;
@import AVFoundation;
@import UserNotifications;


#import "AppDelegate.h"
#import "StayfilmApp.h"
#import "VideoFinishedViewController.h"
#import "PlayerViewController.h"
#import "ProgressViewController.h"
#import "ShadowMotionEffect.h"
#import <Google/Analytics.h>
#import "Uploader.h"
#import "PopupMenuAnimation.h"
#import "Reachability.h"
#import "InstagramActivity.h"
#import "DownloadActivity.h"
//#import <FBSDKShareKit/FBSDKShareLinkContent.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <SDWebImage/UIImageView+WebCache.h>


@implementation UIImage (ImageBlur)
- (UIImage *)imageWithGaussianBlur {
    float weight[5] = {0.2270270270, 0.1945945946, 0.1216216216, 0.0540540541, 0.0162162162};
    // Blur horizontally
    UIGraphicsBeginImageContext(self.size);
    [self drawInRect:CGRectMake(0, 0, self.size.width, self.size.height) blendMode:kCGBlendModePlusLighter alpha:weight[0]];
    for (int x = 1; x < 5; ++x) {
        [self drawInRect:CGRectMake(x, 0, self.size.width, self.size.height) blendMode:kCGBlendModePlusLighter alpha:weight[x]];
        [self drawInRect:CGRectMake(-x, 0, self.size.width, self.size.height) blendMode:kCGBlendModePlusLighter alpha:weight[x]];
    }
    UIImage *horizBlurredImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    // Blur vertically
    UIGraphicsBeginImageContext(self.size);
    [horizBlurredImage drawInRect:CGRectMake(0, 0, self.size.width, self.size.height) blendMode:kCGBlendModePlusLighter alpha:weight[0]];
    for (int y = 1; y < 5; ++y) {
        [horizBlurredImage drawInRect:CGRectMake(0, y, self.size.width, self.size.height) blendMode:kCGBlendModePlusLighter alpha:weight[y]];
        [horizBlurredImage drawInRect:CGRectMake(0, -y, self.size.width, self.size.height) blendMode:kCGBlendModePlusLighter alpha:weight[y]];
    }
    UIImage *blurredImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    //
    return blurredImage;
}
@end



@interface VideoFinishedViewController () <PrivacyOptionsDelegate,NotificationNotAllowedDelegate,PreviewNotificationDelegate,GoToSettingsNotificationDelegate>

@property (nonatomic, strong) NSURLSession *thumbLoadSession;
@property (strong) NSURLSessionDataTask *task;
@property (nonatomic, strong) UITapGestureRecognizer *tapGR;

//@property (nonatomic, strong) UIImage *imageWithoutBlur;

@property (nonatomic, weak) StayfilmApp *sfApp;
@property (nonatomic, weak) Uploader *uploader;
@property (nonatomic, strong) Downloader *downloaderStayfilm;

@property (nonatomic, strong) UIViewController *modalPreview;

@property (nonatomic, strong) Popover_ShareVideo_ViewController * popoverShareView;
@property (nonatomic, strong) Popover_SaveVideo_ViewController * popoverSaveView;
@property (nonatomic, strong) ProgressBarViewController * popoverProgressBar;

@property (nonatomic, assign) int retryThumbAttempts;
@property (nonatomic, assign) int fbAttempts;
@property (nonatomic, assign) int fbLoginAttempts;
@property (nonatomic, assign) BOOL fbSetToShare;
@property (nonatomic, assign) BOOL whatsappSetToShare;
@property (nonatomic, assign) BOOL instagramSetToShare;
@property (nonatomic, assign) int socialLoginAttempts;

@property (nonatomic, assign) BOOL isMovieDownloadedPrivate;

@property (nonatomic, assign) int lastSharedPermission;

@end

@implementation VideoFinishedViewController

@synthesize movieFile, movie, movieThumb, discardButton, tapGR, transitionAnimation, sfApp, downloaderStayfilm, instagramSetToShare, lastSharedPermission;//, imageWithoutBlur;
@synthesize txt_userName, txt_MovieTitle, but_Publish, but_MoreOptions, but_menuPrivacy;


NSURLConnection * connection;

NSMutableData *receivedData;
float expectedBytes;
double progress;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.retryThumbAttempts = 0;
    self.fbAttempts = 0;
    self.fbLoginAttempts = 0;
    self.fbSetToShare = NO;
    
    self.isMovieDownloadedPrivate = NO;
    self.popoverProgressBar = nil;
    self.popoverSaveView = nil;
    self.popoverShareView = nil;
    
    // this will be needed when upon download implementation
//    self.progressChecker = [NSTimer scheduledTimerWithTimeInterval:1.0
//                                                            target:self
//                                                          selector:@selector(timerTick:)
//                                                          userInfo:nil
//                                                           repeats:YES];
    
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [[UIColor alloc] initWithRed:51.0/255.0 green:87.0/255.0 blue:113.0/255.0 alpha:1.0],NSForegroundColorAttributeName,
                                    [[UIColor alloc] initWithRed:51.0/255.0 green:87.0/255.0 blue:113.0/255.0 alpha:1.0],NSBackgroundColorAttributeName,nil];
    
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    
    progress = 0;
    
    self.transitionAnimation = [[PopupMenuAnimation alloc] init];

    self.but_Publish.layer.cornerRadius = 5;
    self.but_Publish.layer.masksToBounds = YES;
    self.img_User.layer.cornerRadius = self.img_User.frame.size.width / 2;
    self.img_User.layer.masksToBounds = YES;
    
    CGFloat width  = [UIScreen mainScreen].bounds.size.width;
    self.constraint_MovieThumb.constant = (width * 360) / 640;
    
    self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    self.thumbLoadSession = [NSURLSession sessionWithConfiguration:config];
    
    self.task = [self.thumbLoadSession dataTaskWithURL:[NSURL URLWithString:self.movie.thumbnailUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(error)
        {
            NSLog(@"StayLog: PhotoCollectionImage request error: %@", error);
            self.retryThumbAttempts++;
            [self retryLoadMovieThumb];
        }
        else {
            //add blur to thumbnail
//            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
//                self.imageWithoutBlur = [UIImage imageWithData:data];
//                UIImage *aux = [self.imageWithoutBlur imageWithGaussianBlur];
//                aux = [aux imageWithGaussianBlur];                    //blur image 2 times
//                self.movieThumb.image = aux;                          //blur image 3 times
//                [(UIActivityIndicatorView *)[self.view viewWithTag:10] stopAnimating];
//            }];
            runOnMainQueueWithoutDeadlocking(^{
                self.movieThumb.image = [UIImage imageWithData:data];
                [(UIActivityIndicatorView *)[self.view viewWithTag:10] stopAnimating];
                [(UIActivityIndicatorView *)[self.view viewWithTag:10] setHidden:YES];
            });
        }
    }];
    [self.task resume];
    
//    self.tapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(shareMovieStayfilm:)];
//    self.tapGR.numberOfTapsRequired = 1;
//    [self.but_Publish addGestureRecognizer:self.tapGR];
    
    self.txt_MovieTitle.text = (self.movie.title != nil && ![self.movie.title isEqualToString:@""]) ? self.movie.title : @"";
    
    self.txt_userName.text = (self.sfApp.currentUser.firstName != nil && ![self.sfApp.currentUser.firstName isEqualToString:@""])? [self.sfApp.currentUser.firstName stringByAppendingString:[NSString stringWithFormat:@" %@",self.sfApp.currentUser.lastName]] : self.sfApp.currentUser.username;
    [self.txt_MovieTitle sizeToFit];
    
    self.movie.permission = MoviePermission_UNLISTED;
    self.lastSharedPermission = -1;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.downloaderStayfilm = [Downloader sharedDownloaderStayfilmSingletonWithDelegate:self];
    
    // Disable swipe gesture to go back to previous viewcontroller
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
    //Google Analytics
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"view-srceen_film-completed"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    if(self.sfApp.currentUser.photo == nil || [self.sfApp.currentUser.photo isEqualToString:@""]) {
        self.img_User.image = [UIImage imageNamed:@"Avatar_None"];
    }
    else {
        [self.img_User sd_setImageWithURL:[NSURL URLWithString:self.sfApp.currentUser.photo] placeholderImage:[UIImage imageNamed:@"Avatar_None"] options:SDWebImageRefreshCached];
    }
//    if([self.sfApp.imagesCache objectForKey:self.sfApp.currentUser.photo] != nil) {
//        self.img_User.image = [self.sfApp.imagesCache objectForKey:self.sfApp.currentUser.photo];
//    }
//    else if(self.sfApp.currentUser.photo == nil || [self.sfApp.currentUser.photo isEqualToString:@""]) {
//        self.img_User.image = [UIImage imageNamed:@"Avatar_None"];
//    }
//    else {
//        __weak typeof(self) selfDelegate = self;
//        self.task = [self.thumbLoadSession dataTaskWithURL:[NSURL URLWithString:self.sfApp.currentUser.photo] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//            if(error)
//            {
//                NSLog(@"StayLog: PhotoCollectionImage user.photo request error: %@", error);
//            }
//            else {
//                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
//                    self.img_User.image = [UIImage imageWithData:data];
//                    [selfDelegate.sfApp.imagesCache setObject:self.img_User.image forKey:self.sfApp.currentUser.photo];
//                }];
//            }
//        }];
//        [self.task resume];
//    }
    
    // Unlock Audio Lock button
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    NSError *error = nil;
    BOOL result = NO;
    if ([audioSession respondsToSelector:@selector(setActive:withOptions:error:)]) {
        result = [audioSession setActive:YES withOptions:0 error:&error]; // iOS6+
    }
    if (!result && error) {
        // deal with the error
        NSLog(@"StayLog: AVAudioSession Error: %@", error);
    }
    
    error = nil;
    result = [audioSession setCategory:AVAudioSessionCategoryPlayback error:&error];
    
    if (!result && error) {
        // deal with the error
        NSLog(@"StayLog: AVAudioSession setCategory Error: %@", error);
    }
 
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.hidden = YES;
    
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    [center getNotificationSettingsWithCompletionHandler:^(UNNotificationSettings *settings){
        
        self.notificationsAllowed = (settings.authorizationStatus == UNAuthorizationStatusAuthorized);
        
    }];
    
     [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateNormal];
//    //Google Analytics
//    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName value:@"Iphone_Nativo_Tela_8_SFMobile_Passo_4_filme-produzido"];
//    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    self.navigationController.navigationBar.hidden = NO;
}

// this will be needed when Download is fully implemented
//-(void)timerTick:(NSTimer *)timer {
//    int progress = [self checkDownloadProgress];
//    if(progress != -1)
//    {
//        self.percentLabel.text = [[NSString alloc] initWithFormat:@"%d %%", progress];
//        if(progress == 100)
//        {
//            [self.progressChecker invalidate];
//            self.progressChecker = nil;
//        }
//    }
//    if(!downloading)
//    {
//        self.percentLabel.text = @"100 %";
//        [self.progressChecker invalidate];
//        self.progressChecker = nil;
//    }
//}

-(BOOL)connected {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

- (IBAction)discardButtonClicked:(id)sender {
//    UIAlertController* alertc = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ARE_YOU_SURE", nil)
//                                                                    message:NSLocalizedString(@"CONFIRM_UNWIND_FROM_VIDEOFINISHED", nil)
//                                                             preferredStyle:UIAlertControllerStyleAlert];
//    
//    UIAlertAction* okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
//                                                     handler:^(UIAlertAction * action) {
//                                                         [alertc dismissViewControllerAnimated:YES completion:nil];
//                                                         AppDelegate *appdelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
//                                                         
////                                                         //Google Analytics Event
////                                                         id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
////                                                         [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Botao"
////                                                                                                               action:@"Filme-produzido_descartar"
////                                                                                                                label:@"Iphone_Nativo_evento_104_botao_Filme-produzido_descartar"
////                                                                                                                value:@1] build]];
//                                                         
//                                                         if(self.movieFile != nil)
//                                                         {
//                                                             if(self.sfApp == nil)
//                                                             {
//                                                                 self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
//                                                             }
//                                                             [self.sfApp deleteMovieFromDevice:self.movieFile];
////                                                             [self.sfApp.settings setObject:@"1" forKey:@"FILM_PRODUCED"];
//                                                             [self.sfApp saveSettings];
//                                                         }
//                                                         [self.sfApp cleanRemakeState];
//                                                         [appdelegate resetMovieMakerStoryboard];
//                                                     }];
//    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"STAY", nil) style:UIAlertActionStyleCancel
//                                                         handler:^(UIAlertAction * action) {
//                                                             [alertc dismissViewControllerAnimated:YES completion:nil];
//                                                         }];
//    
//    [alertc addAction:okAction];
//    [alertc addAction:cancelAction];
//    [self presentViewController:alertc animated:YES completion:nil];
}


- (IBAction)saveCameraRollChanged:(id)sender {
//    if (self.switchSaveToCameraRoll.isOn)
//    {
//        //Google Analytics Event
//        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Botao"
//                                                              action:@"Salvar-filme"
//                                                               label:@"Iphone_Nativo_evento_112_botao_salvar-filme"
//                                                               value:@1] build]];
//    }
}


-(void)dismissModalMenu
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)retryLoadMovieThumb {
    
    self.task = [self.thumbLoadSession dataTaskWithURL:[NSURL URLWithString:self.movie.thumbnailUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(error)
        {
            NSLog(@"StayLog: PhotoCollectionImage request error: %@", error);
            if(self.retryThumbAttempts > 3) {
                runOnMainQueueWithoutDeadlocking(^{
                    UIAlertController *alertc = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UNABLE_LOAD_MOVIE_THUMB", nil)
                                                                                    message:NSLocalizedString(@"UNABLE_LOAD_MOVIE_THUMB_MESSAGE", nil)
                                                                             preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                          handler:^(UIAlertAction * action) {
                                                                              [alertc dismissViewControllerAnimated:YES completion:nil];
                                                                          }];
                    [alertc addAction:defaultAction];
                    [self presentViewController:alertc animated:YES completion:nil];
                });
            } else {
                self.retryThumbAttempts++;
                [self retryLoadMovieThumb];
            }
        }
        else {
            runOnMainQueueWithoutDeadlocking(^{
                self.movieThumb.image = [UIImage imageWithData:data];
                [(UIActivityIndicatorView *)[self.view viewWithTag:10] stopAnimating];
                [(UIActivityIndicatorView *)[self.view viewWithTag:10] setHidden:YES];
            });
        }
    }];
    [self.task resume];
}


#pragma mark - Popover Delegates

-(void)chooseOption:(NSString*)option {
    
    if([option isEqualToString:@"updateFilm"]) {
        ProgressViewController *remakeProgress = [self.storyboard instantiateViewControllerWithIdentifier:@"Progress"];
        [self.navigationController showViewController:remakeProgress sender:self];
    }
    else if([option isEqualToString:@"remake"]) {
        ProgressViewController *remakeProgress = [self.storyboard instantiateViewControllerWithIdentifier:@"Progress"];
        self.sfApp.selectedTemplate = nil;
        [self.navigationController showViewController:remakeProgress sender:self];
    }
    else if ([option isEqualToString:@"makeNew"]) {
//        if(self.movieFile != nil)
//        {
//            if(self.sfApp == nil)
//            {
//                self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
//            }
//            [self.sfApp deleteMovieFromDevice:self.movieFile];
//        }
//        AppDelegate *appdelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
//        [appdelegate resetMovieMakerStoryboard];
    }
    else if ([option isEqualToString:@"delete"]) {
        Popover_Delete_ViewController * view = [[Popover_Delete_ViewController alloc] init];
        view.delegate = self;
        view.analyticTag = @"delete-film";
        view.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        view.transitioningDelegate = self;
        view.view.frame = self.view.frame;
        self.transitionAnimation.duration = 0.3;
        self.transitionAnimation.isPresenting = YES;
        self.transitionAnimation.originFrame = self.view.frame;
        
        [self presentViewController:view animated:YES completion:nil];
    }
    else if ([option isEqualToString:@"confirmDelete"]) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^(void){
            
            if(self.movieFile != nil)
            {
                if(self.sfApp == nil)
                {
                    self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
                }
                [self.sfApp deleteMovieFromDevice:self.movieFile];
                [self.sfApp saveSettings];
            }
            
            [self.movie deleteMovie];
            [self.sfApp cleanRemakeState];
            
            [self.sfApp setEditingMode:NO];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                runOnMainQueueWithoutDeadlocking(^{
                    AppDelegate *appdelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
                    [appdelegate resetMovieMakerStoryboard];
                });
            });
            
        });
        
    }
    else if ([option isEqualToString:@"confirmDownload"])
    {  // Popover_ProgressBar Download delegate
        self.popoverProgressBar = [[ProgressBarViewController alloc] init];
        self.popoverProgressBar.delegate = self;
        self.popoverProgressBar.isModal = YES;
        self.popoverProgressBar.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        self.popoverProgressBar.transitioningDelegate = self;
        self.popoverProgressBar.view.frame = self.view.frame;
        self.transitionAnimation.duration = 0.3;
        self.transitionAnimation.isPresenting = YES;
        self.transitionAnimation.originFrame = self.view.frame;
        
        [self downloadMovie];
        
        [self presentViewController:self.popoverProgressBar animated:YES completion:nil];
    }
    else if ([option isEqualToString:@"cancelDownload"]) {
        if(self.isMovieDownloadedPrivate) {
            AppDelegate *appdelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
            [self.sfApp cleanRemakeState];
            [appdelegate resetMovieMakerStoryboard];
        } else {
            [self.downloaderStayfilm resetDownload];
            self.popoverProgressBar = nil;
            self.popoverSaveView = nil;
            self.popoverShareView = nil;
            runOnMainQueueWithoutDeadlocking(^{
                [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateSelected];
                [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateNormal];
                [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateFocused];
                [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateHighlighted];
            });
        }
    }
    else if([option isEqualToString:@"noDownload"]) {
        AppDelegate *appdelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        [self.sfApp cleanRemakeState];
        [appdelegate resetMovieMakerStoryboard];
    }
}

-(void)changePrivacy:(int)privacy {
    switch (privacy) {
        case MoviePermission_PUBLIC: {
            self.but_menuPrivacy.image = [UIImage imageNamed:@"privacyPublic"];
            self.movie.permission = MoviePermission_PUBLIC;
            runOnMainQueueWithoutDeadlocking(^{
                [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateSelected];
                [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateNormal];
                [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateFocused];
                [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateHighlighted];
            });
        }
            break;
        case MoviePermission_UNLISTED: {
            self.but_menuPrivacy.image = [UIImage imageNamed:@"PrivacyLink"];
            self.movie.permission = MoviePermission_UNLISTED;
            runOnMainQueueWithoutDeadlocking(^{
                [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateSelected];
                [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateNormal];
                [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateFocused];
                [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateHighlighted];
            });
        }
            break;
        case MoviePermission_PRIVATE: {
            self.but_menuPrivacy.image = [UIImage imageNamed:@"privacyPrivate"];
            self.movie.permission = MoviePermission_PRIVATE;
            runOnMainQueueWithoutDeadlocking(^{
                [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateSelected];
                [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateNormal];
                [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateFocused];
                [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateHighlighted];
            });
        }
            break;
            
        default:
            self.but_menuPrivacy.image = [UIImage imageNamed:@"privacyPublic"];
            self.movie.permission = MoviePermission_PUBLIC;
            runOnMainQueueWithoutDeadlocking(^{
                [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateSelected];
                [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateNormal];
                [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateFocused];
                [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateHighlighted];
            });
            break;
    }
}

-(void)decidedOption:(NSString *)p_option {
    if ([p_option isEqualToString:@"done"]) {
        
        self.uploader = [Uploader sharedUploadStayfilmSingleton];
        [self.uploader resetUpload];
        
        AppDelegate *appdelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        [self.sfApp cleanRemakeState];
        [self.sfApp setEditingMode:NO];
        [appdelegate resetMovieMakerStoryboard];
        
    }
}
- (IBAction)imDoneAction:(id)sender {
    
    AppDelegate *appdelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [self.sfApp cleanRemakeState];
    [self.sfApp setEditingMode:NO];
    [appdelegate resetMovieMakerStoryboard];
}




-(void)dismissModalView
{
    [self dismissViewControllerAnimated:YES completion:nil];
    self.modalPreview = nil;
}


#pragma mark - Facebook Share Delegate

- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results
{
    NSLog(@"StayLog: Successfully shared in Facebook");
    
}

- (void)sharerDidCancel:(id<FBSDKSharing>)sharer
{
    NSLog(@"StayLog: Facebook share canceled!");
}

- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error
{
    NSLog(@"StayLog: Facebook sharing error:%@", error.localizedDescription);
    UIAlertController *alertc = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"FACEBOOK_ERROR", nil)
                                                                    message:NSLocalizedString(@"FACEBOOK_ERROR_MESSAGE", nil)
                                                             preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              [alertc dismissViewControllerAnimated:YES completion:nil];
                                                          }];
    [alertc addAction:defaultAction];
    [self presentViewController:alertc animated:YES completion:nil];
    return;
}


#pragma mark - Login Operations Delegate

-(void)loginFacebookDidFinish:(LoginFacebookOperation *)loginOP {
    // switch codemessage
    
    switch (loginOP.messageCode) {
        case LOGIN_SUCCESSFUL:
        {
            self.fbLoginAttempts = 0;
            if(self.fbSetToShare) {
                BOOL success = [self.movie shareMovieOnFacebook];
                if(success) {
                    UIAlertController *alertc = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"FACEBOOK_SHARED", nil)
                                                                                    message:NSLocalizedString(@"FACEBOOK_SHARED_MESSAGE", nil)
                                                                             preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                            style:UIAlertActionStyleDefault
                                                                          handler:^(UIAlertAction * action) {
                                                                              [alertc dismissViewControllerAnimated:YES completion:nil];
                                                                          }];
                    [alertc addAction:defaultAction];
                    [self presentViewController:alertc animated:YES completion:nil];
                    return;
                }
                else {
                    UIAlertController *alertc = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"FACEBOOK_ERROR", nil)
                                                                                    message:NSLocalizedString(@"FACEBOOK_ERROR_MESSAGE", nil)
                                                                             preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                            style:UIAlertActionStyleDefault
                                                                          handler:^(UIAlertAction * action) {
                                                                              [alertc dismissViewControllerAnimated:YES completion:nil];
                                                                          }];
                    [alertc addAction:defaultAction];
                    [self presentViewController:alertc animated:YES completion:nil];
                    return;
                }
            }
        }
            break;
            
        default:
        {
            ++self.fbLoginAttempts;
        }
            break;
    }
    
}

-(void)loginFacebookDidFail:(LoginFacebookOperation *)loginOP {
    
    ++self.fbLoginAttempts;
    if(self.fbLoginAttempts >= 3)
    {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"FACEBOOK_FALIED", nil)
                                                                       message:NSLocalizedString(@"FACEBOOK_FALIED_MESSAGE", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        self.fbLoginAttempts = 0;
        return;
    }
    
    BOOL retryLogin = NO;
    switch (loginOP.messageCode) {
        case CONNECTION_ERROR:
        {
            retryLogin = YES;
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CONNECTION_ERROR", nil)
                                                                           message:NSLocalizedString(@"CONNECTION_ERROR_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                    style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
            break;
        case USER_CANCELLED:
        {
            retryLogin = NO;
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"USER_CANCELLED", nil)
                                                                           message:NSLocalizedString(@"USER_CANCELLED_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                    style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
            break;
        case LOGIN_FAILED:
        {
            retryLogin = YES;
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"LOGIN_FAILED", nil)
                                                                           message:NSLocalizedString(@"LOGIN_FAILED_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                    style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
            break;
        case REGISTRATION_ERROR:
        {
            retryLogin = YES;
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"REGISTRATION_ERROR", nil)
                                                                           message:NSLocalizedString(@"REGISTRATION_ERROR_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                    style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
            break;
        case NO_CONFIG_LOADED:
        {
            retryLogin = YES;
            self.fbLoginAttempts--;
            // say nothing, just try again
            if(self.sfApp != nil)
                self.sfApp.sfConfig = [SFConfig getAllConfigsFromWS:self.sfApp.wsConfig];
            else
                self.sfApp = [StayfilmApp sharedStayfilmAppSingletonWithDelegate:self];
            break;
        }
        case NO_INTERNET_CONNECTION:
        {
            retryLogin = NO;
            self.fbLoginAttempts--;
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"NO_INTERNET_CONNECTION", nil)
                                                                           message:NSLocalizedString(@"NO_INTERNET_CONNECTION_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                    style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
            
            [NSTimer scheduledTimerWithTimeInterval:2.0 repeats:YES block:^(NSTimer * _Nonnull timer) {
                if([self connected])
                {
                    [timer invalidate];
                    timer = nil;
                    LoginFacebookOperation *loginOp = [[LoginFacebookOperation alloc] initWithDelegate:self andSFApp:self.sfApp];
                    [self.sfApp.operationsQueue addOperation:loginOp];
                }
            }];
            break;
        }
        case -1:
        {
            retryLogin = YES;
            NSLog(@"StayLog: loginDidFail with EXCEPTION (messageCode -1)");
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"LOGIN_FAILED", nil)
                                                                           message:NSLocalizedString(@"LOGIN_FAILED_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                    style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
            break;
        default:
        {
            retryLogin = YES;
        }
            break;
    }
    
    if(retryLogin)
    {
        LoginFacebookOperation *logOperation = [[LoginFacebookOperation alloc] initWithDelegate:self andSFApp:self.sfApp];
        [self.sfApp.operationsQueue addOperation:logOperation];
    }
    else
    {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"LOGIN_FAILED", nil)
                                                                       message:NSLocalizedString(@"LOGIN_FAILED_MESSAGE", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    [loginOP cancel];
}

- (void)loginToSocialNetworkDidSucceed:(LoginToSocialNetworkOperation *)loginOP; {
    switch (loginOP.messageCode) {
        case LOGIN_SUCCESSFUL:
        {
            self.socialLoginAttempts = 0;
            if(loginOP.socialNetwork == SOCIAL_INSTAGRAM) {
                [self decidedOption:@"instagram"];
            }
            else if(loginOP.socialNetwork == SOCIAL_TWITTER) {
                [self decidedOption:@"twitter"];
            }
        }
            break;
            
        default:
        {
            ++self.socialLoginAttempts;
        }
            break;
    }
}
- (void)loginToSocialNetworkDidFail:(LoginToSocialNetworkOperation *)loginOP {
    ++self.socialLoginAttempts;
    if(self.socialLoginAttempts >= 3)
    {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"SOCIAL_FALIED", nil)
                                                                       message:NSLocalizedString(@"SOCIAL_FALIED_MESSAGE", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        self.socialLoginAttempts = 0;
        return;
    }
    
    BOOL retryLogin = NO;
    switch (loginOP.messageCode) {
        case CONNECTION_ERROR:
        {
            retryLogin = YES;
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CONNECTION_ERROR", nil)
                                                                           message:NSLocalizedString(@"CONNECTION_ERROR_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                    style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
            break;
        case USER_CANCELLED:
        {
            retryLogin = NO;
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"USER_CANCELLED", nil)
                                                                           message:NSLocalizedString(@"USER_CANCELLED_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                    style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
            break;
        case LOGIN_FAILED:
        {
            retryLogin = YES;
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"LOGIN_FAILED", nil)
                                                                           message:NSLocalizedString(@"LOGIN_FAILED_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                    style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
            break;
        case REGISTRATION_ERROR:
        {
            retryLogin = YES;
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"REGISTRATION_ERROR", nil)
                                                                           message:NSLocalizedString(@"REGISTRATION_ERROR_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                    style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
            break;
        case NO_CONFIG_LOADED:
        {
            retryLogin = YES;
            self.socialLoginAttempts--;
            // say nothing, just try again
            if(self.sfApp == nil)
                self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
            
            self.sfApp.sfConfig = [SFConfig getAllConfigsFromWS:self.sfApp.wsConfig];
            break;
        }
        case NO_INTERNET_CONNECTION:
        {
            retryLogin = NO;
            self.socialLoginAttempts--;
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"NO_INTERNET_CONNECTION", nil)
                                                                           message:NSLocalizedString(@"NO_INTERNET_CONNECTION_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                    style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
            
            if(loginOP.socialNetwork == SOCIAL_INSTAGRAM) {
                [NSTimer scheduledTimerWithTimeInterval:2.0 repeats:YES block:^(NSTimer * _Nonnull timer) {
                    if([self connected])
                    {
                        [timer invalidate];
                        timer = nil;
                        LoginToSocialNetworkOperation *loginOp = [[LoginToSocialNetworkOperation alloc] initWithDelegate:self andSFApp:self.sfApp forSocialNetwork:SOCIAL_INSTAGRAM];
                        [self.sfApp.operationsQueue addOperation:loginOp];
                    }
                }];
            } else if (loginOP.socialNetwork == SOCIAL_GOOGLE) {
                [NSTimer scheduledTimerWithTimeInterval:2.0 repeats:YES block:^(NSTimer * _Nonnull timer) {
                    if([self connected])
                    {
                        [timer invalidate];
                        timer = nil;
                        LoginToSocialNetworkOperation *loginOp = [[LoginToSocialNetworkOperation alloc] initWithDelegate:self andSFApp:self.sfApp forSocialNetwork:SOCIAL_GOOGLE];
                        [self.sfApp.operationsQueue addOperation:loginOp];
                    }
                }];
            }
            break;
        }
        case -1:
        {
            retryLogin = YES;
            NSLog(@"StayLog: loginDidFail with EXCEPTION (messageCode -1)");
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"LOGIN_FAILED", nil)
                                                                           message:NSLocalizedString(@"LOGIN_FAILED_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                    style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
            break;
        default:
        {
            retryLogin = YES;
        }
            break;
    }
    
    if(retryLogin)
    {
        LoginToSocialNetworkOperation *logOperation = [[LoginToSocialNetworkOperation alloc] initWithDelegate:self andSFApp:self.sfApp forSocialNetwork:loginOP.socialNetwork];
        [self.sfApp.operationsQueue addOperation:logOperation];
    }
    else
    {
        switch (loginOP.socialNetwork) {
            case SOCIAL_INSTAGRAM:
                {
                    // do something ? maybe show a friendly message or suggestion?
                }
                break;
            case SOCIAL_GOOGLE:
                {
//                    SocialAlbumsView *google = (SocialAlbumsView *)[self.view viewWithTag:3];
//                    google.albums = self.albunsStayfilm;
//                    google.isExpanded = NO;
//                    google.isLoaded = NO;
                }
                break;
            case SOCIAL_TWITTER:
                {
                    
                }
                break;
            default:
                break;
        }
    }
    
    [loginOP cancel];
}

-(void)loginSocialWebviewDidSucceed:(NSArray *)p_token {
    @try {
        if ([[p_token objectAtIndex:0] isEqualToString:@"instagram"])
        {
            BOOL success = [self.sfApp.currentUser setUserInstagramToken:[p_token objectAtIndex:1] andSFAppSingleton:self.sfApp];
            if(success)
            {
                [Job postSocialNetwork:@"instagram"];
                self.socialLoginAttempts = 0;
                
                if(UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(self.movieFile)) {
            //UISaveVideoAtPathToSavedPhotosAlbum(self.movieFile,self,@selector(video:didFinishSavingWithError:contextInfo:), nil);
                }
            }
            else
            {
                if(self.socialLoginAttempts < 3) {
                    ++self.socialLoginAttempts;
                    LoginToSocialNetworkOperation *loginOp = [[LoginToSocialNetworkOperation alloc] initWithDelegate:self andSFApp:self.sfApp forSocialNetwork:SOCIAL_INSTAGRAM];
                    [self.sfApp.operationsQueue addOperation:loginOp];
                }
                else
                {
                    self.socialLoginAttempts = 0;
                    runOnMainQueueWithoutDeadlocking(^{
                        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"LOGIN_FAILED", nil)
                                                                                       message:NSLocalizedString(@"LOGIN_FAILED_MESSAGE", nil)
                                                                                preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                                style:UIAlertActionStyleDefault
                                                                              handler:^(UIAlertAction * action) {}];
                        
                        [alert addAction:defaultAction];
                        [self.popoverShareView presentViewController:alert animated:YES completion:nil];
                    });
                    return;
                }
            }
        }
        else if ([[p_token objectAtIndex:0] isEqualToString:@"twitter"])
        {
            
            NSString  *tokenVerifier = [[p_token objectAtIndex:1] stringByAppendingString:@":"];
            tokenVerifier = [tokenVerifier stringByAppendingString:[p_token objectAtIndex:2]];
            BOOL success = [self.sfApp.currentUser setUserTwitterToken:tokenVerifier  andSFAppSingleton:self.sfApp];
            if(success)
            {
                self.socialLoginAttempts = 0;
                BOOL successShare = [self.movie shareMovieOnTwitterWithMessage:nil];
                if(successShare) {
                    runOnMainQueueWithoutDeadlocking(^{
                        UIAlertController *alertc = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"TWITTER_SHARED", nil)
                                                                                        message:NSLocalizedString(@"TWITTER_SHARED_MESSAGE", nil)
                                                                                 preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                                style:UIAlertActionStyleDefault
                                                                              handler:^(UIAlertAction * action) {
                                                                                  [alertc dismissViewControllerAnimated:YES completion:nil];
                                                                              }];
                        [alertc addAction:defaultAction];
                        [self.popoverShareView presentViewController:alertc animated:YES completion:nil];
                    });
                    return;
                }
                else {
                    runOnMainQueueWithoutDeadlocking(^{
                        UIAlertController *alertc = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"TWITTER_ERROR", nil)
                                                                                        message:NSLocalizedString(@"TWITTER_ERROR_MESSAGE", nil)
                                                                                 preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                                style:UIAlertActionStyleDefault
                                                                              handler:^(UIAlertAction * action) {
                                                                                  [alertc dismissViewControllerAnimated:YES completion:nil];
                                                                              }];
                        [alertc addAction:defaultAction];
                        [self.popoverShareView presentViewController:alertc animated:YES completion:nil];
                    });
                    return;
                }
            }
            else
            {
                if(self.socialLoginAttempts < 3) {
                    ++self.socialLoginAttempts;
                    LoginToSocialNetworkOperation *loginOp = [[LoginToSocialNetworkOperation alloc] initWithDelegate:self andSFApp:self.sfApp forSocialNetwork:SOCIAL_TWITTER];
                    [self.sfApp.operationsQueue addOperation:loginOp];
                }
                else
                {
                    self.socialLoginAttempts = 0;
                    runOnMainQueueWithoutDeadlocking(^{
                        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"LOGIN_FAILED", nil)
                                                                                       message:NSLocalizedString(@"LOGIN_FAILED_MESSAGE", nil)
                                                                                preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                                style:UIAlertActionStyleDefault
                                                                              handler:^(UIAlertAction * action) {}];
                        
                        [alert addAction:defaultAction];
                        [self.popoverShareView presentViewController:alert animated:YES completion:nil];
                    });
                }
            }
        }
    } @catch (NSException *ex) {
        NSLog(@"StayLog: EXCEPTION in loginSocialWebViewDidSucceed with error: %@", ex.description);
    }
}

-(void)loginSocialWebviewDidFail:(NSArray *)error {
    //TODO: need to do this yet
    
}

#pragma mark - Stayfilm Methods


- (IBAction)but_menuPrivacy_Clicked:(id)sender {
    ChangePrivacyViewController * view = [[ChangePrivacyViewController alloc] init];
    view.delegate = self;
    
    view.videoPosition = -1;
    view.currentPrivacy = self.movie.permission;
//    view.modalPresentationCapturesStatusBarAppearance = YES;
    view.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    view.transitioningDelegate = self;
    view.view.frame = self.view.frame;
    self.transitionAnimation.duration = 0.3;
    self.transitionAnimation.isPresenting = YES;
    self.transitionAnimation.originFrame = self.view.frame;
    
    [self presentViewController:view animated:YES completion:nil];
}




- (IBAction)but_MoreOptions_Clicked:(id)sender {
    if(isSharing) {
        return;
    }
    
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"film"
                                                          action:@"more-options"
                                                           label:@""
                                                           value:@1] build]];
    
    Popover_MoreOptionsViewController * view = [[Popover_MoreOptionsViewController alloc] init];
    view.delegate = self;
    
    view.movie= self.movie;
    view.sfApp = self.sfApp;
    view.movieFile = self.movieFile;
    
    view.modalPresentationStyle = UIModalPresentationOverCurrentContext;

    //view.modalPresentationCapturesStatusBarAppearance = YES;
//    view.transitioningDelegate = self;
//    view.view.frame = self.view.frame;
//    self.transitionAnimation.duration = 0.3;
//    self.transitionAnimation.isPresenting = YES;
//    self.transitionAnimation.originFrame = self.view.frame;

    [self.navigationController showViewController:view sender:nil];
    
    //[self presentViewController:view animated:YES completion:nil];
}

- (IBAction)but_PlayMovie_Clicked:(id)sender {
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"film"
                                                          action:@"played"
                                                           label:@""
                                                           value:@1] build]];
    
    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"Player" owner:self options:nil];
    PlayerViewController *moviePlayerViewController = [topLevelObjects objectAtIndex:0];
    moviePlayerViewController.currentMovie = self.movie;
    if(self.movieFile != nil)
    {
        moviePlayerViewController.player = [AVPlayer playerWithURL:[NSURL fileURLWithPath:self.movieFile]];
    }
    else{
        moviePlayerViewController.player = [AVPlayer playerWithURL:[NSURL URLWithString:self.movie.videoUrl]];
    }

    if ([[[UIDevice currentDevice] systemVersion] floatValue]  < 11.0 ){

        AVPlayerViewController *playerVC = [[AVPlayerViewController alloc]init];

        if(self.movieFile != nil)
        {
           playerVC.player = [AVPlayer playerWithURL:[NSURL fileURLWithPath:self.movieFile]];
        }
        else{
            playerVC.player = [AVPlayer playerWithURL:[NSURL URLWithString:self.movie.videoUrl]];
        }

        [playerVC setShowsPlaybackControls:YES];
        [playerVC.player play];
        [self presentViewController:playerVC animated:YES completion:nil];

    }else{
        [self presentViewController:moviePlayerViewController animated:YES completion:nil];
    }
    
//    [self performSegueWithIdentifier:@"PlayMovieSegue" sender:self];
}

BOOL isSharing = NO;
- (IBAction)shareMovieStayfilm:(id)sender {
    
    if(isSharing) {
        return;
    }
    @try
    {
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"film"
                                                              action:@"publish-share"
                                                               label:@""
                                                               value:@1] build]];
        
//        if(self.movie.permission == MoviePermission_PRIVATE) {
//            [self.but_Publish setTitle:NSLocalizedString(@"SAVING", nil) forState:UIControlStateSelected];
//            [self.but_Publish setTitle:NSLocalizedString(@"SAVING", nil) forState:UIControlStateNormal];
//            [self.but_Publish setTitle:NSLocalizedString(@"SAVING", nil) forState:UIControlStateFocused];
//            [self.but_Publish setTitle:NSLocalizedString(@"SAVING", nil) forState:UIControlStateHighlighted];
//        } else {
        isSharing = YES;
            [self.but_Publish setTitle:NSLocalizedString(@"SAVING", nil) forState:UIControlStateSelected];
            [self.but_Publish setTitle:NSLocalizedString(@"SAVING", nil) forState:UIControlStateNormal];
            [self.but_Publish setTitle:NSLocalizedString(@"SAVING", nil) forState:UIControlStateFocused];
            [self.but_Publish setTitle:NSLocalizedString(@"SAVING", nil) forState:UIControlStateHighlighted];
//        }
        
        if(self.sfApp == nil)
        {
            self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
        }
        
        //        //Google Analytics
        //        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        //        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Botao"
        //                                                              action:@"Filme-produzido_Send"
        //                                                           label:@"Iphone_Nativo_evento_103_botao_Filme-produzido_Send"
        //                                                               value:@1] build]];
        
        if (self.movie != nil && self.movie.idMovie != nil)
        {
            if([self.movie.status integerValue] != 7 && [self.movie.status integerValue] != 1) // 7 = unlisted published  1 = active
            {
                [self performSelectorInBackground:@selector(sendMovieShareRequest) withObject:nil];
            }
            else if([self.movie.status integerValue] == 7 || [self.movie.status integerValue] == 1) {
//                if(self.movie.permission != self.lastSharedPermission) {
//                    [self performSelectorInBackground:@selector(sendMovieShareRequest) withObject:nil];
//                    return;
//                }
                
//                if(self.movie.permission == MoviePermission_PRIVATE) {
//                    [self.but_Publish setTitle:NSLocalizedString(@"SAVED", nil) forState:UIControlStateSelected];
//                    [self.but_Publish setTitle:NSLocalizedString(@"SAVED", nil) forState:UIControlStateNormal];
//                    [self.but_Publish setTitle:NSLocalizedString(@"SAVED", nil) forState:UIControlStateFocused];
//                    [self.but_Publish setTitle:NSLocalizedString(@"SAVED", nil) forState:UIControlStateHighlighted];
//
//                     //Show SAVE MENU POPOVER
//                    self.popoverSaveView = [[Popover_SaveVideo_ViewController alloc] initWithDelegate:self];
//                    self.popoverSaveView.modalPresentationStyle = UIModalPresentationOverCurrentContext;
//                    self.popoverSaveView.transitioningDelegate = self;
//                    self.popoverSaveView.view.frame = self.view.frame;
//
//                    self.transitionAnimation.duration = 0.3;
//                    self.transitionAnimation.isPresenting = YES;
//                    self.transitionAnimation.originFrame = self.view.frame;
//
//                    self.popoverShareView = nil;
//                    [self presentViewController:self.popoverSaveView animated:YES completion:nil];
//                } else {
                    [self.but_Publish setTitle:NSLocalizedString(@"SAVED", nil) forState:UIControlStateSelected];
                    [self.but_Publish setTitle:NSLocalizedString(@"SAVED", nil) forState:UIControlStateNormal];
                    [self.but_Publish setTitle:NSLocalizedString(@"SAVED", nil) forState:UIControlStateFocused];
                    [self.but_Publish setTitle:NSLocalizedString(@"SAVED", nil) forState:UIControlStateHighlighted];
                    
                    // Show CUSTOM SHARE MENU POPOVER
                    self.popoverShareView = [[Popover_ShareVideo_ViewController alloc] initWithDelegate:self];
                    self.popoverShareView.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                    self.popoverShareView.transitioningDelegate = self;
                    //self.popoverShareView.view.frame = self.view.frame;
                    CGRect rect = CGRectMake(self.view.frame.origin.x , self.view.frame.origin.y - 44, self.view.frame.size.width, self.view.frame.size.height + 44 );
                    self.popoverShareView.view.frame = rect;
                    self.popoverShareView.movie = self.movie;
                    self.popoverShareView.isModal = YES;
                    
                    self.transitionAnimation.duration = 0.3;
                    self.transitionAnimation.isPresenting = YES;
                    self.transitionAnimation.originFrame = self.view.frame;
                    
                    self.popoverSaveView = nil;
                    isSharing = NO;
                    [self presentViewController:self.popoverShareView animated:YES completion:nil];
                
                //Google Analytics
                id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                [tracker set:kGAIScreenName value:@"share-screen"];
                [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
                    

            }
        }
        else
        {
            NSLog(@"StayLog: Error: Movie File path missing.");
            isSharing = NO;
            runOnMainQueueWithoutDeadlocking(^{
//                if(self.movie.permission == MoviePermission_PRIVATE) {
//                    [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateSelected];
//                    [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateNormal];
//                    [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateFocused];
//                    [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateHighlighted];
//                } else {
                    [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateSelected];
                    [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateNormal];
                    [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateFocused];
                    [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateHighlighted];
//                }
            });
        }
    }
    @catch (NSException *e)
    {
        NSLog(@"StayLog: EXCEPTION shareMovieStayfilm with error: %@", e.description);
        isSharing = NO;
        runOnMainQueueWithoutDeadlocking(^{
//            if(self.movie.permission == MoviePermission_PRIVATE) {
//                [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateSelected];
//                [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateNormal];
//                [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateFocused];
//                [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateHighlighted];
//            } else {
                [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateSelected];
                [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateNormal];
                [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateFocused];
                [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateHighlighted];
//            }
        });
    }
}

- (void)sendMovieShareRequest {
    
    BOOL success = NO;
//    if(self.txt_UserComment.text == nil || self.txt_UserComment.text.length == 0) {
    self.movie.permission = MoviePermission_UNLISTED;
        success = [self.movie publishMovie];
    self.lastSharedPermission = self.movie.permission;
//    }
//    else {
//        success = [self.movie publishMovieWithDescription:self.txt_UserComment.text];
//    }
    if(success)
    {
        self.movie.status = [NSNumber numberWithInt:7];
        
        runOnMainQueueWithoutDeadlocking(^{
//            if(self.movie.permission == MoviePermission_PRIVATE) {
//                [self.but_Publish setTitle:NSLocalizedString(@"SAVED", nil) forState:UIControlStateSelected];
//                [self.but_Publish setTitle:NSLocalizedString(@"SAVED", nil) forState:UIControlStateNormal];
//                [self.but_Publish setTitle:NSLocalizedString(@"SAVED", nil) forState:UIControlStateFocused];
//                [self.but_Publish setTitle:NSLocalizedString(@"SAVED", nil) forState:UIControlStateHighlighted];
//
//                // Show SAVE MENU POPOVER
//                self.popoverSaveView = [[Popover_SaveVideo_ViewController alloc] initWithDelegate:self];
//                self.popoverSaveView.modalPresentationStyle = UIModalPresentationOverCurrentContext;
//                self.popoverSaveView.transitioningDelegate = self;
//                self.popoverSaveView.view.frame = self.view.frame;
//                self.transitionAnimation.duration = 0.3;
//                self.transitionAnimation.isPresenting = YES;
//                self.transitionAnimation.originFrame = self.view.frame;
//
//                self.popoverShareView = nil;
//                [self presentViewController:self.popoverSaveView animated:YES completion:nil];
//            } else {
                [self.but_Publish setTitle:NSLocalizedString(@"SAVED", nil) forState:UIControlStateSelected];
                [self.but_Publish setTitle:NSLocalizedString(@"SAVED", nil) forState:UIControlStateNormal];
                [self.but_Publish setTitle:NSLocalizedString(@"SAVED", nil) forState:UIControlStateFocused];
                [self.but_Publish setTitle:NSLocalizedString(@"SAVED", nil) forState:UIControlStateHighlighted];
            
            if ([self.sfApp.selectedGenre.config.campaign.idcustomer integerValue] == 37){
                
                [self showNotificationsModals:self.notificationsAllowed];
                
            }else{
            

                // Show SHARE MENU POPOVER
                self.popoverShareView = [[Popover_ShareVideo_ViewController alloc] initWithDelegate:self];
                self.popoverShareView.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                self.popoverShareView.transitioningDelegate = self;
//                self.popoverShareView.view.frame = self.view.frame;
                CGRect rect = CGRectMake(self.view.frame.origin.x , self.view.frame.origin.y - 44, self.view.frame.size.width, self.view.frame.size.height + 44 );
                self.popoverShareView.view.frame = rect;
                self.popoverShareView.movie = self.movie;
                self.popoverShareView.isModal = YES;
                
                self.transitionAnimation.duration = 0.3;
                self.transitionAnimation.isPresenting = YES;
                self.transitionAnimation.originFrame = self.view.frame;

                self.popoverSaveView = nil;
                [self presentViewController:self.popoverShareView animated:YES completion:nil];
            }
                

        });
    }
    else {
        runOnMainQueueWithoutDeadlocking(^{
//            if(self.movie.permission == MoviePermission_PRIVATE) {
//                [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateSelected];
//                [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateNormal];
//                [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateFocused];
//                [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateHighlighted];
//            } else {
                [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateSelected];
                [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateNormal];
                [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateFocused];
                [self.but_Publish setTitle:NSLocalizedString(@"SAVE", nil) forState:UIControlStateHighlighted];
//            }
            
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"SHARE_FAILED", nil)
                                                                           message:NSLocalizedString(@"SHARE_FAILED_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                                    style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        });
    }
    isSharing = NO;
}


- (void)showNotificationsModals:(BOOL)allowed{
    
    if(allowed){
        
        PreviewNotificationViewController * view = [[PreviewNotificationViewController alloc] init];
        view.delegate = self;
        view.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        view.view.frame = self.view.frame;
        
        [self presentViewController:view animated:YES completion:nil];
    }else{
        
        NotificationNotAllowedViewController * view = [[NotificationNotAllowedViewController alloc] init];
        view.delegate = self;
        view.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        view.view.frame = self.view.frame;
        
        [self presentViewController:view animated:YES completion:nil];
        
    }
    
    
}


- (BOOL)fileExists:(NSString*)idMovie
{
    @try
    {
        NSString* filePath;
        
        if (idMovie != nil && [idMovie length] > 0)
        {
            NSArray   *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString  *documentsDirectory = ([paths objectAtIndex:0] != nil)? [paths objectAtIndex:0] : nil;
            
            NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/StayFilms"];
            
            //Check Stayfilm folder if it does exists
            if ([[NSFileManager defaultManager] fileExistsAtPath:dataPath])
            {
                filePath = [NSString stringWithFormat:@"%@/%@", dataPath, [idMovie stringByAppendingString:@".mp4"]];
                //Checks if file exists
                if ([[NSFileManager defaultManager] fileExistsAtPath:filePath])
                {
                    return YES;
                }
            }
            return NO;
        }
        else
        {
            NSLog(@"StayLog: Error: idMovie missing.");
        }
    }
    @catch (NSException *exception)
    {
        NSLog(@"StayLog: fileExists exception with error: %@", exception.description);
    }
    
}

-(void)sfSingletonCreated {
    // nothing
}
-(void)connectivityChanged:(BOOL)isConnected {
    if(!isConnected) {
//        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"NO_INTERNET_CONNECTION", nil)
//                                                                       message:NSLocalizedString(@"NO_INTERNET_CONNECTION_MESSAGE", nil)
//                                                                preferredStyle:UIAlertControllerStyleAlert];
//        
//        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
//                                                              handler:^(UIAlertAction * action) {}];
//        
//        [alert addAction:defaultAction];
//        [self presentViewController:alert animated:YES completion:nil];
    }
}


#pragma mark - Animation Delegates

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source {
    return self.transitionAnimation;
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    self.transitionAnimation.isPresenting = NO;
    return self.transitionAnimation;
}


#pragma mark - Download Operations Delegate

- (void)downloadMovie {
    
    if([self fileExists:self.movie.idMovie]){
        self.downloading = YES;
    }
    
    
    @try {
        if(self.downloading)
        {
            //Already downloading
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ALREADY_DOWNLOADED", nil) message:nil preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:nil];
            
            [alert addAction:defaultAction];
            
            
            [self presentViewController:alert animated:YES completion:nil];
            return;
        }
        self.downloading = YES;
        NSLog(@"StayLog: downloadMovie invoked");
        
        if(self.downloaderStayfilm == nil) {
            self.downloaderStayfilm = [Downloader sharedDownloaderStayfilmSingletonWithDelegate:self];
        }
        if(self.instagramSetToShare) {
            [self.downloaderStayfilm addMovieWithoutSavingToPhotosAlbum:self.movie];
        } else {
            [self.downloaderStayfilm addMovie:self.movie];
        }
        
        self.downloading = NO;
    }
    @catch (NSException *ex) {
        NSLog(@"StayLog: EXCEPTION downloadMovie with error: %@", ex.description);
        self.downloading = NO;
    }
}

- (void)downloadDidFail:(Downloader *)downloader {
    if(self.popoverShareView != nil) {
        [self.popoverShareView showDownloadFailed];
        return;
    }
    
    if(self.popoverProgressBar != nil) {
        self.popoverProgressBar.progressLabel.text = NSLocalizedString(@"DOWNLOAD_FAILED", nil);
        return;
    }
}

- (void)downloadDidFailMedia:(NSString *)movieID {
    if(self.popoverShareView != nil) {
        [self.popoverShareView showDownloadFailed];
        return;
    }
    
    if(self.popoverProgressBar != nil) {
        self.popoverProgressBar.progressLabel.text = NSLocalizedString(@"DOWNLOAD_FAILED", nil);
        return;
    }
}

- (void)downloadDidFinish:(Downloader *)downloader {
//    [self.popoverShareView hideDownloadProgress];
//    [self.popoverShareView setDownloadedButton];
}

- (void)alreadyDownloadedMedia:(Movie *)movie {
    if(self.popoverShareView != nil) {
        [self.popoverShareView showAlreadyDownloaded];
        return;
    }
    
    if(self.popoverProgressBar != nil) {
        self.popoverProgressBar.progressLabel.text = @"100%%";
        self.popoverProgressBar.lbl_downloading.text = NSLocalizedString(@"ALREADY_DOWNLOADED", nil);
        [self.popoverProgressBar.but_cancel setTitle:NSLocalizedString(@"IM_DONE", nil) forState:UIControlStateSelected];
        [self.popoverProgressBar.but_cancel setTitle:NSLocalizedString(@"IM_DONE", nil) forState:UIControlStateNormal];
        [self.popoverProgressBar.but_cancel setTitle:NSLocalizedString(@"IM_DONE", nil) forState:UIControlStateFocused];
        [self.popoverProgressBar.but_cancel setTitle:NSLocalizedString(@"IM_DONE", nil) forState:UIControlStateHighlighted];
        [self.popoverProgressBar.but_cancel setImage:[UIImage imageNamed:@"Icon_xCheck"] forState:UIControlStateNormal];
        [self.popoverProgressBar.but_cancel setImage:[UIImage imageNamed:@"Icon_xCheck"] forState:UIControlStateSelected];
        
        self.isMovieDownloadedPrivate = YES;
        return;
    }
}

- (void)downloadDidStart {
    // do nothing
}

- (void)downloadFinishMedia:(Movie *)movie {
    if(self.popoverShareView != nil) {
        [self.popoverShareView hideDownloadProgressSettingDownloadedButton];
        if(self.instagramSetToShare) {
            [self decidedOption:@"instagram"];
    //        [self.popoverShareView hideDownloadProcessWithoutAnimation];
    //        return;
        }
        return;
    }
    
    if(self.popoverProgressBar != nil) {
        self.isMovieDownloadedPrivate = YES;
        
        self.popoverProgressBar.lbl_downloading.text = NSLocalizedString(@"DOWNLOADED", nil);
        [self.popoverProgressBar.but_cancel setTitle:NSLocalizedString(@"IM_DONE", nil) forState:UIControlStateSelected];
        [self.popoverProgressBar.but_cancel setTitle:NSLocalizedString(@"IM_DONE", nil) forState:UIControlStateNormal];
        [self.popoverProgressBar.but_cancel setTitle:NSLocalizedString(@"IM_DONE", nil) forState:UIControlStateFocused];
        [self.popoverProgressBar.but_cancel setTitle:NSLocalizedString(@"IM_DONE", nil) forState:UIControlStateHighlighted];
        [self.popoverProgressBar.but_cancel setImage:[UIImage imageNamed:@"Icon_xCheck"] forState:UIControlStateNormal];
        [self.popoverProgressBar.but_cancel setImage:[UIImage imageNamed:@"Icon_xCheck"] forState:UIControlStateSelected];
        if(self.movieFile != nil) {
            if(UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(self.movieFile)) {
//                UISaveVideoAtPathToSavedPhotosAlbum(self.movieFile, nil, nil, nil);
            }
        }
        return;
    }
}

- (void)downloadOverallProgressChanged:(NSNumber *)progress {
    // do nothing
}

- (void)downloadProgressChanged:(NSArray *)movie_Progress {
    if(self.popoverShareView != nil) {
        self.popoverShareView.txt_DownloadProgress.text = [NSString stringWithFormat:NSLocalizedString(@"DOWNLOADING_PROGRESS", nil),((NSNumber *)movie_Progress[1]).floatValue * 100.0 ];
        self.popoverShareView.progress_Download.progress = ((NSNumber *)movie_Progress[1]).floatValue;
        return;
    }
    
    if(self.popoverProgressBar != nil) {
        self.popoverProgressBar.progressLabel.text = [NSString stringWithFormat:NSLocalizedString(@"DOWNLOADING_PROGRESS", nil),((NSNumber *)movie_Progress[1]).floatValue * 100.0 ];
        self.popoverProgressBar.downloadProgressBar.progress = ((NSNumber *)movie_Progress[1]).floatValue;
        return;
    }
}

/// Callback for copying to camera roll
- (void)               video: (NSString *) videoPath
    didFinishSavingWithError: (NSError *) error
                 contextInfo: (void *) contextInfo
{
    if (!error) {
        NSString * urlBase = [NSString stringWithFormat:@"instagram://library?AssetPath=%@&InstagramCaption=%@",[videoPath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet alphanumericCharacterSet]], [self.movie.title stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet alphanumericCharacterSet]]];
        NSURL *instagramURL = [NSURL URLWithString:urlBase];

        if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
            if(self.movieFile != nil) {
                [[UIApplication sharedApplication] openURL:instagramURL];
            }
        }
        self.instagramSetToShare = NO;
    }
    else {
        NSLog(@"StayLog: Error in video:didFinishSavingWithError:contextInfo with message: %@", error.localizedDescription);
    }
}


#pragma mark - Navigation


// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
     //Get the new view controller using [segue destinationViewController].
     //Pass the selected object to the new view controller.
    
//    // Feature removed
//    if ([segue.identifier isEqualToString:@"RemakeFilm"])
//    {
//        if(self.sfApp == nil)
//        {
//            self.sfApp = [StayfilmApp sharedStayfilmAppSingletonWithDelegate:self];
//        }
//        
//        @try {
//            [self.sfApp navigateToProduceAgainWithView:nil];
//        }
//        @catch(NSException *ex)
//        {
//            NSLog(@"StayLog: EXCEPTION in prepareForSegue to progressViewController with error: %@", ex.description);
//        }
//        return;
//    }
    
    if ([segue.identifier isEqualToString:@"PlayMovieSegue"])
    {
        PlayerViewController *moviePlayerViewController = segue.destinationViewController;
        moviePlayerViewController.currentMovie = self.movie;
        if(self.movieFile != nil)
        {
            moviePlayerViewController.player = [AVPlayer playerWithURL:[NSURL fileURLWithPath:self.movieFile]];
        }
        else{
            moviePlayerViewController.player = [AVPlayer playerWithURL:[NSURL URLWithString:self.movie.videoUrl]];
        }
    }
}
#pragma mark - Notification Delegates

-(void)closeModal{
    
    AppDelegate *appdelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [self.sfApp cleanRemakeState];
    [self.sfApp setEditingMode:NO];
    [appdelegate resetMovieMakerStoryboard];
    
}
- (void)goToAllowNotification{
    
    GoToSettingsNotificationViewController * view = [[GoToSettingsNotificationViewController alloc] init];
    view.delegate = self;
    view.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    view.view.frame = self.view.frame;
    
    [self presentViewController:view animated:YES completion:nil];
    
}

- (void)goToSettings{
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    
    [self closeModal];
}

@end
