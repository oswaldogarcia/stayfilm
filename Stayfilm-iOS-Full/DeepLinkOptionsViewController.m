//
//  DeepLinkOptionsViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 2/7/19.
//  Copyright © 2019 Stayfilm. All rights reserved.
//

#import "DeepLinkOptionsViewController.h"

@interface DeepLinkOptionsViewController ()

@end

@implementation DeepLinkOptionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
- (IBAction)cancelAction:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
