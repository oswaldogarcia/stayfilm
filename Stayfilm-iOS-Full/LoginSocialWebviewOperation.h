//
//  LoginSocialWebviewOperation.h
//  Stayfilm-iOS-Full
//
//  Created by Henrique Ormonde on 18/09/17.
//  Copyright © 2017 Stayfilm. All rights reserved.
//
#ifndef Stayfilm_Full_LoginSocialWebViewOperation_h
#define Stayfilm_Full_LoginSocialWebViewOperation_h

#import <Foundation/Foundation.h>
#import "StayfilmApp.h"
#import "LoginToSocialNetworkOperation.h"
//#import "LoginWebView.h"

@protocol LoginSocialWebviewOperationDelegate;

@interface LoginSocialWebviewOperation : NSOperation

@property (nonatomic, assign) id <LoginSocialWebviewOperationDelegate> delegate;
@property (nonatomic, weak) StayfilmApp *sfAppLogin;
@property (nonatomic, strong) NSString *loginResultToken;
@property (nonatomic, assign) NSString *loginSocialNetwork;
@property (nonatomic, assign) NSInteger messageCode;

-(id)initWithDelegate:(id<LoginSocialWebviewOperationDelegate>)p_delegate andSFApp:(StayfilmApp *)p_sfApp forSocialNetwork:(NSString *)p_socialNetwork;

@end


@protocol LoginSocialWebviewOperationDelegate <NSObject>
- (void)loginSocialWebviewDidSucceed:(NSString *)token;
- (void)loginSocialWebviewDidFail:(NSString *)error;
@end

#endif
