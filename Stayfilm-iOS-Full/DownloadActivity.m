//
//  DownloadActivity.m
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 16/05/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import "DownloadActivity.h"

@implementation DownloadActivity


- (void)prepareWithActivityItems:(NSArray *)activityItems {
    [super prepareWithActivityItems:activityItems];
    
    
    
}

+ (UIActivityCategory)activityCategory {
    return UIActivityCategoryAction;
}

- (NSString *)activityTitle {
    return NSLocalizedString(@"DOWNLOAD", nil);
}
- (UIActivityType)activityType{
    return @"DownloadActivity";
}

- (UIImage *)activityImage {
    
    return [UIImage imageNamed:@"iconDownload"];
    
}
- (UIImage *)activitySettingsImage {
    return [UIImage imageNamed:@"iconDownload"];
}
- (BOOL)canPerformWithActivityItems:(NSArray *)activityItems {
    return YES;
}

- (void)               video: (NSString *) videoPath
    didFinishSavingWithError: (NSError *) error
                 contextInfo: (void *) contextInfo
{
    
    
//    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ALREADY_DOWNLOADED", nil) message:nil preferredStyle:UIAlertControllerStyleAlert];
//    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:nil];
//
//    [alert addAction:defaultAction];
    [self activityDidFinish:YES];
    [self.view.view makeToast:NSLocalizedString(@"DOWNLOADED", nil)];
//    [self.view presentViewController:alert animated:YES completion:nil];
    
}


- (void)performActivity {
    
    //Getting the path of the document directory
    NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
     NSURL *dataPath = [documentsDirectoryURL URLByAppendingPathComponent:@"StayFilms"];
     
     //Create Stayfilm folder if it does not exist
     NSError *error;
     if (![[NSFileManager defaultManager] fileExistsAtPath:[dataPath path]])
     {
     [[NSFileManager defaultManager] createDirectoryAtPath:[dataPath path] withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
     }
     
    NSURL *fullURL = [dataPath URLByAppendingPathComponent:[NSString stringWithFormat:@"%@.mp4",self.movie.idMovie]];
    
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:[fullURL path]])
    {
        self.progressView = [[ProgressBarViewController alloc] init];
        self.progressView.downloadProgressBar.progress = 0;
        self.progressView.lbl_downloading.text = NSLocalizedString(@"DOWNLOADING", nil);
        self.progressView.delegate = self;
        if (self.profileView != nil){
            
            self.progressView.transitioningDelegate = self.profileView;
            self.progressView.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            self.progressView.view.frame = [UIScreen mainScreen].bounds;
            self.profileView.transitionAnimation.duration = 0.1;
            self.profileView.transitionAnimation.isPresenting = YES;
            self.profileView.transitionAnimation.originFrame = [UIScreen mainScreen].bounds;
            
            [self.profileView presentViewController:self.progressView animated:YES completion:nil];
            
        }else{
            self.progressView.transitioningDelegate = self.videoFinishView;
            self.progressView.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            self.progressView.view.frame = [UIScreen mainScreen].bounds;
            self.videoFinishView.transitionAnimation.duration = 0.1;
            self.videoFinishView.transitionAnimation.isPresenting = YES;
            self.videoFinishView.transitionAnimation.originFrame = [UIScreen mainScreen].bounds;
            
            [self.videoFinishView presentViewController:self.progressView animated:YES completion:nil];
        }
        
        
        
        [self downloadVideoFromURL:self.movie.videoUrl withProgress:^(CGFloat progress) {
            
            runOnMainQueueWithoutDeadlocking(^{
                
                //NSLog(@"%f", progress * 100);
                self.progressView.progressLabel.text = [NSString stringWithFormat:@"%2.0f%%", (progress * 100)];
                self.progressView.downloadProgressBar.progress = progress ;
                
                if (progress >= 1.0){
                    self.progressView.lbl_downloading.text = NSLocalizedString(@"DOWNLOADED", nil);
                    [self.progressView dismissViewControllerAnimated:YES completion:nil];
                    
                }
            });
            
        } completion:^(NSURL *filePath) {
            
            
            if(UIVideoAtPathIsCompatibleWithSavedPhotosAlbum([filePath path])) {
                UISaveVideoAtPathToSavedPhotosAlbum([filePath path],self,@selector(video:didFinishSavingWithError:contextInfo:),nil);
            }
            
        } onError:^(NSError *error) {
            NSLog(@"StayLog: ERROR: %@", error);
        }];
        
    }else{
       
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ALREADY_DOWNLOADED", nil) message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:nil];
        
        [alert addAction:defaultAction];
        
        
        [self.view presentViewController:alert animated:YES completion:nil];
        
    }
    
}

- (void)chooseOption:(NSString *)option{
    
    if ([option isEqualToString:@"cancelDownload"]){
        
         [self.downloadTask cancel];
        
        [self.manager.operationQueue cancelAllOperations];
        
        [self removeVideoAtPath:self.path];
    }
    
}

- (void) downloadVideoFromURL: (NSString *) URL withProgress:(void (^)(CGFloat progress))progressBlock completion:(void (^)(NSURL *filePath))completionBlock onError:(void (^)(NSError *error))errorBlock
{
    //Configuring the session manager
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    self.manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    //Most URLs I come across are in string format so to convert them into an NSURL and then instantiate the actual request
    NSURL *formattedURL = [NSURL URLWithString:URL];
    NSURLRequest *request = [NSURLRequest requestWithURL:formattedURL];
    
    //Watch the manager to see how much of the file it's downloaded
    [self.manager setDownloadTaskDidWriteDataBlock:^(NSURLSession *session, NSURLSessionDownloadTask *downloadTask, int64_t bytesWritten, int64_t totalBytesWritten, int64_t totalBytesExpectedToWrite) {
        //Convert totalBytesWritten and totalBytesExpectedToWrite into floats so that percentageCompleted doesn't get rounded to the nearest integer
        CGFloat written = totalBytesWritten;
        CGFloat total = totalBytesExpectedToWrite;
        CGFloat percentageCompleted = written/total;
        
        //Return the completed progress so we can display it somewhere else in app
        progressBlock(percentageCompleted);
    }];
    
    //Start the download
    self.downloadTask = [self.manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        
        //Getting the path of the document directory
        NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
         NSURL *dataPath = [documentsDirectoryURL URLByAppendingPathComponent:@"StayFilms"];
        
        //Create Stayfilm folder if it does not exist
         NSError *error;
         if (![[NSFileManager defaultManager] fileExistsAtPath:[dataPath path]])
         {
         [[NSFileManager defaultManager] createDirectoryAtPath:[dataPath path] withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
         }
        
        NSURL *fullURL = [dataPath URLByAppendingPathComponent:[NSString stringWithFormat:@"%@.mp4",self.movie.idMovie]];
        
        self.path = fullURL;
        
        //If we already have a video file saved, remove it from the phone
        //[self removeVideoAtPath:fullURL];
        return fullURL;
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        if (!error) {
            //If there's no error, return the completion block
            completionBlock(filePath);
        } else {
            //Otherwise return the error block
            errorBlock(error);
        }
        
    }];
    
    [self.downloadTask resume];
}


- (void)removeVideoAtPath:(NSURL *)filePath
{
    NSString *stringPath = filePath.path;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:stringPath]) {
        [fileManager removeItemAtPath:stringPath error:NULL];
    }
}

@end
