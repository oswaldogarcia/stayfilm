//
//  Popover_ConfirmationMoreOptions_ViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Henrique Ormonde on 08/11/17.
//  Copyright © 2017 Stayfilm. All rights reserved.
//

#import "Popover_ConfirmationMoreOptions_ViewController.h"

@interface Popover_ConfirmationMoreOptions_ViewController ()

@end

@implementation Popover_ConfirmationMoreOptions_ViewController

@synthesize but_addMore, but_reorder, but_remove, but_cancel;

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    
}
-(id)initWithDelegate:(id<Popover_ConfirmationMoreOptions_ViewControllerDelegate>)p_delegate
{
    if(self = [super init]) {
        self.delegate = p_delegate;
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)backgroundTapped:(UIGestureRecognizer *)gesture
{
//    NSLog(@"popover %@",self.navigationController.viewControllers);
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)but_addMore_Clicked:(id)sender {
    [self.delegate performSelector:@selector(dismissModalMenu) withObject:nil];
    [self.delegate performSelector:@selector(decidedOption:) withObject:@"addMore"];
}
- (IBAction)but_reorder_Clicked:(id)sender {
    [self.delegate performSelector:@selector(dismissModalMenu) withObject:nil];
    [self.delegate performSelector:@selector(decidedOption:) withObject:@"reorder"];
}
- (IBAction)but_remove_Clicked:(id)sender {
    [self.delegate performSelector:@selector(dismissModalMenu) withObject:nil];
    [self.delegate performSelector:@selector(decidedOption:) withObject:@"remove"];
}
- (IBAction)but_cancel_Clicked:(id)sender {
    [self.delegate performSelector:@selector(dismissModalMenu) withObject:nil];
}

//not actually being used...
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:self.contentView]) {
        return NO;
    }
    return YES;
}


@end
