//
//  ProfileTableViewCell.m
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 23/04/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//
@import KALoader;

#import "ProfileTableViewCell.h"

@implementation ProfileTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
 
    [self.shareButton.layer setBorderWidth:1.5f];
    [self.shareButton.layer setCornerRadius:3.0f];
    [self.shareButton.layer setBorderColor:[UIColor colorWithRed:99.0 / 255.0 green:192.0 / 255.0 blue:152.0 / 255.0 alpha:1.0].CGColor];
    
    [self.shareButton setClipsToBounds:YES];
    
    
    
}
- (void)initCell{
    
    self.shareLabel.text = NSLocalizedString(@"SHARE", nil);
    
    self.movieTitleLabel.text = self.movie.title;
    
    [self.thumbnailImage showLoader];
    
    [self.thumbnailImage sd_setImageWithURL:[NSURL URLWithString: self.movie.thumbnailUrl] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {

        if([self.thumbnailImage isLoaderShowing]){
            [self.thumbnailImage hideLoader];
        }

        [self.thumbnailImage setImage:image];
    }];
    
    //[self.thumbnailImage sd_setImageWithURL:[NSURL URLWithString: self.movie.thumbnailUrl]
                    //     placeholderImage:[UIImage imageNamed:@"Select_VideoGradient"]];
    

    
    self.publishedTimeLabel.text = self.movie.prettyPublicated;
    
    switch ((int)self.movie.permission) {
        
        case 1:
            [self.permissionsImage setImage:[UIImage imageNamed:@"Privacy_Private"]];
            break;
        case 2:
        case 7:
            [self.permissionsImage setImage:[UIImage imageNamed:@"Privacy_Link"]];
            break;
        case 3:
            [self.permissionsImage setImage:[UIImage imageNamed:@"Privacy_Public"]];
            break;
            
        default:
        [self.permissionsImage setImage:[UIImage imageNamed:@"Privacy_Public"]];
            break;
    }
    
    Genres *genreMovie;
    StayfilmApp *sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    for(Genres* genre in sfApp.sfConfig.genres){
        if (genre.idgenre  == [self.movie.idGenre integerValue]){
            genreMovie = genre;
        }
    }
    
    if([genreMovie.config.campaign.idcustomer integerValue] == 37)/*[self.movie.idGenre integerValue] == 54*/{
        
        [self.logoImage setHidden:NO];
    }else{
        [self.logoImage setHidden:YES];
    }
        
    
  
}

- (IBAction)playMovieAction:(id)sender {
    
    [self.delegate didTapPlay:self.tag];
    
}


- (IBAction)shareAction:(id)sender {
    
    [self.delegate didSelectShare:self.tag];
    
}

- (IBAction)optionsAction:(id)sender {
    
    [self.delegate didSelectOptions:self.tag];
}

- (IBAction)privacyAction:(id)sender {
    
    [self.delegate didTapPrivacy:self.tag];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



@end
