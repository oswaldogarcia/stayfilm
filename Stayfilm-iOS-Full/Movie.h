//
//  Movie.h
//  Stayfilm for Messenger
//
//  Created by Henrique Ormonde on 11/05/15.
//  Copyright (c) 2015 Stayfilm. All rights reserved.
//

#ifndef Stayfilm_Full_Movie_h
#define Stayfilm_Full_Movie_h

#import <Foundation/Foundation.h>
#import "User.h"
#import "Utilities.h"

//@class AppDelegate;

@interface Movie : NSObject
//{
//    AppDelegate *appDelegate;
//}

@property (nonatomic, strong) NSString *idMovie;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *synopsis;
@property (nonatomic, strong) NSString *created;
@property (nonatomic, strong) NSString *updated;
@property (nonatomic, strong) NSString *publicated;
@property (nonatomic, strong) NSString *prettyCreated;
@property (nonatomic, strong) NSString *prettyPublicated;
@property (nonatomic, strong) NSNumber *status;
@property (nonatomic, assign) StayfilmTypes_MoviePermission permission;
//@property (nonatomic, assign) StayfilmTypes_MovieStatus *status;
//@property (nonatomic, assign) StayfilmTypes_MoviePermission *permission;
@property (nonatomic, strong) NSNumber *idGenre;
@property (nonatomic, strong) NSNumber *idTheme;
@property (nonatomic, strong) NSNumber *idTemplate;
@property (nonatomic, strong) NSNumber *likeCount;
@property (nonatomic, strong) NSNumber *shareCount;
@property (nonatomic, strong) NSNumber *viewCount;
@property (nonatomic, strong) NSNumber *commentCount;
@property BOOL liked;
@property (nonatomic, strong) NSString *shareComment;
@property (nonatomic, strong) NSString *idUser;
@property (nonatomic, strong) User *user;
@property (nonatomic, strong) User *sharer;
@property BOOL shared;
@property (nonatomic, strong) NSString *baseUrl;
@property (nonatomic, strong) NSNumber *bestof;
@property (nonatomic, strong) NSString *thumbnailUrl;
@property (nonatomic, strong) NSString *videoUrl;

-(id)init;
//- (void) setBaseUrl:(NSString*) new_value;
+ (id)customClassWithProperties:(NSDictionary *)properties;
- (id)initWithProperties:(NSDictionary *)properties;
+(Movie *)getMovieWithID:(NSString*)p_idMovie;
-(BOOL)watchedPercentage:(NSString*)p_percent;
-(BOOL)publishMovie;
-(BOOL)publishMovieWithDescription:(NSString*)synopsis;
-(BOOL)shareMovieOnFacebook;
-(BOOL)shareMovieOnTwitterWithMessage:(NSString*)p_message;
-(BOOL)downloadedMovie;
-(BOOL)deleteMovie;
-(BOOL)changePermisssionMovie;
@end

#endif
