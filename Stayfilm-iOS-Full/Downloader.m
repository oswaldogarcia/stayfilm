//
//  Downloader.m
//  Stayfilm for Messenger
//
//  Created by Henrique Ormonde on 10/11/15.
//  Copyright © 2015 Stayfilm. All rights reserved.
//

#import "Downloader.h"
//#import "StayfilmApp.h"

@interface Downloader ()

@property (atomic, strong) NSMutableDictionary *listFailedMediasAttempts;
@property (nonatomic, assign) int failedAttempts;

@property (atomic, strong) NSMutableArray *listNotToSaveInPhotos;

@end

@implementation Downloader

@synthesize listDownloadMedias, listDownloadedMedias, listDownloadingMedias, listNotToSaveInPhotos, isDownloading, downloadQueue;

static Downloader *singletonDownloaderObject = nil;

- (id)init
{
    if(self = [super init])
    {
        self.listFailedMediasAttempts = [[NSMutableDictionary alloc] init];
        self.listDownloadMedias = [[NSMutableArray alloc] init];
        self.listDownloadedMedias = [[NSMutableDictionary alloc] init];
        self.listDownloadingMedias = [[NSMutableDictionary alloc] init];
        self.listNotToSaveInPhotos = [[NSMutableArray alloc] init];
        self.isDownloading = NO;
        //        self.maxErrorAttemptsReached = NO;
        self.downloadQueue = [NSOperationQueue new];
        self.downloadQueue.name = @"Stayfilm Download Process Queue";
        self.downloadQueue.maxConcurrentOperationCount = 6;
        self.downloadQueue.qualityOfService = NSOperationQualityOfServiceUserInitiated;
        self.failedAttempts = 0;
    }
    return self;
}

+ (Downloader *) sharedDownloaderStayfilmSingletonWithDelegate:(id<StayfilmDownloaderDelegate>)theDelegate {
    
    @synchronized(self)
    {
        if(singletonDownloaderObject == nil)
        {
            singletonDownloaderObject = [[self alloc] init];
        }
        singletonDownloaderObject.delegate = theDelegate;
    }
    return singletonDownloaderObject;
}
+ (Downloader *) sharedDownloaderStayfilmSingleton {
    @synchronized(self)
    {
        if(singletonDownloaderObject == nil)
        {
            singletonDownloaderObject = [[self alloc] init];
        }
    }
    return singletonDownloaderObject;
}

- (BOOL)addMovie:(Movie *)p_movie {
        if(![self.listDownloadMedias containsObject:p_movie])
        {
            [self.listDownloadMedias addObject:p_movie];
            [self startOperations];
            return YES;
        }
        else
        {
            for (NSString *objKey in self.listDownloadedMedias) {
                Movie *value = [self.listDownloadedMedias objectForKey:objKey];
                if([value.idMovie isEqualToString:p_movie.idMovie]) {
                    if(UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(objKey)) {
//                        UISaveVideoAtPathToSavedPhotosAlbum(objKey,nil,nil,nil);
                        NSLog(@"StayLog: Already downloaded and now saved to Camera Roll");
                        
                        [(NSObject *)self.delegate performSelectorOnMainThread:@selector(alreadyDownloadedMedia:) withObject:p_movie waitUntilDone:YES];
                        return YES;
                    }
                }
            }
            return NO;
        }
}

- (BOOL)addMovieWithoutSavingToPhotosAlbum:(Movie *)p_movie {
    if(![self.listDownloadMedias containsObject:p_movie])
    {
        [self.listDownloadMedias addObject:p_movie];
        [self.listNotToSaveInPhotos addObject:p_movie];
        [self startOperations];
        return YES;
    }
    else
    {
        for (NSString *objKey in self.listDownloadedMedias) {
            Movie *value = [self.listDownloadedMedias objectForKey:objKey];
            if([value.idMovie isEqualToString:p_movie.idMovie]) {
                [(NSObject *)self.delegate performSelectorOnMainThread:@selector(alreadyDownloadedMedia:) withObject:p_movie waitUntilDone:YES];
                return YES;
            }
        }
        return NO;
    }
}

- (int)addMedias:(NSArray *)p_medias {
    int added = 0;
    int count = 0;
    
    for (id object in p_medias) {
        if(![self.listDownloadMedias containsObject:object])
        {
            [self.listDownloadMedias addObject:object];
            ++added;
        }
        else
        {
            ++count;
        }
    }
    if(added)
    {
        [self startOperations];
    }
    
    return count;
}

- (NSString *)getPathForDownloadedMovie:(Movie *)p_movie {
    
    for (NSString* key in self.listDownloadedMedias) {
        Movie* movie = [self.listDownloadedMedias objectForKey:key];
        if ([movie.idMovie isEqualToString:p_movie.idMovie]) {
            return key;
        }
    }
    return nil;
}

- (BOOL)isMovieAlreadyDownloaded:(Movie *)p_movie {
    BOOL found = NO;
    for (NSString* key in self.listDownloadedMedias) {
        Movie* movie = [self.listDownloadedMedias objectForKey:key];
        if ([movie.idMovie isEqualToString:p_movie.idMovie]) {
            found = YES;
            break;
        }
    }
    return found;
}

- (BOOL)isMovieDownloading:(Movie *)p_movie {
    BOOL found = NO;
    for (NSString* key in self.listDownloadingMedias) {
        Movie* movie = [self.listDownloadingMedias objectForKey:key];
        if ([movie.idMovie isEqualToString:p_movie.idMovie]) {
            found = YES;
            break;
        }
    }
    return found;
}

- (void)startOperations{
    @synchronized(self) {
        runOnMainQueueWithoutDeadlocking(^{
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        });
        @try {
            BOOL somethingWrong = NO;
            self.isDownloading = YES;
            
            for (Movie *object in self.listDownloadMedias)
            {
                BOOL found = NO;
                for (NSString* key in self.listDownloadedMedias) {
                    Movie *value = [self.listDownloadedMedias objectForKey:key];
                    if([value.idMovie isEqualToString:object.idMovie])
                    {
                        found = YES;
                        break;
                    }
                    else { // lets check if it is already downloading
                        for (NSString* keyDownloading in self.listDownloadingMedias) {
                            Movie *valueDownloading = [self.listDownloadingMedias objectForKey:keyDownloading];
                            if([valueDownloading.idMovie isEqualToString:object.idMovie]) {
                                found = YES;
                                break;
                            }
                        }
                    }
                }
                if(found)
                {
                    continue;
                }
                else // file not downloaded yet
                {
                    if([object isKindOfClass:[Movie class]]) {
                       VideoDownloadOperation *downloader = [[VideoDownloadOperation alloc] initWithMovie:object withBackgroundSessionIdentifier:object.videoUrl savingToPhotosFolder:YES delegate:self];
                        
                        //comment to save alway on album
                        /*VideoDownloadOperation *downloader = nil;
                        BOOL saveInPhotos = YES;
                        for (Movie* mov in self.listNotToSaveInPhotos) {
                            if([mov.idMovie isEqualToString:object.idMovie]) {
                                saveInPhotos = NO;
                                break;
                            }
                        }
                        if(saveInPhotos)
                        {
                            downloader = [[VideoDownloadOperation alloc] initWithMovie:object withBackgroundSessionIdentifier:object.videoUrl savingToPhotosFolder:YES delegate:self];
                        } else {
                            downloader = [[VideoDownloadOperation alloc] initWithMovie:object withBackgroundSessionIdentifier:object.videoUrl savingToPhotosFolder:NO delegate:self];
                        }*/
                        
                        [self.listDownloadingMedias setObject:object forKey:object.idMovie];
                        [self.downloadQueue addOperation:downloader];
                    }
                    else {
                        NSLog(@"StyaLog: Downloader Media Asset Type is not a MOVIE");
                    }
                }
                if(somethingWrong)
                {
                    NSLog(@"StayLog: Next exception is part of the workflow. (startOperations)");
                    [NSException raise:@"Something Wrong" format:@"upload interrupted."];
                }
            }
        }
        @catch (NSException *exception) {
            NSLog(@"StayLog: EXCEPTION in StartOperations Downloader with error: %@", exception.description);
            self.isDownloading = NO;
            [self.downloadQueue cancelAllOperations];
            ++self.failedAttempts;
            if(self.failedAttempts <= 2)
            {
                [self startOperations];
            }
            else
            {
                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                [self resetDownload];
                [(NSObject *)self.delegate performSelectorOnMainThread:@selector(downloadDidFail:) withObject:self waitUntilDone:NO];
            }
        }
    }
    [(NSObject *)self.delegate performSelectorOnMainThread:@selector(downloadDidStart) withObject:nil waitUntilDone:NO];
}

- (BOOL)resetDownload {
    @synchronized(self) {
        [self.downloadQueue cancelAllOperations];
        self.listDownloadMedias = nil;
        self.listDownloadedMedias = nil;
        self.listFailedMediasAttempts = nil;
        self.downloadQueue = nil;
        
        self.listFailedMediasAttempts = [[NSMutableDictionary alloc] init];
        self.listDownloadMedias = [[NSMutableArray alloc] init];
        self.listDownloadedMedias = [[NSMutableDictionary alloc] init];
        self.listDownloadingMedias = [[NSMutableDictionary alloc] init];
        self.listNotToSaveInPhotos = [[NSMutableArray alloc] init];
        self.isDownloading = NO;
        //    self.maxErrorAttemptsReached = NO;
        self.downloadQueue = [NSOperationQueue new];
        self.downloadQueue.name = @"Stayfilm Download Process Queue";
        self.downloadQueue.maxConcurrentOperationCount = 6;
        self.downloadQueue.qualityOfService = NSOperationQualityOfServiceUserInitiated;
        self.failedAttempts = 0;
        return YES;
    }
}

- (BOOL)isDownloadFinished {
    return (([self.downloadQueue operationCount] == 0) && !self.isDownloading);
}

- (int)getTotalDownloadPercentage
{
    return (int)(self.listDownloadedMedias.count * 100 / self.listDownloadMedias.count);
}

- (int)getDownloadPercentageForIDMovie:(NSString *)p_movieID {
    for (VideoDownloadOperation* op in self.downloadQueue.operations) {
        if([op.movie.idMovie isEqualToString:p_movieID]) {
            return op.progress;
        }
    }
    return 0.0f;
}

-(void)updateOverallProgress
{
    NSNumber *progress = [[NSNumber alloc] initWithFloat:((float)self.listDownloadedMedias.count / (float)self.listDownloadMedias.count)];
    [(NSObject *)self.delegate performSelectorOnMainThread:@selector(downloadOverallProgressChanged:) withObject:progress waitUntilDone:NO];
    if([progress floatValue] == 1.0) {
        [(NSObject *)self.delegate performSelectorOnMainThread:@selector(downloadDidFinish:) withObject:self waitUntilDone:NO];
    }
}


#pragma mark - URL Delegates

- (void)URLSessionDidFinishEventsForBackgroundURLSession:(NSURLSession *)session
{
    if (self.backgroundSessionCompletionHandler) {
        self.backgroundSessionCompletionHandler();
        self.backgroundSessionCompletionHandler = nil;
    }
}


#pragma mark - Operations Callback

-(void) downloadDidCancel:(VideoDownloadOperation *)downloader
{
    [(NSObject *)self.delegate performSelectorOnMainThread:@selector(downloadDidCancel:) withObject:downloader.movie.idMovie waitUntilDone:NO];
}

-(void) downloadDidFail:(VideoDownloadOperation *)downloader
{
    @try {
        NSNumber *attempts;
        if(self.listFailedMediasAttempts[downloader.movie.idMovie] != nil)
        {
            attempts = [NSNumber numberWithInt:(int)self.listFailedMediasAttempts[downloader.movie.idMovie]];
        }
        else
        {
            attempts = [[NSNumber alloc] initWithInt:0];
        }
        int value = [attempts intValue];
        attempts = [NSNumber numberWithInt:value + 1];
        
        [self.listFailedMediasAttempts setObject:attempts forKey:downloader.movie.idMovie];
        
        if([attempts intValue] <= 2)
        {
            VideoDownloadOperation *download = nil;
            BOOL saveInPhotos = YES;
            for (Movie* mov in self.listNotToSaveInPhotos) {
                if([mov.idMovie isEqualToString:downloader.movie.idMovie]) {
                    saveInPhotos = NO;
                    break;
                }
            }
            if(saveInPhotos)
            {
                download = [[VideoDownloadOperation alloc] initWithMovie:downloader.movie withBackgroundSessionIdentifier:[downloader.movie.videoUrl stringByAppendingString:[[NSString alloc] initWithFormat:@"%d",[attempts intValue]]] savingToPhotosFolder:YES delegate:self];
            } else {
                download = [[VideoDownloadOperation alloc] initWithMovie:downloader.movie withBackgroundSessionIdentifier:[downloader.movie.videoUrl stringByAppendingString:[[NSString alloc] initWithFormat:@"%d",[attempts intValue]]] savingToPhotosFolder:NO delegate:self];
            }
            [self.downloadQueue addOperation:download];
        }
        else // media failed 3 times, so lets skip it
        {
            @synchronized(self) {
                if(self.listDownloadingMedias[downloader.movie.idMovie] != nil)
                {
                    [self.listDownloadMedias removeObject:self.listDownloadingMedias[downloader.movie.idMovie]];
                    [self.listDownloadingMedias removeObjectForKey:downloader.movie.idMovie];
                }
                for (Movie* mov in self.listNotToSaveInPhotos) {
                    if([mov.idMovie isEqualToString:downloader.movie.idMovie]) {
                        [self.listNotToSaveInPhotos removeObject:mov];
                        break;
                    }
                }
            }
            [(NSObject *)self.delegate performSelectorOnMainThread:@selector(downloadDidFailMedia:) withObject:downloader.movie.idMovie waitUntilDone:NO];
        }
        
    }
    @catch (NSException *exception) {
        NSLog(@"StayLog: EXCEPTION in converterDidFail with error: %@", exception.description);
        @synchronized(self) {
            if(self.listDownloadingMedias[downloader.movie.idMovie] != nil)
            {
                [self.listDownloadMedias removeObject:self.listDownloadingMedias[downloader.movie.idMovie]];
                [self.listDownloadingMedias removeObjectForKey:downloader.movie.idMovie];
            }
            for (Movie* mov in self.listNotToSaveInPhotos) {
                if([mov.idMovie isEqualToString:downloader.movie.idMovie]) {
                    [self.listNotToSaveInPhotos removeObject:mov];
                    break;
                }
            }
        }
        [(NSObject *)self.delegate performSelectorOnMainThread:@selector(downloadDidFailMedia:) withObject:downloader.movie.idMovie waitUntilDone:NO];
    }
}

-(void) downloadDidFinish:(VideoDownloadOperation *)downloader
{
    @synchronized(self)
    {
        if(self.listDownloadingMedias[downloader.movie.idMovie] != nil) {
            [self.listDownloadedMedias setObject:self.listDownloadingMedias[downloader.movie.idMovie] forKey:downloader.pathFile];
            if(self.delegate != nil)
                [(NSObject *)self.delegate performSelectorOnMainThread:@selector(downloadFinishMedia:) withObject:self.listDownloadingMedias[downloader.movie.idMovie] waitUntilDone:NO];
            [self.listDownloadingMedias removeObjectForKey:downloader.movie.idMovie];
            
            for (Movie* mov in self.listNotToSaveInPhotos) {
                if([mov.idMovie isEqualToString:downloader.movie.idMovie]) {
                    [self.listNotToSaveInPhotos removeObject:mov];
                    break;
                }
            }
            
            [self updateOverallProgress];
            StayfilmApp *sfApp = [StayfilmApp sharedStayfilmAppSingleton];
            [sfApp cleanFilmFolder];
        }
    }
    NSLog(@"StayLog: Media downloaded successfuly: %@", downloader.movie.idMovie);
}

- (void)downloadProgressChanged:(VideoDownloadOperation *)downloader
{
    NSArray *movie_Progress = @[ downloader.movie, [NSNumber numberWithFloat:downloader.progress] ];
    [(NSObject *)self.delegate performSelectorOnMainThread:@selector(downloadProgressChanged:) withObject:movie_Progress waitUntilDone:NO];
}


@end
