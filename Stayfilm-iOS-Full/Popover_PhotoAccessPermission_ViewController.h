//
//  Popover_PhotoAccessPermission_ViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Henrique on 3/20/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol Popover_PhotoPermissionAccess_ViewControllerDelegate <NSObject>
@required
-(void)dismissModalMenu;
@end

@interface Popover_PhotoAccessPermission_ViewController : UIViewController <UIActionSheetDelegate, UIGestureRecognizerDelegate, Popover_PhotoPermissionAccess_ViewControllerDelegate>

@property(nonatomic, weak) id<Popover_PhotoPermissionAccess_ViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIButton *but_GoSettings;
@property (weak, nonatomic) IBOutlet UIButton *but_cancel;

@property (strong, nonatomic) IBOutlet UIView *blur_view;
@property (weak, nonatomic) IBOutlet UIView *contentView;

-(id)initWithDelegate:(id<Popover_PhotoPermissionAccess_ViewControllerDelegate>)p_delegate;
@end
