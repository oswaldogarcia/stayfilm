//
//  ConditionsViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 5/3/19.
//  Copyright © 2019 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SubscriptionConditionsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *conditionsLabel;
@property (weak, nonatomic) IBOutlet UILabel *conditionTitleLabel;
    
@end

NS_ASSUME_NONNULL_END
