//
//  LoginIntroViewController.m
//  Stayfilm for Messenger
//
//  Created by Henrique Ormonde on 29/04/15.
//  Copyright (c) 2015 Stayfilm. All rights reserved.
//

@import AVKit;
@import AVFoundation;

#import "LoginIntroViewController.h"
#import "Reachability.h"


@interface LoginIntroViewController ()

@property (nonatomic, assign) int attempts;
@property (nonatomic, assign) int loginAttempts;
@property (nonatomic, weak) StayfilmApp *sfAppLogin;

@property (nonatomic, assign) BOOL isMovingToStep1;
@property (nonatomic, assign) BOOL didComeBackFromFBLogin;
@property (nonatomic, assign) BOOL isStoriesLoaded;
@property (nonatomic, assign) BOOL doOpenStories;

@property (nonatomic, strong) NSTimer *timerChecker;

@end

@implementation LoginIntroViewController

@synthesize imageBackground, imageBackgroundText, spinnerLoader, loginEmailButton, but_SignUp, sfAppLogin, isStoriesLoaded, doOpenStories;
    

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

-(void)viewDidLoad
{
    [super viewDidLoad];

    self.attempts = 0;
    self.loginAttempts = 0;
    self.isMovingToStep1 = NO;
    self.isStoriesLoaded = NO;
    self.doOpenStories = NO;
    
    NSLog( @"### running FB SDK version: %@", [FBSDKSettings sdkVersion] );
    
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.shadowImage = [[UIImage alloc] init];
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationItem.backBarButtonItem setTitle:@""];

//    self.loginButton.delegate = self;
//    self.loginButton.readPermissions = @[@"public_profile", @"email", @"user_photos", @"user_videos"];
    
    UIInterpolatingMotionEffect *centerX = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.x" type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
    
    centerX.maximumRelativeValue = @75;
    centerX.minimumRelativeValue = @-75;
    
    UIInterpolatingMotionEffect *centerY = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.y" type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
    
    centerY.maximumRelativeValue = @50;
    centerY.minimumRelativeValue = @-50;
    
    UIMotionEffectGroup *group = [[UIMotionEffectGroup alloc] init];
    group.motionEffects = @[centerX, centerY];
    
    [self.imageBackground addMotionEffect:group];
    
//    self.loginButton.layer.cornerRadius = 5;
//    self.loginButton.layer.masksToBounds = YES;
    self.but_SignUp.layer.cornerRadius = 5;
    self.but_SignUp.layer.masksToBounds = YES;
    
    [self.but_SignUp setHidden:YES];
    [self.loginEmailButton setHidden:YES];
    [self.but_Explore setHidden:YES];
    [self.spinnerLoader setHidden:NO];
    [self.spinnerLoader startAnimating];
//    [self.spinnerExplore startAnimating];
//    [self.spinnerExplore setHidden:YES];
    
    //TODO: change image accordingly to the language
    //NSString *language = [[[NSLocale preferredLanguages] objectAtIndex:0] substringToIndex:2];
    NSString *langcode = [[NSLocale currentLocale] objectForKey:NSLocaleLanguageCode];
    
    if([langcode isEqualToString:@"pt"])
    {
        self.imageBackgroundText.image = [UIImage imageNamed:@"Login_HEADLINE_TURN_Portuguese"];
    }
    else if([langcode isEqualToString:@"es"])
    {
        self.imageBackgroundText.image = [UIImage imageNamed:@"Login_HEADLINE_TURN_Spanish"];
    }
//    else if([langcode isEqualToString:@"fr"])
//    {
//        self.imageBackgroundText.image = [UIImage imageNamed:@"Intro_Text_fr"];
//    }
//    else if([langcode isEqualToString:@"it"])
//    {
//        self.imageBackgroundText.image = [UIImage imageNamed:@"Intro_Text_it"];
//    }
//    else if([langcode isEqualToString:@"tr"])
//    {
//        self.imageBackgroundText.image = [UIImage imageNamed:@"Intro_Text_tr"];
//    }
//    else if([langcode isEqualToString:@"zh"])
//    {
//        self.imageBackgroundText.image = [UIImage imageNamed:@"Intro_Text_zh"];
//    }
   
    
    self.didComeBackFromFBLogin = NO;

}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
//    [self.loginButton setHidden:YES];
//    [self.loginButton setNeedsDisplay];
    [self.spinnerExplore stopAnimating];
    [self.spinnerExplore setHidden:YES];
    
    [self prefersStatusBarHidden];
    [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    
    [self performSelectorInBackground:@selector(createStayfilmAppSingleton:) withObject:@1];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.spinnerExplore setHidden:YES];
    
    if(self.sfAppLogin.sfConfig.stories != nil && self.sfAppLogin.sfConfig.stories.count > 0)
        isStoriesLoaded = YES;
    
    // Unlock Audio Lock button
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    NSError *error = nil;
    BOOL result = NO;
    if ([audioSession respondsToSelector:@selector(setActive:withOptions:error:)]) {
        result = [audioSession setActive:YES withOptions:0 error:&error]; // iOS6+
    }
    if (!result && error) {
        // deal with the error
        NSLog(@"StayLog: AVAudioSession Error: %@", error);
    }
    
    error = nil;
    result = [audioSession setCategory:AVAudioSessionCategoryPlayback error:&error];
    
    if (!result && error) {
        // deal with the error
        NSLog(@"StayLog: AVAudioSession setCategory Error: %@", error);
    }

    if(self.sfAppLogin.sfConfig.stories != nil && self.sfAppLogin.sfConfig.stories.count > 0)
        isStoriesLoaded = YES;
    
    //Google Analytics
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"home"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

-(void)createStayfilmAppSingleton:(NSNumber *)state
{
    if([self connected]) {
        if(self.sfAppLogin == nil)
        {
            self.sfAppLogin = [StayfilmApp sharedStayfilmAppSingletonWithDelegate:self];
        }
    } else {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"NO_INTERNET_CONNECTION", nil)
                                                                       message:NSLocalizedString(@"NO_INTERNET_CONNECTION_MESSAGE", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        
        self.timerChecker = [NSTimer timerWithTimeInterval:2.0
                                                             target:self
                                                           selector:@selector(timerCheckerTick:)
                                                           userInfo:nil
                                                            repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:self.timerChecker forMode:NSDefaultRunLoopMode];
    }
}

-(BOOL)connected {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

-(void)connectivityChanged:(BOOL)isConnected {
    if(isConnected) {
        [self createStayfilmAppSingleton:@1];
    } else {
//        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"NO_INTERNET_CONNECTION", nil)
//                                                                       message:NSLocalizedString(@"NO_INTERNET_CONNECTION_MESSAGE", nil)
//                                                                preferredStyle:UIAlertControllerStyleAlert];
//        
//        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
//                                                              handler:^(UIAlertAction * action) {}];
//        
//        [alert addAction:defaultAction];
//        [self presentViewController:alert animated:YES completion:nil];
    }
}

-(void)timerCheckerTick:(NSTimer*)timer {
    if([self connected])
    {
        [timer invalidate];
        timer = nil;
        [self createStayfilmAppSingleton:@1];
    }
}

-(void)sfSingletonCreated
{
    @try {
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [self.loginButton setHidden:YES];
//        });
        
        if(self.sfAppLogin == nil)
            self.sfAppLogin = [StayfilmApp sharedStayfilmAppSingleton];
        self.sfAppLogin.storyState = [StoryState sharedStoryStateSingletonWithCreationDelegate:self];
        
        NSLog(@"StayLog: SingletonCreated sucessfully!");

        if(self.sfAppLogin.settings[@"idSession"] == nil || [self.sfAppLogin.settings[@"idSession"] isEqualToString:@""]) // not logged in
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self showFacebookLoginButton];
            });
            
            runOnMainQueueWithoutDeadlocking(^{
                [self.but_SignUp setHidden:NO];
                [self.loginEmailButton setHidden:NO];
                [self.but_Explore setHidden:NO];
                [self.spinnerLoader stopAnimating];
                [self.spinnerExplore setHidden:YES];
            });
            
        }
        else if(self.sfAppLogin.settings[@"idSession"] != nil && ![self.sfAppLogin.settings[@"idSession"] isEqualToString:@""]) // already logged in once
        {
            if(self.sfAppLogin.settings[@"LOGGEDIN_Email"] == nil) {  // if logged by email ignore facebook token
                if ([FBSDKAccessToken currentAccessToken]) {
                    // User is logged in facebook.
                    if(self.sfAppLogin.currentUser == nil)
                    {
                        if(self.sfAppLogin.settings[@"facebookToken"] == nil || [self.sfAppLogin.settings[@"facebookToken"] isEqualToString:@""])
                        {
                            if(!self.didComeBackFromFBLogin)
                            {
                                [FBSDKProfile setCurrentProfile:nil];
                                [FBSDKAccessToken setCurrentAccessToken:nil];
                                FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
                                [loginManager logOut];
                                [self showFacebookLoginButton];
                            }
                            return;
                        }
                        else
                        {
                            NSData *dataSaved = [[NSUserDefaults standardUserDefaults] objectForKey:@"currentUser"];
                            User *appUser = [NSKeyedUnarchiver unarchiveObjectWithData:dataSaved];
                            self.sfAppLogin.currentUser = appUser;
                            
                            //update user in background
                            /*__weak typeof(self) selfDelegate = self;
                            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^(void){
                                //                            LoginFacebookOperation *loginOp = [[LoginFacebookOperation alloc] initWithDelegate:nil andSFApp:selfDelegate.sfAppLogin];
                                //                            [selfDelegate.sfAppLogin.operationsQueue addOperation:loginOp];
                                User *newAppUser = [User loginWithFacebook:selfDelegate.sfAppLogin.settings[@"facebookToken"] andSFAppSingleton:selfDelegate.sfAppLogin];
                                if(newAppUser != nil)
                                    selfDelegate.sfAppLogin.currentUser = newAppUser;
                            });*/
                            [self moveToStep1];
                            return;
                        }
                    }
                }
            }
            
            // those without facebook login
            runOnMainQueueWithoutDeadlocking(^{
                NSData *dataSaved = [[NSUserDefaults standardUserDefaults] objectForKey:@"currentUser"];
                User *appUser = [NSKeyedUnarchiver unarchiveObjectWithData:dataSaved];
                self.sfAppLogin.currentUser = appUser;
                
                //update user in background
                __weak typeof(self) selfDelegate = self;
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^(void){
                    User *newAppUser = [User loginWithIdSession:selfDelegate.sfAppLogin.currentUser.idSession andSFAppSingleton:selfDelegate.sfAppLogin];
                    if(newAppUser != nil)
                        selfDelegate.sfAppLogin.currentUser = newAppUser;
                });
                
                if(self.sfAppLogin.currentUser != nil && self.sfAppLogin.currentUser.idSession != nil) {
                    //[self.sfAppLogin.settings setObject:@"1" forKey:@"LOGGEDIN_Email"];
                    [self moveToStep1];
                }
                else
                {
                    [self.but_SignUp setHidden:NO];
                    [self.loginEmailButton setHidden:NO];
                    [self.but_Explore setHidden:NO];
                    [self.spinnerLoader stopAnimating];
                    [self.spinnerExplore setHidden:YES];
                }
            });
        }
        
    }
    @catch (NSException *exception) {
        NSLog(@"StayLog: EXCEPTION in sfSingletonCreated with error: %@", exception.description);
        if(self.attempts >= 1)
        {
            self.attempts = 0;
            NSLog(@"StayLog: sfSingeltonCreated: %@", exception.description);
            dispatch_async(dispatch_get_main_queue(), ^{
                [self showFacebookLoginButton];
            });
            
            NSLog(@"StayLog: attempt %i", self.attempts);
            
//            //Google Analytics
//            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//            [tracker set:kGAIScreenName value:@"Iphone_Nativo_Tela_1_SFMobile_inicio"];
//            [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
        }
        else{
            ++self.attempts;
            [self viewDidAppear:YES];
        }
    }
}



- (void)storiesLoaded {
    isStoriesLoaded = YES;
    if(doOpenStories) {
        [self but_Explore_Click:nil];
    }
    runOnMainQueueWithoutDeadlocking(^{
        [self.spinnerExplore stopAnimating];
        [self.spinnerExplore setHidden:YES];
    });
}

-(void)exitProfile {
    runOnMainQueueWithoutDeadlocking(^{
        [self.but_Explore setHidden:NO];
        [self.but_SignUp setHidden:NO];
        [self.loginEmailButton setHidden:NO];
        [self.spinnerLoader stopAnimating];
        [self.spinnerExplore stopAnimating];
        [self.spinnerExplore setHidden:YES];
    });
    [self performSegueWithIdentifier:@"goToRegister" sender:self];
}


#pragma mark - Login Operations Delegate

-(void)loginFacebookDidFinish:(LoginFacebookOperation *)loginOP {
    // switch codemessage
    
    switch (loginOP.messageCode) {
        case LOGIN_SUCCESSFUL:
        {
            self.loginAttempts = 0;
            if(!self.isMovingToStep1) {
                self.isMovingToStep1 = YES;
                [self moveToStep1];
            }
        }
            break;
            
        default:
        {
            ++self.loginAttempts;
            [self.facebookLoginOPButton setHidden:NO];
            //[self.loginButton setHidden:NO];
            [self.spinnerLoader setHidden:YES];
            [self.spinnerLoader stopAnimating];
        }
            break;
    }
    
}

-(void)loginFacebookDidFail:(LoginFacebookOperation *)loginOP {
    
    ++self.loginAttempts;
    if(self.loginAttempts >= 3)
    {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"FACEBOOK_FALIED", nil)
                                                                       message:NSLocalizedString(@"FACEBOOK_FALIED_MESSAGE", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        self.loginAttempts = 0;
        return;
    }
    
    BOOL retryLogin = NO;
    switch (loginOP.messageCode) {
        case CONNECTION_ERROR:
            {
                retryLogin = YES;
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CONNECTION_ERROR", nil)
                                                                               message:NSLocalizedString(@"CONNECTION_ERROR_MESSAGE", nil)
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {}];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
            break;
        case USER_CANCELLED:
            {
                retryLogin = NO;
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"USER_CANCELLED", nil)
                                                                               message:NSLocalizedString(@"USER_CANCELLED_MESSAGE", nil)
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {}];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
            break;
        case LOGIN_FAILED:
            {
                retryLogin = YES;
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"LOGIN_FAILED", nil)
                                                                               message:NSLocalizedString(@"LOGIN_FAILED_MESSAGE", nil)
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {}];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
            break;
        case REGISTRATION_ERROR:
            {
                retryLogin = YES;
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"REGISTRATION_ERROR", nil)
                                                                               message:NSLocalizedString(@"REGISTRATION_ERROR_MESSAGE", nil)
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {}];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
            break;
        case NO_CONFIG_LOADED:
            {
                retryLogin = YES;
                self.loginAttempts--;
                // say nothing, just try again
                if(self.sfAppLogin != nil)
                    self.sfAppLogin.sfConfig = [SFConfig getAllConfigsFromWS:self.sfAppLogin.wsConfig];
                else
                    self.sfAppLogin = [StayfilmApp sharedStayfilmAppSingletonWithDelegate:self];
                break;
            }
        case NO_INTERNET_CONNECTION:
            {
                retryLogin = NO;
                self.loginAttempts--;
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"NO_INTERNET_CONNECTION", nil)
                                                                               message:NSLocalizedString(@"NO_INTERNET_CONNECTION_MESSAGE", nil)
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {}];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
                
                self.timerChecker = [NSTimer timerWithTimeInterval:2.0 repeats:YES block:^(NSTimer * _Nonnull timer) {
                    if([self connected])
                    {
                        [timer invalidate];
                        timer = nil;
                        LoginFacebookOperation *loginOp = [[LoginFacebookOperation alloc] initWithDelegate:self andSFApp:self.sfAppLogin];
                        [self.sfAppLogin.operationsQueue addOperation:loginOp];
                    }
                }];
                [[NSRunLoop mainRunLoop] addTimer:self.timerChecker forMode:NSDefaultRunLoopMode];
                break;
            }
        case -1:
            {
                retryLogin = YES;
                NSLog(@"StayLog: loginDidFail with EXCEPTION (messageCode -1)");
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"LOGIN_FAILED", nil)
                                                                               message:NSLocalizedString(@"LOGIN_FAILED_MESSAGE", nil)
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {}];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
            break;
        default:
            {
                retryLogin = YES;
            }
            break;
    }
    
    if(retryLogin)
    {
        LoginFacebookOperation *logOperation = [[LoginFacebookOperation alloc] initWithDelegate:self andSFApp:self.sfAppLogin];
        [self.sfAppLogin.operationsQueue addOperation:logOperation];
    }
    else
    {
        [self.facebookLoginOPButton setHidden:NO];
//        [self.loginButton setHidden:NO];
        [self.spinnerLoader setHidden:YES];
        [self.spinnerLoader stopAnimating];
    }
    
    [loginOP cancel];
}


#pragma mark - Buttons Actions

- (IBAction)but_Explore_Click:(id)sender {
    if(!isStoriesLoaded) {
        doOpenStories = YES;
        runOnMainQueueWithoutDeadlocking(^{
            [self.spinnerExplore startAnimating];
            [self.spinnerExplore setHidden:NO];
        });
       return;
    }
    if(self.sfAppLogin.sfConfig.stories != nil && self.sfAppLogin.sfConfig.stories.count > 0)
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Explorer" bundle:[NSBundle mainBundle]];
        StorieViewController *storieView = [storyboard instantiateViewControllerWithIdentifier:@"explorerStories"];
        storieView.delegate = self;
        storieView.storiesArray = [self.sfAppLogin.sfConfig.stories copy];
        storieView.storySelected = 0;
        [self presentViewController:storieView animated:YES completion:nil];
        
    } else {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"SOMETHING_WRONG", nil)
                                                                       message:NSLocalizedString(@"PROBLEM_EXPLORE", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"user"
                                                          action:@"explore-films"
                                                           label:@""
                                                           value:@1] build]];
    
}

- (IBAction)loginEmailButton_Click:(id)sender {
    self.viewLogin.hidden = NO;
    
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"user"
                                                          action:@"login"
                                                           label:@""
                                                           value:@1] build]];
}
- (IBAction)signUp_Click:(id)sender {
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"user"
                                                          action:@"register"
                                                           label:@""
                                                           value:@1] build]];
}
- (IBAction)cancelLoginButton_Click:(id)sender {
    self.viewLogin.hidden = YES;
    [self.view endEditing:YES];
}

- (IBAction)okLoginButton_Click:(id)sender {
    [self.view endEditing:YES];
    if(_txt_username.text != nil && _txt_password.text != nil)
    {
        User *appUser = [User loginWithUsername:_txt_username.text withPassword:_txt_password.text andSFAppSingleton:self.sfAppLogin];
        if(appUser == nil)
        {
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CONNECTION_ERROR", nil)
                                                                           message:NSLocalizedString(@"CONNECTION_ERROR_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
            NSLog(@"StayLog: conn error 2");
            //exit(0);
            [self showFacebookLoginButton];
        }
        else if(!appUser.userExists && !appUser.isResponseError) //TODO register user
        {
            [self performSegueWithIdentifier:@"goToRegister" sender:self];
        }
        else if(!appUser.isLogged)
        {
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"LOGIN_FAILED", nil)
                                                                           message:NSLocalizedString(@"LOGIN_FAILED_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
            NSLog(@"StayLog: conn error 3");
            //exit(0);
            [self showFacebookLoginButton];
            
            if(self.sfAppLogin.settings == nil)
            {
                self.sfAppLogin.settings = [[NSMutableDictionary alloc] init];
            }
            // check Login   0 = not started  1 = true   2 = false
            [self.sfAppLogin.settings setObject:@"0" forKey:@"CHECK_Login"];
            [self.sfAppLogin saveSettings];
        }
        else //user logged in successfully
        {
            if(self.sfAppLogin.settings == nil)
            {
                self.sfAppLogin.settings = [[NSMutableDictionary alloc] init];
            }
            [self.sfAppLogin.settings setObject:@"1" forKey:@"CHECK_Login"];
            [self.sfAppLogin.settings setObject:@"1" forKey:@"LOGGEDIN_Email"];
            [self.sfAppLogin.settings setObject:appUser.idSession forKey:@"idSession"];
            [self.sfAppLogin.settings setObject:appUser.idUser forKey:@"idUser"];
            [self.spinnerLoader stopAnimating];
            [self.spinnerLoader setHidden:YES];
            
            [self.sfAppLogin saveSettings];
            self.sfAppLogin.currentUser = appUser;
            
            NSData *dataSave = [NSKeyedArchiver archivedDataWithRootObject:appUser];
            [[NSUserDefaults standardUserDefaults] setObject:dataSave forKey:@"currentUser"];
            
            [self moveToStep1];
            
//            //Google Analytics Event
//            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Botao"
//                                                                  action:@"Connect"
//                                                                   label:@"Iphone_Nativo_evento_101_botao_Connect"
//                                                                   value:@1] build]];
        }
        
    }
    else
    {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"LOGIN_MISSING_CREDENTIALS", nil)
                                                                       message:NSLocalizedString(@"LOGIN_MISSING_CREDENTIALS_MESSAGE", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {[alert dismissViewControllerAnimated:YES completion:nil];}];
        
        [alert addAction:defaultAction];
    }
}



-(void)showFacebookLoginButton
{
//    [self.spinnerLoader stopAnimating];
//    [self.spinnerLoader setHidden:YES];
//    [self.loginButton setHidden:NO];
//    [UIView animateWithDuration:1.0 animations:^{
//        self.loginButton.alpha = 1.0;
//        [self.view setNeedsUpdateConstraints];
//    }];
//    [self.but_SignUp setHidden:NO];
//    [self.loginEmailButton setHidden:NO];
}

- (IBAction)facebookLoginOpButton_Clicked:(id)sender {
    
//    [self.facebookLoginOPButton setHidden:YES];
//    [self.loginButton setHidden:YES];
//    [self.spinnerLoader setHidden:NO];
//    [self.spinnerLoader startAnimating];
//
//    LoginFacebookOperation *loginOp = [[LoginFacebookOperation alloc] initWithDelegate:self andSFApp:self.sfAppLogin];
//    [sfAppLogin.operationsQueue addOperation:loginOp];
}


-(void)facebookLoginNowOnStayfilmWithToken:(NSString*)p_token
{
    if(self.sfAppLogin.sfConfig != nil)
    {
        [self.spinnerLoader setHidden:NO];
        [self.spinnerLoader startAnimating];
        
        User *appUser = [User loginWithFacebook:p_token andSFAppSingleton:self.sfAppLogin]; // already saves token to sfApp.settings
        self.sfAppLogin.currentUser = appUser;
        if(appUser == nil)
        {
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CONNECTION_ERROR", nil)
                                                                           message:NSLocalizedString(@"CONNECTION_ERROR_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
            NSLog(@"StayLog: conn error 2");
            //exit(0);
            [self showFacebookLoginButton];
        }
        else if(!appUser.userExists && !appUser.isResponseError) // register user
        {
            [self.sfAppLogin.settings setObject:p_token forKey:@"facebookToken"];
            [self.sfAppLogin saveSettings];
            //colocar a 1 checklogin
            //token = result.token.tokenString;
            __weak typeof(self) selfDelegate = self;
            dispatch_async(dispatch_get_main_queue(), ^{
            [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id,birthday,email,first_name,last_name,gender"}]
             startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
            {
                 if (!error)
                 {
                     NSMutableDictionary *registerArgs = [[NSMutableDictionary alloc] init];
                     [registerArgs setObject:self.sfAppLogin.settings[@"facebookToken"] forKey:@"facebookToken"];
                     
                     if(result[@"birthday"] != nil)
                     {
                         NSDateFormatter *dateformat = [[NSDateFormatter alloc] init];
                         [dateformat setDateFormat:@"MM/dd/yyyy"];
                         NSDate * date = [dateformat dateFromString:result[@"birthday"]];
                         NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:date];
                         [registerArgs setObject:[NSString stringWithFormat:@"%d", (int)[components day]]forKey:@"birthdayDay"];
                         [registerArgs setObject:[NSString stringWithFormat:@"%d", (int)[components month]]forKey:@"birthdayMonth"];
                         [registerArgs setObject:[NSString stringWithFormat:@"%d", (int)[components year]]forKey:@"birthdayYear"];
                     }
                     if(result[@"email"] != nil)
                     {
                         [registerArgs setObject:(NSString *)result[@"email"] forKey:@"email"];
                     }
                     if(result[@"first_name"] != nil)
                     {
                         [registerArgs setObject:(NSString *)result[@"first_name"] forKey:@"firstName"];
                     }
                     if(result[@"last_name"] != nil)
                     {
                         [registerArgs setObject:(NSString *)result[@"last_name"] forKey:@"lastName"];
                     }
                     if(result[@"gender"] != nil)
                     {
                         if([result[@"gender"] isEqualToString:@"male"])
                         {
                             [registerArgs setObject:@"m"forKey:@"gender"];
                         }
                         else if([result[@"gender"] isEqualToString:@"female"])
                         {
                             [registerArgs setObject:@"f" forKey:@"gender"];
                         }
                         else
                         {
                             [registerArgs setObject:@"n" forKey:@"gender"];
                         }
                     }
                     
                     User *registered = [User registerUserWithArguments:registerArgs andSFAppSingleton:self.sfAppLogin]; // already saves to sfApp.settings
                     self.sfAppLogin.currentUser = registered;
                     BOOL registeredSucessfuly = NO;
                     if(registered != nil && registered.isLogged)
                     {
                         if(result[@"id"] != nil)
                         {
                             // check Login   0 = not started  1 = true   2 = false
                             [self.sfAppLogin.settings setObject:@"1" forKey:@"CHECK_Login"];
                             [self.sfAppLogin.settings setObject:(NSString *)result[@"id"] forKey:@"FB_id"];
                             [self.sfAppLogin.settings setObject:[[FBSDKAccessToken currentAccessToken] tokenString] forKey:@"facebookToken"];
                             [self.sfAppLogin.settings setObject:registered.idSession forKey:@"idSession"];
                             [self.sfAppLogin saveSettings];
                             
//                             //Google Analytics Event
//                             id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//                             [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Botao"
//                                                                                   action:@"Register"
//                                                                                    label:@"Iphone_Nativo_evento_108_botao_Register"
//                                                                                    value:@1] build]];
                             registeredSucessfuly = YES;
                             [self.sfAppLogin.currentUser setUserFacebookToken:self.sfAppLogin.settings[@"facebookToken"] andSFAppSingleton:self.sfAppLogin];
                         }
                         else
                         {
                             registeredSucessfuly = NO;
                         }
                     }
                     else
                     {
                         registeredSucessfuly = NO;
                         [self showFacebookLoginButton];
                     }
                     if(registeredSucessfuly == YES)
                     {
                         NSData *dataSave = [NSKeyedArchiver archivedDataWithRootObject:registered];
                         [[NSUserDefaults standardUserDefaults] setObject:dataSave forKey:@"currentUser"];
                         
                         self.sfAppLogin.currentUser = registered;
                         [self.sfAppLogin.settings setObject:@"1" forKey:@"SF_User_Registered"];
                         [self performSelectorOnMainThread:@selector(hideAndStopSpinner) withObject:nil waitUntilDone:NO];
                         [self moveToStep1];
                     }
                     else
                     {
                         NSLog(@"StayLog: Error on Register %@",error);
                         UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"REGISTRATION_ERROR", nil)
                                                                                        message:NSLocalizedString(@"REGISTRATION_ERROR_MESSAGE", nil)
                                                                                 preferredStyle:UIAlertControllerStyleAlert];
                         
                         UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                               handler:^(UIAlertAction * action) {}];
                         
                         [alert addAction:defaultAction];
                         [selfDelegate presentViewController:alert animated:YES completion:nil];
                     }
                 }
                 else
                 {
                     NSLog(@"StayLog: Error facebookloginnowonstayfilmwithtoken %@",error);
                     UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CONNECTION_ERROR", nil)
                                                                                    message:NSLocalizedString(@"CONNECTION_ERROR_MESSAGE", nil)
                                                                             preferredStyle:UIAlertControllerStyleAlert];
                     
                     UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                           handler:^(UIAlertAction * action) {}];
                     
                     [alert addAction:defaultAction];
                     [self presentViewController:alert animated:YES completion:nil];
                     [selfDelegate showFacebookLoginButton];
                 }
             }];
                });
        }
        else if(!appUser.isLogged)
        {
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"LOGIN_FAILED", nil)
                                                                           message:NSLocalizedString(@"LOGIN_FAILED_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
            NSLog(@"StayLog: conn error 3");
            //exit(0);
            [self showFacebookLoginButton];
            
            // check Login   0 = not started  1 = true   2 = false
            [self.sfAppLogin.settings setObject:@"0" forKey:@"CHECK_Login"];
            [self.sfAppLogin saveSettings];
        }
        else //user logged in successfully
        {
            if(self.sfAppLogin.settings == nil)
            {
                self.sfAppLogin.settings = [[NSMutableDictionary alloc] init];
            }
            [self.sfAppLogin.settings setObject:@"1" forKey:@"CHECK_Login"];
            [self.spinnerLoader stopAnimating];
            [self.spinnerLoader setHidden:YES];
            if([[FBSDKAccessToken currentAccessToken] userID] != nil)
                [self.sfAppLogin.settings setObject:[[FBSDKAccessToken currentAccessToken] userID] forKey:@"FB_id"];
            if([[FBSDKAccessToken currentAccessToken] tokenString] != nil)
                [self.sfAppLogin.settings setObject:[[FBSDKAccessToken currentAccessToken] tokenString] forKey:@"facebookToken"];
            [self.sfAppLogin.settings setObject:appUser.idSession forKey:@"idSession"];
            [self.sfAppLogin saveSettings];
            
            self.sfAppLogin.currentUser = appUser;
            NSData *dataSave = [NSKeyedArchiver archivedDataWithRootObject:appUser];
            [[NSUserDefaults standardUserDefaults] setObject:dataSave forKey:@"currentUser"];
            
            //update user in background
            __weak typeof(self) selfDelegate = self;
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^(void){
                [selfDelegate.sfAppLogin.currentUser setUserFacebookToken:self.sfAppLogin.settings[@"facebookToken"] andSFAppSingleton:selfDelegate.sfAppLogin];
            });
            
            [self moveToStep1];
            
//            //Google Analytics Event
//            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Botao"
//                                                                  action:@"Connect"
//                                                                   label:@"Iphone_Nativo_evento_101_botao_Connect"
//                                                                   value:@1] build]];
        }
    }
    else
    {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CONNECTION_ERROR", nil)
                                                                       message:NSLocalizedString(@"CONNECTION_ERROR_MESSAGE", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        NSLog(@"StayLog: conn error 4");
        //exit(0);
        [self showFacebookLoginButton];
        
        // check Login   0 = not started  1 = true   2 = false
        [self.sfAppLogin.settings setObject:@"0" forKey:@"CHECK_Login"];
        [self.sfAppLogin saveSettings];
    }
}

-(void)hideAndStopSpinner
{
    [self.spinnerLoader stopAnimating];
    [self.spinnerLoader setHidden:YES];
}

//-(void)showAndStartSpinner
//{
//    [self.spinnerLoader setHidden:NO];
//    [self.spinnerLoader startAnimating];
//    [UIView animateWithDuration:1.0 animations:^{
//        self.loginButton.alpha = 0;
//        [self.view setNeedsUpdateConstraints];
//    }];
//    [self.loginButton setHidden:YES];
//}

- (void)loginButton:(FBSDKLoginButton *)loginButton didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result error:(NSError *)error {
//    if(error == nil && result.token != nil && ![result.token.tokenString isEqual: @""])
//    {
//        self.didComeBackFromFBLogin = YES;
//
//        [self.spinnerLoader setHidden:NO];
//        [self.spinnerLoader startAnimating];
//
//        [UIView animateWithDuration:1.0 animations:^{
//            self.loginButton.alpha = 0;
//            [self.view setNeedsUpdateConstraints];
//        }];
//        [self.loginButton setHidden:YES];
//
//        [self performSelectorInBackground:@selector(facebookLoginNowOnStayfilmWithToken:) withObject:result.token.tokenString];
//    }
//    else
//    {
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
//            [self showFacebookLoginButton];
//        });
//    }
}

-(void)moveToStep1
{
    @try {
        // check Login   0 = not started  1 = true   2 = false
        [self.sfAppLogin.settings setObject:@"1" forKey:@"CHECK_Login"];
        //[self.sfAppLogin.currentUser validateSubscription:^(BOOL hasSubscription) {

            //elf.sfAppLogin.currentUser.hasSubscription = hasSubscription;
        
            [self.sfAppLogin saveSettings];
            
            if ([NSThread isMainThread])
            {
                UIStoryboard *story = [UIStoryboard storyboardWithName:@"MovieMaker" bundle:nil];
                UIViewController *vc = [story instantiateInitialViewController];
                //[self presentViewController:vc animated:YES completion:nil];  //this is a reminder... remember the orientation bug?? BEWARE of the presentViewController method!
                [self showViewController:vc sender:nil];
            }
            else {
                dispatch_sync(dispatch_get_main_queue(), ^{
                    UIStoryboard *story = [UIStoryboard storyboardWithName:@"MovieMaker" bundle:nil];
                    UIViewController *vc = [story instantiateInitialViewController];
                    //[self presentViewController:vc animated:YES completion:nil];  //this is a reminder... remember the orientation bug?? BEWARE of the presentViewController method!
                    [self showViewController:vc sender:nil];
                });
            }
            
            
        //}];
    }
    @catch (NSException *exception) {
        NSLog(@"StayLog: MoveToStep1 exception with error: %@", exception.description);
    }
}

- (void) loginButtonDidLogOut:(FBSDKLoginButton *)loginButton {
    [self.sfAppLogin.settings removeObjectForKey:@"facebookToken"];
    [self.sfAppLogin.settings removeObjectForKey:@"FB_id"];
    [self.sfAppLogin.settings removeObjectForKey:@"idSession"];
    [self.sfAppLogin.settings removeObjectForKey:@"idUser"];
    [self.sfAppLogin saveSettings];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation


//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    if([segue.identifier isEqual: @"loginSegue"]) {
//        segue
//    }
//     Get the new view controller using [segue destinationViewController].
//     Pass the selected object to the new view controller.
//}




@end
