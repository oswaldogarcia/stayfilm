//
//  StoryState.h
//  Stayfilm-iOS-Full
//
//  Created by Henrique Ormonde on 6/14/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#ifndef Stayfilm_Full_StoryState_h
#define Stayfilm_Full_StoryState_h

#import <Foundation/Foundation.h>
#import "SFConfig.h"
#import "Story.h"

@protocol StoryStateDelegate;
@protocol StoryStateCreationDelegate;


@interface StoryState : NSObject

@property (nonatomic, assign) id <StoryStateDelegate> delegate;
@property (nonatomic, assign) id <StoryStateCreationDelegate> delegateCreation;
@property (nonatomic, strong) NSMutableDictionary *viewState;
@property (nonatomic, strong) Genres *currentGenre;
@property (nonatomic, strong) Template *currentTemplate;

+ (StoryState *) sharedStoryStateSingletonWithDelegate:(id<StoryStateDelegate>)theDelegate;
+ (StoryState *) sharedStoryStateSingletonWithCreationDelegate:(id<StoryStateCreationDelegate>)theDelegate;
+ (StoryState *) sharedStoryStateSingleton;

-(void)updateStories;  //will update stories from WS
-(void)setViewedStoryID:(NSNumber *)idStory withIDContent:(NSNumber *)idcontent;  //Will update ViewState and do work related to viewing that story like BI data
-(void)setCurrentViewedIDGenre:(int)p_genre andIDTemplate:(int)p_template;
-(void)resetStories;

@end


@protocol StoryStateDelegate <NSObject>
- (void)storiesChanged;
@end

@protocol StoryStateCreationDelegate <NSObject>
- (void)storiesLoaded;
@end


#endif
