//
//  PreviewNotificationViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 12/7/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol PreviewNotificationDelegate <NSObject>
-(void)closeModal;
@end

@interface PreviewNotificationViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *savedLabel;
@property (weak, nonatomic) IBOutlet UILabel *verifyingLabel;
@property (weak, nonatomic) IBOutlet UILabel *notificationLabel;



@property (nonatomic, assign) id <PreviewNotificationDelegate> delegate;
@end

NS_ASSUME_NONNULL_END
