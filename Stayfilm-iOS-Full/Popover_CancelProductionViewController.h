//
//  Popover_CancelProductionViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Henrique on 5/18/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol Popover_CancelProductionDelegate <NSObject>
@required
-(void)chooseOption:(NSString*)option;
@end


@interface Popover_CancelProductionViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *but_StayHere;
@property (weak, nonatomic) IBOutlet UIButton *but_Back;

@property (nonatomic, strong) id<Popover_CancelProductionDelegate> delegate;

-(id)initWithDelegate:(id<Popover_CancelProductionDelegate>)p_delegate;

@end
