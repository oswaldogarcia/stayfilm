//
//  UploadBlob.h
//  Stayfilm for Messenger
//
//  Created by Henrique Ormonde on 20/05/15.
//  Copyright (c) 2015 Stayfilm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AuthenticationCredential.h"
#import "CloudStorageClient.h"
#import "BlobContainer.h"
#import "Blob.h"
#import "TableEntity.h"
#import "TableFetchRequest.h"

#import "AlbumSF.h"

@interface UploadBlob : NSObject <CloudStorageClientDelegate>
//{
//    NSConditionLock* allUploadedLock;
//}

@property (nonatomic, assign) BOOL isUploading;
@property (nonatomic, assign) BOOL isPreparingUpload;
@property (nonatomic, strong) NSMutableArray *listUploadMedias;
@property (nonatomic, strong) NSMutableArray *listUploadedMedias;
@property (nonatomic, strong) AlbumSF *uploadedAlbum;
@property (nonatomic, strong) NSConditionLock* allUploadedLock;

@property (nonatomic, assign) NSInteger uploadedCount;
@property (nonatomic, assign) NSInteger totalUploadCount;

- (id)init;
+ (UploadBlob *) sharedUploadBlobSingleton;
- (BOOL)addMedias:(NSArray *)p_medias;
- (void)uploadAllMedias;
- (BOOL)resetUpload;
- (BOOL)didUploadFinish;
-(void)addBlobToContainerHack:(BlobContainer *)p_container blobName:(NSString *)blobName contentData:(NSData *)contentData contentType:(NSString*)contentType;

@end
