//
//  Step1ConfirmationViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Henrique Ormonde on 06/11/17.
//  Copyright © 2017 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlbumFB.h"
#import "PhotoFB.h"
#import "VideoFB.h"
#import "AlbumInternal.h"
#import "Popover_ConfirmationMoreOptions_ViewController.h"
#import "Uploader.h"



@protocol ChangeMovieDataDelegate<NSObject>

- (void)titleChanged:(NSString *)title;
- (void)mediaChanged;
@end


@interface Step1ConfirmationViewController : UIViewController <UICollectionViewDelegate, UIScrollViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate, UIViewControllerTransitioningDelegate, StayfilmUploaderDelegate, Popover_ConfirmationMoreOptions_ViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *photosCollectionView;
@property (weak, nonatomic) IBOutlet UIButton *but_MoreOptions;
@property (weak, nonatomic) IBOutlet UIButton *but_back;

@property (weak, nonatomic) IBOutlet UITextField *txt_movieTitle;
@property (weak, nonatomic) IBOutlet UILabel *txt_stripInfo;
@property (weak, nonatomic) IBOutlet UIView *titleView;
@property (weak, nonatomic) IBOutlet UILabel *selectedContentLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lowerViewConstraint;
@property (weak, nonatomic) IBOutlet UIView *editPhotoTabBarView;
@property (weak, nonatomic) IBOutlet UIButton *checkEditButton;
@property (weak, nonatomic) IBOutlet UILabel *filmTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *noMediaView;
@property (weak, nonatomic) IBOutlet UIImageView *tapToStartImage;

@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property (weak, nonatomic) IBOutlet UIButton *reorderButton;
@property (weak, nonatomic) IBOutlet UIButton *removeButton;

@property (weak, nonatomic) IBOutlet UILabel *reorderLabel;
@property (weak, nonatomic) IBOutlet UILabel *removeLabel;


@property (nonatomic, strong) NSString *editingType;
@property (nonatomic, strong) NSString *temporalTitle;

@property (nonatomic , strong) PHCachingImageManager *videoImageManager;
@property (nonatomic, strong) NSURLSession *thumbConnection;

- (IBAction)unwindToStep1:(UIStoryboardSegue *)segue;

- (void)clearAlbumViewController;
- (void)changeSelectedMedias:(NSMutableArray *)p_selected;
- (void)changeEditedMedias:(NSMutableArray *)p_edited;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthCancelConstraint;


@property (nonatomic, strong) id<ChangeMovieDataDelegate> delegate;
@end
