//
//  VideoUploadOperation.m
//  Stayfilm for Messenger
//
//  Created by Henrique Ormonde on 26/10/15.
//  Copyright © 2015 Stayfilm. All rights reserved.
//

#import "VideoUploadOperation.h"
#import "StayfilmApp.h"

@interface VideoUploadOperation ()

@property (nonatomic, weak) StayfilmApp *sfApp;
@property (nonatomic, strong) NSURLSessionUploadTask *uploadDataTask;
@property (nonatomic, strong) NSMutableData *responseData;
@property (nonatomic, strong) NSString *echo;
@property (nonatomic, strong) NSString *albumID;
@property (nonatomic, strong) NSConditionLock *lock;
@end


@implementation VideoUploadOperation

@synthesize sfApp = _sfApp;
@synthesize delegate = _delegate;
@synthesize sessionURLBackground = _sessionURLBackground;
@synthesize uploadDataTask = _uploadDataTask;
@synthesize responseData = _responseData;
@synthesize pathFile = _pathFile;
@synthesize echo = _echo;
@synthesize albumID = _albumID;
@synthesize uploadedMedia = _uploadedMedia;
@synthesize lock;


#pragma mark -
#pragma mark - Life Cycle

- (id)initWithMediaPath:(NSString *)p_pathFile withEcho:(NSString *)p_echo withAlbumID:(NSString *)p_albumID withBackgroundSessionIdentifier:(NSString *)identifier delegate:(id<VideoUploaderDelegate>) theDelegate
{
    if (self = [super init]) {
        self.delegate = theDelegate;
        self.pathFile = p_pathFile;
        self.echo = p_echo;
        self.albumID = p_albumID;
        self.uploadedMedia = nil;
        self.responseData = nil;
        self.lock = nil;
        NSURLSessionConfiguration *sessionConfigBackground = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:identifier];
        sessionConfigBackground.allowsCellularAccess = YES;
        [sessionConfigBackground setHTTPAdditionalHeaders:@{@"Accept": @"*/*"}];
        sessionConfigBackground.timeoutIntervalForRequest = 30.0;
        sessionConfigBackground.timeoutIntervalForResource = 240.0;
        sessionConfigBackground.HTTPMaximumConnectionsPerHost = 2;
        
        self.sessionURLBackground = [NSURLSession sessionWithConfiguration:sessionConfigBackground
                                                                  delegate:self
                                                             delegateQueue:[NSOperationQueue mainQueue]];
    }
    return self;
}

#pragma mark -
#pragma mark - Upload Video

- (void)main {
    @autoreleasepool {
        
        if (self.isCancelled)
            return;
        
        @try
        {
            NSMutableURLRequest* request = nil;
            
            self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
            
            self.lock = nil;
            self.lock = [[NSConditionLock alloc] initWithCondition:1];
            
            BOOL waitingStep = YES;
            while (waitingStep) {
                [NSThread sleepForTimeInterval:0.5];
                waitingStep = ![self.sfApp isLoggedIn];
                //waitingStep = !waitingStep;
            }
            
            request = [[NSMutableURLRequest alloc] initWithURL:[[NSURL alloc] initWithString:[self.sfApp.wsConfig getConfigPathWithKey:@"createMedia"]] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:180];
            
            
            if(self.sfApp.currentUser != nil && self.sfApp.currentUser.idSession != nil && self.sfApp.currentUser.idSession.length != 0)
            {
                [request setValue:self.sfApp.currentUser.idSession forHTTPHeaderField:@"idSession"];
            }
            else if(self.sfApp.settings[@"idSession"] != nil)
            {
                [request setValue:self.sfApp.settings[@"idSession"] forHTTPHeaderField:@"idSession"];
            }
            [request setValue:@"1" forHTTPHeaderField:@"useheader"];
            [request setValue:@"iOS" forHTTPHeaderField:@"appAgent"];
            [request addValue:@"video/quicktime" forHTTPHeaderField:@"Content-Type"];
            [request addValue:self.albumID forHTTPHeaderField:@"idalbum"];
                
            [request setHTTPMethod:@"POST"];

            self.uploadDataTask = [self.sessionURLBackground uploadTaskWithRequest:request fromFile:[[NSURL alloc] initFileURLWithPath:self.pathFile]];
            [self.uploadDataTask resume];

            [self.lock lockWhenCondition:0];
            [self.lock unlock];
            self.lock = nil;

        }
        @catch (NSException *exception) {
            NSLog(@"StayLog: Operation Upload Video EXCEPTION with error: %@", exception.description);
            [(NSObject *)self.delegate performSelectorOnMainThread:@selector(videoUploaderDidFail:) withObject:self waitUntilDone:NO];
            [self.lock lockWhenCondition:0];
            [self.lock unlock];
            [self cancel];
        }
        
        if (self.isCancelled) {
            [self cleanupSession];
            [self cancel];
            return;
        }
    }
}

-(void)cleanupSession
{
    [self.sessionURLBackground finishTasksAndInvalidate];
    [self.sessionURLBackground resetWithCompletionHandler:^{
        self.sessionURLBackground = nil;
    }];
    self.uploadDataTask = nil;
}

#pragma URL Delegates

//- (void)URLSessionDidFinishEventsForBackgroundURLSession:(NSURLSession *)session
//{
//    if (self.backgroundSessionCompletionHandler) {
//        self.backgroundSessionCompletionHandler();
//        self.backgroundSessionCompletionHandler = nil;
//        self.uploadedCount = self.listUploadedMedias.count;
//        self.isUploading = NO;
//        self.isPreparingUpload = NO;
//
//        [self sendConfirmationMedias];
//    }
//}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data
{
    if (self.isCancelled) {
        [dataTask cancel];
        NSLog(@"StayLog: Upload Operation CANCELED with media: %@", self.echo);
        [self cleanupSession];
    }
    if(self.responseData == nil)
    {
        self.responseData = [[NSMutableData alloc] initWithData:data];
    }
    else
    {
        [self.responseData appendData:data];
    }
}

- (void)URLSession:(NSURLSession *)session
              task:(NSURLSessionTask *)task
didCompleteWithError:(NSError *)error
{
    if(error)
    {
        NSLog(@"StayLog: Upload Operation for media: %@ called from URLSession Failed with ERROR: %@", self.echo, error.description);
        
        [(NSObject *)self.delegate performSelectorOnMainThread:@selector(videoUploaderDidFail:) withObject:self waitUntilDone:NO];
        [self.lock lock];
        [self.lock unlockWithCondition:0];
        [self cleanupSession];
        [self cancel];
    }
    else
    {
        @try
        {
            if(self.isCancelled)
            {
                NSLog(@"StayLog: Upload Operation Finished but was CANCELED with media: %@", self.echo);
                [self.lock lock];
                [self.lock unlockWithCondition:0];
                [self cleanupSession];
                return;
            }
            
            NSError *error2 = nil;
            
            NSDictionary *result = [NSJSONSerialization JSONObjectWithData:self.responseData
                                                                   options:kNilOptions
                                                                     error:&error2];
            if (result == nil || error2 != nil)
            {
                // INVALID JSON
                [NSException raise:@"Invalid JSON" format:@"Error message is: %@", error2];
            }
            else if(result[@"data"] != nil)// && result[@"echo"] != nil)
            {
                self.uploadedMedia = [Media customClassWithProperties:result[@"data"]];
                self.uploadedMedia.isVideo = YES;
                
                //NSLog(@"blob uploaded idMedia: %@ echo: %@", self.uploadedMedia.idMidia, result[@"echo"]);
                [(NSObject *)self.delegate performSelectorOnMainThread:@selector(videoUploaderDidFinish:) withObject:self waitUntilDone:NO];
                [self cleanupSession];
                [self.lock lock];
                [self.lock unlockWithCondition:0];
                return;
            }
            else
            {
                NSLog(@"StayLog: WEBSERVICE Response not good...");
                [(NSObject *)self.delegate performSelectorOnMainThread:@selector(videoUploaderDidFail:) withObject:self waitUntilDone:NO];
                [self.lock lock];
                [self.lock unlockWithCondition:0];
                [self cleanupSession];
                [self cancel];
            }
        }
        @catch (NSException *exception) {
            NSLog(@"StayLog: EXCEPTION Uploaded File with error: %@", exception.description);
            
            [(NSObject *)self.delegate performSelectorOnMainThread:@selector(videoUploaderDidFail:) withObject:self waitUntilDone:NO];
            [self.lock lock];
            [self.lock unlockWithCondition:0];
            [self cleanupSession];
            [self cancel];
        }
        
    }
}

@end
