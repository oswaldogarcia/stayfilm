//
//  LoginWebView.h
//  Stayfilm-iOS-Full
//
//  Created by Henrique Ormonde on 18/09/17.
//  Copyright © 2017 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>
#import "LoginToSocialNetworkOperation.h"


@protocol LoginToSocialNetworkOperationDelegate;

@interface LoginWebView : UIViewController <WKUIDelegate, WKNavigationDelegate, WKScriptMessageHandler>

@property (nonatomic, assign) id<LoginToSocialNetworkOperationDelegate> delegate;
@property (nonatomic, strong) NSString *socialNetwork;
@property (nonatomic, strong) NSURL *loginURL;

@end
