//
//  ShellWebService.h
//  Stayfilm
//
//  Created by Victor Manuel Roldan on 5/16/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import "AFHTTPSessionManager.h"
#import "Service.h"
//#import "SFEnums.h"
@interface ShellWebService : AFHTTPSessionManager<Service>

+ (ShellWebService *)getSharedInstance;
@property (strong, nonatomic) NSString *endpoint;
@property (strong, nonatomic) NSString *httpMethod;
@property (nonatomic) Endpoints endpointEnum;
@end
