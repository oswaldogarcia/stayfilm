//
//  EditProfileViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 7/05/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StayfilmApp.h"
#import "User.h"
#import "AFNetworking.h"
#import "PopupMenuAnimation.h"
#import "UIView+Toast.h"

@interface EditProfileViewController : UIViewController <UIViewControllerTransitioningDelegate,StayfilmSingletonDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIView *view_connectionStatus;
@property (weak, nonatomic) IBOutlet UILabel *lbl_connectionStatus;

@property (nonatomic) StayfilmApp *sfAppProfile;
@property (nonatomic) NSString *localFilePath;

@property (weak, nonatomic) IBOutlet UIView *loadingView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;
@property (nonatomic, strong) PopupMenuAnimation *transitionAnimation;

@end
