//
//  LoginIntroViewController.h
//  Stayfilm for Messenger
//
//  Created by Henrique Ormonde on 29/04/15.
//  Copyright (c) 2015 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Google/Analytics.h>
#import "User.h"
#import "AppDelegate.h"
#import "StayfilmApp.h"
#import "LoginFacebookOperation.h"
#import "StorieViewController.h"

@interface LoginIntroViewController : UIViewController <FBSDKLoginButtonDelegate, StayfilmSingletonDelegate, LoginFacebookOperationDelegate, StoryStateCreationDelegate, ProfileStoryDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imageBackground;
@property (weak, nonatomic) IBOutlet UIImageView *imageBackgroundText;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinnerLoader;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinnerExplore;

@property (weak, nonatomic) IBOutlet UIView *viewLogin;
@property (weak, nonatomic) IBOutlet UIButton *okLoginButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelLoginButton;
@property (weak, nonatomic) IBOutlet UITextField *txt_username;
@property (weak, nonatomic) IBOutlet UITextField *txt_password;


@property (weak, nonatomic) IBOutlet UIButton *loginEmailButton;
@property (weak, nonatomic) IBOutlet UIButton *but_SignUp;
@property (weak, nonatomic) IBOutlet UIButton *facebookLoginOPButton;
@property (weak, nonatomic) IBOutlet UIButton *but_Explore;

- (void)loginButton:(FBSDKLoginButton *)loginButton didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result error:(NSError *)error;
- (void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton;

- (void)moveToStep1;

@end
