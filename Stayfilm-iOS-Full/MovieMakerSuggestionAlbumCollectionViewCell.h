//
//  MovieMakerSuggestionAlbumCollectionViewCell.h
//  Stayfilm-iOS-Full
//
//  Created by Henrique Ormonde on 16/10/17.
//  Copyright © 2017 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MovieMakerSuggestionAlbumCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *txt_Title;
@property (weak, nonatomic) IBOutlet UIImageView *img_Big;
@property (weak, nonatomic) IBOutlet UIImageView *img_SmallUp;
@property (weak, nonatomic) IBOutlet UIImageView *img_SmallDown;

//@property (strong, nonatomic) NSMutableArray *medias;

@end
