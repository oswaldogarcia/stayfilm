//
//  TemplateCollectionViewCell.h
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 22/05/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SFConfig.h"
#import <SDWebImage/UIImageView+WebCache.h>

@protocol TemplateDelegate<NSObject>

- (void)unlockTemplate:(Template*)selectedTemplate;


@end

@interface TemplateCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *templateImage;
@property (weak, nonatomic) IBOutlet UIImageView *frameTemplateImage;
@property  (nonatomic) Template *currentTemplate;
@property (nonatomic) BOOL isVertical;
@property (weak, nonatomic) IBOutlet UIView *blockedView;
@property (weak, nonatomic) IBOutlet UIButton *unlockButton;
@property (nonatomic) BOOL isBlocked;
@property (nonatomic, assign) id<TemplateDelegate> delegate;
- (void)initCell;
@end
