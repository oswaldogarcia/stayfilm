//
//  Genres.h
//  Stayfilm for Messenger
//
//  Created by Henrique Ormonde on 23/04/15.
//  Copyright (c) 2015 Stayfilm. All rights reserved.
//

#ifndef Stayfilm_Full_SFConfig_h
#define Stayfilm_Full_SFConfig_h

#import "WSConfig.h"
#import "StayWS.h"
#import "ShellWebService.h"
#import "Utilities.h"


@interface SFConfig : NSObject

@property (nonatomic, strong) NSDictionary *generalConfig;
@property (nonatomic, strong) NSNumber *min_photos;
@property (nonatomic, strong) NSNumber *max_photos;
@property (nonatomic, strong) NSMutableArray *genres;
@property (nonatomic, strong) NSArray *stories;
@property (nonatomic, strong) NSNumber *push_enable_geo;

+(SFConfig *)getAllConfigsFromWS:(WSConfig *)ws_config;
-(void)getTemplates;
@end


@interface Campaign : NSObject

@property (nonatomic, strong) NSNumber *idcustomer;
@property (nonatomic, strong) NSString *idcampaign;
@property (nonatomic, strong) NSNumber *created;
@property (nonatomic, strong) NSNumber *curation;
@property (nonatomic, strong) NSString *data;
@property (nonatomic, strong) NSNumber *startdate;
@property (nonatomic, strong) NSNumber *enddate;
@property (nonatomic, strong) NSNumber *isactive;
@property (nonatomic, strong) NSString *slug;
@property (nonatomic, strong) NSString *title;

+ (Campaign *)customClassWithProperties:(NSDictionary *)properties;
- (Campaign *)initWithProperties:(NSDictionary *)properties;

// isactive is a enum

@end


@interface GenreConfig : NSObject

@property (nonatomic, strong) NSString *gallery_image_url;
@property (nonatomic, strong) NSString *campaignslug;
@property (nonatomic, strong) Campaign *campaign;

+ (GenreConfig *)customClassWithProperties:(NSDictionary *)properties;
- (GenreConfig *)initWithProperties:(NSDictionary *)properties;

@end


@interface Genres : NSObject <NSCopying>

@property (nonatomic) NSUInteger idgenre;
@property (nonatomic, strong) NSString *imageUrl;
@property (nonatomic, strong) NSString *imageUrl_en;
@property (nonatomic, strong) NSString *imageUrl_es;
@property (nonatomic, strong) GenreConfig *config;
@property (nonatomic, strong) NSNumber *created;
@property (nonatomic, strong) NSString *gatag;
@property (nonatomic, strong) NSNumber *genreorder;
@property (nonatomic, strong) NSString *genretype;
@property (nonatomic, strong) NSNumber *isactive;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *name_en;
@property (nonatomic, strong) NSString *name_es;
@property (nonatomic, strong) NSString *name_fr;
@property (nonatomic, strong) NSString *slug;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSNumber *updated;
@property (nonatomic, strong) NSMutableArray *templates;
@property (nonatomic) BOOL hasVertical;
@property (nonatomic) BOOL isPaid;
@property (nonatomic, assign) StayfilmTypesFilmOrientation orientation;




//-(Genres *)getGenreWithIdGenre:(int)idGenre usingWSConfig:(WSConfig *)config;
+ (id)customClassWithProperties:(NSDictionary *)properties;
- (id)initWithProperties:(NSDictionary *)properties;

+ (id)getGenreByID:(int)p_idgenre;


@end


@interface SubTheme : NSObject

@property (nonatomic, strong) NSNumber *idsubtheme;
@property (nonatomic, readonly, copy) NSString *gatag;
@property (nonatomic, strong) NSNumber *isactive;
@property (nonatomic, readonly, copy) NSString *name;
@property (nonatomic, readonly, copy) NSString *name_en;
@property (nonatomic, readonly, copy) NSString *name_es;
@property (nonatomic, readonly, copy) NSString *name_fr;
@property (nonatomic, readonly, copy) NSString *slug;

// isactive is a enum

@end


@interface Theme : NSObject

@property (nonatomic, strong) NSNumber *idtheme;
@property (nonatomic, strong) NSString *gatag;
@property (nonatomic, strong) NSNumber *isactive;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *name_en;
@property (nonatomic, strong) NSString *name_es;
@property (nonatomic, strong) NSString *name_fr;
@property (nonatomic, strong) NSString *slug;
@property (nonatomic, strong) NSArray *subthemes;

// isactive is a enum

@end


@interface  Template : NSObject <NSCopying>

@property (nonatomic) NSUInteger idtemplate;
@property (nonatomic, strong) NSNumber *isactive;
@property (nonatomic, strong) NSString *data;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *image;
@property (nonatomic, strong) NSString *thumbnail;
@property (nonatomic) NSUInteger idgenre;
@property (nonatomic) BOOL isPaid;

+ (id)customClassWithProperties:(NSDictionary *)properties;
- (id)initWithProperties:(NSDictionary *)properties;
@end



@interface StayConfig : NSObject

@property (nonatomic, strong) NSString *fb_app_id;
@property (nonatomic, strong) NSNumber *min_upload_photo;
@property (nonatomic, strong) NSNumber *max_upload_files;
@property (nonatomic, strong) NSString *facebook_permissions;


// isactive is a enum

@end


@interface MovieMakerConfig : NSObject

@property (nonatomic, strong) NSArray *genres;
@property (nonatomic, strong) NSArray *themes;

@end




#endif
