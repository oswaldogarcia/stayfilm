//
//  RecoverEmailViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Henrique on 4/26/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecoverEmailViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *img_Background;
@property (weak, nonatomic) IBOutlet UITextField *txt_Email;
@property (weak, nonatomic) IBOutlet UIButton *but_SendEmail;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner_loader;

@end
