//
//  GenreTableViewCell.m
//  Stayfilm for Messenger
//
//  Created by Henrique Ormonde on 29/04/15.
//  Copyright (c) 2015 Stayfilm. All rights reserved.
//

#import "GenreTableViewCell.h"

@implementation GenreTableViewCell

@synthesize genreImageView=_genreImageView;
@synthesize selectedGenre=_selectedGenre;
@synthesize selection=_selection;
@synthesize task;

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    
    
    
    
}

-(void)setSelect:(BOOL)select {
    [self setSelected:select animated:YES];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    if(selected)
    {
        self.selection.alpha = 0.0;
        self.selection.hidden = NO;
        [UIView animateWithDuration:0.3 animations:^{
            self.selection.alpha = 1.0;
        }];
        
    }
    else
    {
        self.selection.hidden = YES;
    }

}


@end
