//
//  InstagramActivity.h
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 15/05/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StayfilmApp.h"
#import "AFNetworking.h"
#import "Movie.h"
#import "ProgressBarViewController.h"
#import "ProfileViewController.h"
#import "VideoFinishedViewController.h"


@interface InstagramActivity : UIActivity
@property (nonatomic, strong) Movie *movie;
@property (nonatomic, strong) ProgressBarViewController *progressView;
@property (nonatomic, strong) ProfileViewController *profileView;
@property (nonatomic, strong) VideoFinishedViewController *videoFinishView;
@property (nonatomic, strong) UIView *view;
@property (nonatomic, strong) StayfilmApp *sf;
@property (nonatomic, strong) AFURLSessionManager *manager;
@property (nonatomic, strong) NSURLSessionDownloadTask *downloadTask;

@end
