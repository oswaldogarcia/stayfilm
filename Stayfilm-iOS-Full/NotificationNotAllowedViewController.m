//
//  NotificationNotAllowedViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 12/10/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import "NotificationNotAllowedViewController.h"

@interface NotificationNotAllowedViewController ()

@end

@implementation NotificationNotAllowedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.notNowButton.layer.borderWidth = 1.0;
    self.notNowButton.layer.borderColor = [UIColor colorWithRed:0.31 green:0.50 blue:0.57 alpha:1.0].CGColor;
    self.notNowButton.layer.cornerRadius = 5.0;
    self.notNowButton.layer.masksToBounds = YES;
    
    [self setLanguage];
}

-(void)setLanguage{

    self.yesLabel.text =  NSLocalizedString(@"SAVED_FILM_LABEL", nil);
    self.nowLabel.text = NSLocalizedString(@"VERIFYING_FILM", nil);
    self.whenLabel.text = NSLocalizedString(@"ALLOW_NOTIFICATION_LABEL", nil);
    
    [self.allowNotificationButton setTitle:NSLocalizedString(@"ALLOW_NOTIFICATION", nil) forState:UIControlStateNormal];
    
    [self.notNowButton setTitle:NSLocalizedString(@"NOT_NOW", nil) forState:UIControlStateNormal];

}

- (IBAction)allowNotificationAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.delegate goToAllowNotification];
    
    
}
- (IBAction)notNowNotification:(id)sender {
    [self.delegate closeModal];
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
