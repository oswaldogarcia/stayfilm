//
//  AlbumViewerViewController.h
//  fbmessenger
//
//  Created by Henrique Ormonde on 13/07/15.
//  Copyright (c) 2015 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlbumFB.h"
#import "PhotoFB.h"
#import "VideoFB.h"
#import "AlbumInternal.h"
#import "Uploader.h"
#import <SDWebImage/UIImageView+WebCache.h>

#import "AlbumSF.h"
#import "AlbumSN.h"
#import "Media.h"


@interface AlbumViewerViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate,StayfilmUploaderDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *photosCollectionView;
@property (weak, nonatomic) IBOutlet UIButton *but_back;
@property (weak, nonatomic) IBOutlet UILabel *txt_Title;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lowerViewConstraint;
@property (weak, nonatomic) IBOutlet UIView *view_Editing;
@property (weak, nonatomic) IBOutlet UIButton *but_edit_Back;
@property (weak, nonatomic) IBOutlet UIButton *but_edit_Confirm;
@property (weak, nonatomic) IBOutlet UIButton *but_edit_Cancel;

@property (nonatomic, strong) NSURLSession *thumbConnection;
@property (nonatomic, strong) NSMutableArray *selectedMedias;
@property (nonatomic, strong) AlbumFB *albumFB;
@property (nonatomic, strong) AlbumInternal *albumGeneric;
@property (nonatomic, assign) int indexSelectedAlbum;
@property (weak, nonatomic) IBOutlet UIButton *albumsButton;

- (void)realoadAlbum;
- (void)changeSelectedMedias;
- (IBAction)unwindToStep2:(UIStoryboardSegue *)segue;
//- (IBAction)SelectAllTapped:(id)sender;

-(void)getAllPhotosFromFacebookAlbum;

@end
