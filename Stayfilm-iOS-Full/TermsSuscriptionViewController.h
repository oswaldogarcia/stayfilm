//
//  TermsSuscriptionViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 4/10/19.
//  Copyright © 2019 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>
#import "StayfilmApp.h"
#import "UIView+Toast.h"

NS_ASSUME_NONNULL_BEGIN

@protocol SubscriptionTermsDelegate<NSObject>

-(void)subscriptionInTermsConfirmed;
-(void)startPurchase;
-(void)endPurchase;
-(void)restorePurchase;

@end

@interface TermsSuscriptionViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *termsTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *termsDescriptionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *termsImage;
@property (weak, nonatomic) IBOutlet UILabel *productShortDescription;
@property (weak, nonatomic) IBOutlet UILabel *offertLabel;
@property (weak, nonatomic) IBOutlet UIButton *buyButton;
@property (weak, nonatomic) IBOutlet UILabel *productDescription;
@property (weak, nonatomic) StayfilmApp *sfApp;
@property (weak, nonatomic) IBOutlet UIButton *restorePurchaseButton;
@property (nonatomic, assign) id<SubscriptionTermsDelegate> delegate;
@end

NS_ASSUME_NONNULL_END
