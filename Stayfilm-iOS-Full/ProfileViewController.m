//
//  ProfileViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 23/04/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import "ProfileViewController.h"
#import "ProfileTableViewCell.h"
#import "Feed.h"
#include "StayWS.h"
#import "StayfilmApp.h"
#import "Movie.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <AVKit/AVKit.h>
#import "PlayerViewController.h"
#import "InstagramActivity.h"
#import "DownloadActivity.h"
#import "ProgressBarViewController.h"
#import "ExplorerCollectionViewCell.h"
#import "StoriesPageViewController.h"
#import "StorieViewController.h"
#import "Reachability.h"
#import "WaitingTableViewCell.h"
#import "NotificationFilmInfoViewController.h"
#import "StoriesContentViewController.h"


@interface ProfileViewController ()<ProfileDelegate,ProfileSettingsDelegate,VideoOptionsDelegate,PrivacyOptionsDelegate,Popover_DeleteDelegate,ProgressBarViewControllerDelegate, StoryStateDelegate>

@property (nonatomic, strong) StoryState *sfStoryState;
@property (nonatomic, assign) NSInteger shareFilmPosition;


@property (nonatomic, assign) int numberOfCells;
@property (nonatomic, assign) BOOL noFilms;

@property(nonatomic, strong) NSTimer *connectionStatusTimer;

@end

@implementation ProfileViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
   
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor clearColor],
                             // NSFontAttributeName:[UIFont fontWithName:@"ProximaNova-Regular" size:18],
                              
                              }];
    self.navigationController.navigationBar.shadowImage = [[UIImage alloc] init];
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    
    [self.navigationItem.backBarButtonItem setTitle:@""];
    
    if (self.sfAppProfile.sfConfig.stories == nil || self.sfAppProfile.sfConfig.stories.count == 0){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            StoryState *storyState = [StoryState sharedStoryStateSingleton];
            [storyState updateStories];
        });
    }
    self.storiesArray = [[NSMutableArray alloc] initWithArray: self.sfAppProfile.sfConfig.stories];
    
    NSMutableArray *viewedStories = [[NSMutableArray alloc] init];
    NSMutableArray *positions  = [[NSMutableArray alloc] init];
    
    for (int i = 0 ; i < self.storiesArray.count; i++) {
        Story *story = self.storiesArray[i];
        if (story.isStoryViewed){
          
            [viewedStories addObject:story];
            [positions addObject:[NSNumber numberWithInt:i]];
            
        }
    }
    
    if( viewedStories.count > 0){
        NSMutableIndexSet *indexSet = [NSMutableIndexSet indexSet];
        
        for (int i = 0 ; i < positions.count; i++) {
         
            [indexSet addIndex:[positions[i] intValue]];
            
        }
        [self.storiesArray removeObjectsAtIndexes:indexSet];
        [self.storiesArray addObjectsFromArray:viewedStories];
        [self.exploreCollectionView reloadData];
    }
    
   [self.profileImage sd_setImageWithURL:[NSURL URLWithString:self.sfAppProfile.currentUser.photo] placeholderImage:[UIImage imageNamed:@"Avatar_None"] options:SDWebImageRefreshCached];
    
    [self setLanguage];
    
    self.numberOfCells = 5;
    self.noFilms = NO;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.noFilms = NO;
 
    self.myFeed = [[Feed alloc] init];
    self.myFeed.offset = @"0";
    [self callServiceToGetMyFilms];
    
    if(self.sfAppProfile.currentUser.firstName != nil && ![self.sfAppProfile.currentUser.firstName isEqualToString:@""]){
        
        self.userNameLabel.text =  self.sfAppProfile.currentUser.firstName;
        self.profileLastName.text =  self.sfAppProfile.currentUser.lastName;
        
    }else{
        self.userNameLabel.text = @"           ";
        self.profileLastName.text =  self.sfAppProfile.currentUser.username;
    }
    
    self.userEmailLabel.text = self.sfAppProfile.currentUser.email;
    
    self.profileImage.layer.cornerRadius = self.profileImage.frame.size.width / 2;
    self.profileImage.clipsToBounds = YES;

        
    [self.profileImage sd_setImageWithURL:[NSURL URLWithString:self.sfAppProfile.currentUser.photo] placeholderImage:[UIImage imageNamed:@"Avatar_None"] options:SDWebImageRefreshCached];
    
    
}

- (void)viewDidAppear:(BOOL)animated{
    
    self.sfStoryState = [StoryState sharedStoryStateSingletonWithDelegate:self];
    
    [self.sfAppProfile clearGalleryPlayerObservers];
    [self.sfAppProfile.filmStrip resetSingletonDelegate];
    [self.sfStoryState updateStories];
    
    if(self.sfAppProfile.currentUser.firstName != nil && ![self.sfAppProfile.currentUser.firstName isEqualToString:@""]){
        
        self.userNameLabel.text =  self.sfAppProfile.currentUser.firstName;
        self.profileLastName.text =  self.sfAppProfile.currentUser.lastName;
        
    }else{
        self.userNameLabel.text = @"           ";
        self.profileLastName.text =  self.sfAppProfile.currentUser.username;
    }
    
    self.transitionAnimation = [[PopupMenuAnimation alloc] init];
    
    //Google Analytics
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"profile"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    
    if (networkStatus != NotReachable) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self connectivityChanged:YES];
        });
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self connectivityChanged:NO];
        });
    }
    
}

- (void)setLanguage{
    
    [self.navigationItem setTitle: NSLocalizedString(@"PROFILE", nil)];
    self.noFilmsLabel.text = NSLocalizedString(@"NO_FILMS_YET", nil);
    
}

- (void)callServiceToGetMyFilms{
    
    ShellWebService *service = [ShellWebService getSharedInstance];

    NSDictionary *parameters = @{ @"idUser" : self.sfAppProfile.currentUser.idUser,
                                  @"limit"  : @20,
                                  @"offset" : self.myFeed.offset,
                                  };
    
    [service callService:parameters endpointName:endpointGetUserMovies withCompletionBlock:^(NSDictionary *result, NSError *error) {
        
        if(error == nil){
            
            //NSLog(@"Result: %@",result);
            [self.loadingView setHidden:YES];
            [self.loadingIndicator stopAnimating];
            
            if(result != nil){
                self.myFeed.rawResult = result;
                self.myFeed.offset = (result[@"offset"] == nil) ? @"" : result[@"offset"];
                self.myFeed.timestampNow = (result[@"timestampNow"] == nil) ? @"" : result[@"timestampNow"];
                
                NSArray *dataArray = (NSArray*)result[@"data"];
                
                if(result != nil && (dataArray.count > 0)){
                    self.needPagination = YES;
                    for (NSDictionary* object in result[@"data"]) {
                        
                        Movie *movie = [Movie customClassWithProperties:object];
                        [self.myFeed.itemsList addObject:movie];
//
                    }
                }else{
                    
                    self.needPagination = NO;
                    if(self.myFeed.itemsList.count == 0){
                        self.noFilms = YES;
                        self.numberOfCells = 1;
                        [self.userFilmsTable reloadData];
                    }
                }
                if(self.myFeed.itemsList.firstObject != nil && [(Movie *)self.myFeed.itemsList.firstObject created] != nil)
                {
                    self.myFeed.offsetPrevious = [(Movie *)self.myFeed.itemsList.firstObject created];
                }
                
                if (self.myFeed.itemsList.count > 0){
                    

                    
                    NSMutableArray *activeFilms = [[NSMutableArray alloc]init];
                    NSMutableArray *pendingFilms = [[NSMutableArray alloc]init];
                    
                    for(Movie *movie in  self.myFeed.itemsList){
                        
                        Genres *genreMovie;
                        for(Genres* genre in self.sfAppProfile.sfConfig.genres){
                            if (genre.idgenre  == [movie.idGenre integerValue]){
                                genreMovie = genre;
                            }
                        }
                        
                        if (([movie.status integerValue] == 1)){
                            [activeFilms addObject:movie];
                        }else if(([movie.status integerValue] == 4)
                                 && ([genreMovie.config.campaign.idcustomer integerValue] == 37)
                                    /* [movie.idGenre integerValue] == 54)*/){
                            
                            [pendingFilms addObject:movie];
                        }
                    
                    }
                    self.myFilms = [[NSMutableArray alloc]init];
                    [self.myFilms addObjectsFromArray:pendingFilms];
                    [self.myFilms addObjectsFromArray:activeFilms];
                    
                    [self.userFilmsTable reloadData];
                }
                
            }
        }else{
            
            NSLog(@"StayLog: callservicetogetmyfilms Error: %@",error.description);
            
            self.navigationItem.hidesBackButton = NO;
            [self.userFilmsTable setScrollEnabled:YES];
            [self.noFilmsView setHidden:YES];
            self.numberOfCells = 0;
            [self.userFilmsTable reloadData];
            [self.loadingView setHidden:YES];
            [self.loadingIndicator stopAnimating];
            
            [self.imageProfileButtom setHidden:NO];
            [self.profileNameButtom setHidden:NO];
            [self.profileLastNameButtom setHidden:NO];
            
            
            [self.imageProfileButtom setUserInteractionEnabled:YES];
            
        }
        
        

    }];

 }

#pragma mark - Actions

- (IBAction)openSettings:(id)sender {
    
    ProfileSettingsViewController* view = [[ProfileSettingsViewController alloc] init];
    view.delegate = self;
    view.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    view.transitioningDelegate = self;
    view.view.frame = self.view.frame;
    self.transitionAnimation.duration = 0.3;
    self.transitionAnimation.isPresenting = YES;
    self.transitionAnimation.originFrame = self.view.frame;
    
    view.sfAppProfile = self.sfAppProfile;

    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"settings"
                                                          action:@" "
                                                           label:@""
                                                           value:@1] build]];
    
    [self presentViewController:view animated:YES completion:nil];
}
- (IBAction)goToEditProfile:(id)sender {
    
    
   
    User *user = self.sfAppProfile.currentUser;
    [self didSelectEditProfile:user];
}

- (IBAction)dissmisProfile:(id)sender {
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"close-profile"
                                                          action:@" "
                                                           label:@""
                                                           value:@1] build]];
    [self.sfAppProfile.filmStrip resetSingletonDelegate];
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - StayfilmApp delegates

-(void)sfSingletonCreated {
    // nothing
}
-(void)connectivityChanged:(BOOL)isConnected {
    self.lbl_connectionStatus.text = NSLocalizedString(@"NO_INTERNET_RECONNECTING", nil);
//    [self.view bringSubviewToFront:self.view_connectionStatus];
    if(!isConnected) {
        self.view_connectionStatus.alpha = 0.0;
        [self.view_connectionStatus setHidden:NO];
        [UIView animateWithDuration:0.4 animations:^{
            self.view_connectionStatus.alpha = 1.0;
        }];
        if(self.connectionStatusTimer == nil) {
            __weak typeof(self) selfDelegate = self;
            self.connectionStatusTimer = [NSTimer timerWithTimeInterval:1.0 repeats:YES block:^(NSTimer * _Nonnull timer) {
                Reachability *reachability = [Reachability reachabilityForInternetConnection];
                NetworkStatus networkStatus = [reachability currentReachabilityStatus];
                
                if (networkStatus != NotReachable) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [selfDelegate connectivityChanged:YES];
                    });
                    [timer invalidate];
                    selfDelegate.connectionStatusTimer = nil;
                }
            }];
        }
    } else if(!self.view_connectionStatus.isHidden){
        self.view_connectionStatus.alpha = 1.0;
        [self.view_connectionStatus setHidden:NO];
        [UIView animateWithDuration:0.4 animations:^{
            self.view_connectionStatus.alpha = 0.0;
        } completion:^(BOOL finished) {
            [self.view_connectionStatus setHidden:YES];
        }];
    }
}


#pragma mark - ProfileStory Delegate

- (void)exitProfile {
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"close-profile"
                                                          action:@" "
                                                           label:@""
                                                           value:@1] build]];
    [self.sfAppProfile.filmStrip resetSingletonDelegate];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Waiting Notifications

-(void)showNotificationInfoModal{
    
    NotificationFilmInfoViewController *view = [[NotificationFilmInfoViewController alloc] init];
    view.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    view.view.frame = self.view.frame;
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"curation_info"
                                                          action:@" "
                                                           label:@""
                                                           value:@1] build]];
    
    
    [self presentViewController:view animated:YES completion:nil];
}


#pragma mark - Table delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.myFilms.count > 0){
        
        for(int i = 0;i<self.myFilms.count;i++){
            Movie *mov = self.myFilms[i];
            if([self.sfAppProfile.deletedFilms containsObject:mov.idMovie]){
                [self.myFilms removeObject:mov];
            }
        }
        
        return self.myFilms.count;
    }else{
        return self.numberOfCells;
    }
    
    
}

- (UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    
    
    if (self.myFilms.count > 0){

    Movie *movie = self.myFilms[indexPath.row];
        
            Genres *genreMovie;
            for(Genres* genre in self.sfAppProfile.sfConfig.genres){
                if (genre.idgenre  == [movie.idGenre integerValue]){
                    genreMovie = genre;
                }
            }
    
        
        if(([movie.status integerValue] == 4) && ([genreMovie.config.campaign.idcustomer integerValue] == 37))/*([movie.idGenre integerValue] == 54))*/{
            
            WaitingTableViewCell *cell = (WaitingTableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"profileWaitingCell"];
            cell.movie = movie;
            cell.tag = indexPath.row;
            [cell initCell];
            return cell;
            
        }else{
            ProfileTableViewCell *cell = (ProfileTableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"profileCell"];
            cell.delegate = self;
            cell.movie = movie;
            cell.tag = indexPath.row;
            [cell initCell];
            return cell;
        }

        
    }else if (self.noFilms){
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"noFilmsCell"];
        
        return cell;
    
    }else{
    
        UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"placeholderCell"];
        
        [cell.contentView setBounds:CGRectMake(cell.contentView.frame.origin.x, cell.contentView.frame.origin.y,[UIScreen mainScreen].bounds.size.width,cell.contentView.frame.size.height)];
        
        for (UIView *view in cell.contentView.subviews){
            
           
          
            [view showLoaderWithColors:@[[UIColor whiteColor],[UIColor groupTableViewBackgroundColor],[UIColor groupTableViewBackgroundColor],[UIColor groupTableViewBackgroundColor],[UIColor whiteColor]] animationDuration:2.8];
            
        }
        
        return cell;
    }
    
  
    
    
    
}
- (void)tableView:(UITableView *)tableView willDisplayCell: (UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == self.myFilms.count-1) {
        if (self.needPagination){
        [self callServiceToGetMyFilms];
           //self.needPagination = NO;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.noFilms){
        return 400;
    }else{
    return 150;
    }
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    //    if(section == 0){
    //        return @"Pending Films";
    //    }
    //    else if(section == 1){
    //        return @"Films";
    //    }
    //    else{
    return NSLocalizedString(@"FILMS", nil);
    //    }
}
- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,self.view.frame.size.width,75)];
    [headerView setBackgroundColor:[UIColor whiteColor]];
    
    UIView *bottomBorder = [[UIView alloc] initWithFrame:CGRectMake(0,35,self.view.frame.size.width,1)];
    [bottomBorder setBackgroundColor:[UIColor colorWithRed:0.87 green:0.86 blue:0.87 alpha:1.0]];
    
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(15, 8, 320, 20);
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor colorWithRed:0.20 green:0.34 blue:0.44 alpha:1.0];
    label.shadowOffset = CGSizeMake(-1.0, 1.0);
    label.font = [UIFont fontWithName:@"ProximaNova-Regular" size:20];
    label.text = NSLocalizedString(@"FILMS", nil);
    
    UILabel *notificationLabel = [[UILabel alloc] init];
    notificationLabel.textColor = [UIColor colorWithRed:0.20 green:0.34 blue:0.44 alpha:1.0];
    notificationLabel.numberOfLines = 0;
    notificationLabel.adjustsFontSizeToFitWidth = YES;
    notificationLabel.font = [UIFont fontWithName:@"ProximaNova-Regular" size:16];
    notificationLabel.text = NSLocalizedString(@"HOW_DISNEY_WORK", nil);
   
//    CGSize maximumLabelSize = CGSizeMake(self.view.frame.size.width - 50, 55);
//    CGSize expectedLabelSize = [notificationLabel sizeThatFits:maximumLabelSize];
    
    notificationLabel.frame = CGRectMake(15, 5, self.view.frame.size.width - 50, 20);
    
    UIView *notificationView = [[UIView alloc] initWithFrame:CGRectMake(0, 36, self.view.frame.size.width, 30)];
    [notificationView setBackgroundColor:[UIColor colorWithRed:0.97 green:0.97 blue:0.97 alpha:1.0]];
    
    UIButton *infoButton = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 30, 5, 20, 20)];
    [infoButton setBounds:CGRectMake(self.view.frame.size.width - 30, 5, 40, 40)];
    [infoButton setImage:[UIImage imageNamed:@"questionMarkGrey"] forState:UIControlStateNormal];
    [infoButton addTarget:self
               action:@selector(showNotificationInfoModal)
     forControlEvents:UIControlEventTouchUpInside];
    
    [headerView addSubview:label];
    [headerView addSubview:bottomBorder];
    [headerView addSubview:notificationView];
    [notificationView addSubview:notificationLabel];
    [notificationView addSubview:infoButton];

    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 75;
}

#pragma mark - Explorer Colletion View delegates


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.storiesArray.count;
}




- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
 ExplorerCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"explorerCell" forIndexPath:indexPath];
    
    
    if (_storiesArray.count > 0){
        Story *story = self.storiesArray[indexPath.row];
        [cell.storieImage sd_setImageWithURL:[NSURL URLWithString: story.thumbnailUrl] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            [cell.storieImage setImage:image];
            cell.storieImage.contentMode = UIViewContentModeScaleAspectFill;
        }];
        
        cell.storieTitleLabel.text = story.title;
        
        if (story.isStoryViewed){
            [cell.borderImage setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
            
        }else{
            [cell.borderImage setBackgroundColor:[UIColor colorWithRed:0.39 green:0.75 blue:0.60 alpha:1.0]];
            
        }
    }
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath  {
    
    //[self.exploreCollectionView setUserInteractionEnabled:NO];
    
//    for(ExplorerCollectionViewCell *cell in collectionView.visibleCells){
//
//        [cell setUserInteractionEnabled:NO];
//    }
    
    if (self.storiesArray.count > 0 ){
//        ExplorerCollectionViewCell *cell = (ExplorerCollectionViewCell*)[self.exploreCollectionView cellForItemAtIndexPath:indexPath];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Explorer" bundle:[NSBundle mainBundle]];
        StorieViewController *storieView = [storyboard instantiateViewControllerWithIdentifier:@"explorerStories"];
        
        storieView.delegate = self;
        
        storieView.storiesArray = self.storiesArray;
        storieView.storySelected = (int)indexPath.row;
        
        [self presentViewController:storieView animated:YES completion:nil];
        /*Story *story = self.storiesArray[(int)indexPath.row];
        
        UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        spinner.center = cell.storieImage.center;
        
        [cell addSubview:spinner];
       
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
           
         
                dispatch_async(dispatch_get_main_queue(), ^{
                    [spinner startAnimating];
                    
                });
                
                
                [story setPlayersWithCompletion:^{
                   
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.exploreCollectionView setUserInteractionEnabled:YES];
//                        for(ExplorerCollectionViewCell *cell in collectionView.visibleCells){
//
//                            [cell setUserInteractionEnabled:YES];
//                        }
                      
                        [spinner stopAnimating];
                        storieView.storySelected = (int)indexPath.row;
                        
                            [self presentViewController:storieView animated:YES completion:nil];
                        
                    });
                }];
          
        });*/
    
    }
    
    
}

#pragma mark - Settings delegates

- (void)didSelectEditProfile:(User *)user{
    if ([self.loadingView isHidden]){
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"user"
                                                              action:@"edit-profile"
                                                               label:@""
                                                               value:@1] build]];
        [self performSegueWithIdentifier:@"goToSettings" sender:user];
    }
}

#pragma mark - Video Options delegates
- (void)didSelectChangePrivacy:(NSInteger)videoPosition{
    
    
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"film-in-profile"
                                                          action:@"changing-privacy"
                                                           label:@""
                                                           value:@1] build]];
    
    
    ChangePrivacyViewController * view = [[ChangePrivacyViewController alloc] init];
    view.delegate = self;
    view.videoPosition = videoPosition;
    Movie *movie = self.myFilms[videoPosition];
    view.currentPrivacy = movie.permission;
    view.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    view.transitioningDelegate = self;
    view.view.frame = self.view.frame;
    self.transitionAnimation.duration = 0.3;
    self.transitionAnimation.isPresenting = YES;
    self.transitionAnimation.originFrame = self.view.frame;
    
    //view.sfAppProfile = sfAppProfile;
    
    [self presentViewController:view animated:YES completion:nil];
    
}

- (void)didSelectDownloadVideo:(NSInteger)videoPosition{
    
    self.filmToEdit = self.myFilms[videoPosition];
    
    
    //Getting the path of the document directory
    NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
    NSURL *dataPath = [documentsDirectoryURL URLByAppendingPathComponent:@"StayFilms"];
    
    //Create Stayfilm folder if it does not exist
    NSError *error;
    if (![[NSFileManager defaultManager] fileExistsAtPath:[dataPath path]])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:[dataPath path] withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
    }
    
    NSURL *fullURL = [dataPath URLByAppendingPathComponent:[NSString stringWithFormat:@"%@.mp4",self.filmToEdit.idMovie]];
    
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:[fullURL path]])
    {
        ProgressBarViewController *progressView = [[ProgressBarViewController alloc] init];
        progressView.downloadProgressBar.progress = 0;
        progressView.lbl_downloading.text = NSLocalizedString(@"DOWNLOADING", nil);
        progressView.delegate = self;
      
            
            progressView.transitioningDelegate = self;
            progressView.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            progressView.view.frame = [UIScreen mainScreen].bounds;
            self.transitionAnimation.duration = 0.1;
            self.transitionAnimation.isPresenting = YES;
            self.transitionAnimation.originFrame = [UIScreen mainScreen].bounds;
            
            [self presentViewController:progressView animated:YES completion:nil];
            
      
        
        
        [self downloadVideoFromURL:self.filmToEdit.videoUrl withProgress:^(CGFloat progress) {
            
            runOnMainQueueWithoutDeadlocking(^{
                
                //NSLog(@"%f", progress * 100);
                progressView.progressLabel.text = [NSString stringWithFormat:@"%2.0f%%", (progress * 100)];
                progressView.downloadProgressBar.progress = progress ;
                
                if (progress >= 1.0){
                    progressView.lbl_downloading.text = NSLocalizedString(@"DOWNLOADED", nil);
                    [progressView dismissViewControllerAnimated:YES completion:nil];
                    
                }
            });
            
        } completion:^(NSURL *filePath) {
            
            
//            if(UIVideoAtPathIsCompatibleWithSavedPhotosAlbum([filePath path])) {
//                UISaveVideoAtPathToSavedPhotosAlbum([filePath path],self,@selector(video:didFinishSavingWithError:contextInfo:),nil);
//            }
            
            NSString *collectionTitle = @"Stayfilm";
             // Find the album
             PHFetchOptions *fetchOptions = [[PHFetchOptions alloc] init];
             fetchOptions.predicate = [NSPredicate predicateWithFormat:@"title = %@", collectionTitle];
             PHAssetCollection *collection = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum subtype:PHAssetCollectionSubtypeAny options:fetchOptions].firstObject;
             
             // Combine (possible) album creation and adding in one change block
            [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
                PHAssetCollectionChangeRequest *albumRequest;
                if (collection == nil) {
                    // create the album if it doesn't exist
                    albumRequest = [PHAssetCollectionChangeRequest creationRequestForAssetCollectionWithTitle:collectionTitle];
                } else {
                    // otherwise request to change the existing album
                    albumRequest = [PHAssetCollectionChangeRequest changeRequestForAssetCollection:collection];
                }
                // add the asset to the album
                [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
                    PHAssetChangeRequest *assetChangeRequest = [PHAssetChangeRequest creationRequestForAssetFromVideoAtFileURL:filePath];
                    
                    // add asset
                    PHAssetCollectionChangeRequest *assetCollectionChangeRequest = [PHAssetCollectionChangeRequest changeRequestForAssetCollection:collection];
                    [assetCollectionChangeRequest addAssets:@[[assetChangeRequest placeholderForCreatedAsset]]];
                } completionHandler:^(BOOL success, NSError *error) {
                    if (!success) {
                        NSLog(@"Error: %@", error);
                    }
                }];
                
            } completionHandler:^(BOOL success, NSError *error) {
                if (!success) {
                    NSLog(@"error creating or adding to album: %@", error);
                }
            }];
            
            
            
            
            
        } onError:^(NSError *error) {
            NSLog(@"StayLog: didselectdownloadvideo %@", error);
        }];
        
    }else{
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ALREADY_DOWNLOADED", nil) message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:nil];
        
        [alert addAction:defaultAction];
        
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
}
    
- (void)               video: (NSString *) videoPath
    didFinishSavingWithError: (NSError *) error
                 contextInfo: (void *) contextInfo
{
    
    [self.view makeToast:NSLocalizedString(@"DOWNLOADED", nil)];
    
    [self.sfAppProfile cleanFilmFolder];
    
}

- (void)didSelectDeleteVideo:(NSInteger)videoPosition{
    
    self.filmToEdit = self.myFilms[videoPosition];
    
    Popover_Delete_ViewController * view = [[Popover_Delete_ViewController alloc] init];
    view.delegate = self;
    view.thumbnailUrl = self.filmToEdit.thumbnailUrl;
    view.analyticTag = @"delete_film-in-profile";
    view.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    view.transitioningDelegate = self;
    view.view.frame = self.view.frame;
    
    self.transitionAnimation.duration = 0.3;
    self.transitionAnimation.isPresenting = YES;
    self.transitionAnimation.originFrame = self.view.frame;
    
    [self presentViewController:view animated:YES completion:nil];
    
}
- (void)chooseOption:(NSString*)option{
    
    if ([option isEqualToString:@"confirmDelete"]) {
        
        if(self.filmToEdit != nil)
        {
            if(self.sfAppProfile == nil)
            {
                self.sfAppProfile = [StayfilmApp sharedStayfilmAppSingleton];
            }
            [self.sfAppProfile deleteMovieFromDevice:self.filmToEdit.videoUrl];
            [self.sfAppProfile saveSettings];
        }
        [self.sfAppProfile cleanRemakeState];
        
        [self callServiceToDeleteMovie];
        
//        if( [self.filmToEdit deleteMovie]){
//            
//            [self.view makeToast:@"Video Delete"];
//            [self callServiceToGetMyFilms];
//        }
    }else if ([option isEqualToString:@"cancelDownload"]){
        
        [self.downloadTask cancel];
        
        [self.manager.operationQueue cancelAllOperations];
        
        [self removeVideoAtPath:self.downloadVideopath];
    }
}

- (void)callServiceToDeleteMovie{
    
    [self.view makeToast:NSLocalizedString(@"DELETING", nil)];
    
    ShellWebService *service = [ShellWebService getSharedInstance];
    
    NSDictionary *parameters = @{ @"idMovie" : self.filmToEdit.idMovie
                                 };
    
    
    [service callService:parameters endpointName:endpointDeleteMovie withCompletionBlock:^(NSDictionary *result, NSError *error) {
        
        if(error == nil){
            
            [self.sfAppProfile.deletedFilms addObject:self.filmToEdit.idMovie];
            
            [self.myFilms removeObject:self.filmToEdit];
            self.needPagination = NO;
            [self.userFilmsTable reloadData];
            
            if ( self.myFilms.count == 0){
                
                [self.noFilmsView setHidden:NO];
                self.noFilms = YES;
                self.numberOfCells = 1;
                [self.userFilmsTable reloadData];
            }
            
            
            [self.view makeToast:NSLocalizedString(@"FILM_DELETED", nil)];
            
        }else{
            NSLog(@"StayLog: Error to Delete: %@",error.description);
        }
    }];
    
}

#pragma mark - Privacy Options delegates

- (void)changePrivacy:(int)privacy position:(NSInteger)videoPosition{
    
    [self.loadingView setHidden:NO];
    [self.loadingIndicator startAnimating];
    
    NSDictionary * args = @{@"privacy": [NSNumber numberWithInt:privacy],
                            @"position":[NSNumber numberWithInteger:videoPosition]
                            };
    [self performSelectorInBackground:@selector(change:)
                           withObject:args];
    
   
}

- (void)changePrivacy:(int)privacy {
    // nothing
}


-(void)change:(NSDictionary *)args{
    
    NSInteger videoPosition = [[args objectForKey:@"position"] integerValue];
    int privacy = [[args objectForKey:@"privacy"] intValue];
    
    Movie *movie = self.myFilms[videoPosition];
    
    switch (privacy) {
        case 7:
            movie.permission = MoviePermission_UNLISTED;
            break;
        case 3:
            movie.permission = MoviePermission_PUBLIC;
            break;
        case 2:
            movie.permission = MoviePermission_UNLISTED;
            break;
        case 1:
            movie.permission = MoviePermission_PRIVATE;
            break;
        default:
            movie.permission = MoviePermission_PUBLIC;
            break;
    }
    
    BOOL success = NO;
    
    success = [movie changePermisssionMovie];
    
    if(success)
    {
        //self.myFeed = [self getMyFilms];
        runOnMainQueueWithoutDeadlocking(^{
            
            
            [self callServiceToGetMyFilms];
            
            [self.loadingView setHidden:YES];
            [self.loadingIndicator stopAnimating];
        });
    }
    
    
}


#pragma mark - StoryState delegate

-(void)storiesChanged {
    
    self.storiesArray = [[NSMutableArray alloc] initWithArray: self.sfAppProfile.sfConfig.stories];
    [self.exploreCollectionView reloadData];
}

#pragma mark - Profile delegates

- (void)didSelectShare:(NSInteger)position {
    /*///////////////////////////////////////////////////////////////////////////////////////////////////////
     self.popoverShareView = [[Popover_ShareVideo_ViewController alloc] initWithDelegate:self];
     self.popoverShareView.modalPresentationStyle = UIModalPresentationOverCurrentContext;
     self.popoverShareView.transitioningDelegate = self;
     self.popoverShareView.view.frame = self.view.frame;
     self.transitionAnimation.duration = 0.3;
     self.transitionAnimation.isPresenting = YES;
     self.transitionAnimation.originFrame = self.view.frame;
     
     [self presentViewController:self.popoverShareView animated:YES completion:nil];
     ////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"film-in-profile"
                                                          action:@"share"
                                                           label:@""
                                                           value:@1] build]];
    
    self.popoverShareView = [[Popover_ShareVideo_ViewController alloc] initWithDelegate:self];
    self.popoverShareView.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    self.popoverShareView.transitioningDelegate = self;
    self.popoverShareView.view.frame = self.view.frame;
    self.transitionAnimation.duration = 0.3;
    self.transitionAnimation.isPresenting = YES;
    self.transitionAnimation.originFrame = self.view.frame;
    
    self.popoverShareView.movie = self.myFilms[position];
    self.shareFilmPosition = position;
    
    [self presentViewController:self.popoverShareView animated:YES completion:nil];
    
    //Google Analytics
    tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"profile_share-screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    //    Movie *movie = self.myFilms[position];
    //    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.stayfilm.com/movie/watch/%@", movie.idMovie]];
    //
    //    NSArray * shareItems = @[url];
    //
    //    InstagramActivity *instaActivity = [[InstagramActivity alloc] init];
    //    instaActivity.movie = movie;
    //    instaActivity.profileView = self;
    //    instaActivity.videoFinishView = nil;
    //    instaActivity.view = self.view;
    //    instaActivity.sf = sfAppProfile;
    //
    //
    //    DownloadActivity *downloadActivity = [[DownloadActivity alloc] init];
    //    downloadActivity.movie = movie;
    //    downloadActivity.view = self;
    //    downloadActivity.profileView = self;
    //    downloadActivity.videoFinishView = nil;
    //
    //    UIActivityViewController * avc = [[UIActivityViewController alloc] initWithActivityItems:shareItems applicationActivities:@[instaActivity,downloadActivity]];
    //
    //
    //    NSArray *excludeActivities = @[UIActivityTypePrint,
    //                                   UIActivityTypeAssignToContact,
    //                                   UIActivityTypeSaveToCameraRoll,
    //                                   UIActivityTypeAddToReadingList,
    //                                   UIActivityTypeAirDrop,
    //                                   UIActivityTypeMessage,
    //                                   UIActivityTypeMail,
    //                                   UIActivityTypePostToFlickr,
    //                                   UIActivityTypePostToVimeo,
    //                                   UIActivityTypePostToTencentWeibo,
    //                                   UIActivityTypePostToWeibo];
    //
    //    avc.excludedActivityTypes = excludeActivities;
    //
    //    __weak typeof(avc) selfDelegate = avc;
    //    [avc setCompletionWithItemsHandler:^(UIActivityType  _Nullable activityType, BOOL completed, NSArray * _Nullable returnedItems, NSError * _Nullable activityError) {
    //
    //        NSLog(@"TYPE: %@", activityType);
    //        NSLog(@"returnedItems: %@", returnedItems);
    //        NSLog(@"Error: %@", activityError);
    //        /*net.whatsapp.WhatsApp.ShareExtension
    //        //DownloadActivity */
    //        if ([activityType isEqual:@"com.apple.UIKit.activity.CopyToPasteboard"]){
    //
    //            [self.view makeToast:NSLocalizedString(@"LINK_COPIED", nil)];
    //
    //        }else if([activityType isEqual:@"com.apple.UIKit.activity.PostToTwitter"]){
    //
    //            [self.view makeToast:NSLocalizedString(@"SHARED_LOWERED", nil)];
    //
    //        }
    //
    //        selfDelegate.completionWithItemsHandler = nil;
    //    }];
    //
    //    [self presentViewController:avc animated:YES completion:nil];
    
}


- (void)didSelectOptions:(NSInteger)position{
    
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"film-in-profile"
                                                          action:@"film-options"
                                                           label:@""
                                                           value:@1] build]];
    
    VideoOptionsViewController* view = [[VideoOptionsViewController alloc] init];
    view.delegate = self;
    view.videoPosition = (int)position;
    view.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    view.transitioningDelegate = self;
    view.view.frame = self.view.frame;
    
    self.transitionAnimation.duration = 0.3;
    self.transitionAnimation.isPresenting = YES;
    self.transitionAnimation.originFrame = self.view.frame;
    
    [self presentViewController:view animated:YES completion:nil];
    
}
- (void)didTapPlay:(NSInteger)position{
    
    //Google Analytics Event
   id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"film-in-profile"
                                                          action:@"play"
                                                           label:@""
                                                           value:@1] build]];
    
    Movie * movie = self.myFilms[position];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue]  < 11.0 ){
        
        AVPlayerViewController *playerVC = [[AVPlayerViewController alloc]init];
        playerVC.player = [AVPlayer playerWithURL:[NSURL URLWithString:movie.videoUrl]];
        [playerVC setShowsPlaybackControls:YES];
        [playerVC.player play];
        [self presentViewController:playerVC animated:YES completion:nil];
        
    }else{
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"Player" owner:self options:nil];
        PlayerViewController *moviePlayerViewController = [topLevelObjects objectAtIndex:0];
        moviePlayerViewController.currentMovie = movie;
        
        moviePlayerViewController.showsPlaybackControls = YES;
        
        moviePlayerViewController.player = [AVPlayer playerWithURL:[NSURL URLWithString:movie.videoUrl]];
        
        
        moviePlayerViewController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        moviePlayerViewController.transitioningDelegate = self;
        moviePlayerViewController.view.frame = self.view.frame;
        
        self.transitionAnimation.duration = 0.3;
        self.transitionAnimation.isPresenting = YES;
        self.transitionAnimation.originFrame = self.view.frame;
        
        
        [self presentViewController:moviePlayerViewController animated:YES completion:nil];
    }
    
}
- (void)didTapPrivacy:(NSInteger)position{
    
    [self didSelectChangePrivacy:position];
}

#pragma mark - Animation Delegates

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source {
    return self.transitionAnimation;
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    self.transitionAnimation.isPresenting = NO;
    return self.transitionAnimation;
}

#pragma mark - Popover Delegates

-(void)decidedOption:(NSString *)p_option {
    
    if ([p_option isEqualToString: @"done"]){
        //[self dismissViewControllerAnimated:YES completion:nil];
        self.myFilms[self.shareFilmPosition] = self.popoverShareView.movie;
        [self dismissModalMenu];
        [self.userFilmsTable reloadData];
    } else if([p_option isEqualToString:@"changedPrivacy"]) {
        [self.userFilmsTable reloadData];
    }
    
}

- (void)dismissModalMenu {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)dismissModalView
{
    [self.sfAppProfile.filmStrip resetSingletonDelegate];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
#pragma mark - Prepare For Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"goToSettings"]) {
        
        EditProfileViewController *editVC = segue.destinationViewController;
        editVC.sfAppProfile = self.sfAppProfile;
        
    }
    
}
#pragma mark - Download Film

- (void) downloadVideoFromURL: (NSString *) URL withProgress:(void (^)(CGFloat progress))progressBlock completion:(void (^)(NSURL *filePath))completionBlock onError:(void (^)(NSError *error))errorBlock
{
    //Configuring the session manager
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    self.manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    //Most URLs I come across are in string format so to convert them into an NSURL and then instantiate the actual request
    NSURL *formattedURL = [NSURL URLWithString:URL];
    NSURLRequest *request = [NSURLRequest requestWithURL:formattedURL];
    
    //Watch the manager to see how much of the file it's downloaded
    [self.manager setDownloadTaskDidWriteDataBlock:^(NSURLSession *session, NSURLSessionDownloadTask *downloadTask, int64_t bytesWritten, int64_t totalBytesWritten, int64_t totalBytesExpectedToWrite) {
        //Convert totalBytesWritten and totalBytesExpectedToWrite into floats so that percentageCompleted doesn't get rounded to the nearest integer
        CGFloat written = totalBytesWritten;
        CGFloat total = totalBytesExpectedToWrite;
        CGFloat percentageCompleted = written/total;
        
        //Return the completed progress so we can display it somewhere else in app
        progressBlock(percentageCompleted);
    }];
    
    //Start the download
    self.downloadTask = [self.manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        
        //Getting the path of the document directory
        NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
        NSURL *dataPath = [documentsDirectoryURL URLByAppendingPathComponent:@"StayFilms"];
        
        //Create Stayfilm folder if it does not exist
        NSError *error;
        if (![[NSFileManager defaultManager] fileExistsAtPath:[dataPath path]])
        {
            [[NSFileManager defaultManager] createDirectoryAtPath:[dataPath path] withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
        }
        
        NSURL *fullURL = [dataPath URLByAppendingPathComponent:[NSString stringWithFormat:@"%@.mp4",self.filmToEdit.idMovie]];
        
        self.downloadVideopath = fullURL;
        
//        //If we already have a video file saved, remove it from the phone
//        [self removeVideoAtPath:fullURL];
        return fullURL;
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        if (!error) {
            //If there's no error, return the completion block
            
            completionBlock(filePath);
        } else {
            //Otherwise return the error block
            errorBlock(error);
        }
        
    }];
    
    [self.downloadTask resume];
}
- (void)removeVideoAtPath:(NSURL *)filePath
{
    NSString *stringPath = filePath.path;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:stringPath]) {
        [fileManager removeItemAtPath:stringPath error:NULL];
    }
}

@end
