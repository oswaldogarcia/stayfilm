//
//  main.m
//  Stayfilm-iOS-Full
//
//  Created by Henrique Ormonde on 18/05/17.
//  Copyright © 2017 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
         return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
