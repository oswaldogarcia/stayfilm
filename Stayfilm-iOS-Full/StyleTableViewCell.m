//
//  StyleTableViewCell.m
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 21/05/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

@import KALoader;

#import "StyleTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "TemplateCollectionViewCell.h"
#import "Constants.h"

@implementation StyleTableViewCell 



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.templateHeightConstraint setConstant:0.0];

    [self.genreImageView setBounds:CGRectMake(self.frame.origin.x, self.frame.origin.y,[UIScreen mainScreen].bounds.size.width,self.frame.size.height)];
    
    self.needScroll = YES;
    
}


- (void)initCell{
    
    if(!self.isVerticalAvailable){
        
        
        self.verticalButton.backgroundColor = [UIColor colorWithRed:0.97 green:0.97 blue:0.97 alpha:1.0];;
        [self.verticalButton setImage:[UIImage imageNamed:@"iconVerticalGrey"] forState:UIControlStateNormal];
        [self.verticalButton setTitleColor:[UIColor colorWithRed:0.87 green:0.86 blue:0.87 alpha:1.0] forState:UIControlStateNormal];
        
    }
    
    if(self.isPremium){
        
        [self.premiumTagImage setHidden:NO];
        if(self.isBlocked){
            [self.verticalButton setEnabled:NO];
            [self.horizontalButton setEnabled:NO];
            
            [self.premiumTagImage setImage:[UIImage imageNamed:@"premiumUnlocked"]];
            
            self.verticalButton.backgroundColor = [UIColor colorWithRed:0.97 green:0.97 blue:0.97 alpha:1.0];;
            [self.verticalButton setImage:[UIImage imageNamed:@"iconVerticalGrey"] forState:UIControlStateNormal];
            [self.verticalButton setTitleColor:[UIColor colorWithRed:0.87 green:0.86 blue:0.87 alpha:1.0] forState:UIControlStateNormal];
            self.horizontalButton.backgroundColor = [UIColor colorWithRed:0.97 green:0.97 blue:0.97 alpha:1.0];;
            [self.horizontalButton setImage:[UIImage imageNamed:@"iconHorizontalGrey"] forState:UIControlStateNormal];
            [self.horizontalButton setTitleColor:[UIColor colorWithRed:0.87 green:0.86 blue:0.87 alpha:1.0] forState:UIControlStateNormal];
            
        }else{
            [self.premiumTagImage setImage:[UIImage imageNamed:@"premiumActive"]];
            
            [self.verticalButton setEnabled:YES];
            [self.horizontalButton setEnabled:YES];
            
            self.horizontalButton.backgroundColor = [UIColor colorWithRed:0.39 green:0.75 blue:0.60 alpha:1.0];
            [self.horizontalButton setImage:[UIImage imageNamed:@"iconHorizontalWhite"] forState:UIControlStateNormal];
            [self.horizontalButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            
            if(self.isVerticalAvailable){
            self.verticalButton.backgroundColor = [UIColor colorWithRed:0.97 green:0.97 blue:0.97 alpha:1.0];;
            [self.verticalButton setImage:[UIImage imageNamed:@"iconVerticalBlue"] forState:UIControlStateNormal];
            [self.verticalButton setTitleColor:[UIColor colorWithRed:0.20 green:0.34 blue:0.44 alpha:1.0] forState:UIControlStateNormal];
            }
        }
        
    }else{
        [self.premiumTagImage setHidden:YES];
    }
    

    [self.verticalButton setTitle:NSLocalizedString(@"VERTICAL", nil) forState:UIControlStateNormal];
    [self.horizontalButton setTitle:NSLocalizedString(@"HORIZONTAL", nil) forState:UIControlStateNormal];
    
    [self.horizontalButton setHidden:YES];
    [self.verticalButton setHidden:YES];

    [self.genreImageView showLoader];
    NSString *langcode = [[NSLocale currentLocale] objectForKey:NSLocaleLanguageCode];
    
    NSString *imageURL;
    
    if([langcode isEqualToString:@"en"]){
        imageURL = self.selectedGenre.imageUrl_en;
    }else if([langcode isEqualToString:@"es"]){
        imageURL = self.selectedGenre.imageUrl_es;
    }else if([langcode isEqualToString:@"pt"]){
        imageURL = self.selectedGenre.imageUrl;
    }else{
        imageURL = self.selectedGenre.imageUrl_en;
    }
    
       imageURL = self.selectedGenre.imageUrl;//mientras no hay imagen en los otros idiomas 

    [self.genreImageView sd_setImageWithURL:[NSURL URLWithString: imageURL]  placeholderImage:nil options:SDWebImageRefreshCached progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL * _Nullable targetURL) {/*...*/ } completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        if([self.genreImageView isLoaderShowing]){

            [self.genreImageView hideLoader];
        }

        [self.genreImageView setImage:image];
    }];
    
    if(self.isVertical){
        
    }
  

}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if([[UIScreen mainScreen] bounds].size.height >= kIPhone5Height){
        if (self.ipadTime){
            self.ipadTime = NO;
            return;
        }
    }
    if([[UIScreen mainScreen] bounds].size.height >= kIPadHeight){
       
        self.ipadTime = YES;
    }
    
    if (!self.isSelectedByUser){
        
        self.isSelectedByUser = selected;
        if(selected){
            
            //self.isSelectedByUser = selected;
            [UIView animateWithDuration:0.8 animations:^{
                
                
                [self.templatesCollectionView reloadData];
                
                [self.templateArrowImage setHidden:NO];
                if([[UIScreen mainScreen] bounds].size.height == kIPhone5Height){
                    [self.imageheight setConstant:kSelectedTemplateSmallImageHeight];
                }else{
                    [self.imageheight setConstant:kSelectedTemplateImageHeight];
                }
                [self.imageLeadingConstraint setConstant:15.0];
                [self.imageTrailingConstaint setConstant:15.0];
                [self.templateHeightConstraint setConstant:kSelectedTemplateHeightConstraint];
                
                
                [self.horizontalButton setHidden:NO];
                [self.verticalButton setHidden:NO];
            }];
            
//            if(self.isVertical){
//                [self.delegate setFilmOrintation:VERTICAL];
//            }else{
//                [self.delegate setFilmOrintation:HORIZONTAL];
//            }
            
        }else{
        
//            self.isSelectedByUser = selected;
            [UIView animateWithDuration:0.3 animations:^{
                [self.templateArrowImage setHidden:YES];

                if([[UIScreen mainScreen] bounds].size.height == kIPhone5Height){
                    [self.imageheight setConstant:kSmallIPhoneCellSize];
                }else if([[UIScreen mainScreen] bounds].size.height >= kIPadHeight){
                    
                    [self.imageheight setConstant:kIPadCellSize];
                    
                }else{
                    [self.imageheight setConstant:kIPhoneCellSize];
                }
            [self.templateHeightConstraint setConstant:0.0];
            [self.imageLeadingConstraint setConstant:0.0];
            [self.imageTrailingConstaint setConstant:0.0];
                
            [self.horizontalButton setHidden:YES];
            [self.verticalButton setHidden:YES];
            }];
            
        }
    }else{
        
        self.isSelectedByUser = NO;
        
        [UIView animateWithDuration:0.3 animations:^{
            [self.templateArrowImage setHidden:YES];
            
            if([[UIScreen mainScreen] bounds].size.height == kIPhone5Height){
                [self.imageheight setConstant:kSmallIPhoneCellSize];
            }else if([[UIScreen mainScreen] bounds].size.height >= kIPadHeight){
                
                [self.imageheight setConstant:kIPadCellSize];
                
            }else{
                [self.imageheight setConstant:kIPhoneCellSize];
            }
            [self.templateHeightConstraint setConstant:0.0];
            [self.imageLeadingConstraint setConstant:0.0];
            [self.imageTrailingConstaint setConstant:0.0];
            [self.delegate didDeselectTemplate];
            
            [self.horizontalButton setHidden:YES];
            [self.verticalButton setHidden:YES];
        }];
        
        
    
    }
    
    // editing
    
    if (_isEditing){
       
        [self.templatesCollectionView reloadData];
        [self.templateHeightConstraint setConstant:kSelectedTemplateHeightConstraint];
        
        [self.templateArrowImage setHidden:NO];
        if([[UIScreen mainScreen] bounds].size.height == kIPhone5Height){
            [self.imageheight setConstant:kSelectedTemplateSmallImageHeight];
        }else{
            [self.imageheight setConstant:kSelectedTemplateImageHeight];
        }
        [self.imageLeadingConstraint setConstant:15.0];
        [self.imageTrailingConstaint setConstant:15.0];
        
        
        self.isEditing = NO;
        
        [self.horizontalButton setHidden:NO];
        [self.verticalButton setHidden:NO];
        
       
        
    }
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.selectedGenre.templates.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TemplateCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"templateCell" forIndexPath:indexPath];
    
    Template *template = self.selectedGenre.templates[indexPath.row];
    
    cell.currentTemplate = template;
    
    if(self.isPremium && self.isBlocked){
        cell.isBlocked = YES;
    }else if(!self.isPremium && self.isBlocked){
        cell.isBlocked = template.isPaid;
    }else if(!self.isPremium && !self.isBlocked){
        cell.isBlocked = NO;
    }else if(self.isPremium && !self.isBlocked){
            cell.isBlocked = NO;
    }else{
        cell.isBlocked = template.isPaid;
    }
    
    cell.isVertical = self.isVertical;
    cell.delegate = self;
    [cell initCell];
    
    [self scrollToCurrentTemplate];
    
    if(indexPath.row == self.currentTemplateSelection){
        
        [cell setSelected:YES];

        if(cell.isBlocked){
            self.currentTemplateSelection = -1;
            [self.delegate didDeselectTemplate];
        }else{
            [self.delegate didSelectTemplate:self.selectedGenre Template:self.selectedGenre.templates[indexPath.row]];
        }
        
    }else{
        [cell setSelected:NO];
    }

    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath  {
    
    if (self.currentTemplateSelection  != (int)indexPath.row){
        
        self.currentTemplateSelection  = (int)indexPath.row;
        
        TemplateCollectionViewCell *cell =(TemplateCollectionViewCell*) [collectionView cellForItemAtIndexPath:indexPath];
        if(cell.isBlocked){
            self.currentTemplateSelection = -1;
            [self.delegate didDeselectTemplate];
        }else{
             [self.delegate didSelectTemplate:self.selectedGenre Template:self.selectedGenre.templates[indexPath.row]];
            [collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        }
        
    }else{
        self.currentTemplateSelection = -1;
        [self.delegate didDeselectTemplate];
    }
    
    [collectionView reloadData];
//    NSLog(@"%d", (int)indexPath.row);
    
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    //horizontal 262/152
    //vertical 91/152
    
    int width = 262;
    
    if(self.isVertical){
        width = 91;
    }
    return CGSizeMake(width, 152);
   
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    int width = 262;
    
    if(self.isVertical){
        width = 91;
    }
    
    CGFloat left = (UIScreen.mainScreen.bounds.size.width - width) / 2;
    
    return UIEdgeInsetsMake(20, left, 20, 20);
}
- (IBAction)changeToVertical:(id)sender {
   
    if(!self.isVerticalAvailable){
        
        [self.delegate noAvailableOrientation];
        
    }else{
        
        if(!self.isVertical){
            
            self.verticalButton.backgroundColor = [UIColor colorWithRed:0.39 green:0.75 blue:0.60 alpha:1.0];
            [self.verticalButton setImage:[UIImage imageNamed:@"iconVerticalWhite"] forState:UIControlStateNormal];
            [self.verticalButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            
            self.horizontalButton.backgroundColor = [UIColor colorWithRed:0.97 green:0.97 blue:0.97 alpha:1.0];;
            [self.horizontalButton setImage:[UIImage imageNamed:@"iconHorizontalBlue"] forState:UIControlStateNormal];
            [self.horizontalButton setTitleColor:[UIColor colorWithRed:0.20 green:0.34 blue:0.44 alpha:1.0] forState:UIControlStateNormal];
            
            self.isVertical = YES;
            self.needScroll = YES;
            [self.delegate setFilmOrintation:VERTICAL];
            [self changeTemplatesSize];
        }
    }
    
}
- (IBAction)changeToHorizontal:(id)sender {
    
    if(!self.isVerticalAvailable){
        self.verticalButton.backgroundColor = [UIColor colorWithRed:0.97 green:0.97 blue:0.97 alpha:1.0];;
        [self.verticalButton setImage:[UIImage imageNamed:@"iconVerticalGrey"] forState:UIControlStateNormal];
        [self.verticalButton setTitleColor:[UIColor colorWithRed:0.87 green:0.86 blue:0.87 alpha:1.0] forState:UIControlStateNormal];
    }
  
    
    if(self.isVertical){
        self.horizontalButton.backgroundColor = [UIColor colorWithRed:0.39 green:0.75 blue:0.60 alpha:1.0];
        [self.horizontalButton setImage:[UIImage imageNamed:@"iconHorizontalWhite"] forState:UIControlStateNormal];
        [self.horizontalButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
       
        self.verticalButton.backgroundColor = [UIColor colorWithRed:0.97 green:0.97 blue:0.97 alpha:1.0];;
        [self.verticalButton setImage:[UIImage imageNamed:@"iconVerticalBlue"] forState:UIControlStateNormal];
        [self.verticalButton setTitleColor:[UIColor colorWithRed:0.20 green:0.34 blue:0.44 alpha:1.0] forState:UIControlStateNormal];
        
        self.isVertical = NO;
        self.needScroll = YES;
        [self.delegate setFilmOrintation:HORIZONTAL];
        [self changeTemplatesSize];
    }
}
-(void)changeTemplatesSize{
    
    TemplateCollectionViewCell *cell = (TemplateCollectionViewCell*)[self.templatesCollectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:self.currentTemplateSelection inSection:0]];
    
    [cell.frameTemplateImage setAlpha:0.0];
    
    [self.templatesCollectionView  performBatchUpdates:^{
        [self.templatesCollectionView reloadData];
        
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:0.3 animations:^{
            
            [cell.frameTemplateImage setAlpha:1.0];
            
            UIImage *image;
            if(self.isVertical){
                image = [UIImage imageNamed:@"frameStyleSmall"];
            }else{
                image = [UIImage imageNamed:@"transparentFrame"];
            }
            [cell.frameTemplateImage setImage:image];
        }];

    }];
}

- (void)scrollToCurrentTemplate {
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.currentTemplateSelection inSection:0];
    
    if(self.needScroll){
        [self.templatesCollectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
            self.needScroll = NO;
    }
}

- (void)unlockTemplate:(Template *)selectedTemplate{
    
    [self.delegate unlockStyle:selectedTemplate];
    
}
@end
