//
//  ReorderCollectionViewCell.h
//  Stayfilm-iOS-Full
//
//  Created by Rick on 04/12/17.
//  Copyright © 2017 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReorderCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;
@property (weak, nonatomic) IBOutlet UIView *numberView;
@property (weak, nonatomic) IBOutlet UILabel *txt_Number;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property (weak, nonatomic) IBOutlet UIImageView *vid_Gradient;
@property (weak, nonatomic) IBOutlet UIImageView *vid_Play;
@property (weak, nonatomic) IBOutlet UILabel *vid_Time;

@property (strong, nonatomic) NSString *source;
@property (strong, nonatomic) NSNumber *duration;

@property (strong) NSURLSessionDataTask *task;

@end
