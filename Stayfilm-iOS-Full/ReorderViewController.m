//
//  ReorderViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Rick on 29/11/17.
//  Copyright © 2017 Stayfilm. All rights reserved.
//

#import "ReorderViewController.h"
#import <Lottie/Lottie.h>
#import <Google/Analytics.h>
#import "StayfilmApp.h"
#import "ReorderCollectionViewCell.h"
#import "Step1ConfirmationViewController.h"

@interface ReorderViewController ()

@property (nonatomic, weak) StayfilmApp *sfApp;
@property (nonatomic, strong) Uploader* uploaderStayfilm;
@property (strong, nonatomic) LOTAnimationView *animation;
@property (assign) BOOL reordered;

@property (nonatomic, strong) NSTimer *timer;

@end

@implementation ReorderViewController

@synthesize selectedMedias, initialSelectedMedias, videoImageManager, thumbConnection, animationView, animation, reordered, timer, navigationController;

static NSString *cellIdentifierReorder = @"ReorderItemCell";


-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
     self.automaticallyAdjustsScrollViewInsets = NO;
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    self.thumbConnection = [NSURLSession sessionWithConfiguration:config];
    
    self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    self.uploaderStayfilm = [Uploader sharedUploadStayfilmSingletonWithDelegate:self];
    
//    self.animation = [LOTAnimationView animationNamed:@"van"];
//    self.animation.frame = CGRectMake(0, 0, self.animationView.frame.size.width, self.animationView.frame.size.height);
//    self.animation.contentMode = UIViewContentModeScaleAspectFit;
//    self.animation.loopAnimation = YES;
//    
//    [self.animationView addSubview:self.animation];
    
    self.reordered = NO;
    
    if(self.videoImageManager == nil) {
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            switch (status) {
                case PHAuthorizationStatusAuthorized:
                {
                    self.videoImageManager = [[PHCachingImageManager alloc] init];
                }
                    break;
                case PHAuthorizationStatusDenied:
                case PHAuthorizationStatusRestricted:
                case PHAuthorizationStatusNotDetermined:
                {
                    self.videoImageManager = nil;
                }
                    break;
                default:
                    break;
            }
        }];
    }
//    if(self.videoImageManager == nil)
//        self.videoImageManager = [[PHCachingImageManager alloc] init];
    
    self.selectedMedias = [self.initialSelectedMedias mutableCopy];
    
    [self.draggableCollection registerClass:[ReorderCollectionViewCell class] forCellWithReuseIdentifier:cellIdentifierReorder];
    
    UIApplication *app = [UIApplication sharedApplication];
    CGFloat statusBarHeight = app.statusBarFrame.size.height;
    UIView *statusBarView =  [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, statusBarHeight)];
    statusBarView.backgroundColor  =  [UIColor colorWithRed:51.0/255 green:87.0/255 blue:113.0/255 alpha:1.0];
    [self.view addSubview:statusBarView];
    
    [self setNeedsStatusBarAppearanceUpdate];
}
- (void)viewWillAppear:(BOOL)animated{
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = [UIColor colorWithRed:0.20 green:0.34 blue:0.44 alpha:1.0];
    }
    
    if(self.sfApp == nil)
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    [self.sfApp.filmStrip setViewInsideFilmStrip];
    
    if(self.sfApp.isEditingMode) {
        [self.sfApp.filmStrip changeTopStatusBarColorToWhite:YES];
    } else {
        [self.sfApp.filmStrip changeTopStatusBarColorToWhite:NO];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
//    [self.animation play];
    
    if(self.sfApp == nil)
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    self.uploaderStayfilm = [Uploader sharedUploadStayfilmSingletonWithDelegate:self];

    [self.sfApp.filmStrip setViewInsideFilmStrip];
    
    if(self.sfApp.isEditingMode) {
        //Google Analytics
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:@"film-editing_content_reorder"];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    } else {
        //Google Analytics
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:@"moviemaker_content_reorder"];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Buttons clicks

- (IBAction)but_ConfirmReorder_Clicked:(id)sender {
    
    for (UIViewController *view in self.navigationController.viewControllers) {
        if([view isKindOfClass:[Step1ConfirmationViewController class]])
        {
            if(self.sfApp.isEditingMode) {
                [view performSelectorOnMainThread:@selector(changeEditedMedias:) withObject:self.selectedMedias waitUntilDone:YES];
            } else {
                [view performSelectorOnMainThread:@selector(changeSelectedMedias:) withObject:self.selectedMedias waitUntilDone:YES];
            }
            break;
        }
    }
    
    [self.selectedMedias removeAllObjects];
    self.selectedMedias = nil;
    self.initialSelectedMedias = nil;
    [self.draggableCollection removeFromSuperview];
    self.draggableCollection = nil;
    self.thumbConnection = nil;
    self.animationView = nil;
    //[self.navigationController popViewControllerAnimated:YES];
    
    if(self.sfApp.isEditingMode) {
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"edit-film"
                                                              action:@"reorder"
                                                               label:@"ok"
                                                               value:@1] build]];
    } else {
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"film-production"
                                                              action:@"reorder"
                                                               label:@"ok"
                                                               value:@1] build]];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)backAction:(id)sender {
    
    [self backButtonPressed];
    
}

- (void)backButtonPressed
{
    
    // this next commented code is here so if we decide to include the confirmation of unsaved changes feature again.
    @try {
        if(![StayfilmApp isArray:self.selectedMedias equalToArray:self.initialSelectedMedias])
        {
            UIAlertController* alertc = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ARE_YOU_SURE", nil)
                                                                            message:NSLocalizedString(@"CONFIRM_UNWIND_FROM_REORDER", nil)
                                                                     preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction* okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {
                                                                 [alertc dismissViewControllerAnimated:YES completion:nil];
                                                                // [self.navigationController popViewControllerAnimated:YES];
                                                                 
                                                                 if(self.sfApp.isEditingMode) {
                                                                     //Google Analytics Event
                                                                     id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                                                                     [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"edit-film"
                                                                                                                           action:@"reorder"
                                                                                                                            label:@"cancel"
                                                                                                                            value:@1] build]];
                                                                 } else {
                                                                     //Google Analytics Event
                                                                     id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                                                                     [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"film-production"
                                                                                                                           action:@"reorder"
                                                                                                                            label:@"cancel"
                                                                                                                            value:@1] build]];
                                                                 }
                                                                 [self dismissViewControllerAnimated:YES completion:nil];
                                                             }];
            UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"STAY", nil) style:UIAlertActionStyleCancel
                                                                 handler:^(UIAlertAction * action) {
                                                                     [alertc dismissViewControllerAnimated:YES completion:nil];
                                                                 }];

            [alertc addAction:okAction];
            [alertc addAction:cancelAction];
            [self presentViewController:alertc animated:YES completion:nil];
        }
        else
        {
            [self.selectedMedias removeAllObjects];
            self.selectedMedias = nil;
            self.initialSelectedMedias = nil;
            [self.draggableCollection removeFromSuperview];
            self.draggableCollection = nil;
            self.animationView = nil;
            self.thumbConnection = nil;

            for (UIViewController *view in self.navigationController.viewControllers) {
                if([view isKindOfClass:[Step1ConfirmationViewController class]])
                {
                    [view performSelectorOnMainThread:@selector(clearAlbumViewController) withObject:nil waitUntilDone:NO];
                    break;
                }
            }

            //[self.navigationController popViewControllerAnimated:YES];
            [self dismissViewControllerAnimated:YES completion:nil];
        }

    }
    @catch (NSException *exception) {
        NSLog(@"StayLog: EXCEPTION on backButtonPressed with error: %@", exception.description);
    }
}

#pragma mark - Draggable Collection View Methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    [(UIActivityIndicatorView *)[self.view viewWithTag:10] stopAnimating];
    if(self.selectedMedias != nil)
        return [self.selectedMedias count];
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ReorderCollectionViewCell *cell = (ReorderCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifierReorder forIndexPath:indexPath];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ReorderCollectionViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    if(cell.task)
    {
        [cell.task cancel];
    }
    
    cell.photoImageView.image = nil;
    [cell.spinner startAnimating];
    [cell.spinner setHidden:NO];
    [cell.numberView setHidden:NO];
    cell.txt_Number.text = [@(indexPath.item + 1) stringValue];
    if(self.reordered) {
        cell.numberView.alpha = 0.0;
    } else {
        [self performSelectorOnMainThread:@selector(executeAnimationforCell:) withObject:cell waitUntilDone:NO];
    }
    
    cell.photoImageView.alpha = 1.0;
    cell.source = nil;
    
    if([[self.selectedMedias objectAtIndex:indexPath.item] isKindOfClass:[Media class]]) {
        Media * media = [self.selectedMedias objectAtIndex:indexPath.item];
        cell.source = media.source;
    }
    else if([[self.selectedMedias objectAtIndex:indexPath.item] isKindOfClass:[PhotoFB class]]) {
        PhotoFB * passet = [self.selectedMedias objectAtIndex:indexPath.item];
        cell.source = passet.source;
    }
    else if([[self.selectedMedias objectAtIndex:indexPath.item] isKindOfClass:[VideoFB class]]) {
        VideoFB * passet = [self.selectedMedias objectAtIndex:indexPath.item];
        cell.source = passet.source;
    }
    if(cell.source != nil) {
        if(self.sfApp == nil)
        {
            self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
        }
        
        if([self.sfApp.imagesCache objectForKey:cell.source] != nil)
        {
            cell.photoImageView.image = [self.sfApp.imagesCache objectForKey:cell.source];
            [cell.spinner stopAnimating];
            [cell.spinner setHidden:YES];
            if([cell.source containsString:@".mp4"] ||
               [cell.source containsString:@".avi"] ||
               [cell.source containsString:@".mpg"] ||
               [cell.source containsString:@".mov"] ){
                [cell.vid_Play setHidden:NO];
                [cell.vid_Time setHidden:NO];
                [cell.vid_Gradient setHidden:NO];
                if([[self.selectedMedias objectAtIndex:indexPath.item] isKindOfClass:[VideoFB class]]) {
                    cell.duration = ((VideoFB*)[self.selectedMedias objectAtIndex:indexPath.item]).length;
                    long seconds = lroundf([cell.duration floatValue]);
                    int hour = (int)seconds / 3600;
                    int mins = (seconds % 3600) / 60;
                    int secs = seconds % 60;
                    if(hour > 0) {
                        cell.vid_Time.text = [NSString stringWithFormat:@"%d:%02d:%02d",hour,mins,secs];
                    } else {
                        cell.vid_Time.text = [NSString stringWithFormat:@"%d:%02d",mins,secs];
                    }
                }
                else {
                    AVAsset *asset = [AVAsset assetWithURL:[NSURL URLWithString:cell.source]];
                    cell.duration = [NSNumber numberWithFloat:lroundf(CMTimeGetSeconds(asset.duration))];
                    long seconds = lroundf([cell.duration floatValue]);
                    int hour = (int)seconds / 3600;
                    int mins = (seconds % 3600) / 60;
                    int secs = seconds % 60;
                    if(hour > 0) {
                        cell.vid_Time.text = [NSString stringWithFormat:@"%d:%02d:%02d",hour,mins,secs];
                    } else {
                        cell.vid_Time.text = [NSString stringWithFormat:@"%d:%02d",mins,secs];
                    }
                }
            } else {
                [cell.vid_Play setHidden:YES];
                [cell.vid_Time setHidden:YES];
                [cell.vid_Gradient setHidden:YES];
            }
        }
        else
        {
            __weak typeof(self) selfDelegate = self;
            if([cell.source containsString:@".mp4"] ||
               [cell.source containsString:@".avi"] ||
               [cell.source containsString:@".mpg"] ||
               [cell.source containsString:@".mov"] ) {
                if([[self.selectedMedias objectAtIndex:indexPath.item] isKindOfClass:[VideoFB class]]) {
                    [cell.vid_Time setHidden:NO];
                    [cell.vid_Play setHidden:NO];
                    cell.duration = ((VideoFB*)[self.selectedMedias objectAtIndex:indexPath.item]).length;
                    long seconds = lroundf([cell.duration floatValue]);
                    int hour = (int)seconds / 3600;
                    int mins = (seconds % 3600) / 60;
                    int secs = seconds % 60;
                    if(hour > 0) {
                        cell.vid_Time.text = [NSString stringWithFormat:@"%d:%02d:%02d",hour,mins,secs];
                    } else {
                        cell.vid_Time.text = [NSString stringWithFormat:@"%d:%02d",mins,secs];
                    }
                    
                    [cell.photoImageView sd_setImageWithURL:[NSURL URLWithString:((VideoFB*)[self.initialSelectedMedias objectAtIndex:indexPath.item]).picture] placeholderImage:nil completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                        if(error)
                        {
                            NSLog(@"StayLog: PhotoCollectionImage request error: %@", error);
                        }else{
                            if (image != nil){
                                [cell.photoImageView setImage:image];
                                
                                if(cell.photoImageView.image != nil){
                                    [selfDelegate.sfApp.imagesCache setObject:cell.photoImageView.image forKey:cell.source];
                                }
                                [cell.spinner stopAnimating];
                            }
                        }
                    }];
                    /*
                    cell.task = [self.thumbConnection dataTaskWithURL:[NSURL URLWithString:((VideoFB*)[self.selectedMedias objectAtIndex:indexPath.item]).picture] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                        if(error)
                        {
                            NSLog(@"StayLog: PhotoCollectionImage request error: %@", error);
                        }
                        else {
                            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                cell.photoImageView.image = [UIImage imageWithData:data];
                                if(cell.photoImageView.image != nil)
                                    [selfDelegate.sfApp.imagesCache setObject:cell.photoImageView.image forKey:cell.source];
                                [cell.spinner stopAnimating];
                            }];
                        }
                    }];
                    [cell.task resume];
                    [cell.spinner stopAnimating];*/
                }
                else {
                    AVAsset *asset = [AVAsset assetWithURL:[NSURL URLWithString:cell.source]];
                    AVAssetImageGenerator *generator = [AVAssetImageGenerator assetImageGeneratorWithAsset:asset];
                    generator.appliesPreferredTrackTransform = YES;
                    CMTime time = CMTimeMakeWithSeconds(1, 2);
                    NSError *error = nil;
                    @try {
                        CGImageRef img = [generator copyCGImageAtTime:time actualTime:nil error:&error];
                        if(!error)
                        {
                            UIImage *image = [UIImage imageWithCGImage:img];
                            cell.photoImageView.image = image;
                            [selfDelegate.sfApp.imagesCache setObject:cell.photoImageView.image forKey:cell.source];
                        }
                        [cell.vid_Play setHidden:NO];
                        [cell.vid_Gradient setHidden:NO];
                        [cell.vid_Time setHidden:NO];
                        
                        cell.duration = [NSNumber numberWithFloat:lroundf(CMTimeGetSeconds(asset.duration))];
                        long seconds = lroundf([cell.duration floatValue]);
                        int hour = (int)seconds / 3600;
                        int mins = (seconds % 3600) / 60;
                        int secs = seconds % 60;
                        if(hour > 0) {
                            cell.vid_Time.text = [NSString stringWithFormat:@"%d:%02d:%02d",hour,mins,secs];
                        } else {
                            cell.vid_Time.text = [NSString stringWithFormat:@"%d:%02d",mins,secs];
                        }
                        [cell.spinner stopAnimating];
                        [cell.spinner setHidden:YES];
                    } @catch (NSException *ex) {
                        NSLog(@"StayLog: EXCEPTION getting video thumb with error: %@", ex.description);
                    }
                }
            }
            else {
                
                [cell.photoImageView sd_setImageWithURL:[NSURL URLWithString:cell.source] placeholderImage:nil completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                    if(error)
                    {
                        NSLog(@"StayLog: PhotoCollectionImage request error: %@", error);
                    }else{
                        if (image != nil){
                            [cell.photoImageView setImage:image];
                            
                            if(cell.photoImageView.image != nil){
                                [selfDelegate.sfApp.imagesCache setObject:cell.photoImageView.image forKey:cell.source];
                            }
                            [cell.spinner stopAnimating];
                            [cell.spinner setHidden:YES];
                        }
                    }
                }];
                /*cell.task = [self.thumbConnection dataTaskWithURL:[NSURL URLWithString:cell.source] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    if(error)
                    {
                        NSLog(@"StayLog: PhotoCollectionImage request error: %@", error);
                    }
                    else {
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            cell.photoImageView.image = [UIImage imageWithData:data];
                            [selfDelegate.sfApp.imagesCache setObject:cell.photoImageView.image forKey:cell.source];
                            [cell.spinner stopAnimating];
                            [cell.spinner setHidden:YES];
                        }];
                    }
                }];
                [cell.task resume];*/
                [cell.vid_Play setHidden:YES];
                [cell.vid_Time setHidden:YES];
                [cell.vid_Gradient setHidden:YES];
            }
        }
    }
    if([[self.selectedMedias objectAtIndex:indexPath.item] isKindOfClass:[PHAsset class]]) {
        cell.source = nil;
        PHAsset * asset = [self.selectedMedias objectAtIndex:indexPath.item];
        if(asset.mediaType == PHAssetMediaTypeVideo) {
            [cell.vid_Play setHidden:NO];
            [cell.vid_Time setHidden:NO];
            [cell.vid_Gradient setHidden:NO];
            
            long seconds = lroundf(asset.duration);
            int hour = (int)seconds / 3600;
            int mins = (seconds % 3600) / 60;
            int secs = seconds % 60;
            if(hour > 0) {
                cell.vid_Time.text = [NSString stringWithFormat:@"%d:%02d:%02d",hour,mins,secs];
            } else {
                cell.vid_Time.text = [NSString stringWithFormat:@"%d:%02d",mins,secs];
            }
        } else {
            [cell.vid_Play setHidden:YES];
            [cell.vid_Time setHidden:YES];
            [cell.vid_Gradient setHidden:YES];
        }
        [self.videoImageManager requestImageForAsset:asset targetSize:cell.photoImageView.frame.size contentMode:PHImageContentModeAspectFill options:nil resultHandler:^(UIImage *result, NSDictionary *info)
         {
             cell.photoImageView.image = result;
         }];
        if([self.uploaderStayfilm isAssetOnUploadList:asset]) {
            cell.photoImageView.alpha = 0.7;
            [cell.spinner startAnimating];
            [cell.spinner setHidden:NO];
        } else {
            cell.photoImageView.alpha = 1.0;
            [cell.spinner stopAnimating];
            [cell.spinner setHidden:YES];
        }
    }
    
    return cell;
}

- (BOOL)collectionView:(LSCollectionViewHelper *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    // Prevent item from being moved to index 0
    //    if (toIndexPath.item == 0) {
    //        return NO;
    //    }
    return YES;
}

- (void)collectionView:(LSCollectionViewHelper *)collectionView moveItemAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    NSString *index = [self.selectedMedias objectAtIndex:fromIndexPath.item];
    
    [self.selectedMedias removeObjectAtIndex:fromIndexPath.item];
    [self.selectedMedias insertObject:index atIndex:toIndexPath.item];
    
    [self.timer invalidate];
    self.timer = nil;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:2.5
                                                  target:self
                                                selector:@selector(reloadDraggable:)
                                                userInfo:nil
                                                 repeats:YES];
    self.reordered = YES;
    [self.draggableCollection reloadData];
//    if(!self.reordered) {
//        self.reordered = YES;
       // [self performSelectorOnMainThread:@selector(reloadDraggable) withObject:nil waitUntilDone:NO];
//    }
}

-(void)reloadDraggable:(NSTimer*)timer  {
 
//    //NSArray *idx = self.draggableCollection.indexPathsForVisibleItems;
//    for (ReorderCollectionViewCell *cell in [self.draggableCollection visibleCells]) {
//        //[cell.numberView setHidden:YES];
//        cell.numberView.alpha = 0.0;
//        [self performSelector:@selector(executeAnimationforCell:) withObject:cell afterDelay:2.0];
//    }
    
    //self.but_ConfirmReorder.imageView.image = [UIImage imageNamed:@"Floating_CheckBig_green"];
    self.reordered = NO;
    [self.draggableCollection reloadData];
    [self.timer invalidate];
    self.timer = nil;
}

-(void)executeAnimationforCell:(ReorderCollectionViewCell*)cell {
    cell.numberView.alpha = 0.0;
    [UIView animateWithDuration:0.5 animations:^{
        cell.numberView.alpha = 0.5;
    }];
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeMake(0, 0);
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    //Set cell size to fit all screens
    CGFloat width  = self.view.frame.size.width;
    width -= 6.0;
    width /= 3.0;
    return CGSizeMake(width,width);
}


#pragma mark - Uploader Delegates


-(void)uploadProgressChanged:(NSNumber *)progress
{
    // Not important on this page.
}

- (void)converterDidFailMedia:(NSString *)type
{
    NSString *message = nil;
    if(self.sfApp == nil)
    {
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    }
    if (  (self.sfApp.isEditingMode)? (self.sfApp.editedMedias.count < [self.sfApp.sfConfig.min_photos intValue]) : (self.sfApp.finalMedias.count < [self.sfApp.sfConfig.min_photos intValue]) ) {
        message = [NSString stringWithFormat:NSLocalizedString(@"UPLOADER_CONVERT_FAILED_UPLOAD_AGAIN_MESSAGE", nil), ((self.sfApp.isEditingMode)? ([self.sfApp.sfConfig.min_photos intValue] - self.sfApp.editedMedias.count) : ([self.sfApp.sfConfig.min_photos intValue] - self.sfApp.finalMedias.count))];
    }
    else if ([type isEqualToString:@"image"]) {
        message = NSLocalizedString(@"UPLOADER_CONVERT_FAILED_IMAGE_MESSAGE", nil);
    }
    else
    {
        message = NSLocalizedString(@"UPLOADER_CONVERT_FAILED_VIDEO_MESSAGE", nil);
    }
    
    if(self.sfApp.isEditingMode) {
        @synchronized (self.sfApp.editedMedias) {
            for (id obj in self.sfApp.editedMedias) {
                if([obj isKindOfClass:[PHAsset class]]) {
                    BOOL find = NO;
                    for (PHAsset *asset in self.uploaderStayfilm.listUploadMedias) {
                        if(asset == obj) {
                            find = YES;
                            break;
                        }
                    }
                    if(!find) {
                        NSUInteger i = [self.sfApp.editedMedias indexOfObject:obj];
                        [self.sfApp.editedMedias removeObjectAtIndex:i];
                        [self.draggableCollection reloadData];
                        break;
                    }
                }
            }
        }
    } else {
        @synchronized (self.sfApp.finalMedias) {
            for (id obj in self.sfApp.finalMedias) {
                if([obj isKindOfClass:[PHAsset class]]) {
                    BOOL find = NO;
                    for (PHAsset *asset in self.uploaderStayfilm.listUploadMedias) {
                        if(asset == obj) {
                            find = YES;
                            break;
                        }
                    }
                    if(!find) {
                        NSUInteger i = [self.sfApp.finalMedias indexOfObject:obj];
                        [self.sfApp.finalMedias removeObjectAtIndex:i];
                        [self.draggableCollection reloadData];
                        break;
                    }
                }
            }
        }
    }
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_CONVERT_FAILED_TITLE", nil)
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)uploadDidFailMedia:(NSString *)type
{
    NSString *message = nil;
    if(self.sfApp == nil)
    {
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    }
    if ( (self.sfApp.isEditingMode)? (self.sfApp.editedMedias.count < [self.sfApp.sfConfig.min_photos intValue]) : (self.sfApp.finalMedias.count < [self.sfApp.sfConfig.min_photos intValue]) ) {
        message = [NSString stringWithFormat:NSLocalizedString(@"UPLOADER_UPLOAD_FAILED_UPLOAD_AGAIN_MESSAGE", nil), ((self.sfApp.isEditingMode)? ([self.sfApp.sfConfig.min_photos intValue] - self.sfApp.editedMedias.count) : ([self.sfApp.sfConfig.min_photos intValue] - self.sfApp.finalMedias.count))];
    }
    else if ([type isEqualToString:@"image"]) {
        message = NSLocalizedString(@"UPLOADER_UPLOAD_FAILED_IMAGE_MESSAGE", nil);
    }
    else
    {
        message = NSLocalizedString(@"UPLOADER_UPLOAD_FAILED_VIDEO_MESSAGE", nil);
    }
    
    if(self.sfApp.isEditingMode) {
        @synchronized (self.sfApp.editedMedias) {
            for (id obj in self.sfApp.editedMedias) {
                if([obj isKindOfClass:[PHAsset class]]) {
                    BOOL find = NO;
                    for (PHAsset *asset in self.uploaderStayfilm.listUploadMedias) {
                        if(asset == obj) {
                            find = YES;
                            break;
                        }
                    }
                    if(!find) {
                        NSUInteger i = [self.sfApp.editedMedias indexOfObject:obj];
                        [self.sfApp.editedMedias removeObjectAtIndex:i];
                        [self.draggableCollection reloadData];
                        break;
                    }
                }
            }
        }
    } else {
        @synchronized (self.sfApp.finalMedias) {
            for (id obj in self.sfApp.finalMedias) {
                if([obj isKindOfClass:[PHAsset class]]) {
                    BOOL find = NO;
                    for (PHAsset *asset in self.uploaderStayfilm.listUploadMedias) {
                        if(asset == obj) {
                            find = YES;
                            break;
                        }
                    }
                    if(!find) {
                        NSUInteger i = [self.sfApp.finalMedias indexOfObject:obj];
                        [self.sfApp.finalMedias removeObjectAtIndex:i];
                        [self.draggableCollection reloadData];
                        break;
                    }
                }
            }
        }
    }
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_UPLOAD_FAILED_TITLE", nil)
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)uploadDidFail:(Uploader *)uploader
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_ERROR_TITLE", nil)
                                                                   message:NSLocalizedString(@"UPLOADER_ERROR_MESSAGE", nil)
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)uploadDidStartSendingConfirmationMedias:(Uploader *)uploader
{
    // not important on this page.
}

-(void)uploadDidStart
{
    // does not happen in this page
}

-(void)uploadDidFinish:(Uploader *)uploader
{
    if(self.sfApp == nil) {
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    }
    if(self.uploaderStayfilm.listUploadedMedias != nil && self.uploaderStayfilm.listUploadedMedias.count > 0) {
        for (PHAsset * asset in self.uploaderStayfilm.listUploadedMedias.allValues) {
            if(self.sfApp.isEditingMode) {
                if(![self.sfApp.editedMedias containsObject:asset]) {
                    [self.sfApp.editedMedias addObject:asset];
                }
            } else {
                if(![self.sfApp.finalMedias containsObject:asset]) {
                    [self.sfApp.finalMedias addObject:asset];
                }
            }
        }
    }
    if(self.sfApp.isDebugMode) {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_FINISHED", nil)
                                                                       message:[[NSString alloc] initWithFormat:NSLocalizedString(@"UPLOADED_X_MEDIAS", nil), uploader.listUploadedMedias.count]
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    [self.draggableCollection reloadData];
}

-(void)uploadFinishedMedia:(PHAsset *)asset {
    [self.draggableCollection reloadData];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
