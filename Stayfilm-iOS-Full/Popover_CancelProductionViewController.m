//
//  Popover_CancelProductionViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Henrique on 5/18/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import "Popover_CancelProductionViewController.h"

@interface Popover_CancelProductionViewController ()

@property (nonatomic, assign) BOOL didClick;

@end

@implementation Popover_CancelProductionViewController

-(id)initWithDelegate:(id<Popover_CancelProductionDelegate>)p_delegate
{
    if(self = [super init]) {
        self.delegate = p_delegate;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *modalTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOutside)];
    modalTap.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:modalTap];
    
    self.but_Back.layer.borderWidth = 1.0f;
    self.but_Back.layer.borderColor = [[UIColor colorWithRed:80.0/255.0 green:128.0/255.0 blue:146.0/255.0 alpha:1.0] CGColor];
    self.but_Back.layer.cornerRadius = 5;
    self.but_Back.clipsToBounds = YES;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.didClick = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tapOutside {
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.delegate performSelector:@selector(chooseOption:) withObject:@"stayHere"];
}

- (IBAction)but_Back_Clicked:(id)sender {
    if(!self.didClick) {
        self.didClick = YES;
        [self tapOutside];
        [self.delegate performSelector:@selector(chooseOption:) withObject:@"cancelProduction"];
    }
}

- (IBAction)but_stayHere_Clicked:(id)sender {
    if(!self.didClick) {
        self.didClick = YES;
        [self dismissViewControllerAnimated:YES completion:nil];
        [self.delegate performSelector:@selector(chooseOption:) withObject:@"stayHere"];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
