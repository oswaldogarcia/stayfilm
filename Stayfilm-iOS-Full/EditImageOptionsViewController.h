//
//  EditImageOptionsViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 9/05/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EditImageOptionsDelegate<NSObject>

- (void)didSelectTakePicture;
- (void)didSelectChoosePicture;

@end


@interface EditImageOptionsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *takePictureButton;

@property (weak, nonatomic) IBOutlet UIButton *choosePictureButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

@property (nonatomic, assign) id<EditImageOptionsDelegate> delegate;
@end
