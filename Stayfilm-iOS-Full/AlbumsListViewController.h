//
//  AlbumsListViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 2/4/19.
//  Copyright © 2019 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>



@protocol AlbumsDelegate<NSObject>
@required
- (void)albumSelected:(PHAssetCollection*)selected;
@end

NS_ASSUME_NONNULL_BEGIN

@interface AlbumsListViewController : UIViewController
@property (nonatomic, strong) NSMutableArray *collectionsAlbums;
@property (weak, nonatomic) IBOutlet UILabel *albumsLabel;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (nonatomic, strong) id<AlbumsDelegate> delegate;
@end

NS_ASSUME_NONNULL_END
