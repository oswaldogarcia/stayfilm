//
//  Popover_MoreOptionsViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Henrique on 5/16/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import "AppDelegate.h"
#import "Popover_MoreOptionsViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "Step3StylesViewController.h"
#import "AlbumFB.h"
#import "PhotoFB.h"
#import "VideoFB.h"
#import "Step1ConfirmationViewController.h"
#import "ProgressViewController.h"
#import "NoAvailableOrientationViewController.h"

@interface Popover_MoreOptionsViewController ()<Popover_DeleteDelegate,ChangeStyleDelegate,ChangeMovieDataDelegate>

@property (nonatomic, weak) Uploader *uploaderStayfilm;

@property (nonatomic, assign) BOOL updateFilmTapped;


@end

@implementation Popover_MoreOptionsViewController



- (void)viewWillAppear:(BOOL)animated{
    
    self.navigationController.navigationBar.hidden = YES;
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = [UIColor colorWithRed:0.20 green:0.34 blue:0.44 alpha:1.0];
        self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    }
    
    self.uploaderStayfilm = [Uploader sharedUploadStayfilmSingletonWithDelegate:self];
    
    [self.sfApp setEditingMode:YES];
    
    [self.sfApp.filmStrip setHidden:YES];
    
    [self.sfApp.filmStrip.viewEditing setHidden:YES];

    [self setMovieInfo];

    [self.sfApp.filmStrip changeTopStatusBarColorToWhite:YES];
    
    self.filmOrientationLabel.text = NSLocalizedString(@"FILM_ORIENTATION", nil);
    
    if(self.sfApp.editedGenre != nil){
        
        if(self.sfApp.editedGenre.hasVertical){
            
            if(self.sfApp.editedGenre.orientation == VERTICAL){
                [self.orientationImage setImage:[UIImage imageNamed:@"morOptionsVertical"]];
                self.currentOrientationLabel.text = NSLocalizedString(@"VERTICAL", nil);
            }else{
                [self.orientationImage setImage:[UIImage imageNamed:@"moreOptionsHorizontal"]];
                self.currentOrientationLabel.text = NSLocalizedString(@"HORIZONTAL", nil);
            }
            self.selectedOrientation = self.sfApp.editedGenre.orientation;
            
        }else{
            [self.orientationImage setImage:[UIImage imageNamed:@"moreOptionsHorizontal"]];
            self.currentOrientationLabel.text = NSLocalizedString(@"HORIZONTAL", nil);
            self.selectedOrientation = self.sfApp.editedGenre.orientation;
        }
        
    }else{
        
        if(self.sfApp.selectedGenre.hasVertical){
            
            if(self.sfApp.selectedGenre.orientation == VERTICAL){
                [self.orientationImage setImage:[UIImage imageNamed:@"morOptionsVertical"]];
                self.currentOrientationLabel.text = NSLocalizedString(@"VERTICAL", nil);
            }else{
                [self.orientationImage setImage:[UIImage imageNamed:@"moreOptionsHorizontal"]];
                self.currentOrientationLabel.text = NSLocalizedString(@"HORIZONTAL", nil);
            }
            self.selectedOrientation = self.sfApp.selectedGenre.orientation;
            
        }else{
            [self.orientationImage setImage:[UIImage imageNamed:@"moreOptionsHorizontal"]];
            self.currentOrientationLabel.text = NSLocalizedString(@"HORIZONTAL", nil);
            self.selectedOrientation = self.sfApp.selectedGenre.orientation;
        }
        
    }
    
    if([self isSomethingEdited]){
        [self.updateFilmButton setHidden:NO];
        [self.updateArrowImage setHidden:NO];
    }
    
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    //Google Analytics
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"more-options"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [self.sfApp.filmStrip connectivityViewUpdate];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.modalPresentationCapturesStatusBarAppearance = YES;
    
    if(self.sfApp == nil){
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    }
    self.sfApp.editedMedias = [self.sfApp.finalMedias mutableCopy];

    self.updateFilmTapped = NO;
}

- (void)viewWillDisappear:(BOOL)animated{
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]){
        statusBar.backgroundColor = [UIColor clearColor];
    }
    
    [self.sfApp.filmStrip connectivityViewUpdate];
}

- (void)tapOutside{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)setMovieInfo{
    
    // set info to change style
    if (self.sfApp.editedGenre != nil){
        
        [self.styleImage sd_setImageWithURL:[NSURL URLWithString: self.sfApp.editedGenre.imageUrl]];
        
        if (self.sfApp.editedTemplate != nil){
            
            [self.templateImage sd_setImageWithURL:[NSURL URLWithString: self.sfApp.editedTemplate.thumbnail]];
        }
        
        self.styleNameLabel.text = self.sfApp.editedGenre.name;
    }else{
        
        [self.styleImage sd_setImageWithURL:[NSURL URLWithString: self.sfApp.selectedGenre.imageUrl]];
        
        [self.templateImage sd_setImageWithURL:[NSURL URLWithString: self.sfApp.selectedTemplate.thumbnail]];
        
        self.styleNameLabel.text = self.sfApp.selectedGenre.name;
        
    }
    // set info to edit media
    self.numberOfImageLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)self.sfApp.editedMedias.count];
    
    NSMutableArray *assetsImageType = [[NSMutableArray alloc]init];
    NSMutableArray *assetsVideoType = [[NSMutableArray alloc]init];

    for (int i = 0;i < self.sfApp.editedMedias.count ;i++){
       
        if (i < 4){
            
            UIImageView *imageView = [[UIImageView alloc] init];
            
            switch (i) {
                case 0:
                    imageView = self.collageImage1;
                    break;
                case 1:
                    imageView = self.collageImage2;
                    break;
                case 2:
                    imageView = self.collageImage3;
                    break;
                case 3:
                    imageView = self.collageImage4;
                    break;
                    
                default:
                    imageView = self.collageImage1;
                    break;
            }
            
            if([self.sfApp.editedMedias[i] isKindOfClass:[Media class]]){
                Media * media = self.sfApp.editedMedias[i];
                [imageView sd_setImageWithURL:[NSURL URLWithString:media.source]];
            }else if([self.sfApp.editedMedias[i] isKindOfClass:[PhotoFB class]]){
                PhotoFB * passet = self.sfApp.editedMedias[i];
                [imageView sd_setImageWithURL:[NSURL URLWithString:passet.source]];
            }else if([self.sfApp.editedMedias[i] isKindOfClass:[VideoFB class]]){
                VideoFB * passet = self.sfApp.editedMedias[i];
                [imageView sd_setImageWithURL:[NSURL URLWithString:passet.picture]];
            }else if([self.sfApp.editedMedias[i] isKindOfClass:[PHAsset class]]){
                
                PHAsset * asset = self.sfApp.editedMedias[i];
                
                PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
                options.resizeMode = PHImageRequestOptionsResizeModeExact;

                NSInteger retinaMultiplier = [UIScreen mainScreen].scale;

                CGSize retinaSquare = CGSizeMake(imageView.bounds.size.width * retinaMultiplier, imageView.bounds.size.height * retinaMultiplier);

                [[PHImageManager defaultManager]
                 requestImageForAsset:asset
                 targetSize:retinaSquare
                 contentMode:PHImageContentModeAspectFill
                 options:options
                 resultHandler:^(UIImage *result, NSDictionary *info) {

                     imageView.image = [UIImage imageWithCGImage:result.CGImage scale:retinaMultiplier orientation:result.imageOrientation];
                     
                 }];
            }
        }
        
        if(self.sfApp.editedMedias.count < 4) {
            if (self.sfApp.editedMedias.count == 3) {
                [self.collageImage1 setHidden:NO];
                [self.collageImage2 setHidden:NO];
                [self.collageImage3 setHidden:NO];
                [self.collageImage4 setHidden:YES];
            } else if (self.sfApp.editedMedias.count == 2) {
                [self.collageImage1 setHidden:NO];
                [self.collageImage2 setHidden:NO];
                [self.collageImage3 setHidden:YES];
                [self.collageImage4 setHidden:YES];
            } else if (self.sfApp.editedMedias.count == 1) {
                [self.collageImage1 setHidden:NO];
                [self.collageImage2 setHidden:YES];
                [self.collageImage3 setHidden:YES];
                [self.collageImage4 setHidden:YES];
            } else if (self.sfApp.editedMedias.count == 0) {
                [self.collageImage1 setHidden:YES];
                [self.collageImage2 setHidden:YES];
                [self.collageImage3 setHidden:YES];
                [self.collageImage4 setHidden:YES];
            }
        } else {
            [self.collageImage1 setHidden:NO];
            [self.collageImage2 setHidden:NO];
            [self.collageImage3 setHidden:NO];
            [self.collageImage4 setHidden:NO];
        }
        
        if([self.sfApp.editedMedias[i] isKindOfClass:[Media class]]) {
            Media * media = self.sfApp.editedMedias[i];
            
            if(!media.isVideo){
                [assetsImageType addObject:media];
            }else{
                [assetsVideoType addObject:media];
            }
        }
        else if([self.sfApp.editedMedias[i] isKindOfClass:[PhotoFB class]]) {
            PhotoFB * passet = self.sfApp.editedMedias[i];
            
            [assetsImageType addObject:passet];
        }
        else if([self.sfApp.editedMedias[i] isKindOfClass:[VideoFB class]]) {
            VideoFB * passet = self.sfApp.editedMedias[i];
            
            [assetsVideoType addObject:passet];
        }else if([self.sfApp.editedMedias[i] isKindOfClass:[PHAsset class]]) {
            
            PHAsset * asset = self.sfApp.editedMedias[i];
            
            if (asset.mediaType == 1) {
                [assetsImageType addObject:asset];
            }else if (asset.mediaType == 2){
                [assetsVideoType addObject:asset];
            }
        }

    }
    
    self.numberItemsLabel.text = [NSString stringWithFormat:NSLocalizedString(@"MEDIAS_COUNT", nil),(unsigned long)assetsImageType.count, (unsigned long)assetsVideoType.count];
    
    
    if(self.sfApp.editedTitle == nil) {
        self.currentFilmTitleLabel.text = (self.sfApp.selectedTitle != nil && ![self.sfApp.selectedTitle isEqualToString:@""]) ? self.sfApp.selectedTitle : self.movie.title;
    } else {
        self.currentFilmTitleLabel.text = self.sfApp.editedTitle;
    }
}

-(BOOL)isSomethingEdited {
    if(![StayfilmApp isArray:self.sfApp.finalMedias equalToArray:self.sfApp.editedMedias] ||
       (self.sfApp.editedGenre != nil && self.sfApp.editedGenre != self.sfApp.selectedGenre) ||
       (self.sfApp.editedTitle !=nil && ![self.sfApp.editedTitle isEqualToString:self.sfApp.selectedTitle]) ||
       (self.sfApp.editedTemplate != nil && self.sfApp.editedTemplate != self.sfApp.selectedTemplate) ||
       (self.selectedOrientation != self.sfApp.selectedGenre.orientation)) {
        return YES;
    } else {
        return NO;
    }
}


#pragma mark - Buttons Actions

- (IBAction)cancelAction:(id)sender {
    if(self.updateFilmTapped)
        return;
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"more-options"
                                                          action:@"cancel"
                                                           label:@""
                                                           value:@1] build]];
    
    
    if([self isSomethingEdited]) {
        UIAlertController* alertc = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ARE_YOU_SURE", nil)
                                                                        message:NSLocalizedString(@"CONFIRM_UNWIND_FROM_MOREOPTIONS", nil)
                                                                 preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action) {
                                                             [alertc dismissViewControllerAnimated:YES completion:nil];
                                                             self.sfApp.editedGenre = nil;
                                                             self.sfApp.editedTemplate = nil;
                                                             self.sfApp.editedMedias = nil;
                                                             self.sfApp.editedMedias = [[NSMutableArray alloc] init];
                                                             self.sfApp.editedTitle = nil;
                                                             
                                                             [self.sfApp setEditingMode:NO];
                                                             [self.sfApp.filmStrip setViewOutsideFilmStrip];
                                                             [self.sfApp.filmStrip updateFilmStrip];
                                                             [self.sfApp.filmStrip setButtonNextHidden:YES];
                                                             [self.sfApp.filmStrip changeTopStatusBarColorToWhite:NO];
                                                             
                                                             [self.navigationController popViewControllerAnimated:YES];
                                                             [self dismissViewControllerAnimated:YES completion:nil];
                                                         }];
        UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"STAY", nil) style:UIAlertActionStyleCancel
                                                             handler:^(UIAlertAction * action) {
                                                                 [alertc dismissViewControllerAnimated:YES completion:nil];
                                                             }];
        
        [alertc addAction:okAction];
        [alertc addAction:cancelAction];
        [self presentViewController:alertc animated:YES completion:nil];
    }
    else {
        self.sfApp.editedGenre = nil;
        self.sfApp.editedTemplate = nil;
        self.sfApp.editedMedias = nil;
        self.sfApp.editedMedias = [[NSMutableArray alloc] init];
        self.sfApp.editedTitle = nil;
        
        [self.sfApp setEditingMode:NO];
        [self.sfApp.filmStrip setViewOutsideFilmStrip];
        [self.sfApp.filmStrip updateFilmStrip];
        [self.sfApp.filmStrip setButtonNextHidden:YES];
        [self.sfApp.filmStrip changeTopStatusBarColorToWhite:NO];
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

- (IBAction)updateFilmAction:(id)sender {
    
    self.updateFilmTapped = YES;
    [self.view_Loading setHidden:NO];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^(void){
        [self.movie deleteMovie];
        
        //self.sfApp.finalMedias = [self.sfApp.editedMedias mutableCopy];
        self.sfApp.finalMedias = nil;
        if(self.sfApp.editedTitle != nil)
            self.sfApp.selectedTitle = [NSString stringWithString:self.sfApp.editedTitle];
        if(self.sfApp.editedGenre != nil)
            self.sfApp.selectedGenre = self.sfApp.editedGenre;
        if(self.sfApp.editedTemplate != nil)
            self.sfApp.selectedTemplate = self.sfApp.editedTemplate;
        
        self.sfApp.editedGenre = nil;
        self.sfApp.editedTemplate = nil;
        //self.sfApp.editedMedias = nil;
        //self.sfApp.editedMedias = [[NSMutableArray alloc] init];
        self.sfApp.editedTitle = nil;
        
     
        self.sfApp.selectedGenre.orientation = self.selectedOrientation;
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            runOnMainQueueWithoutDeadlocking(^{
                [self.navigationController popViewControllerAnimated:NO];
                
                //Google Analytics Event
                id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"update-film"
                                                                      action:@" "
                                                                       label:@""
                                                                       value:@1] build]];
                [self.delegate chooseOption:@"updateFilm"];
//                ProgressViewController *remakeProgress = [self.storyboard instantiateViewControllerWithIdentifier:@"Progress"];
//                [self.navigationController showViewController:remakeProgress sender:self];
            });
        });
    });
    
}

- (IBAction)editPhotoAction:(id)sender {
    if(self.updateFilmTapped)
        return;
    
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"edit-film"
                                                          action:@"edit-photos-videos"
                                                           label:@""
                                                           value:@1] build]];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MovieMaker" bundle:[NSBundle mainBundle]];
    
    Step1ConfirmationViewController *step1View = [storyboard instantiateViewControllerWithIdentifier:@"Step1Confirmation"];
   
    step1View.editingType = @"editPhoto";
    step1View.delegate = self;
    
    [self.navigationController showViewController:step1View sender:self];
}

- (IBAction)editTitleAction:(id)sender {
    if(self.updateFilmTapped)
        return;
    
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"edit-film"
                                                          action:@"edit-title"
                                                           label:@""
                                                           value:@1] build]];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MovieMaker" bundle:[NSBundle mainBundle]];
    
    
    Step1ConfirmationViewController *step1View = [storyboard instantiateViewControllerWithIdentifier:@"Step1Confirmation"];
    
    step1View.editingType = @"title";
    step1View.temporalTitle = self.movie.title;
    step1View.delegate = self;

    [self.navigationController showViewController:step1View sender:self];
}

- (IBAction)changeStyleAction:(id)sender {
    if(self.updateFilmTapped)
        return;
    
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"edit-film"
                                                          action:@"change-style"
                                                           label:@""
                                                           value:@1] build]];
    
    
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MovieMaker" bundle:[NSBundle mainBundle]];
    
    
    Step3StylesViewController *styleView = [storyboard instantiateViewControllerWithIdentifier:@"styleView"];
    styleView.selectedOrientation = self.selectedOrientation;
    styleView.isEditing = YES;
    styleView.delegate = self;
   // UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:styleView];
    
    [self.navigationController showViewController:styleView sender:self];
  // [self presentViewController:navigationController animated:YES completion:nil];
   
}


- (IBAction)but_delete_Clicked:(id)sender {
    if(self.updateFilmTapped)
        return;
    
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"edit-film"
                                                          action:@"delete-film"
                                                           label:@""
                                                           value:@1] build]];
    
    
    Popover_Delete_ViewController * view = [[Popover_Delete_ViewController alloc] init];
    view.thumbnailUrl = self.movie.thumbnailUrl;
    view.delegate = self;
    view.sfApp = self.sfApp;
    view.analyticTag = @"delete-film";
    view.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    view.view.frame = self.view.frame;
    
    [self presentViewController:view animated:YES completion:nil];
}
- (IBAction)filmOrientationAction:(id)sender {

    
    if (self.selectedOrientation == VERTICAL){
        
        [self.orientationImage setImage:[UIImage imageNamed:@"moreOptionsHorizontal"]];
        self.currentOrientationLabel.text = @"Horizontal";
        self.selectedOrientation = HORIZONTAL;
      
        self.sfApp.editedGenre.orientation = HORIZONTAL;
        
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"edit-film"
                                                              action:@"orientation"
                                                               label:@"horizontal"
                                                               value:@1] build]];
        
    }else {
        
        if(self.sfApp.editedGenre != nil){
            
            if(self.sfApp.editedGenre.hasVertical){
                
                [self.orientationImage setImage:[UIImage imageNamed:@"morOptionsVertical"]];
                self.currentOrientationLabel.text = @"Vertical";
                self.selectedOrientation = VERTICAL;
                self.sfApp.editedGenre.orientation = VERTICAL;
                
                //Google Analytics Event
                id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"edit-film"
                                                                      action:@"orientation"
                                                                       label:@"vertical"
                                                                       value:@1] build]];
                
            }else{
                
                
                NoAvailableOrientationViewController * view = [[NoAvailableOrientationViewController alloc] init];
                
                view.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                view.view.frame = self.view.frame;
                
                //Google Analytics Event
                id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"edit-film"
                                                                      action:@"orientation"
                                                                       label:@"Vertical-not-available"
                                                                       value:@1] build]];
                
                [self presentViewController:view animated:YES completion:nil];
            }
        }else{
            
            if(self.sfApp.selectedGenre.hasVertical){
                
                [self.orientationImage setImage:[UIImage imageNamed:@"morOptionsVertical"]];
                self.currentOrientationLabel.text = @"Vertical";
                self.selectedOrientation = VERTICAL;
               
                self.sfApp.editedGenre.orientation = VERTICAL;
                
                //Google Analytics Event
                id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"edit-film"
                                                                      action:@"orientation"
                                                                       label:@"vertical"
                                                                       value:@1] build]];
                
            }else{
                
                
                NoAvailableOrientationViewController * view = [[NoAvailableOrientationViewController alloc] init];
                
                view.modalPresentationStyle = UIModalPresentationOverCurrentContext;
                view.view.frame = self.view.frame;
                
                //Google Analytics Event
                id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"edit-film"
                                                                      action:@"orientation"
                                                                       label:@"Vertical-not-available"
                                                                       value:@1] build]];
                
                [self presentViewController:view animated:YES completion:nil];
            }
            
        }
    }
    
    if([self isSomethingEdited]){
        [self.updateFilmButton setHidden:NO];
        [self.updateArrowImage setHidden:NO];
    }else {
        [self.updateFilmButton setHidden:YES];
        [self.updateArrowImage setHidden:YES];
    }

    
    
}

#pragma mark - Popover Delete Delegate

-(void)chooseOption:(NSString*)option {
    
    if ([option isEqualToString:@"confirmDelete"]) {
        [self.view_Loading setHidden:NO];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^(void){
            
            if(self.movieFile != nil)
            {
                if(self.sfApp == nil)
                {
                    self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
                }
                [self.sfApp deleteMovieFromDevice:self.movieFile];
                [self.sfApp saveSettings];
            }
            
            [self.movie deleteMovie];
            [self.sfApp cleanRemakeState];
            
            [self.sfApp setEditingMode:NO];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                runOnMainQueueWithoutDeadlocking(^{
                    AppDelegate *appdelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
                    [appdelegate resetMovieMakerStoryboard];
                });
            });
        
        });
    }
    
}

#pragma mark - Change Style Delegate

- (void)styleChanged:(Genres *)genre Template:(Template *)selectedTemplate{
    
    [self setMovieInfo];
    
    if(self.sfApp.editedMedias.count >= [self.sfApp.sfConfig.min_photos intValue]) {
        [self.updateFilmButton setHidden:NO];
        [self.updateArrowImage setHidden:NO];
    } else {
        [self.updateFilmButton setHidden:YES];
        [self.updateArrowImage setHidden:YES];
    }
}

#pragma mark - Change Title Delegate

- (void)titleChanged:(NSString *)title{
    
    self.sfApp.editedTitle = title;
    
    [self setMovieInfo];
    
    if(self.sfApp.editedMedias.count >= [self.sfApp.sfConfig.min_photos intValue]) {
        [self.updateFilmButton setHidden:NO];
        [self.updateArrowImage setHidden:NO];
    } else {
        [self.updateFilmButton setHidden:YES];
        [self.updateArrowImage setHidden:YES];
    }
    
}

- (void)mediaChanged {
    
    [self setMovieInfo];
    
    if(self.sfApp.editedMedias.count >= [self.sfApp.sfConfig.min_photos intValue]) {
        [self.updateFilmButton setHidden:NO];
        [self.updateArrowImage setHidden:NO];
    } else {
        [self.updateFilmButton setHidden:YES];
        [self.updateArrowImage setHidden:YES];
    }
}


#pragma mark - Upload Delegates

- (void)converterDidFailMedia:(NSString *)type {
    NSString *message = nil;
    if(self.sfApp == nil)
    {
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    }
    
    if ( (self.sfApp.isEditingMode)? (self.sfApp.editedMedias.count < [self.sfApp.sfConfig.min_photos intValue]) : (self.sfApp.finalMedias.count < [self.sfApp.sfConfig.min_photos intValue]) ) {
        message = [NSString stringWithFormat:NSLocalizedString(@"UPLOADER_CONVERT_FAILED_UPLOAD_AGAIN_MESSAGE", nil), ((self.sfApp.isEditingMode)? ([self.sfApp.sfConfig.min_photos intValue] - self.sfApp.editedMedias.count) : ([self.sfApp.sfConfig.min_photos intValue] - self.sfApp.finalMedias.count))];
    }
    else if ([type isEqualToString:@"image"]) {
        message = NSLocalizedString(@"UPLOADER_CONVERT_FAILED_IMAGE_MESSAGE", nil);
    }
    else
    {
        message = NSLocalizedString(@"UPLOADER_CONVERT_FAILED_VIDEO_MESSAGE", nil);
    }
    
    if(self.sfApp.isEditingMode) {
        @synchronized (self.sfApp.editedMedias) {
            for (id obj in self.sfApp.editedMedias) {
                if([obj isKindOfClass:[PHAsset class]]) {
                    BOOL find = NO;
                    for (PHAsset *asset in self.uploaderStayfilm.listUploadMedias) {
                        if(asset == obj) {
                            find = YES;
                            break;
                        }
                    }
                    if(!find) {
                        NSUInteger i = [self.sfApp.editedMedias indexOfObject:obj];
                        [self.sfApp.editedMedias removeObjectAtIndex:i];
                        [self setMovieInfo];
                        break;
                    }
                }
            }
        }
    } else {
        @synchronized (self.sfApp.finalMedias) {
            for (id obj in self.sfApp.finalMedias) {
                if([obj isKindOfClass:[PHAsset class]]) {
                    BOOL find = NO;
                    for (PHAsset *asset in self.uploaderStayfilm.listUploadMedias) {
                        if(asset == obj) {
                            find = YES;
                            break;
                        }
                    }
                    if(!find) {
                        NSUInteger i = [self.sfApp.finalMedias indexOfObject:obj];
                        [self.sfApp.finalMedias removeObjectAtIndex:i];
                        [self setMovieInfo];
                        break;
                    }
                }
            }
        }
    }
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_CONVERT_FAILED_TITLE", nil)
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)uploadDidFail:(Uploader *)uploader {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_ERROR_TITLE", nil)
                                                                   message:NSLocalizedString(@"UPLOADER_ERROR_MESSAGE", nil)
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)uploadDidFailMedia:(NSString *)type {
    NSString *message = nil;
    if(self.sfApp == nil)
    {
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    }
    
    if ( (self.sfApp.isEditingMode)? (self.sfApp.editedMedias.count < [self.sfApp.sfConfig.min_photos intValue]) : (self.sfApp.finalMedias.count < [self.sfApp.sfConfig.min_photos intValue]) ) {
        message = [NSString stringWithFormat:NSLocalizedString(@"UPLOADER_UPLOAD_FAILED_UPLOAD_AGAIN_MESSAGE", nil), ((self.sfApp.isEditingMode)? ([self.sfApp.sfConfig.min_photos intValue] - self.sfApp.editedMedias.count) : ([self.sfApp.sfConfig.min_photos intValue] - self.sfApp.finalMedias.count))];
    }
    else if ([type isEqualToString:@"image"]) {
        message = NSLocalizedString(@"UPLOADER_UPLOAD_FAILED_IMAGE_MESSAGE", nil);
    }
    else
    {
        message = NSLocalizedString(@"UPLOADER_UPLOAD_FAILED_VIDEO_MESSAGE", nil);
    }
    
    if(self.sfApp.isEditingMode) {
        @synchronized (self.sfApp.editedMedias) {
            for (id obj in self.sfApp.editedMedias) {
                if([obj isKindOfClass:[PHAsset class]]) {
                    BOOL find = NO;
                    for (PHAsset *asset in self.uploaderStayfilm.listUploadMedias) {
                        if(asset == obj) {
                            find = YES;
                            break;
                        }
                    }
                    if(!find) {
                        NSUInteger i = [self.sfApp.editedMedias indexOfObject:obj];
                        [self.sfApp.editedMedias removeObjectAtIndex:i];
                        [self setMovieInfo];
                        break;
                    }
                }
            }
        }
    } else {
        @synchronized (self.sfApp.finalMedias) {
            for (id obj in self.sfApp.finalMedias) {
                if([obj isKindOfClass:[PHAsset class]]) {
                    BOOL find = NO;
                    for (PHAsset *asset in self.uploaderStayfilm.listUploadMedias) {
                        if(asset == obj) {
                            find = YES;
                            break;
                        }
                    }
                    if(!find) {
                        NSUInteger i = [self.sfApp.finalMedias indexOfObject:obj];
                        [self.sfApp.finalMedias removeObjectAtIndex:i];
                        [self setMovieInfo];
                        break;
                    }
                }
            }
        }
    }
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_UPLOAD_FAILED_TITLE", nil)
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)uploadDidFinish:(Uploader *)uploader {
    if(self.sfApp == nil) {
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    }
    if(self.uploaderStayfilm.listUploadedMedias != nil && self.uploaderStayfilm.listUploadedMedias.count >0) {
        for (PHAsset * asset in self.uploaderStayfilm.listUploadedMedias.allValues) {
            if(self.sfApp.isEditingMode) {
                if(![self.sfApp.editedMedias containsObject:asset]) {
                    [self.sfApp.editedMedias addObject:asset];
                }
            } else {
                if(![self.sfApp.finalMedias containsObject:asset]) {
                    [self.sfApp.finalMedias addObject:asset];
                }
            }
        }
    }
    if(self.sfApp.isDebugMode) {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_FINISHED", nil)
                                                                       message:[[NSString alloc] initWithFormat:NSLocalizedString(@"UPLOADED_X_MEDIAS", nil), uploader.listUploadedMedias.count]
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    [self setMovieInfo];
}

- (void)uploadDidStart {
    // do nothing
}

- (void)uploadDidStartSendingConfirmationMedias:(Uploader *)uploader {
    // not important on this page.
}

- (void)uploadFinishedMedia:(PHAsset *)asset {
    // do nothing
}

- (void)uploadProgressChanged:(NSNumber *)progress {
    // not important in this screen
}








/*
 // old methods
 - (IBAction)but_remake_Clicked:(id)sender {
 [self tapOutside];
 [self.delegate performSelector:@selector(chooseOption:) withObject:@"remake"];
 }
 
 - (IBAction)but_makeNew_Clicked:(id)sender {
 [self tapOutside];
 [self.delegate performSelector:@selector(chooseOption:) withObject:@"makeNew"];
 }
 */
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */



@end
