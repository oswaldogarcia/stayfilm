//
//  Popover_SaveVideo_ViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Henrique on 5/17/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol Popover_SaveVideo_ViewControllerDelegate <NSObject>
@required
-(void)chooseOption:(NSString*)option;
@end

@interface Popover_SaveVideo_ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *but_Download;
@property (weak, nonatomic) IBOutlet UIButton *but_Cancel;

@property (nonatomic, strong) id<Popover_SaveVideo_ViewControllerDelegate> delegate;

-(id)initWithDelegate:(id<Popover_SaveVideo_ViewControllerDelegate>)p_delegate;

@end
