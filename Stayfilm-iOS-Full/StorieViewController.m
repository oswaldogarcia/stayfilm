//
//  StorieViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 13/06/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import "StorieViewController.h"
#import "StoriesPageViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <Google/Analytics.h>
#import "UIView+Toast.h"
#import "Story.h"
#import "Reachability.h"

@interface StorieViewController ()<StoryDelegate>

@property(nonatomic, weak) StayfilmApp *sfApp;

@property(nonatomic, strong) NSTimer *connectionStatusTimer;

@end

@implementation StorieViewController



- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self prefersStatusBarHidden];
    [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
   
    [self.swipeUpView.layer setZPosition:1];
    
    self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    
    // adjust screen size for iphone X
    if (@available(iOS 11.0, *)) {
        UIWindow *mainWindow = [[[UIApplication sharedApplication] delegate] window];
        if (mainWindow.safeAreaInsets.top > 0.0) {
//            [self.iPhoneXSizeFix setConstant:-44];
            [self.logoConstraint setConstant:50.f];
            [self.xConstraint setConstant:40.f];
        }
    }
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    
    if (networkStatus != NotReachable) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self connectivityChanged:YES];
        });
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self connectivityChanged:NO];
        });
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    
    
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)exitAction:(id)sender {
    
    @try {
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:self.sfApp.analyticsTag
                                                              action:@"close"
                                                               label:@"button-x"
                                                               value:@1] build]];
    }@catch (NSException *ex) {
        NSLog(@"StayLog: EXCEPTION in exitAction with error: %@",ex.description);
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)but_makeAFilm_tapped:(id)sender {
    if(self.sfApp == nil)
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    
    self.sfApp.selectedGenre = self.sfApp.storyState.currentGenre;
    self.sfApp.selectedTemplate = self.sfApp.storyState.currentTemplate;
    
    if(self.sfApp.currentUser == nil || self.sfApp.currentUser.idUser == nil || [self.sfApp.currentUser.idUser isEqualToString:@""]) {
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:self.sfApp.analyticsTag
                                                              action:@"make-film"
                                                               label:@"create_account"
                                                               value:@1] build]];
    } else {
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:self.sfApp.analyticsTag
                                                              action:@"make-film"
                                                               label:@"film_producing"
                                                               value:@1] build]];
    }
    
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"choice"
                                                          action:self.sfApp.selectedGenre.slug
                                                           label:[NSString stringWithFormat:@"%li", self.sfApp.selectedTemplate.idtemplate]
                                                           value:@1] build]];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.delegate performSelector:@selector(exitProfile) withObject:nil];
}


#pragma mark - Story Delegate
/*
- (void)changeLogo:(NSString*)urlImageLogo{
    
    [self.storyLogoImage sd_setImageWithURL:[NSURL URLWithString:urlImageLogo]];
    
}

-(void)buttonIsEnable:(BOOL)enable{
    
    [self.buttonView setHidden:!enable];
    [self.makeAFilmButton setHidden:!enable];
}
- (void)closeButtonIsEnable:(BOOL) enable{
    [self.closeButton setHidden:!enable];
}

-(void)showSwipeUpToast:(BOOL)show {

    if(show){
    
    [UIView animateWithDuration:5.0 animations:^{
        [self.swipeUpView setAlpha:1.0];
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:5.0 animations:^{
            [self.swipeUpView setAlpha:0.0];
        } completion:nil];
        
        
    }];
    
    }else{
        
         [self.swipeUpView setAlpha:0.0];
    }
    
}
- (void)changeButtonText:(NSString *)text Color:(UIColor *)color andIcon:(NSString*) imageString{
    
    self.buttonTextLabel.text = text;
    
    [self.buttonView setBackgroundColor:color];
    
    //NSLog(@"URL LOGO : %@", imageString);
    
    if(imageString != nil){
        [self.iconButtonImage sd_setImageWithURL:[NSURL URLWithString:imageString]];
    }else{
        [self.iconButtonImage setImage:[UIImage imageNamed:@"clapper"]];
    }
    
    
}
- (void)changeDescription:(NSString *)description{
    
    self.storyDescriptionLabel.text = description;
    [self.storyDescriptionLabel setHidden:NO];
}
*/
- (void)makeAFilmAction{
    
    [self.delegate performSelector:@selector(exitProfile) withObject:nil];

}

- (void)setAUXNavigationController:(UINavigationController *)nav {
}


- (void)setCurrentIDGenre:(int)p_genre andIDTemplate:(int)p_template {
}


#pragma mark - StayfilmApp Delegates

-(void)sfSingletonCreated {
    // nothing
}
-(void)connectivityChanged:(BOOL)isConnected {
    self.lbl_connectionStatus.text = NSLocalizedString(@"NO_INTERNET_RECONNECTING", nil);
    if(!isConnected) {
        self.view_connectionStatus.alpha = 0.0;
        [self.view_connectionStatus setHidden:NO];
        [UIView animateWithDuration:0.4 animations:^{
            self.view_connectionStatus.alpha = 1.0;
        }];
        if(self.connectionStatusTimer == nil) {
            __weak typeof(self) selfDelegate = self;
            self.connectionStatusTimer = [NSTimer timerWithTimeInterval:1.0 repeats:YES block:^(NSTimer * _Nonnull timer) {
                Reachability *reachability = [Reachability reachabilityForInternetConnection];
                NetworkStatus networkStatus = [reachability currentReachabilityStatus];
                
                if (networkStatus != NotReachable) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [selfDelegate connectivityChanged:YES];
                    });
                    [timer invalidate];
                    selfDelegate.connectionStatusTimer = nil;
                }
            }];
        }
    } else if(!self.view_connectionStatus.isHidden){
        self.view_connectionStatus.alpha = 1.0;
        [self.view_connectionStatus setHidden:NO];
        [UIView animateWithDuration:0.4 animations:^{
            self.view_connectionStatus.alpha = 0.0;
        } completion:^(BOOL finished) {
            [self.view_connectionStatus setHidden:YES];
        }];
    }
//    if(!isConnected) {
//        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"NO_INTERNET_CONNECTION", nil)
//                                                                       message:NSLocalizedString(@"NO_INTERNET_CONNECTION_MESSAGE", nil)
//                                                                preferredStyle:UIAlertControllerStyleAlert];
//
//        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
//                                                              handler:^(UIAlertAction * action) {}];
//
//        [alert addAction:defaultAction];
//        [self presentViewController:alert animated:YES completion:nil];
//    }
}

- (void)changedConnectivity:(BOOL)connected {
    [self connectivityChanged:connected];
}




#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"page"]){
        
        StoriesPageViewController *page = segue.destinationViewController;
        //        page.myFilms = self.myFilms;
        page.storiesArray = self.storiesArray;
        page.storySelected = self.storySelected;
        page.storyDelegate = self;
        
    }
    
}
@end
