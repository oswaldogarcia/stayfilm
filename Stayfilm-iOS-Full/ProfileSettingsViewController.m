//
//  ProfileSettingsViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 30/04/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import "ProfileSettingsViewController.h"
#import <Google/Analytics.h>

@interface ProfileSettingsViewController ()

@end

@implementation ProfileSettingsViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setLanguage];
    
    UITapGestureRecognizer *modalTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOutside)];
    modalTap.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:modalTap];
    
}

- (void)viewDidAppear:(BOOL)animated{
    
    //Google Analytics
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"settings"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
}
-(void)tapOutside{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)setLanguage{
    
    [self.editProfileButton setTitle:NSLocalizedString(@"EDIT_PROFILE", nil) forState:normal];
    [self.logoutButton setTitle:NSLocalizedString(@"LOGOUT", nil) forState:normal];
    [self.cancelButton setTitle:NSLocalizedString(@"CANCEL", nil) forState:normal];
    
    self.versionLabel.text = [NSString stringWithFormat:NSLocalizedString(@"VERSION_X",nil),[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
    
    
}


- (IBAction)editProfileAction:(id)sender {
    
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"user"
                                                          action:@"edit-profile"
                                                           label:@""
                                                           value:@1] build]];
    
    [self tapOutside];
    [self.delegate didSelectEditProfile:self.sfAppProfile.currentUser];
   
}

- (IBAction)logoutAction:(id)sender {

     
     UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ARE_YOU_SURE", nil)
     message:NSLocalizedString(@"DO_LOGOUT", nil)
     preferredStyle:UIAlertControllerStyleAlert];
     
     UIAlertAction* okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"YES", nil) style:UIAlertActionStyleDefault
     handler:^(UIAlertAction * action) {
         [alert dismissViewControllerAnimated:YES completion:nil];
         
         //Google Analytics Event
         id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
         [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"user"
                                                               action:@"logout"
                                                                label:@"yes"
                                                                value:@1] build]];
         
         [self.sfAppProfile logout];
     
     }];
     
     UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"NO", nil) style:UIAlertActionStyleCancel
     handler:^(UIAlertAction * action) {
         [alert dismissViewControllerAnimated:YES completion:nil];
         
         //Google Analytics Event
         id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
         [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"user"
                                                               action:@"logout"
                                                                label:@"no"
                                                                value:@1] build]];
     }];
     
     [alert addAction:okAction];
     [alert addAction:cancelAction];
     [self presentViewController:alert animated:YES completion:nil];
    
    
    
}
- (IBAction)FAQAction:(id)sender {
    
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"faq"
                                                          action:@" "
                                                           label:@""
                                                           value:@1] build]];
    
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://www.stayfilm.com/institutional/faq"]];
    
}
- (IBAction)TermsAction:(id)sender {
    
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"terms-conditions"
                                                          action:@" "
                                                           label:@""
                                                           value:@1] build]];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://www.stayfilm.com/institutional/terms"]];
    
    
}

- (IBAction)cancelAction:(id)sender {
    
    [self tapOutside];
    
}





@end
