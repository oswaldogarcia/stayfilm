 //
//  Uploader.m
//  Stayfilm
//
//  Created by Henrique Ormonde on 26/10/15.
//  Copyright © 2015 Stayfilm. All rights reserved.
//

#import "Uploader.h"

@interface Uploader ()

@property (atomic, strong) NSMutableDictionary *listFailedMediasAttempts;
@property (nonatomic, strong) NSString *albumID;
@property (nonatomic, assign) int failedAttempts;
@property (nonatomic, assign) int fileNumberImage;
@property (nonatomic, assign) int fileNumberVideo;

@end

@implementation Uploader

@synthesize listUploadingMedias, uploadQueue, listUploadMedias, listUploadedMedias, listConfirmMedias, albumID, uploadedAlbum, isUploading, failedAttempts, listFailedMediasAttempts, dictionaryMediaIDwithMediaPath, delegate, fileNumberImage, fileNumberVideo;


static Uploader *singletonUploaderObject = nil;


- (id)init
{
    if(self = [super init])
    {
        self.listFailedMediasAttempts = [[NSMutableDictionary alloc] init];
        self.listUploadMedias = [[NSMutableArray alloc] init];
        self.listUploadedMedias = [[NSMutableDictionary alloc] init];
        self.listConfirmMedias = [[NSMutableArray alloc] init];
        self.albumID = [[NSUUID UUID] UUIDString];
        self.listUploadingMedias = [[NSMutableDictionary alloc] init];
        self.dictionaryMediaIDwithMediaPath = [[NSMutableDictionary alloc] init];
        self.isUploading = NO;
//        self.maxErrorAttemptsReached = NO;
        self.uploadQueue = [NSOperationQueue new];
        self.uploadQueue.name = @"Stayfilm Upload Process Queue";
        self.uploadQueue.maxConcurrentOperationCount = 4;
        self.uploadQueue.qualityOfService = NSOperationQualityOfServiceUserInitiated;
        self.failedAttempts = 0;
        self.fileNumberVideo = 1;
        self.fileNumberImage = 1;
        self.uploadedAlbum = nil;
    }
    return self;
}

+ (Uploader *) sharedUploadStayfilmSingletonWithDelegate:(id<StayfilmUploaderDelegate>)theDelegate {
    
    @synchronized(self)
    {
        if(singletonUploaderObject == nil)
        {
            singletonUploaderObject = [[self alloc] init];
        }
        singletonUploaderObject.delegate = theDelegate;
    }
    return singletonUploaderObject;
}
+ (Uploader *) sharedUploadStayfilmSingleton {
    @synchronized(self)
    {
        if(singletonUploaderObject == nil)
        {
            singletonUploaderObject = [[self alloc] init];
        }
    }
    return singletonUploaderObject;
}

- (int)addMedias:(NSArray *)p_medias {
    int added = 0;
    int count = 0;

    for (id object in p_medias) {
        if([object isKindOfClass:[PHAsset class]] && ![self.listUploadMedias containsObject:object])
        {
            [self.listUploadMedias addObject:object];
            ++added;
        }
        else if([object isKindOfClass:[PHAsset class]])
        {
            ++count;
        }
    }
    if(added)
    {
//        //TODO: test same solution in Downloader that doesn't cancel all current uploads
//        if (self.isUploading) {
//            [self.uploadQueue cancelAllOperations];
//        }
        [self startOperations];
    }
    
    return count;
}

- (void)startOperations{
    @synchronized(self) {
        runOnMainQueueWithoutDeadlocking(^{
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        });
        @try {
            BOOL somethingWrong = NO;
            self.isUploading = YES;
            
            for (PHAsset *object in self.listUploadMedias)
            {
                BOOL found = NO;
                for (NSString* key in self.listUploadedMedias) {
                    PHAsset *value = [self.listUploadedMedias objectForKey:key];
                    if(value == object)
                    {
                        found = YES;
                        break;
                    }
                    else { // lets check if it is already uploading
                        for (NSString* keyUploading in self.listUploadingMedias) {
                            PHAsset *valueUploading = [self.listUploadingMedias objectForKey:keyUploading];
                            if(valueUploading == object) {
                                found = YES;
                                break;
                            }
                        }
                    }
                }
                if(found)
                {
                    continue;
                }
                else // file not uploaded yet
                {
                    @try
                    {
                        NSString* docsPath = [NSTemporaryDirectory()stringByStandardizingPath];
                        NSString* filePathImage = nil;
                        NSString* filePathVideo = nil;
                        NSFileManager* fileMgr = [[NSFileManager alloc] init];
                        int i = self.fileNumberImage;
                        do {
                            filePathImage = [NSString stringWithFormat:@"%@/%@%03d.%@", docsPath, @"sf_photo_", i++, @"jpg"];
                        } while ([fileMgr fileExistsAtPath:filePathImage]);
                        self.fileNumberImage = i +1;
                        i = self.fileNumberVideo;
                        do {
                            filePathVideo = [NSString stringWithFormat:@"%@/%@%03d.%@", docsPath, @"sf_video_", i++, @"mov"];
                        } while ([fileMgr fileExistsAtPath:filePathVideo]);
                        self.fileNumberVideo = i +1;
                        
                        if(object.mediaType == PHAssetMediaTypeImage)
                        {
                            ConvertMediaOperation *converter = [[ConvertMediaOperation alloc] initWithAsset:object withMediaPath:filePathImage delegate:self];
                            ImageUploadOperation *imageUpload = [[ImageUploadOperation alloc] initWithMediaPath:filePathImage withEcho:filePathImage withAlbumID:self.albumID withBackgroundSessionIdentifier:filePathImage delegate:self];
                            [imageUpload addDependency:converter];
                            [self.listUploadingMedias setObject:object forKey:filePathImage];
                            
                            [self.uploadQueue addOperation:converter];
                            [self.uploadQueue addOperation:imageUpload];
                        }
                        else if(object.mediaType == PHAssetMediaTypeVideo)
                        {
                            ConvertMediaOperation *converter = [[ConvertMediaOperation alloc] initWithAsset:object withMediaPath:filePathVideo delegate:self];
                            VideoUploadOperation *videoUpload = [[VideoUploadOperation alloc] initWithMediaPath:filePathVideo withEcho:filePathVideo withAlbumID:self.albumID withBackgroundSessionIdentifier:filePathVideo delegate:self];
                            [videoUpload addDependency:converter];
                            [self.listUploadingMedias setObject:object forKey:filePathVideo];
                            
                            [self.uploadQueue addOperation:converter];
                            [self.uploadQueue addOperation:videoUpload];
                        }
                        else{
                            NSLog(@"StayLog: Media Asset Type UNKNOWN");
                        }
                        
                    }
                    @catch (NSException *exception) {
                        NSLog(@"StayLog: EXCEPTION processing asset: %@ with error: %@",object.localIdentifier, exception.description);
                        somethingWrong = YES;
                    }
                }
                if(somethingWrong)
                {
                    NSLog(@"StayLog: Next exception is part of the workflow. (startOperations)");
                    [NSException raise:@"Something Wrong" format:@"upload interrupted."];
                }
            }
        }
        @catch (NSException *exception) {
            NSLog(@"StayLog: EXCEPTION in the StartOperations that does for in listUploadMedias with error: %@", exception.description);
            self.isUploading = NO;
            [self.uploadQueue cancelAllOperations];
            ++self.failedAttempts;
            if(self.failedAttempts <= 2)
            {
                [self startOperations];
            }
            else
            {
                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                [self resetUpload];
                [(NSObject *)self.delegate performSelectorOnMainThread:@selector(uploadDidFail:) withObject:self waitUntilDone:NO];
            }
        }
    }
    [(NSObject *)self.delegate performSelectorOnMainThread:@selector(uploadDidStart) withObject:nil waitUntilDone:NO];
}

- (BOOL)resetUpload {
    @synchronized(self) {
        [self.uploadQueue cancelAllOperations];
        self.listUploadMedias = nil;
        self.listUploadedMedias = nil;
        self.listConfirmMedias = nil;
        self.listFailedMediasAttempts = nil;
        self.uploadedAlbum = nil;
        self.uploadQueue = nil;
        self.dictionaryMediaIDwithMediaPath = nil;
        
        self.listFailedMediasAttempts = [[NSMutableDictionary alloc] init];
        self.listUploadMedias = [[NSMutableArray alloc] init];
        self.listUploadedMedias = [[NSMutableDictionary alloc] init];
        self.listConfirmMedias = [[NSMutableArray alloc] init];
        self.dictionaryMediaIDwithMediaPath = [[NSMutableDictionary alloc] init];
        self.albumID = [[NSUUID UUID] UUIDString];
        self.listUploadingMedias = [[NSMutableDictionary alloc] init];
        self.isUploading = NO;
        //    self.maxErrorAttemptsReached = NO;
        self.uploadQueue = [NSOperationQueue new];
        self.uploadQueue.name = @"Stayfilm Upload Process Queue";
        self.uploadQueue.maxConcurrentOperationCount = 4;
        self.uploadQueue.qualityOfService = NSOperationQualityOfServiceUserInitiated;
        self.failedAttempts = 0;
        self.fileNumberImage = 1;
        self.fileNumberVideo = 1;
        return YES;
    }
}

- (BOOL)isAssetUploading:(PHAsset *)p_asset {
    if([self.listUploadingMedias.allValues containsObject:p_asset]) {
        return YES;
    }
    return NO;
}

- (BOOL)isAssetUploaded:(PHAsset *)p_asset {
    if([self.listUploadedMedias.allValues containsObject:p_asset]) {
        return YES;
    }
    return NO;
}

- (BOOL)isAssetOnUploadList:(PHAsset *)p_asset {
    if([self.listUploadMedias containsObject:p_asset] && ![self.listUploadedMedias.allValues containsObject:p_asset]) {
        return YES;
    }
    return NO;
}

- (BOOL)removeAssetFromUpload:(id)p_asset {
    BOOL removed = NO;
    @try {
        if([p_asset isKindOfClass:[PHAsset class]]) {
            NSString *idKey = nil;
            if([self.listUploadMedias containsObject:p_asset]) {
                [self.listUploadMedias removeObject:p_asset];
                removed = YES;
            }
            if([self.listUploadedMedias.allValues containsObject:p_asset]) {
                NSArray *arr = [self.listUploadedMedias allKeysForObject:p_asset];
                NSString *key = [arr lastObject];
                [self.listUploadedMedias removeObjectForKey:key];
                
                for (NSString* k in self.dictionaryMediaIDwithMediaPath) {
                    NSString *val = [self.dictionaryMediaIDwithMediaPath objectForKey:k];
                    if([val isEqualToString:key]) {
                        idKey = k;
                        [self.dictionaryMediaIDwithMediaPath removeObjectForKey:k];
                        removed = YES;
                        break;
                    }
                }
            }
            if([self.listUploadingMedias.allValues containsObject:p_asset]) {
                NSArray *arr = [self.listUploadingMedias allKeysForObject:p_asset];
                NSString *key = [arr lastObject];
                [self.listUploadingMedias removeObjectForKey:key];
                // now we need to cancel the upload
                for (id op in self.uploadQueue.operations) {
                    if([op isKindOfClass:[ImageUploadOperation class]]) {
                        if([((ImageUploadOperation *)op).pathFile isEqualToString:key]) {
                            [((ImageUploadOperation *)op) cancel];
                        }
                    } else if([op isKindOfClass:[VideoUploadOperation class]]) {
                        if([((VideoUploadOperation *)op).pathFile isEqualToString:key]) {
                            [((VideoUploadOperation *)op) cancel];
                        }
                    }
                }
                for (NSString* k in self.dictionaryMediaIDwithMediaPath) {
                    NSString *val = [self.dictionaryMediaIDwithMediaPath objectForKey:k];
                    if([val isEqualToString:key]) {
                        idKey = k;
                        [self.dictionaryMediaIDwithMediaPath removeObjectForKey:k];
                        removed = YES;
                        break;
                    }
                }
            }
            for (id m in self.listConfirmMedias) {
                if([m isKindOfClass:[Media class]] && [((Media *)m).idMidia isEqualToString:idKey]) {
                    [self.listConfirmMedias removeObject:m];
                    removed = YES;
                    break;
                }
            }
            for (id m in self.uploadedAlbum.idMedias) {
                if([m isKindOfClass:[Media class]] && [((Media *)m).idMidia isEqualToString:idKey]) {
                    [self.uploadedAlbum.idMedias removeObject:m];
                    removed = YES;
                    break;
                }
            }
        }
    }
    @catch (NSException *ex) {
        NSLog(@"StayLog: EXCEPTION in removeAssetFromUpload with error: %@", ex.description);
    }
    return removed;
}

- (BOOL)isUploadFinished {
    return (([self.uploadQueue operationCount] == 0) && !self.isUploading);
}

- (int)getUploadPercentage
{
    return (int)(self.listUploadedMedias.count * 100 / self.listUploadMedias.count);
}

BOOL isSendingConfirmation = NO;
- (void)sendConfirmMedias
{
    if(!isSendingConfirmation)
    {
        @try {
            isSendingConfirmation = YES;
            [(NSObject *)self.delegate performSelectorOnMainThread:@selector(uploadDidStartSendingConfirmationMedias:) withObject:nil waitUntilDone:NO];
            
            ConfirmMediasOperation *confirmOP = [[ConfirmMediasOperation alloc] initWithMedias:self.listConfirmMedias withAlbum:self.uploadedAlbum withAlbumID:self.albumID delegate:self];
            [self.uploadQueue addOperation:confirmOP];
            
            isSendingConfirmation = NO;
        }
        @catch (NSException *exception) {
            NSLog(@"StayLog: EXCEPTION on sendConfirmationMedias with error: %@", exception.description);
            isSendingConfirmation = NO;
        }
    }
}

-(void)updateUpdateProgress
{
    NSNumber *progress = [[NSNumber alloc] initWithFloat:((float)self.listUploadedMedias.count / (float)self.listUploadMedias.count)];
    [(NSObject *)self.delegate performSelectorOnMainThread:@selector(uploadProgressChanged:) withObject:progress waitUntilDone:NO];
}

#pragma mark - URL Delegates

- (void)URLSessionDidFinishEventsForBackgroundURLSession:(NSURLSession *)session
{
    if (self.backgroundSessionCompletionHandler) {
        self.backgroundSessionCompletionHandler();
        self.backgroundSessionCompletionHandler = nil;
    }
}


#pragma mark - Operations Callback

-(void) converterDidFail:(ConvertMediaOperation *)converter
{
    @try {
        NSNumber *attempts;
        if(self.listFailedMediasAttempts[converter.asset] != nil)
        {
            attempts = self.listFailedMediasAttempts[converter.asset];
        }
        else
        {
            attempts = [[NSNumber alloc] initWithInt:0];
        }
        int value = [attempts intValue];
        attempts = [NSNumber numberWithInt:value + 1];
        
        [self.listFailedMediasAttempts setObject:attempts forKey:converter.asset];
        
        if([attempts intValue] <= 2)
        {
            if(converter.asset.mediaType == PHAssetMediaTypeImage)
            {
                ConvertMediaOperation *converterOP = [[ConvertMediaOperation alloc] initWithAsset:converter.asset withMediaPath:converter.mediaPath delegate:self];
                ImageUploadOperation *imageUpload = [[ImageUploadOperation alloc] initWithMediaPath:converter.mediaPath withEcho:converter.mediaPath withAlbumID:self.albumID withBackgroundSessionIdentifier:[converter.mediaPath stringByAppendingString:[[NSString alloc] initWithFormat:@"%d",[attempts intValue]]] delegate:self];
                [imageUpload addDependency:converterOP];
                [self.uploadQueue addOperation:converterOP];
                [self.uploadQueue addOperation:imageUpload];
            }
            else if(converter.asset.mediaType == PHAssetMediaTypeVideo)
            {
                ConvertMediaOperation *converterOP = [[ConvertMediaOperation alloc] initWithAsset:converter.asset withMediaPath:converter.mediaPath delegate:self];
                VideoUploadOperation *videoUpload = [[VideoUploadOperation alloc] initWithMediaPath:converter.mediaPath withEcho:converter.mediaPath withAlbumID:self.albumID withBackgroundSessionIdentifier:[converter.mediaPath stringByAppendingString:[[NSString alloc] initWithFormat:@"%d",[attempts intValue]]] delegate:self];
                [videoUpload addDependency:converterOP];
                [self.uploadQueue addOperation:converterOP];
                [self.uploadQueue addOperation:videoUpload];
            }
        }
        else // media failed 3 times, so lets skip it
        {
            @synchronized(self.listUploadMedias) {
                [self.listUploadMedias removeObject:converter.asset];
            }
            if(converter.asset.mediaType == PHAssetMediaTypeImage)
            {
                [(NSObject *)self.delegate performSelectorOnMainThread:@selector(converterDidFailMedia:) withObject:@"image" waitUntilDone:NO];
            }
            else if(converter.asset.mediaType == PHAssetMediaTypeVideo)
            {
                [(NSObject *)self.delegate performSelectorOnMainThread:@selector(converterDidFailMedia:) withObject:@"video" waitUntilDone:NO];
            }
            
            //check if was the last media
            if(self.listUploadedMedias.count == self.listUploadMedias.count)
            {
                [self sendConfirmMedias];
            }
        }

    }
    @catch (NSException *exception) {
        NSLog(@"StayLog: EXCEPTION in converterDidFail with error: %@", exception.description);
        @synchronized(self.listUploadMedias) {
            [self.listUploadMedias removeObject:converter.asset];
        }
        if(converter.asset.mediaType == PHAssetMediaTypeImage)
        {
            [(NSObject *)self.delegate performSelectorOnMainThread:@selector(converterDidFailMedia:) withObject:@"image" waitUntilDone:NO];
        }
        else if(converter.asset.mediaType == PHAssetMediaTypeVideo)
        {
            [(NSObject *)self.delegate performSelectorOnMainThread:@selector(converterDidFailMedia:) withObject:@"video" waitUntilDone:NO];
        }
    }
}

-(void) converterDidFinish:(ConvertMediaOperation *)converter
{
    NSLog(@"StayLog: Convertion of file: %@ SUCCESS.", converter.mediaPath);
}

-(void) imageUploaderDidFail:(ImageUploadOperation *)uploader
{
    @try {
        NSNumber *attempts;
        if(self.listFailedMediasAttempts[uploader.pathFile] != nil)
        {
            attempts = self.listFailedMediasAttempts[uploader.pathFile];
        }
        else
        {
            attempts = [[NSNumber alloc] initWithInt:0];
        }
        int value = [attempts intValue];
        attempts = [NSNumber numberWithInt:value + 1];
        
        [self.listFailedMediasAttempts setObject:attempts forKey:uploader.pathFile];
        
        if([attempts intValue] <= 2)
        {
            ImageUploadOperation *imageUpload = [[ImageUploadOperation alloc] initWithMediaPath:uploader.pathFile withEcho:uploader.pathFile withAlbumID:self.albumID withBackgroundSessionIdentifier:[[uploader.pathFile lastPathComponent] stringByAppendingString:[[NSString alloc] initWithFormat:@"%d",[attempts intValue]]] delegate:self];
            [self.uploadQueue addOperation:imageUpload];
        }
        else // media failed 3 times, so lets skip it
        {
            @synchronized(self) {
                if(self.listUploadingMedias[uploader.pathFile] != nil)
                {
                    [self.listUploadMedias removeObject:self.listUploadingMedias[uploader.pathFile]];
                    [self.listUploadingMedias removeObjectForKey:uploader.pathFile];
                }
            }
            [(NSObject *)self.delegate performSelectorOnMainThread:@selector(uploadDidFailMedia:) withObject:@"image" waitUntilDone:NO];
            
            //check if was the last media
            if(self.listUploadedMedias.count == self.listUploadMedias.count)
            {
                [self sendConfirmMedias];
            }
        }
        
    }
    @catch (NSException *exception) {
        NSLog(@"StayLog: EXCEPTION in converterDidFail with error: %@", exception.description);
        @synchronized(self) {
            if(self.listUploadingMedias[uploader.pathFile] != nil)
            {
                [self.listUploadMedias removeObject:self.listUploadingMedias[uploader.pathFile]];
                [self.listUploadingMedias removeObjectForKey:uploader.pathFile];
            }
        }
        [(NSObject *)self.delegate performSelectorOnMainThread:@selector(uploadDidFailMedia:) withObject:@"image" waitUntilDone:NO];
    }
}

-(void) imageUploaderDidFinish:(ImageUploadOperation *)uploader
{
    @synchronized(self)
    {
        @try {
            if(self.listUploadingMedias[uploader.pathFile] != nil) {
                [self.listUploadedMedias setObject:self.listUploadingMedias[uploader.pathFile] forKey:uploader.pathFile];
                if(self.delegate != nil)
                    [(NSObject *)self.delegate performSelectorOnMainThread:@selector(uploadFinishedMedia:) withObject:self.listUploadingMedias[uploader.pathFile] waitUntilDone:NO];
                [self.listUploadingMedias removeObjectForKey:uploader.pathFile];
                [self.listConfirmMedias addObject:uploader.uploadedMedia];
                [self.dictionaryMediaIDwithMediaPath setObject:uploader.pathFile forKey:uploader.uploadedMedia.idMidia];
                
                [self updateUpdateProgress];
            }
            
            if(self.listUploadedMedias.count == self.listUploadMedias.count)
            {
                [self sendConfirmMedias];
            }
        }
        @catch (NSException *ex) {
            NSLog(@"StayLog: EXCEPTION in Uploader imageUploaderDidFinish with error: %@", ex.description);
        }
    }
    NSLog(@"StayLog: Image uploaded successfuly: %@", uploader.pathFile);
}

-(void) videoUploaderDidFail:(VideoUploadOperation *)uploader
{
    @try {
        NSNumber *attempts;
        if(self.listFailedMediasAttempts[uploader.pathFile] != nil)
        {
            attempts = self.listFailedMediasAttempts[uploader.pathFile];
        }
        else
        {
            attempts = [[NSNumber alloc] initWithInt:0];
        }
        int value = [attempts intValue];
        attempts = [NSNumber numberWithInt:value + 1];
        
        [self.listFailedMediasAttempts setObject:attempts forKey:uploader.pathFile];
        
        if([attempts intValue] <= 2)
        {
            NSString *identif = [[uploader.pathFile lastPathComponent] stringByAppendingString:[[NSString alloc] initWithFormat:@"%d",[attempts intValue]]];
            VideoUploadOperation *videoUpload = [[VideoUploadOperation alloc] initWithMediaPath:uploader.pathFile withEcho:uploader.pathFile withAlbumID:self.albumID withBackgroundSessionIdentifier:identif delegate:self];
            [self.uploadQueue addOperation:videoUpload];
        }
        else // media failed 3 times, so lets skip it
        {
            @synchronized(self) {
                if(self.listUploadingMedias[uploader.pathFile] != nil)
                {
                    [self.listUploadMedias removeObject:self.listUploadingMedias[uploader.pathFile]];
                    [self.listUploadingMedias removeObjectForKey:uploader.pathFile];
                }
            }
            [(NSObject *)self.delegate performSelectorOnMainThread:@selector(uploadDidFailMedia:) withObject:@"video" waitUntilDone:NO];
            
            //check if was the last media
            if(self.listUploadedMedias.count == self.listUploadMedias.count)
            {
                [self sendConfirmMedias];
            }
        }
        
    }
    @catch (NSException *exception) {
        NSLog(@"StayLog: EXCEPTION in converterDidFail with error: %@", exception.description);
        @synchronized(self) {
            if(self.listUploadingMedias[uploader.pathFile] != nil)
            {
                [self.listUploadMedias removeObject:self.listUploadingMedias[uploader.pathFile]];
                [self.listUploadingMedias removeObjectForKey:uploader.pathFile];
            }
        }
        [(NSObject *)self.delegate performSelectorOnMainThread:@selector(uploadDidFailMedia:) withObject:@"image" waitUntilDone:NO];
    }
}

-(void) videoUploaderDidFinish:(VideoUploadOperation *)uploader
{
    @synchronized(self)
    {
        if(self.listUploadingMedias[uploader.pathFile] != nil) {
            [self.listUploadedMedias setObject:self.listUploadingMedias[uploader.pathFile] forKey:uploader.pathFile];
            if(self.delegate != nil)
                [(NSObject *)self.delegate performSelectorOnMainThread:@selector(uploadFinishedMedia:) withObject:self.listUploadingMedias[uploader.pathFile] waitUntilDone:NO];
            [self.listUploadingMedias removeObjectForKey:uploader.pathFile];
            [self.listConfirmMedias addObject:uploader.uploadedMedia];
            [self.dictionaryMediaIDwithMediaPath setObject:uploader.pathFile forKey:uploader.uploadedMedia.idMidia];
            
            [self updateUpdateProgress];
        }
        
        if(self.listUploadedMedias.count == self.listUploadMedias.count)
        {
            [self sendConfirmMedias];
        }
    }
    NSLog(@"StayLog: Video uploaded successfuly: %@", uploader.pathFile);
}

- (void) confirmMediasDidFailWithTheseMedias:(NSArray *)medias
{
    @try {
        for (NSString *mediaFailedID in medias)
        {
            for (NSString* path in self.dictionaryMediaIDwithMediaPath)
            {
                NSString *mediaid = [self.dictionaryMediaIDwithMediaPath objectForKey:path];
                if([mediaid isEqualToString:mediaFailedID])
                {
                    NSNumber *attempts;
                    if(self.listFailedMediasAttempts[path] != nil)
                    {
                        attempts = self.listFailedMediasAttempts[path];
                    }
                    else
                    {
                        attempts = [[NSNumber alloc] initWithInt:0];
                    }
                    int value = [attempts intValue];
                    attempts = [NSNumber numberWithInt:value + 1];
                    
                    [self.listFailedMediasAttempts setObject:attempts forKey:path];
                    
                    if([attempts intValue] <= 2)
                    {
                        if([[path pathExtension] containsString:@"jpg"])
                        {
                            ImageUploadOperation *imageUpload = [[ImageUploadOperation alloc] initWithMediaPath:path withEcho:path withAlbumID:self.albumID withBackgroundSessionIdentifier:[path stringByAppendingString:[[NSString alloc] initWithFormat:@"%d",[attempts intValue]]] delegate:self];
                            [self.uploadQueue addOperation:imageUpload];
                        }
                        else if([[path pathExtension] containsString:@"mov"])
                        {
                            VideoUploadOperation *videoUpload = [[VideoUploadOperation alloc] initWithMediaPath:path withEcho:path withAlbumID:self.albumID withBackgroundSessionIdentifier:[path stringByAppendingString:[[NSString alloc] initWithFormat:@"%d",[attempts intValue]]] delegate:self];
                            [self.uploadQueue addOperation:videoUpload];
                        }
                    }
                    else // media failed 3 times, so lets skip it
                    {
                        @synchronized(self) {
                            if(self.listUploadingMedias[path] != nil)
                            {
                                [self.listUploadMedias removeObject:self.listUploadingMedias[path]];
                                [self.listUploadingMedias removeObjectForKey:path];
                                [self.listUploadedMedias removeObjectForKey:path];
                                for (Media *object in self.listConfirmMedias) {
                                    if([object.idMidia isEqualToString:mediaid])
                                    {
                                        [self.listConfirmMedias removeObject:object];
                                        break;
                                    }
                                }
                            }
                        }
                        if([[path pathExtension] containsString:@"jpg"])
                        {
                            [(NSObject *)self.delegate performSelectorOnMainThread:@selector(uploadDidFailMedia:) withObject:@"image" waitUntilDone:NO];
                        }
                        else if([[path pathExtension] containsString:@"mov"])
                        {
                            [(NSObject *)self.delegate performSelectorOnMainThread:@selector(uploadDidFailMedia:) withObject:@"video" waitUntilDone:NO];
                        }
                    }
                }
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"StayLog: EXCEPTION on confirmMediasDidFailWithTheseMedias with error: %@", exception.description);
        @synchronized(self) {
            for (NSString *mediaFailedID in medias) {
                for (Media *media in self.listConfirmMedias) {
                    if([media.idMidia isEqualToString:mediaFailedID])
                    {
                        [self.listConfirmMedias removeObject:media];
                        
                        for (NSString* path in self.dictionaryMediaIDwithMediaPath)
                        {
                            NSString *mediaid = [self.dictionaryMediaIDwithMediaPath objectForKey:path];
                            if([mediaid isEqualToString:mediaFailedID])
                            {
                                [self.listUploadedMedias removeObjectForKey:path];
                                break;
                            }
                        }
                        break;
                    }
                }
            }
        }
        ConfirmMediasOperation *confirmOp = [[ConfirmMediasOperation alloc] initWithMedias:self.listConfirmMedias withAlbum:self.uploadedAlbum withAlbumID:self.albumID delegate:self];
        [self.uploadQueue addOperation:confirmOp];
    }
}

-(void) confirmMediasDidFail:(ConfirmMediasOperation *)confirmer
{
    @try {
        NSNumber *attempts;
        if(self.listFailedMediasAttempts[@"confirmer"] != nil)
        {
            attempts = self.listFailedMediasAttempts[@"confirmer"];
        }
        else
        {
            attempts = [[NSNumber alloc] initWithInt:0];
        }
        int value = [attempts intValue];
        attempts = [NSNumber numberWithInt:value + 1];
        
        [self.listFailedMediasAttempts setObject:attempts forKey:@"confirmer"];
        
        if([attempts intValue] <= 2)
        {
            ConfirmMediasOperation *confirmOp = [[ConfirmMediasOperation alloc] initWithMedias:self.listConfirmMedias withAlbum:self.uploadedAlbum withAlbumID:self.albumID delegate:self];
            [self.uploadQueue addOperation:confirmOp];
        }
        else // ConfirmMedia failed 3 times, so must restart upload
        {
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
            [self resetUpload];
            [(NSObject *)self.delegate performSelectorOnMainThread:@selector(uploadDidFail:) withObject:self waitUntilDone:NO];
        }
        
    }
    @catch (NSException *exception) {
        NSLog(@"StayLog: EXCEPTION in confirmerDidFail with error: %@", exception.description);
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        [self resetUpload];
        [(NSObject *)self.delegate performSelectorOnMainThread:@selector(uploadDidFail:) withObject:self waitUntilDone:NO];
    }
}

-(void) confirmMediasDidFinish:(ConfirmMediasOperation *)confirmer
{
    NSLog(@"StayLog: Confirm Medias SUCCESS");
    self.isUploading = NO;
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    self.uploadedAlbum = confirmer.confirmedAlbum;
    [(NSObject *)self.delegate performSelectorOnMainThread:@selector(uploadDidFinish:) withObject:self waitUntilDone:NO];
}

@end
