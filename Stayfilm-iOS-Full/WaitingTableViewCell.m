//
//  WaitingTableViewCell.m
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 12/11/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import "WaitingTableViewCell.h"

@implementation WaitingTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)initCell{
    
    self.statusLabel.text = NSLocalizedString(@"IN_REVIEW_TITLE", nil);
    
    self.movieTitleLabel.text = self.movie.title;
    
    [self.thumbnailImage showLoader];
    
    [self.thumbnailImage sd_setImageWithURL:[NSURL URLWithString: self.movie.thumbnailUrl] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        
        if([self.thumbnailImage isLoaderShowing]){
            [self.thumbnailImage hideLoader];
        }
        
        [self.thumbnailImage setImage:image];
        self.thumbnailImage.alpha = 0.5;;
    }];
    
    
}


@end
