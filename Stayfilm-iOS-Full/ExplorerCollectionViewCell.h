//
//  ExplorerCollectionViewCell.h
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 13/06/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExplorerCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *storieImage;
@property (weak, nonatomic) IBOutlet UILabel *storieTitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *borderImage;


@end
