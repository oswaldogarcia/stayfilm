//
//  ChangePrivacyViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 8/05/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Movie.h"

@protocol PrivacyOptionsDelegate<NSObject>

- (void)changePrivacy:(int)privacy position:(NSInteger)videoPosition;
- (void)changePrivacy:(int)privacy;

@end

@interface ChangePrivacyViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *publicView;
@property (weak, nonatomic) IBOutlet UIView *friendsView;
@property (weak, nonatomic) IBOutlet UIView *privateView;

@property (weak, nonatomic) IBOutlet UILabel *whoCanSeeLabel;


@property (weak, nonatomic) IBOutlet UILabel *publicLabel;
@property (weak, nonatomic) IBOutlet UILabel *publicDescriptionLabel;

@property (weak, nonatomic) IBOutlet UILabel *friendsLabel;
@property (weak, nonatomic) IBOutlet UILabel *friendsDescriptionLabel;

@property (weak, nonatomic) IBOutlet UILabel *privateLabel;
@property (weak, nonatomic) IBOutlet UILabel *privateDescriptionLabel;

@property (weak, nonatomic) IBOutlet UIButton *cancelButton;


@property (nonatomic, assign) NSInteger videoPosition;
@property (nonatomic, assign) NSInteger currentPrivacy;
@property (nonatomic, assign) id<PrivacyOptionsDelegate> delegate;
@end
