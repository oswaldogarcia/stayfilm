//
//  ConfirmMediasOperation.h
//  Stayfilm for Messenger
//
//  Created by Henrique Ormonde on 26/10/15.
//  Copyright © 2015 Stayfilm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Media.h"
#import "AlbumSF.h"

@protocol ConfirmMediaDelegate;

@interface ConfirmMediasOperation : NSOperation

@property (nonatomic, assign) id <ConfirmMediaDelegate> delegate;
@property (nonatomic, strong) NSArray *listConfirmMedias;
@property (nonatomic, strong) NSString *albumID;
@property (nonatomic, strong) AlbumSF *confirmedAlbum;

- (id)initWithMedias:(NSArray *)p_medias withAlbum:(AlbumSF *)p_album withAlbumID:(NSString *)p_albumID delegate:(id<ConfirmMediaDelegate>) theDelegate;

@end

@protocol ConfirmMediaDelegate <NSObject>
- (void)confirmMediasDidFailWithTheseMedias:(NSArray *)medias;
- (void)confirmMediasDidFinish:(ConfirmMediasOperation *)confirmer;
- (void)confirmMediasDidFail:(ConfirmMediasOperation *)confirmer;
@end
