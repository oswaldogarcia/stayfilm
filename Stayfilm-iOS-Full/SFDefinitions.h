//
//  SFDefinitions.h
//  Stayfilm
//
//  Created by Victor Manuel Roldan on 5/16/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <Foundation/Foundation.h>

#define sessionUser @"user"

/*Endpoints*/

#define uploadProfilePicture @"user/%@/profile"
#define getTemplates @"template?device=%@"
#define getUserMovies @"user/%@/movie?limit=%@&offset=%@"
#define deleteMovie @"movie/%@"
#define subscriptionPurchase @"user/%@/purchase"

/*Endpoints*/

/*HTTPMETHODS*/
#define httpPost @"POST"
#define httpGetWithOutParameters @"GET_WITH_OUT_PARAMETERS"
#define httpGet @"GET"
#define httpPut @"PUT"
#define httpDelete @"DELETE"
/*Endpoints*/

/* TITLES */
#define VALIDATION_DEFAULT_TITLE @"Validations"
#define ERROR_SERVER_DEFAULT_MESSAGE @"Server error"
#define ERROR_SERVER_DEFAULT_TITLE @"Server message"
#define ERROR_SERVER_DEFAULT_KEY @"serverError"
#define UNAUTHORIZED_DEFAULT_TILE @"Unauthorized"
/* TITLES */

#define httpContentTypeJson @"application/json"
#define httpContentTypeMultipart @"multipart/form-data"

//ROUTES
//#define SERVER_API_URL_BASE @"https://staging.stayfilm.com/rest/1.2/"
#define SERVER_API_URL_BASE @"https://www.stayfilm.com/rest/1.2/"
//#define SERVER_API_URL_BASE @"http://lucas.office.stayfilm.com.br/rest/1.1/"


/* IAP Plans identifiers */
#define IAP_MONTHLY_SUBSCRIPTION @"com.stayfilm.fun.MonthlySubscription"
/* IAP Plans identifiers */
