//
//  LoginViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Henrique on 1/24/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Google/Analytics.h>
#import "User.h"
#import "AppDelegate.h"
#import "StayfilmApp.h"
#import "LoginFacebookOperation.h"

@interface LoginViewController : UIViewController <FBSDKLoginButtonDelegate, StayfilmSingletonDelegate, LoginFacebookOperationDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imageBackground;
@property (weak, nonatomic) IBOutlet UILabel *lbl_version;

@property (weak, nonatomic) IBOutlet UILabel *lbl_Email;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Password;
@property (weak, nonatomic) IBOutlet UITextField *txt_Email;
@property (weak, nonatomic) IBOutlet UITextField *txt_Password;
@property (weak, nonatomic) IBOutlet UIButton *but_Enter;
@property (weak, nonatomic) IBOutlet UIButton *but_ForgotPassword;
@property (weak, nonatomic) IBOutlet FBSDKLoginButton *but_FacebookConnect;
@property (weak, nonatomic) IBOutlet UIButton *but_FacebookCustom;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinnerLoader;
@property (weak, nonatomic) IBOutlet UIView *viewLoader;

@end
