//
//  StepFinalPreviewViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Henrique on 1/8/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StayfilmApp.h"
#import "Uploader.h"

@interface StepFinalPreviewViewController : UIViewController <StayfilmUploaderDelegate>

@property (weak, nonatomic) IBOutlet UILabel *txt_TotalCountMedias;
@property (weak, nonatomic) IBOutlet UILabel *txt_Title;
@property (weak, nonatomic) IBOutlet UILabel *txt_Style;
@property (weak, nonatomic) IBOutlet UIButton *but_finish;



//@property (nonatomic, strong) NSMutableArray *selectedMedias;
@property (nonatomic, strong) NSString *selectedTitle;
@property (nonatomic, strong) Genres *selectedGenre;

@end
