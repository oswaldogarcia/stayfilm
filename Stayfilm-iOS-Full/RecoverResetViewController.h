//
//  RecoverResetViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Henrique on 5/1/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecoverResetViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *img_Background;
@property (weak, nonatomic) IBOutlet UITextField *txt_NewPassword;
@property (weak, nonatomic) IBOutlet UITextField *txt_ConfirmPassword;
@property (weak, nonatomic) IBOutlet UIButton *but_Save;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner_loader;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Status;

@property (nonatomic, strong) NSString * email;
@property (nonatomic, strong) NSString * code;

@end
