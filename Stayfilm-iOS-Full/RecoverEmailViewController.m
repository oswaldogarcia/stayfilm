//
//  RecoverEmailViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Henrique on 4/26/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import "RecoverEmailViewController.h"
#import <Google/Analytics.h>
#import "StayfilmApp.h"
#import "User.h"
#import "AppDelegate.h"
#import "Reachability.h"
#import "RecoverCodeViewController.h"

@interface RecoverEmailViewController ()

@property (nonatomic, weak) StayfilmApp *sfApp;
@property (nonatomic, assign) BOOL isSendingEmail;
@property (nonatomic, assign) BOOL isMovingToCode;
@property (nonatomic, strong) UITapGestureRecognizer * tapGest;

@end

@implementation RecoverEmailViewController

@synthesize txt_Email, but_SendEmail, spinner_loader, img_Background;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.isMovingToCode = NO;
    self.isSendingEmail = NO;
    [self.txt_Email addTarget:self action:@selector(txt_Email_Ended) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    self.tapGest = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backgroundTapped:)];
    self.tapGest.numberOfTapsRequired = 1;
    [self.tapGest setCancelsTouchesInView:NO];
    [self.img_Background addGestureRecognizer:self.tapGest];
}

-(void)backgroundTapped:(UIGestureRecognizer *)gesture {
    [self.view endEditing:YES];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    
    //Google Analytics
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"reset_password"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backButton_tapped:(id)sender {
    AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appdelegate goToLoginPage];
}

-(BOOL)connected {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

- (IBAction)but_SendEmail_clicked:(id)sender {
    if(![self connected])
    {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CONNECTION_ERROR", nil)
                                                                       message:NSLocalizedString(@"CONNECTION_ERROR_MESSAGE", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    if(self.sfApp == nil) {
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    }
    [self.view endEditing:YES];
    
    if(self.txt_Email.text != nil && ![self.txt_Email.text isEqualToString: @""])
    {
        NSArray* words = [self.txt_Email.text componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        self.txt_Email.text = [words componentsJoinedByString:@""];
        
        NSError *error = NULL;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"^[A-Z0-9a-z._%+-]+@.+.[A-Za-z]{2}[A-Za-z]*$"
                                                                               options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:self.txt_Email.text
                                                            options:0
                                                              range:NSMakeRange(0, [self.txt_Email.text length])];
        if(numberOfMatches > 0)
        {
            if(!self.isSendingEmail) {
                self.isSendingEmail = YES;
                [self.spinner_loader setHidden:NO];
                [self performSelectorInBackground:@selector(sendEmail:) withObject:self.txt_Email.text];
            }
            return;
        }
        else
        {
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"EMAIL_ERROR", nil)
                                                                           message:NSLocalizedString(@"EMAIL_ERROR_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) { [self.txt_Email becomeFirstResponder]; }];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
            return;
        }
    }
}

-(void)sendEmail:(NSString *)p_email {
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"password"
                                                          action:@"send"
                                                           label:@"email"
                                                           value:@1] build]];
    
    NSString *response = [User sendRecoveryEmail:p_email andSFAppSingleton:self.sfApp];
    if(response == nil) { //Valid response
        [self performSelectorOnMainThread:@selector(moveToNextStep) withObject:nil waitUntilDone:NO];
        //[self performSegueWithIdentifier:@"EmailToCodeSegue" sender:self];
    }
    else if ([response isEqualToString:@"exception"]) {
        runOnMainQueueWithoutDeadlocking(^{
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CONNECTION_ERROR", nil)
                                                                           message:NSLocalizedString(@"CONNECTION_ERROR_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) { [self.txt_Email becomeFirstResponder]; }];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        });
    }
    else { //TODO: We should send a Damn email to the user even if the user isn't registered... No email found on our database... no email will be sent... but we still going to next step to fool the user.... (yes I'm against this solution)
        [self performSelectorOnMainThread:@selector(moveToNextStep) withObject:nil waitUntilDone:NO];
        //[self performSegueWithIdentifier:@"EmailToCodeSegue" sender:self];
    }
    runOnMainQueueWithoutDeadlocking(^{
        [self.spinner_loader setHidden:YES];
    });
    self.isSendingEmail = NO;
}

-(void)txt_Email_Ended {
    if(self.txt_Email.text != nil && ![self.txt_Email.text isEqualToString: @""])
    {
        NSArray* words = [self.txt_Email.text componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        self.txt_Email.text = [words componentsJoinedByString:@""];
        
        NSError *error = NULL;
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"^[A-Z0-9a-z._%+-]+@.+.[A-Za-z]{2}[A-Za-z]*$"
                                                                               options:NSRegularExpressionCaseInsensitive error:&error];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:self.txt_Email.text
                                                            options:0
                                                              range:NSMakeRange(0, [self.txt_Email.text length])];
        if(numberOfMatches > 0)
        {
            [self.view endEditing:YES];
            return;
        }
        else
        {
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"EMAIL_ERROR", nil)
                                                                           message:NSLocalizedString(@"EMAIL_ERROR_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) { [self.txt_Email becomeFirstResponder]; }];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
            return;
        }
    }
}

-(void)moveToNextStep {
    [self performSegueWithIdentifier:@"EmailToCodeSegue" sender:self];
}

#pragma mark - Navigation


// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"EmailToCodeSegue"] && !self.isMovingToCode) {
        self.isMovingToCode = YES;
        RecoverCodeViewController *recoverCode = segue.destinationViewController;
        recoverCode.email = self.txt_Email.text;
        self.isMovingToCode = NO;
    }
}


@end
