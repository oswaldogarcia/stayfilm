//
//  Service.h
//  Stayfilm
//
//  Created by Victor Manuel Roldan on 5/16/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "GenericModel.h"
#import "SFHelper.h"
#import "Endpoints.h"

@protocol Service <NSObject>

- (void)callService:(NSDictionary*)parameters endpointName:(enum Endpoints)endpointName withCompletionBlock:(void(^)(NSDictionary *resultArray, NSError *error))completionBlock;

- (void)callServiceImage:(NSDictionary*)parameters endpointName:(enum Endpoints)endpointName withCompletionBlock:(void(^)(NSDictionary *resultArray, NSError *error))completionBlock;

@end
