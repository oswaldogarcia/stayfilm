//
//  PopupMenuAnimation.h
//  Stayfilm-iOS-Full
//
//  Created by Henrique Ormonde on 16/11/17.
//  Copyright © 2017 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface PopupMenuAnimation : NSObject <UIViewControllerAnimatedTransitioning>

@property (nonatomic, assign) NSTimeInterval duration;
@property (nonatomic, assign) BOOL isPresenting;
@property (nonatomic, assign) CGRect originFrame;

@end
