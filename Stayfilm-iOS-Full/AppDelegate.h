//
//  AppDelegate.h
//  Stayfilm-iOS-Full
//
//  Created by Henrique Ormonde on 18/05/17.
//  Copyright © 2017 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <OneSignal/OneSignal.h>
#import "Harpy.h"
@import Photos;

@interface AppDelegate : UIResponder <UIApplicationDelegate, OSSubscriptionObserver, OSPermissionObserver,UNUserNotificationCenterDelegate>
//PHPhotoLibraryChangeObserver
@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, assign) BOOL deviceDeactivated;

-(void)resetMovieMakerStoryboard;
-(void)goToLoginPage;

@property (nonatomic, strong) NSArray *sectionFetchResults;
@property (nonatomic, strong) PHFetchResult *assetsFetchResults;
@property (nonatomic, strong) PHAssetCollection *assetCollection;
@property (nonatomic, strong) NSMutableArray *numberOfPhotoArray;
@end

