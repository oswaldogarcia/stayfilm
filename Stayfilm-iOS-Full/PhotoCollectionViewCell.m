//
//  PhotoCollectionViewCell.m
//  fbmessenger
//
//  Created by Henrique Ormonde on 03/07/15.
//  Copyright (c) 2015 Stayfilm. All rights reserved.
//

#import "PhotoCollectionViewCell.h"
#import "ShadowMotionEffect.h"

@implementation PhotoCollectionViewCell

@synthesize selectedView, unselectedView, photoImageView, spinner, task, source, duration;

- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    if (self) {
        // Initialization code
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"PhotoItem" owner:self options:nil];
        
        if ([arrayOfViews count] < 1) {
            return nil;
        }
        
        if (![[arrayOfViews objectAtIndex:0] isKindOfClass:[UICollectionViewCell class]]) {
            return nil;
        }
        
        self = [arrayOfViews objectAtIndex:0];
        
    }
    return self;
    
}

@end
