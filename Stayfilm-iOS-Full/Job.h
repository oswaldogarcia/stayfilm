//
//  Job.h
//  Stayfilm for Messenger
//
//  Created by Henrique Ormonde on 12/05/15.
//  Copyright (c) 2015 Stayfilm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Utilities.h"
#import "User.h"
#import "AlbumSF.h"
#import "SFConfig.h"

@interface JobItem : NSObject

@property (nonatomic, strong) User* user;
@property (nonatomic, strong) NSString* movieTitle;
@property (nonatomic, assign) StayfilmTypes_SocialNetwork* snTypes;
@property (nonatomic, strong) NSArray* idAlbumsList;
@property (nonatomic, strong) NSString* tags;
@property (nonatomic, assign) NSInteger* idGenre;
@property (nonatomic, assign) NSInteger* idTheme;
@property (nonatomic, assign) NSInteger* idSubtheme;


@end

@interface Job : NSObject

@property (nonatomic, strong) NSString *idJob;
@property (nonatomic, strong) NSString *idUser;
@property (nonatomic, strong) NSString *idMovie;
@property (nonatomic) StayfilmTypes_JobType *jobType;
@property (nonatomic) StayfilmTypes_SocialNetwork *source;
@property BOOL notEnoughContent;
@property BOOL uncertainContent;
@property BOOL socialnetworkPendingJob;
@property BOOL imageAnalyzerPendingJob;
@property (nonatomic, strong) NSMutableDictionary *medias;
@property (nonatomic, strong) NSNumber *created;
@property (nonatomic, strong) NSNumber *updated;
@property (nonatomic, strong) NSNumber *progress;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSDictionary *data;

+(Job *)getJobWithID:(NSString *)p_idJob withUser:(User *)p_user;
+(Job *)postMeliesJobwithMedias:(NSArray *)p_mediasFacebook withAlbum:(AlbumSF *)p_album withGenre:(Genres *)p_genre andIsUpload:(BOOL)isUpload;
+(Job *)postImageAnalyzerWithAlbumID:(NSString*)p_idAlbum;
+(Job *)postSocialNetwork:(NSString*)p_Network;

@end
