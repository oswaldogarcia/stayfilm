//
//  SFHelper.m
//  Stayfilm
//
//  Created by Victor Manuel Roldan on 5/16/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import "SFHelper.h"
#import "SFDefinitions.h"

@implementation SFHelper

#pragma mark - Session methods
+ (void) saveInSession:(NSObject*)object forKey:(NSString*)key {
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    
    if ([object isKindOfClass:[NSDictionary class]]) {
        
        NSDictionary *entityUser = [self removeNullValues:(NSDictionary*)object];
        [user setObject:entityUser forKey: key];
    }
    else {
        [user setObject:object forKey:key];
    }
    
    [user synchronize];
    
}

+ (NSObject*) getSessionObject:(NSString*)key {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    return [userDefaults valueForKey:key];
    
}

+ (void) removeFromSession:(NSString*)key {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults removeObjectForKey:key];
    [userDefaults synchronize];
}

#pragma mark - Dictionary methods
+ (NSDictionary*)removeNullValues:(NSDictionary*) dictionary {
    
    NSMutableDictionary *mutableDict = [dictionary mutableCopy];
    NSMutableArray *keysToDelete = [NSMutableArray array];
    
    [mutableDict enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        if (obj == [NSNull null]) {
            [keysToDelete addObject:key];
        }
    }];
    [mutableDict removeObjectsForKeys:keysToDelete];
    return [mutableDict copy];
}

#pragma mark - Blurred Image Helper

+ (UIImage *)blurredImageWithImage:(UIImage *)sourceImage{
    
    //  Create our blurred image
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *inputImage = [CIImage imageWithCGImage:sourceImage.CGImage];
    
    //  Setting up Gaussian Blur
    CIFilter *filter = [CIFilter filterWithName:@"CIGaussianBlur"];
    [filter setValue:inputImage forKey:kCIInputImageKey];
    [filter setValue:[NSNumber numberWithFloat:5.0f] forKey:@"inputRadius"];
    CIImage *result = [filter valueForKey:kCIOutputImageKey];
    
    /*  CIGaussianBlur has a tendency to shrink the image a little, this ensures it matches
     *  up exactly to the bounds of our original image */
    CGImageRef cgImage = [context createCGImage:result fromRect:[inputImage extent]];
    
    UIImage *retVal = [UIImage imageWithCGImage:cgImage];
    
    if (cgImage) {
        CGImageRelease(cgImage);
    }
    
    return retVal;
}

#pragma mark - Color From Hexadecimal String

+ (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

@end
