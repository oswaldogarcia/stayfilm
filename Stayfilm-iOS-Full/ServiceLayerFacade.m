//
//  ServiceLayerFacade.m
//  Stayfilm
//
//  Created by Victor Manuel Roldan on 5/16/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import "ServiceLayerFacade.h"

@implementation ServiceLayerFacade

+ (ServiceLayerFacade*)getSharedInstance {
    static ServiceLayerFacade *sharedInstance = nil;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        sharedInstance = [[ServiceLayerFacade alloc] init];
    });
    
    return sharedInstance;
}

- (id<Service>)getServiceCall {
    return [ShellWebService getSharedInstance];
}

@end
