//
//  StayWS.h
//  Stayfilm for Messenger
//
//  Created by Henrique Ormonde on 27/04/15.
//  Copyright (c) 2015 Stayfilm. All rights reserved.
//

#ifndef Stayfilm_Full_StayWS_h
#define Stayfilm_Full_StayWS_h

@interface StayWS : NSObject

@property (nonatomic, readwrite, copy) NSString *wsURL;
@property (nonatomic, readwrite, strong) NSURLResponse *responseMessage;
@property (nonatomic, readwrite, strong) NSData *responseBody;
@property (nonatomic, assign) NSInteger maxRequestAttempts;
@property (nonatomic, readwrite, copy) NSString *friendlyMessage;


-(id)init;
-(id)initWithMaxAttemps:(int)maxAttempts;
-(NSDictionary *)requestWebServiceWithURL:(NSString *)wsURLFormat withRequestType:(NSString *)requestType withPathArguments:(NSArray *)pathArguments withArguments:(NSDictionary *)arguments withIdSession:(NSString *)idSession withCache:(BOOL)cache;

@end


#endif
