//
//  ProgressViewController.m
//  fbmessenger
//
//  Created by Henrique Ormonde on 12/06/15.
//  Copyright (c) 2015 Stayfilm. All rights reserved.
//


#import "ProgressViewController.h"
#import "VideoFinishedViewController.h"
#import "StayfilmApp.h"
#import "Utilities.h"
#import "Movie.h"
#import "PhotoFB.h"
#import "VideoFB.h"
#import "ShadowMotionEffect.h"
#import "PopupMenuAnimation.h"
#import <Google/Analytics.h>
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
#import <SDWebImage/UIImageView+WebCache.h>
@import AVKit;
@import AVFoundation;

@interface ProgressViewController ()<UINavigationControllerDelegate,GADBannerViewDelegate>

@property (nonatomic, strong) NSTimer *progressChecker;
@property (nonatomic, assign) int strike; //exception counter
@property (nonatomic, assign) BOOL isSendingMeliesJob;
@property (nonatomic, assign) BOOL isProducingJob;
@property (nonatomic, assign) BOOL isTicked;
@property (nonatomic, assign) BOOL isPopoverOpen;
@property (nonatomic, assign) BOOL isReadyToMove;

@property (nonatomic, strong) Movie* movieFinished;

@property (nonatomic, strong) Uploader* uploader;
@property (nonatomic, weak) StayfilmApp *sfApp;

@property (nonatomic, strong) PopupMenuAnimation *transitionAnimation;

@property(atomic, strong) NSTimer *timer;
@property(atomic, strong) NSMutableArray *backgroundImages;

@end

@implementation ProgressViewController

@synthesize progressBar, progressUploadBar, progressLabel, progressChecker, jobMelies, strike, isSendingMeliesJob, isProducingJob, isTicked, movieFinished, percentageLabel, img_Background, img_clapper, progressView, sfApp;
@synthesize uploader;

 int timeTick;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.progressBar.progress = 0;
    self.navigationController.delegate = self;
//    //rick debug skip send Job
//    StayfilmApp *sfapp = [StayfilmApp sharedStayfilmAppSingleton];
//    if(sfapp.settings[@"createdMovie"] != nil)
//    {
//        self.movieFinished = [Movie getMovieWithID:sfapp.settings[@"createdMovie"]];
//        [self performSegueWithIdentifier: @"ProgressToVideoFinished" sender: self];
//        return;
//    }
    
    self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    self.uploader = [Uploader sharedUploadStayfilmSingletonWithDelegate:self];
    
    self.transitionAnimation = [[PopupMenuAnimation alloc] init];
    
    self.strike = 0;
    self.isSendingMeliesJob = NO;
    self.isProducingJob = NO;
    self.isTicked = NO;
    self.isPopoverOpen = NO;
    self.isReadyToMove = NO;
    
    self.progressChecker = nil;
    
    self.progressView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.progressView.layer.shadowOpacity = 0.15f;
    self.progressView.layer.shadowRadius = 8.0f;
    self.progressView.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    self.progressView.layer.masksToBounds = NO;
    
    
    if(self.uploader.isUploading)// || self.uploader.isPreparingUpload)
    {
        self.progressLabel.text = NSLocalizedString(@"STILL_UPLOADING", nil);
    }
    else //Upload finished or not used
    {
        self.progressUploadBar.hidden = YES;
        self.progressBar.hidden = NO;
        self.progressLabel.text = NSLocalizedString(@"PROGRESS_PRODUCE_START", nil);
        self.progressChecker = [NSTimer scheduledTimerWithTimeInterval:2.5
                                                                target:self
                                                              selector:@selector(sendJobToMelies:)
                                                              userInfo:nil
                                                               repeats:YES];
    }
    
    CGRect f = self.progressBar.bounds;
    f.size.height += 5;
    self.progressBar.bounds = f;
    self.progressUploadBar.bounds = f;
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self.sfApp.filmStrip setHidden:YES];
    
    if (!self.sfApp.isAutomaticMode){
        [self.clapperView setHidden:YES];
        [self.img_clapper setHidden:NO];
        NSString *langcode = [[NSLocale currentLocale] objectForKey:NSLocaleLanguageCode];
        
        
        if([langcode isEqualToString:@"pt"])
        {
            self.img_clapper.image = [UIImage imageNamed:@"clapperBoxPortuguese"];
        }
        else if([langcode isEqualToString:@"es"])
        {
            self.img_clapper.image = [UIImage imageNamed:@"clapperBoxSpanish"];
        }
    }else{
        // automatic film background
        [self.img_clapper setHidden:YES];
        [self.clapperView setHidden:NO];
        
        self.clapperView.layer.borderWidth = 10;
        self.clapperView.layer.borderColor = [UIColor whiteColor].CGColor;
        
        NSString *title = self.sfApp.selectedTitle;
        
        self.automaticTitleLabel.text = [NSString stringWithFormat:NSLocalizedString(@"AF_PROGRESS_TEXT",nil),[title uppercaseString]];
        [self backgroundImageAnimation];
        
        self.sfApp.mediasFromAutoFilmCreated = [[NSMutableArray alloc] init];
        self.sfApp.mediasFromAutoFilmCreated = self.sfApp.selectedMediasToAutoFilm;
        self.sfApp.autoFilmCreated = YES;
        
    }
    
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"hasSubscription"]){
    //Ads
    NSString *adUnitID;
    //adUnitID = @"ca-app-pub-3940256099942544/2934735716"; //TEST
    adUnitID =@"ca-app-pub-7053124358559686/1950845880"; //STAYFILM
    
    //[self.bannerView setAdSize:kGADAdSizeSmartBannerPortrait];
    self.bannerView.delegate = self;
    self.bannerView.adUnitID = adUnitID;
    self.bannerView.rootViewController = self;
    [self.bannerView loadRequest:[DFPRequest request]];
    }
}

- (void)navigationController:(UINavigationController *)navigationController
      willShowViewController:(UIViewController *)viewController
                    animated:(BOOL)animated{
    [navigationController setNavigationBarHidden:YES animated:NO];
}
-(void)backgroundImageAnimation{
 
    PHImageRequestOptions *requestOptions  = [[PHImageRequestOptions alloc] init];
    requestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeFastFormat;
    requestOptions.synchronous = YES;
    
    NSArray *images;
    if(self.sfApp.isEditingMode){
     images = self.sfApp.editedMedias;
    }else{
    images = self.sfApp.selectedMedias;
    }
    
    PHImageManager *manager = [PHImageManager defaultManager];
    self.backgroundImages = [NSMutableArray arrayWithCapacity:[images count]];
    
    UIImageView *imageView = [[UIImageView alloc] init];
    
    for (id photo in images) {
        if([photo isKindOfClass:[PhotoFB class]]){
           PhotoFB * passet = (PhotoFB*)photo;
           [imageView sd_setImageWithURL:[NSURL URLWithString:passet.source] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                [self.backgroundImages addObject:image];
           }];
        }else if([photo isKindOfClass:[Media class]]) {
            Media * media = (Media*)photo;
            [imageView sd_setImageWithURL:[NSURL URLWithString:media.source] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                [self.backgroundImages addObject:image];
            }];
        }else if([photo isKindOfClass:[VideoFB class]]){
            VideoFB * passet =(VideoFB*)photo;
            [imageView sd_setImageWithURL:[NSURL URLWithString:passet.picture] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                [self.backgroundImages addObject:image];
            }];
        }else if([photo isKindOfClass:[PHAsset class]]){
            [manager requestImageForAsset:photo
                                targetSize:self.img_Background.frame.size
                                contentMode:PHImageContentModeDefault
                                options:requestOptions
                                resultHandler:^void(UIImage *image, NSDictionary *info) {
                                [self.backgroundImages addObject:image];
            }];
        }
        
        if (self.backgroundImages.count <= 1){
            [self starTimer];
        }
    }
  
}

- (void)starTimer{
    
    [self.timer invalidate];
    self.timer = nil;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:3.5 target:self selector:@selector(myTicker) userInfo:nil repeats:YES];
}

-(void)myTicker{
    //increment the timer
    if (timeTick >= self.backgroundImages.count-1){
        timeTick = 0;
    }else{
        timeTick ++;
    }
    
    [UIView transitionWithView:self.view
                      duration:0.5f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        [self.img_Background setImage: self.backgroundImages[timeTick]];
                        self.img_Background.contentMode = UIViewContentModeScaleAspectFill;
                        self.img_Background.clipsToBounds = YES;
                    } completion:nil];
    
    
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(viewCameBackFromBackground)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
    
    if(self.sfApp == nil)
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    if(self.uploader == nil)
        self.uploader = [Uploader sharedUploadStayfilmSingletonWithDelegate:self];
    
    [self.sfApp.filmStrip setButtonNextHidden:YES];
    [self.sfApp.filmStrip changeTopStatusBarColorToWhite:YES];
    [self.sfApp.filmStrip connectivityViewUpdate];
    
    // Disable swipe gesture to go back to previous viewcontroller
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
    //Google Analytics
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"wating-step"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if(self.progressChecker != nil) {
        [self.progressChecker invalidate];
        self.progressChecker = nil;
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
    
    [self.navigationController setNavigationBarHidden:NO];
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = [UIColor clearColor];
    }
    
    [self.sfApp.filmStrip connectivityViewUpdate];
    
    [self.timer invalidate];
    self.timer = nil;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
//    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
//    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
//        //statusBar.backgroundColor = [UIColor colorWithRed:0.49 green:0.82 blue:0.80 alpha:1.0];
//        statusBar.backgroundColor = [UIColor colorWithRed:124.0/255.0 green:208/255.0 blue:204./255.0 alpha:1.0];
//    }

//    //Google Analytics
//    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName value:@"Iphone_Nativo_Tela_7_SFMobile_Passo_3_produzindo-filme_tela-trailer"];
//    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}


-(void)viewCameBackFromBackground
{
    self.strike = 0;
    
    [self.progressChecker invalidate];
    self.progressChecker = nil;
    
    if(self.uploader.isUploading)
    {
        self.progressLabel.text = NSLocalizedString(@"STILL_UPLOADING", nil);
    }
    if(self.progressChecker != nil) {
        [self.progressChecker invalidate];
        self.progressChecker = nil;
    }
    if(self.isProducingJob || self.jobMelies.idJob != nil) {
        self.progressChecker = [NSTimer scheduledTimerWithTimeInterval:2.0
                                                                target:self
                                                              selector:@selector(checkProgressTick:)
                                                              userInfo:nil
                                                               repeats:YES];
    } else {
        self.progressChecker = [NSTimer scheduledTimerWithTimeInterval:2.5
                                                                target:self
                                                              selector:@selector(sendJobToMelies:)
                                                              userInfo:nil
                                                               repeats:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


-(void)checkProgressTick:(NSTimer *)timer {
    if (!self.isTicked)
    {
        self.isTicked = YES;
        if(self.sfApp == nil)
        {
            self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
        }
        [self performSelectorInBackground:@selector(sendJobCheckProgress) withObject:nil];
    }
}

-(void)sendJobCheckProgress {
    @try
    {
        NSString *idJob = self.jobMelies.idJob;
        self.jobMelies = [Job getJobWithID:self.jobMelies.idJob withUser:self.sfApp.currentUser];
        self.isTicked = NO;
        
        runOnMainQueueWithoutDeadlocking(^{
            if(self.jobMelies.idJob == nil)
            {
                self.jobMelies.idJob = idJob;  //save idJob from sendtoMelies
            }
            if (self.jobMelies.progress != nil)
            {
                self.progressLabel.text = NSLocalizedString(@"PROGRESS_WORKING", nil);
                if(self.progressBar.progress < [self.jobMelies.progress floatValue] / 100.0f)
                {
                    [self.progressBar setProgress:([self.jobMelies.progress floatValue] / 100.0f) animated:YES];
                    //self.progressBar.progress = [self.jobMelies.progress floatValue] / 100;
                    self.percentageLabel.text = [NSString stringWithFormat:@"%d %%", [self.jobMelies.progress intValue]];
                }
            }
            else if(self.jobMelies.idMovie != nil)
            {
                if(self.progressBar.progress < 0.2)
                    self.progressBar.progress += 0.01;
                self.percentageLabel.text = [NSString stringWithFormat:@"%d %%", (int)self.progressBar.progress * 100];
                
                if ([self.jobMelies.idJob isEqualToString:@"00000000-0000-0000-0000-000000000000"]) //TODO check if this is really idUser or idJob (I changed it to idJob)
                {
                    [self.uploader resetUpload];
                    [self.progressChecker invalidate];
                    self.progressChecker = nil;
                    AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                    [appDelegate resetMovieMakerStoryboard];
                }
            }
            
            if (self.jobMelies.idMovie != nil && [[self.jobMelies.status uppercaseString] isEqualToString:@"SUCCESS"])
            {
                self.progressLabel.text = NSLocalizedString(@"PROGRESS_FINISHED", nil);
                
                self.movieFinished = [Movie getMovieWithID:self.jobMelies.idJob];
                self.movieFinished.permission = MoviePermission_PRIVATE;
                
                if(self.movieFinished != nil)
                {
                    if(self.uploader.uploadedAlbum != nil && self.uploader.uploadedAlbum.idAlbum != nil)
                    {
                        self.sfApp.uploadedAlbum = [self.uploader.uploadedAlbum copy];
                    }
                    [self.progressChecker invalidate];
                    self.progressChecker = nil;
                    
                     self.movieFinished.idTemplate = [NSNumber numberWithInteger:self.sfApp.selectedTemplate.idtemplate] ;
                    //[self.uploader resetUpload];
                    
                    [self.sfApp setEditingMode:NO];
                    if(!self.isPopoverOpen) {
                        [self.sfApp.filmStrip changeTopStatusBarColorToWhite:NO];
                        [self performSegueWithIdentifier: @"ProgressToVideoFinished" sender: self];
                    }
                    else {
                        self.isReadyToMove = YES;
                    }
                    
                }
                
                // Copied from Windows Phone ... this may be needed on Profile page (just to remember - delay on solar causes pendind movies count to be wrong)
                //                //App.Current.AppUser.countPendingMovies += 1;
                
            }
            else if(self.progressBar.progress >= 1.0)
            {
                self.progressLabel.text = NSLocalizedString(@"PROGRESS_FINISHED", nil);
            }
        });
    }
    @catch (NSException* ex)
    {
        NSLog(@"StayLog: EXCEPTION in sendJobCheckProgress with error: %@", ex.description);
        self.isTicked = NO;
    }
}

-(void)sendJobToMelies:(NSTimer*)timer {

    if (!self.isSendingMeliesJob) {
        self.isSendingMeliesJob = YES;
        self.progressBar.progress = 0.0f;
        @try {
            if(self.sfApp == nil)
            {
                self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
            }
            BOOL isUpload = NO;
            int n_photos = 0;
            int n_videos = 0;
            
            if(self.sfApp.isEditingMode) {
                self.sfApp.finalMedias = [self.sfApp.editedMedias mutableCopy];
                //[self.sfApp setEditingMode:NO];  make sure this is called before sending the request, this was commented for analytics purposes below
            }
            
            NSMutableArray *mediaMerge = [[NSMutableArray alloc] init];
           
            if(self.sfApp.finalMedias.count > 0)
            {
                for (id photo in self.sfApp.finalMedias) {
                    if([photo isKindOfClass:[PhotoFB class]]) {
                        NSDictionary *insertMedia = @{ @"idMidia" : [NSString stringWithFormat:@"tmp_fb_%@", ((PhotoFB*)photo).idPhoto],
                                                       @"width" : [NSString stringWithFormat: @"%d", (int)((PhotoFB*)photo).width],
                                                       @"height" : [NSString stringWithFormat: @"%d", (int)((PhotoFB*)photo).height],
                                                       @"uri" : ((PhotoFB*)photo).source };
                        [mediaMerge addObject:insertMedia];
                        n_photos++;
                    }
                    else if([photo isKindOfClass:[Media class]]) {
                        NSDictionary *insertMedia = @{ @"idMidia" : ((Media*)photo).idMidia };
                        [mediaMerge addObject:insertMedia];
                        isUpload = YES;
                        n_photos++;
                    }
                    else if([photo isKindOfClass:[VideoFB class]]) {
                        NSDictionary *insertMedia = @{ @"idMidia" : [NSString stringWithFormat:@"tmp_fb_%@", ((VideoFB*)photo).idVideo],
                                                       @"width" : @"0",
                                                       @"height" : @"0",
                                                       @"uri" : ((VideoFB*)photo).source };
                        [mediaMerge addObject:insertMedia];
                        n_videos++;
                    }
                    else if([photo isKindOfClass:[PHAsset class]]) {
                        if(((PHAsset*)photo).mediaType == PHAssetMediaTypeVideo) {
                            n_videos++;
                        } else {
                            n_photos++;
                        }
                    }
                }
            }
            
//             NSLog(@"FINALMEDIAS:%lu",(unsigned long)self.sfApp.finalMedias.count);
//             NSLog(@"uploadedAlbum:%lu",(unsigned long)self.uploader.uploadedAlbum.idMedias.count);
            
            if(self.uploader.uploadedAlbum != nil && self.uploader.uploadedAlbum.idAlbum != nil)
            {
                NSMutableArray *auxArray = [[NSMutableArray alloc] init];
                for (id m in self.uploader.uploadedAlbum.idMedias) {
                    NSDictionary *insertMedia = nil;
                    if([m isKindOfClass:[Media class]]) {
                        insertMedia = @{ @"idMidia" : ((Media*)m).idMidia };
                    } else if([m isKindOfClass:[NSString class]]) {
                        insertMedia = @{ @"idMidia" : (NSString*)m };
                    }
                    
                    long index = -1;
                    if([m isKindOfClass:[Media class]]) {
                        if(self.uploader.dictionaryMediaIDwithMediaPath[((Media*)m).idMidia] != nil) {
                            NSString * path = self.uploader.dictionaryMediaIDwithMediaPath[((Media*)m).idMidia];
                            if(self.sfApp.isEditingMode) {
                                index = [self.sfApp.editedMedias indexOfObject:self.uploader.listUploadedMedias[path]];
                            } else {
                                index = [self.sfApp.finalMedias indexOfObject:self.uploader.listUploadedMedias[path]];
                            }
                        }
                    } else if([m isKindOfClass:[NSString class]]) {
                        if(self.uploader.dictionaryMediaIDwithMediaPath[m] != nil) {
                            NSString * path = self.uploader.dictionaryMediaIDwithMediaPath[m];
                            if(self.sfApp.isEditingMode) {
                                index = [self.sfApp.editedMedias indexOfObject:self.uploader.listUploadedMedias[path]];
                            } else {
                                index = [self.sfApp.finalMedias indexOfObject:self.uploader.listUploadedMedias[path]];
                            }
                        }
                    }
                    if(index != -1)
                    {
                        [auxArray addObject:@{[NSString stringWithFormat:@"%ld", index] : insertMedia}];
                    }
                    isUpload = YES;
                }
                NSArray *sortedArray = [auxArray sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *m1, NSDictionary *m2){
                    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
                    f.numberStyle = NSNumberFormatterDecimalStyle;
                    NSNumber *i1 = [f numberFromString:m1.allKeys[0]];
                    NSNumber *i2 = [f numberFromString:m2.allKeys[0]];
                    return [i1 compare:i2];
                }];
                
                for (NSDictionary* m in sortedArray) {
                    NSNumber* index = (NSNumber*)m.allKeys[0];
                    [mediaMerge insertObject:m[m.allKeys[0]] atIndex:[index integerValue]];
                }
            }
//            NSLog(@"Staylog: sending job melies with %lu medias merged and total count %d", (unsigned long)mediaMerge.count, n_photos+n_videos);
//
            if(n_videos > 0) {

                if(self.sfApp.isEditingMode) {
                    //Google Analytics Event
                    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"edit-film-add-medias"
                                                                          action:@"count-medias"
                                                                           label:@"photos-video"
                                                                           value:[NSNumber numberWithInt:n_photos+n_videos]] build]];

                    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"photo-video-analysis"
                                                                          action:@"edit"
                                                                           label:@"photos"
                                                                           value:[NSNumber numberWithInt:n_photos]] build]];

                    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"photo-video-analysis"
                                                                          action:@"edit"
                                                                           label:@"videos"
                                                                           value:[NSNumber numberWithInt:n_videos]] build]];

                    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"job-film"
                                                                          action:[@"edited_" stringByAppendingString:((mediaMerge.count<20)?@"mini_photovideo":@"full_photovideo")]
                                                                                                               label:@""
                                                                                                               value:@1] build]];
                    
                    [self.sfApp setEditingMode:NO];
                } else {
                    //Google Analytics Event
                    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"add-medias"
                                                                          action:@"count-medias"
                                                                           label:@"photos-video"
                                                                           value:[NSNumber numberWithInt:n_photos+n_videos]] build]];

                    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"photo-video-analysis"
                                                                          action:@"production"
                                                                           label:@"photos"
                                                                           value:[NSNumber numberWithInt:n_photos]] build]];

                    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"photo-video-analysis"
                                                                          action:@"production"
                                                                           label:@"videos"
                                                                           value:[NSNumber numberWithInt:n_videos]] build]];
                    
                    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"job-film"
                                                                          action:[@"flow_" stringByAppendingString:((mediaMerge.count<20)?@"mini_photovideo":@"full_photovideo")]
                                                                           label:@""
                                                                           value:@1] build]];
                }
            } else {
                if(self.sfApp.isEditingMode) {
                    //Google Analytics Event
                    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"edit-film-add-medias"
                                                                          action:@"count-medias"
                                                                           label:@"photos"
                                                                           value:[NSNumber numberWithFloat:n_photos]] build]];
                    
                    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"job-film"
                                                                          action:[@"flow_" stringByAppendingString:((mediaMerge.count<20)?@"mini_photo":@"full_photo")]
                                                                           label:@""
                                                                           value:@1] build]];
                    
                    [self.sfApp setEditingMode:NO];
                } else {
                    //Google Analytics Event
                    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"waiting-step"
                                                                          action:@"count-medias"
                                                                           label:@"photos"
                                                                           value:[NSNumber numberWithFloat:n_photos]] build]];
                    
                    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"job-film"
                                                                          action:[@"flow_" stringByAppendingString:((mediaMerge.count<20)?@"mini_photo":@"full_photo")]
                                                                           label:@""
                                                                           value:@1] build]];
                }
            }
            
            if(n_photos+n_videos < [self.sfApp.sfConfig.min_photos intValue]) {
                self.progressLabel.text = NSLocalizedString(@"SOMETHING_WRONG", nil);
                
                [self.progressChecker invalidate];
                self.progressChecker = nil;
                
                [self.uploader resetUpload];
              
                [self.sfApp cleanRemakeState];
                
                //                //Google Analytics
                //                id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                //                [tracker set:kGAIScreenName value:@"Iphone_Nativo_Tela_11_SFMobile_Erro-do-usuario_1_erro-producao"];
                //                [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
                
                UIAlertController* alertc = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"SOMETHING_WRONG", nil)
                                                                                message:NSLocalizedString(@"PROBLEM_MELIES", nil)
                                                                         preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                 handler:^(UIAlertAction * action) {
                                                                     [alertc dismissViewControllerAnimated:YES completion:nil];
                                                                     AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                                                                     [[NSNotificationCenter defaultCenter] removeObserver:self];
                                                                     [appDelegate resetMovieMakerStoryboard];
                                                                     [self.navigationController popViewControllerAnimated:YES];
                                                                 }];
                [alertc addAction:okAction];
                [self presentViewController:alertc animated:YES completion:nil];
                return;
            }
            
            NSLog(@"mediaMerge.count: %lu", (unsigned long)mediaMerge.count);
            NSLog(@"uploadedAlbum.idAlbu :%@", self.uploader.uploadedAlbum.idAlbum);
            NSLog(@"isUpload :%d",isUpload);
           
            if(mediaMerge.count < [self.sfApp.sfConfig.min_photos intValue] || (self.uploader.uploadedAlbum.idAlbum == nil && mediaMerge.count == 0)) {
                
                self.progressLabel.text = NSLocalizedString(@"SOMETHING_WRONG", nil);
                
                [self.progressChecker invalidate];
                self.progressChecker = nil;
                
                [self.uploader resetUpload];
                
                [self.sfApp cleanRemakeState];
                
                //                //Google Analytics
                //                id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                //                [tracker set:kGAIScreenName value:@"Iphone_Nativo_Tela_11_SFMobile_Erro-do-usuario_1_erro-producao"];
                //                [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
                
                UIAlertController* alertc = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"SOMETHING_WRONG", nil)
                                                                                message:NSLocalizedString(@"PROBLEM_MELIES", nil)
                                                                         preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                 handler:^(UIAlertAction * action) {
                                                                     [alertc dismissViewControllerAnimated:YES completion:nil];
                                                                     AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                                                                     [[NSNotificationCenter defaultCenter] removeObserver:self];
                                                                     [appDelegate resetMovieMakerStoryboard];
                                                                     [self.navigationController popViewControllerAnimated:YES];
                                                                 }];
                [alertc addAction:okAction];
                [self presentViewController:alertc animated:YES completion:nil];
                return;
            }else{
            
            self.jobMelies = [Job postMeliesJobwithMedias:mediaMerge withAlbum:self.uploader.uploadedAlbum withGenre:self.sfApp.selectedGenre andIsUpload:isUpload];
            }
            // check if the job already started
            if (self.jobMelies.idJob != nil)
            {
                //Debug.WriteLine("SUCCESS! JOB SENT TO MELIES!!");
                
                [self.progressChecker invalidate];
                self.progressChecker = nil;
                self.progressChecker = [NSTimer scheduledTimerWithTimeInterval:2.0
                                                                        target:self
                                                                      selector:@selector(checkProgressTick:)
                                                                      userInfo:nil
                                                                       repeats:YES];
                self.isProducingJob = YES;
            }
            else if(self.jobMelies.uncertainContent)
            {
                [self.progressChecker invalidate];
                self.progressChecker = nil;
                
                [self.uploader resetUpload];
                
                [self.sfApp cleanRemakeState];
//                //Google Analytics
//                id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//                [tracker set:kGAIScreenName value:@"Iphone_Nativo_Tela_11_SFMobile_Erro-do-usuario_1_erro-producao"];
//                [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
                
                UIAlertController* alertc = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"SOMETHING_WRONG", nil)
                                                                                message:NSLocalizedString(@"PROBLEM_UNCERTAIN_CONTENT", nil)
                                                                         preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                 handler:^(UIAlertAction * action) {
                                                                     [alertc dismissViewControllerAnimated:YES completion:nil];
                                                                     AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                                                                     [[NSNotificationCenter defaultCenter] removeObserver:self];
                                                                     [appDelegate resetMovieMakerStoryboard];
                                                                     [self.navigationController popViewControllerAnimated:YES];
                                                                 }];
                [alertc addAction:okAction];
                [self presentViewController:alertc animated:YES completion:nil];
            }
            else if (self.jobMelies.imageAnalyzerPendingJob)
            {
                // Try again because we have an imageanalyzer job pending.
                if (self.progressBar.progress < 0.2)
                    self.progressBar.progress += 0.01;
                self.percentageLabel.text = [NSString stringWithFormat:@"%d %%", (int)self.progressBar.progress * 100];
            }
            else if (self.jobMelies.notEnoughContent && self.jobMelies.socialnetworkPendingJob)
            {
                // Try again because we do not have enough content yet but there is a socialnetwork job pending.

            }
            else if (self.jobMelies.notEnoughContent && !self.jobMelies.socialnetworkPendingJob)
            {
                // At this point, there is no social network job pending but we still do not have enough content. Show MissinContentException to user.
                
                self.progressLabel.text = NSLocalizedString(@"SOMETHING_WRONG", nil);
                
                [self.progressChecker invalidate];
                self.progressChecker = nil;
                
                [self.uploader resetUpload];
                
                [self.sfApp cleanRemakeState];
//                //Google Analytics
//                id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//                [tracker set:kGAIScreenName value:@"Iphone_Nativo_Tela_11_SFMobile_Erro-do-usuario_1_erro-producao"];
//                [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
                
                UIAlertController* alertc = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"SOMETHING_WRONG", nil)
                                                                                message:NSLocalizedString(@"PROBLEM_NOT_ENOUGH_CONTENT", nil)
                                                                         preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                 handler:^(UIAlertAction * action) {
                                                                     [alertc dismissViewControllerAnimated:YES completion:nil];
                                                                     AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                                                                     [[NSNotificationCenter defaultCenter] removeObserver:self];
                                                                     [appDelegate resetMovieMakerStoryboard];
                                                                     [self.navigationController popViewControllerAnimated:YES];
                                                                 }];
                [alertc addAction:okAction];
                [self presentViewController:alertc animated:YES completion:nil];
            }
            else
            {
                //Debug.WriteLine("PROBLEM WITH MELIES! Show error message to user.");
                // - No idJob created;
                // - Not enough content to false;
                // - No socialnetwork job running;
                // PROBLEM WITH MELIES! Show error message to user.
                
                self.progressLabel.text = NSLocalizedString(@"SOMETHING_WRONG", nil);
                
                [self.progressChecker invalidate];
                self.progressChecker = nil;
                
                [self.uploader resetUpload];
                [self.sfApp cleanRemakeState];
//                //Google Analytics
//                id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//                [tracker set:kGAIScreenName value:@"Iphone_Nativo_Tela_11_SFMobile_Erro-do-usuario_1_erro-producao"];
//                [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
                
                UIAlertController* alertc = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"SOMETHING_WRONG", nil)
                                        message:NSLocalizedString(@"PROBLEM_MELIES", nil)
                                 preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                     handler:^(UIAlertAction * action) {
                         [alertc dismissViewControllerAnimated:YES completion:nil];
                         AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                         [[NSNotificationCenter defaultCenter] removeObserver:self];
                         [appDelegate resetMovieMakerStoryboard];
                         [self.navigationController popViewControllerAnimated:YES];
                     }];
                [alertc addAction:okAction];
                [self presentViewController:alertc animated:YES completion:nil];
            }
            
            self.isSendingMeliesJob = NO;
        }
        @catch (NSException *exception) {
            NSLog(@"StayLog: EXCEPTION on senfJobToMelies with error: %@", exception.description);
            if (strike > 3) {
                
                self.progressLabel.text = NSLocalizedString(@"SOMETHING_WRONG", nil);
                
                [self.progressChecker invalidate];
                self.progressChecker = nil;
                
                [self.uploader resetUpload];
                [self.sfApp cleanRemakeState];
//                //Google Analytics
//                id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//                [tracker set:kGAIScreenName value:@"Iphone_Nativo_Tela_11_SFMobile_Erro-do-usuario_1_erro-producao"];
//                [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
                
                UIAlertController* alertc = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"SOMETHING_WRONG", nil)
                                        message:NSLocalizedString(@"PROBLEM_MELIES", nil)
                                 preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                     handler:^(UIAlertAction * action) {
                         [alertc dismissViewControllerAnimated:YES completion:nil];
                         AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                         [[NSNotificationCenter defaultCenter] removeObserver:self];
                         [appDelegate resetMovieMakerStoryboard];
                         [self.navigationController popViewControllerAnimated:YES];
                     }];
                [alertc addAction:okAction];
                [self presentViewController:alertc animated:YES completion:nil];
            }
            else
            {
                ++strike;
                self.isSendingMeliesJob = NO;
            }
        }
    }
    
    
}


#pragma mark - Back button Delegate

-(void)chooseOption:(NSString *)option {
    if([option isEqualToString:@"cancelProduction"]) {
        self.isPopoverOpen = NO;
        
        if(self.progressChecker != nil)
            [self.progressChecker invalidate];
        self.progressChecker = nil;
        
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"film"
                                                           action:@"canceled"
                                                            label:@""
                                                            value:@1] build]];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self]; //remove come from background state observer
        

        if([[self.navigationController.viewControllers lastObject] isKindOfClass:[VideoFinishedViewController class]])
        {
            [[self.navigationController.viewControllers lastObject].navigationController popViewControllerAnimated:YES];
        }
        
        [self.sfApp setEditingMode:NO];
        //[self.sfApp cleanRemakeState];
        [self.sfApp.filmStrip setButtonNextHidden:NO];
        [self.sfApp.filmStrip changeTopStatusBarColorToWhite:NO];
        
//        UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
//        if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
//            statusBar.backgroundColor = [UIColor clearColor];
//        }

        runOnMainQueueWithoutDeadlocking(^{
            AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            [appdelegate resetMovieMakerStoryboard];
        });
    }
    else if([option isEqualToString:@"stayHere"]) {
        self.isPopoverOpen = NO;
//        BOOL didMove = NO;
//
//        for (UIViewController *view in self.navigationController.viewControllers) {
//            if([view isKindOfClass:[VideoFinishedViewController class]])
//            {
//                didMove = YES;
//                break;
//            }
//        }
        
//        if(!didMove && self.movieFinished != nil)
        if(self.isReadyToMove && self.movieFinished != nil)
        {
            [self.progressChecker invalidate];
            self.progressChecker = nil;
            
            if(self.uploader.uploadedAlbum != nil && self.uploader.uploadedAlbum.idAlbum != nil)
            {
                self.sfApp.uploadedAlbum = [self.uploader.uploadedAlbum copy];
            }
            
            //[self.uploader resetUpload];
            [self.sfApp.filmStrip changeTopStatusBarColorToWhite:NO];
            
            [self performSegueWithIdentifier: @"ProgressToVideoFinished" sender: self];
            
        }
    }
}


#pragma mark - Stayfilm Uploader Delegates

-(void)addedMedias {
    // Not important on this page.
}

-(void)uploadProgressChanged:(NSNumber *)progress
{
    self.progressLabel.text = [NSString stringWithFormat:NSLocalizedString(@"UPLOAD_PROGRESS", nil), self.uploader.listUploadedMedias.count, self.uploader.listUploadMedias.count];
    [self.progressUploadBar setProgress:[progress floatValue] animated:YES];
}

- (void)converterDidFailMedia:(NSString *)type
{
    NSString *message = nil;
    if(self.sfApp == nil)
    {
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    }
    if ( (self.sfApp.isEditingMode)?
        (self.sfApp.editedMedias.count < [self.sfApp.sfConfig.min_photos intValue]) :
        (self.sfApp.finalMedias.count < [self.sfApp.sfConfig.min_photos intValue]) ) {
        message = [NSString stringWithFormat:NSLocalizedString(@"UPLOADER_CONVERT_FAILED_UPLOAD_AGAIN_MESSAGE", nil), (self.sfApp.isEditingMode)? ([self.sfApp.sfConfig.min_photos intValue] -  self.sfApp.editedMedias.count) : ([self.sfApp.sfConfig.min_photos intValue] -  self.sfApp.finalMedias.count)];
        
//        //Google Analytics
//        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//        [tracker set:kGAIScreenName value:@"Iphone_Nativo_Tela_15_SFMobile_Erro-do-usuario_5_erro-upload-convert"];
//        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
        
        UIAlertController* alertc = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_ERROR_TITLE", nil)
                                                                        message:message
                                                                 preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action) {
                                                             [alertc dismissViewControllerAnimated:YES completion:nil];
                                                             [[NSNotificationCenter defaultCenter] removeObserver:self];
                                                             if(self.progressChecker != nil)
                                                                 [self.progressChecker invalidate];
                                                             self.progressChecker = nil;
                                                             [self.navigationController popViewControllerAnimated:YES];
                                                         }];
        
        [alertc addAction:okAction];
        [self presentViewController:alertc animated:YES completion:nil];
        return;
    }
    else if ([type isEqualToString:@"image"]) {
        message = NSLocalizedString(@"UPLOADER_CONVERT_FAILED_IMAGE_MESSAGE", nil);
    }
    else
    {
        message = NSLocalizedString(@"UPLOADER_CONVERT_FAILED_VIDEO_MESSAGE", nil);
    }
    
//    //Google Analytics
//    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName value:@"Iphone_Nativo_Tela_15_SFMobile_Erro-do-usuario_5_erro-upload-convert"];
//    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_CONVERT_FAILED_TITLE", nil)
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)uploadDidFailMedia:(NSString *)type
{
    NSString *message = nil;
    if(self.sfApp == nil)
    {
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    }
    if ( (self.sfApp.isEditingMode)?
        (self.sfApp.editedMedias.count < [self.sfApp.sfConfig.min_photos intValue]) :
        (self.sfApp.finalMedias.count < [self.sfApp.sfConfig.min_photos intValue]) ) {
        message = [NSString stringWithFormat:NSLocalizedString(@"UPLOADER_UPLOAD_FAILED_UPLOAD_AGAIN_MESSAGE", nil), (self.sfApp.isEditingMode)? ([self.sfApp.sfConfig.min_photos intValue] -  self.sfApp.editedMedias.count) : ([self.sfApp.sfConfig.min_photos intValue] -  self.sfApp.finalMedias.count)];
        
//        //Google Analytics
//        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//        [tracker set:kGAIScreenName value:@"Iphone_Nativo_Tela_14_SFMobile_Erro-do-usuario_4_erro-upload"];
//        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
        
        UIAlertController* alertc = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_ERROR_TITLE", nil)
                                                                        message:message
                                                                 preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action) {
                                                             [alertc dismissViewControllerAnimated:YES completion:nil];
                                                             [[NSNotificationCenter defaultCenter] removeObserver:self];
                                                             if(self.progressChecker != nil)
                                                                 [self.progressChecker invalidate];
                                                             self.progressChecker = nil;
                                                             [self.navigationController popViewControllerAnimated:YES];
                                                         }];
        
        [alertc addAction:okAction];
        [self presentViewController:alertc animated:YES completion:nil];
        return;
    }
    else if ([type isEqualToString:@"image"]) {
        message = NSLocalizedString(@"UPLOADER_UPLOAD_FAILED_IMAGE_MESSAGE", nil);
    }
    else
    {
        message = NSLocalizedString(@"UPLOADER_UPLOAD_FAILED_VIDEO_MESSAGE", nil);
    }
    
//    //Google Analytics
//    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName value:@"Iphone_Nativo_Tela_14_SFMobile_Erro-do-usuario_4_erro-upload"];
//    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_UPLOAD_FAILED_TITLE", nil)
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)uploadDidFail:(Uploader *)uploader
{
    UIAlertController* alertc = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_ERROR_TITLE", nil)
                                                                    message:NSLocalizedString(@"UPLOADER_ERROR_MESSAGE", nil)
                                                             preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {
                                                         [alertc dismissViewControllerAnimated:YES completion:nil];
                                                         if(self.progressChecker != nil)
                                                             [self.progressChecker invalidate];
                                                         self.progressChecker = nil;
                                                         AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                                                         [[NSNotificationCenter defaultCenter] removeObserver:self];
                                                         [appDelegate resetMovieMakerStoryboard];
                                                         [self.navigationController popViewControllerAnimated:YES];
                                                     }];
    
    [alertc addAction:okAction];
    [self presentViewController:alertc animated:YES completion:nil];
}

-(void)uploadDidStartSendingConfirmationMedias:(Uploader *)uploader
{
    self.progressLabel.text = NSLocalizedString(@"UPLOAD_FINISHED_START_CONFIRMATION", nil);
    self.percentageLabel.text = @"0 %";
}

-(void)uploadDidStart
{
    //Not important in this page.
}

-(void)uploadDidFinish:(Uploader *)uploader
{
    @try {
        self.progressLabel.text = NSLocalizedString(@"UPLOAD_FINISHED_START_PROGRESS", nil);
        self.percentageLabel.text = @"0 %";
        self.progressBar.hidden = NO;
        self.progressUploadBar.hidden = YES;
        if(self.progressChecker != nil)
        {
            [self.progressChecker invalidate];
            self.progressChecker = nil;
        }
        if(self.uploader.listUploadedMedias != nil && self.uploader.listUploadedMedias.count > 0) {
            for (PHAsset * asset in self.uploader.listUploadedMedias.allValues) {
                if(self.sfApp.isEditingMode) {
                    if(![self.sfApp.editedMedias containsObject:asset]) {
                        [self.sfApp.editedMedias addObject:asset];
                    }
                } else {
                    if(![self.sfApp.finalMedias containsObject:asset]) {
                        [self.sfApp.finalMedias addObject:asset];
                    }
                }
            }
        }
        
        self.progressChecker = [NSTimer scheduledTimerWithTimeInterval:2.5
                                                                target:self
                                                              selector:@selector(sendJobToMelies:)
                                                              userInfo:nil
                                                               repeats:YES];
        self.progressBar.progress = 0;
        self.progressUploadBar.hidden = YES;
    }
    @catch (NSException *exception) {
        NSLog(@"StayLog: EXCEPTION on uploadDidFinish... progress page with error: %@", exception.description);
    }
}

-(void)uploadFinishedMedia:(PHAsset *)asset {
    // not important on this view
}


#pragma mark - Animation Delegates

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source {
    return self.transitionAnimation;
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    self.transitionAnimation.isPresenting = NO;
    return self.transitionAnimation;
}


#pragma mark - Navigation

BOOL isOpeningPopover = NO;
- (IBAction)but_back_Clicked:(id)sender {
    //[self backButtonPressed];
    if(isOpeningPopover) {
        return;
    }
    isOpeningPopover = YES;
    
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"film"
                                                          action:@"back"
                                                           label:@""
                                                           value:@1] build]];

    Popover_CancelProductionViewController * view = [[Popover_CancelProductionViewController alloc] init];
    view.delegate = self;
    view.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    view.transitioningDelegate = self;
    view.view.frame = self.view.frame;
    self.transitionAnimation.duration = 0.3;
    self.transitionAnimation.isPresenting = YES;
    self.transitionAnimation.originFrame = self.view.frame;
    
    self.isPopoverOpen = YES;
    [self presentViewController:view animated:YES completion:nil];
    
    isOpeningPopover = NO;
}

//- (void)backButtonPressed
//{
//    @try {
//
//            UIAlertController* alertc = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ARE_YOU_SURE", nil)
//                                    message:NSLocalizedString(@"CONFIRM_UNWIND_FROM_PROGRESS", nil)
//                             preferredStyle:UIAlertControllerStyleAlert];
//
//            UIAlertAction* okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
//                 handler:^(UIAlertAction * action) {
//                     [alertc dismissViewControllerAnimated:YES completion:nil];
//                     if(self.progressChecker != nil)
//                         [self.progressChecker invalidate];
//                     self.progressChecker = nil;
//
////                                                                 //Google Analytics Event
////                                                                 id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
////                                                                 [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Botao"
////                                                                                                                       action:@"Filme-produzido_descartar"
////                                                                                                                        label:@"Iphone_Nativo_evento_104_botao_Filme-produzido_descartar"
////                                                                                                                        value:@1] build]];
//
//                     [[NSNotificationCenter defaultCenter] removeObserver:self]; //remove come from background state observer
//
//                     for (UIViewController *view in self.navigationController.viewControllers) {
//                         if([view isKindOfClass:[VideoFinishedViewController class]])
//                         {
//                             [view.navigationController popViewControllerAnimated:YES];
//                             break;
//                         }
//                     }
//
//                     [self.sfApp cleanRemakeState];
//                     [self.sfApp setEditingMode:NO];
//
//                     runOnMainQueueWithoutDeadlocking(^{
//                         AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//                         [appdelegate resetMovieMakerStoryboard];
//                     });
//                     //[self.navigationController popViewControllerAnimated:YES];
//                 }];
//            UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"STAY", nil) style:UIAlertActionStyleCancel
//                     handler:^(UIAlertAction * action) {
//                         [alertc dismissViewControllerAnimated:YES completion:nil];
//
//                         BOOL didMove = NO;
//                         UIViewController *view = [self.navigationController.viewControllers lastObject];
//                         if([view isKindOfClass:[VideoFinishedViewController class]])
//                         {
//                             didMove = YES;
//                         }
//
//                         if(!didMove && self.movieFinished != nil)
//                         {
//                             [self.progressChecker invalidate];
//                             self.progressChecker = nil;
//
//                             if(self.uploader.uploadedAlbum != nil && self.uploader.uploadedAlbum.idAlbum != nil)
//                             {
//                                 self.sfApp.uploadedAlbum = [self.uploader.uploadedAlbum copy];
//                             }
//
//                             [self.sfApp setEditingMode:NO];
//                             //[self.uploader resetUpload];
//                             if(!self.isPopoverOpen)
//                                 [self performSegueWithIdentifier: @"ProgressToVideoFinished" sender: self];
//                         }
//                     }];
//
//            [alertc addAction:okAction];
//            [alertc addAction:cancelAction];
//            [self presentViewController:alertc animated:YES completion:nil];
//    }
//    @catch (NSException *exception) {
//        NSLog(@"StyaLog: EXCEPTION on backButtonPressed with error: %@", exception.description);
//    }
//}


// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.

    if ([segue.identifier isEqualToString:@"ProgressToVideoFinished"]) {
        VideoFinishedViewController *videoFinishedViewController = segue.destinationViewController;
        videoFinishedViewController.movie = self.movieFinished;
        [self.progressChecker invalidate];
        self.progressChecker = nil;
        //[videoFinishedViewController downloadMovieToDevice];
        [[NSNotificationCenter defaultCenter] removeObserver:self]; //remove come from background state observer
        
        [self.sfApp.filmStrip changeTopStatusBarColorToWhite:NO];
//        //rick debug skip send Job
//        StayfilmApp *sfapp = [StayfilmApp sharedStayfilmAppSingleton];
//        if(sfapp.settings[@"createdMovie"] == nil)
//        {
//            sfapp.settings[@"createdMovie"] = self.movieFinished.idMovie;
//            [sfapp saveSettings];
//        }
    }
}

#pragma mark - Ads

/// Tells the delegate an ad request loaded an ad.
- (void)adViewDidReceiveAd:(DFPBannerView *)adView {
    NSLog(@"adViewDidReceiveAd");
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"advertinsing"
                                                          action:@"played"
                                                           label:@""
                                                           value:@1] build]];
}

/// Tells the delegate an ad request failed.
- (void)adView:(DFPBannerView *)adView
didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"adView:didFailToReceiveAdWithError: %@", [error localizedDescription]);
}
/*
/// Tells the delegate that a full-screen view will be presented in response
/// to the user clicking on an ad.
- (void)adViewWillPresentScreen:(DFPBannerView *)adView {
    NSLog(@"adViewWillPresentScreen");
}

/// Tells the delegate that the full-screen view will be dismissed.
- (void)adViewWillDismissScreen:(DFPBannerView *)adView {
    NSLog(@"adViewWillDismissScreen");
}

/// Tells the delegate that the full-screen view has been dismissed.
- (void)adViewDidDismissScreen:(DFPBannerView *)adView {
    NSLog(@"adViewDidDismissScreen");
}
*/
/// Tells the delegate that a user click will open another app (such as
/// the App Store), backgrounding the current app.
- (void)adViewWillLeaveApplication:(DFPBannerView *)adView {
    NSLog(@"adViewWillLeaveApplication");
}

@end
