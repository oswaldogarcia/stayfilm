//
//  RemoverViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Henrique Ormonde on 17/11/17.
//  Copyright © 2017 Stayfilm. All rights reserved.
//

#import "RemoverViewController.h"
#import "StayfilmApp.h"
#import <Google/Analytics.h>
#import "Media.h"
#import "PhotoCollectionViewCell.h"
#import "PlayerViewController.h"
#import "Step1ConfirmationViewController.h"
#import "MovieMakerStep1ViewController.h"

@interface RemoverViewController ()

@property (nonatomic, assign) BOOL isAllSelected;
@property (nonatomic, assign) BOOL isLongPressing;
@property (nonatomic, strong) UIViewController *modalPreview;
@property (nonatomic, weak) StayfilmApp *sfApp;
@property (nonatomic, strong) Uploader* uploaderStayfilm;

@property (nonatomic, assign) CGRect showingPoint;
@property (nonatomic, assign) CGRect hiddenPoint;
@property (nonatomic, assign) BOOL isRemoveButtonShowing;

@end

@implementation RemoverViewController

@synthesize removeMedias, initialSelectedMedias, photosCollectionView, thumbConnection, modalPreview, imageManager, uploaderStayfilm, sfApp, navigationController, view_Remove;

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
     self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    self.uploaderStayfilm = [Uploader sharedUploadStayfilmSingletonWithDelegate:self];
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    self.thumbConnection = [NSURLSession sessionWithConfiguration:config];
    
    if(self.imageManager == nil) {
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            switch (status) {
                case PHAuthorizationStatusAuthorized:
                {
                    self.imageManager = [[PHCachingImageManager alloc] init];
                }
                    break;
                case PHAuthorizationStatusDenied:
                case PHAuthorizationStatusRestricted:
                case PHAuthorizationStatusNotDetermined:
                {
                    self.imageManager = nil;
                }
                    break;
                default:
                    break;
            }
        }];
    }
//    if(self.imageManager == nil)
//        self.imageManager = [[PHCachingImageManager alloc] init];
    
    self.removeMedias = [[NSMutableArray alloc] init];
    
    [self.photosCollectionView registerClass:[PhotoCollectionViewCell class] forCellWithReuseIdentifier:cellIdentifierPhoto];
    
    self.isLongPressing = NO;
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = 1; //seconds
    lpgr.delegate = self;
    [self.photosCollectionView addGestureRecognizer:lpgr];
    
    self.isAllSelected = NO;
//    [self.view bringSubviewToFront:self.txt_SelectAll];
//    self.txt_SelectAll.userInteractionEnabled = YES;
//    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(SelectAllTapped:)];
//    tapGestureRecognizer.numberOfTapsRequired = 1;
//    [self.txt_SelectAll addGestureRecognizer:tapGestureRecognizer]; // select all gesture
    
//    //Google Analytics
//    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName value:@"Iphone_Nativo_Tela_4_SFMobile_Passo_2_escolher-fotos_facebook-album"];
//    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationItem.backBarButtonItem setTitle:@""];
    
    UIApplication *app = [UIApplication sharedApplication];
    CGFloat statusBarHeight = app.statusBarFrame.size.height;
    UIView *statusBarView =  [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, statusBarHeight)];
    statusBarView.backgroundColor  =  [UIColor colorWithRed:51.0/255 green:87.0/255 blue:113.0/255 alpha:1.0];
    [self.view addSubview:statusBarView];
    
    [self setNeedsStatusBarAppearanceUpdate];
    
    self.isRemoveButtonShowing = NO;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.showingPoint = [self.view_Remove frame];
    self.hiddenPoint = CGRectMake(self.view_Remove.frame.origin.x, self.view_Remove.frame.origin.y + 100, self.view_Remove.frame.size.width, self.view_Remove.frame.size.height);
    
    [self.view_Remove setFrame:self.hiddenPoint];
    
    // Disable swipe gesture to go back to previous viewcontroller
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
    if(self.sfApp == nil)
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    self.uploaderStayfilm = [Uploader sharedUploadStayfilmSingletonWithDelegate:self];
    
    if(self.sfApp.isEditingMode) {
        //Google Analytics
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:@"film-editing_content_remove"];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    } else {
        //Google Analytics
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:@"moviemaker_content_remove"];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    }
}
- (void)viewWillAppear:(BOOL)animated{
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = [UIColor colorWithRed:0.20 green:0.34 blue:0.44 alpha:1.0];
    }
    
    self.view_Remove.alpha = 0.0;
    
    if(self.sfApp == nil)
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    [self.sfApp.filmStrip setViewInsideFilmStrip];
    
    if(self.sfApp.isEditingMode) {
        [self.sfApp.filmStrip changeTopStatusBarColorToWhite:YES];
    } else {
        [self.sfApp.filmStrip changeTopStatusBarColorToWhite:NO];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backAction:(id)sender {
    [self backButtonPressed];
}


- (void)backButtonPressed
{
    @try {
        if(![StayfilmApp isArray:self.removeMedias equalToArray:self.initialSelectedMedias] && self.removeMedias.count != 0)
        {
            UIAlertController* alertc = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ARE_YOU_SURE", nil)
                                                                            message:NSLocalizedString(@"CONFIRM_UNWIND_FROM_REMOVE", nil)
                                                                     preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {
                                                                 [alertc dismissViewControllerAnimated:YES completion:nil];
//                                                                 [self.navigationController popViewControllerAnimated:YES];
                                                                 if(self.sfApp.isEditingMode) {
                                                                     //Google Analytics Event
                                                                     id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                                                                     [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"edit-film"
                                                                                                                           action:@"remove"
                                                                                                                            label:@"cancel"
                                                                                                                            value:@1] build]];
                                                                 } else {
                                                                     //Google Analytics Event
                                                                     id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                                                                     [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"film-production"
                                                                                                                           action:@"remove"
                                                                                                                            label:@"cancel"
                                                                                                                            value:@1] build]];
                                                                 }
                                                                 [self dismissViewControllerAnimated:YES completion:nil];
                                                             }];
            UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"STAY", nil) style:UIAlertActionStyleCancel
                                                                 handler:^(UIAlertAction * action) {
                                                                     [alertc dismissViewControllerAnimated:YES completion:nil];
                                                                 }];
            
            [alertc addAction:okAction];
            [alertc addAction:cancelAction];
            [self presentViewController:alertc animated:YES completion:nil];
        }
        else
        {
            [self.removeMedias removeAllObjects];
            self.removeMedias = nil;
            [self.photosCollectionView removeFromSuperview];
            self.photosCollectionView = nil;
            self.thumbConnection = nil;
            
            for (UIViewController *view in self.navigationController.viewControllers) {
                if([view isKindOfClass:[Step1ConfirmationViewController class]])
                {
                    [view performSelectorOnMainThread:@selector(clearAlbumViewController) withObject:nil waitUntilDone:NO];
                    break;
                }
            }
            
//            [self.navigationController popViewControllerAnimated:YES];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"StayLog: EXCEPTION on backButtonPressed with error: %@", exception.description);
    }
}

-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if(!self.isLongPressing)
    {
        self.isLongPressing = YES;
        CGPoint p = [gestureRecognizer locationInView:self.photosCollectionView];
        
        NSIndexPath *indexPath = [self.photosCollectionView indexPathForItemAtPoint:p];
        if (indexPath == nil) {
            //            NSLog(@"long press on table view but not on a row");
        } else if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
            //            NSLog(@"long press on table view at row %d", indexPath.item);
            
            self.modalPreview = nil;
            UIImageView *imageView = nil;
            
            if([[self.initialSelectedMedias objectAtIndex:indexPath.item] isKindOfClass:[Media class]]) {
                self.modalPreview = [[UIViewController alloc] init];
                
                self.modalPreview.view.backgroundColor = [UIColor whiteColor];
                self.modalPreview.view.userInteractionEnabled = YES;
                
                imageView = [[UIImageView alloc] initWithFrame:self.modalPreview.view.frame];
                imageView.contentMode = UIViewContentModeScaleAspectFit;
                
                Media * asset = [self.initialSelectedMedias objectAtIndex:indexPath.item];
                
                if([asset.source containsString:@".mp4"] ||
                   [asset.source containsString:@".mpg"] ||
                   [asset.source containsString:@".avi"] ||
                   [asset.source containsString:@".mov"] )
                {
                    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"Player" owner:self options:nil];
                    PlayerViewController *moviePlayerViewController = [topLevelObjects objectAtIndex:0];
                    moviePlayerViewController.currentMovie = nil;
                    moviePlayerViewController.videoUrl = asset.source;
                    
                    [self presentViewController:moviePlayerViewController animated:YES completion:nil];
                    //[self performSegueWithIdentifier: @"PlayRemMovieSegue" sender:asset.source];
                    self.isLongPressing = NO;
                    return;
                }
                else {
                    if(self.sfApp.imagesCache != nil && [self.sfApp.imagesCache objectForKey:asset.source] != nil)
                    {
                        imageView.image = [self.sfApp.imagesCache objectForKey:asset.source];
                    }
                    else
                    {
                        [imageView sd_setImageWithURL:[NSURL URLWithString:asset.source] placeholderImage:nil completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                            if(error)
                            {
                                NSLog(@"StayLog: PhotoCollectionImage request error: %@", error);
                            }else{
                                
                                [imageView setImage:image];
                                
                                if(imageView.image != nil){
                                    [self.sfApp.imagesCache setObject:imageView.image forKey:asset.source];
                                }
                            }
                        }];
                        
                        /*
                        NSURLSessionDataTask *task = [self.thumbConnection dataTaskWithURL:[NSURL URLWithString:asset.source] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                            if(error)
                            {
                                NSLog(@"StayLog: PhotoCollectionImage request error: %@", error);
                            }
                            else {
                                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                    imageView.image = [UIImage imageWithData:data];
                                    [self.sfApp.imagesCache setObject:imageView.image forKey:asset.source];
                                }];
                            }
                        }];
                        [task resume];*/
                    }
                }
            }
            else if([[self.initialSelectedMedias objectAtIndex:indexPath.item] isKindOfClass:[PhotoFB class]]) {
                self.modalPreview = [[UIViewController alloc] init];
                
                self.modalPreview.view.backgroundColor = [UIColor whiteColor];
                self.modalPreview.view.userInteractionEnabled = YES;
                
                imageView = [[UIImageView alloc] initWithFrame:self.modalPreview.view.frame];
                imageView.contentMode = UIViewContentModeScaleAspectFit;
                
                PhotoFB * asset = [self.initialSelectedMedias objectAtIndex:indexPath.item];
                
                if([asset.source containsString:@".mp4"] ||
                   [asset.source containsString:@".mpg"] ||
                   [asset.source containsString:@".avi"] ||
                   [asset.source containsString:@".mov"] )
                {
                    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"Player" owner:self options:nil];
                    PlayerViewController *moviePlayerViewController = [topLevelObjects objectAtIndex:0];
                    moviePlayerViewController.currentMovie = nil;
                    moviePlayerViewController.videoUrl = asset.source;
                    
                    [self presentViewController:moviePlayerViewController animated:YES completion:nil];
                    //[self performSegueWithIdentifier: @"PlayRemMovieSegue" sender:asset.source];
                    self.isLongPressing = NO;
                    return;
                }
                else {
                    if(self.sfApp.imagesCache != nil && [self.sfApp.imagesCache objectForKey:asset.source] != nil)
                    {
                        imageView.image = [self.sfApp.imagesCache objectForKey:asset.source];
                    }
                    else
                    {
                        NSURLSessionDataTask *task = [self.thumbConnection dataTaskWithURL:[NSURL URLWithString:asset.source] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                            if(error)
                            {
                                NSLog(@"StayLog: PhotoCollectionImage request error: %@", error);
                            }
                            else {
                                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                    imageView.image = [UIImage imageWithData:data];
                                    if(self.sfApp.imagesCache != nil && imageView.image != nil)
                                        [self.sfApp.imagesCache setObject:imageView.image forKey:asset.source];
                                }];
                            }
                        }];
                        [task resume];
                    }
                }
            }
            else if([[self.initialSelectedMedias objectAtIndex:indexPath.item] isKindOfClass:[VideoFB class]]) {
                self.modalPreview = [[UIViewController alloc] init];
                
                self.modalPreview.view.backgroundColor = [UIColor whiteColor];
                self.modalPreview.view.userInteractionEnabled = YES;
                
                imageView = [[UIImageView alloc] initWithFrame:self.modalPreview.view.frame];
                imageView.contentMode = UIViewContentModeScaleAspectFit;
                
                VideoFB * asset = [self.initialSelectedMedias objectAtIndex:indexPath.item];
                
                NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"Player" owner:self options:nil];
                PlayerViewController *moviePlayerViewController = [topLevelObjects objectAtIndex:0];
                moviePlayerViewController.currentMovie = nil;
                moviePlayerViewController.videoUrl = asset.source;
                
                [self presentViewController:moviePlayerViewController animated:YES completion:nil];
                //[self performSegueWithIdentifier: @"PlayRemMovieSegue" sender:asset.source];
                self.isLongPressing = NO;
                return;
            }
            else if([[self.initialSelectedMedias objectAtIndex:indexPath.item] isKindOfClass:[PHAsset class]]) {
                PHAsset * asset = [self.initialSelectedMedias objectAtIndex:indexPath.item];
                if(asset.mediaType == PHAssetMediaTypeVideo) {
                    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"Player" owner:self options:nil];
                    PlayerViewController *moviePlayerViewController = [topLevelObjects objectAtIndex:0];
                    
                    moviePlayerViewController.assetMovie = asset;
                    moviePlayerViewController.currentMovie = nil;
                    
                    if(self.imageManager == nil)
                        self.imageManager = [[PHCachingImageManager alloc] init];
                    [self.imageManager requestAVAssetForVideo:asset options:nil resultHandler:^(AVAsset * _Nullable asset, AVAudioMix * _Nullable audioMix, NSDictionary * _Nullable info) {
                        moviePlayerViewController.player = [AVPlayer playerWithURL:((AVURLAsset *)asset).URL];
                    }];
                    
                    [self presentViewController:moviePlayerViewController animated:YES completion:nil];
                    //[self performSegueWithIdentifier: @"PlayRemMovieSegue" sender:asset];
                    self.isLongPressing = NO;
                    return;
                }
                else {
                    self.modalPreview = [[UIViewController alloc] init];
                    
                    self.modalPreview.view.backgroundColor = [UIColor whiteColor];
                    self.modalPreview.view.userInteractionEnabled = YES;
                    
                    imageView = [[UIImageView alloc] initWithFrame:self.modalPreview.view.frame];
                    imageView.contentMode = UIViewContentModeScaleAspectFit;
                    
                    [self.imageManager requestImageForAsset:asset targetSize:imageView.frame.size contentMode:PHImageContentModeAspectFill options:nil resultHandler:^(UIImage *result, NSDictionary *info)
                     {
                         imageView.image = result;
                     }];
                }
            }
            
            if(imageView != nil) {
                [self.modalPreview.view addSubview:imageView];
                
                UITapGestureRecognizer *modalTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissModalView)];
                [self.modalPreview.view addGestureRecognizer:modalTap];
                
                self.modalPreview.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                self.modalPreview.modalPresentationStyle = UIModalPresentationFullScreen;
                
                [self presentViewController:self.modalPreview animated:YES completion:nil];
            }
        } else {
            //            NSLog(@"gestureRecognizer.state = %d", gestureRecognizer.state);
        }
        self.isLongPressing = NO;
    }
}

-(void)dismissModalView
{
    [self dismissViewControllerAnimated:YES completion:nil];
    self.modalPreview = nil;
}

#pragma mark - Buttons

- (IBAction)but_SelectAll_Tapped:(id)sender {
    [self SelectAllTapped:nil];
}
-(void)SelectAllTapped:(UITapGestureRecognizer *)tapGesture {
    if(self.sfApp.isEditingMode) {
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"edit-film"
                                                              action:@"remove"
                                                               label:@"select-all"
                                                               value:@1] build]];
    } else {
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"film-production"
                                                              action:@"remove"
                                                               label:@"select-all"
                                                               value:@1] build]];
    }
    
    if (!self.isAllSelected) { // not all were selected

        self.removeMedias = [self.initialSelectedMedias mutableCopy];
        
        self.isAllSelected = YES;
        //self.img_SelectAll.image = [UIImage imageNamed:@"SelectAll_Check_ON"];
        [self.but_SelectAll setTitle:NSLocalizedString(@"DESELECT_ALL", nil) forState:UIControlStateNormal];
        [self.but_SelectAll setTitle:NSLocalizedString(@"DESELECT_ALL", nil) forState:UIControlStateSelected];
        [self.but_SelectAll setTitle:NSLocalizedString(@"DESELECT_ALL", nil) forState:UIControlStateFocused];
        [self.but_SelectAll setTitle:NSLocalizedString(@"DESELECT_ALL", nil) forState:UIControlStateHighlighted];
        if(!self.isRemoveButtonShowing) {
            [self showRemoveButton];
        }
        //self.but_DeleteSelection.imageView.image = [UIImage imageNamed:@"Floating_Trash_green"];
        
//        //Google Analytics Event
//        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
//        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Botao"
//                                                              action:@"Select-all"
//                                                               label:@"Iphone_Nativo_evento_109_botao_select-all"
//                                                               value:@1] build]];
    }
    else // all photos were selected
    {
        if(self.removeMedias.count > 0) {
            [self.removeMedias removeAllObjects];
        }
        
        self.isAllSelected = NO;
        //self.img_SelectAll.image = [UIImage imageNamed:@"SelectAll_Check_OFF"];
        [self.but_SelectAll setTitle:NSLocalizedString(@"SELECT_ALL", nil) forState:UIControlStateNormal];
        [self.but_SelectAll setTitle:NSLocalizedString(@"SELECT_ALL", nil) forState:UIControlStateSelected];
        [self.but_SelectAll setTitle:NSLocalizedString(@"SELECT_ALL", nil) forState:UIControlStateFocused];
        [self.but_SelectAll setTitle:NSLocalizedString(@"SELECT_ALL", nil) forState:UIControlStateHighlighted];
        if(self.isRemoveButtonShowing) {
            [self hideRemoveButton];
        }
        //self.but_DeleteSelection.imageView.image = [UIImage imageNamed:@"Floating_Trash_grey"];
    }
    [self.photosCollectionView reloadData];
}


- (IBAction)but_DeleteSelection_Clicked:(id)sender {
    NSMutableArray *newSelection = [[NSMutableArray alloc] init];
    for (id obj in self.initialSelectedMedias) {
        if(![self.removeMedias containsObject:obj]) {
            [newSelection addObject:obj];
        }
    }
    for(id obj in self.removeMedias) { //this is to clean upload logic to avoid bugs
        [self.uploaderStayfilm removeAssetFromUpload:obj];
    }
    
    NSArray* reversedArray = [[self.navigationController.viewControllers reverseObjectEnumerator] allObjects];
    for (UIViewController *view in reversedArray) {
        if([view isKindOfClass:[MovieMakerStep1ViewController class]])
        {
            [view performSelectorOnMainThread:@selector(changeSelectedMedias:) withObject:newSelection waitUntilDone:YES];
            break;
        } else if ([view isKindOfClass:[Step1ConfirmationViewController class]]) {
            if(self.sfApp.isEditingMode) {
                [view performSelectorOnMainThread:@selector(changeEditedMedias:) withObject:newSelection waitUntilDone:YES];
            } else {
                [view performSelectorOnMainThread:@selector(changeSelectedMedias:) withObject:newSelection waitUntilDone:YES];
            }
            break;
        }
    }
    
    [self.removeMedias removeAllObjects];
    self.removeMedias = nil;
    [self.photosCollectionView removeFromSuperview];
    self.photosCollectionView = nil;
    self.thumbConnection = nil;
    [self hideRemoveButton];
    
    if(self.sfApp.isEditingMode) {
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"edit-film"
                                                              action:@"remove"
                                                               label:@"ok"
                                                               value:@1] build]];
    } else {
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"film-production"
                                                              action:@"remove"
                                                               label:@"ok"
                                                               value:@1] build]];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    //[self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Movement bottom bar

-(void)showRemoveButton {
    self.isRemoveButtonShowing = YES;
    [self.view_Remove setFrame:self.hiddenPoint];
    self.view_Remove.alpha = 1.0;
    [self.view_Remove setHidden:NO];
    
    [UIView animateWithDuration:0.4 animations:^{
        [self.view_Remove setFrame:self.showingPoint];
    }];
}

-(void)hideRemoveButton {
    self.isRemoveButtonShowing = NO;
    self.view_Remove.alpha = 1.0;
    [self.view_Remove setHidden:NO];
    
    [UIView animateWithDuration:0.4 animations:^{
        self.view_Remove.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self.view_Remove setHidden:YES];
        self.view_Remove.alpha = 1.0;
    }];
}

#pragma mark - Collection Delegates

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    [(UIActivityIndicatorView *)[self.view viewWithTag:10] stopAnimating];
    if(self.initialSelectedMedias != nil)
        return [self.initialSelectedMedias count];
    return 0;
}

static NSString *cellIdentifierPhoto = @"PhotoRemoveItemCell";

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PhotoCollectionViewCell *cell = (PhotoCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifierPhoto forIndexPath:indexPath];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PhotoItem" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    if(cell.task)
    {
        [cell.task cancel];
    }
    
    cell.photoImageView.image = nil;
    [cell.spinner startAnimating];
    [cell.spinner setHidden:NO];
    cell.source = nil;
    
    if(self.removeMedias != nil) {
        if([self.removeMedias containsObject:[self.initialSelectedMedias objectAtIndex:indexPath.item]])
        { // Selected photo
            [cell.selectedView setHidden:NO];
            [cell.unselectedView setHidden:YES];
            cell.photoImageView.alpha = 0.7;
        }
        else // Deselect Photo
        {
            [cell.selectedView setHidden:YES];
            cell.photoImageView.alpha = 1.0;
            [cell.unselectedView setHidden:NO];
        }
    }
    else
    {
        [cell.selectedView setHidden:YES];
        [cell.unselectedView setHidden:NO];
        cell.photoImageView.alpha = 1.0;
    }
    
    if([[self.initialSelectedMedias objectAtIndex:indexPath.item] isKindOfClass:[Media class]]) {
        Media * media = [self.initialSelectedMedias objectAtIndex:indexPath.item];
        cell.source = media.source;
    }
    else if([[self.initialSelectedMedias objectAtIndex:indexPath.item] isKindOfClass:[PhotoFB class]]) {
        PhotoFB * passet = [self.initialSelectedMedias objectAtIndex:indexPath.item];
        cell.source = passet.source;
    }
    else if([[self.initialSelectedMedias objectAtIndex:indexPath.item] isKindOfClass:[VideoFB class]]) {
        VideoFB * passet = [self.initialSelectedMedias objectAtIndex:indexPath.item];
        cell.source = passet.source;
    }
    if(cell.source != nil) {
        if(self.sfApp == nil)
        {
            self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
        }
        
        if([self.sfApp.imagesCache objectForKey:cell.source] != nil)
        {
            cell.photoImageView.image = [self.sfApp.imagesCache objectForKey:cell.source];
            [cell.spinner stopAnimating];
            [cell.spinner setHidden:YES];
            if([cell.source containsString:@".mp4"] ||
               [cell.source containsString:@".avi"] ||
               [cell.source containsString:@".mpg"] ||
               [cell.source containsString:@".mov"] ){
                [cell.vid_Play setHidden:NO];
                [cell.vid_Time setHidden:NO];
                [cell.vid_Gradient setHidden:NO];
                if([[self.initialSelectedMedias objectAtIndex:indexPath.item] isKindOfClass:[VideoFB class]]) {
                    cell.duration = ((VideoFB*)[self.initialSelectedMedias objectAtIndex:indexPath.item]).length;
                    long seconds = lroundf([cell.duration floatValue]);
                    int hour = (int)seconds / 3600;
                    int mins = (seconds % 3600) / 60;
                    int secs = seconds % 60;
                    if(hour > 0) {
                        cell.vid_Time.text = [NSString stringWithFormat:@"%d:%02d:%02d",hour,mins,secs];
                    } else {
                        cell.vid_Time.text = [NSString stringWithFormat:@"%d:%02d",mins,secs];
                    }
                }
                else {
                    AVAsset *asset = [AVAsset assetWithURL:[NSURL URLWithString:cell.source]];
                    cell.duration = [NSNumber numberWithFloat:lroundf(CMTimeGetSeconds(asset.duration))];
                    long seconds = lroundf([cell.duration floatValue]);
                    int hour = (int)seconds / 3600;
                    int mins = (seconds % 3600) / 60;
                    int secs = seconds % 60;
                    if(hour > 0) {
                        cell.vid_Time.text = [NSString stringWithFormat:@"%d:%02d:%02d",hour,mins,secs];
                    } else {
                        cell.vid_Time.text = [NSString stringWithFormat:@"%d:%02d",mins,secs];
                    }
                }
            } else {
                [cell.vid_Play setHidden:YES];
                [cell.vid_Time setHidden:YES];
                [cell.vid_Gradient setHidden:YES];
            }
        }
        else
        {
            __weak typeof(self) selfDelegate = self;
            if([cell.source containsString:@".mp4"] ||
               [cell.source containsString:@".avi"] ||
               [cell.source containsString:@".mpg"] ||
               [cell.source containsString:@".mov"] ){
                if([[self.initialSelectedMedias objectAtIndex:indexPath.item] isKindOfClass:[VideoFB class]]) {
                    [cell.vid_Time setHidden:NO];
                    [cell.vid_Play setHidden:NO];
                    cell.duration = ((VideoFB*)[self.initialSelectedMedias objectAtIndex:indexPath.item]).length;
                    long seconds = lroundf([cell.duration floatValue]);
                    int hour = (int)seconds / 3600;
                    int mins = (seconds % 3600) / 60;
                    int secs = seconds % 60;
                    if(hour > 0) {
                        cell.vid_Time.text = [NSString stringWithFormat:@"%d:%02d:%02d",hour,mins,secs];
                    } else {
                        cell.vid_Time.text = [NSString stringWithFormat:@"%d:%02d",mins,secs];
                    }
                    
                    [cell.photoImageView sd_setImageWithURL:[NSURL URLWithString:((VideoFB*)[self.initialSelectedMedias objectAtIndex:indexPath.item]).picture] placeholderImage:nil completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                        if(error)
                        {
                            NSLog(@"StayLog: PhotoCollectionImage request error: %@", error);
                        }else{
                            if (image != nil){
                                [cell.photoImageView setImage:image];
                                
                                if(cell.photoImageView.image != nil){
                                    [selfDelegate.sfApp.imagesCache setObject:cell.photoImageView.image forKey:cell.source];
                                }
                                [cell.spinner stopAnimating];
                            }
                        }
                    }];
                   /* cell.task = [self.thumbConnection dataTaskWithURL:[NSURL URLWithString:((VideoFB*)[self.initialSelectedMedias objectAtIndex:indexPath.item]).picture] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                        if(error)
                        {
                            NSLog(@"StayLog: PhotoCollectionImage request error: %@", error);
                        }
                        else {
                            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                cell.photoImageView.image = [UIImage imageWithData:data];
                                if(cell.photoImageView.image != nil)
                                    [selfDelegate.sfApp.imagesCache setObject:cell.photoImageView.image forKey:cell.source];
                                [cell.spinner stopAnimating];
                            }];
                        }
                    }];
                    [cell.task resume];
                    [cell.spinner stopAnimating];*/
                }
                else {
                    AVAsset *asset = [AVAsset assetWithURL:[NSURL URLWithString:cell.source]];
                    AVAssetImageGenerator *generator = [AVAssetImageGenerator assetImageGeneratorWithAsset:asset];
                    generator.appliesPreferredTrackTransform = YES;
                    CMTime time = CMTimeMakeWithSeconds(1, 2);
                    NSError *error = nil;
                    @try {
                        CGImageRef img = [generator copyCGImageAtTime:time actualTime:nil error:&error];
                        if(!error)
                        {
                            UIImage *image = [UIImage imageWithCGImage:img];
                            cell.photoImageView.image = image;
                            [selfDelegate.sfApp.imagesCache setObject:cell.photoImageView.image forKey:cell.source];
                        }
                        [cell.vid_Play setHidden:NO];
                        [cell.vid_Gradient setHidden:NO];
                        [cell.vid_Time setHidden:NO];
                        
                        cell.duration = [NSNumber numberWithFloat:lroundf(CMTimeGetSeconds(asset.duration))];
                        long seconds = lroundf([cell.duration floatValue]);
                        int hour = (int)seconds / 3600;
                        int mins = (seconds % 3600) / 60;
                        int secs = seconds % 60;
                        if(hour > 0) {
                            cell.vid_Time.text = [NSString stringWithFormat:@"%d:%02d:%02d",hour,mins,secs];
                        } else {
                            cell.vid_Time.text = [NSString stringWithFormat:@"%d:%02d",mins,secs];
                        }
                        [cell.spinner stopAnimating];
                        [cell.spinner setHidden:YES];
                    } @catch (NSException *ex) {
                        NSLog(@"StayLog: EXCEPTION getting video thumb with error: %@", ex.description);
                    }
                }
            }
            else {
                [cell.photoImageView sd_setImageWithURL:[NSURL URLWithString:cell.source] placeholderImage:nil completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                    if(error){
                        NSLog(@"StayLog: PhotoCollectionImage request error: %@", error);
                    }else{
                        if (image != nil){
                            [cell.photoImageView setImage:image];
                            
                            if(cell.photoImageView.image != nil){
                                [selfDelegate.sfApp.imagesCache setObject:cell.photoImageView.image forKey:cell.source];
                            }
                            [cell.spinner stopAnimating];
                            [cell.spinner setHidden:YES];
                        }
                    }
                    
                }];
                /*cell.task = [self.thumbConnection dataTaskWithURL:[NSURL URLWithString:cell.source] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    if(error)
                    {
                        NSLog(@"StayLog: PhotoCollectionImage request error: %@", error);
                    }
                    else {
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            cell.photoImageView.image = [UIImage imageWithData:data];
                            [selfDelegate.sfApp.imagesCache setObject:cell.photoImageView.image forKey:cell.source];
                            [cell.spinner stopAnimating];
                            [cell.spinner setHidden:YES];
                        }];
                    }
                }];
                [cell.task resume];*/
                [cell.vid_Play setHidden:YES];
                [cell.vid_Time setHidden:YES];
                [cell.vid_Gradient setHidden:YES];
            }
        }
    }
    if([[self.initialSelectedMedias objectAtIndex:indexPath.item] isKindOfClass:[PHAsset class]]) {
        cell.source = nil;
        PHAsset * asset = [self.initialSelectedMedias objectAtIndex:indexPath.item];
        if(asset.mediaType == PHAssetMediaTypeVideo) {
            [cell.vid_Play setHidden:NO];
            [cell.vid_Time setHidden:NO];
            [cell.vid_Gradient setHidden:NO];
            
            long seconds = lroundf(asset.duration);
            int hour = (int)seconds / 3600;
            int mins = (seconds % 3600) / 60;
            int secs = seconds % 60;
            if(hour > 0) {
                cell.vid_Time.text = [NSString stringWithFormat:@"%d:%02d:%02d",hour,mins,secs];
            } else {
                cell.vid_Time.text = [NSString stringWithFormat:@"%d:%02d",mins,secs];
            }
        } else {
            [cell.vid_Play setHidden:YES];
            [cell.vid_Time setHidden:YES];
            [cell.vid_Gradient setHidden:YES];
        }
        [self.imageManager requestImageForAsset:asset targetSize:cell.photoImageView.frame.size contentMode:PHImageContentModeAspectFill options:nil resultHandler:^(UIImage *result, NSDictionary *info)
         {
             cell.photoImageView.image = result;
         }];
        if([self.uploaderStayfilm isAssetOnUploadList:asset]) {
            cell.photoImageView.alpha = 0.7;
            [cell.spinner startAnimating];
            [cell.spinner setHidden:NO];
        } else {
            cell.photoImageView.alpha = 1.0;
            [cell.spinner stopAnimating];
            [cell.spinner setHidden:YES];
        }
    }
    
    return cell;
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
        @try {
            PhotoCollectionViewCell *cell = (PhotoCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifierPhoto forIndexPath:indexPath];
    
            if(self.removeMedias != nil) {
                if(![self.removeMedias containsObject:[self.initialSelectedMedias objectAtIndex:indexPath.item]])
                { // Selecting photo
                    [self.removeMedias addObject:[self.initialSelectedMedias objectAtIndex:indexPath.item]];
                    [cell.selectedView setHidden:NO];
                    [cell.unselectedView setHidden:YES];
                    cell.photoImageView.alpha = 0.7;
                }
                else // Deselect Photo
                {
                    [self.removeMedias removeObject:[self.initialSelectedMedias objectAtIndex:indexPath.item]];
                    [cell.selectedView setHidden:YES];
                    [cell.unselectedView setHidden:NO];
                    cell.photoImageView.alpha = 1.0;
                    //self.img_SelectAll.image = [UIImage imageNamed:@"SelectAll_Check_OFF"];
                    [self.but_SelectAll setTitle:NSLocalizedString(@"SELECT_ALL", nil) forState:UIControlStateNormal];
                    [self.but_SelectAll setTitle:NSLocalizedString(@"SELECT_ALL", nil) forState:UIControlStateSelected];
                    [self.but_SelectAll setTitle:NSLocalizedString(@"SELECT_ALL", nil) forState:UIControlStateFocused];
                    [self.but_SelectAll setTitle:NSLocalizedString(@"SELECT_ALL", nil) forState:UIControlStateHighlighted];
                }
            }

    
            if(self.removeMedias.count > 0) {
                //self.but_DeleteSelection.imageView.image = [UIImage imageNamed:@"Floating_Trash_green"];
                if(!self.isRemoveButtonShowing) {
                    [self showRemoveButton];
                }
            }
            else {
                if(self.isRemoveButtonShowing) {
                    [self hideRemoveButton];
                }
                //self.but_DeleteSelection.imageView.image = [UIImage imageNamed:@"Floating_Trash_grey"];
            }
    
            [self.photosCollectionView reloadItemsAtIndexPaths:[NSMutableArray arrayWithObject:indexPath]];
            [self.photosCollectionView reloadData];
        }
        @catch (NSException *exception) {
            NSLog(@"StayLog: EXCEPTION didSelectItemAtPath with error: %@", exception.description);
        }
    
}

//- (CGSize)collectionView:(UICollectionView *)collectionView
//                  layout:(UICollectionViewLayout *)collectionViewLayout
//  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    return  CGSizeMake((self.photosCollectionView.frame.size.width - 20)/3, 140);
//}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeMake(0, 0);
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    //Set cell size to fit all screens
    CGFloat width  = self.view.frame.size.width;
    width -= 6.0;
    width /= 3.0;
    return CGSizeMake(width,width);
}

#pragma mark - Uploader Delegates


-(void)uploadProgressChanged:(NSNumber *)progress
{
    // Not important on this page.
}

- (void)converterDidFailMedia:(NSString *)type
{
    NSString *message = nil;
    if(self.sfApp == nil)
    {
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    }
    if ( (self.sfApp.isEditingMode)? (self.sfApp.editedMedias.count < [self.sfApp.sfConfig.min_photos intValue]) : (self.sfApp.finalMedias.count < [self.sfApp.sfConfig.min_photos intValue]) ) {
        message = [NSString stringWithFormat:NSLocalizedString(@"UPLOADER_CONVERT_FAILED_UPLOAD_AGAIN_MESSAGE", nil), ((self.sfApp.isEditingMode)? ([self.sfApp.sfConfig.min_photos intValue] - self.sfApp.editedMedias.count) : ([self.sfApp.sfConfig.min_photos intValue] - self.sfApp.finalMedias.count))];
    }
    else if ([type isEqualToString:@"image"]) {
        message = NSLocalizedString(@"UPLOADER_CONVERT_FAILED_IMAGE_MESSAGE", nil);
    }
    else
    {
        message = NSLocalizedString(@"UPLOADER_CONVERT_FAILED_VIDEO_MESSAGE", nil);
    }
    
    if(self.sfApp.isEditingMode) {
        @synchronized (self.sfApp.editedMedias) {
            for (id obj in self.sfApp.editedMedias) {
                if([obj isKindOfClass:[PHAsset class]]) {
                    BOOL find = NO;
                    for (PHAsset *asset in self.uploaderStayfilm.listUploadMedias) {
                        if(asset == obj) {
                            find = YES;
                            break;
                        }
                    }
                    if(!find) {
                        NSUInteger i = [self.sfApp.editedMedias indexOfObject:obj];
                        [self.sfApp.editedMedias removeObjectAtIndex:i];
                        [self.photosCollectionView reloadData];
                        break;
                    }
                }
            }
        }
    } else {
        @synchronized (self.sfApp.finalMedias) {
            for (id obj in self.sfApp.finalMedias) {
                if([obj isKindOfClass:[PHAsset class]]) {
                    BOOL find = NO;
                    for (PHAsset *asset in self.uploaderStayfilm.listUploadMedias) {
                        if(asset == obj) {
                            find = YES;
                            break;
                        }
                    }
                    if(!find) {
                        NSUInteger i = [self.sfApp.finalMedias indexOfObject:obj];
                        [self.sfApp.finalMedias removeObjectAtIndex:i];
                        [self.photosCollectionView reloadData];
                        break;
                    }
                }
            }
        }
    }
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_CONVERT_FAILED_TITLE", nil)
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)uploadDidFailMedia:(NSString *)type
{
    NSString *message = nil;
    if(self.sfApp == nil)
    {
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    }
    if ( (self.sfApp.isEditingMode)? (self.sfApp.editedMedias.count < [self.sfApp.sfConfig.min_photos intValue]) : (self.sfApp.finalMedias.count < [self.sfApp.sfConfig.min_photos intValue]) ) {
        message = [NSString stringWithFormat:NSLocalizedString(@"UPLOADER_UPLOAD_FAILED_UPLOAD_AGAIN_MESSAGE", nil), ((self.sfApp.isEditingMode)? ([self.sfApp.sfConfig.min_photos intValue] - self.sfApp.editedMedias.count) : ([self.sfApp.sfConfig.min_photos intValue] - self.sfApp.finalMedias.count))];
    }
    else if ([type isEqualToString:@"image"]) {
        message = NSLocalizedString(@"UPLOADER_UPLOAD_FAILED_IMAGE_MESSAGE", nil);
    }
    else
    {
        message = NSLocalizedString(@"UPLOADER_UPLOAD_FAILED_VIDEO_MESSAGE", nil);
    }
    
    if(self.sfApp.isEditingMode) {
        @synchronized (self.sfApp.editedMedias) {
            for (id obj in self.sfApp.editedMedias) {
                if([obj isKindOfClass:[PHAsset class]]) {
                    BOOL find = NO;
                    for (PHAsset *asset in self.uploaderStayfilm.listUploadMedias) {
                        if(asset == obj) {
                            find = YES;
                            break;
                        }
                    }
                    if(!find) {
                        NSUInteger i = [self.sfApp.editedMedias indexOfObject:obj];
                        [self.sfApp.editedMedias removeObjectAtIndex:i];
                        [self.photosCollectionView reloadData];
                        break;
                    }
                }
            }
        }
    } else {
        @synchronized (self.sfApp.finalMedias) {
            for (id obj in self.sfApp.finalMedias) {
                if([obj isKindOfClass:[PHAsset class]]) {
                    BOOL find = NO;
                    for (PHAsset *asset in self.uploaderStayfilm.listUploadMedias) {
                        if(asset == obj) {
                            find = YES;
                            break;
                        }
                    }
                    if(!find) {
                        NSUInteger i = [self.sfApp.finalMedias indexOfObject:obj];
                        [self.sfApp.finalMedias removeObjectAtIndex:i];
                        [self.photosCollectionView reloadData];
                        break;
                    }
                }
            }
        }
    }
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_UPLOAD_FAILED_TITLE", nil)
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)uploadDidFail:(Uploader *)uploader
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_ERROR_TITLE", nil)
                                                                   message:NSLocalizedString(@"UPLOADER_ERROR_MESSAGE", nil)
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)uploadDidStartSendingConfirmationMedias:(Uploader *)uploader
{
    // not important on this page.
}

-(void)uploadDidStart
{
    // does not happen in this page
}

-(void)uploadDidFinish:(Uploader *)uploader
{
    if(self.sfApp == nil) {
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    }
    if(self.uploaderStayfilm.listUploadedMedias != nil && self.uploaderStayfilm.listUploadedMedias.count > 0) {
        for (PHAsset * asset in self.uploaderStayfilm.listUploadedMedias.allValues) {
            if(self.sfApp.isEditingMode) {
                if(![self.sfApp.editedMedias containsObject:asset]) {
                    [self.sfApp.editedMedias addObject:asset];
                }
            } else {
                if(![self.sfApp.finalMedias containsObject:asset]) {
                    [self.sfApp.finalMedias addObject:asset];
                }
            }
        }
    }
    if(self.sfApp.isDebugMode) {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_FINISHED", nil)
                                                                       message:[[NSString alloc] initWithFormat:NSLocalizedString(@"UPLOADED_X_MEDIAS", nil), uploader.listUploadedMedias.count]
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    [self.photosCollectionView reloadData];
}

-(void)uploadFinishedMedia:(PHAsset *)asset {
    [self.photosCollectionView reloadData];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"PlayRemMovieSegue"])
    {
        //        //Google Analytics
        //        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        //        [tracker set:kGAIScreenName value:@"Iphone_Nativo_Tela_10_SFMobile_Meus-filmes_ver-filme"];
        //        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
        
        AVAudioSession *audioSession = [AVAudioSession sharedInstance];
        NSError *error = nil;
        BOOL result = NO;
        result = [audioSession setActive:YES withOptions:0 error:&error]; // iOS6+
        
        if (!result && error) {
            // deal with the error
            NSLog(@"StayLog: AVAudioSession Error: %@", error);
        }
        
        error = nil;
        result = [audioSession setCategory:AVAudioSessionCategoryPlayback error:&error];
        
        if (!result && error) {
            // deal with the error
            NSLog(@"StayLog: AVAudioSession setCategory Error: %@", error);
        }
        
        PlayerViewController *moviePlayerViewController = segue.destinationViewController;
        if([sender isKindOfClass:[PHAsset class]]) {
            moviePlayerViewController.assetMovie = (PHAsset *)sender;
            moviePlayerViewController.currentMovie = nil;
            
            [self.imageManager requestAVAssetForVideo:sender options:nil resultHandler:^(AVAsset * _Nullable asset, AVAudioMix * _Nullable audioMix, NSDictionary * _Nullable info) {
                moviePlayerViewController.player = [AVPlayer playerWithURL:((AVURLAsset *)asset).URL];
            }];
        }
        else if([sender isKindOfClass:[Movie class]]) {
            moviePlayerViewController.currentMovie = (Movie *)sender;
            moviePlayerViewController.assetMovie = nil;
            moviePlayerViewController.videoUrl = [sender videoUrl];
        }
        else if([sender isKindOfClass:[NSString class]]) {
            moviePlayerViewController.assetMovie = nil;
            moviePlayerViewController.currentMovie = nil;
            moviePlayerViewController.videoUrl = sender;
        }
    }
}


@end
