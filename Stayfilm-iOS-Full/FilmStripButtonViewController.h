//
//  FilmStripButtonViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Henrique on 6/22/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilmStripButtonViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *viewAll;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *txt_stripCounter;
@property (weak, nonatomic) IBOutlet UILabel *txt_stripCounterBig;
@property (weak, nonatomic) IBOutlet UILabel *txt_stripMinimum;
@property (weak, nonatomic) IBOutlet UIView *view_stripArrow;
@property (weak, nonatomic) IBOutlet UIImageView *img_stripCounter;
@property (weak, nonatomic) IBOutlet UIButton *but_filmStrip;
@property (weak, nonatomic) IBOutlet UIButton *but_NextStep;
@property (weak, nonatomic) IBOutlet UIImageView *view_NextStepArrow;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *but_filmStripConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTrailing;
@property (weak, nonatomic) IBOutlet UIView *viewEditing;
@property (weak, nonatomic) IBOutlet UILabel *txt_stripBigMinimum;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_but_Next_height;
@property (weak, nonatomic) IBOutlet UIView *view_connectionStatus;
@property (weak, nonatomic) IBOutlet UILabel *lbl_connectionStatus;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintConnectionStatusHeight;
@property (nonatomic, assign) BOOL isInsideFilmStrip;
@property (weak, nonatomic) UINavigationController *auxNavigation;
@property (assign, nonatomic) int manualCounter;
@property (assign, nonatomic) BOOL isEditingMode;
@property (assign, nonatomic) BOOL isAnimating;

-(void)enableButton;
-(void)disableButton;
-(void)setViewInsideFilmStrip;
-(void)setViewOutsideFilmStrip;
-(void)setHidden:(BOOL)hidden;
-(void)setButtonNextHidden:(BOOL)hidden;
-(void)setEditingMode:(BOOL)isEditing;
-(void)showEditConfirmationButton:(BOOL)didEdit;
-(void)setUpdateButtonPositionHigher:(BOOL)raiseButton;
-(void)updateFilmStrip;
-(void)updateFilmStripWithCounter:(NSString *)counter andCondition:(BOOL)condition;
-(void)connectivityViewUpdate;

-(void)changeTopStatusBarColorToWhite:(BOOL)isWhite;

-(void)resetSingletonDelegate;

@end
