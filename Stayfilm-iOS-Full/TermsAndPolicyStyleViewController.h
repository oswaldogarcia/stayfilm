//
//  TermsAndPolicyStyleViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 12/6/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol TermsStyleDelegate <NSObject>
- (void)confirmTerms;
@end

@interface TermsAndPolicyStyleViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *outView;
@property (weak, nonatomic) IBOutlet UILabel *mainTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *noSmokingLabel;
@property (weak, nonatomic) IBOutlet UILabel *noIlegalLabel;
@property (weak, nonatomic) IBOutlet UILabel *noCommercialLabel;
@property (weak, nonatomic) IBOutlet UISwitch *termsDineySwitch;
@property (weak, nonatomic) IBOutlet UILabel *termsDisneyLabel;
@property (weak, nonatomic) IBOutlet UISwitch *termsSFSwitch;
@property (weak, nonatomic) IBOutlet UILabel *termsSFLabel;
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;
@property (nonatomic) BOOL isEditing;
@property (nonatomic, assign) id <TermsStyleDelegate> delegate;
@end

NS_ASSUME_NONNULL_END
