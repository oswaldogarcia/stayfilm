//
//  ImageUploadOperation.h
//  Stayfilm for Messenger
//
//  Created by Henrique Ormonde on 26/10/15.
//  Copyright © 2015 Stayfilm. All rights reserved.
//

//@import Photos;
//@import AssetsLibrary;
//@import Foundation;
#import <Foundation/Foundation.h>
#import "StayfilmApp.h"
#import "Media.h"

@protocol ImageUploaderDelegate;

@interface ImageUploadOperation : NSOperation <NSURLSessionTaskDelegate, NSURLSessionDataDelegate>

@property (nonatomic, assign) id <ImageUploaderDelegate> delegate;
@property (nonatomic, strong) NSString *pathFile;
@property (nonatomic, strong) Media *uploadedMedia;


- (id)initWithMediaPath:(NSString *)p_pathFile withEcho:(NSString *)p_echo withAlbumID:(NSString *)p_albumID withBackgroundSessionIdentifier:(NSString *)identifier delegate:(id<ImageUploaderDelegate>) theDelegate;

@end

@protocol ImageUploaderDelegate <NSObject>
- (void)imageUploaderDidFinish:(ImageUploadOperation *)uploader;
- (void)imageUploaderDidFail:(ImageUploadOperation *)uploader;
@end
