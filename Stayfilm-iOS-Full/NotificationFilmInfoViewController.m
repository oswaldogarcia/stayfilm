//
//  NotificationFilmInfoViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 12/11/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import "NotificationFilmInfoViewController.h"
#import <Google/Analytics.h>

@interface NotificationFilmInfoViewController ()

@end

@implementation NotificationFilmInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setLanguage];
}
- (void)viewDidAppear:(BOOL)animated{
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"profile_curation"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
}

-(void)setLanguage{
    
    self.titleLabel.text = NSLocalizedString(@"HOW_DISNEY_WORK_TITLE", nil);

    
    UIFont *font1 = [UIFont fontWithName:@"ProximaNova-Regular" size:15];
    UIFont *font2 = [UIFont fontWithName:@"ProximaNova-Bold" size:15];
    UIColor *greenColor =  [UIColor colorWithRed:0.39 green:0.75 blue:0.60 alpha:1.0];
    NSDictionary *fontDict1 = [NSDictionary dictionaryWithObject: font1 forKey:NSFontAttributeName];
    NSMutableDictionary *fontDict2 = [[NSMutableDictionary alloc]init];
    [fontDict2 setObject:font2  forKey:NSFontAttributeName];
    [fontDict2 setObject:greenColor  forKey:NSForegroundColorAttributeName];
    
    NSMutableAttributedString *fontType1 = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"TERM_DISNEY", nil) attributes: fontDict1];
    
    NSMutableAttributedString *fontType2 = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"TERMS_AND_POLICY", nil) attributes: fontDict2];
    
    [fontType1 appendAttributedString:fontType2];
    self.termsLabel.attributedText = fontType1;
    
   // self.termsLabel.text =  NSLocalizedString(@"TERM_DISNEY", nil);
    
    self.noSmokingLabel.text = NSLocalizedString(@"NO_SMOKING", nil);
    self.noIllegalLabel.text = NSLocalizedString(@"NO_ILEGAL", nil);
    self.noCommercialLabel.text = NSLocalizedString(@"NO_COMMERCIAL", nil);
    
    self.inReviewLabel.text = NSLocalizedString(@"IN_REVIEW_TITLE", nil);
    self.inReviewTextLabel.text = NSLocalizedString(@"IN_REVIEW_TEXT", nil);
    
    self.editedLabel.text = NSLocalizedString(@"EDITED_TITLE", nil);
     self.editedTextLabel.text = NSLocalizedString(@"EDITED_TEXT", nil);

    [self.dismissButton setTitle:NSLocalizedString(@"OK", nil) forState:UIControlStateNormal];


}
- (IBAction)termsLinkAction:(id)sender {
    
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"curation"
                                                          action:@"link"
                                                           label:@"terms_disney"
                                                           value:@1] build]];
    
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://www.stayfilm.com/institutional/customerterms/disney2019"]];
}


- (IBAction)dismissAction:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
