//
//  AlbumSF.h
//  Stayfilm for Messenger
//
//  Created by Henrique Ormonde on 08/05/15.
//  Copyright (c) 2015 Stayfilm. All rights reserved.
//

#ifndef Stayfilm_Full_AlbumSF_h
#define Stayfilm_Full_AlbumSF_h

#import <Foundation/Foundation.h>
#import "User.h"

@class User;

@interface AlbumSF : NSObject <NSCopying>

@property (nonatomic, strong) NSString *idAlbum;
@property (nonatomic, strong) NSMutableArray *idMedias;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *description;
@property (nonatomic, strong) NSString *cover;
@property (nonatomic, strong) NSString *network;
@property (nonatomic, strong) NSNumber *mediaCount;

-(id)init;
+(NSMutableArray *)getAlbumsFromUser:(User *)p_user fromNetwork:(NSString *)p_network withOffset:(int)p_offset withLimit:(int)p_limit;
+(AlbumSF *)getAlbumWithID:(NSString *)p_idAlbum;
+(AlbumSF *)createAlbumWithMedias:(NSArray *)p_medias withTitle:(NSString *)p_title;
-(BOOL)updateAlbumWithMedias:(NSArray *)p_medias;
-(BOOL)updateMediasList;

@end

#endif
