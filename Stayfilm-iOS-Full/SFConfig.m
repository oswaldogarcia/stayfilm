//
//  Genres.m
//  Stayfilm for Messenger
//
//  Created by Henrique Ormonde on 23/04/15.
//  Copyright (c) 2015 Stayfilm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SFConfig.h"
#import "StayfilmApp.h"

@interface SFConfig ()
@end

@implementation SFConfig

@synthesize generalConfig, genres, stories;

StayfilmApp *sfAppConfig;

int getTemplatesAttempts = 0;

+(SFConfig *)getAllConfigsFromWS:(WSConfig *)ws_config {
    
    @try
    {
        SFConfig *config = [[SFConfig alloc] init];
        StayWS *ws = [[StayWS alloc] init];

        NSDictionary *args = nil;
        NSDictionary *response = nil;
        NSArray *pathArgs = nil;
//        NSString *langID = [[NSLocale preferredLanguages] objectAtIndex:0];
//        NSLocale *local = [NSLocale localeWithLocaleIdentifier:@"EN"];
//        NSString * language = [[local displayNameForKey:NSLocaleLanguageCode value:langID]lowercaseString];
//
//        args = @{ @"device" : @"ios-native",
//                  @"language" : language };
        
        args = @{ @"device" : @"ios-native" };
        
        response = [ws requestWebServiceWithURL:[ws_config getConfigPathWithKey:@"getConfig"] withRequestType:@"GET" withPathArguments:pathArgs withArguments:args withIdSession:nil withCache:NO];
        
        NSHTTPURLResponse *statusResponse = (NSHTTPURLResponse *) ws.responseMessage;
        if(response != nil && (statusResponse.statusCode == 200 || statusResponse.statusCode == 201))
        {
            NSMutableDictionary *conf = [[NSMutableDictionary alloc] init];
            //get ios-native config
            if(response[@"general"] != nil)
             {
                 config.generalConfig = response[@"general"];
                 [conf setObject:response[@"general"] forKey:@"general"];
                 
                 if(response[@"general"][@"enable_push_geo"] != nil)
                 {
                     [conf setObject:response[@"general"][@"enable_push_geo"] forKey:@"enable_push_geo"];
                     config.push_enable_geo = response[@"general"][@"enable_push_geo"];
                 }
                 if(response[@"general"][@"min_upload_photo"] != nil)
                 {
                     [conf setObject:response[@"general"][@"min_upload_photo"] forKey:@"min_upload_photo"];
                     config.min_photos = response[@"general"][@"min_upload_photo"];
                 }
                 if(response[@"general"][@"max_photo"] != nil)
                 {
                     [conf setObject:response[@"general"][@"max_photo"] forKey:@"max_photo"];
                     config.max_photos = response[@"general"][@"max_photo"];
                 }else{
                      [conf setObject:[NSNumber numberWithInteger:100] forKey:@"max_photo"];
                     config.max_photos = [NSNumber numberWithInteger:100];
                 }
             }
            if(response[@"moviemaker"] != nil)
            {
                if(response[@"moviemaker"][@"genres"] != nil)
                {
                    [conf setObject:response[@"moviemaker"][@"genres"] forKey:@"moviemaker_genres"];
                    
                    config.genres = [[NSMutableArray alloc] init];
                    for(NSDictionary *var in response[@"moviemaker"][@"genres"]) {
                        [config.genres addObject:[Genres customClassWithProperties:var]];
                    }
                    [config getTemplates];
                }
            }
            if(response[@"moviemaker"] != nil && response[@"general"] != nil) {
                NSData *dataSave = [NSKeyedArchiver archivedDataWithRootObject:conf];
                [[NSUserDefaults standardUserDefaults] setObject:dataSave forKey:@"SFConfig"];
                
                NSString * md5string = [[NSString alloc] initWithData:ws.responseBody encoding:NSUTF8StringEncoding];
                NSString *md5 = [StayfilmApp MD5String:md5string];
                [[NSUserDefaults standardUserDefaults] setObject:md5 forKey:@"hash_config"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }

            return config;
        }
        return nil;
    }
    @catch (NSException *ex)
    {
        NSLog(@"StayLog: EXCEPTION getAllConfigsFromWS: %@", ex.description);
        return nil;
    }
}

-(void)getTemplates {
    
    ShellWebService *service = [ShellWebService getSharedInstance];
    
    NSString *langID = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSLocale *local = [NSLocale localeWithLocaleIdentifier:@"EN"];
    NSString * language = [[local displayNameForKey:NSLocaleLanguageCode value:langID]lowercaseString];
    
    NSDictionary *parameters = @{ @"device" : @"ios-native",
                                  @"language" : language };
    
    [service callService:parameters endpointName:endpointGetTemplates withCompletionBlock:^(NSDictionary *resultArray, NSError *error) {
        
        
        if(error) {
            if(getTemplatesAttempts < 6) {
                ++getTemplatesAttempts;
                [self getTemplates];
            }else {
                NSLog(@"StayLog: EXCEPTION in getTemplates with error: %@", error.description);
                getTemplatesAttempts = 0;
                [self getTemplates];
            }
            return;
        }
        
        NSMutableArray *templates = [[NSMutableArray alloc]init];
        int cont = 0;
        for ( NSDictionary *template in resultArray[@"data"]){
            
            [templates addObject:[Template customClassWithProperties:template]];
            cont ++;
        }
        
     
        for ( int i = 0; i < self.genres.count;i++){
            
            Genres *gen = self.genres[i];
            gen.templates = [[NSMutableArray alloc] init];
            
            for (Template *templ in templates){
                
                if (templ.idgenre == gen.idgenre){
                    
                    [gen.templates addObject:templ];
                    self.genres[i] = gen;
                }
            }
        }
        
        getTemplatesAttempts = 0;
    }];
}

@end


@interface GenreConfig()
@end

@implementation GenreConfig

+ (GenreConfig *)customClassWithProperties:(NSDictionary *)properties {
    if(properties != nil)
    {
        NSMutableDictionary *filteredObjs = [[NSMutableDictionary alloc] init];
        for (NSString* key in properties) {
            id value = [properties objectForKey:key];
            if ([key isEqualToString:@"campaignslug"]) {
                [filteredObjs setValue:value forKey:@"campaignslug"];
            }
            else if ([key isEqualToString:@"campaign"]) {
                [filteredObjs setValue:value forKey:@"campaign"];
            }
            
        }
        return [[self alloc] initWithProperties:filteredObjs];
    }
    else
    {
        return nil;
    }
}

- (GenreConfig *)initWithProperties:(NSDictionary *)properties {
    if (self = [self init]) {
        @try {
            [self setValuesForKeysWithDictionary:properties];
            self.campaign = [Campaign customClassWithProperties:properties[@"campaign"]];
        }
        @catch (NSException *ex) {
            return nil;
        }
    }
    return self;
}

@end

@interface Campaign()
@end

@implementation Campaign

+ (Campaign *)customClassWithProperties:(NSDictionary *)properties {
    if(properties != nil)
    {
        NSMutableDictionary *filteredObjs = [[NSMutableDictionary alloc] init];
        for (NSString* key in properties) {
            id value = [properties objectForKey:key];
            if ([key isEqualToString:@"idcustomer"]) {
                [filteredObjs setValue:value forKey:@"idcustomer"];
            }else if ([key isEqualToString:@"idcampaign"]) {
                [filteredObjs setValue:value forKey:@"idcampaign"];
            }else if ([key isEqualToString:@"created"]) {
                [filteredObjs setValue:value forKey:@"created"];
            }else if ([key isEqualToString:@"curation"]) {
                [filteredObjs setValue:value forKey:@"curation"];
            }else if ([key isEqualToString:@"data"]) {
                [filteredObjs setValue:value forKey:@"data"];
            }else if ([key isEqualToString:@"startdate"]) {
                [filteredObjs setValue:value forKey:@"startdate"];
            }else if ([key isEqualToString:@"enddate"]) {
                [filteredObjs setValue:value forKey:@"enddate"];
            }else if ([key isEqualToString:@"isactive"]) {
                [filteredObjs setValue:value forKey:@"isactive"];
            }else if ([key isEqualToString:@"slug"]) {
                [filteredObjs setValue:value forKey:@"slug"];
            }else if ([key isEqualToString:@"title"]) {
                [filteredObjs setValue:value forKey:@"title"];
            }
            
        }
        return [[self alloc] initWithProperties:filteredObjs];
    }
    else
    {
        return nil;
    }
}

- (Campaign *)initWithProperties:(NSDictionary *)properties {
    if (self = [self init]) {
        @try {
            [self setValuesForKeysWithDictionary:properties];
        }
        @catch (NSException *ex) {
            return nil;
        }
    }
    return self;
}

@end


@interface Genres ()

@end

@implementation Genres


+ (id)customClassWithProperties:(NSDictionary *)properties {
    if(properties != nil)
    {
        NSMutableDictionary *filteredObjs = [[NSMutableDictionary alloc] init];
        for (NSString* key in properties) {
            id value = [properties objectForKey:key];
            if ([key isEqualToString:@"idgenre"]) {
                [filteredObjs setValue:value forKey:@"idgenre"];
            }
            else if ([key isEqualToString:@"imageUrl"]) {
                [filteredObjs setValue:value forKey:@"imageUrl"];
            }
            else if ([key isEqualToString:@"imageUrl_en"]) {
                [filteredObjs setValue:value forKey:@"imageUrl_en"];
            }
            else if ([key isEqualToString:@"imageUrl_es"]) {
                [filteredObjs setValue:value forKey:@"imageUrl_es"];
            }
            else if ([key isEqualToString:@"config"]) {
                [filteredObjs setValue:value forKey:@"config"];
            }
            else if ([key isEqualToString:@"gatag"]) {
                [filteredObjs setValue:value forKey:@"gatag"];
            }
            else if ([key isEqualToString:@"created"]) {
                [filteredObjs setValue:value forKey:@"created"];
            }
            else if ([key isEqualToString:@"genreorder"]) {
                [filteredObjs setValue:value forKey:@"genreorder"];
            }
            else if ([key isEqualToString:@"isactive"]) {
                [filteredObjs setValue:value forKey:@"isactive"];
            }
            else if ([key isEqualToString:@"name"]) {
                [filteredObjs setValue:value forKey:@"name"];
            }
            else if ([key isEqualToString:@"name_en"]) {
                [filteredObjs setValue:value forKey:@"name_en"];
            }
            else if ([key isEqualToString:@"name_es"]) {
                [filteredObjs setValue:value forKey:@"name_es"];
            }
            else if ([key isEqualToString:@"name_fr"]) {
                [filteredObjs setValue:value forKey:@"name_fr"];
            }
            else if ([key isEqualToString:@"slug"]) {
                [filteredObjs setValue:value forKey:@"slug"];
            }
            else if ([key isEqualToString:@"type"]) {
                [filteredObjs setValue:value forKey:@"type"];
            }
            else if ([key isEqualToString:@"updated"]) {
                [filteredObjs setValue:value forKey:@"updated"];
                
            }
            else if ([key isEqualToString:@"hasvertical"]) {
                if(![value isKindOfClass:([NSNull class])]){
                [filteredObjs setValue:value forKey:@"hasVertical"];
                }
            }
            else if ([key isEqualToString:@"paid"]) {
                if(![value isKindOfClass:([NSNull class])]){
                    [filteredObjs setValue:value forKey:@"isPaid"];
                }
            }
            
        }
        return [[self alloc] initWithProperties:filteredObjs];
    }
    else
    {
        return nil;
    }
}

- (id)initWithProperties:(NSDictionary *)properties {
    if (self = [self init]) {
        @try {
            [self setValuesForKeysWithDictionary:properties];
            self.config = [GenreConfig customClassWithProperties:properties[@"config"]];
        }
        @catch (NSException *ex) {
            return nil;
        }
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone
{
    Genres *copy = [[Genres alloc] init];
    
    if (copy)
    {
        
        copy.idgenre = self.idgenre;
        copy.imageUrl_en = self.imageUrl_en;
        copy.imageUrl_es = self.imageUrl_es;
        copy.imageUrl = self.imageUrl;
        copy.config = self.config;
        copy.created = self.created;
        copy.gatag = self.gatag;
        copy.genreorder = self.genreorder;
        copy.genretype = self.genretype;
        copy.isactive = self.isactive;
        copy.name = self.name;
        copy.name_en = self.name_en;
        copy.name_es = self.name_es;
        copy.name_fr = self.name_fr;
        copy.slug = self.slug;
        copy.slug = self.slug;
        copy.updated = self.updated;
        copy.templates = [self.templates mutableCopy];
        copy.hasVertical = self.hasVertical;
    }
    
    return copy;
}

//-(Genres *)getGenreWithIdGenre:(int)idGenre usingWSConfig:(WSConfig *)config {
//    return nil;
//}

+(NSArray *)getGenresArray {
    @try {
        //appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        sfAppConfig = (StayfilmApp *)[StayfilmApp sharedStayfilmAppSingleton];
        if(sfAppConfig.sfConfig != nil && sfAppConfig.sfConfig.genres != nil)
        {
            return sfAppConfig.sfConfig.genres;
        }
    }
    @catch (NSException *ex) {
        return nil;
    }
}

+ (id)getGenreByID:(int)p_idgenre {
    sfAppConfig = (StayfilmApp *)[StayfilmApp sharedStayfilmAppSingleton];
    for (Genres* g in sfAppConfig.sfConfig.genres) {
        if(((int)g.idgenre) == p_idgenre) {
            return g;
        }
    }
    return nil;
}

@end


@interface Template ()

@end

@implementation Template


+ (id)customClassWithProperties:(NSDictionary *)properties {
    if(properties != nil)
    {
        NSMutableDictionary *filteredObjs = [[NSMutableDictionary alloc] init];
        for (NSString* key in properties) {
            id value = [properties objectForKey:key];
            if ([key isEqualToString:@"idtemplate"]) {
                [filteredObjs setValue:value forKey:@"idtemplate"];
            }
            else if ([key isEqualToString:@"isactive"]) {
                [filteredObjs setValue:value forKey:@"isactive"];
            }
            else if ([key isEqualToString:@"data"]) {
                [filteredObjs setValue:value forKey:@"data"];
            }
            else if ([key isEqualToString:@"image"]) {
                [filteredObjs setValue:value forKey:@"image"];
            }
            else if ([key isEqualToString:@"thumbnail"]) {
                [filteredObjs setValue:value forKey:@"thumbnail"];
            }
            else if ([key isEqualToString:@"idgenre"]) {
                [filteredObjs setValue:value forKey:@"idgenre"];
            }
            else if ([key isEqualToString:@"isactive"]) {
                [filteredObjs setValue:value forKey:@"isactive"];
            }
            else if ([key isEqualToString:@"name"]) {
                [filteredObjs setValue:value forKey:@"name"];
            }
            else if ([key isEqualToString:@"paid"]) {
                if(![value isKindOfClass:([NSNull class])]){
                    [filteredObjs setValue:value forKey:@"isPaid"];
                }
            }
        }
        return [[self alloc] initWithProperties:filteredObjs];
    }
    else
    {
        return nil;
    }
}

- (id)initWithProperties:(NSDictionary *)properties {
    if (self = [self init]) {
        @try {
            [self setValuesForKeysWithDictionary:properties];
        }
        @catch (NSException *ex) {
            return nil;
        }
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone
{
    Template *copy = [[Template alloc] init];
    
    if (copy)
    {
        copy.idtemplate = self.idtemplate;
        copy.isactive = self.isactive;
        copy.data = self.data;
        copy.name = self.name;
        copy.image = self.image;
        copy.thumbnail = self.thumbnail;
        copy.idgenre = self.idgenre;
    }
    
    return copy;
}

@end
