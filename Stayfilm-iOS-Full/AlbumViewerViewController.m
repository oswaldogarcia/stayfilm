//
//  AlbumViewerViewController.m
//  fbmessenger
//
//  Created by Henrique Ormonde on 13/07/15.
//  Copyright (c) 2015 Stayfilm. All rights reserved.
//

#import "AlbumViewerViewController.h"
#import "PhotoCollectionViewCell.h"
#import "MovieMakerStep1ViewController.h"
#import "PlayerViewController.h"
#import "StayfilmApp.h"
#import "Uploader.h"
#import <Google/Analytics.h>
#import "UIView+Toast.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#include <stdlib.h>
#include "AlbumsListViewController.h"

@interface AlbumViewerViewController ()<AlbumsDelegate>

@property (nonatomic, strong) NSArray *initialSelectedMedias;
@property (nonatomic, weak) StayfilmApp *sfApp;
@property (nonatomic, strong) Uploader* uploaderStayfilm;

@property (nonatomic, strong) UIViewController *modalPreview;
@property (nonatomic, assign) BOOL isLongPressing;
//@property (nonatomic, assign) BOOL isAllSelected;

@property (nonatomic, assign) BOOL isEdited;

@property (nonatomic , strong) PHCachingImageManager *imageManager;

@property (nonatomic, assign) unsigned int viewCount;

@end

@implementation AlbumViewerViewController

@synthesize selectedMedias, albumFB, albumGeneric, photosCollectionView, thumbConnection, modalPreview, imageManager, txt_Title, but_back, sfApp;
@synthesize view_Editing, but_edit_Back, but_edit_Cancel, but_edit_Confirm;

static NSString *cellIdentifierPhoto = @"PhotoItemCell";

- (void)viewDidLoad {
    [super viewDidLoad];

    self.automaticallyAdjustsScrollViewInsets = NO;
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    self.thumbConnection = [NSURLSession sessionWithConfiguration:config];
    
    //clear Back Button text
//    [self.navigationItem.backBarButtonItem setTitle:@""];
//    [self.navigationController.navigationItem.backBarButtonItem setTitle:@""];
//    [self.navigationController.navigationBar.backItem setTitle:@""];
    self.navigationController.navigationBar.topItem.title = @" ";
    
    if(self.albumFB != nil){
        self.txt_Title.text = self.albumFB.name;
        [self.albumsButton setHidden:YES];
    }
    if(self.albumGeneric != nil) {
        self.txt_Title.text = self.albumGeneric.title;
        self.imageManager = [[PHCachingImageManager alloc] init];
        [self.albumsButton setHidden:NO];
    }
    
    self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    self.uploaderStayfilm = [Uploader sharedUploadStayfilmSingletonWithDelegate:self];
    
    self.initialSelectedMedias = [self.selectedMedias copy];
    
    [self.photosCollectionView registerClass:[PhotoCollectionViewCell class] forCellWithReuseIdentifier:cellIdentifierPhoto];
    
    self.isLongPressing = NO;
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = 1; //seconds
    lpgr.delegate = self;
    [self.photosCollectionView addGestureRecognizer:lpgr];
    
    if (self.selectedMedias.count > 0) {
        if(!self.sfApp.isEditingMode)
            [self.sfApp.filmStrip enableButton];
        [self updateFilmStrip];
    }
    
    self.viewCount = 0;
    
    if(self.sfApp.isEditingMode) {
        [self.view_Editing setHidden:NO];
        [self.but_edit_Back setHidden:NO];
        [self.but_edit_Cancel setHidden:YES];
        [self.but_edit_Confirm setHidden:YES];
        [self.but_back setHidden:YES];
        [self.view bringSubviewToFront:self.view_Editing];
    } else {
        [self.but_back setHidden:NO];
        [self.view_Editing setHidden:YES];
    }
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Disable swipe gesture to go back to previous viewcontroller
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
    self.uploaderStayfilm = [Uploader sharedUploadStayfilmSingletonWithDelegate:self];
    
    [self.sfApp.filmStrip.but_filmStrip removeTarget:nil action:nil forControlEvents:UIControlEventAllEvents];
    [self.sfApp.filmStrip.but_filmStrip addTarget:self action:@selector(but_filmStrip_Clicked) forControlEvents:UIControlEventTouchUpInside];
    [self.sfApp.filmStrip.but_NextStep removeTarget:nil action:nil forControlEvents:UIControlEventAllEvents];
    [self.sfApp.filmStrip.but_NextStep addTarget:self action:@selector(but_NextStep_Clicked) forControlEvents:UIControlEventTouchUpInside];
    
    if(!self.sfApp.isEditingMode) {
        if(self.sfApp.finalMedias.count > 0) {
            [self.sfApp.filmStrip setHidden:NO];
            [self.sfApp.filmStrip enableButton];
        }
    }
    
    if(self.albumGeneric != nil) {
        [self performSelectorInBackground:@selector(startCacheImages) withObject:nil];
    }
    
    self.viewCount++;
    
    [self updateFilmStrip];
    
    [self.sfApp.filmStrip connectivityViewUpdate];
    
    if(self.albumGeneric != nil && self.albumFB == nil) {
        //Google Analytics
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:@"moviemaker_content_camera-roll"];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    } else {
        //Google Analytics
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:@"moviemaker_content_facebook_albuns"];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    }
    
}

- (void)startCacheImages {
    PHImageRequestOptions *cropToSquare = [[PHImageRequestOptions alloc] init];
//    cropToSquare.resizeMode = PHImageRequestOptionsResizeModeExact;
    cropToSquare.resizeMode = PHImageRequestOptionsResizeModeFast;
    cropToSquare.networkAccessAllowed = YES;
    cropToSquare.synchronous = YES;
    
    //NSInteger retinaScale = [UIScreen mainScreen].scale;
    //CGSize retinaSquare = CGSizeMake(123.0*retinaScale, 123.0*retinaScale);
    CGSize retinaSquare = CGSizeMake(123,123);
    
    [self.imageManager startCachingImagesForAssets:self.albumGeneric.medias targetSize:retinaSquare contentMode:PHImageContentModeAspectFill options:cropToSquare];
}

- (void)handlePopGesture:(UIGestureRecognizer *)gesture
{
    if (gesture.state == UIGestureRecognizerStateBegan)
    {
        @try {
            
            if(![StayfilmApp isArray:self.selectedMedias equalToArray:self.initialSelectedMedias])
            {
                for (UIViewController *view in self.navigationController.viewControllers) {
                    if([view isKindOfClass:[MovieMakerStep1ViewController class]])
                    {
                        if(self.albumGeneric != nil) {
                            [view performSelectorOnMainThread:@selector(pickedSelectedMedias:) withObject:self.selectedMedias waitUntilDone:YES];
                        } else {
                            [view performSelectorOnMainThread:@selector(changeSelectedMedias:) withObject:self.selectedMedias waitUntilDone:YES];
                            [view performSelectorOnMainThread:@selector(reloadAlbumsCounters) withObject:nil waitUntilDone:NO];
                        }
                        break;
                    }
                }
                
                [self.selectedMedias removeAllObjects];
                self.selectedMedias = nil;
                self.albumFB = nil;
                self.albumGeneric = nil;
                [self.photosCollectionView removeFromSuperview];
                self.photosCollectionView = nil;
                self.thumbConnection = nil;
                //            self.img_SelectAll.image = nil;
                //            self.txt_SelectAll = nil;
                [self.navigationController popViewControllerAnimated:YES];
            }
            else
            {
                [self.selectedMedias removeAllObjects];
                self.selectedMedias = nil;
                self.albumFB = nil;
                self.albumGeneric = nil;
                [self.photosCollectionView removeFromSuperview];
                self.photosCollectionView = nil;
                self.thumbConnection = nil;
                //            self.txt_SelectAll = nil;
                //            self.img_SelectAll = nil;
                
                for (UIViewController *view in self.navigationController.viewControllers) {
                    if([view isKindOfClass:[MovieMakerStep1ViewController class]])
                    {
                        [view performSelectorOnMainThread:@selector(clearAlbumViewController) withObject:nil waitUntilDone:NO];
                        break;
                    }
                }
                
                [self.navigationController popViewControllerAnimated:YES];
            }
            
        }
        @catch (NSException *exception) {
            NSLog(@"StayLog: EXCEPTION on backButtonPressed with error: %@", exception.description);
        }
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.hidden = YES;

}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
   
    [self.sfApp.filmStrip connectivityViewUpdate];
}

-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getAllPhotosFromFacebookAlbum{
    
    //AlbumSN *album = (AlbumSN*)self.albumFB;
    __weak typeof(self) selfDelegate = self;
    
    BOOL isAdding = (self.albumFB.after != nil);
    NSString *url = [NSString stringWithFormat:@"/%@/photos" , [self.albumFB idAlbum]];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:@{@"fields": @"id, source, width, height, images",
                                                                                    @"limit" : @"50" }];
    if(isAdding){
        [params setValue:self.albumFB.after forKey:@"after"];
    }
    
    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                  initWithGraphPath:url
                                  parameters:params
                                  HTTPMethod:@"GET"];
    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                          id result,
                                          NSError *error) {
        if(error)
        {
            NSLog(@"StayLog: Request for photos in Facebook Album failed. FBSDKGraphAPI error: %@", error.description);
           
        }else {
            @try {
                if(result[@"data"] != nil)
                {
                    if(!isAdding){
                        [[selfDelegate.albumFB  idMedias] removeAllObjects];
                    }
                    
                    for(NSDictionary *var in result[@"data"]) {
                        [[selfDelegate.albumFB  idMedias] addObject:[PhotoFB customClassWithProperties:var]];
                    }
                    [self realoadAlbum];
                    
                    if(result[@"paging"] != nil) {
                        if(result[@"paging"][@"next"] != nil && result[@"paging"][@"cursors"][@"after"] != nil) {
                            
                            self.albumFB.after = result[@"paging"][@"cursors"][@"after"];
                             [self getAllPhotosFromFacebookAlbum];
                        }
                        else {
                            NSLog(@"StayLog: Finished reading album: %@",self.albumFB.name);
                        }
                    }
                
                }
            }
            @catch (NSException *exception) {
                NSLog(@"StayLog: EXCEPTION in readAllPhotosFromFacebookAlbum with error: %@", exception.description);
            }
        }
    }];
    
    
    
}

#pragma mark - Stayfilm methods

-(void)setEditedMode {
    
    if(!self.isEdited) {
        self.isEdited = YES;
        self.but_edit_Cancel.alpha = 0.0;
        self.but_edit_Confirm.alpha = 0.0;
        [self.but_edit_Confirm setHidden:NO];
        [self.but_edit_Cancel setHidden:NO];
        self.but_edit_Back.alpha = 1.0;
        
        [UIView animateWithDuration:0.4 animations:^{
            self.but_edit_Cancel.alpha = 1.0;
            self.but_edit_Confirm.alpha = 1.0;
            self.but_edit_Back.alpha = 0.0;
        } completion:^(BOOL finished) {
            [self.but_edit_Back setHidden:YES];
            [self.but_edit_Confirm setHidden:NO];
            [self.but_edit_Cancel setHidden:NO];
            [self.sfApp.filmStrip.viewAll setHidden:YES];
            [self.view bringSubviewToFront:self.but_edit_Confirm];
        }];
    } else {
        self.but_edit_Cancel.alpha = 1.0;
        self.but_edit_Confirm.alpha = 1.0;
        [self.but_edit_Confirm setHidden:NO];
        [self.but_edit_Cancel setHidden:NO];
        self.but_edit_Back.alpha = 0.0;
        [self.but_edit_Back setHidden:YES];
    }
}

-(void)setUneditedMode {
    
    if(self.isEdited) {
        self.isEdited = NO;
        self.but_edit_Back.alpha = 0.0;
        [self.but_edit_Back setHidden:NO];
        self.but_edit_Cancel.alpha = 1.0;
        self.but_edit_Confirm.alpha = 1.0;
        
        [UIView animateWithDuration:0.4 animations:^{
            self.but_edit_Cancel.alpha = 0.0;
            self.but_edit_Confirm.alpha = 0.0;
            self.but_edit_Back.alpha = 1.0;
        } completion:^(BOOL finished) {
            [self.but_edit_Confirm setHidden:YES];
            [self.but_edit_Cancel setHidden:YES];
            [self.but_edit_Back setHidden:NO];
        }];
    } else {
        self.but_edit_Cancel.alpha = 0.0;
        self.but_edit_Confirm.alpha = 0.0;
        [self.but_edit_Confirm setHidden:YES];
        [self.but_edit_Cancel setHidden:YES];
        self.but_edit_Back.alpha = 1.0;
        [self.but_edit_Back setHidden:NO];
    }
}

-(void)dismissModalView
{
    [self dismissViewControllerAnimated:YES completion:nil];
    self.modalPreview = nil;
}
-(void)updateFilmStrip {
    runOnMainQueueWithoutDeadlocking(^{
        self.sfApp.filmStrip.manualCounter = (int)(self.sfApp.finalMedias.count + self.selectedMedias.count - self.initialSelectedMedias.count);
        if(!self.sfApp.isEditingMode) {
            if((self.sfApp.finalMedias.count + self.selectedMedias.count - self.initialSelectedMedias.count) > 0) {
                if((self.sfApp.finalMedias.count + self.selectedMedias.count - self.initialSelectedMedias.count) >= [self.sfApp.sfConfig.min_photos intValue]) {
                    
                    [self.sfApp.filmStrip updateFilmStripWithCounter:[NSString stringWithFormat:@"%lu", (unsigned long)(self.sfApp.finalMedias.count + self.selectedMedias.count - self.initialSelectedMedias.count)] andCondition:YES];
           
                }else {
                    [self.sfApp.filmStrip updateFilmStripWithCounter:[NSString stringWithFormat:@"%lu", (unsigned long)(self.sfApp.finalMedias.count + self.selectedMedias.count - self.initialSelectedMedias.count)] andCondition:NO];
                }
            } else if(self.viewCount > 1){
                [self.sfApp.filmStrip updateFilmStripWithCounter:[NSString stringWithFormat:@"%lu", (unsigned long)(self.sfApp.finalMedias.count + self.selectedMedias.count - self.initialSelectedMedias.count)] andCondition:NO];
            }
        } else { // editing mode
            if((self.sfApp.editedMedias.count + self.selectedMedias.count - self.initialSelectedMedias.count) > 0) {
                if((self.sfApp.editedMedias.count + self.selectedMedias.count - self.initialSelectedMedias.count) >= [self.sfApp.sfConfig.min_photos intValue]) {
                    [self.sfApp.filmStrip updateFilmStripWithCounter:[NSString stringWithFormat:@"%lu", (unsigned long)(self.sfApp.editedMedias.count + self.selectedMedias.count - self.initialSelectedMedias.count)] andCondition:YES];
                } else {
                    [self.sfApp.filmStrip updateFilmStripWithCounter:[NSString stringWithFormat:@"%lu", (unsigned long)(self.sfApp.editedMedias.count + self.selectedMedias.count - self.initialSelectedMedias.count)] andCondition:NO];
                }
            } else {
                [self.sfApp.filmStrip updateFilmStripWithCounter:[NSString stringWithFormat:@"%lu", (unsigned long)(self.sfApp.editedMedias.count + self.selectedMedias.count - self.initialSelectedMedias.count)] andCondition:NO];
            }
        }

    });
}

-(void)realoadAlbum {
    [self.photosCollectionView reloadData];
}

-(void)changeSelectedMedias {
    self.initialSelectedMedias = [self.sfApp.finalMedias mutableCopy];
    self.selectedMedias = [self.sfApp.finalMedias mutableCopy];
    [self.photosCollectionView reloadData];
    [self updateFilmStrip];
} 

-(id)checkCustomContainsMedia:(id)media {
    id found = nil;
    for (id m in self.selectedMedias) {
        if([m isKindOfClass:[PhotoFB class]] && [media isKindOfClass:[PhotoFB class]]) {
            if([((PhotoFB*)m).idPhoto isEqualToString:((PhotoFB*)media).idPhoto]) {
                found = m;
                break;
            }
        } else if ([m isKindOfClass:[VideoFB class]] && [media isKindOfClass:[VideoFB class]]) {
            if([((VideoFB*)m).idVideo isEqualToString:((VideoFB*)media).idVideo]) {
                found = m;
                break;
            }
        }
    }
    return found;
}

#pragma mark - Button Delegates

- (IBAction)but_back_Clicked:(id)sender {
    [self backButtonPressed];
}

- (void)backButtonPressed
{
    @try {
        
        // This will work for both cemera roll and facebook albums
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"back"
                                                              action:@" "
                                                               label:@""
                                                               value:@1] build]];
        
        
        if(![StayfilmApp isArray:self.selectedMedias equalToArray:self.initialSelectedMedias])
        {
            for (UIViewController *view in self.navigationController.viewControllers) {
                if([view isKindOfClass:[MovieMakerStep1ViewController class]])
                {
                    if(self.albumGeneric != nil) {
                        [view performSelectorOnMainThread:@selector(pickedSelectedMedias:) withObject:self.selectedMedias waitUntilDone:YES];
                    } else {
                        if(self.sfApp.isEditingMode) {
                            [view performSelectorOnMainThread:@selector(changeEditedMedias:) withObject:self.selectedMedias waitUntilDone:YES];
                        } else {
                            [view performSelectorOnMainThread:@selector(changeSelectedMedias:) withObject:self.selectedMedias waitUntilDone:YES];
                        }
                        [view performSelectorOnMainThread:@selector(reloadAlbumsCounters) withObject:nil waitUntilDone:NO];
                    }
                    break;
                }
            }
            
            [self.selectedMedias removeAllObjects];
            self.selectedMedias = nil;
            self.albumFB = nil;
            self.albumGeneric = nil;
            [self.photosCollectionView removeFromSuperview];
            self.photosCollectionView = nil;
            self.thumbConnection = nil;
            //            self.img_SelectAll.image = nil;
            //            self.txt_SelectAll = nil;
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            [self.selectedMedias removeAllObjects];
            self.selectedMedias = nil;
            self.albumFB = nil;
            self.albumGeneric = nil;
            [self.photosCollectionView removeFromSuperview];
            self.photosCollectionView = nil;
            self.thumbConnection = nil;
            //            self.txt_SelectAll = nil;
            //            self.img_SelectAll = nil;
            
            for (UIViewController *view in self.navigationController.viewControllers) {
                if([view isKindOfClass:[MovieMakerStep1ViewController class]])
                {
                    [view performSelectorOnMainThread:@selector(clearAlbumViewController) withObject:nil waitUntilDone:NO];
                    break;
                }
            }
            
            [self.navigationController popViewControllerAnimated:YES];
        }
        
    }
    @catch (NSException *exception) {
        NSLog(@"StayLog: EXCEPTION on backButtonPressed with error: %@", exception.description);
    }
}

- (IBAction)confirmSelection_Clicked:(id)sender {
    
    for (UIViewController *view in self.navigationController.viewControllers) {
        if([view isKindOfClass:[MovieMakerStep1ViewController class]])
        {
            [view performSelectorOnMainThread:@selector(changeEditedMedias:) withObject:self.selectedMedias waitUntilDone:YES];
            
            //Google Analytics Event
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"edit-film"
                                                                  action:@"add-media"
                                                                   label:@"ok"
                                                                   value:@1] build]];
            
            [self.navigationController popViewControllerAnimated:NO];
            [view performSelectorOnMainThread:@selector(dismissFromAlbumViewer) withObject:nil waitUntilDone:NO];
            //[view performSelectorOnMainThread:@selector(reloadAlbumsCounters) withObject:nil waitUntilDone:NO];
            break;
        }
    }
    
    [self.selectedMedias removeAllObjects];
    self.selectedMedias = nil;
    self.albumFB = nil;
    self.albumGeneric = nil;
    [self.photosCollectionView removeFromSuperview];
    self.photosCollectionView = nil;
    self.thumbConnection = nil;
    //    self.img_SelectAll.image = nil;
    //    self.txt_SelectAll = nil;
    //[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)but_edit_Cancel_tapped:(id)sender {
    @try {
        [self.selectedMedias removeAllObjects];
        self.selectedMedias = nil;
        self.albumFB = nil;
        self.albumGeneric = nil;
        [self.photosCollectionView removeFromSuperview];
        self.photosCollectionView = nil;
        self.thumbConnection = nil;
        
        for (UIViewController *view in self.navigationController.viewControllers) {
            if([view isKindOfClass:[MovieMakerStep1ViewController class]])
            {
                [view performSelectorOnMainThread:@selector(clearAlbumViewController) withObject:nil waitUntilDone:NO];
                break;
            }
        }
        
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"edit-film"
                                                              action:@"add-media"
                                                               label:@"cancel"
                                                               value:@1] build]];
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    @catch (NSException *exception) {
        NSLog(@"StayLog: EXCEPTION on but_edit_Cancel_tapped with error: %@", exception.description);
    }
}

BOOL isMovingStrip = NO;
- (IBAction)but_filmStrip_Clicked {
    if(!isMovingStrip) {
        isMovingStrip = YES;
        
        if(self.albumGeneric != nil && self.albumFB == nil) {
            //Google Analytics Event
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"film-strip"
                                                                  action:@"camera-roll"
                                                                   label:@""
                                                                   value:@1] build]];
        } else {
            //Google Analytics Event
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"film-strip"
                                                                  action:@"facebook_albuns"
                                                                   label:@""
                                                                   value:@1] build]];
        }
        
        
        [self performSegueWithIdentifier:@"AlbumToConfirmationSegue" sender:self];
        isMovingStrip = NO;
    }
}

- (void)but_NextStep_Clicked {
    if(!isMovingStrip) {
        isMovingStrip = YES;
        
        if(self.albumGeneric != nil && self.albumFB == nil) {
            //Google Analytics Event
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"film-production"
                                                                  action:@"next"
                                                                   label:@"camera-roll"
                                                                   value:@1] build]];
        } else {
            //Google Analytics Event
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"film-production"
                                                                  action:@"next"
                                                                   label:@"facebook_albuns"
                                                                   value:@1] build]];
        }
        if(self.sfApp.selectedGenre != nil && self.sfApp.selectedTemplate != nil && self.sfApp.canSkipStyles){
            [self.sfApp.filmStrip setHidden:YES];
            [self performSegueWithIdentifier:@"AlbumToProgressSegue" sender:self];
        }else{
//            [self performSegueWithIdentifier:@"AlbumToStyleSegue" sender:self];
            [self performSegueWithIdentifier:@"AlbumToConfirmationSegue" sender:self];
        }
        
        isMovingStrip = NO;
    }
}

-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if(!self.isLongPressing)
    {
        self.isLongPressing = YES;
        CGPoint p = [gestureRecognizer locationInView:self.photosCollectionView];
        
        NSIndexPath *indexPath = [self.photosCollectionView indexPathForItemAtPoint:p];
        
        if (indexPath == nil) {
            //            NSLog(@"long press on table view but not on a row");
        } else if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
            //            NSLog(@"long press on table view at row %d", indexPath.item);
            
            self.modalPreview = nil;
            UIImageView *imageView = nil;
            
            if(self.albumFB != nil) {
                self.modalPreview = [[UIViewController alloc] init];
                
                self.modalPreview.view.backgroundColor = [UIColor whiteColor];
                self.modalPreview.view.userInteractionEnabled = YES;
                
                imageView = [[UIImageView alloc] initWithFrame:self.modalPreview.view.frame];
                imageView.contentMode = UIViewContentModeScaleAspectFit;
                
                if([[[self.albumFB.idMedias objectAtIndex:indexPath.item] source] containsString:@".mp4"] ||
                   [[[self.albumFB.idMedias objectAtIndex:indexPath.item] source] containsString:@".mpg"] ||
                   [[[self.albumFB.idMedias objectAtIndex:indexPath.item] source] containsString:@".mov"] ||
                   [[[self.albumFB.idMedias objectAtIndex:indexPath.item] source] containsString:@".avi"])
                {
                    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"Player" owner:self options:nil];
                    PlayerViewController *moviePlayerViewController = [topLevelObjects objectAtIndex:0];
                    moviePlayerViewController.currentMovie = nil;
                    moviePlayerViewController.videoUrl = [[self.albumFB.idMedias objectAtIndex:indexPath.item] source];
                    //moviePlayerViewController.player = [AVPlayer playerWithURL:[NSURL URLWithString:[[self.albumFB.idMedias objectAtIndex:indexPath.item] source]]];
                    
                    [self presentViewController:moviePlayerViewController animated:YES completion:nil];
                    //[self performSegueWithIdentifier: @"PlayMyMovieSegue" sender:[[self.albumFB.idMedias objectAtIndex:indexPath.item] source]];
                    self.isLongPressing = NO;
                    return;
                }
                else {
                    if([self.sfApp.imagesCache objectForKey:[[self.albumFB.idMedias objectAtIndex:indexPath.item] source]] != nil)
                    {
                        imageView.image = [self.sfApp.imagesCache objectForKey:[[self.albumFB.idMedias objectAtIndex:indexPath.item] source]];
                    }
                    else
                    {
                        [imageView sd_setImageWithURL:[NSURL URLWithString:[[self.albumFB.idMedias objectAtIndex:indexPath.item] source]] placeholderImage:nil completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                            if(error)
                            {
                                NSLog(@"StayLog: PhotoCollectionImage request error: %@", error);
                            }else{
                                
                                [imageView setImage:image];
                                
                                if(imageView.image != nil){
                                    [self.sfApp.imagesCache setObject:imageView.image forKey:[[self.albumFB.idMedias objectAtIndex:indexPath.item] source]];
                                }
                            }
                        }];
                        
                    }
                }
            }
            if(self.albumGeneric != nil) {
                if([self.albumGeneric isKindOfClass:[AlbumInternal class]]) {
                    PHAsset * asset = (PHAsset *)[((AlbumInternal *)self.albumGeneric).medias objectAtIndex:indexPath.item];
                    if(asset.mediaType == PHAssetMediaTypeVideo) {
                        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"Player" owner:self options:nil];
                        PlayerViewController *moviePlayerViewController = [topLevelObjects objectAtIndex:0];
                        
                        
                        moviePlayerViewController.assetMovie = asset;
                        moviePlayerViewController.currentMovie = nil;
                        moviePlayerViewController.videoUrl = nil;
                        
                        
                        PHVideoRequestOptions *options = [[PHVideoRequestOptions alloc] init];
                        options.deliveryMode = PHVideoRequestOptionsDeliveryModeFastFormat;
                        [options setNetworkAccessAllowed:YES];
                        
                        if(asset.mediaSubtypes ==  PHAssetMediaSubtypeVideoHighFrameRate ){
                            
                            [self.view makeToastActivity:CSToastPositionCenter];
                            
                            
                            [[PHImageManager defaultManager] requestAVAssetForVideo:asset options:options resultHandler:^(AVAsset * avasset, AVAudioMix * audioMix, NSDictionary * info) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                                    NSString *documentsDirectory = paths.firstObject;
                                    NSString *myPathDocs =  [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"video-%d.mov",arc4random() % 1000]];
                                    NSURL *url = [NSURL fileURLWithPath:myPathDocs];
                                    
                                    AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:avasset presetName:AVAssetExportPreset640x480];
                                    exporter.outputURL = url;
                                    exporter.outputFileType = AVFileTypeQuickTimeMovie;
                                    exporter.shouldOptimizeForNetworkUse = YES;
                                    
                                    [exporter exportAsynchronouslyWithCompletionHandler:^{
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            if (exporter.status == AVAssetExportSessionStatusCompleted) {
                                                NSURL *URL = exporter.outputURL;
                                                moviePlayerViewController.player = [AVPlayer playerWithURL:URL];
                                                
                                                [self.view hideToastActivity];
                                                [self presentViewController:moviePlayerViewController animated:YES completion:nil];
                                                self.isLongPressing = NO;
                                            }
                                        });
                                    }];
                                });
                            }];
                        }else{
                            if(self.imageManager == nil)
                                self.imageManager = [[PHCachingImageManager alloc] init];
                            [self.imageManager requestAVAssetForVideo:asset options:options resultHandler:^(AVAsset * _Nullable asset, AVAudioMix * _Nullable audioMix, NSDictionary * _Nullable info) {
                                moviePlayerViewController.player = [AVPlayer playerWithURL:((AVURLAsset *)asset).URL];
                                
                                [self presentViewController:moviePlayerViewController animated:YES completion:nil];
                                self.isLongPressing = NO;
                            }];
                            
                        }
                        return;
                    }else{
                        
                        self.modalPreview = [[UIViewController alloc] init];
                        self.modalPreview.view.backgroundColor = [UIColor whiteColor];
                        self.modalPreview.view.userInteractionEnabled = YES;
                        
                        imageView = [[UIImageView alloc] initWithFrame:self.modalPreview.view.frame];
                        imageView.contentMode = UIViewContentModeScaleAspectFit;
                        
                        [self.imageManager requestImageForAsset:asset targetSize:imageView.frame.size contentMode:PHImageContentModeAspectFill options:nil resultHandler:^(UIImage *result, NSDictionary *info){
                             imageView.image = result;
                         }];
                    }
                }
                
            }
            
            if(imageView != nil) {
                [self.modalPreview.view addSubview:imageView];
                
                UITapGestureRecognizer *modalTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissModalView)];
                [self.modalPreview.view addGestureRecognizer:modalTap];
                
                self.modalPreview.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                self.modalPreview.modalPresentationStyle = UIModalPresentationFullScreen;
                
                [self presentViewController:self.modalPreview animated:YES completion:nil];
            }
        } else {
            //            NSLog(@"gestureRecognizer.state = %d", gestureRecognizer.state);
        }
        self.isLongPressing = NO;
    }
}


#pragma mark - Collection Delegates

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {

    [(UIActivityIndicatorView *)[self.view viewWithTag:10] stopAnimating];
    if(self.albumFB != nil)
        return [self.albumFB.idMedias count];
    if(self.albumGeneric != nil && [self.albumGeneric isKindOfClass:[AlbumInternal class]])
        return [((AlbumInternal *)self.albumGeneric).medias count];
    return 0;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PhotoCollectionViewCell *cell = (PhotoCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifierPhoto forIndexPath:indexPath];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PhotoItem" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    if(cell.task)
    {
        [cell.task cancel];
    }
    
    cell.photoImageView.image = nil;
    [cell.spinner startAnimating];
    [cell.spinner setHidden:NO];
    cell.photoImageView.alpha = 1.0;
    
    if(self.albumFB != nil) {
        if([self.selectedMedias containsObject:[self.albumFB.idMedias objectAtIndex:indexPath.item]] || [self checkCustomContainsMedia:[self.albumFB.idMedias objectAtIndex:indexPath.item]] != nil)
        { // Photo selected
            [cell.selectedView setHidden:NO];
            [cell.unselectedView setHidden:YES];
            cell.photoImageView.alpha = 0.7;
        }
        else
        {
            [cell.selectedView setHidden:YES];
            [cell.unselectedView setHidden:NO];
        }
        cell.source = [[self.albumFB.idMedias objectAtIndex:indexPath.item] source];
        
        if(self.sfApp == nil)
        {
            self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
        }
        
        if([self.sfApp.imagesCache objectForKey:cell.source] != nil)
        {
            cell.photoImageView.image = [self.sfApp.imagesCache objectForKey:cell.source];
            [cell.spinner stopAnimating];
            if([cell.source containsString:@".mp4"] ||
               [cell.source containsString:@".avi"] ||
               [cell.source containsString:@".mpg"] ||
               [cell.source containsString:@".mov"] )
            {
                [cell.vid_Play setHidden:NO];
                [cell.vid_Gradient setHidden:NO];
                
                if([[self.albumFB.idMedias objectAtIndex:indexPath.item] isKindOfClass:[VideoFB class]]) {
                    [cell.vid_Time setHidden:NO];
                    cell.duration = ((VideoFB*)[self.albumFB.idMedias objectAtIndex:indexPath.item]).length;
                    long seconds = lroundf([cell.duration floatValue]);
                    int hour = (int)seconds / 3600;
                    int mins = (seconds % 3600) / 60;
                    int secs = seconds % 60;
                    if(hour > 0) {
                        cell.vid_Time.text = [NSString stringWithFormat:@"%d:%02d:%02d",hour,mins,secs];
                    } else {
                        cell.vid_Time.text = [NSString stringWithFormat:@"%d:%02d",mins,secs];
                    }
                }
                else {
                    [cell.vid_Time setHidden:NO];
                    AVAsset *asset = [AVAsset assetWithURL:[NSURL URLWithString:cell.source]];
                    cell.duration = [NSNumber numberWithFloat:lroundf(CMTimeGetSeconds(asset.duration))];
                    long seconds = lroundf([cell.duration floatValue]);
                    int hour = (int)seconds / 3600;
                    int mins = (seconds % 3600) / 60;
                    int secs = seconds % 60;
                    if(hour > 0) {
                        cell.vid_Time.text = [NSString stringWithFormat:@"%d:%02d:%02d",hour,mins,secs];
                    } else {
                        cell.vid_Time.text = [NSString stringWithFormat:@"%d:%02d",mins,secs];
                    }
                }
            }
            else {
                [cell.vid_Play setHidden:YES];
                [cell.vid_Gradient setHidden:YES];
                [cell.vid_Time setHidden:YES];
            }
        }
        else
        {
            __weak typeof(self) selfDelegate = self;
            if([cell.source containsString:@".mp4"] ||
               [cell.source containsString:@".avi"] ||
               [cell.source containsString:@".mpg"] ||
               [cell.source containsString:@".mov"] )
            {
                if([[self.albumFB.idMedias objectAtIndex:indexPath.item] isKindOfClass:[VideoFB class]]) {
                    [cell.vid_Time setHidden:NO];
                    [cell.vid_Play setHidden:NO];
                    [cell.vid_Gradient setHidden:NO];
                    cell.duration = ((VideoFB*)[self.albumFB.idMedias objectAtIndex:indexPath.item]).length;
                    long seconds = lroundf([cell.duration floatValue]);
                    int hour = (int)seconds / 3600;
                    int mins = (seconds % 3600) / 60;
                    int secs = seconds % 60;
                    if(hour > 0) {
                        cell.vid_Time.text = [NSString stringWithFormat:@"%d:%02d:%02d",hour,mins,secs];
                    } else {
                        cell.vid_Time.text = [NSString stringWithFormat:@"%d:%02d",mins,secs];
                    }
                   [cell.photoImageView sd_setImageWithURL:[NSURL URLWithString:((VideoFB*)[self.albumFB.idMedias objectAtIndex:indexPath.item]).picture] placeholderImage:nil completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                        if(error)
                        {
                            NSLog(@"StayLog: PhotoCollectionImage request error: %@", error);
                        }else{
                        [cell.photoImageView setImage:image];
                        
                        [selfDelegate.sfApp.imagesCache setObject:cell.photoImageView.image forKey:cell.source];
                        
                        [cell.spinner stopAnimating];
                        }
                    }];
                    
                    /*cell.task = [self.thumbConnection dataTaskWithURL:[NSURL URLWithString:((VideoFB*)[self.albumFB.idMedias objectAtIndex:indexPath.item]).picture] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                        if(error)
                        {
                            NSLog(@"StayLog: PhotoCollectionImage request error: %@", error);
                        }
                        else {
                            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                cell.photoImageView.image = [UIImage imageWithData:data];
                                if(cell.photoImageView.image != nil)
                                    [selfDelegate.sfApp.imagesCache setObject:cell.photoImageView.image forKey:cell.source];
                                [cell.spinner stopAnimating];
                            }];
                        }
                    }];
                    [cell.task resume];
                    [cell.spinner stopAnimating];*/
                }
                else {
                    AVAsset *asset = [AVAsset assetWithURL:[NSURL URLWithString:cell.source]];
                    AVAssetImageGenerator *generator = [AVAssetImageGenerator assetImageGeneratorWithAsset:asset];
                    generator.appliesPreferredTrackTransform = YES;
                    CMTime time = CMTimeMakeWithSeconds(1, 2);
                    NSError *error = nil;
                    @try {
                        CGImageRef img = [generator copyCGImageAtTime:time actualTime:nil error:&error];
                        if(!error)
                        {
                            UIImage *image = [UIImage imageWithCGImage:img];
                            cell.photoImageView.image = image;
                            if(cell.photoImageView.image != nil)
                                [selfDelegate.sfApp.imagesCache setObject:cell.photoImageView.image forKey:cell.source];
                        }
                        [cell.vid_Play setHidden:NO];
                        [cell.vid_Gradient setHidden:NO];
                        [cell.vid_Time setHidden:NO];
                        
                        cell.duration = [NSNumber numberWithFloat:lroundf(CMTimeGetSeconds(asset.duration))];
                        long seconds = lroundf([cell.duration floatValue]);
                        int hour = (int)seconds / 3600;
                        int mins = (seconds % 3600) / 60;
                        int secs = seconds % 60;
                        if(hour > 0) {
                            cell.vid_Time.text = [NSString stringWithFormat:@"%d:%02d:%02d",hour,mins,secs];
                        } else {
                            cell.vid_Time.text = [NSString stringWithFormat:@"%d:%02d",mins,secs];
                        }
                        [cell.spinner stopAnimating];
                    } @catch (NSException *ex) {
                        NSLog(@"StayLog: EXCEPTION getting video thumb with error: %@", ex.description);
                    }
                }
            }
            else {
                
                [cell.photoImageView sd_setImageWithURL:[NSURL URLWithString:cell.source] placeholderImage:nil completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                    if(error){
                        NSLog(@"StayLog: PhotoCollectionImage request error: %@", error);
                    }else{
                        [cell.photoImageView setImage:image];
                        
                        [selfDelegate.sfApp.imagesCache setObject:cell.photoImageView.image forKey:cell.source];
                        
                        [cell.spinner stopAnimating];
                    }
                    
                }];
               
                
                
                
             /* cell.task = [self.thumbConnection dataTaskWithURL:[NSURL URLWithString:cell.source] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    if(error)
                    {
                        NSLog(@"StayLog: PhotoCollectionImage request error: %@", error);
                    }
                    else {
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            cell.photoImageView.image = [UIImage imageWithData:data];
                            if(cell.photoImageView.image != nil)
                                [selfDelegate.sfApp.imagesCache setObject:cell.photoImageView.image forKey:cell.source];
                            [cell.spinner stopAnimating];
                        }];
                    }
                }];
                [cell.task resume];*/
                [cell.vid_Play setHidden:YES];
                [cell.vid_Gradient setHidden:YES];
                [cell.vid_Time setHidden:YES];
            }
        }
    }
    else if(self.albumGeneric != nil) {
        if([self.selectedMedias containsObject:[self.albumGeneric.medias objectAtIndex:indexPath.item]])
        { // Photo selected
            [cell.selectedView setHidden:NO];
            [cell.unselectedView setHidden:YES];
            cell.photoImageView.alpha = 0.7;
        }
        else
        {
            [cell.selectedView setHidden:YES];
            [cell.unselectedView setHidden:NO];
        }
        cell.source = nil;
        PHAsset * asset = (PHAsset *)[self.albumGeneric.medias objectAtIndex:indexPath.item];
       
        if(asset.mediaType == PHAssetMediaTypeVideo) {
            
            [cell.vid_Play setHidden:NO];
            [cell.vid_Time setHidden:NO];
            [cell.vid_Gradient setHidden:NO];
            
            long seconds = lroundf(asset.duration);
            int hour = (int)seconds / 3600;
            int mins = (seconds % 3600) / 60;
            int secs = seconds % 60;
            if(hour > 0) {
                cell.vid_Time.text = [NSString stringWithFormat:@"%d:%02d:%02d",hour,mins,secs];
            } else {
                cell.vid_Time.text = [NSString stringWithFormat:@"%d:%02d",mins,secs];
            }
        } else {
            [cell.vid_Play setHidden:YES];
            [cell.vid_Gradient setHidden:YES];
            [cell.vid_Time setHidden:YES];
        }
        
        PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
        options.resizeMode = PHImageRequestOptionsResizeModeFast;
        options.synchronous = YES;
        options.networkAccessAllowed = YES;
        
        CGSize imageSize = cell.photoImageView.frame.size;
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            [self.imageManager requestImageForAsset:asset targetSize:imageSize contentMode:PHImageContentModeAspectFill options:options resultHandler:^(UIImage *result, NSDictionary *info)
             {
                 
                 dispatch_sync(dispatch_get_main_queue(), ^{
                     
                     __block BOOL isICloudAsset = NO;
                     
                     if ([[info objectForKey: PHImageResultIsInCloudKey] boolValue]) {
                         isICloudAsset = YES;
                     }
                     if (!isICloudAsset){
                         cell.photoImageView.image = result;
                         [cell.spinner stopAnimating];
                     }
                 });
             }];
        });
    }
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    @try {
        PhotoCollectionViewCell *cell = (PhotoCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifierPhoto forIndexPath:indexPath];
        
        if(self.viewCount <= 1)
            self.viewCount++;
        
        long mediaCount;
        if(self.sfApp.isEditingMode){
            
            mediaCount = self.sfApp.editedMedias.count + self.selectedMedias.count - self.initialSelectedMedias.count;
            
        }else{
          
            mediaCount = self.sfApp.finalMedias.count + self.selectedMedias.count - self.initialSelectedMedias.count;
        }
        
        if(self.albumFB != nil) {
            
            if(![self.selectedMedias containsObject:[self.albumFB.idMedias objectAtIndex:indexPath.item]] && [self checkCustomContainsMedia:[self.albumFB.idMedias objectAtIndex:indexPath.item]] == nil)
            { // Selecting photo
                if(!(mediaCount >= [self.sfApp.sfConfig.max_photos intValue])){
                
                    [self.selectedMedias addObject:[self.albumFB.idMedias objectAtIndex:indexPath.item]];
                    mediaCount++;

                    [cell.selectedView setHidden:NO];
                    [cell.unselectedView setHidden:YES];
                    cell.photoImageView.alpha = 0.7;
                }
            }
            else // Deselect Photo
            {
//                if(self.sfApp.isEditingMode) {
                    [self.selectedMedias removeObject:[self checkCustomContainsMedia:[self.albumFB.idMedias objectAtIndex:indexPath.item]]];
//                } else {
                    [self.selectedMedias removeObject:[self.albumFB.idMedias objectAtIndex:indexPath.item]];
//                }
                mediaCount--;
                [cell.selectedView setHidden:YES];
                [cell.unselectedView setHidden:NO];
                cell.photoImageView.alpha = 1.0;
            }
        }
        if(self.albumGeneric != nil) {

            if(![self.selectedMedias containsObject:[self.albumGeneric.medias objectAtIndex:indexPath.item]])
            { // Selecting photo
                
                if(!(mediaCount >= [self.sfApp.sfConfig.max_photos intValue])){
                    mediaCount++;
                    [self.selectedMedias addObject:[self.albumGeneric.medias objectAtIndex:indexPath.item]];
                    [cell.selectedView setHidden:NO];
                    [cell.unselectedView setHidden:YES];
                    cell.photoImageView.alpha = 0.7;
                }
                
            }
            else // Deselect Photo
            {
                mediaCount--;
                [self.selectedMedias removeObject:[self.albumGeneric.medias objectAtIndex:indexPath.item]];
                [cell.selectedView setHidden:YES];
                [cell.unselectedView setHidden:NO];
                cell.photoImageView.alpha = 1.0;
                Uploader *uploader = [Uploader sharedUploadStayfilmSingleton];
                [uploader removeAssetFromUpload:[self.albumGeneric.medias objectAtIndex:indexPath.item]];
            }

        }
      
        self.sfApp.currentSelectionCount = (int)mediaCount;
        
        if(self.sfApp.isEditingMode) {
            if(![StayfilmApp isArray:self.selectedMedias equalToArray:self.initialSelectedMedias])
            {
                [self setEditedMode];
            } else {
                [self setUneditedMode];
            }
        } else {
            if((self.selectedMedias.count + self.initialSelectedMedias.count > 0) || self.sfApp.finalMedias.count > 0) {
                [self.sfApp.filmStrip enableButton];
            }
            else {
                if(self.viewCount <= 1)
                    [self.sfApp.filmStrip disableButton];
            }
        }
        [self updateFilmStrip];
        
        [self.photosCollectionView reloadItemsAtIndexPaths:@[indexPath]];
    }
    @catch (NSException *exception) {
        NSLog(@"StayLog: EXCEPTION didSelectItemAtPath with error: %@", exception.description);
    }
    
}


- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeMake(0, 0);
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    //Set cell size to fit all screens
    CGFloat width  = self.view.frame.size.width;
    width -= 6.0;
    width /= 3.0;
    return CGSizeMake(width,width);
}


#pragma mark - Uploader Delegates

-(void)uploadProgressChanged:(NSNumber *)progress
{
    // Not important on this page.
}

- (void)converterDidFailMedia:(NSString *)type
{
    NSString *message = nil;
    if(self.sfApp == nil)
    {
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    }
    
    if ( (self.sfApp.isEditingMode)? (self.sfApp.editedMedias.count < [self.sfApp.sfConfig.min_photos intValue]) : (self.sfApp.finalMedias.count < [self.sfApp.sfConfig.min_photos intValue]) ) {
        message = [NSString stringWithFormat:NSLocalizedString(@"UPLOADER_CONVERT_FAILED_UPLOAD_AGAIN_MESSAGE", nil), ((self.sfApp.isEditingMode)? ([self.sfApp.sfConfig.min_photos intValue] - self.sfApp.editedMedias.count) : ([self.sfApp.sfConfig.min_photos intValue] - self.sfApp.finalMedias.count))];
    }
    else if ([type isEqualToString:@"image"]) {
        message = NSLocalizedString(@"UPLOADER_CONVERT_FAILED_IMAGE_MESSAGE", nil);
    }
    else
    {
        message = NSLocalizedString(@"UPLOADER_CONVERT_FAILED_VIDEO_MESSAGE", nil);
    }
    
    if(self.sfApp.isEditingMode) {
        @synchronized (self.sfApp.editedMedias) {
            for (id obj in self.sfApp.editedMedias) {
                if([obj isKindOfClass:[PHAsset class]]) {
                    BOOL find = NO;
                    for (PHAsset *asset in self.uploaderStayfilm.listUploadMedias) {
                        if(asset == obj) {
                            find = YES;
                            break;
                        }
                    }
                    if(!find) {
                        NSUInteger i = [self.sfApp.editedMedias indexOfObject:obj];
                        [self.sfApp.editedMedias removeObjectAtIndex:i];
                        [self.photosCollectionView reloadData];
                        break;
                    }
                }
            }
        }
    } else {
        @synchronized (self.sfApp.finalMedias) {
            for (id obj in self.sfApp.finalMedias) {
                if([obj isKindOfClass:[PHAsset class]]) {
                    BOOL find = NO;
                    for (PHAsset *asset in self.uploaderStayfilm.listUploadMedias) {
                        if(asset == obj) {
                            find = YES;
                            break;
                        }
                    }
                    if(!find) {
                        NSUInteger i = [self.sfApp.finalMedias indexOfObject:obj];
                        [self.sfApp.finalMedias removeObjectAtIndex:i];
                        [self.photosCollectionView reloadData];
                        break;
                    }
                }
            }
        }
    }
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_CONVERT_FAILED_TITLE", nil)
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)uploadDidFailMedia:(NSString *)type
{
    NSString *message = nil;
    if(self.sfApp == nil)
    {
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    }
    
    if ( (self.sfApp.isEditingMode)? (self.sfApp.editedMedias.count < [self.sfApp.sfConfig.min_photos intValue]) : (self.sfApp.finalMedias.count < [self.sfApp.sfConfig.min_photos intValue]) ) {
        message = [NSString stringWithFormat:NSLocalizedString(@"UPLOADER_UPLOAD_FAILED_UPLOAD_AGAIN_MESSAGE", nil), ((self.sfApp.isEditingMode)? ([self.sfApp.sfConfig.min_photos intValue] - self.sfApp.editedMedias.count) : ([self.sfApp.sfConfig.min_photos intValue] - self.sfApp.finalMedias.count))];
    }
    else if ([type isEqualToString:@"image"]) {
        message = NSLocalizedString(@"UPLOADER_UPLOAD_FAILED_IMAGE_MESSAGE", nil);
    }
    else
    {
        message = NSLocalizedString(@"UPLOADER_UPLOAD_FAILED_VIDEO_MESSAGE", nil);
    }
    
    if(self.sfApp.isEditingMode) {
        @synchronized (self.sfApp.editedMedias) {
            for (id obj in self.sfApp.editedMedias) {
                if([obj isKindOfClass:[PHAsset class]]) {
                    BOOL find = NO;
                    for (PHAsset *asset in self.uploaderStayfilm.listUploadMedias) {
                        if(asset == obj) {
                            find = YES;
                            break;
                        }
                    }
                    if(!find) {
                        NSUInteger i = [self.sfApp.editedMedias indexOfObject:obj];
                        [self.sfApp.editedMedias removeObjectAtIndex:i];
                        [self.photosCollectionView reloadData];
                        break;
                    }
                }
            }
        }
    } else {
        @synchronized (self.sfApp.finalMedias) {
            for (id obj in self.sfApp.finalMedias) {
                if([obj isKindOfClass:[PHAsset class]]) {
                    BOOL find = NO;
                    for (PHAsset *asset in self.uploaderStayfilm.listUploadMedias) {
                        if(asset == obj) {
                            find = YES;
                            break;
                        }
                    }
                    if(!find) {
                        NSUInteger i = [self.sfApp.finalMedias indexOfObject:obj];
                        [self.sfApp.finalMedias removeObjectAtIndex:i];
                        [self.photosCollectionView reloadData];
                        break;
                    }
                }
            }
        }
    }
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_UPLOAD_FAILED_TITLE", nil)
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)uploadDidFail:(Uploader *)uploader
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_ERROR_TITLE", nil)
                                                                   message:NSLocalizedString(@"UPLOADER_ERROR_MESSAGE", nil)
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)uploadDidStartSendingConfirmationMedias:(Uploader *)uploader
{
    // not important on this page.
}

-(void)uploadDidFinish:(Uploader *)uploader
{
    if(self.sfApp == nil) {
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    }
    if(self.uploaderStayfilm.listUploadedMedias != nil && self.uploaderStayfilm.listUploadedMedias.count >0) {
        for (PHAsset * asset in self.uploaderStayfilm.listUploadedMedias.allValues) {
            if(self.sfApp.isEditingMode) {
                if(![self.sfApp.editedMedias containsObject:asset]) {
                    [self.sfApp.editedMedias addObject:asset];
                }
            } else {
                if(![self.sfApp.finalMedias containsObject:asset]) {
                    [self.sfApp.finalMedias addObject:asset];
                }
            }
        }
    }
    if(self.sfApp.isDebugMode) {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_FINISHED", nil)
                                                                       message:[[NSString alloc] initWithFormat:NSLocalizedString(@"UPLOADED_X_MEDIAS", nil), uploader.listUploadedMedias.count]
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    [self.photosCollectionView reloadData];
}

-(void)uploadFinishedMedia:(PHAsset *)asset {
    // not important on this view
}

-(void)uploadDidStart
{
    // not important on this view
}

- (IBAction)albumsAction:(id)sender {
    
    
    PHFetchOptions *options = [[PHFetchOptions alloc] init];
    options.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];
  
    
    NSArray *collectionsFetchResults;
    NSMutableArray *localizedTitles = [[NSMutableArray alloc] init];
    NSMutableArray *collections = [[NSMutableArray alloc] init];

    PHFetchResult *smartAlbums = [PHAssetCollection       fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum
                                                                                subtype:PHAssetCollectionSubtypeAlbumRegular options:nil];
    PHFetchResult *syncedAlbums = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum
                                                                           subtype:PHAssetCollectionSubtypeAlbumSyncedAlbum options:nil];
    PHFetchResult *userCollections = [PHCollectionList fetchTopLevelUserCollectionsWithOptions:nil];

    // Add each PHFetchResult to the array
    collectionsFetchResults = @[smartAlbums, userCollections, syncedAlbums];

    for (int i = 0; i < collectionsFetchResults.count; i ++) {

        PHFetchResult *fetchResult = collectionsFetchResults[i];

        for (int x = 0; x < fetchResult.count; x ++) {
            if(fetchResult[x] != nil){
                PHAssetCollection *collection = fetchResult[x];
                [collections addObject:collection];
                localizedTitles[x] = collection.localizedTitle;
            }
        }
    }

    
    
    AlbumsListViewController *albumsTableVC = [[AlbumsListViewController alloc]init];
    albumsTableVC.collectionsAlbums = collections;
    albumsTableVC.delegate = self;
    [self presentViewController:albumsTableVC animated:YES completion:nil];
    
 
    
}

- (void)albumSelected:(PHAssetCollection*)selected{
    
    AlbumInternal *album = [[AlbumInternal alloc] init];
    
    if(self.imageManager == nil)
    {
        self.imageManager = [[PHCachingImageManager alloc] init];
    }
    PHFetchOptions *options = [[PHFetchOptions alloc] init];
    options.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];
    
    self.txt_Title.text = selected.localizedTitle;
    
    PHFetchResult *fetchResultAssets = [PHAsset fetchAssetsInAssetCollection:selected options:options];
    for (PHAsset *item in fetchResultAssets) {
        
        [album.medias addObject:item];
    }
    self.albumGeneric = album;
    [self.photosCollectionView reloadData];
    
}





#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"PlayMyMovieSegue"])
    {
        
        AVAudioSession *audioSession = [AVAudioSession sharedInstance];
        NSError *error = nil;
        BOOL result = NO;
        if ([audioSession respondsToSelector:@selector(setActive:withOptions:error:)]) {
            result = [audioSession setActive:YES withOptions:0 error:&error];
        }
        if (!result && error) {
            // deal with the error
            NSLog(@"StayLog: AVAudioSession Error: %@", error);
        }
     
        error = nil;
        result = [audioSession setCategory:AVAudioSessionCategoryPlayback error:&error];
        
        if (!result && error) {
            // deal with the error
            NSLog(@"StayLog: AVAudioSession setCategory Error: %@", error);
        }
        
        PlayerViewController *moviePlayerViewController = segue.destinationViewController;
        if([sender isKindOfClass:[PHAsset class]]) {
            moviePlayerViewController.assetMovie = (PHAsset *)sender;
            moviePlayerViewController.currentMovie = nil;
            
            [self.imageManager requestAVAssetForVideo:sender options:nil resultHandler:^(AVAsset * _Nullable asset, AVAudioMix * _Nullable audioMix, NSDictionary * _Nullable info) {
                moviePlayerViewController.player = [AVPlayer playerWithURL:((AVURLAsset *)asset).URL];
            }];
        }
        else if([sender isKindOfClass:[Movie class]]) {
            moviePlayerViewController.currentMovie = (Movie *)sender;
            moviePlayerViewController.assetMovie = nil;
            moviePlayerViewController.videoUrl = [sender videoUrl];
        }
        else if([sender isKindOfClass:[NSString class]]) {
            moviePlayerViewController.assetMovie = nil;
            moviePlayerViewController.currentMovie = nil;
            moviePlayerViewController.videoUrl = sender;
        }
    }
    if([segue.identifier isEqualToString:@"AlbumToConfirmationSegue"] || [segue.identifier isEqualToString:@"AlbumToStyleSegue"] || [segue.identifier isEqualToString:@"AlbumToProgressSegue"]) {
        if(!isMovingOut) {
            isMovingOut = YES;
            
            self.sfApp.filmStrip.isAnimating = NO;
            if(![StayfilmApp isArray:self.selectedMedias equalToArray:self.initialSelectedMedias])
            {
                for (UIViewController *view in self.navigationController.viewControllers) {
                    if([view isKindOfClass:[MovieMakerStep1ViewController class]])
                    {
                        if(self.albumGeneric != nil) {
                            [view performSelectorOnMainThread:@selector(pickedSelectedMedias:) withObject:self.selectedMedias waitUntilDone:YES];
                        } else {
                            [view performSelectorOnMainThread:@selector(changeSelectedMedias:) withObject:self.selectedMedias waitUntilDone:YES];
                            [view performSelectorOnMainThread:@selector(reloadAlbumsCounters) withObject:nil waitUntilDone:NO];
                        }
                        break;
                    }
                }
                self.initialSelectedMedias = [self.selectedMedias copy];
            }
            else
            {
                for (UIViewController *view in self.navigationController.viewControllers) {
                    if([view isKindOfClass:[MovieMakerStep1ViewController class]])
                    {
                        [view performSelectorOnMainThread:@selector(clearAlbumViewController) withObject:nil waitUntilDone:NO];
                        break;
                    }
                }
            }
            isMovingOut = NO;
        }
    }
}

- (IBAction)unwindToStep2:(UIStoryboardSegue *)segue
{
    //TODO: Cleanup maybe?
}

BOOL isMovingOut = NO;
-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if([identifier isEqualToString:@"AlbumViewerBackToStep1"])
    {
        for (UIViewController *view in self.navigationController.viewControllers) {
            if([view isKindOfClass:[MovieMakerStep1ViewController class]])
            {
                if(self.albumGeneric != nil) {
                    [view performSelectorOnMainThread:@selector(pickedSelectedMedias:) withObject:self.selectedMedias waitUntilDone:YES];
                } else {
                    [view performSelectorOnMainThread:@selector(changeSelectedMedias:) withObject:self.selectedMedias waitUntilDone:YES];
                    [view performSelectorOnMainThread:@selector(reloadAlbumsCounters) withObject:nil waitUntilDone:NO];
                }
                break;
            }
        }
        
        [self.selectedMedias removeAllObjects];
        self.selectedMedias = nil;
        self.albumFB = nil;
        self.albumGeneric = nil;
        [self.photosCollectionView removeFromSuperview];
        self.photosCollectionView = nil;
        self.thumbConnection = nil;
//        self.img_SelectAll.image = nil;
//        self.txt_SelectAll = nil;
        [self.navigationController popViewControllerAnimated:YES];
    }
    if([identifier isEqualToString:@"AlbumToConfirmationSegue"] || [identifier isEqualToString:@"AlbumToStyleSegue"] || [identifier isEqualToString:@"AlbumToProgressSegue"]) {
        if(!isMovingOut) {
            isMovingOut = YES;
            
            if(![StayfilmApp isArray:self.selectedMedias equalToArray:self.initialSelectedMedias])
            {
                for (UIViewController *view in self.navigationController.viewControllers) {
                    if([view isKindOfClass:[MovieMakerStep1ViewController class]])
                    {
                        if(self.albumGeneric != nil) {
                            [view performSelectorOnMainThread:@selector(pickedSelectedMedias:) withObject:self.selectedMedias waitUntilDone:YES];
                        } else {
                            [view performSelectorOnMainThread:@selector(changeSelectedMedias:) withObject:self.selectedMedias waitUntilDone:YES];
                            [view performSelectorOnMainThread:@selector(reloadAlbumsCounters) withObject:nil waitUntilDone:NO];
                        }
                        break;
                    }
                }
                self.initialSelectedMedias = [self.selectedMedias copy];
            }
            else
            {
                for (UIViewController *view in self.navigationController.viewControllers) {
                    if([view isKindOfClass:[MovieMakerStep1ViewController class]])
                    {
                        [view performSelectorOnMainThread:@selector(clearAlbumViewController) withObject:nil waitUntilDone:NO];
                        break;
                    }
                }
            }
            isMovingOut = NO;
            return YES;
        }
    }
    return NO;
}

@end
