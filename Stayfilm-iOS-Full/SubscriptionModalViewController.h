//
//  SubscriptionModalViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 3/27/19.
//  Copyright © 2019 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>
#import "StayfilmApp.h"
@import KALoader;
#import "UIView+Toast.h"

NS_ASSUME_NONNULL_BEGIN

@protocol SubscriptionDelegate<NSObject>

-(void)subscriptionConfirmed;

@end

@interface SubscriptionModalViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *buyButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) StayfilmApp *sfApp;
@property (weak, nonatomic) IBOutlet UIImageView *titleSuscriptionImage;
@property  (nonatomic) Template *currentTemplate;
@property (weak, nonatomic) IBOutlet UIImageView *productImage;
@property (weak, nonatomic) IBOutlet UILabel *smallDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *offerLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@property (weak, nonatomic) IBOutlet UIButton *restorePurchaseButton;
@property (weak, nonatomic) IBOutlet UIButton *termsButton;
@property (weak, nonatomic) IBOutlet UILabel *benefitsTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *benefitsLabel;
@property (weak, nonatomic) IBOutlet UILabel *conditionsLabel;
@property (weak, nonatomic) IBOutlet UIButton *conditionsButton;

//@property (weak, nonatomic) IBOutlet UILabel *productNameLabel;
//@property (weak, nonatomic) IBOutlet UILabel *productDescriptionLabel;
//@property (weak, nonatomic) IBOutlet UILabel *productPriceLabel;

@property (nonatomic) NSString *userIdRegistered;

@property (nonatomic, assign) id<SubscriptionDelegate> delegate;
@end

NS_ASSUME_NONNULL_END
