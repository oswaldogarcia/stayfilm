//
//  ProgressViewController.h
//  fbmessenger
//
//  Created by Henrique Ormonde on 12/06/15.
//  Copyright (c) 2015 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SFConfig.h"
#import "Job.h"
#import "Uploader.h"
#import "Popover_CancelProductionViewController.h"

@import GoogleMobileAds;


@interface ProgressViewController : UIViewController <UIViewControllerTransitioningDelegate, StayfilmUploaderDelegate, Popover_CancelProductionDelegate>

@property (weak, nonatomic) IBOutlet UILabel *progressLabel;
@property (weak, nonatomic) IBOutlet UIProgressView *progressBar;
@property (weak, nonatomic) IBOutlet UIProgressView *progressUploadBar;
@property (weak, nonatomic) IBOutlet UIView *progressView;
@property (weak, nonatomic) IBOutlet UILabel *percentageLabel;
@property (weak, nonatomic) IBOutlet UIImageView *img_Background;
@property (weak, nonatomic) IBOutlet UIImageView *img_clapper;
@property (weak, nonatomic) IBOutlet DFPBannerView *bannerView;
@property (weak, nonatomic) IBOutlet UIView *clapperView;
@property (weak, nonatomic) IBOutlet UILabel *automaticTitleLabel;


@property (nonatomic, strong) Job *jobMelies;

-(void)viewCameBackFromBackground;

@end
