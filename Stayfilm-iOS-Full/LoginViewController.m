//
//  LoginViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Henrique on 1/24/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import "LoginViewController.h"
#import "Reachability.h"
#import "LoginIntroViewController.h"


@interface LoginViewController ()

@property (nonatomic, assign) int attempts;
@property (nonatomic, assign) int loginAttempts;
@property (nonatomic, weak) StayfilmApp *sfAppLogin;

@property (nonatomic, assign) BOOL didComeBackFromFBLogin;
@property (nonatomic, assign) BOOL ignoreOnceKBmovement;

@end

@implementation LoginViewController

@synthesize imageBackground, sfAppLogin, lbl_Email, lbl_Password, txt_Email, txt_Password, but_Enter, but_ForgotPassword, spinnerLoader, viewLoader;

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.attempts = 0;
    self.loginAttempts = 0;
    
    NSLog( @"### running FB SDK version: %@", [FBSDKSettings sdkVersion] );
    
    //self.but_FacebookConnect.loginBehavior = FBSDKLoginBehaviorBrowser;
    //self.but_FacebookConnect.loginBehavior = FBSDKLoginBehaviorNative;
    
    self.but_FacebookConnect.delegate = self;
    self.but_FacebookConnect.readPermissions = @[@"public_profile", @"email", @"user_photos", @"user_videos"];
    self.but_FacebookConnect.loginBehavior = FBSDKLoginBehaviorSystemAccount;
    //self.but_FacebookConnect.loginBehavior = FBSDKLoginBehaviorNative;
    
    UIInterpolatingMotionEffect *centerX = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.x" type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
    
    centerX.maximumRelativeValue = @60;
    centerX.minimumRelativeValue = @-60;
    
    UIInterpolatingMotionEffect *centerY = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.y" type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
    
    centerY.maximumRelativeValue = @50;
    centerY.minimumRelativeValue = @-50;
    
    UIMotionEffectGroup *group = [[UIMotionEffectGroup alloc] init];
    group.motionEffects = @[centerX, centerY];
    
    [self.imageBackground addMotionEffect:group];
    
    self.but_FacebookConnect.layer.cornerRadius = 5;
    self.but_FacebookConnect.layer.masksToBounds = YES;
    
    self.but_Enter.layer.cornerRadius = 5;
    self.but_Enter.layer.masksToBounds = YES;
    
    [self.txt_Email addTarget:self action:@selector(txt_Email_Ended) forControlEvents:UIControlEventEditingDidEndOnExit];
    [self.txt_Password addTarget:self action:@selector(txt_Password_Ended) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    [self.navigationController.navigationItem.backBarButtonItem setTitle:@""];
    
    self.lbl_version.text = [NSString stringWithFormat:NSLocalizedString(@"VERSION_X", nil), [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
    
    //TODO change image accordingly to the language
//    NSString *langcode = [[NSLocale currentLocale] objectForKey:NSLocaleLanguageCode];
//    if([langcode isEqualToString:@"pt"])
//    {
//        self.imageBackgroundText.image = [UIImage imageNamed:@"Intro_Text_pt"];
//    }
//    else if([langcode isEqualToString:@"es"])
//    {
//        self.imageBackgroundText.image = [UIImage imageNamed:@"Intro_Text_es"];
//    }
//    else if([langcode isEqualToString:@"fr"])
//    {
//        self.imageBackgroundText.image = [UIImage imageNamed:@"Intro_Text_fr"];
//    }
//    else if([langcode isEqualToString:@"it"])
//    {
//        self.imageBackgroundText.image = [UIImage imageNamed:@"Intro_Text_it"];
//    }
//    else if([langcode isEqualToString:@"tr"])
//    {
//        self.imageBackgroundText.image = [UIImage imageNamed:@"Intro_Text_tr"];
//    }
//    else if([langcode isEqualToString:@"zh"])
//    {
//        self.imageBackgroundText.image = [UIImage imageNamed:@"Intro_Text_zh"];
//    }
    
    self.didComeBackFromFBLogin = NO;
    [self setNeedsStatusBarAppearanceUpdate];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //Google Analytics
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"login"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];

    [self performSelectorInBackground:@selector(createStayfilmAppSingleton:) withObject:@1];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)createStayfilmAppSingleton:(NSNumber *)state
{
    if(self.sfAppLogin == nil)
    {
        self.sfAppLogin = [StayfilmApp sharedStayfilmAppSingletonWithDelegate:self];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(BOOL)connected {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

-(void)sfSingletonCreated
{
    @try {
        if(self.sfAppLogin == nil)
            self.sfAppLogin = [StayfilmApp sharedStayfilmAppSingleton];
        
        NSLog(@"StayLog: SingletonCreated sucessfully!");
        
        if ([FBSDKAccessToken currentAccessToken]) {
            // User is logged in facebook.
            if(self.sfAppLogin.currentUser == nil)
            {
                if(self.sfAppLogin.settings[@"facebookToken"] == nil || [self.sfAppLogin.settings[@"facebookToken"] isEqualToString:@""])
                {
                    if(!self.didComeBackFromFBLogin)
                    {
                        [FBSDKProfile setCurrentProfile:nil];
                        [FBSDKAccessToken setCurrentAccessToken:nil];
                        FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
                        [loginManager logOut];
                        [self showFacebookLoginButton];
                    }
                    return;
                }
                else
                {
                    LoginFacebookOperation *loginOp = [[LoginFacebookOperation alloc] initWithDelegate:self andSFApp:self.sfAppLogin];
                    [sfAppLogin.operationsQueue addOperation:loginOp];
                }
            }
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self showFacebookLoginButton];
            });
        }
        
    }
    @catch (NSException *exception) {
        NSLog(@"StayLog: EXCEPTION sfSingletonCreated in LoginViewController with error: %@",exception.description);
        if(self.attempts >= 1)
        {
            self.attempts = 0;
            NSLog(@"StayLog: sfSingeltonCreated: %@", exception.description);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self showFacebookLoginButton];
            });
            
            NSLog(@"StayLog: attempt %i", self.attempts);
            
        }
        else{
            ++self.attempts;
            [self viewDidAppear:YES];
        }
    }
}

-(void)connectivityChanged:(BOOL)isConnected {
    if(isConnected) {
        [self createStayfilmAppSingleton:@1];
    } else {
//        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"NO_INTERNET_CONNECTION", nil)
//                                                                       message:NSLocalizedString(@"NO_INTERNET_CONNECTION_MESSAGE", nil)
//                                                                preferredStyle:UIAlertControllerStyleAlert];
//        
//        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
//                                                              handler:^(UIAlertAction * action) {}];
//        
//        [alert addAction:defaultAction];
//        [self presentViewController:alert animated:YES completion:nil];
    }
}


#pragma mark - Keybord Control Events

/*
-(void)viewWillAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];

}

-(void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}


- (void)keyboardWasShown:(NSNotification*)aNotification {
    // Animate the current view out of the way

    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES withOffset:[[[aNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size];
    }
 
//    else if (self.view.frame.origin.y < 0) // this was causing the jumpdown of the keyboard when typing on some phones that called this methood when the keyboard was already opened
//    {
//        [self setViewMovedUp:NO withOffset:[[[aNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size];
//    }
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification {
    if (self.view.frame.origin.y >= 0)
    {
        if(self.ignoreOnceKBmovement) {
            self.ignoreOnceKBmovement = NO;
            return;
        }
        [self setViewMovedUp:YES withOffset:[[[aNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO withOffset:[[[aNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if ([sender isEqual:self.txt_Email] || [sender isEqual:self.txt_Password])
    {
        //move the main view, so that the keyboard does not hide it.
        if  (self.view.frame.origin.y >= 0)
        {
            [self setViewMovedUp:YES withOffset:CGSizeZero];
        }
    }
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp withOffset:(CGSize)kbSize {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    int offset = 0;
    if(!CGSizeEqualToSize(kbSize, CGSizeZero)) {
        offset = kbSize.height;
    }
    else {
        offset = 80;
    }
    offset = 80;
    
    CGRect rect = self.view.frame;
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y -= offset;
        rect.size.height += offset;
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += offset;
        rect.size.height -= offset;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}
*/

#pragma mark - Login Operations Delegate

-(void)loginFacebookDidFinish:(LoginFacebookOperation *)loginOP {
    // switch codemessage
    
    switch (loginOP.messageCode) {
        case LOGIN_SUCCESSFUL:
        {
            self.loginAttempts = 0;
            [self moveToStep1];
        }
            break;
            
        default:
        {
            ++self.loginAttempts;
            [self.but_FacebookConnect setHidden:NO];
            //[self.spinnerLoader setHidden:YES];
            [self.spinnerLoader stopAnimating];
        }
            break;
    }
    
}

-(void)loginFacebookDidFail:(LoginFacebookOperation *)loginOP {
    
    ++self.loginAttempts;
    if(self.loginAttempts >= 3)
    {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"FACEBOOK_FALIED", nil)
                                                                       message:NSLocalizedString(@"FACEBOOK_FALIED_MESSAGE", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        self.loginAttempts = 0;
        return;
    }
    
    BOOL retryLogin = NO;
    switch (loginOP.messageCode) {
        case CONNECTION_ERROR:
        {
            retryLogin = YES;
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CONNECTION_ERROR", nil)
                                                                           message:NSLocalizedString(@"CONNECTION_ERROR_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
            break;
        case USER_CANCELLED:
        {
            retryLogin = NO;
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"USER_CANCELLED", nil)
                                                                           message:NSLocalizedString(@"USER_CANCELLED_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
            break;
        case LOGIN_FAILED:
        {
            retryLogin = YES;
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"LOGIN_FAILED", nil)
                                                                           message:NSLocalizedString(@"LOGIN_FAILED_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
            break;
        case REGISTRATION_ERROR:
        {
            retryLogin = YES;
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"REGISTRATION_ERROR", nil)
                                                                           message:NSLocalizedString(@"REGISTRATION_ERROR_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
            break;
        case NO_CONFIG_LOADED:
        {
            retryLogin = YES;
            self.loginAttempts--;
            // say nothing, just try again
            if(self.sfAppLogin != nil)
                self.sfAppLogin.sfConfig = [SFConfig getAllConfigsFromWS:self.sfAppLogin.wsConfig];
            else
                self.sfAppLogin = [StayfilmApp sharedStayfilmAppSingletonWithDelegate:self];
            break;
        }
        case NO_INTERNET_CONNECTION:
        {
            retryLogin = NO;
            self.loginAttempts--;
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"NO_INTERNET_CONNECTION", nil)
                                                                           message:NSLocalizedString(@"NO_INTERNET_CONNECTION_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
            
            [NSTimer scheduledTimerWithTimeInterval:2.0 repeats:YES block:^(NSTimer * _Nonnull timer) {
                if([self connected])
                {
                    [timer invalidate];
                    timer = nil;
                    LoginFacebookOperation *loginOp = [[LoginFacebookOperation alloc] initWithDelegate:self andSFApp:self.sfAppLogin];
                    [self.sfAppLogin.operationsQueue addOperation:loginOp];
                }
            }];
            break;
        }
        case -1:
        {
            retryLogin = YES;
            NSLog(@"StayLog: loginDidFail with EXCEPTION (messageCode -1)");
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"LOGIN_FAILED", nil)
                                                                           message:NSLocalizedString(@"LOGIN_FAILED_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
            break;
        default:
        {
            retryLogin = YES;
        }
            break;
    }
    
    if(retryLogin)
    {
        LoginFacebookOperation *logOperation = [[LoginFacebookOperation alloc] initWithDelegate:self andSFApp:self.sfAppLogin];
        [self.sfAppLogin.operationsQueue addOperation:logOperation];
    }
    else
    {
        [self.but_FacebookConnect setHidden:NO];
        [self.spinnerLoader stopAnimating];
    }
    
    [loginOP cancel];
}

-(void)facebookLoginNowOnStayfilmWithToken:(NSString*)p_token
{
    if(self.sfAppLogin.sfConfig != nil)
    {
        [self.spinnerLoader setHidden:NO];
        [self.spinnerLoader startAnimating];
        
        User *appUser = [User loginWithFacebook:p_token andSFAppSingleton:self.sfAppLogin]; // already saves to sfApp.settings
        self.sfAppLogin.currentUser = appUser;
        if(appUser == nil)
        {
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CONNECTION_ERROR", nil)
                                                                           message:NSLocalizedString(@"CONNECTION_ERROR_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
            NSLog(@"StayLog: fbconn error 2");
            //exit(0);
            [self showFacebookLoginButton];
        }
        else if(!appUser.userExists && !appUser.isResponseError) // register user
        {
            self.sfAppLogin.settings = nil;
            self.sfAppLogin.settings = [[NSMutableDictionary alloc] init];
            
            [self.sfAppLogin.settings setObject:p_token forKey:@"facebookToken"];
            [self.sfAppLogin saveSettings];
            //colocar a 1 checklogin
            //token = result.token.tokenString;
            __weak typeof(self) selfDelegate = self;
            dispatch_async(dispatch_get_main_queue(), ^{
                [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id,birthday,email,first_name,last_name,gender"}]
                 startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
                 {
                     if (!error)
                     {
                         NSMutableDictionary *registerArgs = [[NSMutableDictionary alloc] init];
                         [registerArgs setObject:self.sfAppLogin.settings[@"facebookToken"] forKey:@"facebookToken"];
                         
                         if(result[@"birthday"] != nil)
                         {
                             NSDateFormatter *dateformat = [[NSDateFormatter alloc] init];
                             [dateformat setDateFormat:@"MM/dd/yyyy"];
                             NSDate * date = [dateformat dateFromString:result[@"birthday"]];
                             NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:date];
                             [registerArgs setObject:[NSString stringWithFormat:@"%d", (int)[components day]]forKey:@"birthdayDay"];
                             [registerArgs setObject:[NSString stringWithFormat:@"%d", (int)[components month]]forKey:@"birthdayMonth"];
                             [registerArgs setObject:[NSString stringWithFormat:@"%d", (int)[components year]]forKey:@"birthdayYear"];
                         }
                         if(result[@"email"] != nil)
                         {
                             [registerArgs setObject:(NSString *)result[@"email"] forKey:@"email"];
                         }
                         if(result[@"first_name"] != nil)
                         {
                             [registerArgs setObject:(NSString *)result[@"first_name"] forKey:@"firstName"];
                         }
                         if(result[@"last_name"] != nil)
                         {
                             [registerArgs setObject:(NSString *)result[@"last_name"] forKey:@"lastName"];
                         }
                         if(result[@"gender"] != nil)
                         {
                             if([result[@"gender"] isEqualToString:@"male"])
                             {
                                 [registerArgs setObject:@"m"forKey:@"gender"];
                             }
                             else if([result[@"gender"] isEqualToString:@"female"])
                             {
                                 [registerArgs setObject:@"f" forKey:@"gender"];
                             }
                             else
                             {
                                 [registerArgs setObject:@"n" forKey:@"gender"];
                             }
                         }
                         
                         User *registered = [User registerUserWithArguments:registerArgs andSFAppSingleton:selfDelegate.sfAppLogin]; // already saves to sfApp.settings
                         selfDelegate.sfAppLogin.currentUser = registered;
                         BOOL registeredSucessfuly = NO;
                         if(registered != nil && registered.isLogged)
                         {
                             if(result[@"id"] != nil)
                             {
                                 // check Login   0 = not started  1 = true   2 = false
                                 [selfDelegate.sfAppLogin.settings setObject:@"1" forKey:@"CHECK_Login"];
                                 [selfDelegate.sfAppLogin.settings setObject:(NSString *)result[@"id"] forKey:@"FB_id"];
                                 [selfDelegate.sfAppLogin.settings setObject:[[FBSDKAccessToken currentAccessToken] tokenString] forKey:@"facebookToken"];
                                 [selfDelegate.sfAppLogin.settings setObject:registered.idSession forKey:@"idSession"];
                                 [selfDelegate.sfAppLogin saveSettings];
                                 
                                 //                                 //Google Analytics Event
                                 //                                 id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
                                 //                                 [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Botao"
                                 //                                                                                       action:@"Register"
                                 //                                                                                        label:@"Iphone_Nativo_evento_108_botao_Register"
                                 //                                                                                        value:@1] build]];
                                 registeredSucessfuly = YES;
                                 [self.sfAppLogin.currentUser setUserFacebookToken:self.sfAppLogin.settings[@"facebookToken"] andSFAppSingleton:self.sfAppLogin];
                             }
                             else
                             {
                                 registeredSucessfuly = NO;
                             }
                         }
                         else
                         {
                             registeredSucessfuly = NO;
                             [selfDelegate showFacebookLoginButton];
                         }
                         if(registeredSucessfuly == YES)
                         {
                             NSData *dataSave = [NSKeyedArchiver archivedDataWithRootObject:registered];
                             [[NSUserDefaults standardUserDefaults] setObject:dataSave forKey:@"currentUser"];
                             
                             selfDelegate.sfAppLogin.currentUser = registered;
                             [selfDelegate.sfAppLogin.settings setObject:@"1" forKey:@"SF_User_Registered"];
                             [selfDelegate performSelectorOnMainThread:@selector(hideAndStopSpinner) withObject:nil waitUntilDone:NO];
                             [selfDelegate.viewLoader setHidden:YES];
                             [self.spinnerLoader stopAnimating];
                             [self.spinnerLoader setHidden:YES];
                             [selfDelegate moveToStep1];
                         }
                         else
                         {
                             NSLog(@"StayLog: fb Error on Register %@",error);
                             UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"REGISTRATION_ERROR", nil)
                                                                                            message:NSLocalizedString(@"REGISTRATION_ERROR_MESSAGE", nil)
                                                                                     preferredStyle:UIAlertControllerStyleAlert];
                             
                             UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                                   handler:^(UIAlertAction * action) {}];
                             
                             [alert addAction:defaultAction];
                             [selfDelegate presentViewController:alert animated:YES completion:nil];
                             [selfDelegate.viewLoader setHidden:YES];
                             [self.spinnerLoader stopAnimating];
                             [self.spinnerLoader setHidden:YES];
                         }
                     }
                     else
                     {
                         NSLog(@"StayLog: fb Error %@",error);
                         UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CONNECTION_ERROR", nil)
                                                                                        message:NSLocalizedString(@"CONNECTION_ERROR_MESSAGE", nil)
                                                                                 preferredStyle:UIAlertControllerStyleAlert];
                         
                         UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                               handler:^(UIAlertAction * action) {}];
                         
                         [alert addAction:defaultAction];
                         [selfDelegate presentViewController:alert animated:YES completion:nil];
                         [selfDelegate showFacebookLoginButton];
                     }
                 }];
            });
        }
        else if(!appUser.isLogged)
        {
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"LOGIN_FAILED", nil)
                                                                           message:NSLocalizedString(@"LOGIN_FAILED_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
            NSLog(@"StayLog: fbconn error 3");
            //exit(0);
            [self showFacebookLoginButton];
            
            // check Login   0 = not started  1 = true   2 = false
            [self.sfAppLogin.settings setObject:@"0" forKey:@"CHECK_Login"];
            [self.sfAppLogin saveSettings];
        }
        else //user logged in successfully
        {
            if(self.sfAppLogin.settings == nil)
            {
                self.sfAppLogin.settings = [[NSMutableDictionary alloc] init];
            }
            [self.sfAppLogin.settings setObject:@"1" forKey:@"CHECK_Login"];
            [self.spinnerLoader stopAnimating];
            
            if([[FBSDKAccessToken currentAccessToken] userID] != nil)
                [self.sfAppLogin.settings setObject:[[FBSDKAccessToken currentAccessToken] userID] forKey:@"FB_id"];
            if([[FBSDKAccessToken currentAccessToken] tokenString] != nil)
                [self.sfAppLogin.settings setObject:[[FBSDKAccessToken currentAccessToken] tokenString] forKey:@"facebookToken"];
            [self.sfAppLogin.settings setObject:appUser.idSession forKey:@"idSession"];
            [self.sfAppLogin saveSettings];
            self.sfAppLogin.currentUser = appUser;
            
            NSData *dataSave = [NSKeyedArchiver archivedDataWithRootObject:appUser];
            [[NSUserDefaults standardUserDefaults] setObject:dataSave forKey:@"currentUser"];
            
            //update user in background
            __weak typeof(self) selfDelegate = self;
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^(void){
                [selfDelegate.sfAppLogin.currentUser setUserFacebookToken:selfDelegate.sfAppLogin.settings[@"facebookToken"] andSFAppSingleton:selfDelegate.sfAppLogin];
            });
            
            [self.viewLoader setHidden:YES];
            [self moveToStep1];
            
            //            //Google Analytics Event
            //            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            //            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Botao"
            //                                                                  action:@"Connect"
            //                                                                   label:@"Iphone_Nativo_evento_101_botao_Connect"
            //                                                                   value:@1] build]];
        }
    }
    else
    {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CONNECTION_ERROR", nil)
                                                                       message:NSLocalizedString(@"CONNECTION_ERROR_MESSAGE", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        NSLog(@"StayLog: fbconn error 4");
        //exit(0);
        [self showFacebookLoginButton];
        
        // check Login   0 = not started  1 = true   2 = false
        [self.sfAppLogin.settings setObject:@"0" forKey:@"CHECK_Login"];
        [self.sfAppLogin saveSettings];
    }
}


#pragma mark - Button auto events

-(void)txt_Email_Ended {
    [self.txt_Password becomeFirstResponder];
}

-(void)txt_Password_Ended {
    [self.view endEditing:YES];
}

#pragma mark - Buttons Actions

- (IBAction)keyboardDismissal:(id)sender {
    [self.view endEditing:YES];
}


- (IBAction)backButton_tapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)cancelLoginButton_Click:(id)sender {
    [self.view endEditing:YES];
}

//BOOL isClickingForgotPassword = NO;
- (IBAction)but_forgotPassword_Clicked:(id)sender {
    [self.view endEditing:YES];
    
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"password"
                                                          action:@"forgot"
                                                           label:@""
                                                           value:@1] build]];
//    if(!isClickingForgotPassword) {
//        isClickingForgotPassword = YES;
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://www.stayfilm.com/user/passwordRecover"]];
//        isClickingForgotPassword = NO;
//    }
}

- (IBAction)but_Enter_tapped:(id)sender {
    [self.view endEditing:YES];
    
    if(self.txt_Email.text != nil && ![self.txt_Email.text isEqualToString:@""] && self.txt_Password.text != nil && ![self.txt_Password.text isEqualToString:@""])
    {
        [self performSelectorInBackground:@selector(EnterButtonAction) withObject:nil];
    }
    else
    {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"LOGIN_MISSING_CREDENTIALS", nil)
                                                                       message:NSLocalizedString(@"LOGIN_MISSING_CREDENTIALS_MESSAGE", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {[alert dismissViewControllerAnimated:YES completion:nil];}];
        
        [alert addAction:defaultAction];
        self.ignoreOnceKBmovement = YES;
        [self presentViewController:alert animated:YES completion:nil];
        [self.viewLoader setHidden:YES];
        [self.spinnerLoader stopAnimating];
        [self.spinnerLoader setHidden:YES];
    }
}

-(void)EnterButtonAction {
    @try {
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"user"
                                                              action:@"login"
                                                               label:@"email"
                                                               value:@1] build]];
        
        runOnMainQueueWithoutDeadlocking(^{
            [self.viewLoader setHidden:NO];
            [self.spinnerLoader startAnimating];
            [self.spinnerLoader setHidden:NO];
        });
        User *appUser = [User loginWithUsername:self.txt_Email.text withPassword:self.txt_Password.text andSFAppSingleton:self.sfAppLogin];
        if(appUser == nil)
        {
            runOnMainQueueWithoutDeadlocking(^{
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CONNECTION_ERROR", nil)
                                                                               message:NSLocalizedString(@"CONNECTION_ERROR_MESSAGE", nil)
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {}];
                
                [alert addAction:defaultAction];
                self.ignoreOnceKBmovement = YES;
                [self presentViewController:alert animated:YES completion:nil];
                NSLog(@"StayLog: fbconn error 2");
                //exit(0);
                [self showFacebookLoginButton];
            });
        }
        else if(!appUser.userExists && !appUser.isResponseError) //TODO: register user maybe?
        {
            runOnMainQueueWithoutDeadlocking(^{
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"LOGIN_FAILED", nil)
                                                                               message:[NSString stringWithFormat:NSLocalizedString(@"LOGIN_FAILED_WITH_MESSAGE", nil), appUser.responseErrorMessage, nil]
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {}];
                
                [alert addAction:defaultAction];
                self.ignoreOnceKBmovement = YES;
                [self presentViewController:alert animated:YES completion:nil];
                [self.viewLoader setHidden:YES];
                [self.spinnerLoader stopAnimating];
                [self.spinnerLoader setHidden:YES];
            });
        }
        else if(!appUser.isLogged)
        {
            runOnMainQueueWithoutDeadlocking(^{
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"LOGIN_FAILED", nil)
                                                                               message:[NSString stringWithFormat:NSLocalizedString(@"LOGIN_FAILED_WITH_MESSAGE", nil), appUser.responseErrorMessage, nil]
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {}];
                
                [alert addAction:defaultAction];
                self.ignoreOnceKBmovement = YES;
                [self presentViewController:alert animated:YES completion:nil];
                NSLog(@"StayLog: fbconn error 3");
                //exit(0);
                [self showFacebookLoginButton];
                
                if(self.sfAppLogin.settings == nil)
                {
                    self.sfAppLogin.settings = [[NSMutableDictionary alloc] init];
                }
                // check Login   0 = not started  1 = true   2 = false
                [self.sfAppLogin.settings setObject:@"0" forKey:@"CHECK_Login"];
                [self.sfAppLogin saveSettings];
            });
        }
        else //user logged in successfully
        {
            self.sfAppLogin.settings = nil;
            self.sfAppLogin.settings = [[NSMutableDictionary alloc] init];
            
            [self.sfAppLogin.settings setObject:@"1" forKey:@"CHECK_Login"];
            [self.sfAppLogin.settings setObject:@"1" forKey:@"LOGGEDIN_Email"];
            [self.sfAppLogin.settings setObject:appUser.idSession forKey:@"idSession"];
            [self.sfAppLogin.settings setObject:appUser.idUser forKey:@"idUser"];
            [self.viewLoader setHidden:YES];
            [self.spinnerLoader stopAnimating];
            [self.spinnerLoader setHidden:YES];
            
            [self.sfAppLogin saveSettings];
            self.sfAppLogin.currentUser = appUser;
            
            NSData *dataSave = [NSKeyedArchiver archivedDataWithRootObject:appUser];
            [[NSUserDefaults standardUserDefaults] setObject:dataSave forKey:@"currentUser"];
            
            [self moveToStep1];
            
            //            //Google Analytics Event
            //            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            //            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Botao"
            //                                                                  action:@"Connect"
            //                                                                   label:@"Iphone_Nativo_evento_101_botao_Connect"
            //                                                                   value:@1] build]];
        }
    } @catch (NSException *ex) {
        NSLog(@"StayLog: EXCEPTION in EnterButtonAction with error: %@", ex.description);
    }
    
}


-(void)showFacebookLoginButton
{
    [self.spinnerLoader stopAnimating];
    [self.spinnerLoader setHidden:YES];
    [self.viewLoader setHidden:YES];
    [self.but_FacebookConnect setHidden:NO];
    [UIView animateWithDuration:1.0 animations:^{
        self.but_FacebookConnect.alpha = 1.0;
        [self.view setNeedsUpdateConstraints];
    }];
}

//- (IBAction)facebookLoginOpButton_Clicked:(id)sender {
//
//    [self.but_FacebookConnect setHidden:YES];
//    [self.spinnerLoader setHidden:NO];
//    [self.spinnerLoader startAnimating];
//
//    LoginFacebookOperation *loginOp = [[LoginFacebookOperation alloc] initWithDelegate:self andSFApp:self.sfAppLogin];
//    [sfAppLogin.operationsQueue addOperation:loginOp];
//}


-(void)hideAndStopSpinner
{
    [self.spinnerLoader stopAnimating];
}

//-(void)showAndStartSpinner
//{
//    [self.spinnerLoader setHidden:NO];
//    [self.spinnerLoader startAnimating];
//    [UIView animateWithDuration:1.0 animations:^{
//        self.but_FacebookConnect.alpha = 0;
//        [self.view setNeedsUpdateConstraints];
//    }];
//    [self.but_FacebookConnect setHidden:YES];
//}

- (IBAction)but_FacebookCustom_tapped:(id)sender {
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    login.loginBehavior = FBSDKLoginBehaviorSystemAccount;
    [login logInWithReadPermissions:@[@"public_profile", @"email", @"user_photos", @"user_videos"]
                 fromViewController:self
                            handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"StayLog: FB: Process error");
             
             dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                 [self showFacebookLoginButton];
             });
         } else if (result.isCancelled) {
             NSLog(@"StayLog: FB: Cancelled");
             
             dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                 [self showFacebookLoginButton];
             });
         } else {
             NSLog(@"StayLog: FB: Logged in");
             
             self.didComeBackFromFBLogin = YES;
             
             [self.viewLoader setHidden:NO];
             [self.spinnerLoader startAnimating];
             [self.spinnerLoader setHidden:NO];
             
             if(self.sfAppLogin.settings[@"LOGGEDIN_Email"] != nil) {
                 [self.sfAppLogin.settings removeObjectForKey:@"LOGGEDIN_Email"];
             }
             
             [self performSelectorInBackground:@selector(facebookLoginNowOnStayfilmWithToken:) withObject:result.token.tokenString];
             
             //Google Analytics Event
             id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
             [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"user"
                                                                   action:@"login"
                                                                    label:@"facebook"
                                                                    value:@1] build]];
         }
     }];
}


- (void)loginButton:(FBSDKLoginButton *)loginButton didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result error:(NSError *)error {
    if(error == nil && result.token != nil && ![result.token.tokenString isEqual: @""])
    {
        self.didComeBackFromFBLogin = YES;
        
        [self.viewLoader setHidden:NO];
        [self.spinnerLoader startAnimating];
        [self.spinnerLoader setHidden:NO];
        
        if(self.sfAppLogin.settings[@"LOGGEDIN_Email"] != nil) {
            [self.sfAppLogin.settings removeObjectForKey:@"LOGGEDIN_Email"];
        }
        
        [self performSelectorInBackground:@selector(facebookLoginNowOnStayfilmWithToken:) withObject:result.token.tokenString];
        
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"user"
                                                              action:@"login"
                                                               label:@"facebook"
                                                               value:@1] build]];
    }
    else
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            [self showFacebookLoginButton];
        });
    }
}

-(void)moveToStep1
{
    @try {
        // check Login   0 = not started  1 = true   2 = false
        [self.sfAppLogin.settings setObject:@"1" forKey:@"CHECK_Login"];
        
//        [self.sfAppLogin.currentUser validateSubscription:^(BOOL hasSubscription) {
//            self.sfAppLogin.currentUser.hasSubscription = hasSubscription;
       
        [self.sfAppLogin saveSettings];
        
        
//        UIStoryboard *story = [UIStoryboard storyboardWithName:@"MovieMaker" bundle:nil];
//        UIViewController *vc = [story instantiateInitialViewController];
//        [self showViewController:vc sender:nil];

//        [self dismissViewControllerAnimated:YES completion:^{
//            runOnMainQueueWithoutDeadlocking(^{
//                [self.presentingViewController performSelectorOnMainThread:@selector(moveToStep1) withObject:nil waitUntilDone:YES];
//            });
//        }];
        
        //[self.navigationController popViewControllerAnimated:YES];
        [self performSegueWithIdentifier:@"LoginToStep1" sender:nil];
            
//        }];
    }
    @catch (NSException *exception) {
        NSLog(@"StayLog: EXCEPTION MoveToStep1 exception with error: %@", exception.description);
    }
}

- (void) loginButtonDidLogOut:(FBSDKLoginButton *)loginButton {
    [self.sfAppLogin.settings removeObjectForKey:@"facebookToken"];
    [self.sfAppLogin.settings removeObjectForKey:@"FB_id"];
    [self.sfAppLogin.settings removeObjectForKey:@"idSession"];
    [self.sfAppLogin.settings removeObjectForKey:@"idUser"];
    [self.sfAppLogin cleanSettings];
    [self.sfAppLogin saveSettings];
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

