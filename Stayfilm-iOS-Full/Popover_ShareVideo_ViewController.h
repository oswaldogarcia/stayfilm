//
//  Popover_ShareVideo_ViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Henrique on 2/7/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShareButton_CollectionViewCell.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "LoginSocialWebviewOperation.h"
#import "LoginFacebookOperation.h"
#import "Downloader.h"
#import "Movie.h"
#import "Popover_ConfirmPrivacyShareViewController.h"
#import "PopupMenuAnimation.h"

@protocol Popover_ShareVideo_ViewControllerDelegate <NSObject>
@required
-(void)decidedOption:(NSString *)p_option;
-(void)dismissModalMenu;
@end

@interface Popover_ShareVideo_ViewController : UIViewController <UIActionSheetDelegate, UIGestureRecognizerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UIViewControllerTransitioningDelegate, FBSDKSharingDelegate, StayfilmDownloaderDelegate, LoginToSocialNetworkOperationDelegate, LoginFacebookOperationDelegate, Popover_ShareVideo_ViewControllerDelegate, Popover_ConfirmPrivacyShareDelegate>

@property(nonatomic, weak) id<Popover_ShareVideo_ViewControllerDelegate> delegate;
@property (nonatomic, strong) PopupMenuAnimation *transitionAnimation;
@property (assign, nonatomic) BOOL isModal;
@property (strong, nonatomic) Movie *movie;

@property (weak, nonatomic) IBOutlet UISwitch *switchShowGallery;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionSocial;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionMore;
@property (weak, nonatomic) IBOutlet UIButton *but_help;
@property (weak, nonatomic) IBOutlet UIButton *but_done;
@property (weak, nonatomic) IBOutlet UIView *view_Help;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner_Loading;

@property (weak, nonatomic) IBOutlet UIView *view_Download;
@property (weak, nonatomic) IBOutlet UIProgressView *progress_Download;
@property (weak, nonatomic) IBOutlet UILabel *txt_DownloadProgress;
@property (weak, nonatomic) IBOutlet UILabel *txt_Downloading;

@property (strong, nonatomic) IBOutlet UIView *blur_view;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UISwitch *downloadSwitch;
@property (weak, nonatomic) IBOutlet UILabel *downloadLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *switchConstraint;

-(id)initWithDelegate:(id<Popover_ShareVideo_ViewControllerDelegate>)p_delegate;

-(void)showAlreadyDownloaded;
-(void)showDownloadFailed;
-(void)showDownloadProgress;
-(void)hideDownloadProgress;
-(void)hideDownloadProgressSettingDownloadedButton;
-(void)hideDownloadProcessWithoutAnimation;

@end
