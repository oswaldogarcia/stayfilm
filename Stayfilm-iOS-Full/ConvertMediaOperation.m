//
//  ConvertMediaOperation.m
//  Stayfilm for Messenger
//
//  Created by Henrique Ormonde on 27/10/15.
//  Copyright © 2015 Stayfilm. All rights reserved.
//

#import "ConvertMediaOperation.h"

@interface ConvertMediaOperation ()
@property (nonatomic, strong) NSConditionLock* lockImage;
@property (nonatomic, strong) NSConditionLock* lockVideo;

@end

@implementation ConvertMediaOperation

@synthesize pathConvertedFile, mediaPath, asset, lockImage, delegate;


#pragma mark -
#pragma mark - Life Cycle

- (id)initWithAsset:(PHAsset *)p_asset withMediaPath:(NSString *)p_mediaPath delegate:(id<ConvertMediaDelegate>) theDelegate {
    if (self = [super init]) {
        self.delegate = theDelegate;
        self.asset = p_asset;
        self.pathConvertedFile = @"";
        self.mediaPath = p_mediaPath;
    }
    return self;
}

#pragma mark -
#pragma mark - Convert Media

- (void)main {
    
    @autoreleasepool {
        
        if (self.isCancelled)
            return;
        
        @try
        {
            __block NSData* data = nil;
            PHImageManager *manager = [PHImageManager defaultManager];
            if(self.asset.mediaType == PHAssetMediaTypeImage)
            {
                PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
                options.resizeMode = PHImageRequestOptionsResizeModeExact;
                options.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
                [options setNetworkAccessAllowed:YES];
                options.synchronous = YES;
                
                self.lockImage = nil;
                self.lockImage = [[NSConditionLock alloc] initWithCondition:1];
                
                [manager requestImageForAsset:self.asset
                                   targetSize:CGSizeMake(1880, 720)
                                  contentMode:PHImageContentModeAspectFit
                                      options:options
                                resultHandler:^void(UIImage *image, NSDictionary *info)
                                {
                                    
                                    image = [self normalizedImage:image];
                                    
                                    if ((image.size.width <= 1880 && image.size.height <= 720) || (image.size.height <= 1880 && image.size.width <= 720)) {
                                        data = UIImageJPEGRepresentation(image, 0.9f);
                                    } else {
                                        data = UIImageJPEGRepresentation(image, 1.0f);
                                    }
                                    
                                    NSError* err = nil;
                                    if (![data writeToFile:self.mediaPath options:NSAtomicWrite error:&err]) {
                                        NSLog(@"StayLog: Error writing to File: %@", err.description);
                                        [(NSObject *)self.delegate performSelectorOnMainThread:@selector(converterDidFail:) withObject:self waitUntilDone:NO];
                                        [self cancel];
                                        [self.lockImage lock];
                                        [self.lockImage unlockWithCondition:0];
                                    } else {
                                        self.pathConvertedFile = self.mediaPath;
                                        [(NSObject *)self.delegate performSelectorOnMainThread:@selector(converterDidFinish:) withObject:self waitUntilDone:NO];
                                        
                                        [self.lockImage lock];
                                        [self.lockImage unlockWithCondition:0];
                                    }
                                    
                                }];
                [self.lockImage lockWhenCondition:0];
                [self.lockImage unlock];
            }
            else if(self.asset.mediaType == PHAssetMediaTypeVideo)
            {
                PHVideoRequestOptions *options = [[PHVideoRequestOptions alloc] init];
                options.deliveryMode = PHVideoRequestOptionsDeliveryModeFastFormat;
                [options setNetworkAccessAllowed:YES];
                
                //create locks to avoid thread kill within process
                self.lockVideo = nil;
                self.lockVideo = [[NSConditionLock alloc] initWithCondition:1];
                
                @try {
                    
                    [[PHImageManager defaultManager] requestAVAssetForVideo:self.asset options:options resultHandler:^(AVAsset * avasset, AVAudioMix * audioMix, NSDictionary * info) {
                        dispatch_async(dispatch_get_main_queue(), ^{
//                           
                            NSURL *url = [NSURL URLWithString:[[NSString alloc] initWithFormat:@"file://%@",self.mediaPath]];
                            AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:avasset presetName:AVAssetExportPreset640x480];
                            exporter.outputURL = url;
                            exporter.fileLengthLimit = 19*1024*1024;
                            exporter.outputFileType = AVFileTypeQuickTimeMovie;
                            exporter.shouldOptimizeForNetworkUse = YES;
                            
                            [exporter exportAsynchronouslyWithCompletionHandler:^{
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    if (exporter.status == AVAssetExportSessionStatusCompleted) {
                                       
                                        self.pathConvertedFile = self.mediaPath;
                                        [(NSObject *)self.delegate performSelectorOnMainThread:@selector(converterDidFinish:) withObject:self waitUntilDone:NO];
                                        [self.lockVideo lock];
                                        [self.lockVideo unlockWithCondition:0];
                                        
                                    }else if(exporter.status == AVAssetExportSessionStatusFailed){
                                        NSLog(@"StayLog: video convert status %ld",(long)exporter.status);
                                        NSLog(@"StayLog: video convert error %@",exporter.error.description);
                                        [(NSObject *)self.delegate performSelectorOnMainThread:@selector(converterDidFail:) withObject:self waitUntilDone:NO];
                                        [self cancel];
                                        [self.lockVideo lock];
                                        [self.lockVideo unlockWithCondition:0];
                                    }
                                    
                    
                                });
                            }];
                        });
                    }];
                    
//                    [manager requestExportSessionForVideo:self.asset
//                                                  options:options
//                                             exportPreset:AVAssetExportPreset640x480
//                                            resultHandler:^(AVAssetExportSession *exportSession, NSDictionary *info)
//                    {
//                        exportSession.outputURL = [NSURL URLWithString:[[NSString alloc] initWithFormat:@"file://%@",self.mediaPath]];
//                        //exportSession.fileLengthLimit = 40960;
//                        exportSession.fileLengthLimit = 19*1024*1024;
//                        exportSession.outputFileType = AVFileTypeQuickTimeMovie;
//                        exportSession.shouldOptimizeForNetworkUse = YES;
//                        @try {
//                            [exportSession exportAsynchronouslyWithCompletionHandler:^{
//                                if(exportSession.status == AVAssetExportSessionStatusFailed)
//                                {
//                                    NSLog(@"StayLog: video convert status %ld",(long)exportSession.status);
//                                    NSLog(@"StayLog: video convert error %@",exportSession.error.description);
//                                    [(NSObject *)self.delegate performSelectorOnMainThread:@selector(converterDidFail:) withObject:self waitUntilDone:NO];
//                                    [self cancel];
//                                    [self.lockVideo lock];
//                                    [self.lockVideo unlockWithCondition:0];
//                                }
//                                else if(exportSession.status == AVAssetExportSessionStatusCompleted)
//                                {
//                                    self.pathConvertedFile = self.mediaPath;
//                                    [(NSObject *)self.delegate performSelectorOnMainThread:@selector(converterDidFinish:) withObject:self waitUntilDone:NO];
//                                    [self.lockVideo lock];
//                                    [self.lockVideo unlockWithCondition:0];
//                                }
//                                else
//                                {
//                                    [self.lockVideo lock];
//                                    [self.lockVideo unlockWithCondition:0];
//                                }
//                            }];
//                        }
//                        @catch (NSException *exception) {
//                            NSLog(@"StayLog: EXCEPTION in Video Conversion saving file with error: %@", exception.description);
//                            [(NSObject *)self.delegate performSelectorOnMainThread:@selector(converterDidFail:) withObject:self waitUntilDone:NO];
//                            [self cancel];
//                            [self.lockVideo lock];
//                            [self.lockVideo unlockWithCondition:0];
//                        }
//                    }];
                }
                @catch (NSException *exception) {
                    NSLog(@"StayLog: EXCEPTION in Video Conversion with error: %@", exception.description);
                    [(NSObject *)self.delegate performSelectorOnMainThread:@selector(converterDidFail:) withObject:self waitUntilDone:NO];
                    [self cancel];
                    [self.lockVideo lock];
                    [self.lockVideo unlockWithCondition:0];
                }
                
                [self.lockVideo lockWhenCondition:0];
                [self.lockVideo unlock];
                
            }
            else {
                NSLog(@"StayLog: Uknown asset type");
            }
        }
        @catch (NSException *exception) {
            NSLog(@"StayLog: Operation Convert File: %@ EXCEPTION with error: %@", self.mediaPath, exception.description);
            [(NSObject *)self.delegate performSelectorOnMainThread:@selector(converterDidFail:) withObject:self waitUntilDone:NO];
            [self cancel];
        }
        
        if (self.isCancelled)
            return;
    }
}

- (UIImage *)normalizedImage: (UIImage *)image {
    if (image.imageOrientation == UIImageOrientationUp) return image;
    UIGraphicsBeginImageContextWithOptions(image.size, NO, image.scale);
    [image drawInRect:(CGRect){0, 0, image.size}];
    UIImage *normalizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return normalizedImage;
}

@end
