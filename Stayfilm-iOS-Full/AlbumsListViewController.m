//
//  AlbumsListViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 2/4/19.
//  Copyright © 2019 Stayfilm. All rights reserved.
//
@import Photos;
#import "AlbumsListViewController.h"
#import "AlbumListCell.h"

@interface AlbumsListViewController ()

@end

@implementation AlbumsListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
- (IBAction)closeAction:(id)sender {
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.collectionsAlbums.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    AlbumListCell *cell = (AlbumListCell *)[tableView dequeueReusableCellWithIdentifier:@"albumListCell"];
    NSArray *nibs = [[NSBundle mainBundle]loadNibNamed:@"AlbumListCell" owner:self options:nil];
    if(cell == nil){
        cell = nibs[0];
    }
    PHFetchOptions *options = [[PHFetchOptions alloc] init];
    options.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];
    
    PHAssetCollection *col = self.collectionsAlbums[indexPath.row];
    cell.titleLabel.text = col.localizedTitle;
    
    PHFetchResult *fetchResultAssets = [PHAsset fetchAssetsInAssetCollection:col options:options];
    cell.countLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)fetchResultAssets.count];
    
    
    PHImageManager *manager = [PHImageManager defaultManager];
    
    
    
    [manager requestImageForAsset:(PHAsset*)fetchResultAssets.firstObject
                       targetSize:cell.albumImage.frame.size
                      contentMode:PHImageContentModeDefault
                          options:nil
                    resultHandler:^void(UIImage *image, NSDictionary *info) {
                        [cell.albumImage setImage:image];
                    }];
    
    
    return cell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80.0;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    [self.delegate albumSelected:self.collectionsAlbums[(int)indexPath.row]];
    [self closeAction:nil];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
