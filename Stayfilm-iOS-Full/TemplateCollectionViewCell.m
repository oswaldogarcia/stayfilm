//
//  TemplateCollectionViewCell.m
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 22/05/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import "TemplateCollectionViewCell.h"
@import KALoader;

@implementation TemplateCollectionViewCell


- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.frameTemplateImage setHidden:YES];
    
    
    
}

- (void)initCell{
    [self.templateImage showLoader];
    
    if (self.isBlocked){
        [self.blockedView setHidden:NO];
        [self.frameTemplateImage setHidden:YES];
    }else{
        
        [self.blockedView setHidden:YES];
    }
    self.unlockButton.layer.borderWidth = 1;
    self.unlockButton.layer.borderColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0].CGColor;

    [self.unlockButton setTitle:NSLocalizedString(@"UNLOCK", nil) forState:UIControlStateNormal];
    
    UIImage *image;
    if(self.isVertical){
        image = [UIImage imageNamed:@"frameStyleSmall"];
    }else{
        image = [UIImage imageNamed:@"transparentFrame"];
    }
    [self.frameTemplateImage setImage:image];
    
    
    
        
    [self.templateImage sd_setImageWithURL:[NSURL URLWithString: self.currentTemplate.image]  placeholderImage:nil options:SDWebImageRefreshCached progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL * _Nullable targetURL) {/*...*/ } completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        if([self.templateImage isLoaderShowing]){
            [self.templateImage hideLoader];
        }
        if (image != nil){
            [self.templateImage setImage:image];
        }else{
            [self.templateImage setImage: [UIImage imageNamed:@"Select_VideoGradient"]];
        }
    }];
    
    
}

- (void)setSelected:(BOOL)selected{
    
    
    if(!self.isBlocked){
        if(selected){
            
            [UIView animateWithDuration:0.8 animations:^{
                [self.frameTemplateImage setHidden:NO];
            }];
            
        }else{
            [UIView animateWithDuration:0.8 animations:^{
                
                [self.frameTemplateImage setHidden:YES];
            }];
        }
    }
}

-(void)changeSize{
    
    
    [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, 91, self.frame.size.height)];
    
    [self.frameTemplateImage setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, 91, self.frame.size.height)];
    
     [self.templateImage setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, 90, self.frame.size.height)];
}
- (IBAction)unlockAction:(id)sender {
    
    [self.delegate unlockTemplate:self.currentTemplate];
}


@end
