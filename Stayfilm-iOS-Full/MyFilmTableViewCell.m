//
//  MyFilmTableViewCell.m
//  Stayfilm-iOS-Full
//
//  Created by Henrique on 3/1/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import "MyFilmTableViewCell.h"

@implementation MyFilmTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
