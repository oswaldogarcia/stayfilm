//
//  RecoverCodeViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Henrique on 4/26/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecoverCodeViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *img_Background;
@property (weak, nonatomic) IBOutlet UILabel *lbl_TextTitle;
@property (weak, nonatomic) IBOutlet UITextField *txt_Code;
@property (weak, nonatomic) IBOutlet UIButton *but_EnterCode;
@property (weak, nonatomic) IBOutlet UIButton *but_ResendEmail;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Status;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner_loading;


@property (nonatomic, strong) NSString *email;

@end
