//
//  RecoverResetViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Henrique on 5/1/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <Google/Analytics.h>
#import "RecoverResetViewController.h"
#import "StayfilmApp.h"
#import "Reachability.h"
#import "User.h"
#import "RecoverConfirmationViewController.h"

@interface RecoverResetViewController ()

@property (nonatomic, weak) StayfilmApp *sfApp;
@property (nonatomic, assign) BOOL isResetting;
@property (nonatomic, assign) BOOL isMovingToConfirmation;
@property (nonatomic,strong) UITapGestureRecognizer *tapGest;

@end

@implementation RecoverResetViewController

@synthesize txt_NewPassword, txt_ConfirmPassword, but_Save, lbl_Status, spinner_loader, img_Background, email, code;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.txt_NewPassword addTarget:self action:@selector(txt_NewPassword_Ended) forControlEvents:UIControlEventEditingDidEndOnExit];
    [self.txt_ConfirmPassword addTarget:self action:@selector(txt_ConfirmPassword_Ended) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    self.isMovingToConfirmation = NO;
    self.isResetting = NO;
    
    self.tapGest = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backgroundTapped:)];
    self.tapGest.numberOfTapsRequired = 1;
    [self.tapGest setCancelsTouchesInView:NO];
    [self.img_Background addGestureRecognizer:self.tapGest];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    //Google Analytics
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"new-password"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

-(void)backgroundTapped:(UIGestureRecognizer *)gesture {
    [self.view endEditing:YES];
}

- (IBAction)backButton_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)connected {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

-(void)txt_NewPassword_Ended {
    if(self.txt_NewPassword.text.length < 5) {
        self.lbl_Status.alpha = 0.0;
        [self.lbl_Status setHidden:NO];
        self.lbl_Status.text = NSLocalizedString(@"PASSWORD_MIN_ERROR", nil);
        [UIView animateWithDuration:0.3 animations:^{
            self.lbl_Status.alpha = 1.0;
        }];
        [self.txt_NewPassword becomeFirstResponder];
    }
    else {
        [self.txt_ConfirmPassword becomeFirstResponder];
    }
}

-(void)txt_ConfirmPassword_Ended {
    [self.view endEditing:YES];
    
    if(![self.txt_NewPassword.text isEqualToString:self.txt_ConfirmPassword.text] && self.txt_NewPassword.text.length > 0) {
        self.lbl_Status.alpha = 0.0;
        [self.lbl_Status setHidden:NO];
        self.lbl_Status.text = NSLocalizedString(@"PASSWORD_MISSMATCH", nil);
        [UIView animateWithDuration:0.3 animations:^{
            self.lbl_Status.alpha = 1.0;
        }];
    } else if([self.txt_NewPassword.text isEqualToString:self.txt_ConfirmPassword.text] && self.txt_NewPassword.text.length > 0) {
        self.lbl_Status.text = @"";
    }
}

- (IBAction)but_Save_Clicked:(id)sender {
    if(![self connected])
    {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CONNECTION_ERROR", nil)
                                                                       message:NSLocalizedString(@"CONNECTION_ERROR_MESSAGE", nil)
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    if(self.sfApp == nil) {
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    }
    [self.view endEditing:YES];
    self.lbl_Status.text = @"";
    
    if(self.txt_NewPassword.text == nil || [self.txt_NewPassword.text isEqualToString:@""] ||
       self.txt_ConfirmPassword.text == nil || [self.txt_ConfirmPassword.text isEqualToString:@""]) {
        self.lbl_Status.alpha = 0.0;
        [self.lbl_Status setHidden:NO];
        self.lbl_Status.text = NSLocalizedString(@"INVALID_RECOVER_CODE_EMPTY", nil);
        [UIView animateWithDuration:0.3 animations:^{
            self.lbl_Status.alpha = 1.0;
        }];
        [self.spinner_loader setHidden:YES];
        return;
    }
    else if([self.txt_NewPassword.text isEqualToString:self.txt_ConfirmPassword.text]) {
        if(self.txt_NewPassword.text.length < 5) {
            self.lbl_Status.alpha = 0.0;
            [self.lbl_Status setHidden:NO];
            self.lbl_Status.text = NSLocalizedString(@"PASSWORD_MIN_ERROR", nil);
            [UIView animateWithDuration:0.3 animations:^{
                self.lbl_Status.alpha = 1.0;
            }];
            return;
        }
        else { // Password is OK
            
            if(!self.isResetting) {
                self.isResetting = YES;
                [self.spinner_loader setHidden:NO];
                [self performSelectorInBackground:@selector(resetPassword) withObject:nil];
            }
        }
    }
    else if(![self.txt_NewPassword.text isEqualToString:self.txt_ConfirmPassword.text]  && self.txt_NewPassword.text.length > 0) {
        self.lbl_Status.alpha = 0.0;
        [self.lbl_Status setHidden:NO];
        self.lbl_Status.text = NSLocalizedString(@"PASSWORD_MISSMATCH", nil);
        [UIView animateWithDuration:0.3 animations:^{
            self.lbl_Status.alpha = 1.0;
        }];
    }
}

-(void)resetPassword {
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"password"
                                                          action:@"new"
                                                           label:@"save"
                                                           value:@1] build]];
    
    NSString *response = [User sendRecoveryCode:self.code withEmail:self.email withNewPassword:self.txt_NewPassword.text andSFAppSingleton:self.sfApp];
    
    if(response == nil) { //Valid response
        [self performSelectorOnMainThread:@selector(moveToNextStep) withObject:nil waitUntilDone:NO];
        //[self performSegueWithIdentifier:@"ResetToConfirmationSegue" sender:self];
    }
    else if ([response isEqualToString:@"exception"]) {
        runOnMainQueueWithoutDeadlocking(^{
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CONNECTION_ERROR", nil)
                                                                           message:NSLocalizedString(@"CONNECTION_ERROR_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) { }];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        });
    }
    else {
        runOnMainQueueWithoutDeadlocking(^{
            self.lbl_Status.alpha = 0.0;
            [self.lbl_Status setHidden:NO];
            self.lbl_Status.text = NSLocalizedString(@"INVALID_RECOVER_CODE_ERROR", nil);
            [UIView animateWithDuration:0.3 animations:^{
                self.lbl_Status.alpha = 1.0;
            }];
        });
    }
    runOnMainQueueWithoutDeadlocking(^{
        [self.spinner_loader setHidden:YES];
    });
    self.isResetting = NO;
}

-(void)moveToNextStep {
    [self performSegueWithIdentifier:@"ResetToConfirmationSegue" sender:self];
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"ResetToConfirmationSegue"] && !self.isMovingToConfirmation) {
        self.isMovingToConfirmation = YES;
        RecoverConfirmationViewController *recoverConfirmation = segue.destinationViewController;
        recoverConfirmation.email = self.email;
        recoverConfirmation.password = self.txt_NewPassword.text;
        self.isMovingToConfirmation = NO;
    }
}

@end
