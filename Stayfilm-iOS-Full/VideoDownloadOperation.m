//
//  VideoDownloadOperation.m
//  Stayfilm for Messenger
//
//  Created by Henrique Ormonde on 10/11/15.
//  Copyright © 2015 Stayfilm. All rights reserved.
//

#import "VideoDownloadOperation.h"
#import "StayfilmApp.h"
@import Photos;

@interface VideoDownloadOperation ()

@property (nonatomic, weak) StayfilmApp *sfApp;
@property (nonatomic, strong) NSURLSessionTask *downloadDataTask;
@property (nonatomic, retain) NSMutableURLRequest *request;
@property (nonatomic, retain) NSMutableData *responseData;
@property (nonatomic, strong) NSConditionLock *lock;

@property (nonatomic, assign) float expectedSize;

@end

@implementation VideoDownloadOperation

@synthesize sfApp, delegate, sessionURLBackground, downloadDataTask, responseData, pathFile, movie, progress, expectedSize, lock;

- (id)initWithMovie:(Movie *)p_movie withBackgroundSessionIdentifier:(NSString *)identifier savingToPhotosFolder:(BOOL)p_savePhotos delegate:(id<VideoDownloaderDelegate>) theDelegate {
    if (self = [super init]) {
        self.delegate = theDelegate;
        self.movie = p_movie;
        self.pathFile = nil;
        self.responseData = nil;
        self.downloadDataTask = nil;
        self.lock = nil;
        self.progress = 0.0f;
        self.expectedSize = 0.0f;
        self.saveToPhotosFolder = p_savePhotos;
        
        NSURLSessionConfiguration *sessionConfigBackground = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:identifier];
        sessionConfigBackground.allowsCellularAccess = YES;
        //[sessionConfigBackground setHTTPAdditionalHeaders:@{@"Accept": @"*/*"}];
        sessionConfigBackground.timeoutIntervalForRequest = 30.0;
        sessionConfigBackground.timeoutIntervalForResource = 240.0;
        sessionConfigBackground.HTTPMaximumConnectionsPerHost = 2;
        
        self.sessionURLBackground = [NSURLSession sessionWithConfiguration:sessionConfigBackground
                                                                  delegate:self
                                                             delegateQueue:[NSOperationQueue mainQueue]];
    }
    return self;
}

-(void)updateDownloadProgress {
    [(NSObject *)self.delegate performSelectorOnMainThread:@selector(downloadProgressChanged:) withObject:self waitUntilDone:NO];
}

#pragma mark -
#pragma mark - Download Video

- (void)main {
    @autoreleasepool {
        
        if (self.isCancelled)
            return;
        
        @try
        {
            self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];

            BOOL waitingStep = YES;
            while (waitingStep) {
                [NSThread sleepForTimeInterval:0.5];
                waitingStep = ![self.sfApp isLoggedIn];
            }
            
            self.lock = nil;
            self.lock = [[NSConditionLock alloc] initWithCondition:1];
            
            NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = ([paths objectAtIndex:0] != nil)? [paths objectAtIndex:0] : nil;
            NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/StayFilms"];
            
            //Create Stayfilm folder if it does not exist
            NSError *error;
            if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
            {
                [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
            }
            
            NSString  *filePath = [NSString stringWithFormat:@"%@/%@", dataPath, [self.movie.idMovie stringByAppendingString:@".mp4"]];
            
            if (![[NSFileManager defaultManager] fileExistsAtPath:filePath])
            {
                self.pathFile = filePath;
                self.request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:self.movie.videoUrl]];
                self.downloadDataTask = [self.sessionURLBackground dataTaskWithRequest:self.request];
                [self.downloadDataTask resume];
                [self.sessionURLBackground finishTasksAndInvalidate];
            }
            else
            {
                //File already exists, no need to download again
                self.pathFile = filePath;
                self.progress = 0.0f;
                if(UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(self.pathFile)) {
//                    UISaveVideoAtPathToSavedPhotosAlbum(self.pathFile,nil,nil,nil);
                    NSLog(@"StayLog: Movie already downloaded and just saved to Camera Roll");
                    
                }
                [(NSObject *)self.delegate performSelectorOnMainThread:@selector(downloadDidFinish:) withObject:self waitUntilDone:NO];
            }
            
            [self.lock lockWhenCondition:0];
            [self.lock unlock];
            self.lock = nil;
            
        }
        @catch (NSException *exception) {
            NSLog(@"StayLog: Operation Download EXCEPTION with error: %@", exception.description);
            [(NSObject *)self.delegate performSelectorOnMainThread:@selector(downloadDidFail:) withObject:self waitUntilDone:NO];
            [self.lock lockWhenCondition:0];
            [self.lock unlock];
            [self cancel];
        }
        
        if (self.isCancelled) {
            [self cleanupSession];
            [self cancel];
            return;
        }
    }
}

-(void)cleanupSession
{
    [self.sessionURLBackground finishTasksAndInvalidate];
    [self.sessionURLBackground resetWithCompletionHandler:^{
        self.sessionURLBackground = nil;
    }];
    [self.downloadDataTask cancel];
    self.downloadDataTask = nil;
    self.responseData = nil;
}


#pragma URL Delegates


- (void)URLSession:(NSURLSession *)session
          dataTask:(NSURLSessionDataTask *)dataTask
didReceiveResponse:(NSURLResponse *)response
 completionHandler:(void (^)(NSURLSessionResponseDisposition disposition))completionHandler {
    completionHandler(NSURLSessionResponseAllow);
    
    self.responseData = [[NSMutableData alloc] init];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    self.progress = 0.0f;
    self.expectedSize = [response expectedContentLength];
//    NSLog(@"%f expected  and %@ response header",self.expectedSize, [[(NSHTTPURLResponse *)response allHeaderFields] valueForKey:@"Content-Length"]);
}

- (void)URLSession:(NSURLSession *)session
          dataTask:(NSURLSessionDataTask *)dataTask
    didReceiveData:(NSData *)data {
    if (self.isCancelled) {
        [dataTask cancel];
        NSLog(@"StayLog: Download Operation CANCELED for media: %@", self.movie.idMovie);
        [self cleanupSession];
    }
    
    [self.responseData appendData:data];
    self.progress = self.responseData.length / self.expectedSize;
    [self updateDownloadProgress];
}

- (void)URLSession:(NSURLSession *)session
              task:(NSURLSessionTask *)task
didCompleteWithError:(NSError *)error
{
    if(error)
    {
        NSLog(@"StayLog: Download Operation for media: %@ called from URLSession FAILED with ERROR: %@", self.movie.idMovie, error.description);
        if ([[error.description uppercaseString] containsString:@"CANCELLED"]) {
            [self.lock lock];
            [self.lock unlockWithCondition:0];
            [self cleanupSession];
            [self cancel];
            [(NSObject *)self.delegate performSelectorOnMainThread:@selector(downloadDidCancel:) withObject:self waitUntilDone:NO];
            return;
        }
        [(NSObject *)self.delegate performSelectorOnMainThread:@selector(downloadDidFail:) withObject:self waitUntilDone:NO];
        [self.lock lock];
        [self.lock unlockWithCondition:0];
        [self cleanupSession];
        [self cancel];
    }
    else
    {
        @try
        {
            if(self.isCancelled)
            {
                NSLog(@"StayLog: Download Operation FINISHED but was CANCELED with media: %@", self.movie.idMovie);
                [self.lock lock];
                [self.lock unlockWithCondition:0];
                [self cleanupSession];
                return;
            }
            
            self.progress = 1.0f;
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            NSLog(@"StayLog: Download Successful! Received %lu bytes of data",(unsigned long)[self.responseData length]);
            
            BOOL success = [self.responseData writeToFile:self.pathFile atomically:YES];
            if(success) {
                if(self.saveToPhotosFolder && UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(self.pathFile)) {
//                    UISaveVideoAtPathToSavedPhotosAlbum(self.pathFile,nil,nil,nil);
//                    NSLog(@"StayLog: Saved to Photos Folder");
                    NSString *collectionTitle = @"Stayfilm";
                    // Find the album
                    PHFetchOptions *fetchOptions = [[PHFetchOptions alloc] init];
                    fetchOptions.predicate = [NSPredicate predicateWithFormat:@"title = %@", collectionTitle];
                    PHAssetCollection *collection = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum subtype:PHAssetCollectionSubtypeAny options:fetchOptions].firstObject;
                    
                    // Combine (possible) album creation and adding in one change block
                    [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
                        PHAssetCollectionChangeRequest *albumRequest;
                        if (collection == nil) {
                            // create the album if it doesn't exist
                            albumRequest = [PHAssetCollectionChangeRequest creationRequestForAssetCollectionWithTitle:collectionTitle];
                        } else {
                            // otherwise request to change the existing album
                            albumRequest = [PHAssetCollectionChangeRequest changeRequestForAssetCollection:collection];
                        }
                        // add the asset to the album
                        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
                            PHAssetChangeRequest *assetChangeRequest = [PHAssetChangeRequest creationRequestForAssetFromVideoAtFileURL:[NSURL URLWithString: self.pathFile]];
                            
                            // add asset
                            PHAssetCollectionChangeRequest *assetCollectionChangeRequest = [PHAssetCollectionChangeRequest changeRequestForAssetCollection:collection];
                            [assetCollectionChangeRequest addAssets:@[[assetChangeRequest placeholderForCreatedAsset]]];
                        } completionHandler:^(BOOL success, NSError *error) {
                            if (!success) {
                                NSLog(@"Error: %@", error);
                            }else{
                                 NSLog(@"StayLog: Saved to Stayfilm Album");
                            }
                        }];
                        
                    } completionHandler:^(BOOL success, NSError *error) {
                        if (!success) {
                            NSLog(@"error creating or adding to album: %@", error);
                        }
                    }];
                    [(NSObject *)self.delegate performSelectorOnMainThread:@selector(downloadDidFinish:) withObject:self waitUntilDone:NO];
                }
                else {
                    NSLog(@"StayLog: Donwloaded but did not save in Photos Folder");
                    [(NSObject *)self.delegate performSelectorOnMainThread:@selector(downloadDidFinish:) withObject:self waitUntilDone:NO];
                }
            }
            else {
                NSLog(@"StayLog: Error writing to File: %@", self.pathFile);
                if([[NSFileManager defaultManager] isDeletableFileAtPath:self.pathFile]) {
                    [[NSFileManager defaultManager] removeItemAtPath:self.pathFile error:nil];
                }
                [(NSObject *)self.delegate performSelectorOnMainThread:@selector(downloadDidFail:) withObject:self waitUntilDone:NO];
            }
            
            [self.lock lock];
            [self.lock unlockWithCondition:0];
            [self cleanupSession];
            [self cancel];
        }
        @catch (NSException *exception) {
            NSLog(@"StayLog: EXCEPTION Downloaded File with error: %@", exception.description);
            
            [(NSObject *)self.delegate performSelectorOnMainThread:@selector(downloadDidFail:) withObject:self waitUntilDone:NO];
            [self.lock lock];
            [self.lock unlockWithCondition:0];
            [self cleanupSession];
            [self cancel];
        }
        
    }
}


@end
