//
//  StoriesContentViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 1/10/19.
//  Copyright © 2019 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVKit/AVKit.h>
#import "Movie.h"
#import "SegmentedProgressBar.h"
#import "SFHelper.h"
#import "StayfilmApp.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <Google/Analytics.h>
#import "UIView+Toast.h"
#import "PlayerViewController.h"

NS_ASSUME_NONNULL_BEGIN

@protocol StoriesContentDelegate<NSObject>

@property (nonatomic, assign) BOOL isSwipeUP;

- (void)moveToNext;
- (void)moveToBack;
- (void)makeAfilmButtonTapped;
- (void)setViewedId:(NSNumber *)idStory onId:(NSNumber *)idconten;
@end

@interface StoriesContentViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *avplayerContainer;
@property (weak, nonatomic) IBOutlet UIImageView *storyLogoImage;
@property (weak, nonatomic) IBOutlet UIView *swipeUpView;
@property (weak, nonatomic) IBOutlet UILabel *contentDescriptionLabel;

@property (weak, nonatomic) IBOutlet UIView *makeAFilmView;
@property (weak, nonatomic) IBOutlet UIImageView *makeAFilmImage;
@property (weak, nonatomic) IBOutlet UILabel *makeAFilmLabel;
@property (weak, nonatomic) IBOutlet UIButton *makeAFilmButton;


@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;



@property (nonatomic, strong) NSMutableArray *storiesContent;
@property (nonatomic, strong) NSMutableArray *myPlayers;


@property (nonatomic, strong) Story *story;
@property (nonatomic, strong) StoryContent *storyContent;
@property (nonatomic) int indexConten;
@property (nonatomic) int currentConten;
@property (nonatomic) float storyDuration;
@property (nonatomic,strong)  AVPlayerViewController *playerVC;
@property (nonatomic, retain) IBOutlet SegmentedProgressBar *segmentedProgressBar;

@property(nonatomic, weak) StayfilmApp *sfApp;


@property (nonatomic) int timeTick;

@property (nonatomic, assign) BOOL isImage;
@property (nonatomic, assign) BOOL isSwipeUP;
@property (nonatomic, assign) BOOL isLongPress;
@property (nonatomic, assign) BOOL isExiting;

@property (nonatomic, assign) id<StoriesContentDelegate> delegate;
@end

NS_ASSUME_NONNULL_END
