//
//  LoginToSocialNetworkOperation.m
//  Stayfilm-iOS-Full
//
//  Created by Henrique Ormonde on 15/09/17.
//  Copyright © 2017 Stayfilm. All rights reserved.
//

#import "LoginToSocialNetworkOperation.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>


@interface LoginToSocialNetworkOperation ()

@property (nonatomic, strong) NSArray *facebookPermissions;

@end


@implementation LoginToSocialNetworkOperation

@synthesize facebookPermissions, messageCode, socialNetwork, delegate, sfAppLogin;


#pragma mark - 
#pragma mark - Callable Methods

-(id)initWithDelegate:(id<LoginToSocialNetworkOperationDelegate>)p_delegate andSFApp:(StayfilmApp *)p_sfApp forSocialNetwork:(NSInteger )p_socialNetwork {
    if( self = [super init] ) {
        self.delegate = p_delegate;
        self.sfAppLogin = p_sfApp;
        self.socialNetwork = p_socialNetwork;
        self.facebookPermissions = @[@"public_profile", @"email", @"user_photos", @"user_birthday", @"user_videos", @"user_friends"];
        self.messageCode = -1;
    }
    return self;
}



#pragma mark - Delegate Methods

-(void)loginSocialWebviewDidSucceed:(NSString *)token
{
    @try {
        if(token != nil)
        {
            switch (self.socialNetwork) {
                case SOCIAL_INSTAGRAM:
                {
                    BOOL success = [self.sfAppLogin.currentUser setUserInstagramToken:token andSFAppSingleton:self.sfAppLogin];
                    if(success)
                    {
                        [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginToSocialNetworkDidSucceed:) withObject:self waitUntilDone:NO];
                    }
                    else
                    {
                        [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginToSocialNetworkDidFail:) withObject:self waitUntilDone:NO];
                    }
                }
                    break;
                case SOCIAL_TWITTER:
                {
                    BOOL success = [self.sfAppLogin.currentUser setUserTwitterToken:token andSFAppSingleton:self.sfAppLogin];
                    if(success)
                    {
                        [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginToSocialNetworkDidSucceed:) withObject:self waitUntilDone:NO];
                    }
                    else
                    {
                        [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginToSocialNetworkDidFail:) withObject:self waitUntilDone:NO];
                    }
                }
                    break;
                case SOCIAL_GOOGLE:
                {
                    BOOL success = [self.sfAppLogin.currentUser setUserGoogleToken:token andSFAppSingleton:self.sfAppLogin];
                    if(success)
                    {
                        [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginToSocialNetworkDidSucceed:) withObject:self waitUntilDone:NO];
                    }
                    else
                    {
                        [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginToSocialNetworkDidFail:) withObject:self waitUntilDone:NO];
                    }
                }
                    break;
                    
                default:
                    break;
            }
        }
    } @catch (NSException *exception) {
        NSLog(@"StayLog: EXCEPTION in LoginToSocialNetworkOparation - loginSocialWebviewDidSucceed with error: %@", exception.description);
        self.messageCode = LOGIN_EXCEPTION;
        [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginToSocialNetworkDidFail:) withObject:self waitUntilDone:NO];
    }
}

-(void)loginSocialWebviewDidFail:(NSString *)error
{
    [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginToSocialNetworkDidFail:) withObject:self waitUntilDone:NO];
    [((UIViewController *)self.delegate) dismissViewControllerAnimated:YES completion:nil];
}

-(void)loginFacebookDidFinish:(LoginFacebookOperation *)loginOP
{
    self.messageCode = loginOP.messageCode;
    [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginToSocialNetworkDidSucceed:) withObject:self waitUntilDone:NO];
}

-(void)loginFacebookDidFail:(LoginFacebookOperation *)loginOP
{
    self.messageCode = loginOP.messageCode;
    [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginToSocialNetworkDidFail:) withObject:self waitUntilDone:NO];
}

#pragma mark - 
#pragma mark - Main

-(void)main
{
    if(self.cancelled)
        return;
    
    @try {
        if(self.sfAppLogin == nil)
            self.sfAppLogin = [StayfilmApp sharedStayfilmAppSingleton];
        
        // Update User logged in networks
        if(self.sfAppLogin.currentUser.networks == nil)
            [self.sfAppLogin.currentUser getUserNetworksWithSFAppSingleton:self.sfAppLogin];
        
        switch (self.socialNetwork)
        {
            case SOCIAL_FACEBOOK:
                {
                    if(![self.sfAppLogin.currentUser.networks containsObject:@"facebook"])
                    {
                        LoginFacebookOperation *loginFBop = [[LoginFacebookOperation alloc] initWithDelegate:self andSFApp:self.sfAppLogin];
                        self.sfAppLogin.operationsQueue.qualityOfService = NSQualityOfServiceUserInitiated;
                        [self addDependency:loginFBop];
                        [self.sfAppLogin.operationsQueue addOperation:loginFBop];
                    }
                    else
                    {
                        self.messageCode = LOGIN_SUCCESSFUL;
                        [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginToSocialNetworkDidSucceed:) withObject:self waitUntilDone:NO];
                    }
                }
                break;
                
            case SOCIAL_INSTAGRAM:
                {
                    if(![self.sfAppLogin.currentUser.networks containsObject:@"instagram"])
                    {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            LoginWebView *loginWebView = [[LoginWebView alloc] initWithNibName:@"LoginWebView" bundle:[NSBundle mainBundle]];
                            //LoginWebView *loginWebView = [((UIViewController *)self.delegate).storyboard instantiateViewControllerWithIdentifier:@"LoginWebView"];
                            loginWebView.delegate = self.delegate;
                            loginWebView.loginURL = [[NSURL alloc] initWithString:[[NSString alloc] initWithString:([NSString stringWithFormat:[self.sfAppLogin.wsConfig getConfigPathWithKey:@"loginSocial"], @"instagram", self.sfAppLogin.currentUser.idSession])]];
                            loginWebView.socialNetwork = @"instagram";
                            [((UIViewController *)self.delegate).navigationController pushViewController:loginWebView animated:YES];
                        });
                    }
                    else
                    {
                        self.messageCode = LOGIN_SUCCESSFUL;
                        [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginToSocialNetworkDidSucceed:) withObject:self waitUntilDone:NO];
                    }
                }
                break;
            case SOCIAL_GOOGLE:
                {
                    if(![self.sfAppLogin.currentUser.networks containsObject:@"gplus"])
                    {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            LoginWebView *loginWebView = [[LoginWebView alloc] initWithNibName:@"LoginWebView" bundle:[NSBundle mainBundle]];
                            //LoginWebView *loginWebView = [((UIViewController *)self.delegate).storyboard instantiateViewControllerWithIdentifier:@"LoginWebView"];
                            loginWebView.delegate = self.delegate;
                            loginWebView.loginURL = [[NSURL alloc] initWithString:[[NSString alloc] initWithString:([NSString stringWithFormat:[self.sfAppLogin.wsConfig getConfigPathWithKey:@"loginSocial"], @"gplus", self.sfAppLogin.currentUser.idSession])]];
                            loginWebView.socialNetwork = @"gplus";
                            [((UIViewController *)self.delegate).navigationController pushViewController:loginWebView animated:YES];
                        });
                    }
                    else
                    {
                        self.messageCode = LOGIN_SUCCESSFUL;
                        [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginToSocialNetworkDidSucceed:) withObject:self waitUntilDone:NO];
                    }
                }
                break;
            case SOCIAL_TWITTER:
            {
                if(![self.sfAppLogin.currentUser.networks containsObject:@"twitter"])
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        LoginWebView *loginWebView = [[LoginWebView alloc] initWithNibName:@"LoginWebView" bundle:[NSBundle mainBundle]];
                        //LoginWebView *loginWebView = [((UIViewController *)self.delegate).storyboard instantiateViewControllerWithIdentifier:@"LoginWebView"];
                        loginWebView.delegate = self.delegate;
                        loginWebView.loginURL = [[NSURL alloc] initWithString:[[NSString alloc] initWithString:([NSString stringWithFormat:[self.sfAppLogin.wsConfig getConfigPathWithKey:@"loginSocial"], @"twitter", self.sfAppLogin.currentUser.idSession])]];
                        loginWebView.socialNetwork = @"twitter";
                        [((UIViewController *)self.delegate) presentViewController:loginWebView animated:YES completion:nil];
                        //[((UIViewController *)self.delegate).navigationController pushViewController:loginWebView animated:YES];
                    });
                }
                else
                {
                    self.messageCode = LOGIN_SUCCESSFUL;
                    [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginToSocialNetworkDidSucceed:) withObject:self waitUntilDone:NO];
                }
            }
                break;
                
            default:
                break;
        }
    } @catch (NSException *exception) {
        NSLog(@"StayLog: EXCEPTION in main - LoginToSocialNetworkOperation with error: %@", exception.description);
        self.messageCode = LOGIN_EXCEPTION;
        [(NSObject *)self.delegate performSelectorOnMainThread:@selector(loginToSocialNetworkDidFail:) withObject:self waitUntilDone:NO];
    }
}

@end
