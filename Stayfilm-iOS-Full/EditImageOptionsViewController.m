//
//  EditImageOptionsViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 9/05/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <Google/Analytics.h>
#import "EditImageOptionsViewController.h"

@interface EditImageOptionsViewController ()

@end

@implementation EditImageOptionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setLanguage];
    
    UITapGestureRecognizer *modalTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOutside)];
    modalTap.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:modalTap];
    
    self.choosePictureButton.layer.borderWidth = 1.0f;
    self.choosePictureButton.layer.borderColor =[UIColor colorWithRed:0.87 green:0.86 blue:0.87 alpha:1.0].CGColor;
    
    self.choosePictureButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.takePictureButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
}
-(void)tapOutside{
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"edit-profile"
                                                          action:@"change-photo"
                                                           label:@"cancel"
                                                           value:@1] build]];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)setLanguage{
    
    
    [self.takePictureButton setTitle:NSLocalizedString(@"TAKE_PICTURE", nil) forState:normal];
    [self.choosePictureButton setTitle:NSLocalizedString(@"CHOOSE_PICTURE", nil) forState:normal];
    [self.cancelButton setTitle:NSLocalizedString(@"CANCEL", nil) forState:normal];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)takePictureAction:(id)sender {
    [self tapOutside];
    [self.delegate didSelectTakePicture];
}



- (IBAction)choosePictureAction:(id)sender {
    [self tapOutside];
    [self.delegate didSelectChoosePicture];
}


- (IBAction)cancelAction:(id)sender {
    
    
    
    [self tapOutside];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
