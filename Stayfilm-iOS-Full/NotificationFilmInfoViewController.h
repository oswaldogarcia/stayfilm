//
//  NotificationFilmInfoViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 12/11/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NotificationFilmInfoViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *termsLabel;

@property (weak, nonatomic) IBOutlet UILabel *noSmokingLabel;
@property (weak, nonatomic) IBOutlet UILabel *noIllegalLabel;
@property (weak, nonatomic) IBOutlet UILabel *noCommercialLabel;

@property (weak, nonatomic) IBOutlet UILabel *inReviewLabel;
@property (weak, nonatomic) IBOutlet UILabel *inReviewTextLabel;

@property (weak, nonatomic) IBOutlet UILabel *editedLabel;
@property (weak, nonatomic) IBOutlet UILabel *editedTextLabel;

@property (weak, nonatomic) IBOutlet UIButton *dismissButton;

@end

NS_ASSUME_NONNULL_END
