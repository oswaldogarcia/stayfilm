//
//  StorieViewController.h
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 13/06/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ProfileStoryDelegate;

@interface StorieViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *storiesPageContainer;
@property (nonatomic, strong) NSMutableArray *myFilms;
@property (nonatomic, strong) NSMutableArray *storiesArray;
@property (nonatomic) int storySelected;
@property (nonatomic, strong) NSMutableArray *assetContentOfStorySelected;
@property (weak, nonatomic) IBOutlet UIImageView *storyLogoImage;
@property (weak, nonatomic) IBOutlet UILabel *storyDescriptionLabel;

@property (weak, nonatomic) IBOutlet UIView *swipeUpView;

@property (weak, nonatomic) IBOutlet UIButton *makeAFilmButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iPhoneXSizeFix;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *xConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *iconButtonImage;
@property (weak, nonatomic) IBOutlet UILabel *buttonTextLabel;
@property (weak, nonatomic) IBOutlet UIView *buttonView;
@property (weak, nonatomic) IBOutlet UIView *view_connectionStatus;
@property (weak, nonatomic) IBOutlet UILabel *lbl_connectionStatus;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;

@property (nonatomic, assign) id <ProfileStoryDelegate> delegate;

@end


@protocol ProfileStoryDelegate <NSObject>
- (void)exitProfile;
@end
