//
//  Media.m
//  Stayfilm for Messenger
//
//  Created by Henrique Ormonde on 22/05/15.
//  Copyright (c) 2015 Stayfilm. All rights reserved.
//

#import "Media.h"
#import "SFConfig.h"
#import "StayfilmApp.h"

@implementation Media

@synthesize idMidia, idAlbum, idSocialNetwork, name, cover, source, thumbnail, width, height, isVideo;

StayfilmApp *sfAppMedia;

-(id)init
{
    self = [super init];
    if (0 != self) {
        self.idMidia = nil;
        self.idAlbum = nil;
        self.isVideo = NO;
    }
    return self;
}

+ (id)customClassWithProperties:(NSDictionary *)properties {
    if(properties != nil)
    {
        NSMutableDictionary *filteredObjs = [[NSMutableDictionary alloc] init];
        for (NSString* key in properties) {
            id value = [properties objectForKey:key];
            if ([key isEqualToString:@"idMidia"]) {
                [filteredObjs setValue:value forKey:@"idMidia"];
            }
            else if ([key isEqualToString:@"idAlbum"]) {
                [filteredObjs setValue:value forKey:@"idAlbum"];
            }
            else if ([key isEqualToString:@"idSocialNetwork"]) {
                [filteredObjs setValue:value forKey:@"idSocialNetwork"];
            }
            else if ([key isEqualToString:@"name"]) {
                [filteredObjs setValue:value forKey:@"name"];
            }
            else if ([key isEqualToString:@"cover"]) {
                [filteredObjs setValue:value forKey:@"cover"];
            }
            else if ([key isEqualToString:@"source"]) {
                [filteredObjs setValue:value forKey:@"source"];
            }
            else if ([key isEqualToString:@"thumbnail"]) {
                [filteredObjs setValue:value forKey:@"thumbnail"];
            }
            else if ([key isEqualToString:@"width"]) {
                [filteredObjs setValue:value forKey:@"width"];
            }
            else if ([key isEqualToString:@"height"]) {
                [filteredObjs setValue:value forKey:@"height"];
            }
        }
        return [[self alloc] initWithProperties:filteredObjs];
    }
    else
    {
        return nil;
    }
}

- (id)initWithProperties:(NSDictionary *)properties {
    if (self = [self init]) {
        @try {
            [self setValuesForKeysWithDictionary:properties];
        }
        @catch (NSException *ex) {
            return nil;
        }
    }
    return self;
}


+(NSMutableArray *)getMediasByAlbum:(NSString *)p_idAlbum
{
    @try
    {
        StayWS *ws = [[StayWS alloc] init];
        sfAppMedia = [StayfilmApp sharedStayfilmAppSingleton];
        NSDictionary *args = nil;
        NSDictionary *response = nil;
        NSArray *pathArgs = nil;
        NSMutableArray *medias = [[NSMutableArray alloc] init];
        
        args = @{ @"albums" : p_idAlbum };
        
        pathArgs = [NSArray arrayWithObject: sfAppMedia.settings[@"idUser"]];
        
        response = [ws requestWebServiceWithURL:[sfAppMedia.wsConfig getConfigPathWithKey:@"albumManagerMedia"] withRequestType:@"GET" withPathArguments:pathArgs withArguments:args withIdSession:sfAppMedia.settings[@"idSession"] withCache:NO];
        
        NSHTTPURLResponse *statusResponse = (NSHTTPURLResponse *) ws.responseMessage;
        if(response != nil && (statusResponse.statusCode == 200 || statusResponse.statusCode == 201))
        {
            if(response != nil && response[@"data"] != nil)
            {
                for (id object in response[@"data"])
                {
                    Media *media = [Media customClassWithProperties:object];
                    [medias addObject:media];
                }
            }
        }

        return medias;
    }
    @catch (NSException *ex)
    {
        return nil;
    }
}

+(NSMutableArray *)createXMediasWithX:(int)p_mediasCount
{
    NSMutableArray *medias = [[NSMutableArray alloc] init];
    @try
    {
        StayWS *ws = [[StayWS alloc] init];
        sfAppMedia = [StayfilmApp sharedStayfilmAppSingleton];
        NSDictionary *args = nil;
        NSDictionary *response = nil;
        NSArray *pathArgs = nil;
        
        args = @{ @"count" : [NSString stringWithFormat:@"%d", p_mediasCount] };
        
        response = [ws requestWebServiceWithURL:[sfAppMedia.wsConfig getConfigPathWithKey:@"createMedia"] withRequestType:@"GET" withPathArguments:pathArgs withArguments:args withIdSession:sfAppMedia.settings[@"idSession"] withCache:NO];
        
        NSHTTPURLResponse *statusResponse = (NSHTTPURLResponse *) ws.responseMessage;
        if(response != nil && (statusResponse.statusCode == 200 || statusResponse.statusCode == 201))
        {
            if(response != nil && response[@"data"] != nil)
            {
                for (id object in response[@"data"])
                {
                    NSString *idMedia = object;
                    [medias addObject:idMedia];
                }
            }
        }
        return medias;
    }
    @catch (NSException *ex)
    {
        return nil;
    }
}

+(NSMutableArray *)confirmMedias:(NSArray *)p_medias WithIdAlbum:(NSString *)p_idAlbum andOverrideIdAlbum:(NSString **)p_idAlbumOverride
{
    NSMutableArray *failedMedias = [[NSMutableArray alloc] init];
    @try
    {
        StayWS *ws = [[StayWS alloc] init];
        sfAppMedia = [StayfilmApp sharedStayfilmAppSingleton];
        NSMutableDictionary *args = [[NSMutableDictionary alloc] init];
        NSDictionary *response = nil;
        NSArray *pathArgs = nil;
        
        if(p_idAlbum != nil)
        {
            [args setValue:p_idAlbum forKey:@"idalbum"];
            [args setValue:p_medias forKey:@"media"];
        }
        else
        {
            [args setValue:p_medias forKey:@"media"];
        }
        
        BOOL waitingStep = YES;
        while (waitingStep) {
            waitingStep = [sfAppMedia isLoggedIn];
            waitingStep = !waitingStep;
        }
        
        response = [ws requestWebServiceWithURL:[sfAppMedia.wsConfig getConfigPathWithKey:@"createMedia"] withRequestType:@"POST" withPathArguments:pathArgs withArguments:args withIdSession:sfAppMedia.settings[@"idSession"] withCache:NO];
        
        NSHTTPURLResponse *statusResponse = (NSHTTPURLResponse *) ws.responseMessage;
        if(response != nil && (statusResponse.statusCode == 200 || statusResponse.statusCode == 201))
        {
            if(response != nil && response[@"failedMedias"] != nil)
            {
                for (id object in response[@"failedMedias"])
                {
                    NSString *idMedia = object;
                    [failedMedias addObject:idMedia];
                }
            }
            if(p_idAlbumOverride != nil && response[@"idAlbum"] != nil && *p_idAlbumOverride != nil)
            {
                *p_idAlbumOverride = response[@"idAlbum"];
            }
        }
        else
        {
            [failedMedias addObject:@"0"];
        }
        return failedMedias;
    }
    @catch (NSException *ex)
    {
        return nil;
    }
}


@end
