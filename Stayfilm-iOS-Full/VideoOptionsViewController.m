//
//  VideoOptionsViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 2/05/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <Google/Analytics.h>
#import "VideoOptionsViewController.h"

@interface VideoOptionsViewController ()

@end

@implementation VideoOptionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setLanguage];
    
    UITapGestureRecognizer *modalTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOutside)];
    modalTap.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:modalTap];
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    //Google Analytics
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"options_film-in-profile"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

-(void)tapOutside{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)setLanguage{
    
    [self.downloadButton setTitle:NSLocalizedString(@"DOWNLOAD", nil) forState:normal];
    [self.changePrivacyButton setTitle:NSLocalizedString(@"CHANGE_PRIVACY", nil) forState:normal];
    [self.deleteButtom setTitle:NSLocalizedString(@"DELETE_FILM", nil) forState:normal];
    [self.cancelButton setTitle:NSLocalizedString(@"CANCEL", nil) forState:normal];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)donwloadVideoAction:(id)sender {
    
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"options_film-in-profile"
                                                          action:@"shared"
                                                           label:@"download"
                                                           value:@1] build]];
    
     [self tapOutside];
     [self.delegate didSelectDownloadVideo:self.videoPosition];
    
}

- (IBAction)changePrivacyAction:(id)sender {
    
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"options_film-in-profile"
                                                          action:@"change-privacy"
                                                           label:@""
                                                           value:@1] build]];
    
    [self tapOutside];
    [self.delegate didSelectChangePrivacy:self.videoPosition];
}

- (IBAction)deleteVideoAction:(id)sender {
    
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"options_film-in-profile"
                                                          action:@"delete-film-in-profile"
                                                           label:@""
                                                           value:@1] build]];
    
     [self tapOutside];
     [self.delegate didSelectDeleteVideo:self.videoPosition];
    
}

- (IBAction)cancelAction:(id)sender {
    
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"options_film-in-profile"
                                                          action:@"cancel"
                                                           label:@""
                                                           value:@1] build]];
    
    [self tapOutside];
    
}

@end
