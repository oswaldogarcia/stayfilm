//
//  DeepLinkWatchViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Rick on 17/10/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import "DeepLinkWatchViewController.h"
#import "StayfilmApp.h"
#import "AppDelegate.h"
#import "Movie.h"
#import "User.h"
#import "PlayerViewController.h"
#import "DeepLinkOptionsViewController.h"

@interface DeepLinkWatchViewController ()

@property (nonatomic, strong) StayfilmApp *sfApp;
@property (nonatomic, strong) Movie *movie;
@property (nonatomic, strong) NSURLSession *thumbLoadSession;
@property (strong) NSURLSessionDataTask *task;
@property (nonatomic, assign) int retryThumbAttempts;

@end

@implementation DeepLinkWatchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.img_userImage.layer.cornerRadius = self.img_userImage.frame.size.width / 2;
    self.img_userImage.layer.masksToBounds = YES;
    
    self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    self.thumbLoadSession = [NSURLSession sessionWithConfiguration:config];
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        //Background Thread
        self.movie = [Movie getMovieWithID:self.idMovie];
        self.movie.user = [User getUserWithId:self.movie.idUser andSFAppSingleton:self.sfApp];
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            //Run UI Updates
            [self.but_Play setHidden:NO];
            [self.spinner_Loading stopAnimating];
            
            if(self.movie.user != nil) {
                self.txt_Name.text = self.movie.user.fullName;
                
                __weak typeof(self) selfDelegate = self;
                self.task = [self.thumbLoadSession dataTaskWithURL:[NSURL URLWithString:self.movie.user.photo] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    if(error)
                    {
                        NSLog(@"StayLog: PhotoCollectionImage user.photo request error: %@", error);
                    }
                    else {
                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                            selfDelegate.img_userImage.image = [UIImage imageWithData:data];
                            [selfDelegate.sfApp.imagesCache setObject:selfDelegate.img_userImage.image forKey:selfDelegate.sfApp.currentUser.photo];
                        }];
                    }
                }];
                [self.task resume];
            }
            self.txt_movieTitle.text = (self.movie.title != nil && ![self.movie.title isEqualToString:@""]) ? self.movie.title : @"";
            [self.txt_movieTitle sizeToFit];
            
            NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
            self.thumbLoadSession = [NSURLSession sessionWithConfiguration:config];
            
            self.task = [self.thumbLoadSession dataTaskWithURL:[NSURL URLWithString:self.movie.thumbnailUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                if(error)
                {
                    NSLog(@"StayLog: Movie thumb load request error: %@", error);
                    self.retryThumbAttempts++;
                    [self retryLoadMovieThumb];
                }
                else {
                    //add blur to thumbnail
                    //            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    //                self.imageWithoutBlur = [UIImage imageWithData:data];
                    //                UIImage *aux = [self.imageWithoutBlur imageWithGaussianBlur];
                    //                aux = [aux imageWithGaussianBlur];                    //blur image 2 times
                    //                self.movieThumb.image = aux;                          //blur image 3 times
                    //                [(UIActivityIndicatorView *)[self.view viewWithTag:10] stopAnimating];
                    //            }];
                    runOnMainQueueWithoutDeadlocking(^{
                        self.movieThumb.image = [UIImage imageWithData:data];
                        [(UIActivityIndicatorView *)[self.view viewWithTag:10] stopAnimating];
                        [(UIActivityIndicatorView *)[self.view viewWithTag:10] setHidden:YES];
                    });
                }
            }];
            [self.task resume];
        });
    });
}

-(void)retryLoadMovieThumb {
    
    self.task = [self.thumbLoadSession dataTaskWithURL:[NSURL URLWithString:self.movie.thumbnailUrl] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(error)
        {
            NSLog(@"StayLog: PhotoCollectionImage request error: %@", error);
            if(self.retryThumbAttempts > 3) {
                runOnMainQueueWithoutDeadlocking(^{
                    UIAlertController *alertc = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UNABLE_LOAD_MOVIE_THUMB", nil)
                                                                                    message:NSLocalizedString(@"UNABLE_LOAD_MOVIE_THUMB_MESSAGE", nil)
                                                                             preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                          handler:^(UIAlertAction * action) {
                                                                              [alertc dismissViewControllerAnimated:YES completion:nil];
                                                                          }];
                    [alertc addAction:defaultAction];
                    [self presentViewController:alertc animated:YES completion:nil];
                });
            } else {
                self.retryThumbAttempts++;
                [self retryLoadMovieThumb];
            }
        }
        else {
            runOnMainQueueWithoutDeadlocking(^{
                self.movieThumb.image = [UIImage imageWithData:data];
                [(UIActivityIndicatorView *)[self.view viewWithTag:10] stopAnimating];
                [(UIActivityIndicatorView *)[self.view viewWithTag:10] setHidden:YES];
            });
        }
    }];
    [self.task resume];
}

- (IBAction)but_Close_tapped:(id)sender {
    AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(self.sfApp.currentUser != nil && self.sfApp.currentUser.idSession != nil) {
        [appdelegate resetMovieMakerStoryboard];
    }
    else {
        [appdelegate goToLoginPage];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)optionsAction:(id)sender {
    
    DeepLinkOptionsViewController * view = [[DeepLinkOptionsViewController alloc] init];
    
    view.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    view.view.frame = self.view.frame;
    
    [self presentViewController:view animated:YES completion:nil];
    
    
}
- (IBAction)makeAFilmAction:(id)sender {
    
    for(Genres *genre in self.sfApp.sfConfig.genres){
        
        if(genre.idgenre == [self.movie.idGenre intValue]){
            self.sfApp.selectedGenre = genre;
            
            for(Template *template in genre.templates){
                if(template.idtemplate == [self.movie.idTemplate intValue]){
                    self.sfApp.selectedTemplate = template;
                }
            }
        }
    }
    
    AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if(self.sfApp.currentUser != nil && self.sfApp.currentUser.idSession != nil){
        [appdelegate resetMovieMakerStoryboard];
    }else {
        [appdelegate goToLoginPage];
    }
    
}
- (IBAction)playAction:(id)sender {
    
    
    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"Player" owner:self options:nil];
    PlayerViewController *moviePlayerViewController = [topLevelObjects objectAtIndex:0];
    moviePlayerViewController.player.automaticallyWaitsToMinimizeStalling = NO;
    moviePlayerViewController.currentMovie = self.movie;
    moviePlayerViewController.showsPlaybackControls = YES;
      
    moviePlayerViewController.view.frame = self.view.frame;
    
     moviePlayerViewController.player = [AVPlayer playerWithURL:[NSURL URLWithString:self.movie.videoUrl]];
    
    [self presentViewController:moviePlayerViewController animated:YES completion:nil];
    
}

@end
