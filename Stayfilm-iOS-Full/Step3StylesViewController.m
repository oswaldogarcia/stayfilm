//
//  Step3StylesViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Henrique on 1/5/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <Google/Analytics.h>
#import "Step3StylesViewController.h"
#import "GenreTableViewCell.h"
#import "ProgressViewController.h"
#import "Constants.h"
#import "NoAvailableOrientationViewController.h"
#import "ConfirmSubscriptionViewController.h"

@interface Step3StylesViewController ()

@property (nonatomic, strong) NSMutableArray *genresArray;
@property (nonatomic, strong) NSURLSession *thumbConnection;

@property (nonatomic, weak) Genres *selectedGenre;
@property (nonatomic, weak) Template *selectedTemplate;
@property (weak, nonatomic) StayfilmApp *sfAppStep3;
@property (nonatomic, weak) Uploader *uploaderStayfilm;
@property (nonatomic) int currentSelectionTemplate;
@property (nonatomic) int currentSelection;
@property (nonatomic) BOOL needAdjust;

@end


@implementation Step3StylesViewController

@synthesize genresArray, selectedGenre, genresTable, but_nextStep, txt_Title, genresTableHeightConstraint, uploaderStayfilm, sfAppStep3, img_butNextStep;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    self.thumbConnection = [NSURLSession sessionWithConfiguration:config];
    
    self.genresTable.contentInset = UIEdgeInsetsZero;
    [self.genresTable registerNib:[UINib nibWithNibName:@"GenreItem" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"GenreItemCell"];
    
    self.but_nextStep.layer.cornerRadius = 4;
    self.but_nextStep.layer.masksToBounds = YES;
    
    if (self.txt_Title != nil){
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:self.txt_Title.text];
    //the value paramenter defines your spacing amount, and range is the range of characters in your string the spacing will apply to. Here we want it to apply to the whole string so we take it from 0 to text.length.
    [text addAttribute:NSKernAttributeName value:[NSNumber numberWithDouble:2.0] range:NSMakeRange(0, text.length)];
    [self.txt_Title setAttributedText:text];
    }
    self.sfAppStep3 = [StayfilmApp sharedStayfilmAppSingleton];
    self.uploaderStayfilm = [Uploader sharedUploadStayfilmSingletonWithDelegate:self];
    self.genresArray = [[NSMutableArray alloc]init];
    
    @try {
        if(self.sfAppStep3.sfConfig == nil || self.sfAppStep3.sfConfig.genres == nil){
            self.sfAppStep3.sfConfig = [SFConfig getAllConfigsFromWS:self.sfAppStep3.wsConfig];
            if(self.sfAppStep3.sfConfig == nil || self.sfAppStep3.sfConfig.genres == nil){
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CONNECTION_ERROR", nil)
                                                                               message:NSLocalizedString(@"CONNECTION_ERROR_MESSAGE", nil)
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {[alert dismissViewControllerAnimated:YES completion:nil];}];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
                
                NSLog(@"StayLog: step3 error 1");
                //exit(0);
            }else{
                //remove genre without curren language image
                NSString *langcode = [[NSLocale currentLocale] objectForKey:NSLocaleLanguageCode];
                
                if([langcode isEqualToString:@"en"]){
                    for (int i = 0; i < self.sfAppStep3.sfConfig.genres.count;i++){
                        Genres *genre = self.sfAppStep3.sfConfig.genres[i];
                        if((genre.imageUrl_en != nil)){
                            [self.genresArray addObject:genre];
                        }
                    }
                }else if([langcode isEqualToString:@"es"]){
                    for (int i = 0; i < self.sfAppStep3.sfConfig.genres.count;i++){
                        Genres *genre = self.sfAppStep3.sfConfig.genres[i];
                        if((genre.imageUrl_es != nil)){
                            [self.genresArray addObject:genre];
                        }
                    }
                }else if([langcode isEqualToString:@"pt"]){
                    for (int i = 0; i < self.sfAppStep3.sfConfig.genres.count;i++){
                        Genres *genre = self.sfAppStep3.sfConfig.genres[i];
                        if((genre.imageUrl != nil)){
                            [self.genresArray addObject:genre];
                        }
                    }
                }else{
                    for (int i = 0; i < self.sfAppStep3.sfConfig.genres.count;i++){
                        Genres *genre = self.sfAppStep3.sfConfig.genres[i];
                        if((genre.imageUrl_en != nil)){
                            [self.genresArray addObject:genre];
                        }
                    }
                }
            }
        }else{
            
            //remove genre without curren language image
            NSString *langcode = [[NSLocale currentLocale] objectForKey:NSLocaleLanguageCode];
            
            if([langcode isEqualToString:@"en"]){
                for (int i = 0; i < self.sfAppStep3.sfConfig.genres.count;i++){
                    Genres *genre = self.sfAppStep3.sfConfig.genres[i];
                      if((genre.imageUrl_en != nil)){
                        [self.genresArray addObject:genre];
                    }
                }
            }else if([langcode isEqualToString:@"es"]){
                for (int i = 0; i < self.sfAppStep3.sfConfig.genres.count;i++){
                     Genres *genre = self.sfAppStep3.sfConfig.genres[i];
                        if((genre.imageUrl_es != nil)){
                            [self.genresArray addObject:genre];
                        }
                    }
            }else if([langcode isEqualToString:@"pt"]){
                for (int i = 0; i < self.sfAppStep3.sfConfig.genres.count;i++){
                    Genres *genre = self.sfAppStep3.sfConfig.genres[i];
                    if((genre.imageUrl != nil)){
                        [self.genresArray addObject:genre];
                    }
                }
            }else{
                for (int i = 0; i < self.sfAppStep3.sfConfig.genres.count;i++){
                    Genres *genre = self.sfAppStep3.sfConfig.genres[i];
                    if((genre.imageUrl_en != nil)){
                        [self.genresArray addObject:genre];
                    }
                }
            }
        }
    }
    @catch (NSException *ex) {
        NSLog(@"StayLog: EXCEPTION in viewDidLoad Step3Styles with error: %@", ex.description);
    }
    
    self.currentSelection = -1;
    
    
}

- (IBAction)Back
{
    if(self.sfAppStep3.isEditingMode) {
        
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"edit-film"
                                                              action:@"change-style"
                                                               label:@"cancel"
                                                               value:@1] build]];
    } else {
        //Google Analytics Event
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"film-production"
                                                              action:@"back-to-content"
                                                               label:@""
                                                               value:@1] build]];
    }
    
    if (self.isEditing){
        [self.navigationController popViewControllerAnimated:YES];
    }
    else {
        [self.navigationController popViewControllerAnimated:YES];
        [self.sfAppStep3.filmStrip setHidden:NO];
    }
}

- (IBAction)changeStyleAction:(id)sender {
    
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"style"
                                                          action:self.selectedGenre.slug
                                                           label:[[NSString alloc] initWithFormat:@"%lu",(unsigned long)self.selectedTemplate.idtemplate]
                                                           value:@1] build]];
    
    //Google Analytics Event
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"edit-film"
                                                          action:@"change-style"
                                                           label:@"changed"
                                                           value:@1] build]];
    
    if ([self.selectedGenre.config.campaign.idcustomer integerValue] == 37){
        
        TermsAndPolicyStyleViewController * view = [[TermsAndPolicyStyleViewController alloc] init];
        
        view.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        view.view.frame = self.view.frame;
        view.isEditing = YES;
        view.delegate = self;
        [self presentViewController:view animated:YES completion:nil];
        
    }else{
    
    
        self.sfAppStep3.editedGenre = self.selectedGenre;
        self.sfAppStep3.editedTemplate = self.selectedTemplate;
        self.sfAppStep3.editedGenre.orientation = self.selectedOrientation;
//        if(self.sfAppStep3.editedOrientation == nil){
//            self.sfAppStep3.editedOrientation = HORIZONTAL;
//        }
        
        
        
        
        [self.delegate styleChanged:self.selectedGenre Template:self.selectedTemplate];
        [self Back];
    }
}


-(void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBar.hidden = YES;
//    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
//    self.navigationController.navigationBar.shadowImage = [[UIImage alloc] init];
//    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    self.isFromExplorer = NO;
    if(self.sfAppStep3.selectedGenre != nil){
         self.isFromExplorer = YES;
     }
    
    
    if (self.sfAppStep3.isEditingMode) {
        
        UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
        
                if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
                    statusBar.backgroundColor = [UIColor colorWithRed:0.20 green:0.34 blue:0.44 alpha:1.0];
                }
        
     
        [self.navigationView setBackgroundColor:[UIColor colorWithRed:0.20 green:0.34 blue:0.44 alpha:1.0]];
        
        [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
        
        [self.backButton setImage:[UIImage imageNamed:@"ARROW_Back"] forState:UIControlStateNormal];
        
        
        Genres *gen;
        if (self.sfAppStep3.editedGenre != nil){
            gen = self.sfAppStep3.editedGenre;
        }else{
            gen = self.sfAppStep3.selectedGenre;
        }
       
       for (int i=0; i<self.genresArray.count;i++){
           
           
           if (gen.idgenre == ((Genres*)self.genresArray[i]).idgenre){
                self.currentSelection = i;
                self.selectedGenre = gen;
                
                if(self.sfAppStep3.selectedTemplate != nil) {
                    for (Template *t in self.sfAppStep3.selectedGenre.templates) {
                        if(t.idtemplate == self.sfAppStep3.selectedTemplate.idtemplate) {
                            self.selectedTemplate = t;
                            break;
                        }
                    }
                }
                [self.but_nextStep setHidden:NO];
                [self.img_butNextStep setHidden:NO];
                break;
            }
        }
    } else if(self.isFromExplorer) {
        for (int i=0; i<self.genresArray.count;i++)
        {
            if (self.sfAppStep3.selectedGenre.idgenre == ((Genres*)self.genresArray[i]).idgenre)
            {
                self.currentSelection = i;
                self.selectedGenre = self.genresArray[i];
                
                if(self.sfAppStep3.selectedTemplate != nil)
                {
                    for (Template *t in self.sfAppStep3.selectedGenre.templates) {
                        if(t.idtemplate == self.sfAppStep3.selectedTemplate.idtemplate) {
                            self.selectedTemplate = t;
                            break;
                        }
                    }
                }
                [self.but_nextStep setHidden:NO];
                [self.img_butNextStep setHidden:NO];
                break;
            }
        }
    }

    [self.sfAppStep3.filmStrip setHidden:YES];
    
    if(self.sfAppStep3.isEditingMode){
        
        [self.but_nextStep setHidden:YES];
        [self.img_butNextStep setHidden:YES];
    }
    
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    // Disable swipe gesture to go back to previous viewcontroller
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
//    self.uploaderStayfilm = [Uploader sharedUploadStayfilmSingletonWithDelegate:self];
//
    [self adjustHeightOfGenresTableview:self.sfAppStep3.isEditingMode];
    self.needAdjust = !self.sfAppStep3.isEditingMode;

    [self.sfAppStep3.filmStrip connectivityViewUpdate];
    
    if(self.isFromExplorer)
        self.needAdjust = YES;
    
    if(self.sfAppStep3.isEditingMode) {
        //Google Analytics
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:@"film-editing_style"];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    } else {
        //Google Analytics
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:@"moviemaker_style"];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    }
    
    if(self.sfAppStep3.isEditingMode || self.isFromExplorer) {
        
        if(self.currentSelection != 0) {
            if(self.genresArray.count-1 == self.currentSelection) {
                CGRect visibleRect = [self.view convertRect:[self.genresTable rectForRowAtIndexPath:[NSIndexPath indexPathForRow:self.currentSelection inSection:0]] fromView:self.genresTable];
                
                [self.scrollView setContentOffset:CGPointMake(0,(visibleRect.origin.y - 260) - 60)animated:YES];
                
                [self adjustHeightOfGenresTableview:YES];
            } else {
                CGRect visibleRect = [self.view convertRect:[self.genresTable rectForRowAtIndexPath:[NSIndexPath indexPathForRow:self.currentSelection inSection:0]] fromView:self.genresTable];
                
                [self.scrollView setContentOffset:CGPointMake(0,((visibleRect.origin.y - 260) + 60))animated:YES];
            
                [self adjustHeightOfGenresTableview:YES];
            }
        } else {
            
            [self.scrollView setContentOffset:CGPointMake(0,0)animated:YES];
            
            [self adjustHeightOfGenresTableview:YES];
        }
         
    }
    
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.sfAppStep3.filmStrip connectivityViewUpdate];
}

- (void)adjustHeightOfGenresTableview: (BOOL)adjust
{
    CGFloat height;
    if([[UIScreen mainScreen] bounds].size.height <= kIPhone5Height){
        height = self.genresArray.count * kSmallIPhoneCellSize + (self.view.frame.size.height - self.but_nextStep.frame.origin.y);
    }else if([[UIScreen mainScreen] bounds].size.height >= kIPadHeight){
        
        height = self.genresArray.count * kIPadCellSize + (self.view.frame.size.height - self.but_nextStep.frame.origin.y);
        
    }else{
        height = self.genresArray.count * kIPhoneCellSize + (self.view.frame.size.height - self.but_nextStep.frame.origin.y);
    }
    [UIView animateWithDuration:0.8 animations:^{
        if (adjust){
            if (self.genresTableHeightConstraint.constant < (height + kTableAdjust)){
                    self.genresTableHeightConstraint.constant = height + kTableAdjust + 10;
            }
        }else{
            self.genresTableHeightConstraint.constant = height;
        }

        [self.view setNeedsUpdateConstraints];
      
    }];
}


#pragma mark - Table delegates

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([indexPath row] == self.currentSelection) {
        
        
        if(self.isEditing){
            
            if (self.needAdjust){
                [self adjustHeightOfGenresTableview:YES];
                self.needAdjust = NO;
            }
            
        }
        if([[UIScreen mainScreen] bounds].size.height == kIPhone5Height){

            return kSelectedCell-10;

        }else{
            return kSelectedCell;
        }
        
    }else{
        if([[UIScreen mainScreen] bounds].size.height <= kIPhone5Height){
            
            if (([indexPath row] == self.genresArray.count-1)){
                
                return kSmallIPhoneCellSize + (self.view.frame.size.height - self.but_nextStep.frame.origin.y);
            }else{
            
            return kSmallIPhoneCellSize;
            }
        }else if([[UIScreen mainScreen] bounds].size.height >= kIPadHeight){
            
            if (([indexPath row] == self.genresArray.count-1)){
                
                return kIPadCellSize + (self.view.frame.size.height - self.but_nextStep.frame.origin.y);
            }else{
                
                return kIPadCellSize;
            }
            
        }else{
            
            if (([indexPath row] == self.genresArray.count-1)){
                
                return kIPhoneCellSize + (self.view.frame.size.height - self.but_nextStep.frame.origin.y);
            }else{
            
            return kIPhoneCellSize;
            }
        }
        
        
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(self.genresArray.count > 0)
    {
        [(UIActivityIndicatorView *)[self.view viewWithTag:10] stopAnimating];
    }
    return [self.genresArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try
    {
        static NSString *simpleTableIdentifier = @"StyleTableViewCell";
        
        Genres *genre = self.genresArray[indexPath.row];
        
        StyleTableViewCell *cell = (StyleTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        cell.currentRow = (int)indexPath.row;
        cell.selectedGenre = genre;
        cell.delegate = self;
        [cell.horizontalButton setHidden:YES];
        [cell.verticalButton setHidden:YES];
        cell.isVerticalAvailable = genre.hasVertical;
        
        
        cell.isPremium = genre.isPaid;
        cell.isBlocked =![[NSUserDefaults standardUserDefaults] boolForKey:@"hasSubscription"];
        // !self.sfAppStep3.currentUser.hasSubscription;
        
        
        if(self.sfAppStep3.isEditingMode || self.isFromExplorer){
            
            Genres *gen;
            if (self.sfAppStep3.editedGenre != nil){
                gen = self.sfAppStep3.editedGenre;
            }else{
                gen = self.sfAppStep3.selectedGenre;
            }

            if (gen.idgenre == ((Genres*)self.genresArray[indexPath.row]).idgenre){
                
                self.indexSelectedGenre = indexPath;
                
                if (_changeEditing){
                    cell.isEditing = NO;
                }else{
                    cell.isEditing = YES;
                }
                
                if(self.sfAppStep3.editedGenre != nil || self.changeEditing){
                  if (self.sfAppStep3.editedGenre.orientation == VERTICAL){
                      [cell changeToVertical:nil];
                  }else{
                      [cell changeToHorizontal:nil];
                  }
                }else if (self.selectedOrientation != NO_ORIENTATION){
                    
                    if (self.selectedOrientation == VERTICAL){
                        
                        [cell changeToVertical:nil];
                    }else{
                        [cell changeToHorizontal:nil];
                    }
                
                    
                }else{
                    if (self.sfAppStep3.selectedGenre.orientation == VERTICAL){
                        
                        [cell changeToVertical:nil];
                    }else{
                        [cell changeToHorizontal:nil];
                    }
                    
                }
                
                for (int i=0; i< gen.templates.count;i++){
                    
                    Template *template;
                    if (self.sfAppStep3.editedTemplate != nil){
                        template = self.sfAppStep3.editedTemplate;
                    }else{
                        template = self.sfAppStep3.selectedTemplate;
                    }
                    
                    if(cell.selectedGenre.templates[i] == template)
                    {
                        cell.currentTemplateSelection = i;
                        self.currentSelectionTemplate = i;
                        if(i != 0) {
                            [cell.templatesCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
                        }
                    }
                }
            }
        }
        
       [cell initCell];
        
        return cell;
    }
    @catch (NSException *ex)
    {
        return nil;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        
        
        CGRect visibleRect = [self.view convertRect:[self.genresTable rectForRowAtIndexPath:indexPath] fromView:self.genresTable];
        
        if (self.currentSelection != (int)indexPath.row){
            self.currentSelection  = (int)indexPath.row;
            if (self.needAdjust){
                [self adjustHeightOfGenresTableview:YES];
                self.needAdjust = NO;
                
                if ( visibleRect.origin.y + kExtraScrollValidation > but_nextStep.frame.origin.y){
                    CGFloat extraScroll = 10;

                    if (self.currentSelection == self.genresArray.count-2){
                        extraScroll = kIPhoneCellSize;
                    }

                    [self.scrollView setContentOffset:
                     CGPointMake(0, self.scrollView.contentOffset.y + ((visibleRect.origin.y + kSelectedCell) - but_nextStep.frame.origin.y) + extraScroll)animated:YES];

                }

                StyleTableViewCell* cell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:self.currentSelection inSection:0]];
                              
                if(self.currentSelectionTemplate < cell.selectedGenre.templates.count){
                    
                [cell.templatesCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:self.currentSelectionTemplate inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
                    
                }
            }
            
            
        }else{
            
            self.currentSelection = -1;
            [self adjustHeightOfGenresTableview:NO];
            self.needAdjust = YES;
        }
        
        if (self.sfAppStep3.isEditingMode || self.isFromExplorer){

            [tableView beginUpdates];
            if(!_changeEditing){
                self.changeEditing = YES;
                StyleTableViewCell* cell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:self.indexSelectedGenre.row inSection:0]];
                [cell changeToHorizontal:nil];
                
                [tableView reloadRowsAtIndexPaths:@[self.indexSelectedGenre] withRowAnimation:UITableViewRowAnimationNone];
            }
            [tableView endUpdates];
        }else{
          
            [tableView beginUpdates];
            [tableView endUpdates];
         
        }
        
        
    } @catch ( NSException *ex ) {
        NSLog(@"StayLog: EXCEPTION in didSelectRowAtIndextPath with error: %@", ex.description);
    }
}

- (void)didSelectTemplate:(Genres *)genre Template:(Template *)selectedTemplate{
  
    
    self.selectedGenre = genre;
    self.selectedTemplate = selectedTemplate;
    
    
    if(self.sfAppStep3.isEditingMode){
        
        [self.but_nextStep setHidden:YES];
        [self.img_butNextStep setHidden:YES];
        
        if (self.sfAppStep3.selectedTemplate != self.selectedTemplate){
            
            [self.backButton setImage:nil forState:UIControlStateNormal];
            [self.widthCancelConstraint setConstant:95.0];
            [self.backButton setTitle:NSLocalizedString(@"CANCEL", nil) forState:UIControlStateNormal];
            
            [self.changeStyleButton setHidden:NO];
            
        }
        else{
            
            if (self.sfAppStep3.selectedGenre.orientation != self.selectedOrientation){
                
                [self.backButton setImage:nil forState:UIControlStateNormal];
                [self.widthCancelConstraint setConstant:95.0];
                [self.backButton setTitle:NSLocalizedString(@"CANCEL", nil) forState:UIControlStateNormal];
                
                [self.changeStyleButton setHidden:NO];
                
            }else{
                [self.backButton setImage:[UIImage imageNamed:@"ARROW_Back"] forState:UIControlStateNormal];
                [self.widthCancelConstraint setConstant:44.0];
                [self.backButton setTitle:@"" forState:UIControlStateNormal];
                
                [self.changeStyleButton setHidden:YES];
                
            }
            
        }
        
    } else {
        
        
        [self.but_nextStep setHidden:NO];
        [self.img_butNextStep setHidden:NO];
        
        self.sfAppStep3.selectedGenre = genre;
        self.sfAppStep3.selectedTemplate = selectedTemplate;

    }
}

- (void)didDeselectTemplate{
    if(self.sfAppStep3.isEditingMode){
        
        [self.backButton setImage:[UIImage imageNamed:@"ARROW_Back"] forState:UIControlStateNormal];
        [self.widthCancelConstraint setConstant:44.0];
        [self.backButton setTitle:@"" forState:UIControlStateNormal];
        
        [self.changeStyleButton setHidden:YES];
        
    }else{
        
        self.sfAppStep3.selectedGenre = nil;
        self.sfAppStep3.selectedTemplate = nil;
        
        [self.but_nextStep setHidden:YES];
        [self.img_butNextStep setHidden:YES];
    }
}

- (void)noAvailableOrientation{
    
   NoAvailableOrientationViewController * view = [[NoAvailableOrientationViewController alloc] init];
    
    view.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    view.view.frame = self.view.frame;
    
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"orientation"
                                                          action: @"Vertical-not-available"
                                                           label:@""
                                                           value:@1] build]];
    
    [self presentViewController:view animated:YES completion:nil];
    
}

- (void)setFilmOrintation:(StayfilmTypesFilmOrientation)orientation{
    
    self.selectedOrientation = orientation;
    
    if (self.sfAppStep3.isEditingMode){
        
        [self.but_nextStep setHidden:YES];
        [self.img_butNextStep setHidden:YES];
        
        if(self.sfAppStep3.editedGenre != nil || (self.selectedGenre != self.sfAppStep3.selectedGenre)){
        
                [self.backButton setImage:nil forState:UIControlStateNormal];
                [self.widthCancelConstraint setConstant:95.0];
                [self.backButton setTitle:NSLocalizedString(@"CANCEL", nil) forState:UIControlStateNormal];
                
                [self.changeStyleButton setHidden:NO];
            
        }else{
            
            if ((self.sfAppStep3.selectedGenre.orientation != self.selectedOrientation && self.sfAppStep3.selectedGenre == self.selectedGenre) || (self.sfAppStep3.selectedGenre.orientation == self.selectedOrientation && self.sfAppStep3.selectedTemplate != self.selectedTemplate)) {
                
                [self.backButton setImage:nil forState:UIControlStateNormal];
                [self.widthCancelConstraint setConstant:95.0];
                [self.backButton setTitle:NSLocalizedString(@"CANCEL", nil) forState:UIControlStateNormal];
                
                [self.changeStyleButton setHidden:NO];
                
            }
            else{
                
                [self.backButton setImage:[UIImage imageNamed:@"ARROW_Back"] forState:UIControlStateNormal];
                [self.widthCancelConstraint setConstant:44.0];
                [self.backButton setTitle:@"" forState:UIControlStateNormal];
                
                [self.changeStyleButton setHidden:YES];
                
            }
            
        }
        
    }
    
}
#pragma mark - Purchase
- (void)unlockStyle:(Template *)selectedTemplate{
    
    SubscriptionModalViewController * view = [[SubscriptionModalViewController alloc] init];
    view.delegate = self;
    view.currentTemplate = selectedTemplate;
    view.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    view.view.frame = self.view.frame;
    
    [self presentViewController:view animated:YES completion:nil];
    
    
}
-(void)subscriptionConfirmed{
    

    if([[NSUserDefaults standardUserDefaults] boolForKey:@"hasSubscription"]){
        
        ConfirmSubscriptionViewController * view = [[ConfirmSubscriptionViewController alloc] init];
        view.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        view.view.frame = self.view.frame;
        
        self.currentSelection = -1;
        [self.genresTable reloadData];
        [self presentViewController:view animated:YES completion:nil];
    }
    
}



#pragma mark - Terms Style Delegates

- (void)confirmTerms{
    if (self.sfAppStep3.isEditingMode){
        
        self.sfAppStep3.editedGenre = self.selectedGenre;
        self.sfAppStep3.editedTemplate = self.selectedTemplate;
        
        [self.delegate styleChanged:self.selectedGenre Template:self.selectedTemplate];
        [self Back];
    }else{
        [self performSegueWithIdentifier:@"Step3toFinalPreview" sender:nil];
    }
}

#pragma mark - Uploader Delegates

-(void)uploadProgressChanged:(NSNumber *)progress
{
    // Not important on this page.
}

- (void)converterDidFailMedia:(NSString *)type
{
    NSString *message = nil;
    if(self.sfAppStep3 == nil)
    {
        self.sfAppStep3 = [StayfilmApp sharedStayfilmAppSingleton];
    }
    if ( (self.sfAppStep3.isEditingMode)?
        (self.sfAppStep3.editedMedias.count < [self.sfAppStep3.sfConfig.min_photos intValue]) :
        (self.sfAppStep3.finalMedias.count < [self.sfAppStep3.sfConfig.min_photos intValue]) ) {
        message = [NSString stringWithFormat:NSLocalizedString(@"UPLOADER_CONVERT_FAILED_UPLOAD_AGAIN_MESSAGE", nil), (self.sfAppStep3.isEditingMode)? ([self.sfAppStep3.sfConfig.min_photos intValue] - self.sfAppStep3.editedMedias.count) : ([self.sfAppStep3.sfConfig.min_photos intValue] - self.sfAppStep3.finalMedias.count)];
    }
    else if ([type isEqualToString:@"image"]) {
        message = NSLocalizedString(@"UPLOADER_CONVERT_FAILED_IMAGE_MESSAGE", nil);
    }
    else
    {
        message = NSLocalizedString(@"UPLOADER_CONVERT_FAILED_VIDEO_MESSAGE", nil);
    }
    
    if(self.sfAppStep3.isEditingMode) {
        @synchronized (self.sfAppStep3.editedMedias) {
            for (id obj in self.sfAppStep3.editedMedias) {
                if([obj isKindOfClass:[PHAsset class]]) {
                    BOOL find = NO;
                    for (PHAsset *asset in self.uploaderStayfilm.listUploadMedias) {
                        if(asset == obj) {
                            find = YES;
                            break;
                        }
                    }
                    if(!find) {
                        NSUInteger i = [self.sfAppStep3.editedMedias indexOfObject:obj];
                        [self.sfAppStep3.editedMedias removeObjectAtIndex:i];
                        break;
                    }
                }
            }
        }
    } else {
        @synchronized (self.sfAppStep3.finalMedias) {
            for (id obj in self.sfAppStep3.finalMedias) {
                if([obj isKindOfClass:[PHAsset class]]) {
                    BOOL find = NO;
                    for (PHAsset *asset in self.uploaderStayfilm.listUploadMedias) {
                        if(asset == obj) {
                            find = YES;
                            break;
                        }
                    }
                    if(!find) {
                        NSUInteger i = [self.sfAppStep3.finalMedias indexOfObject:obj];
                        [self.sfAppStep3.finalMedias removeObjectAtIndex:i];
                        break;
                    }
                }
            }
        }
    }
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_CONVERT_FAILED_TITLE", nil)
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)uploadDidFailMedia:(NSString *)type
{
    NSString *message = nil;
    if(self.sfAppStep3 == nil)
    {
        self.sfAppStep3 = [StayfilmApp sharedStayfilmAppSingleton];
    }
    if ( (self.sfAppStep3.isEditingMode)? (self.sfAppStep3.editedMedias.count < [self.sfAppStep3.sfConfig.min_photos intValue]) : (self.sfAppStep3.finalMedias.count < [self.sfAppStep3.sfConfig.min_photos intValue])) {
        message = [NSString stringWithFormat:NSLocalizedString(@"UPLOADER_CONVERT_FAILED_UPLOAD_AGAIN_MESSAGE", nil), (self.sfAppStep3.isEditingMode)? ([self.sfAppStep3.sfConfig.min_photos intValue] - self.sfAppStep3.editedMedias.count) : ([self.sfAppStep3.sfConfig.min_photos intValue] - self.sfAppStep3.finalMedias.count)];
    }
    else if ([type isEqualToString:@"image"]) {
        message = NSLocalizedString(@"UPLOADER_UPLOAD_FAILED_IMAGE_MESSAGE", nil);
    }
    else
    {
        message = NSLocalizedString(@"UPLOADER_UPLOAD_FAILED_VIDEO_MESSAGE", nil);
    }
    
    if(self.sfAppStep3.isEditingMode) {
        @synchronized (self.sfAppStep3.editedMedias) {
            for (id obj in self.sfAppStep3.editedMedias) {
                if([obj isKindOfClass:[PHAsset class]]) {
                    BOOL find = NO;
                    for (PHAsset *asset in self.uploaderStayfilm.listUploadMedias) {
                        if(asset == obj) {
                            find = YES;
                            break;
                        }
                    }
                    if(!find) {
                        NSUInteger i = [self.sfAppStep3.editedMedias indexOfObject:obj];
                        [self.sfAppStep3.editedMedias removeObjectAtIndex:i];
                        break;
                    }
                }
            }
        }
    } else {
        @synchronized (self.sfAppStep3.finalMedias) {
            for (id obj in self.sfAppStep3.finalMedias) {
                if([obj isKindOfClass:[PHAsset class]]) {
                    BOOL find = NO;
                    for (PHAsset *asset in self.uploaderStayfilm.listUploadMedias) {
                        if(asset == obj) {
                            find = YES;
                            break;
                        }
                    }
                    if(!find) {
                        NSUInteger i = [self.sfAppStep3.finalMedias indexOfObject:obj];
                        [self.sfAppStep3.finalMedias removeObjectAtIndex:i];
                        break;
                    }
                }
            }
        }
    }
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_UPLOAD_FAILED_TITLE", nil)
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)uploadDidFail:(Uploader *)uploader
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_ERROR_TITLE", nil)
                                                                   message:NSLocalizedString(@"UPLOADER_ERROR_MESSAGE", nil)
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)uploadDidStartSendingConfirmationMedias:(Uploader *)uploader
{
    // not important on this page.
}

-(void)uploadDidStart
{
    // does not happen in this page
}

-(void)uploadDidFinish:(Uploader *)uploader
{
    if(self.uploaderStayfilm.listUploadedMedias != nil && self.uploaderStayfilm.listUploadedMedias.count > 0) {
        for (PHAsset * asset in self.uploaderStayfilm.listUploadedMedias.allValues) {
            if(self.sfAppStep3.isEditingMode) {
                if(![self.sfAppStep3.editedMedias containsObject:asset]) {
                    [self.sfAppStep3.editedMedias addObject:asset];
                }
            } else {
                if(![self.sfAppStep3.finalMedias containsObject:asset]) {
                    [self.sfAppStep3.finalMedias addObject:asset];
                }
            }
        }
    }
    if(self.sfAppStep3.isDebugMode) {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"UPLOADER_FINISHED", nil)
                                                                       message:[[NSString alloc] initWithFormat:NSLocalizedString(@"UPLOADED_X_MEDIAS", nil), uploader.listUploadedMedias.count]
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

-(void)uploadFinishedMedia:(PHAsset *)asset {
    // not important on this view
}


#pragma mark - Navigation


- (IBAction)but_CreateFilm_Tapped:(id)sender {
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"film-production"
                                                          action:@"started-produce"
                                                           label:@"film"
                                                           value:@1] build]];

    if ([self.selectedGenre.config.campaign.idcustomer integerValue] == 37){
        
        TermsAndPolicyStyleViewController * view = [[TermsAndPolicyStyleViewController alloc] init];
        
        view.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        view.view.frame = self.view.frame;
        view.delegate = self;
        [self presentViewController:view animated:YES completion:nil];
    }else{
        [self performSegueWithIdentifier:@"Step3toFinalPreview" sender:sender];
    }
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    @try {
        
        if ([segue.identifier isEqualToString:@"Step3toFinalPreview"]) {
            //ProgressViewController *progressViewController = segue.destinationViewController;
            
            if(self.sfAppStep3 == nil)
            {
                self.sfAppStep3 = [StayfilmApp sharedStayfilmAppSingleton];
            }
            self.sfAppStep3.selectedGenre = self.selectedGenre;
            self.sfAppStep3.selectedTemplate = self.selectedTemplate;
            self.sfAppStep3.selectedGenre.orientation = self.selectedOrientation;
//            if(self.sfAppStep3.selectedOrientation == nil){
//                self.sfAppStep3.selectedOrientation = @"horizontal";
//            }
            //Google Analytics Event
            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"style"
                                                                action:self.sfAppStep3.selectedGenre.slug
                                                                   label:[[NSString alloc] initWithFormat:@"%lu", (unsigned long)self.sfAppStep3.selectedTemplate.idtemplate]
                                                                   value:@1] build]];
            
            NSString *orientation;
            if(self.sfAppStep3.selectedGenre.orientation == VERTICAL){
                orientation = @"vertical";
            }else{
                orientation = @"horizontal";
            }
            //Google Analytics Event
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"orientation"
                                                            action:orientation
                                                                   label:@""
                                                                   value:@1] build]];
            
        }
    }
    @catch (NSException *exception) {
        NSLog(@"StayLog: EXCEPTION segue Step3 to Progress with error: %@", exception.description);
    }
}

@end
