//
//  RecoverConfirmationViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Henrique on 5/2/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import <Google/Analytics.h>
#import "RecoverConfirmationViewController.h"
#import "StayfilmApp.h"
#import "User.h"
#import "AppDelegate.h"
#import "Reachability.h"

@interface RecoverConfirmationViewController ()

@property (nonatomic, weak) StayfilmApp *sfApp;
@property (nonatomic, assign) BOOL isLoggingIn;
@property (nonatomic, assign) int attempts;

@end

@implementation RecoverConfirmationViewController

@synthesize lbl_Text, img_Check, spinner_loader, email, password;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.attempts = 0;
    self.isLoggingIn = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)connected {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    //Google Analytics
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"new-password_successful"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    if(self.sfApp == nil) {
        self.sfApp = [StayfilmApp sharedStayfilmAppSingleton];
    }
    [NSTimer scheduledTimerWithTimeInterval:1.5 repeats:NO block:^(NSTimer * _Nonnull timer) {
        [self.spinner_loader setHidden:NO];
        self.lbl_Text.alpha = 0.0;
        self.lbl_Text.text = NSLocalizedString(@"LOGGING_IN", nil);
        [UIView animateWithDuration:0.3 animations:^{
            self.lbl_Text.alpha = 1.0;
            self.img_Check.alpha = 0.0;
        } completion:^(BOOL finished) {
            [timer invalidate];
        }];
    }];
    [self logInUser];
}

-(void)logInUser {
    @try {
        self.attempts++;
        if(![self connected])
        {
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CONNECTION_ERROR", nil)
                                                                           message:NSLocalizedString(@"CONNECTION_ERROR_MESSAGE", nil)
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {
                                                                      AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                                                                      [appdelegate goToLoginPage];
                                                                      
                                                                  }];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
            return;
        }
        
        User *appUser = [User loginWithUsername:self.email withPassword:self.password andSFAppSingleton:self.sfApp];
        if(appUser == nil)
        {
            [self.img_Check setHidden:YES];
            [self.spinner_loader setHidden:NO];
            self.lbl_Text.text = NSLocalizedString(@"CONNECTION_ERROR_MESSAGE_2", nil);
            if(self.attempts > 3) {
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CONNECTION_ERROR", nil)
                                                                               message:NSLocalizedString(@"CONNECTION_ERROR_MESSAGE", nil)
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {
                                                                          AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                                                                          [appdelegate goToLoginPage];
                                                                      }];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
                return;
            }
            [self logInUser];
        }
        else if(!appUser.userExists && !appUser.isResponseError) //TODO: register user maybe?
        {
            if(self.attempts > 3) {
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"LOGIN_FAILED", nil)
                                                                               message:[NSString stringWithFormat:NSLocalizedString(@"LOGIN_FAILED_WITH_MESSAGE", nil), appUser.responseErrorMessage, nil]
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {
                                                                          AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                                                                          [appdelegate goToLoginPage];
                                                                      }];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
                return;
            }
            [self logInUser];
        }
        else if(!appUser.isLogged)
        {
            if(self.attempts > 3) {
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"LOGIN_FAILED", nil)
                                                                               message:[NSString stringWithFormat:NSLocalizedString(@"LOGIN_FAILED_WITH_MESSAGE", nil), appUser.responseErrorMessage, nil]
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {
                                                                          AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                                                                          [appdelegate goToLoginPage];
                                                                      }];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
                return;
            }
            [self logInUser];
        }
        else //user logged in successfully
        {
            if(self.sfApp.settings == nil)
            {
                self.sfApp.settings = [[NSMutableDictionary alloc] init];
            }
            [self.sfApp.settings setObject:@"1" forKey:@"LOGGEDIN_Email"];
            [self.sfApp.settings setObject:appUser.idSession forKey:@"idSession"];
            [self.sfApp.settings setObject:appUser.idUser forKey:@"idUser"];
            [self.sfApp saveSettings];
            self.sfApp.currentUser = appUser;
            
            NSData *dataSave = [NSKeyedArchiver archivedDataWithRootObject:appUser];
            [[NSUserDefaults standardUserDefaults] setObject:dataSave forKey:@"currentUser"];
            
            [self.spinner_loader setHidden:YES];
            self.attempts = 0;
            [self moveToStep1];
        }
    } @catch (NSException *ex) {
        NSLog(@"StayLog: EXCEPTION in EnterButtonAction with error: %@", ex.description);
    }
}

-(void)moveToStep1
{
    @try {
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"MovieMaker" bundle:nil];
        UIViewController *vc = [story instantiateInitialViewController];
        [self showViewController:vc sender:nil];
    }
    @catch (NSException *exception) {
        NSLog(@"StayLog: EXCEPTION MoveToStep1 exception with error: %@", exception.description);
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
