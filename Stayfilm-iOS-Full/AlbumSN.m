//
//  AlbumSN.m
//  Stayfilm-iOS-Full
//
//  Created by Henrique Ormonde on 28/09/17.
//  Copyright © 2017 Stayfilm. All rights reserved.
//

#import "AlbumSN.h"

@implementation AlbumSN

@synthesize idAlbum, name, link, imageLEFT, imageRIGHT, imageBIG, count, idMedias;


+ (id)customClassWithProperties:(NSDictionary *)properties {
    if(properties != nil)
    {
        NSMutableDictionary *filteredObjs = [[NSMutableDictionary alloc] init];
        for (NSString* key in properties) {
            id value = [properties objectForKey:key];
            if ([key isEqualToString:@"id"]) {
                [filteredObjs setValue:value forKey:@"idAlbum"];
            }
            else if ([key isEqualToString:@"name"]) {
                [filteredObjs setValue:value forKey:@"name"];
            }
            else if ([key isEqualToString:@"link"]) {
                [filteredObjs setValue:value forKey:@"link"];
            }
            else if ([key isEqualToString:@"cover_photo"] || [key isEqualToString:@"cover"]) {
                [filteredObjs setValue:value forKey:@"imageBIG"];
            }
            else if ([key isEqualToString:@"imageLEFT"]) {
                [filteredObjs setValue:value forKey:@"imageLEFT"];
            }
            else if ([key isEqualToString:@"imageRIGHT"]) {
                [filteredObjs setValue:value forKey:@"imageRIGHT"];
            }
            else if ([key isEqualToString:@"picture"]) {
                id valueData = [value objectForKey:@"data"];
                [filteredObjs setValue:valueData[@"url"] forKey:@"imageBIG"];
            }
            else if ([key isEqualToString:@"count"]) {
                [filteredObjs setValue:value forKey:@"count"];
            }
        }
        return [[self alloc] initWithProperties:filteredObjs];
    }
    else
    {
        return nil;
    }
}

- (id)initWithProperties:(NSDictionary *)properties {
    if (self = [self init]) {
        @try {
            [self setValuesForKeysWithDictionary:properties];
        }
        @catch (NSException *ex) {
            return nil;
        }
    }
    return self;
}

-(id)init {
    self = [super init];
    if (0 != self) {
        self.idAlbum = nil;
        self.imageBIG = nil;
        self.imageRIGHT = nil;
        self.imageLEFT = nil;
        self.count = 0;
        self.idMedias = [[NSMutableArray alloc] init];
        self.after = nil;
    }
    return self;
}


@end
