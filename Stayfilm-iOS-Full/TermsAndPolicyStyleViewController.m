//
//  TermsAndPolicyStyleViewController.m
//  Stayfilm-iOS-Full
//
//  Created by Oswaldo Garcia on 12/6/18.
//  Copyright © 2018 Stayfilm. All rights reserved.
//

#import "TermsAndPolicyStyleViewController.h"
#import <Google/Analytics.h>
@interface TermsAndPolicyStyleViewController ()

@end

@implementation TermsAndPolicyStyleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *modalTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOutside)];
    modalTap.numberOfTapsRequired = 1;
    [self.outView addGestureRecognizer:modalTap];
    
    [self.confirmButton setEnabled:NO];
    [self.confirmButton setBackgroundColor:[UIColor colorWithRed:0.87 green:0.86 blue:0.87 alpha:1.0]];
    
    [self setTextLabels];
    [self setlanguage];
}
- (void)viewDidAppear:(BOOL)animated{

    if (self.isEditing){
        //Google Analytics
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:@"film-editing_style_curation"];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
        
        
    }else{
    //Google Analytics
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"moviemaker_curation"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    }
}

-(void)setlanguage{

    self.mainTextLabel.text =  NSLocalizedString(@"TO_CHOOSE_STYLE", nil);
    self.noSmokingLabel.text = NSLocalizedString(@"NO_SMOKING", nil);
    self.noIlegalLabel.text = NSLocalizedString(@"NO_ILEGAL", nil);
    self.noCommercialLabel.text = NSLocalizedString(@"NO_COMMERCIAL", nil);
    
    [self.confirmButton setTitle:NSLocalizedString(@"CONFIRM", nil) forState:UIControlStateNormal];


}


-(void)setTextLabels{
    
    UIFont *font1 = [UIFont fontWithName:@"ProximaNova-Regular" size:18];
    UIFont *font2 = [UIFont fontWithName:@"ProximaNova-Bold" size:18];
    UIColor *greenColor =  [UIColor colorWithRed:0.39 green:0.75 blue:0.60 alpha:1.0];
    NSDictionary *fontDict1 = [NSDictionary dictionaryWithObject: font1 forKey:NSFontAttributeName];
    NSMutableDictionary *fontDict2 = [[NSMutableDictionary alloc]init];
    [fontDict2 setObject:font2  forKey:NSFontAttributeName];
    [fontDict2 setObject:greenColor  forKey:NSForegroundColorAttributeName];
    
    //set Disney Text
    NSMutableAttributedString *fontType1 = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"I_AGREE", nil) attributes: fontDict1];
    
    NSMutableAttributedString *fontType2 = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"TERMS_DISNEY", nil) attributes: fontDict2];
    
    [fontType1 appendAttributedString:fontType2];
    self.termsDisneyLabel.attributedText = fontType1;
    
    //set Stayfilm Text
    NSMutableAttributedString *fontType3 = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"I_AGREE", nil) attributes: fontDict1];
    
    NSMutableAttributedString *fontType4 = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"TERMS_STAYFILM", nil) attributes: fontDict2];
    
    [fontType3 appendAttributedString:fontType4];
    self.termsSFLabel.attributedText = fontType3;
}

-(void)tapOutside{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)disneySwitchAction:(id)sender {
    [self setEnableConfirmButton];
    
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"curation"
                                                          action:@"apply_tugle"
                                                           label:@"terms_disney"
                                                           value:@1] build]];
    
}

- (IBAction)sfSwitchAction:(id)sender {
    [self setEnableConfirmButton];
    
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"curation"
                                                          action:@"apply_tugle"
                                                           label:@"terms-stayfilm"
                                                           value:@1] build]];
}
- (IBAction)disneyTermsActions:(id)sender {
    
    
    
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"curation"
                                                          action:@"link"
                                                           label:@"terms_disney"
                                                           value:@1] build]];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://www.stayfilm.com/institutional/customerterms/disney2019"]];
    
}

- (IBAction)sfTermsAction:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://www.stayfilm.com/institutional/terms"]];
    
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"curation"
                                                          action:@"link"
                                                           label:@"terms-stayfilm"
                                                           value:@1] build]];
}

-(void)setEnableConfirmButton{
    if(self.termsSFSwitch.on && self.termsDineySwitch.on){
        [self.confirmButton setEnabled:YES];
        [self.confirmButton setBackgroundColor:[UIColor colorWithRed:0.39 green:0.75 blue:0.60 alpha:1.0]];
    }else{
        [self.confirmButton setEnabled:NO];
        [self.confirmButton setBackgroundColor:[UIColor colorWithRed:0.87 green:0.86 blue:0.87 alpha:1.0]];
    }
}

- (IBAction)confirmAction:(id)sender {
    [self tapOutside];
    [self.delegate confirmTerms];
    
    //Google Analytics Event
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"curation"
                                                          action:@"confirm_buttom"
                                                           label:@""
                                                           value:@1] build]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
